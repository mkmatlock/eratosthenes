/*
 * Copyright (c) 2012 University of Tartu
 */
package org.jbibtex;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DigitStringValue extends LiteralValue {

	public DigitStringValue(String string){
		super(string);
	}

	@Override
	protected String format(){
		return getString();
	}

	@Override
	public Set<Key> getReferences() {
		return new HashSet<Key>();
	}

	@Override
	public void assignValues(Map<Key, BibTeXString> strings) {
		
	}
}