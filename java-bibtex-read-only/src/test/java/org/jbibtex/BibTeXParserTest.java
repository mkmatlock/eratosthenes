/*
 * Copyright (c) 2012 University of Tartu
 */
package org.jbibtex;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class BibTeXParserTest {

	@Before
	public void initAckMacros(){
		String[] macros = {"ack-nhfb", "ack-jd", "ack-bnb", "ack-bs", "ack-hk", "ack-kl", "ack-kr", "ack-pb", "ack-rfb"};

		for(String macro : macros){
			BibTeXParser.addMacro(macro, macro);
		}
	}

	@Test
	public void pullParserJabRef() throws Exception {
		BibTeXDatabase db = parse("/jabref.bib");
		
		assertEquals(10, db.getEntries().size());
		assertEquals(12, db.getObjects().size());
		
		assertEquals(6, countType(db.getEntries().values(), BibTeXEntry.TYPE_ARTICLE));
		assertEquals(1, countType(db.getEntries().values(), BibTeXEntry.TYPE_UNPUBLISHED));
		assertEquals(2, countType(db.getEntries().values(), BibTeXEntry.TYPE_INPROCEEDINGS));
		assertEquals(1, countType(db.getEntries().values(), BibTeXEntry.TYPE_MASTERSTHESIS));
	}
	
	@Test
	public void pullParseJava() throws Exception {
		BibTeXDatabase database = parse("/java.bib");

		List<BibTeXObject> objects = database.getObjects();
		assertEquals(4498, objects.size());

		Map<Key, BibTeXString> strings = database.getStrings();
		assertEquals(467, strings.size());

		Map<Key, BibTeXEntry> entries = database.getEntries();
		assertEquals(4030, entries.size());

		Collection<BibTeXEntry> values = entries.values();
		assertEquals(1890, countType(values, BibTeXEntry.TYPE_ARTICLE));
		assertEquals(1675, countType(values, BibTeXEntry.TYPE_BOOK));
		assertEquals(232, countType(values, BibTeXEntry.TYPE_INPROCEEDINGS));
		assertEquals(8, countType(values, BibTeXEntry.TYPE_MANUAL));
		assertEquals(13, countType(values, BibTeXEntry.TYPE_MASTERSTHESIS));
		assertEquals(62, countType(values, BibTeXEntry.TYPE_MISC));
		assertEquals(6, countType(values, new Key("periodical")));
		assertEquals(1, countType(values, BibTeXEntry.TYPE_PHDTHESIS));
		assertEquals(114, countType(values, BibTeXEntry.TYPE_PROCEEDINGS));
		assertEquals(20, countType(values, BibTeXEntry.TYPE_TECHREPORT));
		assertEquals(9, countType(values, BibTeXEntry.TYPE_UNPUBLISHED));
	}

	@Test
	public void pullParseRecoverErrors() throws Exception {
		ByteArrayOutputStream errStream = new ByteArrayOutputStream();
		parse("/errors.bib", errStream);
			
		String errString = new String(errStream.toByteArray());
		String expected =  "Encountered \" \"@\" \"@ \"\" at line 63, column 1.\n"+
							"Was expecting one of:\n"+
							"    \",\" ...\n"+
							"    \"#\" ...\n"+
							"    \"}\" ...\n"+
							"    \",\" ...\n"+
							"    \nLexical error at line 151, column 10.  Encountered: \":\" (58), after : \"\"\n" +
							"Encountered \" \"{\" \"{ \"\" at line 580, column 13.\n"+
							"Was expecting:\n"+
							"    \"=\" ...\n"+ 
							"    \n";
		assertEquals(expected, errString);
	}
	
	@Test
	public void parseMendeley() throws Exception {
		parse("/mendeley.bib");
	}

	@Test
	public void parseMissingString() throws Exception {
		try{
			parse("/missingstring.bib");
		}catch(ObjectResolutionException e){
			
		}
	}
	
	@Test
	public void parseUnix() throws Exception {
		BibTeXDatabase database = parse("/unix.bib");

		List<BibTeXObject> objects = database.getObjects();
		assertEquals(2632, objects.size());

		Map<Key, BibTeXString> strings = database.getStrings();
		assertEquals(358, strings.size());

		Map<Key, BibTeXEntry> entries = database.getEntries();
		assertEquals(2273, entries.size());

		Collection<BibTeXEntry> values = entries.values();
		assertEquals(645, countType(values, BibTeXEntry.TYPE_ARTICLE));
		assertEquals(1447, countType(values, BibTeXEntry.TYPE_BOOK));
		assertEquals(6, countType(values, BibTeXEntry.TYPE_INCOLLECTION));
		assertEquals(48, countType(values, BibTeXEntry.TYPE_INPROCEEDINGS));
		assertEquals(31, countType(values, BibTeXEntry.TYPE_MANUAL));
		assertEquals(12, countType(values, BibTeXEntry.TYPE_MASTERSTHESIS));
		assertEquals(7, countType(values, BibTeXEntry.TYPE_MISC));
		assertEquals(6, countType(values, new Key("periodical")));
		assertEquals(2, countType(values, BibTeXEntry.TYPE_PHDTHESIS));
		assertEquals(34, countType(values, BibTeXEntry.TYPE_PROCEEDINGS));
		assertEquals(33, countType(values, BibTeXEntry.TYPE_TECHREPORT));
		assertEquals(2, countType(values, BibTeXEntry.TYPE_UNPUBLISHED));
	}

	@Test
	public void parseZotero() throws Exception {
		parse("/zotero.bib");
	}

	static
	private BibTeXDatabase parse(String path) throws IOException, ParseException {
		return parse(path, new ByteArrayOutputStream());
	}
	
	static
	private BibTeXDatabase parse(String path, ByteArrayOutputStream err) throws IOException, ParseException {
		InputStream is = (BibTeXParserTest.class).getResourceAsStream(path);
		BibTeXDatabase database = new BibTeXDatabase();
		
		try {
			Reader reader = new InputStreamReader(is, "US-ASCII");
			 
			BibTeXParser parser = new BibTeXParser();
			parser.initPullParser(reader, err);
			
			try {
				BibTeXObject object = null;
				while((object = parser.next()) != null){
					database.addObject(object);
				}
			} finally {
				reader.close();
			}
		} finally {
			is.close();
		}
		
		return database;
	}

	static
	private int countType(Collection<BibTeXEntry> entries, Key type){
		int count = 0;

		for(BibTeXEntry entry : entries){

			if((entry.getType()).equals(type)){
				count++;
			}
		}

		return count;
	}
}