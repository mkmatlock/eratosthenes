package com.mm.eratos.tests;

import java.io.File;
import java.io.IOException;

import org.jbibtex.ParseException;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class WebHandlerUnitTestCase extends EratosthenesUnitTestCase {

	public void check_download_pdf(AbstractWebHandler handler, EratosUri uri) throws IOException, ParseException, FieldRequiredException{
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		EratosUri pdfUri = handler.pdfUri(entry, uri);
		assertNotNull(pdfUri);
		
		WebClient webClient = WebClient.newInstance();
		
		EratosUri fileUri = EratosUri.parseUri("device://download.pdf"); 
		
		System.out.println("Downloading pdf: " + pdfUri);
		webClient.downloadPdfFile(pdfUri.toString(), fileUri);
		File file = FileManager.getFileManager().getFile(fileUri);
		assertTrue(file.exists());
		file.delete();
	}
	
	public void check_download_pdf(AbstractWebHandler handler, BibTeXEntryModel entry, EratosUri uri, String uriTest) throws IOException, ParseException, FieldRequiredException{
		EratosUri pdfUri = handler.pdfUri(entry, uri);
		
		assertNotNull(pdfUri);
		assertStartsWith(uriTest, pdfUri.toString());
		
		WebClient webClient = WebClient.newInstance();
		
		EratosUri fileUri = EratosUri.parseUri("device://download.pdf"); 
		
		System.out.println("Downloading pdf: " + pdfUri);
		webClient.downloadPdfFile(pdfUri.toString(), fileUri);
		File file = FileManager.getFileManager().getFile(fileUri);
		assertTrue(file.exists());
		file.delete();
	}

	public void check_download_pdf(AbstractWebHandler handler, BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException{
		EratosUri pdfUri = handler.pdfUri(entry, uri);
		
		assertNotNull(pdfUri);
		
		WebClient webClient = WebClient.newInstance();
		
		EratosUri fileUri = EratosUri.parseUri("device://download.pdf"); 
		
		System.out.println("Downloading pdf: " + pdfUri);
		webClient.downloadPdfFile(pdfUri.toString(), fileUri);
		File file = FileManager.getFileManager().getFile(fileUri);
		assertTrue(file.exists());
		file.delete();
	}
}
