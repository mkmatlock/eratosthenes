package com.mm.eratos.tests;

import junit.framework.TestCase;

import org.jmock.Mockery;
import org.jmock.internal.ExpectationBuilder;
import org.jmock.lib.legacy.ClassImposteriser;

import com.mm.eratos.utils.StringUtils;

public class MockTestCase extends TestCase {
	private static int id = 0;
	private static Mockery context = new Mockery();
	
	public void setUp() throws Exception {
		super.setUp();
		context.setImposteriser(ClassImposteriser.INSTANCE);
	}
	
	public void checking(ExpectationBuilder expectations){
		context.checking(expectations);
	}
	
	public <T> T mock(Class<T> type){
		return context.mock(type, "Mock " + type.toString() + ": " + Integer.toString(id++));
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		context.assertIsSatisfied();
	}

	public static void assertStartsWith(String expectedStart, String actual) {
		String actualStart = "";
		if( expectedStart.length() < actual.length() ) {
			actualStart = actual.substring(0, expectedStart.length());
		}else{
			actualStart = actual + StringUtils.whitespace( expectedStart.length() - actual.length() );
		}
		
		assertTrue( String.format("Expected '%s' to be '%s'", actualStart, expectedStart), actualStart.equals(expectedStart));
	}
	
	public static void assertContains(String needle, String haystack) {
		assertTrue( String.format("Expected to find '%s' in '%s', but didn't", needle, haystack), haystack.contains(needle) );
	}
}
