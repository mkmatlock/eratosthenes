package com.mm.eratos.tests;

import java.io.File;
import java.util.logging.ConsoleHandler;

import org.jmock.Expectations;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogManager;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.ACSReferenceStyleTest;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.BibTeXTestReferenceResolver;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.ezproxy.EZProxyAccountManager;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class EratosthenesUnitTestCase extends MockTestCase {
	protected EratosApplication mockApplication;
	protected EZProxyAccountManager mockAccountManager;
	protected EratosSettings mockSettings;
	
	protected BibTeXEntryModelFactory modelFactory;
	protected BibTeXHelper helper;
	protected BibTeXTestReferenceResolver resolver;
	protected EratosLogManager logManager;
	protected CiteKeyFormatter citeKeyFormatter;
	private EratosLogger logger;
	
	protected String acctName = "";
	
	public void setUp() throws Exception {
		super.setUp();
		
		WebClient.checkConnectionState = false;
		mockApplication = mock(EratosApplication.class);
		mockSettings = mock(EratosSettings.class);
		mockAccountManager = mock(EZProxyAccountManager.class);
		citeKeyFormatter = new CiteKeyFormatter(EratosSettings.CITE_KEY_FORMATTER_DEFAULT);
		
		EratosApplication.singleton = mockApplication;

		helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		checking(new Expectations(){{
			allowing(mockApplication).isConnected();
			will(returnValue(true));
			
			allowing(mockApplication).getFilesDir();
			will(returnValue(new File("temp/")));
			
			allowing(mockApplication).getSettingsManager();
			will(returnValue(mockSettings));
			
			allowing(mockSettings).getBibTeXHelper();
			will(returnValue(helper));
			
			allowing(mockSettings).getCiteKeyFormatter();
			will(returnValue(citeKeyFormatter));
			
			allowing(mockSettings).getExportUri();
			will(returnValue(EratosUri.parseUri("device://temp")));
			
			allowing(mockSettings).getStoreLocalLocation();
			will(returnValue(EratosUri.parseUri("bibtex://")));

			allowing(mockSettings).getEZProxyAccount();
			will(returnValue(acctName));
			
			allowing(mockSettings).getDropboxExportUri();
			will(returnValue(EratosUri.parseUri("device://.dropbox")));
			
		}});
		
		logManager = new EratosLogManager(mockApplication, false, new ConsoleHandler());
		logger = logManager.getLogger("Application");
		
		checking(new Expectations(){{
			allowing(mockApplication).getLogManager();
			will(returnValue(logManager));
			
			allowing(mockApplication).getLogger();
			will(returnValue(logger));
			
			allowing(mockApplication).getEZProxyAccountManager();
			will(returnValue(mockAccountManager));
		}});
		
		FileManager.getFileManager().setExternalStoragePath(".");
	}
	
	public void setCurrentLibrary(final EratosUri path){
		checking(new Expectations(){{
			allowing(mockApplication).getCurrentLibrary();
			will(returnValue(path));
			allowing(mockApplication).getCurrentLibraryPath();
			will(returnValue(path.parent()));
		}});
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		resolver.clear();
	}
}
