package com.mm.eratos.activity;

import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.List;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.jbibtex.citation.ACSReferenceStyle;
import org.jmock.Expectations;

import android.content.Intent;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.tests.EratosthenesUnitTestCase;

public class TestAddEntryFromISBN extends EratosthenesUnitTestCase {

	public void test_import_from_ISBN() throws Exception {
		final Intent mockIntent = mock(Intent.class);
		
		checking(new Expectations() {
			{
				oneOf(mockIntent).getStringExtra(AddEntryFromISBN.ISBN_VALUE);
				will(returnValue("978-0262025065"));
			}
		});
		
		InputStream is = new AddEntryFromISBN.RetrieveBibtexISBNStream().load(mockIntent);
		byte[] buffer = new byte[4096];
		is.read(buffer);
		
		String expEntry =   "@Book{baldi2001bioinformatics,\n"+
			 				 " author = {Baldi, Pierre},\n"+
			 				 " title = {Bioinformatics : the machine learning approach},\n"+
							 " publisher = {MIT Press},\n"+
							 " year = {2001},\n"+
							 " address = {Cambridge, Mass},\n"+
							 " isbn = {9780262025065}\n"+
							 " }";
		String result = new String(buffer, Charset.forName("UTF8")).trim();
		assertEquals(expEntry, result);
		
		List<BibTeXEntry> parsedBibTeXEntries = new BibTeXHelper(new ACSReferenceStyle(), false).parseBibTeXEntries(new StringReader(result));
		assertEquals("Baldi, Pierre", parsedBibTeXEntries.get(0).getField(new Key("Author")).toUserString());
	}

	
	public void test_import_from_ISBN_2() throws Exception {
		final Intent mockIntent = mock(Intent.class);
		
		checking(new Expectations() {
			{
				oneOf(mockIntent).getStringExtra(AddEntryFromISBN.ISBN_VALUE);
				will(returnValue("0387848576"));
			}
		});
		
		InputStream is = new AddEntryFromISBN.RetrieveBibtexISBNStream().load(mockIntent);
		byte[] buffer = new byte[4096];
		is.read(buffer);
		
		String result = new String(buffer, Charset.forName("UTF8")).trim();
		
		List<BibTeXEntry> parsedBibTeXEntries = helper.parseBibTeXEntries(new StringReader(result));
		assertEquals("Hastie, Trevor", parsedBibTeXEntries.get(0).getField(new Key("Author")).toUserString());
		
		BibTeXEntryModel entryModel = modelFactory.construct(parsedBibTeXEntries.get(0));
		assertEquals("Hastie, T", entryModel.getCreator());
	}
}
