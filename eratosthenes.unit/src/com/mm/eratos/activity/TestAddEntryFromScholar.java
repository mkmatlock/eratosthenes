package com.mm.eratos.activity;

import java.io.InputStream;

import com.mm.eratos.feeds.GoogleScholarHandler;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestAddEntryFromScholar extends EratosthenesUnitTestCase {
	public void test_add_entry_from_google_scholar() throws Exception{
		final String ds = "http://scholar.google.com/scholar.bib?q=info:LqRHxAUtp1cJ:scholar.google.com/&output=citation&scisig=AAGBfm0AAAAAUe1_FzDD4oznRUaIGnXnPRbUCGHTFXdt&scisf=4&hl=en&as_sdt=0,26";
		final EratosUri uri = EratosUri.parseUri(ds);
		
		InputStream is = new GoogleScholarHandler().bibtex(uri);
		String bibtex = WebUtils.readStream(is);
		
		String expected = "@article{cock2009biopython, "+
							"title={Biopython: freely available Python tools for computational molecular biology and bioinformatics}, "+
							"author={Cock, Peter JA and Antao, Tiago and Chang, Jeffrey T and Chapman, Brad A and Cox, Cymon J and Dalke, Andrew and Friedberg, Iddo and Hamelryck, Thomas and Kauff, Frank and Wilczynski, Bartek and others}, "+
							"journal={Bioinformatics}, "+
							"volume={25}, "+
							"number={11}, "+
							"pages={1422--1423}, "+
							"year={2009}, "+
							"publisher={Oxford Univ Press} "+
							"}";
		assertEquals(expected, bibtex.trim());
	}
}
