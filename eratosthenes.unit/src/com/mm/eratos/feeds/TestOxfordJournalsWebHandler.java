package com.mm.eratos.feeds;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestOxfordJournalsWebHandler extends WebHandlerUnitTestCase {
	
	private OxfordJournalsWebHandler testObject;

	public void setUp() throws Exception {
		super.setUp();
		testObject = new OxfordJournalsWebHandler();
	}
	
	public void test_bib_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://m.bib.oxfordjournals.org/content/early/2012/12/11/bib.bbs074.short");
		
		String bibtex = WebUtils.readStream( testObject.bibtex(uri) );
		String expBibtexStart = "@article{";
		System.out.println(bibtex);
		
		assertStartsWith(expBibtexStart, bibtex);
		assertContains("10.1093/bib/bbs074", bibtex);
		assertContains("Oxford University Press", bibtex);
		assertContains("Architecture for interoperable software in biology", bibtex);
	}
	
	public void test_bib_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://m.bib.oxfordjournals.org/content/early/2012/12/11/bib.bbs074.short");
		assertTrue( testObject.handles(uri) );
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expectedStart = "Understanding biological complexity demands a combination of high-throughput data and interdisciplinary skills. One way to bring to bear the necessary";
		assertStartsWith(expectedStart, entry.getSummary());
	}
	
	public void test_nar_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://nar.oxfordjournals.org/content/42/W1/W39.html");
		check_download_pdf(testObject, uri);
	}
	
	public void test_nar_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://nar.oxfordjournals.org/content/42/W1/W39.html");
		assertTrue(testObject.handles(uri));
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		String expBibtex = "@article{";
		
		assertStartsWith(expBibtex, bibtex);
		assertContains("Yamanishi", bibtex);
		assertContains("DINIES: drug-target interaction network", bibtex);
		
		String expSummary="DINIES (drug–target interaction network inference engine based on ";

		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		assertStartsWith(expSummary, entry.getSummary());
	}
	
	public void test_pdf_2() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://bioinformatics.oxfordjournals.org/content/27/20/2903");
		check_download_pdf(testObject, uri);
	}
	
	public void test_abstract_2() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://bioinformatics.oxfordjournals.org/content/27/20/2903");
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expSummary = "Summary: Next generation sequencing (NGS) technologies have enabled de novo gene fusion discovery that could reveal candidates with therapeutic significance in cancer. Here we present an open-source software package, ChimeraScan, for the discovery of chimeric transcription between two independent transcripts in high-throughput transcriptome sequencing data. Availability: http://chimerascan.googlecode.com Contact: cmaher{at}dom.wustl.edu Supplementary Information: Supplementary data are available at Bioinformatics online.";
		assertEquals(expSummary, entry.getSummary());
	}
	
	public void test_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://bioinformatics.oxfordjournals.org/content/29/4/497.full");
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		String expBibtex = "@article{Zaretzki_2013,";
		
		assertStartsWith(expBibtex, bibtex);
	}
	
	public void test_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://m.bioinformatics.oxfordjournals.org/content/29/4/497.full");
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expSummary = "Summary: Regioselectivity-WebPredictor (RS-WebPredictor) is a server that predicts isozyme-specific cytochrome P450 (CYP)-mediated sites of metabolism (SOMs) on drug-like molecules. Predictions may be made for the promiscuous 2C9, 2D6 and 3A4 CYP isozymes, as well as CYPs 1A2, 2A6, 2B6, 2C8, 2C19 and 2E1. RS-WebPredictor is the first freely accessible server that predicts the regioselectivity of the last six isozymes. Server execution time is fast, taking on average 2s to encode a submitted molecule and 1s to apply a given model, allowing for high-throughput use in lead optimization projects. Availability: RS-WebPredictor is accessible for free use at http://reccr.chem.rpi.edu/Software/RS-WebPredictor/ Contact: brenec{at}rpi.edu";
		assertEquals(expSummary, entry.getSummary());
	}
	
	public void test_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.bioinformatics.oxfordjournals.org/content/29/4/497.full");
		check_download_pdf(testObject, uri);
	}
}