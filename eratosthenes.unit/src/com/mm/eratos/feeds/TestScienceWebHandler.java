package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestScienceWebHandler extends WebHandlerUnitTestCase {
	private ScienceWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new ScienceWebHandler();
	}
	
	public void test_subjournal_handling() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://stm.sciencemag.org/content/scitransmed/3/111/111ra121.full.html");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);
		String expBibtex = "@article{Roychowdhury_2011,";
		
		assertStartsWith(expBibtex, bibtex);
		
		uri = testObject.trimUri(uri);
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String summary = entry.getSummary();
		String expSummary = "Individual cancers harbor a set of genetic aberrations that can be informative for identifying rational therapies currently available or in clinical trials";
		assertStartsWith(expSummary, summary);
	}
	
	public void test_handles() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencemag.org/content/342/6164/1367");
		assertTrue(testObject.handles(uri));
	}
	
	public void test_fetch_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencemag.org/content/342/6164/1367.full");
		InputStream is = testObject.bibtex(uri);
		
		String bibtex = WebUtils.readStream(is);
		String expBibtex = "@article{Stergachis_2013,";
		
		assertStartsWith(expBibtex, bibtex);
	}
	
	public void test_fetch_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencemag.org/content/342/6164/1367");
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		
		String summary = entry.getSummary();
		String expected = "Genomes contain both a genetic code specifying amino acids and a regulatory code specifying transcription factor (TF)";
	
		assertStartsWith(expected, summary);
	}
	
	public void test_fetch_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencemag.org/content/342/6164/1367");
		check_download_pdf(testObject, uri);
	}
}
