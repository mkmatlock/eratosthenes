package com.mm.eratos.feeds;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestPMCWebHandler extends WebHandlerUnitTestCase {
	private PMCWebHandler testObject;

	public void setUp() throws Exception {
		super.setUp();
		
		testObject = new PMCWebHandler();
	}
	
	public void test_bibtex_article_2() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3326544/");

		assertTrue(testObject.handles(uri));
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		
		System.out.println(bibtex);
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		System.out.println(entry.getSummary());
		check_download_pdf(testObject, uri);
	}
	
	public void test_abstract_at_pdf_link() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/pmid/24497191/pdf");
		
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		String expSummary = "Most genomes, including yeast Saccharomyces cerevisiae, are pervasively transcribed producing numerous non-coding RNAs, many of which are unstable and eliminated by nuclear or cytoplasmic surveillance pathways. We previously showed that accumulation of PHO84 antisense RNA (asRNA), in cells lacking the nuclear exosome component Rrp6, is paralleled by repression of sense transcription in a process dependent on the Hda1 histone deacetylase (HDAC) and the H3K4 histone methyl transferase Set1. Here we investigate this process genome-wide and measure the whole transcriptome of various histone modification mutants in a Δrrp6 strain using tiling arrays. We confirm widespread occurrence of potentially antisense-dependent gene regulation and identify three functionally distinct classes of genes that accumulate asRNAs in the absence of Rrp6. These classes differ in whether the genes are silenced by the asRNA and whether the silencing is HDACs and histone methyl transferase-dependent. Among the distinguishing features of asRNAs with regulatory potential, we identify weak early termination by Nrd1/Nab3/Sen1, extension of the asRNA into the open reading frame promoter and dependence of the silencing capacity on Set1 and the HDACs Hda1 and Rpd3 particularly at promoters undergoing extensive chromatin remodelling. Finally, depending on the efficiency of Nrd1/Nab3/Sen1 early termination, asRNA levels are modulated and their capability of silencing is changed.";
		
		assertEquals(expSummary, entry.getSummary());
	}
	
	public void test_bibtex_free_article_at_pdf_link() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/pmid/24497191/pdf");

		assertTrue(testObject.handles(uri));
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		
		String expBibtex = "% 24497191 \n"+
					   "@Article{pmid24497191,\n"+
					   "   Author=\"Castelnuovo, M.  and Zaugg, J. B.  and Guffanti, E.  and Maffioletti, A.  and Camblong, J.  and Xu, Z.  and Clauder-Munster, S.  and Steinmetz, L. M.  and Luscombe, N. M.  and Stutz, F. \",\n"+
					   "   Title=\"{{R}ole of histone modifications and early termination in pervasive transcription and antisense-mediated gene silencing in yeast}\",\n"+
					   "   Journal=\"Nucleic Acids Res.\",\n"+
					   "   Year=\"2014\",\n"+
					   "   Volume=\"42\",\n"+
					   "   Number=\"7\",\n"+
					   "   Pages=\"4348--4362\",\n"+
					   "   Month=\"Apr\"\n" +
					   "}\n";
					   
		assertEquals(expBibtex, bibtex);
	}
	
	public void test_bibtex_free_article() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/PMC509117/");

		assertTrue(testObject.handles(uri));
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		
		String expBibtex =
			"% 9802883 \n"+ 
			"@Article{pmid9802883,\n"+
			"   Author=\"Setchell, K. D.  and Schwarz, M.  and O'Connell, N. C.  and Lund, E. G.  and Davis, D. L.  and Lathe, R.  and Thompson, H. R.  and Weslie Tyson, R.  and Sokol, R. J.  and Russell, D. W. \",\n"+
			"   Title=\"{{I}dentification of a new inborn error in bile acid synthesis: mutation of the oxysterol 7alpha-hydroxylase gene causes severe neonatal liver disease}\",\n"+
			"   Journal=\"J. Clin. Invest.\",\n"+
			"   Year=\"1998\",\n"+
			"   Volume=\"102\",\n"+
			"   Number=\"9\",\n"+
			"   Pages=\"1690--1703\",\n"+
			"   Month=\"Nov\"\n"+
			"}\n";

		assertEquals(expBibtex, bibtex);
	}
	
	public void test_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1220963/");
		
		assertTrue(testObject.handles(uri));
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		String expBibtex = "% 10749660 \n"+ 
								"@Article{pmid10749660,\n"+
								"   Author=\"Honkakoski, P.  and Negishi, M. \",\n"+
								"   Title=\"{{R}egulation of cytochrome {P}450 ({C}{Y}{P}) genes by nuclear receptors}\",\n"+
								"   Journal=\"Biochem. J.\",\n"+
								"   Year=\"2000\",\n"+
								"   Volume=\"347\",\n"+
								"   Number=\"Pt 2\",\n"+
								"   Pages=\"321--337\",\n"+
								"   Month=\"Apr\"\n"+
								"}\n";

		assertEquals(expBibtex, bibtex);
	}
	
	public void test_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1220963/");
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		String expSummary = "Members of the nuclear-receptor superfamily mediate crucial physiological functions by regulating the synthesis of their target genes. Nuclear receptors are usually activated by ligand binding. Cytochrome P450 (CYP) isoforms often catalyse both formation and degradation of these ligands. CYPs also metabolize many exogenous compounds, some of which may act as activators of nuclear receptors and disruptors of endocrine and cellular homoeostasis. This review summarizes recent findings that indicate that major classes of CYP genes are selectively regulated by certain ligand-activated nuclear receptors, thus creating tightly controlled networks.";
		
		assertEquals(expSummary, entry.getSummary());
	}
	
	public void test_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1220963/");
		check_download_pdf(testObject, uri);
	}
}