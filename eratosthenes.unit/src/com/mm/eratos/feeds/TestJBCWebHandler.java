package com.mm.eratos.feeds;

import java.io.InputStream;
import java.io.StringReader;

import com.mm.eratos.bibtex.ACSReferenceStyleTest;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestJBCWebHandler extends WebHandlerUnitTestCase {
	private JBCWebHandler testObject;
	
	public void setUp() throws Exception{
		super.setUp();
		
		testObject = new JBCWebHandler();
	}
	
	public void test_get_pdf() throws Exception{
		String url = "http://www.jbc.org/content/289/7/3869.abstract";
		EratosUri uri = EratosUri.parseUri(url);
		
		check_download_pdf(testObject, uri);
	}
	
	public void test_get_jbs_abstract_3() throws Exception{
		String url = "http://www.jbc.org/content/early/2014/04/01/jbc.M113.528968.short";
		EratosUri uri = EratosUri.parseUri(url);
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expAbs="Defects in mitochondrial genome can cause a wide range of clinical disorders, mainly neuromuscular diseases. Up to now, no efficient therapeutic treatment has been developed against this class of pathologies. Since most of deleterious mitochondrial mutations are heteroplasmic, meaning that wild type and mutated forms of mtDNA coexists in the same cell, the shift in proportion between mutant and wild type molecules could restore mitochondrial functions. Recently, we developed mitochondrial RNA vectors that can be used to address anti-replicative oligoribonucleotides into human mitochondria and thus impact heteroplasmy level in cells bearing a large deletion in mtDNA. Here we show that this strategy can be also applied to point mutations in mtDNA. We demonstrate that specifically designed RNA molecules containing structural determinants for mitochondrial import and 20-nucleotide sequence corresponding to the mutated region of mtDNA, are able to anneal selectively to the mutated mitochondrial genomes. Being imported into mitochondria of living human cells in culture, these RNA induced a decrease of the proportion of mtDNA molecules bearing a pathogenic point mutation in the mtDNA ND5 gene.";
		assertEquals(expAbs, entry.getSummary());
	}
	
	public void test_get_jbs_bibtex_2() throws Exception{
		String url = "http://www.jbc.org/content/early/2014/04/01/jbc.M113.528968.short";
		EratosUri uri = EratosUri.parseUri(url);
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);

		assertStartsWith("@article{Tonin_2014,", bibtex);
		assertContains("Modelling of Antigenomic Therapy of Mitochondrial Diseases", bibtex);
		assertContains("Journal of Biological Chemistry", bibtex);
	}
	
	public void test_get_jbs_bibtex() throws Exception{
		String url = "http://www.jbc.org/content/270/31/18175.abs";
		EratosUri uri = EratosUri.parseUri(url);
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);

		assertStartsWith("@article{Denison_1995,", bibtex);
		assertContains("Xenobiotic-inducible Transcription of Cytochrome P450 Genes", bibtex);
		
		BibTeXHelper bibTeXHelper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		bibTeXHelper.parseBibTeXEntries(new StringReader(bibtex));
	}
	
	public void test_get_jbc_abstract() throws Exception{
		String url = "http://www.jbc.org/content/270/31/18175.full";
		EratosUri uri = EratosUri.parseUri(url);
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expAbs="";
		assertEquals(expAbs, entry.getSummary());
	}
	
	public void test_get_jbc_abstract2() throws Exception{
		String url = "http://www.jbc.org/content/289/7/3869.abstract";
		
		EratosUri uri = EratosUri.parseUri(url);
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expAbs="Exosomes are small vesicles (50–150 nm) of endocytic origin that are released " +
				"by many different cell types. Exosomes in the tumor microenvironment may play a key " +
				"role in facilitating cell-cell communication. Exosomes are reported to predominantly " +
				"contain RNA and proteins. In this study, we investigated whether exosomes from " +
				"pancreatic cancer cells and serum from patients with pancreatic ductal adenocarcinoma " +
				"contain genomic DNA. Our results provide evidence that exosomes contain >10-kb " +
				"fragments of double-stranded genomic DNA. Mutations in KRAS and p53 can be detected " +
				"using genomic DNA from exosomes derived from pancreatic cancer cell lines and serum " +
				"from patients with pancreatic cancer. In addition, using whole genome sequencing, we " +
				"demonstrate that serum exosomes from patients with pancreatic cancer contain genomic " +
				"DNA spanning all chromosomes. These results indicate that serum-derived exosomes can " +
				"be used to determine genomic DNA mutations for cancer prediction, treatment, and " +
				"therapy resistance.";
		assertEquals(expAbs, entry.getSummary());
	}
}
