package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestWileyWebHandler extends WebHandlerUnitTestCase {
	private WileyWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new WileyWebHandler();
	}
	
	public void test_get_wiley_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://onlinelibrary.wiley.com/doi/10.1002/jcph.1/abstract");
		check_download_pdf(testObject, new BibTeXEntryModel(), uri);
	}
	
	public void test_get_wiley_article_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://onlinelibrary.wiley.com/doi/10.1002/jcph.1/abstract");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);

		assertStartsWith("@article{Kharasch_2013,", bibtex);
		assertContains("Role of Cytochrome P4502B6 in Methadone Metabolism and Clearance", bibtex);
	}
	
	public void test_get_wiley_article_abstract() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://onlinelibrary.wiley.com/doi/10.1002/jcph.1/abstract");
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expSummary = "Methadone N-demethylation in vitro is catalyzed by hepatic cytochrome P4502B6 (CYP2B6) and CYP3A4, but clinical disposition is often attributed to CYP3A4. This investigation tested the hypothesis that CYP2B6 is a prominent CYP isoform responsible for clinical methadone N-demethylation and clearance, using the in vivo mechanism-based CYP2B6 inhibitor ticlopidine, given orally for 4 days. A preliminary clinical investigation with the CYP3A4/5 substrate probe alfentanil established that ticlopidine did not inhibit intestinal or hepatic CYP3A4/5. Subjects received intravenous plus oral (deuterium-labeled) racemic methadone before and after ticlopidine. Ticlopidine significantly and stereoselectively (S > R) inhibited methadone N-demethylation, decreasing plasma metabolite/methadone area under the curve ratios and metabolite formation clearances. Ticlopidine also significantly increased the dose-adjusted plasma area under the curve for R- and S-methadone by 20% and 60%, respectively, after both intravenous and oral dosing. CYP2B6 inhibition reduces methadone N-demethylation and clearance, and alters methadone concentrations, demonstrating an important role for CYP2B6 in clinical methadone disposition.";
		assertEquals(expSummary , entry.getSummary());
	}
}
