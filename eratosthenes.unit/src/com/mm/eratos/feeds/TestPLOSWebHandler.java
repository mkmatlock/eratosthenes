package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestPLOSWebHandler extends WebHandlerUnitTestCase {
	private PLOSWebHandler testObject;

	public void setUp() throws Exception {
		super.setUp();
		testObject = new PLOSWebHandler();
	}
	
	public void test_get_plos_medicine_article_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.plosmedicine.org/article/info%3Adoi%2F10.1371%2Fjournal.pmed.1001581");
		check_download_pdf(testObject, uri);
	}
	
	public void test_get_plos_medicine_article_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.plosmedicine.org/article/info%3Adoi%2F10.1371%2Fjournal.pmed.1001581");
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expSummary = "Intimate partner violence (IPV) and termination of pregnancy (TOP) are global health concerns, but their interaction is undetermined. ";
		assertTrue(entry.getSummary().startsWith(expSummary));
	}
	
	public void test_get_plos_medicine_article_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.plosmedicine.org/article/info%3Adoi%2F10.1371%2Fjournal.pmed.1001581");
		assertTrue(testObject.handles(uri));
	
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);
		
		assertStartsWith("@article{Hall_2014,", bibtex);
		assertContains("Associations between Intimate Partner Violence and Termination of Pregnancy", bibtex);
		assertContains("Public Library of Science (PLoS)", bibtex);
	}
	
	public void test_get_plos_compbio_article_bibtex_v2() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ploscompbiol.org/article/info%3Adoi%2F10.1371%2Fjournal.pcbi.1003331");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);

		assertStartsWith("@article{Polles_2013,", bibtex);
		assertContains("Mechanical and Assembly Units of Viral Capsids Identified via Quasi-Rigid Domain Decomposition", bibtex);
		assertContains("PLoS Computational Biology", bibtex);
	}
	
	public void test_get_plos_compbio_article_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.0010049");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);

		assertStartsWith("@article{Scheeff_2005,", bibtex);
		assertContains("Structural Evolution of the Protein Kinase", bibtex);
		assertContains("PLoS Computational Biology", bibtex);
	}
	
	public void test_get_plos_compbio_article_abstract() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.0010049");
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expSummary = "The protein kinase family is large and important, ";
		assertTrue(entry.getSummary().startsWith(expSummary));
	}
}
