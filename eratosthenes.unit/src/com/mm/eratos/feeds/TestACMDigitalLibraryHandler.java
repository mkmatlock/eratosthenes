package com.mm.eratos.feeds;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestACMDigitalLibraryHandler extends WebHandlerUnitTestCase {
	private ACMDigitalLibraryHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new ACMDigitalLibraryHandler();
	}
	
	public void test_fetch_article_bibtex() throws Exception {
		String url = "http://dl.acm.org/citation.cfm?id=1558013.1558080&coll=DL&dl=ACM&CFID=356106961&CFTOKEN=20987545";
		EratosUri uri = EratosUri.parseUri(url);
		assertTrue(testObject.handles(uri));
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		String expBibtex = "@inproceedings{Matlock:2009:ETM:1558013.1558080,\n" + 
							" author = {Matlock, Matt and Sen, Sandip},\n" + 
							" title = {Effective Tag Mechanisms for Evolving Cooperation},\n" + 
							" booktitle = {Proceedings of The 8th International Conference on Autonomous Agents and Multiagent Systems - Volume 1},\n" + 
							" series = {AAMAS '09},\n" + 
							" year = {2009},\n" + 
							" isbn = {978-0-9817381-6-1},\n" + 
							" location = {Budapest, Hungary},\n" + 
							" pages = {489--496},\n" + 
							" numpages = {8},\n" + 
							" url = {http://dl.acm.org/citation.cfm?id=1558013.1558080},\n" + 
							" acmid = {1558080},\n" + 
							" publisher = {International Foundation for Autonomous Agents and Multiagent Systems},\n" + 
							" address = {Richland, SC},\n" + 
							" keywords = {cooperation, evolution, games, learning, tags},\n" + 
							"}\n";
		assertEquals(expBibtex, bibtex);
	}
	
	public void test_fetch_article_abstract() throws Exception {
		String url = "http://dl.acm.org/citation.cfm?id=1558013.1558080&coll=DL&dl=ACM&CFID=356106961&CFTOKEN=20987545";
		EratosUri uri = EratosUri.parseUri(url);
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		String expected = "Certain observable features (tags), shared by a group of similar agents, can be used to signal intentions and can be effectively used to infer unobservable properties. Such inference will enable the formulation of appropriate behaviors for interaction with those agents. Tags have been previously shown to be successful in social dilemma situations such as the prisoner's dilemma, and more recently have been shown to be applicable to other games by augmenting the standard tag mechanisms. We examine these more general tag mechanisms, and explain previously reported results by more thoroughly examining their fundamental designs. We show that these new tag mechanisms, along with some adjustments and augmentations, can be effective in enabling stable, socially optimal, and fair cooperative outcomes to emerge in general sum games. We focus, in particular, on general-sum conflicted games, where socially optimal outcomes do not necessarily yield the best results for individual agents. We argue that the improvements and understanding of these mechanisms expands the usability of tag mechanisms for facilitating coordination in multiagent systems. We argue that they allow agents to effectively reuse knowledge learned form interactions with one agent when interacting with other agents sharing the same features. Certain observable features (tags), shared by a group of similar agents, can be used to signal intentions and can be effectively used to infer unobservable properties. Such inference will enable the formulation of appropriate behaviors for interaction with those agents. Tags have been previously shown to be successful in social dilemma situations such as the prisoner's dilemma, and more recently have been shown to be applicable to other games by augmenting the standard tag mechanisms. We examine these more general tag mechanisms, and explain previously reported results by more thoroughly examining their fundamental designs. We show that these new tag mechanisms, along with some adjustments and augmentations, can be effective in enabling stable, socially optimal, and fair cooperative outcomes to emerge in general sum games. We focus, in particular, on general-sum conflicted games, where socially optimal outcomes do not necessarily yield the best results for individual agents. We argue that the improvements and understanding of these mechanisms expands the usability of tag mechanisms for facilitating coordination in multiagent systems. We argue that they allow agents to effectively reuse knowledge learned form interactions with one agent when interacting with other agents sharing the same features.";
		assertEquals(expected, entry.getSummary());
	}
	
	public void test_fetch_article_pdf() throws Exception {
		String url = "http://dl.acm.org/citation.cfm?id=1558013.1558080&coll=DL&dl=ACM&CFID=356106961&CFTOKEN=20987545";
		EratosUri uri = EratosUri.parseUri(url);

		check_download_pdf(testObject, new BibTeXEntryModel(), uri, "http://dl.acm.org/ft_gateway.cfm?id=1558080&ftid=631723&dwn=1");
	}
}

