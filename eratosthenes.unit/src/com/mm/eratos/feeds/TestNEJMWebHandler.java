package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestNEJMWebHandler extends WebHandlerUnitTestCase {
	private NEJMWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new NEJMWebHandler();
	}
	
	public void test_get_nejm_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nejm.org/doi/full/10.1056/NEJMsa065316#t=abstract");
		check_download_pdf(testObject, uri);
	}
	
	public void test_get_nejm_article_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nejm.org/doi/full/10.1056/NEJMsa065316#t=abstract");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);

		assertStartsWith("@article{Curlin_2007,", bibtex);
		assertContains("Religion, Conscience, and Controversial Clinical Practices", bibtex);
	}
	
	public void test_get_nejm_article_abstract() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://www.nejm.org/doi/full/10.1056/NEJMsa065316#t=abstract");
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expBackground = "Background\nThere is a heated debate about whether health professionals may refuse to provide treatments to which they object on moral grounds. It is important to understand how physicians think about their ethical rights and obligations when such conflicts emerge in clinical practice.";
		String expMethods = "\n\nMethods\nWe conducted a cross-sectional survey of a stratified, random sample of 2000 practicing U.S. physicians from all specialties by mail. The primary criterion variables were physicians' judgments about their ethical rights and obligations when patients request a legal medical procedure to which the physician objects for religious or moral reasons. These procedures included administering terminal sedation in dying patients, providing abortion for failed contraception, and prescribing birth control to adolescents without parental approval.";
		String expResults = "\n\nResults\nA total of 1144 of 1820 physicians (63%) responded to our survey. On the basis of our results, we estimate that most physicians believe that it is ethically permissible for doctors to explain their moral objections to patients (63%). Most also believe that physicians are obligated to present all options (86%) and to refer the patient to another clinician who does not object to the requested procedure (71%). Physicians who were male, those who were religious, and those who had personal objections to morally controversial clinical practices were less likely to report that doctors must disclose information about or refer patients for medical procedures to which the physician objected on moral grounds (multivariate odds ratios, 0.3 to 0.5).";
		String expConclusions = "\n\nConclusions\nMany physicians do not consider themselves obligated to disclose information about or refer patients for legal but morally controversial medical procedures. Patients who want information about and access to such procedures may need to inquire proactively to determine whether their physicians would accommodate such requests.";
		
		assertEquals(expBackground + expMethods + expResults + expConclusions, entry.getSummary());
	}
}
