package com.mm.eratos.feeds;

import java.util.Scanner;

import org.jmock.Expectations;

import android.accounts.Account;
import android.content.ContentResolver;

import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;

public class TestNatureWebHandlerEZProxy extends WebHandlerUnitTestCase {
	protected ContentResolver mockContentResolver;
	private NatureWebHandler testObject;
	String acctName = "mkmatlock at becker.wustl.edu";
	
	public void setUp() throws Exception {
		super.acctName = acctName;
		super.setUp();
		
		testObject = new NatureWebHandler();
		mockContentResolver = mock(ContentResolver.class);
	}
	
	public void test_fetch_nature_pdf_via_ezproxy() throws Exception {
		final EratosUri proxyAddr = EratosUri.parseUri("https://beckerproxy.wustl.edu/login?url=%s");
		final EratosUri proxyLogin = EratosUri.parseUri("https://login.beckerproxy.wustl.edu/login");
		
		final String username = "mkmatlock";
		final Account mockAccount = mock(Account.class);
		
		System.out.println("Enter password:");
		Scanner in = new Scanner(System.in);
		final String pw = in.nextLine().trim();
		in.close();
		
		checking(new Expectations(){{
			allowing(mockAccountManager).getEZProxyAccount(acctName);
			will(returnValue(mockAccount));
			
			oneOf(mockAccountManager).getEZProxyFetchURL(mockAccount);
			will(returnValue(proxyAddr));
			oneOf(mockAccountManager).getEZProxyLoginURL(mockAccount);
			will(returnValue(proxyLogin));
			oneOf(mockAccountManager).getEZProxyUsername(mockAccount);
			will(returnValue(username));
			oneOf(mockAccountManager).getEZProxyPassword(mockAccount);
			will(returnValue(pw));
		}});
		
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nrm/journal/v15/n6/full/nrm3810.html");
		check_download_pdf(testObject, uri);
	}
}
