package com.mm.eratos.feeds;


import java.io.InputStream;

import com.mm.eratos.bibtex.ACSReferenceStyleTest;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestPubMedWebHandler extends WebHandlerUnitTestCase {

	private PubMedWebHandler testObject;
	private BibTeXHelper helper;

	public void setUp() throws Exception {
		super.setUp();
		testObject = new PubMedWebHandler();
		helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
	}
	
	public void test_handles_should_recognize_pubmed_pages() throws Exception {
		assertTrue( testObject.handles( EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pubmed/17087821")) );
		assertFalse( testObject.handles( EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1637120/")) );
		assertFalse( testObject.handles( EratosUri.parseUri("http://www.biomedcentral.com/1471-2105/7/488")) );
	}
	
	public void test_get_page_should_fill_entry_fields() throws Exception {
		BibTeXEntryModel entryModel = new BibTeXEntryModel();
		
		testObject.extras(entryModel, EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pubmed/17087821"));
		
		String expAbstract = 
		"BACKGROUND\n"+
		"\n"+
		"Protein interactions are crucial components of all cellular processes. Recently, high-throughput methods have been developed to obtain a global description of the interactome (the whole network of protein interactions for a given organism). In 2002, the yeast interactome was estimated to contain up to 80,000 potential interactions. This estimate is based on the integration of data sets obtained by various methods (mass spectrometry, two-hybrid methods, genetic studies). High-throughput methods are known, however, to yield a non-negligible rate of false positives, and to miss a fraction of existing interactions. The interactome can be represented as a graph where nodes correspond with proteins and edges with pairwise interactions. In recent years clustering methods have been developed and applied in order to extract relevant modules from such graphs. These algorithms require the specification of parameters that may drastically affect the results. In this paper we present a comparative assessment of four algorithms: Markov Clustering (MCL), Restricted Neighborhood Search Clustering (RNSC), Super Paramagnetic Clustering (SPC), and Molecular Complex Detection (MCODE).\n"+
		"\n"+
		"RESULTS\n"+
		"\n"+
		"A test graph was built on the basis of 220 complexes annotated in the MIPS database. To evaluate the robustness to false positives and false negatives, we derived 41 altered graphs by randomly removing edges from or adding edges to the test graph in various proportions. Each clustering algorithm was applied to these graphs with various parameter settings, and the clusters were compared with the annotated complexes. We analyzed the sensitivity of the algorithms to the parameters and determined their optimal parameter values. We also evaluated their robustness to alterations of the test graph. We then applied the four algorithms to six graphs obtained from high-throughput experiments and compared the resulting clusters with the annotated complexes.\n"+
		"\n"+
		"CONCLUSION\n"+
		"\n"+
		"This analysis shows that MCL is remarkably robust to graph alterations. In the tests of robustness, RNSC is more sensitive to edge deletion but less sensitive to the use of suboptimal parameter values. The other two algorithms are clearly weaker under most conditions. The analysis of high-throughput data supports the superiority of MCL for the extraction of complexes from interaction networks.";

		assertEquals(expAbstract, entryModel.getFieldValue("abstract", false, helper) );
	}
	
	public void test_get_pubmed_by_id_22216753() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pubmed/22216753");
		String bibtex = WebUtils.readStream( testObject.bibtex(uri) );
		System.out.println( bibtex );
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		System.out.println(entry.getSummary());
		check_download_pdf(testObject, entry, uri);
	}

	public void test_get_pubmed_by_id() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://ncbi.nlm.nih.gov/pubmed/17708774");
		InputStream stream = testObject.bibtex(uri);
		byte[] buffer = new byte[4096];
		
		stream.read(buffer);
		
		String expEntry = "% 17708774 \n"
							+ "@Article{pmid17708774,\n"
							+ "   Author=\"Knight, R.  and Maxwell, P.  and Birmingham, A.  and Carnes, J.  and Caporaso, J. G.  and Easton, B. C.  and Eaton, M.  and Hamady, M.  and Lindsay, H.  and Liu, Z.  and Lozupone, C.  and McDonald, D.  and Robeson, M.  and Sammut, R.  and Smit, S.  and Wakefield, M. J.  and Widmann, J.  and Wikman, S.  and Wilson, S.  and Ying, H.  and Huttley, G. A. \",\n"
							+ "   Title=\"{{P}y{C}ogent: a toolkit for making sense from sequence}\",\n"
							+ "   Journal=\"Genome Biol.\",\n"
							+ "   Year=\"2007\",\n"
							+ "   Volume=\"8\",\n"
							+ "   Number=\"8\",\n"
							+ "   Pages=\"R171\"\n"
							+ "}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ";
		assertEquals(expEntry, new String(buffer));
	}
	
	public void test_get_pubmedcentral_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pubmed/10749660/");
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		FileManager.getFileManager().setExternalStoragePath("temp");
		
		check_download_pdf(testObject, entry, uri);
	}
}
