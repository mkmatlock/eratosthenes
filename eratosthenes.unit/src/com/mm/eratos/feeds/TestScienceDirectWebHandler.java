package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestScienceDirectWebHandler extends WebHandlerUnitTestCase {
	private ScienceDirectWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new ScienceDirectWebHandler();
	}
	
	public void test_science_direct_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencedirect.com/science/article/pii/S0041008X12001202");
		assertTrue(testObject.handles(uri));
		
		InputStream in = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(in);
		
		String expBibtex = WebUtils.readStream(testObject.getBibtexByDOI("10.1016/j.taap.2012.03.017"));
		
		assertEquals(expBibtex, bibtex);
	}
	
	public void test_science_direct_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencedirect.com/science/article/pii/S0168165605001513");
		assertTrue(testObject.handles(uri));
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		String expSummary="Successful drug discovery requires accurate decision making in order to advance the best candidates from initial ";
		
		assertStartsWith(expSummary, entry.getSummary());
	}
	
	public void test_science_direct_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.sciencedirect.com/science/article/pii/S0168165605001513");
		assertTrue(testObject.handles(uri));
		
		check_download_pdf(testObject, new BibTeXEntryModel(), uri);
	}
}
