package com.mm.eratos.feeds;

import java.io.InputStream;
import java.io.StringReader;

import com.mm.eratos.bibtex.ACSReferenceStyleTest;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestACSWebHandler extends WebHandlerUnitTestCase {
	private ACSWebHandler testObject;
	
	public void setUp() throws Exception{
		super.setUp();
		
		testObject = new ACSWebHandler();
	}
	
	public void test_get_acs_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://pubs.acs.org/doi/abs/10.1021/ci3005868");
		
		check_download_pdf(testObject, new BibTeXEntryModel(), uri);
	}
	
	public void test_get_acs_abstract_null() throws Exception {
		String url = "http://pubs.acs.org/doi/abs/10.1021/ci900419k";
		EratosUri uri = EratosUri.parseUri(url);

		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		assertEquals("", entry.getSummary());
	}
	
	public void test_get_acs_bibtex() throws Exception{
		String url = "http://pubs.acs.org/doi/pdf/10.1021/ci3005868";
		EratosUri uri = EratosUri.parseUri(url);
		
		InputStream is = testObject.bibtex(uri);
		String bibtex = WebUtils.readStream(is);
		
		assertStartsWith("@article{Fernald_2013,", bibtex);
		assertContains("Using Molecular Features of Xenobiotics to Predict Hepatic Gene Expression Response", bibtex);
		
		BibTeXHelper bibTeXHelper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		bibTeXHelper.parseBibTeXEntries(new StringReader(bibtex));
	}
	
	public void test_get_acs_abstract() throws Exception{
		String url = "http://pubs.acs.org/doi/pdf/10.1021/ci3005868";
		EratosUri uri = EratosUri.parseUri(url);
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		
		String expAbs="Despite recent advances in molecular medicine and rational drug design, " +
				"many drugs still fail because toxic effects arise at the cellular and tissue level. " +
				"In order to better understand these effects, cellular assays can generate " +
				"high-throughput measurements of gene expression changes induced by small " +
				"molecules. However, our understanding of how the chemical features of small " +
				"molecules influence gene expression is very limited. Therefore, we investigated " +
				"the extent to which chemical features of small molecules can reliably be associated " +
				"with significant changes in gene expression. Specifically, we analyzed the gene " +
				"expression response of rat liver cells to 170 different drugs and searched for " +
				"genes whose expression could be related to chemical features alone. Surprisingly, " +
				"we can predict the up-regulation of 87 genes (increased expression of at least 1.5 " +
				"times compared to controls). We show an average cross-validation predictive area " +
				"under the receiver operating characteristic curve (AUROC) of 0.7 or greater for " +
				"each of these 87 genes. We applied our method to an external data set of rat liver " +
				"gene expression response to a novel drug and achieved an AUROC of 0.7. We also " +
				"validated our approach by predicting up-regulation of Cytochrome P450 1A2 (CYP1A2) " +
				"in three drugs known to induce CYP1A2 that were not in our data set. Finally, a " +
				"detailed analysis of the CYP1A2 predictor allowed us to identify which fragments " +
				"made significant contributions to the predictive scores.";
		assertEquals(expAbs, entry.getSummary());
		
	}
}
