package com.mm.eratos.feeds;

import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.List;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.jbibtex.citation.ACSReferenceStyle;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.Validator;
import com.mm.eratos.utils.WebUtils;

public class TestDOIWebHandler extends EratosthenesUnitTestCase {
	
	private DOIWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		
		testObject = new DOIWebHandler();
	}
	
	public void test_parse_DOI_extras() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://dx.doi.org/10.1016/j.steroids.2006.12.006");
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		assertStartsWith("The xenobiotic receptors CAR and PXR constitute two important members of the NR1I nuclear receptor family", entry.getSummary());
	}
	
	public void test_should_recognize_DOI() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://dx.doi.org/10.1021/ci300009z");
		
		String bibtex = WebUtils.readStream(testObject.bibtex(uri));
		assertStartsWith("@article{Zaretzki_2012,", bibtex);
		assertContains("RS-Predictor Models Augmented with SMARTCyp Reactivities: Robust Metabolic Regioselectivity Predictions for Nine CYP Isozymes", bibtex);
		assertContains("American Chemical Society (ACS)", bibtex);
	}
	
	public void test_add_entry_from_ISBN() throws Exception {
		String isbn = "978-0262025065";
		// dx.doi.org/10.1086/420537
		assertTrue(Validator.isISBN(isbn));
		
		String doi = Validator.ISBNtoDOI(isbn);
		EratosUri uri = DOIWebHandler.doiUrl.append(doi);
		
		System.out.println(uri);

		InputStream is = testObject.bibtex(uri);
		byte[] buffer = new byte[4096];
		is.read(buffer);

		String expEntry =   "@book{9780262025065,\n"+
							"  Author = {Pierre Baldi and S��ren Brunak},\n"+
							"  Title = {Bioinformatics: The Machine Learning Approach, Second Edition (Adaptive Computation and Machine Learning)},\n"+
							"  Publisher = {A Bradford Book},\n"+
							"  Year = {2001},\n"+
							"  ISBN = {026202506X},\n"+
							"  URL = {http://www.amazon.com/Bioinformatics-Learning-Approach-Adaptive-Computation/dp/026202506X%3FSubscriptionId%3D0JYN1NVW651KCA56C102%26tag%3Dtechkie-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3D026202506X}\n"+
							"}";
		

		
		String result = new String(buffer, Charset.forName("UTF8")).trim();
		assertEquals(expEntry, result);
		
		List<BibTeXEntry> parsedBibTeXEntries = new BibTeXHelper(new ACSReferenceStyle(), false).parseBibTeXEntries(new StringReader(result));
		assertEquals("Pierre Baldi and S��ren Brunak", parsedBibTeXEntries.get(0).getField(new Key("Author")).toUserString());
	}
	

	public void test_import_from_ISBN_2() throws Exception {
		String isbn = "0387848576";
		assertTrue(Validator.isISBN(isbn));
		
		String doi = Validator.ISBNtoDOI(isbn);
		EratosUri uri = DOIWebHandler.doiUrl.append(doi);
		
		System.out.println(uri);
		
		InputStream is = testObject.bibtex(uri);
		byte[] buffer = new byte[4096];
		is.read(buffer);
		
		String result = new String(buffer, Charset.forName("UTF8")).trim();
		
		BibTeXHelper helper = new BibTeXHelper(new ACSReferenceStyle(), false);
		List<BibTeXEntry> parsedBibTeXEntries = helper.parseBibTeXEntries(new StringReader(result));
		assertEquals("Trevor Hastie and Robert Tibshirani and Jerome Friedman", parsedBibTeXEntries.get(0).getField(new Key("Author")).toUserString());
		
		BibTeXEntryModel entryModel = modelFactory.construct(parsedBibTeXEntries.get(0));
		assertEquals("Trevor Hastie and Robert Tibshirani and Jerome Friedman", entryModel.getCreator());
	}
}

