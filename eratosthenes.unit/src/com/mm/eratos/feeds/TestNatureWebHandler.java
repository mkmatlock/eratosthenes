package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestNatureWebHandler extends WebHandlerUnitTestCase {
	private NatureWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new NatureWebHandler();
	}
	
	public void test_unusual_ris_content() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nbt/journal/v31/n10/full/nbt.2721.html");
		
		assertTrue(testObject.handles(uri));
		InputStream is = testObject.bibtex(uri);
		
		String bibtex = WebUtils.readStream(is).replace("\n", " ");
		String expBibtex = "@article{Altschul_2013, 	doi = {10.1038/nbt.2721}, 	url = {http://dx.doi.org/10.1038/nbt.2721}, 	year = 2013, 	month = {Oct}, 	publisher = {Nature Publishing Group}, 	volume = {31}, 	number = {10}, 	pages = {894-897}, 	author = {Stephen Altschul and Barry Demchak and Richard Durbin and Robert Gentleman and Martin Krzywinski and Heng Li and Anton Nekrutenko and James Robinson and Wayne Rasband and James Taylor and Cole Trapnell}, 	title = {The anatomy of successful computational biology software}, 	journal = {Nat Biotechnol} } ";
		
		assertEquals(expBibtex, bibtex);
		
	}
	
	public void test_fetch_pdf() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nature/journal/v462/n7270/full/nature08506.html");
		check_download_pdf(testObject, uri);
	}
	
	public void test_handles() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nature/journal/v462/n7270/abs/nature08506.html");
		assertTrue(testObject.handles(uri));
		EratosUri uri2 = EratosUri.parseUri("http://www.nature.com/nature/journal/v462/n7270/full/nature08506.html");
		assertTrue(testObject.handles(uri2));
	}
	
	public void test_fetch_bibtex() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nature/journal/v462/n7270/full/nature08506.html");
		InputStream is = testObject.bibtex(uri);
		
		String bibtex = WebUtils.readStream(is).replace("\n", " ");
		String expBibtex = "@article{Keiser_2009,";
		
		assertTrue(bibtex.startsWith(expBibtex));
	}
	
	public void test_fetch_abstract() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nature/journal/v462/n7270/abs/nature08506.html");
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		
		String summary = entry.getSummary();
		String expected = "Although drugs are intended to be selective, at least some bind to several physiological targets, explaining side effects and efficacy. Because many drug–target combinations exist, it would be useful to explore possible interactions computationally. Here we compared 3,665 US Food and Drug Administration (FDA)-approved and investigational drugs against hundreds of targets, defining each target by its ligands. Chemical similarities between drugs and ligand sets predicted thousands of unanticipated associations. Thirty were tested experimentally, including the antagonism of the 1 receptor by the transporter inhibitor Prozac, the inhibition of the 5-hydroxytryptamine (5-HT) transporter by the ion channel drug Vadilex, and antagonism of the histamine H4 receptor by the enzyme inhibitor Rescriptor. Overall, 23 new drug–target associations were confirmed, five of which were potent (<100 nM). The physiological relevance of one, the drug N,N-dimethyltryptamine (DMT) on serotonergic receptors, was confirmed in a knockout mouse. The chemical similarity approach is systematic and comprehensive, and may suggest side-effects and new indications for many drugs.";
	
		assertEquals(expected, summary);
	}
	
	public void test_fetch_bibtex_2() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nrg/journal/v13/n6/full/nrg3208.html");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		
		String bibtex = WebUtils.readStream(is).replace("\n", " ");
		String expBibtex = "@article{Jensen_2012, 	doi = {10.1038/nrg3208}, 	url = {http://dx.doi.org/10.1038/nrg3208}, 	year = 2012, 	month = {May}, 	publisher = {Nature Publishing Group}, 	volume = {13}, 	number = {6}, 	pages = {395-405}, 	author = {Peter B. Jensen and Lars J. Jensen and S\\oren Brunak}, 	title = {Mining electronic health records: towards better research applications and clinical care}, 	journal = {Nat Rev Genet} } ";
		
		assertEquals(expBibtex, bibtex);
	}
	
	public void test_fetch_bibtex_3() throws Exception {
		EratosUri uri = EratosUri.parseUri("http://www.nature.com/nrd/journal/v10/n9/full/nrd3439-c1.html");
		assertTrue(testObject.handles(uri));
		
		InputStream is = testObject.bibtex(uri);
		
		String bibtex = WebUtils.readStream(is).replace("\n", " ");
		String expBibtex = "@article{Prinz_2011, 	doi = {10.1038/nrd3439-c1}, 	url = {http://dx.doi.org/10.1038/nrd3439-c1}, 	year = 2011, 	month = {Aug}, 	publisher = {Nature Publishing Group}, 	volume = {10}, 	number = {9}, 	pages = {712-712}, 	author = {Florian Prinz and Thomas Schlange and Khusru Asadullah}, 	title = {Believe it or not: how much can we rely on published data on potential drug targets?}, 	journal = {Nat Rev Drug Discov} } ";
		
		assertEquals(expBibtex, bibtex);
	}
}
