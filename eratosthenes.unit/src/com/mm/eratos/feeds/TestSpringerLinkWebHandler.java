package com.mm.eratos.feeds;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestSpringerLinkWebHandler extends WebHandlerUnitTestCase {
	private SpringerLinkWebHandler testObject;

	public void setUp() throws Exception{
		super.setUp();
		testObject = new SpringerLinkWebHandler();
	}
	
	public void test_handles() throws Exception {
		assertTrue(testObject.handles(EratosUri.parseUri("http://link.springer.com/article/10.1208/s12248-012-9322-0")));
		assertTrue(testObject.handles(EratosUri.parseUri("http://link.springer.com/article/10.1007/s10822-014-9813-4")));
		assertFalse(testObject.handles(EratosUri.parseUri("http://pubs.acs.org/doi/abs/10.1021/ci2004835")));
	}
	
	public void test_fetch_article_bibtex() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://link.springer.com/article/10.1208/s12248-012-9322-0");
		String bibtex = WebUtils.readStream( testObject.bibtex(uri) );
		
		assertContains("journal = {The {AAPS} Journal}", bibtex);
		assertContains("author = {Tiejun Cheng and Qingliang Li and Zhigang Zhou and Yanli Wang and Stephen H. Bryant}", bibtex);
		assertContains("publisher = {American Association of Pharmaceutical Scientists ({AAPS})}", bibtex);
		assertContains("url = {http://dx.doi.org/10.1208/s12248-012-9322-0}", bibtex);
	}
	
	public void test_fetch_article_abstract() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://link.springer.com/article/10.1007/s10822-014-9813-4");
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		String expAbstract = "3-D ligand conformations are required for most ligand-based drug design methods, such as pharmacophore modeling, shape-based screening, and 3-D QSAR model building. Many studies of conformational search methods have focused on the reproduction of crystal structures (i.e. bioactive conformations); however, for ligand-based modeling the key question is how to generate a ligand alignment that produces the best results for a given query molecule. In this work, we study different conformation generation modes of ConfGen and the impact on virtual screening (Shape Screening and e-Pharmacophore) and QSAR predictions (atom-based and field-based). In addition, we develop a new search method, called common scaffold alignment, that automatically detects the maximum common scaffold between each screening molecule and the query to ensure identical coordinates of the common core, thereby minimizing the noise introduced by analogous parts of the molecules. In general, we find that virtual screening results are relatively insensitive to the conformational search protocol; hence, a conformational search method that generates fewer conformations could be considered “better” because it is more computationally efficient for screening. However, for 3-D QSAR modeling we find that more thorough conformational sampling tends to produce better QSAR predictions. In addition, significant improvements in QSAR predictions are obtained with the common scaffold alignment protocol developed in this work, which focuses conformational sampling on parts of the molecules that are not part of the common scaffold.";
		
		assertEquals(expAbstract, entry.getSummary());
	}
	
	public void test_fetch_article_pdf() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://link.springer.com/article/10.1208/s12248-012-9322-0");
		check_download_pdf(testObject, new BibTeXEntryModel(), uri);
	}
}
