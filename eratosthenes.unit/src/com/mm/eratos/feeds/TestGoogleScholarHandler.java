package com.mm.eratos.feeds;

//import java.io.File;
//import java.util.List;
//
//import com.mm.eratos.feeds.GoogleScholarHandler.ScholarEntry;
//import com.mm.eratos.files.FileManager;
//import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.EratosthenesUnitTestCase;
//import com.mm.eratos.utils.WebClient.DownloadListener;

public class TestGoogleScholarHandler extends EratosthenesUnitTestCase {

	GoogleScholarHandler testObject;
	
	public void setUp() throws Exception {
		super.setUp();
		testObject = new GoogleScholarHandler();
	}
	
	public void test_edit_cookie(){
		String cookie = "PREF=ID=c2554a370ba305fc:TM=1424047896:LM=1424047896:S=XyFyPkOYGA0-AuO7; NID=67=SSSok65X_pn-XJ17ez1OE7gMJUtc3FHjp8uwx3k8rKxWQXBN29IDY7mKTP8B6Qcp76zKnRIYxlvlJzKPtyFKzRzOZMlLl2CPtnZPvefSkBSO3G1YO6jlaA5SdM8VMOhj; GSP=ID=c2554a370ba305fc:A=b3sfAg:CPTS=1424047905:LM=1424047905:S=k8oJyeFC2WzrQQaf;";
		String expcookie = "PREF=ID=c2554a370ba305fc:TM=1424047896:LM=1424047896:S=XyFyPkOYGA0-AuO7; NID=67=SSSok65X_pn-XJ17ez1OE7gMJUtc3FHjp8uwx3k8rKxWQXBN29IDY7mKTP8B6Qcp76zKnRIYxlvlJzKPtyFKzRzOZMlLl2CPtnZPvefSkBSO3G1YO6jlaA5SdM8VMOhj; GSP=ID=c2554a370ba305fc:A=b3sfAg:CPTS=1424047905:LM=1424047905:S=k8oJyeFC2WzrQQaf:CF=4;";
		
		String result = testObject.setImportBibTeXFlag(cookie);
		
		assertEquals(expcookie, result);
	}
	
	public void test_edit_cookie_2(){
		String cookie = "PREF=ID=c2554a370ba305fc:TM=1424047896:LM=1424047896:S=XyFyPkOYGA0-AuO7; NID=67=SSSok65X_pn-XJ17ez1OE7gMJUtc3FHjp8uwx3k8rKxWQXBN29IDY7mKTP8B6Qcp76zKnRIYxlvlJzKPtyFKzRzOZMlLl2CPtnZPvefSkBSO3G1YO6jlaA5SdM8VMOhj; GSP=ID=c2554a370ba305fc:A=b3sfAg:CF=2:CPTS=1424047905:LM=1424047905:S=k8oJyeFC2WzrQQaf;";
		String expcookie = "PREF=ID=c2554a370ba305fc:TM=1424047896:LM=1424047896:S=XyFyPkOYGA0-AuO7; NID=67=SSSok65X_pn-XJ17ez1OE7gMJUtc3FHjp8uwx3k8rKxWQXBN29IDY7mKTP8B6Qcp76zKnRIYxlvlJzKPtyFKzRzOZMlLl2CPtnZPvefSkBSO3G1YO6jlaA5SdM8VMOhj; GSP=ID=c2554a370ba305fc:A=b3sfAg:CF=4:CPTS=1424047905:LM=1424047905:S=k8oJyeFC2WzrQQaf;";
		
		String result = testObject.setImportBibTeXFlag(cookie);
		
		assertEquals(expcookie, result);
	}
	
//	public void test_retrieveBibtex_for_citation_type() throws Exception{
//		String uid = "info:i-UCe01rE_QJ:scholar.google.com";
//		String expected = "";
//		
//		List<ScholarEntry> entries = testObject.getEntries(uid);
//		String result = entries.get(0).retrieveBibtex();
//		
//		assertEquals(expected, result);
//	}
//	
//	public void test_parseSearchUri_should_capture_query(){
//		String badQ = "https://scholar.google.com/scholar.bib?q=info:gUFoQRdgO4kJ:scholar.google.com/&output=citation&hl=en&ct=citation&cd=0";
//		String query = "info:gUFoQRdgO4kJ:scholar.google.com/";
//		
//		assertTrue(testObject.handles(EratosUri.parseUri(badQ)));
//		String result = testObject.parseSearchUri(badQ);
//		
//		assertEquals(query, result);
//	}
//	
//	public void test_retrieveBibtex_for_citation_type_should_retrieve_entries() throws Exception {
//		String uid = "info:94JIw16NefwJ:scholar.google.com";
//		String expected = "@article{matlock2013sharing, " +
//				"title={Sharing chemical relationships does not reveal structures}, " +
//				"author={Matlock, Matthew and Swamidass, S Joshua}, " +
//				"journal={Journal of chemical information and modeling}, " +
//				"volume={54}, " +
//				"number={1}, " +
//				"pages={37--48}, " +
//				"year={2013}, " +
//				"publisher={ACS Publications} }";
//		
//		List<ScholarEntry> entries = testObject.getEntries(uid);
//		String result = entries.get(0).retrieveBibtex();
//		
//		assertEquals(expected, result);
//	}
//	
//	public void test_getWebUrl_should_retrieve_web_url() throws Exception{
//		String uid = "info:gUFoQRdgO4kJ:scholar.google.com";
//		List<ScholarEntry> entries = testObject.getEntries(uid);
//		EratosUri result = entries.get(0).getWebUri();
//		assertEquals("http://pq.oxfordjournals.org/content/53/211/243.short", result.toString());
//	}
//	
//	
//	public void test_retrieveBibtex_should_retrieve_entries() throws Exception{
//		String uid = "info:gUFoQRdgO4kJ:scholar.google.com";
//		String expected = "@article{bostrom2003we, "+
//			  "title={Are we living in a computer simulation?}, "+
//			  "author={Bostrom, Nick}, "+
//			  "journal={The Philosophical Quarterly}, "+
//			  "volume={53}, "+
//			  "number={211}, "+
//			  "pages={243--255}, "+
//			  "year={2003}, "+
//			  "publisher={Oxford University Press} "+
//			"}";
//		
//		List<ScholarEntry> entries = testObject.getEntries(uid);
//		String result = entries.get(0).retrieveBibtex();
//		
//		assertEquals(result, expected);
//	}
//	
//	public void test_retrievePdf_should_retrieve_pdfs() throws Exception{
//		String uid = "info:gUFoQRdgO4kJ:scholar.google.com";
//		
//		List<ScholarEntry> entries = testObject.getEntries(uid);
//		FileManager.getFileManager().setExternalStoragePath("temp");
//		
//		entries.get(0).retrievePdf(EratosUri.parseUri("device://download.pdf"), new DownloadListener() {
//			public void progress(long downloaded, long fileSize) {
//				
//			}
//		});
//		
//		File f = new File("temp/download.pdf");
//		assertTrue(f.exists());
//		f.delete();
//	}
}
