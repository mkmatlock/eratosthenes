package com.mm.eratos.feeds;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestGenomeResearchWebHandler extends WebHandlerUnitTestCase {
	private GenomeResearchWebHandler testObject;


	public void setUp() throws Exception{
		super.setUp();
		
		testObject = new GenomeResearchWebHandler();
	}
	
	
	public void test_retrieve_doi_bibtex() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://genome.cshlp.org/content/22/2/188");
		
		assertTrue(testObject.handles(uri));
		
		String bibtex = WebUtils.readStream( testObject.bibtex(uri) );
		
		assertStartsWith("@article{", bibtex);
		assertContains("Vucic", bibtex);
		assertContains("Translating cancer 'omics' to improved outcomes", bibtex);
	}
	
	public void test_retrieve_abstract() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://genome.cshlp.org/content/22/2/188");
		
		BibTeXEntryModel entry = new BibTeXEntryModel();
		testObject.extras(entry, uri);
		
		assertStartsWith("The genomics era has yielded great advances", entry.getSummary());
	}
	
	public void test_retrieve_pdf() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://genome.cshlp.org/content/22/2/188");
		check_download_pdf(testObject, uri);
	}
}
