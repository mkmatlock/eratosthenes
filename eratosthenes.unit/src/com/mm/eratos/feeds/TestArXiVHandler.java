package com.mm.eratos.feeds;

import java.io.InputStream;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.WebHandlerUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestArXiVHandler extends WebHandlerUnitTestCase {

	private final String arxivnum = "1203.6902";
	
	private ArXiVHandler testObject;
	
	public void setUp() throws Exception {
		super.setUp();
		testObject = new ArXiVHandler();
	}
	
	public void test_handles() {
		assertTrue(testObject.handles(EratosUri.parseUri("http://arxiv.org/abs/1203.06902")));
		assertTrue(testObject.handles(EratosUri.parseUri("http://arxiv.org/ps/1203.06902")));
		assertTrue(testObject.handles(EratosUri.parseUri("http://arxiv.org/pdf/1203.06902")));
		assertFalse(testObject.handles(EratosUri.parseUri("http://arxiv.org/new")));
	}
	
	public void test_fetch_cs_bibtex() throws Exception {
		InputStream bibtexStream = testObject.bibtex(ArXiVHandler.absUrl.format("1509.09292"));
		String bibtex = WebUtils.readStream(bibtexStream);
		String expected = "@misc{1509.09292,\n"+
							"Author = {David Duvenaud and Dougal Maclaurin and Jorge Aguilera-Iparraguirre and Rafael Gómez-Bombarelli and Timothy Hirzel and Alán Aspuru-Guzik and Ryan P. Adams},\n"+
							"Title = {Convolutional Networks on Graphs for Learning Molecular Fingerprints},\n"+
							"Year = {2015},\n"+
							"Eprint = {arXiv:1509.09292},\n"+
							"}\n";
		assertEquals(expected, bibtex);
		
	}
	
	public void test_fetch_cs_abstract() throws Exception {
		EratosUri uri = ArXiVHandler.absUrl.format("1509.09292");
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		
		String begins = "We introduce a convolutional neural network that operates directly on graphs";
		assertEquals(begins, entry.getSummary().substring(0, begins.length()));
	}
	
	public void test_fetch_bibtex() throws Exception{
		InputStream bibtexStream = testObject.bibtex(EratosUri.parseUri("http://arxiv.org/pdf/"+arxivnum));
		String bibtex = WebUtils.readStream(bibtexStream);
		String expected = "@misc{1203.6902,\n"+
						"Author = {Daniel Schoch},\n"+
						"Title = {Gods as Topological Invariants},\n"+
						"Year = {2012},\n"+
						"Eprint = {arXiv:1203.6902},\n"+
						"}\n";
		
		assertEquals(expected, bibtex);
	}
	
	public void test_fetch_abstract() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://arxiv.org/pdf/"+arxivnum);
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		testObject.extras(entry, uri);
		
		String begins = "We show that the number of gods in a universe must equal the Euler characteristics of";
		assertEquals(begins, entry.getSummary().substring(0, begins.length()));
	}
	
	public void test_fetch_pdf() throws Exception{
		EratosUri uri = EratosUri.parseUri("http://arxiv.org/abs/"+arxivnum);
		check_download_pdf(testObject, uri);
	}
	
}
