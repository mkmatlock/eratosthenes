package com.mm.eratos.model;

import org.jmock.Expectations;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.tests.MockTestCase;

public class TestEratosUri extends MockTestCase {
	
	public void test_parent_uris() throws Exception {
		String fileUri = "dropbox:///library.bib";
		EratosUri uri = EratosUri.parseUri(fileUri);
		
		System.out.println(uri.parent().path());
	}
	
	public void test_parse_uris_with_query() throws Exception {
		String uriWithQuery = "https://bitbucket.org/mkmatlock/eratosthenes/issues?status=new&status=open";
		EratosUri uWQ = EratosUri.parseUri(uriWithQuery);
		
		assertEquals("https", uWQ.protocol());
		assertEquals("bitbucket.org/mkmatlock/eratosthenes/issues", uWQ.path());
		assertEquals("status=new&status=open", uWQ.query());
		assertEquals("", uWQ.fragment());
	}
	
	public void test_parse_uris_with_fragment() throws Exception {
		String uriWithFragment = "http://www.nejm.org/doi/full/10.1056/NEJMsa065316#t=abstract";
		EratosUri uWF = EratosUri.parseUri(uriWithFragment);
		
		assertEquals("http", uWF.protocol());
		assertEquals("www.nejm.org/doi/full/10.1056/NEJMsa065316", uWF.path());
		assertEquals("", uWF.query());
		assertEquals("t=abstract", uWF.fragment());
	}
	
	public void test_parse_uris_with_query_and_fragment() throws Exception {
		String uriWithQueryAndFragment = "http://www.nejm.org/doi/full/10.1056/NEJMsa065316?elements=1,2,3&fetchid=asdf354f;OK#t=abstract";
		EratosUri uWQF = EratosUri.parseUri(uriWithQueryAndFragment);
				
		assertEquals("http", uWQF.protocol());
		assertEquals("www.nejm.org/doi/full/10.1056/NEJMsa065316", uWQF.path());
		assertEquals("elements=1,2,3&fetchid=asdf354f;OK", uWQF.query());
		assertEquals("t=abstract", uWQF.fragment());
	}
	
	public void test_resolve_remove_self() throws Exception {
		final EratosSettings mockSettings = mock(EratosSettings.class);
		final EratosApplication mockApplication = mock(EratosApplication.class);
		
		checking(new Expectations() {{
			allowing(mockApplication).getSettingsManager();
			will(returnValue(mockSettings));
			
			allowing(mockSettings).getStoreLocalLocation();
			will(returnValue(EratosUri.parseUri("device:///sdcard/Library/.dropbox")));
		}});
		
		EratosUri uri = EratosUri.parseUri("library://./Papers/element.pdf");
		EratosUri result = uri.resolveUri(mockApplication);
		assertEquals("device", result.protocol());
		assertEquals("/sdcard/Library/.dropbox/Papers/element.pdf", result.path());
	}
	
	
	public void test_resolve_root_relative() throws Exception {
		EratosUri uri1 = EratosUri.parseUri("library://../../Dropbox/Papers/element.pdf");
		EratosUri uri2 = EratosUri.parseUri("library://../Papers/element.pdf");
		
		EratosUri result1 = uri1.resolveToRootFolder("Dropbox", EratosUri.DROPBOX_PROTOCOL);
		EratosUri result2 = uri2.resolveToRootFolder("Dropbox", EratosUri.DROPBOX_PROTOCOL);
		
		
		assertEquals(EratosUri.DROPBOX_PROTOCOL, result1.protocol());
		assertEquals("/Papers/element.pdf", result1.path());
		
		assertEquals("library", result2.protocol());
		assertEquals("../Papers/element.pdf", result2.path());
	}
	
	public void test_parse_uri2() throws Exception {
		EratosUri uri1 = EratosUri.parseUri("http://www.tandfonline.com/doi/abs/10.1080/15265160802478560");
		EratosUri uri2 = EratosUri.parseUri("http://linkinghub.elsevier.com/retrieve/pii/S0169260711000459?showall=true");
		EratosUri uri3 = EratosUri.parseUri("http://string-db.org");
		
		assertEquals("http", uri1.protocol());
		assertEquals("www.tandfonline.com/doi/abs/10.1080/15265160802478560", uri1.path());
		assertEquals("15265160802478560", uri1.name());
		
		assertEquals("http", uri2.protocol());
		assertEquals("linkinghub.elsevier.com/retrieve/pii/S0169260711000459", uri2.path());
		assertEquals("showall=true", uri2.query());
		assertEquals("S0169260711000459", uri2.name());
		
		assertEquals("http", uri3.protocol());
		assertEquals("string-db.org", uri3.path());
		
	}
	public void test_parse_uri() throws Exception {
		EratosUri uri1 = EratosUri.parseUri("protocol://sub.domain.tk/path/to/res.source");
		EratosUri uri2 = EratosUri.parseUri("http://sub.domain.tk/path/to/resource");
		EratosUri uri3 = EratosUri.parseUri("proto://");
		EratosUri uri4 = EratosUri.parseUri("file://file");
		
		assertEquals("protocol", uri1.protocol());
		assertEquals("sub.domain.tk/path/to/res.source", uri1.path());
		assertEquals("res.source", uri1.name());
		assertEquals("source", uri1.extension());
		
		assertEquals("http", uri2.protocol());
		assertEquals("sub.domain.tk/path/to/resource", uri2.path());
		assertEquals("resource", uri2.name());
		assertEquals("", uri2.extension());
		
		assertEquals("proto", uri3.protocol());
		assertEquals("", uri3.path());
		assertEquals("", uri3.name());
		assertEquals("", uri3.extension());
		
		assertEquals("file", uri4.protocol());
		assertEquals("file", uri4.path());
		assertEquals("file", uri4.name());
		assertEquals("", uri4.extension());
	}
	
	public void test_parent_root() throws Exception {
		EratosUri uri = EratosUri.parseUri("dropbox:///Eratosthenes");
		assertEquals("/", uri.parent().path());
		
		uri = EratosUri.parseUri("dropbox:///");
		System.out.println(uri);
		assertEquals("/file.txt", uri.append("file.txt").path());
	}
	
	public void test_append() throws Exception {
		EratosUri uri1 = EratosUri.parseUri("protocol://sub.domain.tk/path/to/res.source");
		EratosUri uri2 = EratosUri.parseUri("protocol://sub.domain.tk/path/to/res.source/");
		String path1 = "/sub/path";
		String path2 = "sub/path";
		
		EratosUri exp = EratosUri.parseUri("protocol://sub.domain.tk/path/to/res.source/sub/path");
		
		assertEquals(exp, uri1.append(path1));
		assertEquals(exp, uri2.append(path1));
		assertEquals(exp, uri1.append(path2));
		assertEquals(exp, uri2.append(path2));
	}
	
	public void test_path_manipulation() throws Exception {
		EratosUri uri = EratosUri.parseUri("protocol://sub.domain.tk/path/to/res.source");
		EratosUri suburi = EratosUri.parseUri("protocol://sub.domain.tk/path");
		EratosUri other = EratosUri.parseUri("http://sub.domain.tk/path/to");
		
		assertTrue(uri.childOf(suburi));
		assertEquals(EratosUri.parseUri("protocol://sub.domain.tk/path/to"), uri.parent());
		assertFalse(uri.childOf(other));
		
		EratosUri child = suburi.append("new.res.source");
		assertEquals(EratosUri.parseUri("protocol://sub.domain.tk/path/new.res.source"), child);
		assertEquals("source", child.extension());
		assertEquals("new.res.source", child.name());
	}
	
	public void test_relativeTo() throws Exception {
		EratosUri uri1 = EratosUri.parseUri("protocol://sub.domain.tk/path/to/res.source");
		EratosUri uri2 = EratosUri.parseUri("protocol://sub.domain.tk/path/another/resource/location");
		EratosUri uri3 = EratosUri.parseUri("http://another.domain.com/path/to/res.source");
		
		assertEquals("../../another/resource/location", uri2.relativeTo(uri1));
		try{
			uri1.relativeTo(uri3);
			fail();
		}catch(IllegalArgumentException ae){
		}
	}
}
