package com.mm.eratos.io.endnote;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.xmlpull.v1.XmlPullParser;

public class XmlReaderWrapper extends IXMLParser {

	private XMLStreamReader streamReader;

	public XmlReaderWrapper() {
		
	}
	
	@Override
	public IXMLParser newInstance() throws IOException {
		return new XmlReaderWrapper();
	}

	@Override
	public void setInput(InputStreamReader inputStreamReader) throws IOException {
		try {
			streamReader = XMLInputFactory.newInstance().createXMLStreamReader(inputStreamReader);
		} catch (XMLStreamException e) {
			throw new IOException(e);
		} catch (FactoryConfigurationError e) {
			throw new IOException(e);
		}
	}

	@Override
	public int next() throws IOException {
		try {
			int event = streamReader.next();
			return translateEvent(event);
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	private int translateEvent(int event) {
		switch(event){
		case XMLStreamReader.START_DOCUMENT:
			return XmlPullParser.START_DOCUMENT;
		case XMLStreamReader.END_DOCUMENT:
			return XmlPullParser.END_DOCUMENT;
		case XMLStreamReader.START_ELEMENT:
			return XmlPullParser.START_TAG;
		case XMLStreamReader.END_ELEMENT:
			return XmlPullParser.END_TAG;
		case XMLStreamReader.CHARACTERS:
			return XmlPullParser.TEXT;
		case XMLStreamReader.SPACE:
			return XmlPullParser.IGNORABLE_WHITESPACE;
		default:
			return -1;
		}
	}

	@Override
	public String getName() {
		return streamReader.getName().getLocalPart();
	}

	@Override
	public String getText() {
		return streamReader.getText();
	}

}
