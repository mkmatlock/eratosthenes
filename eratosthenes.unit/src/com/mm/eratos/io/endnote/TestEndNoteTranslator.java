package com.mm.eratos.io.endnote;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jbibtex.BibTeXEntry;

import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestEndNoteTranslator extends EratosthenesUnitTestCase {

	private EndNoteWriter testWriter;
	private EndNoteParser testParser;

	public void setUp() throws Exception{
		super.setUp();
		IXMLParser.xmlParser = new XmlReaderWrapper();
		testParser = new EndNoteParser();
		testWriter = new EndNoteWriter();
	}
	
	public void test_import_export_endnote_document() throws Exception {
		File file = new File("data/sample.xml");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		while(testParser.next()){
			BibTeXEntry entry = (BibTeXEntry) testParser.get();
			entries.add(entry);
		}
		
		assertEquals(2, entries.size());
		
		File outputFile = new File("temp/output.enl");
		testWriter.init(new FileOutputStream(outputFile), "UTF-8");
		for(BibTeXEntry entry : entries)
			testWriter.write(entry);
		testWriter.writeExtras();
		testWriter.finish();
		
		System.out.println(WebUtils.readStream(new FileInputStream(outputFile)));
		System.out.flush();
		
		assertEquals("", testParser.errors());
		outputFile.delete();
	}
	
	public void test_import_export_endnote_references() throws Exception {
		File file = new File("data/endnote.xml");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		while(testParser.next()){
			BibTeXEntry entry = (BibTeXEntry) testParser.get();
			entries.add(entry);
		}
		
		assertEquals(33, entries.size());
		
		File outputFile = new File("temp/output.enl");
		testWriter.init(new FileOutputStream(outputFile), "UTF-8");
		for(BibTeXEntry entry : entries)
			testWriter.write(entry);
		testWriter.writeExtras();
		testWriter.finish();
		
		System.out.println(WebUtils.readStream(new FileInputStream(outputFile)));
		System.out.flush();
		
		assertEquals("", testParser.errors());
		outputFile.delete();
	}
}
