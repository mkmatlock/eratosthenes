package com.mm.eratos.io.ris;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jbibtex.BibTeXEntry;

import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.WebUtils;

public class TestRISTranslator extends EratosthenesUnitTestCase {

	private RISWriter testWriter;
	private RISParser testParser;

	public void setUp() throws Exception{
		super.setUp();
		testParser = new RISParser();
		testWriter = new RISWriter();
	}
	
	public void test_import_export_ris_document() throws Exception {
		File file = new File("data/sample.ris");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		while(testParser.next()){
			BibTeXEntry entry = (BibTeXEntry) testParser.get();
			
			System.out.println(helper.formatBibTeX(entry));
			
			entries.add(entry);
		}
		
		assertEquals(6, entries.size());
		
		File outputFile = new File("temp/output.ris");
		testWriter.init(new FileOutputStream(outputFile), "UTF-8");
		for(BibTeXEntry entry : entries){
			testWriter.write(entry);
		}
		
		System.out.println(WebUtils.readStream(new FileInputStream(outputFile)));
		
		assertEquals("", testParser.errors());
		outputFile.delete();
	}
}
