package com.mm.eratos.io.bibtex.jabref;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import com.mm.eratos.io.bibtex.jabref.JabRefGroup;
import com.mm.eratos.io.bibtex.jabref.JabRefGroupsTree;
import com.mm.eratos.io.bibtex.jabref.JabRefKeywordGroup;
import com.mm.eratos.io.bibtex.jabref.JabRefSearchGroup;
import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.EOLConvertingInputStreamReader;
import com.mm.eratos.utils.StringUtils;

public class TestJabRefGroupsTree extends EratosthenesUnitTestCase {
	
	public String readStream(Reader testObject) throws Exception {
		StringBuilder sb = new StringBuilder();
		char []cbuf = new char[1024];
		int read = 0;
		while((read = testObject.read(cbuf)) > -1)
			sb.append(cbuf, 0, read);
		return sb.toString();
	}
	
	public void test_parser_keyword_and_search_groups() throws Exception {
		String groups = readStream(new EOLConvertingInputStreamReader(new FileReader(new File("data/search_and_key_groups.txt"))));
		JabRefGroupsTree testObject = new JabRefGroupsTree();
		
		testObject.parse(groups);
		
		JabRefSearchGroup readingList = (JabRefSearchGroup)testObject.getGroup(1, "SearchGroup", "Reading list");
		JabRefSearchGroup climateGroup = (JabRefSearchGroup)testObject.getGroup(1, "SearchGroup", "Climate group");
		JabRefKeywordGroup riskKeyword = (JabRefKeywordGroup)testObject.getGroup(1, "KeywordGroup", "Risk group");
		
		assertEquals("1\\;rs = r\\;0\\;0\\;", readingList.getFilter());
		assertEquals("0\\;Climate\\;0\\;0\\;", climateGroup.getFilter());
		assertEquals("0\\;keywords\\;Risk\\;0\\;0\\;", riskKeyword.getFilter());
	}
	
	public void test_parse_write_jenny_groups() throws Exception{
		String groups = readStream(new EOLConvertingInputStreamReader(new FileReader(new File("data/jennygrptree"))));
		JabRefGroupsTree testObject = new JabRefGroupsTree();
		
		testObject.parse(groups);
		String output = testObject.format();
		
		assertEquals(groups, output);
	}
	
	public void test_wrap_text() throws Exception{
		JabRefGroupsTree testObject = new JabRefGroupsTree();
		String group = "1 ExplicitGroup:Cu Uptake in Bicontinuous Structures side project\\;0\\;Mostafa_2012\\;;";
		
		List<String> res = testObject.formatWidth(group, JabRefGroupsTree.GROUP_TREE_FORMAT_WIDTH);
		
		assertEquals("1 ExplicitGroup:Cu Uptake in Bicontinuous Structures side project\\;0\\;", res.get(0));
		assertEquals("Mostafa_2012\\;;", res.get(1));
	}

	public void test_parse_physics_groups() throws Exception{
		String groups = readStream(new EOLConvertingInputStreamReader(new FileReader(new File("data/physics_groups.txt"))));
		JabRefGroupsTree testObject = new JabRefGroupsTree();
		
		testObject.parse(groups);
		
		List<JabRefGroup> subGroups = testObject.getRoot().getSubGroups();
		assertEquals(8, subGroups.size());
		
		for(JabRefGroup subGroup : subGroups){
			if(subGroup instanceof JabRefSearchGroup){
				JabRefSearchGroup searchGroup = (JabRefSearchGroup)subGroup;
				if(searchGroup.query()!=null)
					System.out.println( searchGroup.query().sql() + " : [ \"" + StringUtils.join("\", \"", searchGroup.query().args() ) + "\" ]");
				else
					System.out.println("Unsupported group: " + searchGroup.print());
			}
			if(subGroup instanceof JabRefKeywordGroup) {
				JabRefKeywordGroup keywordGroup = (JabRefKeywordGroup)subGroup;
				if(keywordGroup.query() != null)
					System.out.println( keywordGroup.query().sql() + " : [ \"" + StringUtils.join("\", \"", keywordGroup.query().args() ) + "\" ]");
				else
					System.out.println("Unsupported group: " + keywordGroup.print());
			}
		}
	}
	
	public void test_parse_italian() throws Exception{
		String groups = readStream(new EOLConvertingInputStreamReader(new FileReader(new File("data/testgroups.txt"))));
		JabRefGroupsTree testObject = new JabRefGroupsTree();
		
		testObject.parse(groups);
		
		assertEquals(21, testObject.getRoot().getSubGroups().size());
	}
	
	public void test_parse_italian2() throws Exception{
		String groups = readStream(new EOLConvertingInputStreamReader(new FileReader(new File("data/testgroups2.txt"))));
		JabRefGroupsTree testObject = new JabRefGroupsTree();
		
		testObject.parse(groups);
		assertEquals(21, testObject.getRoot().getSubGroups().size());
	}
}
