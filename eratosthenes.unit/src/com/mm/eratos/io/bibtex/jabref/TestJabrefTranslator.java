package com.mm.eratos.io.bibtex.jabref;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbibtex.BibTeXComment;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;
import org.jmock.Mockery;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXTestReferenceResolver;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.TestBibTeXContentProviderAdapter;

public class TestJabrefTranslator extends EratosthenesUnitTestCase {
	private JabRefParser testParser;
	private JabRefWriter testWriter;
	private BibTeXEntryModelFactory modelFactory;

	@Override
	public void setUp() throws Exception {
		super.setUp();

		testParser = new JabRefParser();
		testWriter = new JabRefWriter();
	}

	Mockery context = new Mockery();
	private TestBibTeXContentProviderAdapter bibtexAdapter;
	private BibTeXTestReferenceResolver resolver;
	
	private CaseInsensitiveMap<BibTeXEntry> loadFile(String filename, List<BibTeXEntry> entries, List<BibTeXString> strings, List<BibTeXObject> objects) throws Exception {
		File file = new File(filename);
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());

		bibtexAdapter = new TestBibTeXContentProviderAdapter();
		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		while(testParser.next()){
			BibTeXObject object = testParser.get();
			
			if(object instanceof BibTeXEntry){
				BibTeXEntry entry = (BibTeXEntry)object;
				resolver.addEntry(entry);
				bibtexAdapter.insert(modelFactory.construct(entry));
			}else if(object instanceof BibTeXString){
				BibTeXString string = (BibTeXString)object;
				strings.add(string);
				resolver.addString(string);
				bibtexAdapter.insert(helper, string);
			}else{
				objects.add(object);
			}
		}
		
		testParser.extras(bibtexAdapter, modelFactory, new ProgressCallback() {
			public void update(String stage, int progress, int maxProgress) {
			}
			public void finished() {
			}
			public void error(Throwable error) {
			}
		});
		testParser.finish();
		
		for(String key : bibtexAdapter.getAllRefKeys()){
			entries.add(bibtexAdapter.getBibTeXEntry(key));
		}
		
		CaseInsensitiveMap<BibTeXEntry> entryMap = new CaseInsensitiveMap<BibTeXEntry>();
		for(BibTeXEntry entry : entries)
			entryMap.put(entry.getKey().getValue(), entry);
		return entryMap;
	}
	
	private void saveFile(String filename, List<BibTeXEntry> entries, List<BibTeXString> strings, List<BibTeXObject> objects) throws IOException, ParseException {
		File file = new File(filename);
		testWriter.init(new FileOutputStream(file), "UTF-8");
		
		for(BibTeXString str : strings)
			testWriter.write(str);
		for(BibTeXEntry entry : entries)
			testWriter.write(entry);
		for(BibTeXObject object : objects)
			testWriter.write(object);
		
		testWriter.writeExtras();
		testWriter.finish();
	}
	
	public void test_translator_load_bibtex_without_extras() throws Exception {
		String filename = "data/mendeley.bib";

		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		
		loadFile(filename, entries, strings, objects);
	}
	
	public void test_translator_save_reload() throws Exception {
		String tempFile = "temp/test.bib";
		
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		
		CaseInsensitiveMap<BibTeXEntry> entryMap = loadFile("data/jabref.bib", entries, strings, objects);

		assertEquals("onegroup", entryMap.get("sloutsky2012accounting").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("onegroup", entryMap.get("janes2006data").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("onegroup", entryMap.get("matlock2013security").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("twogroup", entryMap.get("matlock2009effective").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("twogroup", entryMap.get("matlock2007effective").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		
		entryMap.get("janes2006data").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("threegroup", Style.BRACED));
		entryMap.get("sloutsky2012accounting").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("fourgroup", Style.BRACED));
		entryMap.get("matlock2007effective").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("fourgroup", Style.BRACED));
		saveFile(tempFile, entries, strings, objects);
		
		entries.clear();
		strings.clear();
		objects.clear();
		
		entryMap = loadFile(tempFile, entries, strings, objects);
		new File(tempFile).delete();
		
		assertEquals("fourgroup", entryMap.get("sloutsky2012accounting").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("threegroup", entryMap.get("janes2006data").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());

		assertEquals("onegroup", entryMap.get("matlock2013security").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("twogroup", entryMap.get("matlock2009effective").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("fourgroup", entryMap.get("matlock2007effective").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
	}
	
	public void test_parses_all_the_entries() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("device://data/string_and_crossref.bib"));
		File file = new File("data/string_and_crossref.bib");

		int i = 0;
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		assertEquals(3048, testParser.total());
		while(testParser.next())
			i++;
		
		assertEquals(13, i);
		assertEquals("", testParser.errors());
	}

	public void test_translator_eratos() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		
		loadFile("data/eratos.bib", entries, strings, objects);
	}

	public void test_translator_master_save_marked_read() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/master.bib", entries, strings, objects);
		
		entries.get(0).addField(BibTeXEntryModel.MARKED_READ_KEY, new StringValue("true", Style.BRACED));
		entries.get(1).addField(BibTeXEntryModel.MARKED_READ_KEY, new StringValue("true", Style.BRACED));
		entries.get(2).addField(BibTeXEntryModel.MARKED_READ_KEY, new StringValue("true", Style.BRACED));
		entries.get(3).addField(BibTeXEntryModel.MARKED_READ_KEY, new StringValue("false", Style.BRACED));
		
		List<BibTeXEntry> exported = writeAndRetrieve(false, entries);
		
		assertEquals("true", exported.get(0).getField(BibTeXEntryModel.MARKED_READ_KEY).toUserString());
		assertEquals("true", exported.get(1).getField(BibTeXEntryModel.MARKED_READ_KEY).toUserString());
		assertEquals("true", exported.get(2).getField(BibTeXEntryModel.MARKED_READ_KEY).toUserString());
		assertEquals("false", exported.get(3).getField(BibTeXEntryModel.MARKED_READ_KEY).toUserString());
		
		List<BibTeXObject> exportedObjects = writeAndRetrieve(true, objects);
		
		String expBibtex = helper.formatBibTeX(exportedObjects.get(0));
		String bibtex = helper.formatBibTeX(objects.get(0));
		
		assertEquals(expBibtex, bibtex);
	}
	
	public void test_translator_wilson() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/wilson_library.bib", entries, strings, objects);
	}
	
	public void test_translator_chinese() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/chinese.bib", entries, strings, objects);
	}
	
	public void test_translator_import_files_zotero() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/ZoteroOutput.bib", entries, strings, objects);
	}
	
	public void test_translator_import_files_escaped_colons() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		CaseInsensitiveMap<BibTeXEntry> entryMap = loadFile("data/physics.bib", entries, strings, objects);
		
		BibTeXEntry imported = entryMap.get("Alcalde2011PhysicaA:StatisticalMechanicsanditsApplications");
		assertEquals("library://files/articles/Alcalde2011PhysicaA:StatisticalMechanicsanditsApplications.pdf", imported.getField(new Key("attached-file-1")).toUserString());
		assertEquals("Alcalde2011PhysicaA:StatisticalMechanicsanditsApplications.pdf", imported.getField(new Key("file-description-1")).toUserString());
	
		BibTeXEntry exported = writeAndRetrieve(false, Arrays.asList(imported)).get(0);
		assertEquals("Alcalde2011PhysicaA\\:StatisticalMechanicsanditsApplications.pdf:files/articles/Alcalde2011PhysicaA\\:StatisticalMechanicsanditsApplications.pdf:PDF", exported.getField(new Key("file")).toUserString());
	}
	
	public void test_translator_import_physics_groups() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/physics.bib", entries, strings, objects);
		
		Map<String, String> entryGroups = new HashMap<String, String>();
		for(BibTeXEntry entry : entries){
			Value groupsField = entry.getField(BibTeXEntryModel.GROUPS_KEY);
			if(groupsField!=null)
				entryGroups.put(entry.getKey().getValue(), groupsField.toUserString());
		}
		
		String comment1 = helper.formatBibTeX(objects.get(5));
		assertEquals("@Comment{jabref-meta: groupsversion:3;}", comment1);
		
		String comment2 = helper.formatBibTeX(objects.get(6));
		assertTrue("Unexpected start: " + comment2.substring(0,35), comment2.startsWith("@Comment{jabref-meta: groupstree:"));
		
		assertEquals("Rev Mod Phys", entryGroups.get("Gammaitoni_1998_ReviewsofModernPhysics"));
		assertEquals("Rev Mod Phys", entryGroups.get("Lee_1985_ReviewsofModernPhysics"));
	}
	
	public void test_translator_import_groups() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/Biblio.NotOk.bib", entries, strings, objects);
		
		Map<String, String> entryGroups = new HashMap<String, String>();
		for(BibTeXEntry entry : entries){
			Value groupsField = entry.getField(BibTeXEntryModel.GROUPS_KEY);
			if(groupsField!=null) {
				entryGroups.put(entry.getKey().getValue(), groupsField.toUserString());
			}
		}
		
		String comment1 = helper.formatBibTeX(objects.get(5));
		assertEquals("@Comment{jabref-meta: groupsversion:3;}", comment1);
		
		String comment2 = helper.formatBibTeX(objects.get(6));
		assertTrue("Unexpected start: " + comment2.substring(0,35), comment2.startsWith("@Comment{jabref-meta: groupstree:"));
		
		assertTrue(entryGroups.containsKey("Agarwal2005"));
		assertEquals("Ventilazione assistita e CHF", entryGroups.get("Agarwal2005"));
		
		assertTrue(entryGroups.containsKey("Little2009"));
		assertEquals("Ventricular-Vascular coupling", entryGroups.get("Little2009"));
	}
	
	public void test_translator_import_export() throws Exception {
		File file = new File("data/jabref.bib");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		while(testParser.next()) {
			if(testParser.raw() instanceof BibTeXEntry){
				BibTeXEntry entry = (BibTeXEntry)testParser.raw();
				BibTeXEntry eratosEntry = (BibTeXEntry)testParser.get();
				String original = helper.formatBibTeX(entry);
				
				String imported = helper.formatBibTeX(eratosEntry);
				
				if(entry.getField(BibTeXEntryModel.TIMESTAMP_KEY) != null){
					String jabrefTimestamp = entry.getField(BibTeXEntryModel.TIMESTAMP_KEY).toUserString();
					String eratosTimestamp = eratosEntry.getField(BibTeXEntryModel.CREATED_KEY).toUserString();
					assertTrue(eratosTimestamp.startsWith(jabrefTimestamp.replace(".", "-")));
				}
				
				if(entry.getField(BibTeXEntryModel.FILE_KEY) != null){
					String fileUri = entry.getField(BibTeXEntryModel.FILE_KEY).toUserString().split(":")[1];
					String parsedFileUri = eratosEntry.getField(new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + "1")).toUserString();
					
					assertEquals("library://" + fileUri.replace("\\\\", "/"), parsedFileUri);
				}
				
				if(original.contains("file")){
					assertTrue( imported.contains("attached-file-1 = {library://") );
				}
				
				BibTeXEntry bibdeskEntry = writeAndRetrieve(false, Arrays.asList(eratosEntry)).get(0);
				String exported = helper.formatBibTeX(bibdeskEntry);
				
				assertEquals(original.replace("\\\\", "/"), exported);
			}
		}
	}
	
//	jabref-meta: groupstree:
//		0 AllEntriesGroup:;
//		1 ExplicitGroup:onegroup\;0\;janes2006data\;matlock2013security\;slout
//		sky2012accounting\;swamidass2010croc\;;
//		1 ExplicitGroup:twogroup\;0\;matlock2007effective\;matlock2009effectiv
//		e\;;
//		2 ExplicitGroup:subgroup\;0\;matlock2007effective\;;
//		1 ExplicitGroup:threegroup\;0\;zaretzki2011rs\;zaretzki2012rs\;;
//		1 ExplicitGroup:fourgroup\;0\;matlock2011thesis\;;
	
	public void test_translator_import_objects() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		loadFile("data/jabref.bib", entries, strings, objects);
		
		assertEquals(2, objects.size());
		String second = helper.formatBibTeX(objects.get(1));
		assertTrue("Unexpected start: " + second.substring(0,35), second.startsWith("@Comment{jabref-meta: groupstree:"));
		
		Map<String, BibTeXEntry> entryMap = new HashMap<String, BibTeXEntry>();
		for(BibTeXEntry entry : entries)
			entryMap.put(entry.getKey().getValue(), entry);
		
		assertEquals("onegroup", entryMap.get("janes2006data").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("onegroup", entryMap.get("matlock2013security").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("onegroup", entryMap.get("sloutsky2012accounting").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("onegroup", entryMap.get("swamidass2010croc").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		
		assertEquals("twogroup", entryMap.get("matlock2007effective").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("twogroup", entryMap.get("matlock2009effective").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		
		assertEquals("threegroup", entryMap.get("zaretzki2011rs").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("threegroup", entryMap.get("zaretzki2012rs").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		
		assertEquals("fourgroup", entryMap.get("matlock2011thesis").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
	}
	
	public void test_translator_export_objects() throws Exception {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		CaseInsensitiveMap<BibTeXEntry> entryMap = loadFile("data/jabref.bib", entries, strings, objects);

		entryMap.get("janes2006data").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("onegroup", Style.BRACED));
		entryMap.get("matlock2013security").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("onegroup;newgroup", Style.BRACED));
		
		entryMap.get("matlock2007effective").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("onegroup;twogroup", Style.BRACED));
		entryMap.get("matlock2009effective").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("twogroup", Style.BRACED));
		
		entryMap.get("zaretzki2011rs").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("onegroup;threegroup;newgroup", Style.BRACED));
		entryMap.get("zaretzki2012rs").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("twogroup;threegroup", Style.BRACED));
		
		entryMap.get("matlock2011thesis").addField(BibTeXEntryModel.GROUPS_KEY, new StringValue("twogroup;fourgroup;newgroup", Style.BRACED));
		
		testWriter.init(new ByteArrayOutputStream(), "UTF-8");
		for(String refKey : entryMap.keySet())
			testWriter.write(entryMap.get(refKey));
		testWriter.finish();
		List<BibTeXObject> exported = writeAndRetrieve(true, objects);
		
		assertEquals(2, exported.size());
		String comment = helper.formatBibTeX(exported.get(1));
		assertTrue("Unexpected start: " + comment.substring(0,35), comment.startsWith("@Comment{jabref-meta: groupstree:"));
		
		String groupsComment = ((BibTeXComment)exported.get(1)).getValue().toUserString();

		JabRefGroupsTree jabRefGroups = new JabRefGroupsTree();
		jabRefGroups.parse(groupsComment);
		
		assertArrayContents(((JabRefExplicitGroup) jabRefGroups.getGroup(1, "ExplicitGroup", "onegroup")).keyList(), "janes2006data", "matlock2007effective", "matlock2013security", "zaretzki2011rs", "sloutsky2012accounting", "swamidass2010croc");
		assertArrayContents(((JabRefExplicitGroup) jabRefGroups.getGroup(1, "ExplicitGroup", "twogroup")).keyList(), "matlock2007effective", "matlock2009effective", "zaretzki2012rs", "matlock2011thesis");
		assertArrayContents(((JabRefExplicitGroup) jabRefGroups.getGroup(2, "ExplicitGroup", "subgroup")).keyList(), "matlock2007effective");
		assertArrayContents(((JabRefExplicitGroup) jabRefGroups.getGroup(1, "ExplicitGroup", "threegroup")).keyList(), "zaretzki2011rs", "zaretzki2012rs");
		assertArrayContents(((JabRefExplicitGroup) jabRefGroups.getGroup(1, "ExplicitGroup", "fourgroup")).keyList(), "matlock2011thesis");
		assertArrayContents(((JabRefExplicitGroup) jabRefGroups.getGroup(1, "ExplicitGroup", "newgroup")).keyList(), "matlock2013security", "zaretzki2011rs", "matlock2011thesis");
	}

	private void assertArrayContents(List<String> list, String ...items) {
		assertEquals(items.length + " != len( " + StringUtils.join(", ", list) + " )", items.length, list.size());
		for(int i = 0; i < items.length; i++){
			assertTrue(items[i] + " not in [ " + StringUtils.join(", ", list) + " ]", list.contains(items[i]));
		}
	}
	
	@SuppressWarnings("unchecked")
	private <T extends BibTeXObject> List<T> writeAndRetrieve(boolean writeExtras, List<T> objects)
			throws IOException, ParseException {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		testWriter.init(writer, "UTF-8");
		for(BibTeXObject object : objects)
			testWriter.write(object);
		if(writeExtras)
			testWriter.writeExtras();
		testWriter.finish();
		
		String result = new String(writer.toByteArray(), "UTF-8");
		return (List<T>) helper.parseBibTeXObjects(new StringReader(result));
	}
}
