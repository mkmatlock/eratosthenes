package com.mm.eratos.io.bibtex.jabref;

import com.mm.eratos.search.db.QueryElementSearchLike;
import com.mm.eratos.tests.EratosthenesUnitTestCase;

public class TestJabRefSearchGroup extends EratosthenesUnitTestCase {

	public void test_builds_query_with_one_field_equals() throws Exception {
		// 1 SearchGroup:Reading list\;1\;rs = r\;0\;0\;;
		JabRefSearchGroup group = new JabRefSearchGroup(1, "Reading List", 0, "1\\;rs = r\\;0\\;0\\;");
		group.parse();
		assertEquals("field = ? AND value LIKE ?", group.query().sql());
		assertEquals("rs", group.query().args().get(0));
		assertEquals("%r%", group.query().args().get(1));
	}
	
	public void test_builds_query_with_generic_search() throws Exception {
		// 1 SearchGroup:Climate group\;0\;Climate\;0\;0\;;
		JabRefSearchGroup group = new JabRefSearchGroup(1, "Climate group", 0, "0\\;Climate\\;0\\;0\\;");
		group.parse();
		assertEquals("Climate", ((QueryElementSearchLike)group.query()).query());
	}
}
