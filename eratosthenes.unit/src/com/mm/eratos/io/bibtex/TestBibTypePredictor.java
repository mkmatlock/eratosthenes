package com.mm.eratos.io.bibtex;

import java.io.File;

import junit.framework.TestCase;

import com.mm.eratos.io.bibtex.bibdesk.BibDeskParser;
import com.mm.eratos.io.bibtex.jabref.JabRefParser;

public class TestBibTypePredictor extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
	}
	
	public void test_predict_eratosthenes_2() throws Exception {
		File bibtexFile = new File("data/library.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals(EratosthenesParser.ERATOS_FILE_TYPE, actual);
	}
	
	public void test_predict_bibdesk_1() throws Exception {
		File bibtexFile = new File("data/bibdesk.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals(BibDeskParser.BIBDESK_FILE_TYPE, actual);
	}
	
	public void test_predict_bibdesk_2() throws Exception {
		File bibtexFile = new File("data/bibdesk2.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals(BibDeskParser.BIBDESK_FILE_TYPE, actual);
	}
	
	public void test_predict_bibdesk_3() throws Exception {
		File bibtexFile = new File("data/bibdesk3.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals(BibDeskParser.BIBDESK_FILE_TYPE, actual);
	}
	

	public void test_predict_jabref() throws Exception {
		File bibtexFile = new File("data/jabref.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals(JabRefParser.JABREF_FILE_TYPE, actual);
	}
	

	public void test_predict_eratosthenes() throws Exception {
		File bibtexFile = new File("data/all_the_bad_things.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals(EratosthenesParser.ERATOS_FILE_TYPE, actual);
	}
	

	public void test_predict_unknown() throws Exception {
		File bibtexFile = new File("data/longtext.bib");
		String actual = BibTypePredictor.predictBibFileType(bibtexFile, "UTF-8");
		assertEquals("", actual);
	}
}
