package com.mm.eratos.io.bibtex.bibdesk;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbibtex.BibTeXComment;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;
import org.jmock.Mockery;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;
import com.dd.plist.XMLPropertyListParser;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXTestReferenceResolver;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.tests.EratosthenesUnitTestCase;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.TestBibTeXContentProviderAdapter;

public class TestBibdeskTranslator extends EratosthenesUnitTestCase {
	Mockery context = new Mockery();
	private BibDeskParser testParser;
	private BibDeskWriter testWriter;
	private TestBibTeXContentProviderAdapter bibtexAdapter;
	private BibTeXTestReferenceResolver resolver;
	private BibTeXEntryModelFactory modelFactory;
	
	public void setUp() throws Exception {
		super.setUp();
		
		testParser = new BibDeskParser();
		testWriter = new BibDeskWriter();
	}
	
	public void test_parses_all_the_entries() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("device://data/string_and_crossref.bib"));
		File file = new File("data/string_and_crossref.bib");

		int i = 0;
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		assertEquals(3048, testParser.total());
		
		while(testParser.next())
			i++;
		
		assertEquals(13, i);
		assertEquals("", testParser.errors());
		
		testParser.finish();
	}
	
	public void test_translator_import_distinct_local_paths_writes_correct_file_fields() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("dropbox:///Papers/bibdesk4.bib"));
		
		File file = new File("data/bibdesk4.bib");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		while(testParser.next()){
			BibTeXObject object = testParser.get();
			if(object instanceof BibTeXEntry){
				BibTeXEntry entry = (BibTeXEntry) object;
				BibTeXEntryModel entryModel = modelFactory.construct(entry);
				
				assertEquals("Too many items: " + entryModel.getFiles(), 1, entryModel.getFiles().size());
			}
		}	
		testParser.finish();
	}
	
	public void test_translator_export_writes_correct_local_url_fields() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("device://data/bibdesk2.bib"));
		File file = new File("data/bibdesk2.bib");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		List<BibTeXEntry> expEntries = helper.parseBibTeXEntries(new FileReader(new File("data/bibdesk3.bib")));

		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		int i = 0;
		while(testParser.next()){
			BibTeXObject object = testParser.get();
			
			if(object instanceof BibTeXEntry) {
				BibTeXEntry eratosEntry = (BibTeXEntry)object;
				BibTeXEntryModel entryModel = modelFactory.construct(eratosEntry);
				
				if(i == 0){
					entryModel.attachFile(EratosUri.parseUri("library://Sources/Bagley2002a.pdf"));
					eratosEntry = entryModel.createEntry();
					eratosEntry.removeField(BibTeXEntryModel.MODIFIED_KEY);
				} else if(i == 1){
					entryModel.attachFile(EratosUri.parseUri("library://Sources/Carter1992a.pdf"));
					entryModel.attachFile(EratosUri.parseUri("library://Sources/Carter1992b.pdf"));
					eratosEntry = entryModel.createEntry();
					eratosEntry.removeField(BibTeXEntryModel.MODIFIED_KEY);
				}
				
				BibTeXEntry bibdeskEntry = (BibTeXEntry)writeAndRetrieve(eratosEntry);
				BibTeXEntry expectedEntry = expEntries.get(i);
				
				Key lurlKey = new Key("local-url");
				Key lurlKey2 = new Key("local-url-2");
				Key lurlKey3 = new Key("local-url-3");
				
				assertEquals(expectedEntry.getField(lurlKey).toUserString(), bibdeskEntry.getField(lurlKey).toUserString());
				assertEquals(expectedEntry.getField(lurlKey2).toUserString(), bibdeskEntry.getField(lurlKey2).toUserString());
				
				if(expectedEntry.getFields().containsKey(lurlKey3))
					assertEquals(expectedEntry.getField(lurlKey3).toUserString(), bibdeskEntry.getField(lurlKey3).toUserString());
				
				i++;
			}
		}
	}
	
	public void test_translator_import_handles_local_url() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("device://data/bibdesk2.bib"));
		File file = new File("data/bibdesk2.bib");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());

		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		int i = 0;
		while(testParser.next()){
			BibTeXEntry eratosEntry = (BibTeXEntry)testParser.get();
			
			BibTeXEntryModel entryModel = modelFactory.construct(eratosEntry);
			
			assertEquals("Expected array size 1 got: " + StringUtils.join(", ", entryModel.getFiles()), 1, entryModel.getFileCount());
			
			if(i == 0)
				assertEquals("library://Sources/Bagley2002.pdf", entryModel.getFiles().get(0).toString());
			if(i == 1)
				assertEquals("library://Sources/Carter1992.pdf", entryModel.getFiles().get(0).toString());
			i++;
		}
		testParser.finish();
	}
	
	public void test_translator_import_export() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("device://data/bibdesk.bib"));
		File file = new File("data/bibdesk.bib");
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		List<BibTeXEntry> expEntries = helper.parseBibTeXEntries(new FileReader(new File("data/expBibdesk.bib")));
		
		int i = 0;
		while(testParser.next()){
			BibTeXObject object = testParser.raw();
			if(object instanceof BibTeXEntry){
				BibTeXEntry entry = (BibTeXEntry)object;
				String original = helper.formatBibTeX(entry);
				
				Value read = entry.getFields().get(BibTeXEntryModel.MARKED_READ_KEY);
				Value important = entry.getFields().get(BibTeXEntryModel.IMPORTANT_KEY);
				
				BibTeXEntry eratosEntry = (BibTeXEntry) testParser.get();
				String imported = helper.formatBibTeX(eratosEntry);
				
				if(read != null){
					String expValue = read.toUserString();
					if(read.toUserString().equals("1")) expValue = "true";
					if(read.toUserString().equals("0")) expValue = "false";
					
					String value = eratosEntry.getField(BibTeXEntryModel.MARKED_READ_KEY).toUserString();
					assertEquals("Expected " + expValue + " but got " + value + " for entry " + entry.getKey().getValue() + ":" + BibTeXEntryModel.MARKED_READ_KEY.getValue(), expValue, value);
				} if(important != null){
					String expValue = important.toUserString();
					if(important.toUserString().equals("1")) expValue = "true";
					if(important.toUserString().equals("0")) expValue = "false";
					
					String value = eratosEntry.getField(BibTeXEntryModel.IMPORTANT_KEY).toUserString();
					assertEquals("Expected " + expValue + " but got " + value + " for entry " + entry.getKey().getValue() + ":" + BibTeXEntryModel.IMPORTANT_KEY.getValue(), expValue, value);
				}
				
				
				if(original.contains("Bdsk-File-1"))
					assertTrue( (imported.contains("attached-file-1 = {library://") ||
							    imported.contains("attached-file-1 = {dropbox://")) );
				if(original.contains("Bdsk-File-2"))
					assertTrue( (imported.contains("attached-file-2 = {library://") ||
								imported.contains("attached-file-2 = {dropbox://")) );
				if(original.toLowerCase().equals("local-url")){
					String fn = entry.getField(new Key("local-url")).toUserString();
					assertHasFile(fn, eratosEntry);
				}
				if(original.toLowerCase().equals("local-url-2")){
					String fn = entry.getField(new Key("local-url-2")).toUserString();
					assertHasFile(fn, eratosEntry);
				}
	
				BibTeXEntry bibdeskEntry = (BibTeXEntry)writeAndRetrieve(eratosEntry);
				
				if(read != null){
					String value = bibdeskEntry.getField(BibTeXEntryModel.MARKED_READ_KEY).toUserString();
					
					String expValue = read.toUserString();
					if(expValue.equalsIgnoreCase("true")) expValue = "1";
					if(expValue.equalsIgnoreCase("false")) expValue = "0";
					
					assertEquals("Expected " + expValue + " but got " + value + " for entry " + entry.getKey().getValue() + ":" + BibTeXEntryModel.MARKED_READ_KEY.getValue(), expValue, value);
				} if(important != null){
					String value = bibdeskEntry.getField(BibTeXEntryModel.IMPORTANT_KEY).toUserString();
					
					String expValue = important.toUserString();
					if(expValue.equalsIgnoreCase("true")) expValue = "1";
					if(expValue.equalsIgnoreCase("false")) expValue = "0";
					
					assertEquals( "Expected " + expValue + " but got " + value + " for entry " + entry.getKey().getValue() + ":" + BibTeXEntryModel.IMPORTANT_KEY.getValue(), expValue, value);
				}
				
				BibTeXEntry expectedEntry = expEntries.get(i);
				for(Key k : expectedEntry.getFields().keySet()){
					String expectedValue = expectedEntry.getField(k).toUserString();
					String exportedValue = bibdeskEntry.getField(k).toUserString();
					if(!k.getValue().toLowerCase().startsWith("bdsk-file"))
						assertEquals(expectedValue, exportedValue);
				}
				i+=1;
			}
		}
	}
	
	private void assertHasFile(String fn, BibTeXEntry eratosEntry) {
		for(Key k : eratosEntry.getFields().keySet()){
			
			if(k.getValue().startsWith(BibTeXEntryModel.eratosthenesFileFieldPrefix)){
				String fPath = eratosEntry.getField(k).toUserString();
				
				if(fPath.endsWith(fn))
					return;
			}
		}
		fail("File not found: " + fn);
	}

	public void test_import_objects() throws Exception {
		File file = new File("data/bibdesk.bib");
		setCurrentLibrary(EratosUri.parseUri("device://data/bibdesk.bib"));
		
		Map<String, BibTeXEntry> entryMap = parse(file);
		
		assertEquals(2, bibtexAdapter.getAllObjects().size());
		String first = helper.formatBibTeX(bibtexAdapter.getAllObjects().get(0));
		assertTrue(first.startsWith("@Comment{BibDesk Static Groups{"));
		
		Map<String, QueryElement> smartGroups = testParser.getSmartGroups();
		
		for(String gName : smartGroups.keySet()){
			QueryElement query = smartGroups.get(gName);
			System.out.println(gName + " : " + query.sql() + " : " + StringUtils.join(" ", query.args()));
		}
		
		assertEquals("Cytochrome P450", entryMap.get("Meyer2009The-role").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Nelson2006Cytochrome").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Zaretzki2011RS-predictor").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Zarelzki2011Rs-predictor---creation").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Zaretzki2013RS-WebPredictor").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Singh2011Novel").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Rydberg2010The-SMARTCyp").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Kelly2006Cytochrome").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("sun2011predictive").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Cytochrome P450", entryMap.get("Zaretzki2012RS-Predictor").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		
		assertEquals("Privacy: DNA", entryMap.get("Greenbaum2008Genomic").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Privacy: DNA", entryMap.get("Li2012Improvements").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
		assertEquals("Privacy: DNA", entryMap.get("Malin2001Re-identification").getField(BibTeXEntryModel.GROUPS_KEY).toUserString());
	}
	
	private Map<String, BibTeXEntry> parse(File file) throws Exception {
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		bibtexAdapter = new TestBibTeXContentProviderAdapter();
		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		while(testParser.next()){
			BibTeXObject object = testParser.get();
			if(object instanceof BibTeXEntry){
				BibTeXEntry entry = (BibTeXEntry) object;
				bibtexAdapter.insert( modelFactory.commit( modelFactory.construct(entry) ) ); 
			}else if(object instanceof BibTeXString){
				BibTeXString string = (BibTeXString) object;
				bibtexAdapter.insert(helper, string);
			}else{
				bibtexAdapter.insert(helper, object);
			}
		}
		
		
		testParser.extras(bibtexAdapter, modelFactory, new ProgressCallback() {
			public void update(String stage, int progress, int maxProgress) {
			}
			public void finished() {
			}
			public void error(Throwable error) {
			}
		});
		testParser.finish();
		
		Map<String, BibTeXEntry> entryMap = new HashMap<String, BibTeXEntry>();
		for(String k : bibtexAdapter.getAllRefKeys()){
			entryMap.put(k, bibtexAdapter.getBibTeXEntry(k));
		}

		return entryMap;
	}

	public void test_export_objects() throws Exception {
		String groups[] = {"g1;g2;g3", "g2;g3", "g1;g2", "g1;g3"};
		
		File file = new File("data/bibdesk.bib");
		Map<String, BibTeXEntry> entryMap = parse(file);
		
		int i = 0;
		for(String k : bibtexAdapter.getAllRefKeys()){
			BibTeXEntry entry = bibtexAdapter.getBibTeXEntry(k);
			entry.addField(BibTeXEntryModel.GROUPS_KEY, new StringValue(groups[i % groups.length], Style.BRACED));
			i++;
		}
		
		testWriter.init(new ByteArrayOutputStream(), "UTF-8");
		for(String refKey : entryMap.keySet()){
			BibTeXEntry entry = entryMap.get(refKey);
			testWriter.write(entry);
		}
		
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		testWriter.init(writer, "UTF-8");
		for(BibTeXObject object : bibtexAdapter.getAllObjects()){
			testWriter.write(object);
		}
		testWriter.writeExtras();
		testWriter.finish();
		
		String result = writer.toString("UTF-8");
		List<BibTeXObject> exported = helper.parseBibTeXObjects(new StringReader(result));
		
		assertEquals(2, exported.size());
		String comments = helper.formatBibTeX(exported.get(1));
		assertTrue(comments.startsWith("@Comment{BibDesk Static Groups{"));
		
		BibTeXComment comment = (BibTeXComment) exported.get(1);
		String commentValue = comment.getValue().toUserString().replace("BibDesk Static Groups{\n", "").replace("}", "");
		assertGroups(entryMap, commentValue);
	}
	
	private void assertGroups(Map<String, BibTeXEntry> entryMap, String plistXml) throws IOException {
		InputStream is = new ByteArrayInputStream(plistXml.getBytes());
		NSObject plist = null;
		try {
			plist = XMLPropertyListParser.parse(is);
		} catch (Exception e) {
			throw new IOException(e);
		}
		
		for(NSObject obj : ((NSArray)plist).getArray()){
			NSDictionary group = (NSDictionary) obj;
			
			String groupName = ((NSString) group.get("group name")).getContent();
			String keyList = ((NSString) group.get("keys")).getContent();
			
			assertTrue("Group: " + groupName + " not valid!", Arrays.asList(new String[]{"g1","g2","g3"}).contains(groupName));
			String[] keyArray = keyList.split(BibDeskParser.keyDelimiter);
			
			assertEquals(25, keyArray.length);
			for(String key : keyArray){
				assertTrue( "AssertFail: '" + groupName + "' not in '" + entryMap.get(key).getField(BibTeXEntryModel.GROUPS_KEY).toUserString() + "'",  entryMap.get(key).getField(BibTeXEntryModel.GROUPS_KEY).toUserString().contains(groupName) );
			}
		}
	}
	

	private BibTeXObject writeAndRetrieve(BibTeXObject object)
			throws IOException, ParseException {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		testWriter.init(writer, "UTF-8");
		testWriter.write(object);
		testWriter.finish();
		String result = new String(writer.toByteArray(), "UTF-8");
		
		BibTeXObject bibobject = helper.parseBibTeXObject(new StringReader(result));
		return bibobject;
	}
}
