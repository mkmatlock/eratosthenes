package com.mm.eratos.io.bibtex;

import java.io.File;
import java.io.FileInputStream;

import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.EratosthenesUnitTestCase;

public class TestEratosthenesParser extends EratosthenesUnitTestCase {
	private EratosthenesParser testParser;
	
	public void setUp() throws Exception {
		super.setUp();
		
		testParser = new EratosthenesParser();
	}
	
	public void test_parses_all_the_entries() throws Exception {
		setCurrentLibrary(EratosUri.parseUri("device://data/string_and_crossref.bib"));
		File file = new File("data/string_and_crossref.bib");

		int i = 0;
		testParser.init(new FileInputStream(file), "UTF-8", (int)file.length());
		
		assertEquals(3048, testParser.total());
		while(testParser.next())
			i++;
		
		assertEquals(13, i);
		assertEquals("", testParser.errors().trim());
	}
}
