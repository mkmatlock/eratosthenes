package com.mm.eratos.bibtex;

import java.util.HashMap;
import java.util.Map;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;

import com.mm.eratos.bibtex.BibTeXEntryModelFactory.ReferenceResolver;

public class BibTeXTestReferenceResolver extends ReferenceResolver {

	private Map<Key, BibTeXString> strings;
	private Map<Key, BibTeXEntry> entries;
	private BibTeXDatabase db;

	public BibTeXTestReferenceResolver(){
		super();
		strings = new HashMap<Key, BibTeXString>();
		entries = new HashMap<Key, BibTeXEntry>();
	}
	
	public void setDatabase(BibTeXDatabase db) {
		this.db = db;
	}
	
	public void addString(BibTeXString string){
		strings.put(string.getKey(), string);
	}
	
	public void addEntry(BibTeXEntry entry){
		entries.put(entry.getKey(), entry);
	}
	
	@Override
	public BibTeXEntry resolveEntry(Key key) {
		if(db != null){
			BibTeXEntry entry = db.resolveEntry(key);
			if(entry != null)
				return entry;
		}
		return entries.get(key);
	}

	@Override
	public BibTeXString resolveString(Key key) {
		if(db != null){
			BibTeXString string = db.resolveString(key);
			if(string != null)
				return string;
		}
		return strings.get(key);
	}

	public void clear() {
		this.entries.clear();
		this.strings.clear();
		this.db=null;
	}

}
