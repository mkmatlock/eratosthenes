package com.mm.eratos.bibtex;


import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import junit.framework.TestCase;

import org.jbibtex.LaTeXObject;
import org.jbibtex.LaTeXParser;
import org.jbibtex.LaTeXPrinter;
import org.jbibtex.ParseException;

public class TestBibTeXAccentParser extends TestCase {
    public static List<LaTeXObject> parseLaTeX(String string) throws IOException, ParseException {
    	LaTeXParser latexParser = new LaTeXParser();
        Reader reader = new StringReader(string);
        try {
                return latexParser.parse(reader);
        } finally {
                reader.close();
        }
    }

    public static String printLaTeX(List<LaTeXObject> objects){
    	LaTeXPrinter printer = new LaTeXPrinter();
        return printer.print(objects);
    }
	
	public static String fieldToUserString(String latex) throws IOException, ParseException {
		List<LaTeXObject> objects = parseLaTeX(latex);
        return printLaTeX(objects);
	}
	
	public void test_convertLatexSpecialSymbols() throws Exception {
		String inp = "%_\\b";
		
		String res = BibTeXAccentParser.replaceSpecialChars(inp);
		
		assertEquals("\\%\\_\\\\b", res);
	}
	
	public void test_fieldToUserString() throws Exception {
		String inp = "\u03b1\u03b2\u03c2\u03c3\u03c4\u03b3\u03b4\u03c8\u03a0\u03a3\u03a4ı–‘’“”‚„ÀÁÂÃĀĂȦĄȂȀǍÅẢÄẠḀẦẤẪẨẰẬǺǞǠẲẴẮẶḂČĊĈĆƇḆÇḈḊḌḎḐḒĎÈËĖĔĒẼÊÉẺĚȄȆẸȨĘḔỂỄỀḚḘḖỆḜÏḮĶḼĹḺḾǸŇÑṊǐîíỉïìĭīįȉṅňñńņṉṇņṋṉŉõōŏȯöȏȍőỏ";
		String resUL = BibTeXAccentParser.unicodeToLatex(inp);
		
		System.out.println("Intermediate: " + resUL);
		
		String resLU = "";
		try {
			resLU = fieldToUserString(resUL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertEquals( inp, resLU );
	}
	
	public void test_unicodeToLatex() throws Exception {
		String inp = "\u03b1\u03b2\u03c2\u03c3\u03c4\u03b3\u03b4\u03c8\u03a0\u03a3\u03a4ı–‘’“”‚„ÀÁÂÃĀĂȦĄȂȀǍÅẢÄẠḀẦẤẪẨẰẬǺǞǠẲẴẮẶḂČĊĈĆƇḆÇḈḊḌḎḐḒĎÈËĖĔĒẼÊÉẺĚȄȆẸȨĘḔỂỄỀḚḘḖỆḜÏḮĶḼĹḺḾǸŇÑṊǐîíỉïìĭīįȉṅňñńņṉṇņṋṉŉõōŏȯöȏȍőỏ";
		
		String resUL = BibTeXAccentParser.unicodeToLatex(inp);
		
		System.out.println("to");
		System.out.println( resUL );
	}
	
	public void test_nearestAscii() throws Exception {
		String inp = "\u03b1\u03b2\u03c2\u03c3\u03c4\u03b3\u03b4\u03c8\u03a0\u03a3\u03a4ı–‘’“”‚„ÀÁÂÃĀĂȦĄȂȀǍÅẢÄẠḀẦẤẪẨẰẬǺǞǠẲẴẮẶḂČĊĈĆƇḆÇḈḊḌḎḐḒĎÈËĖĔĒẼÊÉẺĚȄȆẸȨĘḔỂỄỀḚḘḖỆḜÏḮĶḼĹḺḾǸŇÑṊǐîíỉïìĭīįȉṅňñńņṉṇņṋṉŉõōŏȯöȏȍőỏ";
		String resNearest = BibTeXAccentParser.nearestAscii(inp);
		
		assertEquals("αβςστγδψΠΣΤı–‘’“”‚„AAAAAAAAAAAAAAAAAAAAAAAAAAAAABCCCCƇBCCDDDDDDEEEEEEEEEEEEEEEEEEEEEEEEIIKLLLMNNNNiiiiiiiiiinnnnnnnnnnŉooooooooo", resNearest);
	}
}
