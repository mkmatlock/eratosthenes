package com.mm.eratos.bibtex;

import org.jbibtex.ParseException;

import junit.framework.TestCase;

public class TestAuthorList extends TestCase {
	private AuthorList testAuthorNames(String expectedFormat, String authorNames, int style) throws ParseException {
		AuthorList testObject = new AuthorList(authorNames, style);
		assertEquals(expectedFormat, testObject.format("; "));
		return testObject;
	}
	
	public void test_author_list_last_first() throws Exception{
		String authorNames="Kortagere, Sandhya and Krasowski, Matthew D and Ekins, Sean";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Kortagere, S; Krasowski, MD; Ekins, S";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}

	public void test_author_list_last_first_initials() throws Exception{
		String authorNames = "Sporzynski, S";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Sporzynski, S";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
	
	public void test_author_list_last_first_MI() throws Exception{
		String authorNames = "Liu, Wai-Hung and Sherman, Andrew H";
		int style = AuthorList.LAST_FIRST_MIDDLE_STYLE;
		String expectedFormat = "Liu, Wai-Hung; Sherman, Andrew H";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
	
	public void test_author_list_last_first_connectives() throws Exception{
		String authorNames = "van Schaik, Ron H and de Meyer, Martine and Wallemacq, Pierre";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "van Schaik, RH; de Meyer, M; Wallemacq, P";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
	
	public void test_author_list_first_then_last_connectives() throws Exception{
		String authorNames = "Lars J Jensen AND Christian von Mering";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Jensen, LJ; von Mering, C";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
	
	public void test_author_list_first_then_last() throws Exception{
		String authorNames = "Fred Q. Bloggs AND John P. Doe and Another Idiot";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Bloggs, FQ; Doe, JP; Idiot, A";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
	
	public void test_author_list_last_first_suffix() throws Exception{
		String authorNames = "Matlock, MS, Matthew K and Swamidass, MD PhD, S Joshua";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Matlock, MK; Swamidass, SJ";
		
		AuthorList authors = testAuthorNames(expectedFormat, authorNames, style);
		assertEquals("MS", authors.get(0).getSuffix());
		assertEquals("MD PhD", authors.get(1).getSuffix());
	}
	
	public void test_author_list_empty() throws Exception{
		String authorNames = "";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "";
		
		AuthorList list = testAuthorNames(expectedFormat, authorNames, style);
		assertEquals(0, list.size());
	}
	
	public void test_author_list_last_name_only() throws Exception{
		String authorNames = "Zaretzki";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Zaretzki";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
	
	public void test_author_list_malformed() throws Exception{
		String authorNames = "Fred Q. Bloggs, John P. Doe & Another Idiot";
		int style = AuthorList.LAST_WITH_INITIALS_STYLE;
		String expectedFormat = "Fred Q. Bloggs, JPD&AI";
		
		testAuthorNames(expectedFormat, authorNames, style);
	}
}
