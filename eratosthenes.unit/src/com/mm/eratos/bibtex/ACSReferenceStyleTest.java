package com.mm.eratos.bibtex;

import java.util.Arrays;
import java.util.List;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.jbibtex.Value;
import org.jbibtex.citation.EntryFormat;
import org.jbibtex.citation.FieldFormat;
import org.jbibtex.citation.ReferenceStyle;

public class ACSReferenceStyleTest extends ReferenceStyle {
	public ACSReferenceStyleTest(){
		super();
		
		addFormat(BibTeXEntry.TYPE_INBOOK, createInBookFormat());
		addFormat(BibTeXEntry.TYPE_MASTERSTHESIS, createThesisFormat());
		addFormat(BibTeXEntry.TYPE_PHDTHESIS, createThesisFormat());
		addFormat(BibTeXEntry.TYPE_CONFERENCE, createInProceedingsFormat());
		addFormat(BibTeXEntry.TYPE_MISC, createMiscFormat());
		addFormat(BibTeXEntry.TYPE_PROCEEDINGS, createProceedingsFormat());
		addFormat(BibTeXEntry.TYPE_TECHREPORT, createTechReportFormat());
		
		addFormat(BibTeXEntry.TYPE_BOOKLET, createManualFormat());
		addFormat(BibTeXEntry.TYPE_MANUAL, createManualFormat());
		
		addFormat(BibTeXEntry.TYPE_ARTICLE, createArticleFormat());
		addFormat(BibTeXEntry.TYPE_BOOK, createBookFormat());
		addFormat(BibTeXEntry.TYPE_INCOLLECTION, createInCollectionFormat());
		addFormat(BibTeXEntry.TYPE_INPROCEEDINGS, createInProceedingsFormat());
		addFormat(BibTeXEntry.TYPE_UNPUBLISHED, createUnpublishedFormat());
	}

	private EntryFormat createManualFormat() {
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new BookTitleFormat("."),
			new FieldFormat(BibTeXEntry.KEY_ORGANIZATION, ","),
			new FieldFormat(BibTeXEntry.KEY_MONTH, ","),
			new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ";"),
			new EditionFormat("."),
			new DOIFormat(null)
		);
		return new EntryFormat(fields);
	}

	private EntryFormat createTechReportFormat() {
		List<FieldFormat> fields = Arrays.asList(
				new AuthorFormat(null),
				new BookTitleFormat("."),
				new FieldFormat(BibTeXEntry.KEY_INSTITUTION, ","),
				new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ".")
			);

			return new EntryFormat(fields);
	}

	private EntryFormat createMiscFormat() {
		List<FieldFormat> fields = Arrays.asList(
				new AuthorFormat(null),
				new FieldFormat(BibTeXEntry.KEY_TITLE, ";"),
				new FieldFormat(BibTeXEntry.KEY_HOWPUBLISHED, ":"),
				new FieldFormat(BibTeXEntry.KEY_MONTH, ","),
				new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ";"),
				new DOIFormat(null)
			);

			return new EntryFormat(fields);
	}
	
	private EntryFormat createProceedingsFormat(){
		List<FieldFormat> fields = Arrays.asList(
				new BookTitleFormat(";"),
				new EditorFormat(";"),
				new FieldFormat(BibTeXEntry.KEY_PUBLISHER, ":"),
				new FieldFormat(BibTeXEntry.KEY_MONTH, ","),
				new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ";"),
				new VolumeFormat(","),
				new FieldFormat(BibTeXEntry.KEY_NUMBER, "."),
				new DOIFormat(null)
			);

		return new EntryFormat(fields);
	}

	private EntryFormat createInBookFormat(){
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new InBookTitleFormat(";"),
			new EditorFormat(";"),
			new FieldFormat(BibTeXEntry.KEY_PUBLISHER, ":"),
			new FieldFormat(BibTeXEntry.KEY_ADDRESS, ","),
			new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ";"),
			new PageFormat("."),
			new DOIFormat(null)
		);

		return new EntryFormat(fields);
	}
	
	private EntryFormat createThesisFormat() {
		List<FieldFormat> fields = Arrays.asList(
				new AuthorFormat(null),
				new BookTitleFormat("."),
				new FieldFormat(BibTeXEntry.KEY_SCHOOL, ","),
				new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ".")
			);

			return new EntryFormat(fields);
	}
	
	private EntryFormat createInProceedingsFormat(){
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new FieldFormat(BibTeXEntry.KEY_TITLE, "."),
			new InCollectionTitleFormat(";"),
			new EditorFormat(";"),
			new FieldFormat(BibTeXEntry.KEY_ORGANIZATION, ";"),
			new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, "."),
			new DOIFormat(null)
		);

		return new EntryFormat(fields);
	}
	
	static private class SearchableFieldFormat extends FieldFormat {
		public SearchableFieldFormat(Key key, String separator){
			super(key, separator);
		}
		
		@Override
		public String format(Value value, boolean latex, boolean html){
			String val = super.format(value, latex, html);
			
			return val;	
		}
	}
	
	static
	private class EditorFormat extends FieldFormat {

		public EditorFormat(String separator){
			super(BibTeXEntry.KEY_EDITOR, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			String []editors = string.split(" and ");
			
			boolean plural = editors.length > 1;
			
			StringBuilder sb = new StringBuilder();
			
			for(String editor : editors){
				sb.append(editor);
				sb.append("; ");
			}
			
			string = (sb.toString() + ", " + (plural ? "Eds." : "Ed."));
			return string;
		}

	}
	
	static
	private class AuthorFormat extends FieldFormat {

		public AuthorFormat(String separator){
			super(BibTeXEntry.KEY_AUTHOR, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			String []authors = string.split(" and ");
			
			StringBuilder sb = new StringBuilder();
			
			for(String author : authors){
				sb.append(author);
				sb.append("; ");
			}

			return sb.toString();
		}

	}
	
	static
	private class BookTitleFormat extends FieldFormat {

		public BookTitleFormat(String separator){
			super(BibTeXEntry.KEY_TITLE, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			return italic(string, html);
		}
	}
	
	static
	private class InBookTitleFormat extends BookTitleFormat {

		public InBookTitleFormat(String separator){
			super(separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			string = ("In " + italic(string, html));

			return string;
		}
	}

	static
	private class InCollectionTitleFormat extends BookTitleFormat {

		public InCollectionTitleFormat(String separator){
			super(separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			string = ("In " + string);

			return string;
		}
	}
	

	static
	private class DOIFormat extends FieldFormat {

		public DOIFormat(String separator){
			super(BibTeXEntry.KEY_DOI, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			if(html){
				string = ("DOI: <a href=\"http://dx.doi.org/" + string + "\">" + string + "</a>");
			} else

			{
				string = ("DOI: " + string);
			}

			return string;
		}
	}

	static
	private class PageFormat extends FieldFormat {

		public PageFormat(String separator){
			super(BibTeXEntry.KEY_PAGES, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			return "pp " + string;
		}
	}


	static
	private class VolumeFormat extends FieldFormat {

		public VolumeFormat(String separator){
			super(BibTeXEntry.KEY_VOLUME, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			return bold(string, html);
		}
	}

	static
	private class EditionFormat extends FieldFormat {

		public EditionFormat(String separator){
			super(BibTeXEntry.KEY_EDITION, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String string = super.format(value, latex, html);

			return "Edition " + string;
		}
	}
	
	static
	private String bold(String string, boolean html){

		if(html){
			string = ("<b>" + string + "</b>");
		}

		return string;
	}

	static
	private String italic(String string, boolean html){

		if(html){
			string = ("<i>" + string + "</i>");
		}

		return string;
	}
	

	static
	private EntryFormat createArticleFormat(){
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new FieldFormat(BibTeXEntry.KEY_TITLE, "."),
			new JournalFormat(null),
			new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, ","),
			new VolumeFormat(","),
			new FieldFormat(BibTeXEntry.KEY_NUMBER, ","),
			new FieldFormat(BibTeXEntry.KEY_PAGES, "."),
			new DOIFormat(null)
		);

		return new EntryFormat(fields);
	}

	static
	private EntryFormat createBookFormat(){
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new BookTitleFormat(";"),
			new EditorFormat(";"),
			new FieldFormat(BibTeXEntry.KEY_PUBLISHER, ":"),
			new FieldFormat(BibTeXEntry.KEY_ADDRESS, ";"),
			new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, "."),
			new DOIFormat(null)
		);

		return new EntryFormat(fields);
	}

	static
	private EntryFormat createInCollectionFormat(){
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new FieldFormat(BibTeXEntry.KEY_TITLE, "."),
			new InBookTitleFormat(";"),
			new EditorFormat(";"),
			new FieldFormat(BibTeXEntry.KEY_PUBLISHER, ";"),
			new SearchableFieldFormat(BibTeXEntry.KEY_YEAR, "."),
			new DOIFormat(null)
		);

		return new EntryFormat(fields);
	}

	static
	private EntryFormat createUnpublishedFormat(){
		List<FieldFormat> fields = Arrays.asList(
			new AuthorFormat(null),
			new FieldFormat(BibTeXEntry.KEY_TITLE, ".")
		);

		return new EntryFormat(fields);
	}

	static
	private class JournalFormat extends FieldFormat {

		public JournalFormat(String separator){
			super(BibTeXEntry.KEY_JOURNAL, separator);
		}

		@Override
		public String format(Value value, boolean latex, boolean html){
			String journal = super.format(value, latex, html);

			return italic(journal, html);
		}
	}
}
