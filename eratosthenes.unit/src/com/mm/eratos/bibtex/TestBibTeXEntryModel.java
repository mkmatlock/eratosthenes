package com.mm.eratos.bibtex;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

import com.mm.eratos.model.EratosUri;

public class TestBibTeXEntryModel extends TestCase {
	BibTeXEntryModel testObject;
	BibTeXHelper helper;
	
	@Override
	public void setUp() throws Exception{
		helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		testObject = new BibTeXEntryModel();
	}
	
	public void test_parseSummaryAndExternalLinks() throws Exception{
		BibTeXEntry entry = new BibTeXEntry(new Key("article"), new Key("test-entry"));
		Map<Key, BibTeXString> strings = new HashMap<Key, BibTeXString>();
		
		entry.addField(BibTeXEntryModel.ABSTRACT_KEY, new StringValue("Abstract contents. www.contains.a.webpage/with/a/path to somewhere else.", Style.BRACED));
		entry.addField(BibTeXEntryModel.COMMENTS_KEY, new StringValue("Notes contents. ftp://a.webpage/with/a/path to somewhere else. https://www.another.page.com", Style.BRACED));
		entry.addField(BibTeXEntryModel.DOI_KEY, new StringValue("DOIVALUE", Style.BRACED));
		entry.addField(BibTeXEntryModel.URL_KEY, new StringValue("http://some.web.res.so/urce", Style.BRACED));
		entry.addField(new Key(BibTeXEntryModel.eratosthenesLinkFieldPrefix + "1"), new StringValue("http://some.other.res.so/urce", Style.BRACED));
		entry.addField(new Key(BibTeXEntryModel.eratosthenesLinkFieldPrefix + "2"), new StringValue("http://some.new.res.so/urce", Style.BRACED));
		
		testObject.parseFields(entry, strings);
		testObject.parseSummary();
		testObject.parseNotes();
		testObject.parseExternalLinks();
		
		for(EratosUri uri : testObject.getExternalLinks()){
			System.out.println(uri.toString());
		}
		
		assertEquals(7, testObject.getExternalLinks().size());
		
		EratosUri uri1 = testObject.getExternalLinks().get(0);
		EratosUri uri2 = testObject.getExternalLinks().get(1);
		EratosUri uri3 = testObject.getExternalLinks().get(2);
		EratosUri uri4 = testObject.getExternalLinks().get(3);
		EratosUri uri5 = testObject.getExternalLinks().get(4);
		EratosUri uri6 = testObject.getExternalLinks().get(5);
		EratosUri uri7 = testObject.getExternalLinks().get(6);
		
		assertEquals("http://dx.doi.org/DOIVALUE", uri1.toString());
		assertEquals("http://some.web.res.so/urce", uri2.toString());
		
		assertEquals("ftp://a.webpage/with/a/path", uri3.toString());
		assertEquals("https://www.another.page.com", uri4.toString());
		assertEquals("http://www.contains.a.webpage/with/a/path", uri5.toString());
		
		assertEquals("http://some.other.res.so/urce", uri6.toString());
		assertEquals("http://some.new.res.so/urce", uri7.toString());
		
		assertTrue( testObject.isParsedLink(uri1) );
		assertTrue( testObject.isParsedLink(uri2) );
		assertTrue( testObject.isParsedLink(uri3) );
		assertTrue( testObject.isParsedLink(uri4) );
		assertTrue( testObject.isParsedLink(uri5) );
		assertFalse( testObject.isParsedLink(uri6) );
		assertFalse( testObject.isParsedLink(uri7) );
	}
}
