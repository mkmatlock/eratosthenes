package com.mm.eratos.bibtex;

import junit.framework.TestCase;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.CiteKeyFormatter;

public class TestCiteKeyFormatter extends TestCase {
	Mockery context = new Mockery();
	
	public void setUp(){
		context.setImposteriser(ClassImposteriser.INSTANCE);
	}
	
	public void test_format_is_lower_case() throws Exception {
		final BibTeXEntryModel mockEntry = context.mock(BibTeXEntryModel.class);
		
		CiteKeyFormatter testObject = new CiteKeyFormatter("%A-%m%y-%T");
		
		context.checking(new Expectations(){{
			allowing(mockEntry).getType();
			will(returnValue("article"));
			
			allowing(mockEntry).getTitle();
			will(returnValue("Opinion: Genomic variants in exons and introns: identifying the splicing spoilers"));
			
			allowing(mockEntry).getCreator();
			will(returnValue("Frånco Pagani and Francisco E. Baralle"));
			
			allowing(mockEntry).getYear();
			will(returnValue("2011"));
			
			allowing(mockEntry).getMonth();
			will(returnValue("May"));
		}});
		
		String citeKey = testObject.format(mockEntry);
		assertEquals("pagani_franco-may2011-opinion_genomic_variants", citeKey);
		
		context.assertIsSatisfied();
	}
	
	public void test_format_removes_quotes() throws Exception {
		final BibTeXEntryModel mockEntry = context.mock(BibTeXEntryModel.class);
		
		CiteKeyFormatter testObject = new CiteKeyFormatter("%A-%m%y-%T");
		
		context.checking(new Expectations(){{
			allowing(mockEntry).getType();
			will(returnValue("article"));
			
			allowing(mockEntry).getTitle();
			will(returnValue("'calling cards': robust blah stuff"));
			
			allowing(mockEntry).getCreator();
			will(returnValue("Frånco Pagani and Francisco E. Baralle"));
			
			allowing(mockEntry).getYear();
			will(returnValue("2011"));
			
			allowing(mockEntry).getMonth();
			will(returnValue("May"));
		}});
		
		String citeKey = testObject.format(mockEntry);
		assertEquals("pagani_franco-may2011-calling_cards_robust", citeKey);
		
		context.assertIsSatisfied();
	}
}
