
package com.mm.eratos.bibtex;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;

import junit.framework.TestCase;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;

import com.mm.eratos.files.NullOutputStream;

public class TestBibTeXHelper extends TestCase {
	
	private BibTeXHelper helper;
	private BibTeXTestReferenceResolver resolver;
	private BibTeXEntryModelFactory modelFactory;

	public void setUp() throws Exception{
		super.setUp();
		helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		resolver = new BibTeXTestReferenceResolver();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		resolver.clear();
	}
	
	public void test_parse_recover_multiple_errors() throws Exception {
		String bibtexFile = "data/errors.bib";
		InputStreamReader in = new InputStreamReader(new FileInputStream(bibtexFile));
		
		BibTeXDatabase db = helper.parseBibTeX(in, new NullOutputStream());
		assertEquals(14, db.getEntries().size());
//		assertEquals(3, parser.getErrors().size());
	}
	
	public void test_add_xref_entries_reverse() throws Exception {
		String entryBib = "@ARTICLE{article-crossref,"+
						   "crossref = {WHOLE-JOURNAL},"+
						   "key = \"\","+
						   "author = {L[eslie] A. Aamport},"+
						   "title = {The Gnats and Gnus Document Preparation System},"+
						   "pages = \"73+\","+
						   "note = \"This is a cross-referencing ARTICLE entry\","+
						"}";
		
		String xrefBib = "@ARTICLE{whole-journal,"+
						   "key = \"GAJ\","+
						   "journal = {\\mbox{G-Animal's} Journal},"+
						   "year = 1986,"+
						   "volume = 41,"+
						   "number = 7,"+
						   "month = jul,"+
						   "organization = ACM,"+
						   "note = {The entire issue is devoted to gnats and gnus (this entry is a cross-referenced ARTICLE (journal))},"+
						"}";
		
		String stBib = "@STRING{ACM = \"The OX Association for Computing Machinery\"}";
		
		BibTeXEntry entry = helper.parseBibTeXEntries(new StringReader(entryBib)).get(0);
		resolver.addEntry( entry );
		resolver.addEntry( helper.parseBibTeXEntries(new StringReader(xrefBib)).get(0) );
		resolver.addString( helper.parseBibTeXString(new StringReader(stBib)) );
		
		BibTeXEntryModel model = modelFactory.construct(entry);		
		assertEquals("The OX Association for Computing Machinery", model.getFieldValue("organization") );
	}
	
	
	
	public void test_parse_file_with_string_entries() throws Exception {
		String bibtexFile = "data/string_and_crossref.bib";
		InputStreamReader in = new InputStreamReader(new FileInputStream(bibtexFile));
		
		BibTeXDatabase db = helper.parseBibTeX(in, new NullOutputStream());
		BibTeXHelper helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);

		resolver.setDatabase(db);
		
		for(Key k : db.getEntries().keySet()){
			BibTeXEntry entry = db.getEntries().get(k);
			BibTeXEntryModel model = modelFactory.construct(entry);
			System.out.println(model.getTextCitation(helper)+"\n");
        }
	}
	
	public void test_parse_huge_file() throws Exception {
		String bibtexFile = "data/6000refs.bib";
		InputStreamReader in = new InputStreamReader(new FileInputStream(bibtexFile));
		
		BibTeXDatabase db = helper.parseBibTeX(in, new NullOutputStream());
		BibTeXHelper helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		resolver.setDatabase(db);
		
		int i = 0;
		for(Key k : db.getEntries().keySet()){
			BibTeXEntry entry = db.getEntries().get(k);
			BibTeXEntryModel model = modelFactory.construct(entry);
			model.getHtmlCitation(helper);
			i++;
        }
		assertTrue(i > 6000);
	}
	
	public void test_parse_italian() throws Exception {
		String bibtexFile = "data/Biblio.bib";
		InputStreamReader in = new InputStreamReader(new FileInputStream(bibtexFile));
		
		BibTeXDatabase db = helper.parseBibTeX(in, new NullOutputStream());
		BibTeXHelper helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		resolver.setDatabase(db);
		
		int i = 0;
		for(Key k : db.getEntries().keySet()){
			BibTeXEntry entry = db.getEntries().get(k);
			BibTeXEntryModel model = modelFactory.construct(entry);
			model.getHtmlCitation(helper);
			i++;
        }
		
		assertEquals(1729, i);
		
	}
	
	public void test_parse_offensive_file() throws Exception {
		String bibtexFile = "data/all_the_bad_things.bib";
		InputStreamReader in = new InputStreamReader(new FileInputStream(bibtexFile));
		
		BibTeXDatabase db = helper.parseBibTeX(in, new NullOutputStream());
		BibTeXHelper helper = new BibTeXHelper(new ACSReferenceStyleTest(), false);
		assertEquals(5, db.getEntries().size());
		resolver.setDatabase(db);
		
		for(Key k : db.getEntries().keySet()){
			BibTeXEntry entry = db.getEntries().get(k);
			BibTeXEntryModel model = modelFactory.construct(entry);
			model.getHtmlCitation(helper);
        }
	}
	
	public void test_parse_offensive_entry_2() throws Exception {
		String bibtex = "@article{TestEntry,\n" +
							"Author = {Ana Rossini and Luis Felipe Ribeiro Pinto},\n"+
							"Journal = {Pharmacogenomics},\n"+
							"Month = {November},\n"+
							"Number = {11},\n"+
							"Pages = {1737-1752},\n"+
							"Title = {CYP2A6 polymorphisms: Inattention to P(H) and to P(\\ ~H): A converging operation},\n"+
							"Volume = {9},\n"+
							"Year = {2008}}";
		
		List<BibTeXEntry> parseBibTeXEntries = helper.parseBibTeXEntries(new StringReader(bibtex));
		
		assertEquals(1, parseBibTeXEntries.size());
		
		BibTeXEntryModel eModel = modelFactory.construct(parseBibTeXEntries.get(0));
		assertEquals("CYP2A6 polymorphisms: Inattention to P(H) and to P(\\ ~H): A converging operation", eModel.getTitle());
	}
	
	public void test_parse_offensive_entry() throws Exception {
		String bibtex = "@article{Ideker2002Discovering,"
								+ "abstract = {},"
								+ "pmid = {12169552}," 
								+ "important = {false},"
								+ "pages = {S233-S240},"
								+ "number = {suppl 1},"
								+ "collectionid = {PPI Networks},"
								+ "publisher = {Oxford Univ Press},"
								+ "journal = {Bioinformatics},"
								+ "author = {Ideker, Trey and Ozier, Owen and Schwikowski, Benno and Siegel, Andrew F},"
								+ "title = {Discovering regulatory and signalling circuits in molecular interaction networks},"
								+ "volume = {18},"
								+ "read = {true},"
								+ "year = {2002},"
								+ "groups = {PPI Mechanisms},"
								+ "attached-file-1 = {library://Ideker2002Discovering.pdf},"
								+ "date-added = {2013-07-12 18:53:04 -0500},"
								+ "date-modified = {2013-08-10 17:31:03 -0500}"
								+ "}";
		
		List<BibTeXEntry> parseBibTeXEntries = helper.parseBibTeXEntries(new StringReader(bibtex));
		
		assertEquals(1, parseBibTeXEntries.size());
		
		BibTeXEntryModel eModel = modelFactory.construct(parseBibTeXEntries.get(0));
		assertEquals("Discovering regulatory and signalling circuits in molecular interaction networks", eModel.getTitle());
	}
	
	public void test_parse_with_math_mode() throws Exception {
		String bibtex = "@article{rossini2008cyp2a6,\n"+
							"Author = {Ana Rossini and Rodolpho Mattos Albano and Luis Felipe Ribeiro Pinto and Tatiana de Almeida Sim$\\tilde{a}$o},\n"+
							"Date-Added = {2012-08-30 17:30:14 -0500},\n"+
							"Date-Modified = {2012-08-30 17:30:14 -0500},\n"+
							"Journal = {Pharmacogenomics},\n"+
							"Month = {November},\n"+
							"Number = {11},\n"+
							"Pages = {1737-1752},\n"+
							"Title = {CYP2A6 polymorphisms and risk for tobacco-related cancers},\n"+
							"Volume = {9},\n"+
							"Year = {2008}}";
		
		List<BibTeXEntry> parseBibTeXEntries = helper.parseBibTeXEntries(new StringReader(bibtex));
		
		assertEquals(1, parseBibTeXEntries.size());
		
		BibTeXEntryModel eModel = modelFactory.construct(parseBibTeXEntries.get(0));
		assertEquals("Ana Rossini and Rodolpho Mattos Albano and Luis Felipe Ribeiro Pinto and Tatiana de Almeida Sim$\\tilde{a}$o", eModel.getFieldValue("author", true, helper));
		assertEquals("Rossini, A; Albano, RM; Pinto, LFR; de Almeida Sim$\\tilde{a}$o, T", eModel.getCreator());
	}
	
	public void test_parse_bibtex_with_tilde() throws Exception {
		String bibtex = "@misc{weininger1995method,\n"+
							"Author = {Weininger, David},\n"+
							"Date-Added = {2013-04-20 19:12:02 -0500},\n"+
							"Date-Modified = {2013-04-20 19:12:02 -0500},\n"+
							"Month = jul # {~18},\n"+
							"Note = {US Patent 5,434,796},\n"+
							"Publisher = {Google Patents},\n"+
							"Title = {Method and apparatus for designing molecules with desired properties by evolving successive populations},\n"+
							"Year = {1995}}";
		
		List<BibTeXEntry> parseBibTeXEntries = helper.parseBibTeXEntries(new StringReader(bibtex));
		
		assertEquals(1, parseBibTeXEntries.size());
		
		BibTeXEntryModel eModel = modelFactory.construct(parseBibTeXEntries.get(0));
		assertEquals("Weininger, David;  Method and apparatus for designing molecules with desired properties by evolving successive populations; July~18, 1995;", eModel.getTextCitation(helper));
	}
	
	public void test_parse_bibtex_with_underscores() throws Exception {
		String goodbibtex = "@misc{weininger1995method,\n" + 
								"title = {Method and apparatus for designing molecules with desired properties by evolving successive populations},\n"+
								"abstract = {Some summary of the text enclosed.},\n"+
								"author = {Weininger, David},\n"+
								"howpublished = {Google Patents},\n"+
								"month = {Jul},\n"+
								"year = {1995},\n"+
								"doi = {10.1021/some_entry},\n"+
								"url = {http://www.example.com/some_entry}}";
		
		List<BibTeXEntry> parseBibTeXEntries = helper.parseBibTeXEntries(new StringReader(goodbibtex));
		
		assertEquals(1, parseBibTeXEntries.size());

		BibTeXEntryModel eModel = modelFactory.construct(parseBibTeXEntries.get(0));
		assertEquals("Weininger, David;  Method and apparatus for designing molecules with desired properties by evolving successive populations; Google Patents: Jul, 1995; DOI: 10.1021/some_entry", eModel.getTextCitation(helper));
	}
	
	public void test_build_entry_with_underscores() throws Exception {
		BibTeXEntryModel entry = new BibTeXEntryModel();
		
		entry.setType("unpublished");
		entry.setRefKey("weininger1995method");
		
		parseAndSetField("title", "Method and apparatus for designing molecules with desired properties by evolving successive populations", entry, helper);
		parseAndSetField("abstract", "Some summary of the text enclosed.", entry, helper);
		parseAndSetField("author", "Weininger, David", entry, helper);
		parseAndSetField("howpublished", "Google Patents", entry, helper);
		parseAndSetField("month", "Jul", entry, helper);
		parseAndSetField("year", "1995", entry, helper);
		parseAndSetField("doi", "10.1021/some_entry", entry, helper);
		parseAndSetField("url", "http://www.example.com/some_entry", entry, helper);
		
		BibTeXEntryModel newEntry = modelFactory.commit(entry);
		
		assertEquals("Method and apparatus for designing molecules with desired properties by evolving successive populations", newEntry.getTitle());
		assertEquals("10.1021/some_entry", newEntry.getFieldValue("doi", true, helper));
	}
	
	private void parseAndSetField(String key, String value, BibTeXEntryModel entry, BibTeXHelper helper) throws Exception {
		value = BibTeXAccentParser.unicodeToLatex(value);
		value = BibTeXAccentParser.newlinesToLaTeX(value);
		
		entry.setField(key, value);
	}
}
