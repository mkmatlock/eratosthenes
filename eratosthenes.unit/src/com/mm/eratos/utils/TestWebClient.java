package com.mm.eratos.utils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.jmock.Expectations;

import android.accounts.Account;

import com.mm.eratos.model.EratosUri;
import com.mm.eratos.tests.EratosthenesUnitTestCase;

public class TestWebClient extends EratosthenesUnitTestCase {
	final String acctName = "mkmatlock at becker.wustl.edu";
	
	public void setUp() throws Exception{
	}
	
	public void test_register_ezproxy_wrong_password_should_fail() throws Exception {
		super.acctName = acctName;
		super.setUp();
		
		final EratosUri proxyAddr = EratosUri.parseUri("https://beckerproxy.wustl.edu/login?url=%s");
		final EratosUri proxyLogin = EratosUri.parseUri("https://login.beckerproxy.wustl.edu/login");
		
		final String username = "mkmatlock";
		final Account mockAccount = mock(Account.class);
		final String pw = "wrongpassword";
		
		checking(new Expectations(){{
			allowing(mockAccountManager).getEZProxyAccount(acctName);
			will(returnValue(mockAccount));
			
			oneOf(mockAccountManager).getEZProxyFetchURL(mockAccount);
			will(returnValue(proxyAddr));
			oneOf(mockAccountManager).getEZProxyLoginURL(mockAccount);
			will(returnValue(proxyLogin));
			oneOf(mockAccountManager).getEZProxyUsername(mockAccount);
			will(returnValue(username));
			oneOf(mockAccountManager).getEZProxyPassword(mockAccount);
			will(returnValue(pw));
		}});
		
		try{
			WebClient browser = WebClient.newInstance();
			browser.initHttpClient();
			browser.queryHtmlPage("http://www.nature.com", true);
			
			fail("Expected an error to be thrown");
		}catch(IOException e){
			
		}
	}
	
	public void test_fetch_pdf_through_proxy() throws Exception {
		super.acctName = acctName;
		super.setUp();
		
		final EratosUri proxyAddr = EratosUri.parseUri("https://beckerproxy.wustl.edu/login?url=%s");
		final EratosUri proxyLogin = EratosUri.parseUri("https://login.beckerproxy.wustl.edu/login");
		
		final String acctName = "mkmatlock at becker.wustl.edu";
		final String username = "mkmatlock";
		final Account mockAccount = mock(Account.class);
		
		System.out.println("Enter password:");
		Scanner in = new Scanner(System.in);
		final String pw = in.nextLine().trim();
		in.close();
		
		checking(new Expectations(){{
			allowing(mockAccountManager).getEZProxyAccount(acctName);
			will(returnValue(mockAccount));
			
			oneOf(mockAccountManager).getEZProxyFetchURL(mockAccount);
			will(returnValue(proxyAddr));
			oneOf(mockAccountManager).getEZProxyLoginURL(mockAccount);
			will(returnValue(proxyLogin));
			oneOf(mockAccountManager).getEZProxyUsername(mockAccount);
			will(returnValue(username));
			oneOf(mockAccountManager).getEZProxyPassword(mockAccount);
			will(returnValue(pw));
		}});
		
		String url = "http://www.nature.com/nrc/journal/v6/n1/full/nrc1784.html";
		String pdfurl = "http://www.nature.com/nrc/journal/v6/n1/pdf/nrc1784.pdf";
		String pdfurl2 = "http://pubs.acs.org/doi/pdf/10.1021/ci3005868";
		
		WebClient browser = WebClient.newInstance();
		browser.initHttpClient();
		
		String content = WebUtils.readStream( browser.queryContent(EratosUri.parseUri(url), true).getContent() );
		assertTrue( content.contains("environmental factors such as smoking, diet, reproductive") );
		
		File downloadedFile = new File("temp/download.pdf");
		browser.downloadFile(pdfurl, downloadedFile);
		
		assertEquals(new File("data/nrc1784.pdf").length(), downloadedFile.length());
		downloadedFile.delete();
		
		browser.downloadFile(pdfurl2, downloadedFile);
		assertEquals(new File("data/ci3005868.pdf").length(), downloadedFile.length());
		downloadedFile.delete();
	}
	
	public void test_fetch_acs_pdf_should_set_cookie_redirect_and_download() throws Exception {
		super.setUp();
		
		checking(new Expectations(){{
			allowing(mockSettings).getEZProxyAccount();
			will(returnValue(""));
		}});
		
		String url = "http://pubs.acs.org/doi/pdf/10.1021/ci3005868";
		WebClient browser = WebClient.newInstance();
		File downloadedFile = new File("temp/download.pdf");
		browser.downloadFile(url, downloadedFile);
		
		assertEquals(new File("data/ci3005868.pdf").length(), downloadedFile.length());
		downloadedFile.delete();
	}
	
	public void test_get_redirects() throws Exception {
		super.setUp();
		
		checking(new Expectations(){{
			allowing(mockSettings).getEZProxyAccount();
			will(returnValue(""));
		}});
		
		EratosUri dxLink = EratosUri.parseUri("http://dx.doi.org/10.1126/science.1243490");
		
		WebClient browser = WebClient.newInstance();
		EratosUri destination = browser.followRedirects(dxLink);
		
		String expDestination = "http://www.sciencemag.org/content/342/6164/1367";
		assertEquals(expDestination, destination.toString());
	}
}
