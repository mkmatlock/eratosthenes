package com.mm.eratos.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXString;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.BibTeXStringModel;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.search.db.QueryElement;

public class TestBibTeXContentProviderAdapter extends BibTeXContentProviderAdapter {
	Map<String, QueryElement> filters = new HashMap<String, QueryElement>();
	Map<String, BibTeXEntry> entries = new HashMap<String, BibTeXEntry>();
	Map<String, BibTeXString> strings = new HashMap<String, BibTeXString>();
	List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
	
	public TestBibTeXContentProviderAdapter() {
		super(null);
	}

	@Override
	public List<BibTeXObject> getAllObjects() {
		return new ArrayList<BibTeXObject>(objects);
	}
	
	@Override
	public boolean createFilter(String name, QueryElement query) throws IOException {
		filters.put(name, query);
		return true;
	}
	

	@Override
	public QueryElement getFilterByName(String filterName) throws IOException {
		return filters.get(filterName);
	}
	
	@Override
	public void insert(BibTeXEntryModel entryModel) throws KeyCollisionException {
		if(entries.containsKey(entryModel.getRefKey()))
			throw new KeyCollisionException(entryModel.getRefKey());
		entries.put(entryModel.getRefKey(), entryModel.getEntry());
	}

	@Override
	public void bulkInsert(List<BibTeXEntryModel> entryModels, BulkInsertUpdateListener bulkInsertUpdateListener) {
		int i=0;
		for(BibTeXEntryModel entry : entryModels){
			entries.put(entry.getRefKey(), entry.getEntry());
			bulkInsertUpdateListener.update(i, entryModels.size());
			i++;
		}
	}

	@Override
	public void insert(BibTeXStringModel st) throws KeyCollisionException {
		if(entries.containsKey(st.getKey()))
			throw new KeyCollisionException(st.getKey());
		strings.put(st.getKey(), st.getBibTeXString());
	}

	@Override
	public void insert(BibTeXHelper helper, BibTeXString st) throws IOException, KeyCollisionException {
		if(entries.containsKey(st.getKey().getValue()))
			throw new KeyCollisionException(st.getKey().getValue());
		strings.put(st.getKey().getValue(), st);
	}

	@Override
	public void insert(BibTeXHelper helper, BibTeXObject obj) throws IOException {
		objects.add(obj);
	}

	@Override
	public BibTeXEntry getBibTeXEntry(String refKey) {
		return entries.get(refKey);
	}

	@Override
	public List<BibTeXEntryModel> getEntries(Collection<String> allKeys, BibTeXEntryModelFactory modelFactory) {
		List<BibTeXEntryModel> rval = new ArrayList<BibTeXEntryModel>(allKeys.size());
		
		for(String key : allKeys){
			if(entries.containsKey(key)){
				try {
					BibTeXEntry entry = entries.get(key);
					rval.add( modelFactory.construct(entry) );
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return rval;
	}

	@Override
	public int deleteAll(Collection<String> refKeys) {
		int deleted = 0;
		
		for(String k : refKeys){
			if(entries.containsKey(k)){
				entries.remove(k);
				deleted++;
			}
		}
		
		return deleted;
	}

	public Set<String> getAllRefKeys() {
		return entries.keySet();
	}
}
