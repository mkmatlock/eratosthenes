package com.mm.eratos.utils;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;

import junit.framework.TestCase;

public class TestEOLConvertingInputStreamReader extends TestCase {
	
	public String readStream(Reader testObject) throws Exception {
		StringBuilder sb = new StringBuilder();
		char []cbuf = new char[1024];
		int read = 0;
		while((read = testObject.read(cbuf)) > -1){
			sb.append(cbuf, 0, read);
		}
		return sb.toString();
	}
	
	public void test_read_windows_stream() throws Exception{
		EOLConvertingInputStreamReader testObject = new EOLConvertingInputStreamReader(new FileReader(new File("data/testgroups2.txt")));
		String exp = WebUtils.readStream(new FileReader(new File("data/testgroups2.txt")));
		String res = readStream(testObject);
		assertEquals(exp, res);
	}
}
