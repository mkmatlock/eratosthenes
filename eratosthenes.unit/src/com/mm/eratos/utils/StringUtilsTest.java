package com.mm.eratos.utils;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

public class StringUtilsTest extends TestCase {
	public void test_join() throws Exception {
		List<String> testList = Arrays.asList(new String[]{ "elem1", "elem2", "elem3" });
		List<String> testList2 = Arrays.asList(new String[]{});
		
		assertEquals("elem1;elem2;elem3", StringUtils.join(";", testList));
		assertEquals("", StringUtils.join(";", testList2));
	}
}
