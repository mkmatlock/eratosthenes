package com.mm.eratos.fragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import za.co.immedia.pinnedheaderlistview.PinnedHeaderListView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.mm.eratos.R;
import com.mm.eratos.activity.EntryListActivity;
import com.mm.eratos.activity.FileBrowserActivity;
import com.mm.eratos.activity.ListChooserActivity;
import com.mm.eratos.adapter.HeaderListCursorAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemLongClickCallback;
import com.mm.eratos.adapter.ReadableItemAdapter;
import com.mm.eratos.adapter.SelectableItemCompositeAdapter;
import com.mm.eratos.adapter.SelectableItemCompositeAdapter.ItemSelectedCallback;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.bibtex.operations.AddGroupTransaction;
import com.mm.eratos.bibtex.operations.AddKeywordTransaction;
import com.mm.eratos.bibtex.operations.AssignCollectionTransaction;
import com.mm.eratos.bibtex.operations.BibTeXEntryTransaction;
import com.mm.eratos.bibtex.operations.MarkReadTransaction;
import com.mm.eratos.bibtex.operations.RepairEntriesTransaction;
import com.mm.eratos.bibtex.operations.ToggleImportantTransaction;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.dialog.AutocompleteTextEntryDialog;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.files.FileCacheListener;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IUriResultListener;
import com.mm.eratos.io.IOFactory;
import com.mm.eratos.io.bibtex.EratosthenesParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolUploadResultReceiver;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementTrue;
import com.mm.eratos.task.AlterEntryTask;
import com.mm.eratos.task.CacheEntriesTask;
import com.mm.eratos.task.CacheEntriesTask.CacheEntryResultListener;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.ProcessURLForPdfTask;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.InputUtils;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebUtils;
import com.mm.eratos.view.MultiItemDragShadowBuilder;

/**
 * A list fragment representing a list of Entries. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link EntryDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class EntryListFragment extends Fragment implements LoaderCallbacks<Cursor>  {
	private static final int REQUEST_CHOOSE_EXPORT_LOCATION = 22;
	private static final int REQUEST_CHOOSE_COLLECTION = 33;
	private static final int REQUEST_CHOOSE_GROUP = 44;
	
	public static final String STATE_ACTIVATED_POSITION = "activated_position";
	public static final String DISABLE_BUTTONS = "disable_buttons";
	public static final String ENABLE_DRAG = "draggable";
	
	public static final CharSequence ENTRY_CLIP_LABEL = "Entry";

	private Handler onLoadHandler;

	private static final int MSG_RESET_ACTIVE_ITEM = 1;

	private class ItemSelectedListener implements ItemSelectedCallback<Cursor> {

		public void selected(int position, Cursor item) {
			if(mActionMode == null){
				getActivity().startActionMode(mActionModeCallback);
			}else{
				mActionMode.setTitle(Integer.toString(selectableAdapter.countSelectedItems()));
			}
		}

		public void deselected(int position, Cursor item) {
			if(mActionMode != null){
				if(selectableAdapter.countSelectedItems() == 0)
					mActionMode.finish();
				else
					mActionMode.setTitle(Integer.toString(selectableAdapter.countSelectedItems()));
			}
		}
		
		public boolean initialize(int position, Cursor item, View v) {
			String type = bibtexAdapter.getType(item);
			
			if(BibTeXEntryModel.ARTICLE.equalsIgnoreCase(type)){
				((ImageButton)v).setImageResource(R.drawable.article_icon);
			}else if(BibTeXEntryModel.BOOK.equalsIgnoreCase(type)){
				((ImageButton)v).setImageResource(R.drawable.book_icon);
			}else if(BibTeXEntryModel.INBOOK.equalsIgnoreCase(type)){
				((ImageButton)v).setImageResource(R.drawable.book_icon);
			}else{
				((ImageButton)v).setImageResource(R.drawable.article_icon);
			}
			return false;
		}
	}
	
	private class ReadableItemInitializer implements ReadableItemAdapter.SetupStateCallback<Cursor> {
		public void setupState(int position, Cursor c, com.mm.eratos.view.IReadable v) {
			boolean read = bibtexAdapter.isRead(c);
			v.setRead(read);
		}
	}
	
	private class ItemClickListener extends PinnedHeaderListView.OnItemClickListener{
		public void onItemClick(AdapterView<?> adapterView, View view, int section, int position, long id) {
			int absPosition = pinnedAdapter.getAbsolutePosition(section, position);
			int itemPosition = pinnedAdapter.getPositionForItem(absPosition);
			
			Cursor cursor = (Cursor) listView.getItemAtPosition(itemPosition);	
			String key = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_KEY) );
			
			mCallbacks.onItemSelected(absPosition, key);
			setActivatedPosition(absPosition);
		}
		
		public void onSectionClick(AdapterView<?> adapterView, View view, int section, long id) {
			setActivatedPosition(mActivatedPosition);
		}
	}
	
	private class AttachmentOverwriteListener implements ItemLongClickCallback<Cursor>{
		public boolean perform(int position, Cursor cursor, View v) {
			final Cursor c = cursor;
			final ImageButton buttonView = (ImageButton)v;
			
			NotificationUtils.confirmDialog(getActivity(), "Force Download?", "Force download of remote file and overwrite local copy?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
					BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
					BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
					
					BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(c, modelFactory);
					EratosUri uri = entry.getFiles().get(0);
					uri = uri.resolveUri(EratosApplication.getApplication());
					
					FileManager.getFileManager().fetchFile(getActivity(), uri, new FileCacheListener(getActivity(), new IUriResultListener() {
						public boolean onResult(EratosUri result) {
							setupView(result, buttonView);
							return true;
						}
					}));
				}
			});
			return true;
		}

		private void setupView(EratosUri uri, ImageButton v) {
			File f = FileManager.getFileManager().getFile(uri);
			
			v.setEnabled(true);
			v.setVisibility(View.VISIBLE);
			
			if(f.exists())
				v.setImageResource(R.drawable.content_attachment_available);
			else
				v.setImageResource(R.drawable.content_attachment);
		}
	}
	
	private class AttachmentFetchListener implements ItemActionCallback<Cursor>{
		private boolean disableButtons;

		public AttachmentFetchListener(boolean disableButtons) {
			this.disableButtons = disableButtons;
		}

		public void perform(int position, Cursor c, View v) {
			boolean show = c.getInt( c.getColumnIndex( DBHelper.COLUMN_HASATTACHMENTS ) ) == 1;
			if(show){
				BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
				BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
				BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
				
				BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(c, modelFactory);
				EratosUri uri = entry.getFiles().get(0);
				uri = uri.resolveUri(EratosApplication.getApplication());
				
				final ImageButton buttonView = (ImageButton)v;
				FileManager.getFileManager().cacheFile(getActivity(), uri, new FileCacheListener(getActivity(), new IUriResultListener() {
					public boolean onResult(EratosUri result) {
						setupView(result, buttonView);
						return true;
					}
				}));
			}
		}
		
		@Override
		public void initialize(int position, Cursor c, View v) {
			boolean show = bibtexAdapter.hasAttachments(c);
			if(show){
				EratosUri uri = bibtexAdapter.getAttachment(c);
				setupView(uri, (ImageButton)v);
				
				if(disableButtons){
					v.setEnabled(false);
					v.setAlpha(0.5f);
				}
			}else{
				v.setEnabled(false);
				v.setVisibility(View.INVISIBLE);
			}
		}

		private void setupView(EratosUri uri, ImageButton v) {
			File f = FileManager.getFileManager().getFile(uri);
			
			v.setEnabled(true);
			v.setVisibility(View.VISIBLE);
			
			if(f.exists())
				v.setImageResource(R.drawable.content_attachment_available);
			else
				v.setImageResource(R.drawable.content_attachment);
		}
	}
	
	private class StarToggleListener implements ItemActionCallback<Cursor>{
		private boolean disableButtons;

		public StarToggleListener(boolean disableButtons) {
			this.disableButtons = disableButtons;
		}

		public void perform(int position, Cursor item, View v) {
			BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
			BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
			BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
			
			BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(item, modelFactory);
			
			try {
				new ToggleImportantTransaction(getActivity(), bibtexAdapter, fileAdapter).execute(entry);
				
				if(entry.important())
					((ImageButton)v).setImageResource(android.R.drawable.btn_star_big_off);
				else
					((ImageButton)v).setImageResource(android.R.drawable.btn_star_big_on);
			} catch(Exception e){
				NotificationUtils.notifyException(getActivity(), "Unexpected Error", e);
			}
		}
		
		@Override
		public void initialize(int position, Cursor c, View v) {
			boolean important = bibtexAdapter.isImportant(c);
			if(important)
				((ImageButton)v).setImageResource(android.R.drawable.btn_star_big_on);
			else
				((ImageButton)v).setImageResource(android.R.drawable.btn_star_big_off);
			
			if(disableButtons){
				v.setEnabled(false);
				v.setAlpha(0.5f);
			}
		}
	}
	
	private class ItemLongClickListener extends PinnedHeaderListView.OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> adapterView, View view, int section, int position, long id) {
			if(enableDrag){
				int absPosition = pinnedAdapter.getAbsolutePosition(section, position);
				int itemPosition = pinnedAdapter.getPositionForItem(absPosition);
				
				Cursor cursor = (Cursor) listView.getItemAtPosition(itemPosition);
				String refKey = cursor.getString( cursor.getColumnIndex( DBHelper.COLUMN_KEY) );
				List<String> selectedKeys = getSelectedRefKeys();
				
				if(selectedKeys.contains(refKey)){
					dragMultiple(view, selectedKeys);
				}else{
					dragSingle(view, refKey);
				}
				return true;
			}
			return false;
		}

		@Override
		public boolean onSectionLongClick(AdapterView<?> adapterView, View view, int section, long id) {
			return false;
		}
		
		private void dragSingle(View v, String refKey) {
			ClipData.Item item = new ClipData.Item( refKey );
			ClipDescription clipDesc = new ClipDescription( ENTRY_CLIP_LABEL, new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN} );
			ClipData dragData = new ClipData( clipDesc, item );
			View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
			
			v.startDrag(dragData,  // the data to be dragged
			            myShadow,  // the drag shadow builder
			            null,      // no need to use local data
			            0          // flags (not currently used, set to 0)
			);
		}

		private void dragMultiple(View v, List<String> selectedKeys) {
			ClipDescription clipDesc = new ClipDescription( ENTRY_CLIP_LABEL, new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN} );
			ClipData dragData = null;
			for(String selKey : selectedKeys){
				ClipData.Item item = new ClipData.Item( selKey );
				if(dragData == null)
					dragData = new ClipData( clipDesc, item );
				else
					dragData.addItem(item);
			}
			
			View.DragShadowBuilder myShadow = new MultiItemDragShadowBuilder(v, selectedKeys);
			
			v.startDrag(dragData,  // the data to be dragged
			        myShadow,  // the drag shadow builder
			        null,      // no need to use local data
			        0          // flags (not currently used, set to 0)
			);
		}
	}
	

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
		public void onItemSelected(int pos, String id);
		public void onItemDeselected();
		public void onDataLoaded(Adapter adapter);
		public boolean isLargeView();
	}
	

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(int pos, String id) {
		}
		@Override
		public void onItemDeselected() {
		}
		@Override
		public void onDataLoaded(Adapter adapter) {
		}
		public boolean isLargeView(){
			return false;
		}
	};
	
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = sDummyCallbacks;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;
	private static final int LIST_LOADER = 0x01;

	private BibTeXContentProviderAdapter bibtexAdapter;
	private FileRevisionContentProviderAdapter fileAdapter;

	private ActionMode mActionMode;

	private PinnedHeaderListView listView;
	private SimpleCursorAdapter adapter;
	private SelectableItemCompositeAdapter<Cursor> selectableAdapter;
	private ReadableItemAdapter<Cursor> readableAdapter;
	private ItemActionCompositeAdapter<Cursor> attachAdapter;
	private ItemActionCompositeAdapter<Cursor> starAdapter;
	private HeaderListCursorAdapter pinnedAdapter;
	
	private boolean enableDrag;
	private boolean largeView;
	private boolean disableButtons;
	
	private QueryElement query;
	private int sortOrder;
	private int sortDirection;
	private ProgressDialog progressDialog;

	public static class EntryListLoadedHandler extends Handler {
		private EntryListFragment fragment;

		public EntryListLoadedHandler(EntryListFragment fragment){
			this.fragment = fragment;
		}
		
		public void handleMessage(Message msg) {
			if (msg.what == MSG_RESET_ACTIVE_ITEM) {
				fragment.mCallbacks.onItemDeselected();
				fragment.setActivatedPosition(ListView.INVALID_POSITION);
				fragment.mCallbacks.onDataLoaded(fragment.adapter);
			}
		}
	}
	
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public EntryListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		bibtexAdapter = new BibTeXContentProviderAdapter(getActivity().getContentResolver());
		fileAdapter = new FileRevisionContentProviderAdapter(getActivity().getContentResolver());
		
		query = new QueryElementTrue();
		enableDrag = false;
		disableButtons = false;
		
		Bundle args = getArguments();
		if(savedInstanceState != null){
			args = savedInstanceState;
		}
		
		if(args != null){
			mActivatedPosition = args.getInt(STATE_ACTIVATED_POSITION, ListView.INVALID_POSITION);
			enableDrag = args.getBoolean(EntryListFragment.ENABLE_DRAG, false);
			query = (QueryElement) args.getSerializable(EntryListActivity.QUERY);
			disableButtons = args.getBoolean(EntryListFragment.DISABLE_BUTTONS, false);
		}
		
		onLoadHandler = new EntryListLoadedHandler(this);

		largeView = mCallbacks.isLargeView();
		String[] uiBindFrom = { DBHelper.COLUMN_TITLE };
	    int[] uiBindTo = { R.id.label };
	    int layout_item = R.layout.bibtex_list_entry;

		if(largeView){
			uiBindFrom = new String[]{ DBHelper.COLUMN_TITLE, DBHelper.COLUMN_AUTHOR, DBHelper.COLUMN_YEAR };
		    uiBindTo = new int[]{ R.id.label, R.id.labelAuthor, R.id.yearLabel };
		    layout_item = R.layout.bibtex_list_entry_large;
		}

		adapter = new SimpleCursorAdapter(
			getActivity().getBaseContext(), layout_item,
			null, uiBindFrom, uiBindTo,
			CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		selectableAdapter = new SelectableItemCompositeAdapter<Cursor>(R.id.entryListItem, R.id.checkBox, new ItemSelectedListener(), adapter);
		readableAdapter = new ReadableItemAdapter<Cursor>(R.id.entryListItem, new ReadableItemInitializer(), selectableAdapter);
		if(largeView) {
			attachAdapter = new ItemActionCompositeAdapter<Cursor>(R.id.attachment, new AttachmentFetchListener(disableButtons), new AttachmentOverwriteListener(), readableAdapter);
			starAdapter = new ItemActionCompositeAdapter<Cursor>(R.id.star, new StarToggleListener(disableButtons), attachAdapter);
		}
		else
			starAdapter = new ItemActionCompositeAdapter<Cursor>(R.id.star, new StarToggleListener(disableButtons), readableAdapter);

		pinnedAdapter = new HeaderListCursorAdapter(getActivity(), R.layout.bibtex_collection_entry, R.id.header, DBHelper.COLUMN_COLLECTION, false, starAdapter);

		int sortOrder = EratosApplication.getApplication().getSettingsManager().getSortOrder();
		int sortDirection = EratosApplication.getApplication().getSettingsManager().getSortDirection();
		storeSortOrder(sortOrder, sortDirection);
		
		getLoaderManager().initLoader( LIST_LOADER, null, this );
	}
	
	public void resetLoader() {
		getLoaderManager().restartLoader( LIST_LOADER, null, this );
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION)
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		
		outState.putBoolean(EntryListFragment.ENABLE_DRAG, enableDrag);
		outState.putBoolean(EntryListFragment.DISABLE_BUTTONS, disableButtons);
		outState.putSerializable(EntryListActivity.QUERY, query);
	}
	
	public void setSortOrder(int sortOrder, int sortDirection){
		storeSortOrder(sortOrder, sortDirection);
		getLoaderManager().restartLoader(LIST_LOADER, null, this);
	}

	private void storeSortOrder(int sortOrder, int sortDirection) {
		this.sortOrder = sortOrder;
		this.sortDirection = sortDirection;
		
		if((sortOrder & BibTeXContentProviderAdapter.CUSTOM_SORT_BIT) == BibTeXContentProviderAdapter.CUSTOM_SORT_BIT){
			String mode = EratosApplication.getApplication().getSettingsManager().getSortFieldHeaderMode(sortOrder - BibTeXContentProviderAdapter.CUSTOM_SORT_BIT);
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_VALUE, mode.equals(EratosSettings.CUSTOM_SORT_FIELD_TYPE_FIRST_CHAR));
		}
		
		if(sortOrder == BibTeXContentProviderAdapter.SORT_RANDOM)
			pinnedAdapter.setSectionColumn(null, false);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_COLLECTION)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_COLLECTION, false);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_YEAR)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_YEAR, false);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_AUTHOR)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_AUTHOR, true);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_TITLE)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_TITLE, true);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_DATE_ADDED)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_DATE_ADDED, false);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_DATE_MODIFIED)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_DATE_MODIFIED, false);
		if(sortOrder == BibTeXContentProviderAdapter.SORT_SOURCE)
			pinnedAdapter.setSectionColumn(DBHelper.COLUMN_SOURCE, false);
	}


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.list_fragment, container, false);
		listView = (PinnedHeaderListView) rootView.findViewById(R.id.entry_list_tree);
		listView.setOnItemClickListener(new ItemClickListener());
		listView.setOnItemLongClickListener(new ItemLongClickListener());
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setAdapter(pinnedAdapter);
	    return rootView;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException( "Activity must implement fragment's callbacks." );
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	public ListView getListView(){
		return listView;
	}
	
	public void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			if(mActivatedPosition != ListView.INVALID_POSITION) {
				int sectionPos = pinnedAdapter.getPositionForItem(mActivatedPosition);
				listView.setItemChecked(sectionPos, false);
			}
			mCallbacks.onItemDeselected();
		} else {
			int sectionPos = pinnedAdapter.getPositionForItem(position);
			if(sectionPos > -1)
				listView.setItemChecked(sectionPos, true);
		}
		scrollIfNeeded(position);
		
		mActivatedPosition = position;
	}
	
	public void scrollIfNeeded(int position){
		if(position != ListView.INVALID_POSITION){
			int sectionPos = pinnedAdapter.getPositionForItem(position);
			int firstPos = listView.getFirstVisiblePosition() + 2;
			int lastPos = listView.getLastVisiblePosition() - 1;
			
			if(sectionPos < firstPos){
				listView.setSelection(sectionPos-1);
			}else if(lastPos < sectionPos){
				listView.setSelection(sectionPos+1);
			}
		}
	}
	
	public void setScroll(int y){
		listView.setScrollY(y);
	}

	public void clearSelection(){
		setActivatedPosition(ListView.INVALID_POSITION);
		mCallbacks.onItemDeselected();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		if(progressDialog != null)
			progressDialog.hide();
		progressDialog = ProgressDialog.show(getActivity(), "Loading Entries", "Loading...");
		return bibtexAdapter.filter(getActivity(), query, sortOrder, sortDirection);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if(progressDialog != null){
			progressDialog.hide();
			progressDialog = null;
		}
		adapter.swapCursor(cursor);
		onLoadHandler.sendEmptyMessage(MSG_RESET_ACTIVE_ITEM);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		if(progressDialog != null){
			progressDialog.hide();
			progressDialog = null;
		}
		adapter.swapCursor(null);
		mCallbacks.onDataLoaded(adapter);
	}
	
	public void submitQuery(QueryElement query) {
		clearSelection();
		this.query = query;
		getLoaderManager().restartLoader(LIST_LOADER, null, this);
	}
	
	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

	    // Called when the action mode is created; startActionMode() was called
	    @Override
	    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
	    	mActionMode = mode;
	    	
	        // Inflate a menu resource providing context menu items
	        MenuInflater inflater = mode.getMenuInflater();
	        inflater.inflate(R.menu.batch_manage, menu);
	        return true;
	    }

	    // Called each time the action mode is shown. Always called after onCreateActionMode, but
	    // may be called multiple times if the mode is invalidated.
	    @Override
	    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
	        return false; 
	    }

	    // Called when the user selects a contextual menu item
	    @Override
	    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
	        switch (item.getItemId()) {
	        	case R.id.tag_entries:
	        		chooseKeyword();
	        		break;
	        	case R.id.group_entries:
	        		chooseGroup();
	        		break;
	        	case R.id.set_collection:
	        		chooseCollection();
	        		break;
	        	case R.id.cite_entries:
	        		copyCitationToClipboard();
	        		break;
	        	case R.id.export_entries:
	        		chooseExportLocation();
	        		break;
	        	case R.id.share_entries:
	        		queryIncludeAttachments();
	        		break;
	        	case R.id.delete_entries:
	        		confirmDelete();
	        		break;
	        	case R.id.mark_read:
	        		markRead(true);
	        		break;
	        	case R.id.mark_unread:
	        		markRead(false);
	        		break;
				case R.id.cache_entries:
					cacheEntries();
					break;
	        	case R.id.format_citekey:
	        		formatCiteKeys();
	        		break;
	        	case R.id.select_all:
	        		selectAll();
	        		break;
	        	case R.id.repair_entries:
	        		repairEntries();
	        		break;
	        	case R.id.download_pdfs:
	        		downloadPdfs();
	        		break;
	            default:
	            	return false;
	        }
	        return true;
	    }

		// Called when the user exits the action mode
	    @Override
	    public void onDestroyActionMode(ActionMode mode) {
	    	mActionMode = null;
	    	selectableAdapter.deselectAll();
	    }
	};

	protected void confirmDelete() {
		NotificationUtils.confirmDialog(getActivity(), 
				"Delete entries?",
				"Are you sure you wish to delete these entries?", 
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						deleteSelectedEntries();
						clearSelection();
					}
				});
	}
	
	protected void selectAll() {
		selectableAdapter.selectAll();
		mActionMode.setTitle(Integer.toString(selectableAdapter.countSelectedItems()));
	}

	protected void downloadPdfs() {
		if(EratosApplication.getApplication().isOnline()){
			List<String> selectedRefKeys = getSelectedRefKeys();
			new ProcessURLForPdfTask(getActivity()).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, selectedRefKeys.toArray(new String[]{}));
		}else{
			NotificationUtils.notify(getActivity(), "No Internet Connection", "Internet connection is not available, connect to your wifi or mobile network first");
		}
	}

	protected void cacheEntries() {
		String protocol = EratosApplication.getApplication().getCurrentLibrary().protocol();
		
		ProtocolConnectionManager protocolConnectionManager = ProtocolConnectionManager.getConnectionManager(protocol);
		protocolConnectionManager.connect(getActivity(), 
				new ProtocolConnectionManager.ConnectionListenerNotifier(protocolConnectionManager, getActivity()) {
					public void success() {
						List<String> selectedRefKeys = getSelectedRefKeys();
						new CacheEntriesTask(getActivity(), new CacheEntryResultListener() {
							public void success() {
								adapter.notifyDataSetChanged();
							}
							public void failed() {
							}
						}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, selectedRefKeys.toArray(new String[]{}));
					}
				});
	}
	
	protected void cacheEntries(final List<String> selectedRefKeys, final CacheEntryResultListener listener) {
		String protocol = EratosApplication.getApplication().getCurrentLibrary().protocol();
		
		ProtocolConnectionManager protocolConnectionManager = ProtocolConnectionManager.getConnectionManager(protocol);
		protocolConnectionManager.connect(getActivity(), 
				new ProtocolConnectionManager.ConnectionListenerNotifier(protocolConnectionManager, getActivity()) {
					public void success() {
						new CacheEntriesTask(getActivity(), listener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, selectedRefKeys.toArray(new String[]{}));
					}
				});
	}

	protected void formatCiteKeys() {
		CiteKeyFormatter citeKeyFormatter = EratosApplication.getApplication().getSettingsManager().getCiteKeyFormatter();
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		int success = 0;
		int errors = 0;
		List<String> selectedRefKeys = getSelectedRefKeys();
		for(String refKey : selectedRefKeys){
			try{
				BibTeXEntryModel entry = bibtexAdapter.getEntry(refKey, modelFactory);
				String newKey = citeKeyFormatter.format(entry);
				entry.setRefKey(newKey);
				
				if(bibtexAdapter.getEntry(newKey, modelFactory) != null)
					throw new KeyCollisionException("Duplicate key: " + newKey);
				
				BibTeXEntryModel newEntry = modelFactory.commit(entry);
				
				bibtexAdapter.delete(refKey);	
				bibtexAdapter.insert(newEntry);
				success++;
			}catch(Exception e){
				e.printStackTrace();
				errors++;
			}
		}
		
		if(errors > 0)
			NotificationUtils.longToast(getActivity(), String.format("Formatted cite keys for %d entries, encountered %d errors", success, errors));
		else
			NotificationUtils.longToast(getActivity(), String.format("Formatted cite keys for %d entries", success));
	}

	protected void copyCitationToClipboard() {
		List<String> selectedRefKeys = getSelectedRefKeys();
		String citeString = String.format("~\\%s{%s}", EratosApplication.getApplication().getSettingsManager().getCitationCommand(), StringUtils.join(",", selectedRefKeys));
		
		ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("simple text", citeString);
		clipboard.setPrimaryClip(clip);
		
		NotificationUtils.longToast(getActivity(), "Copied cite string to the clipboard");
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == REQUEST_CHOOSE_EXPORT_LOCATION && resultCode == Activity.RESULT_OK){
			finishExport( EratosUri.parseUri( data.getStringExtra(FileBrowserActivity.PATH_ARG) ) );
		}else if(requestCode == REQUEST_CHOOSE_COLLECTION && resultCode == Activity.RESULT_OK){
			try{
				assignCollection(data.getStringExtra(ListChooserActivity.CATEGORY_FIELD));
			}catch(Exception e){
				e.printStackTrace();
				NotificationUtils.shortToast(getActivity(), "Could not set collection. An error occurred");
			}
		}else if(requestCode == REQUEST_CHOOSE_GROUP && resultCode == Activity.RESULT_OK){
			try{
				addToGroup(data.getStringExtra(ListChooserActivity.CATEGORY_FIELD));
			}catch(Exception e){
				e.printStackTrace();
				NotificationUtils.shortToast(getActivity(), "Could not set group. An error occurred");
			}
		}
	}
	
	protected void queryIncludeAttachments() {
		NotificationUtils.ternaryDialog(getActivity(), "Export Attachments", "Include attached documents with exported citations?",
				new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which) {
						switch(which){
						case DialogInterface.BUTTON_NEGATIVE:
							break;
						case DialogInterface.BUTTON_NEUTRAL:
							trySendSelectedEntries(false);
							break;
						case DialogInterface.BUTTON_POSITIVE:
							trySendSelectedEntries(true);
							break;
						}
					}
		});
	}

	private void trySendSelectedEntries(boolean includeAttachments) {
		try {
			sendSelectedEntries(includeAttachments);
		} catch (IOException e) {
			NotificationUtils.notifyException(getActivity(), "Unexpected Exception", e);
		}
	}

	protected void sendSelectedEntries(boolean includeAttachments) throws IOException {
		EratosSettings settingsManager = EratosApplication.getApplication().getSettingsManager();
		BibTeXHelper helper = settingsManager.getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		final List<String> selectedRefKeys = getSelectedRefKeys();
		List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>();
		for(String refKey : selectedRefKeys){
			BibTeXEntryModel entry = bibtexAdapter.getEntry(refKey, modelFactory);
			entries.add(entry);
		}
		
		String formattedCitations = settingsManager.getBibTeXHelper().formatCitations(entries, false);
		final String formattedCitationText = "Bibliography:\n\n" + formattedCitations + "\n\n" + EratosApplication.SENT_BY_ERATOSTHENES_MESSAGE;
		
		boolean isDropbox = EratosApplication.getApplication().getCurrentLibrary().protocol().equals(EratosUri.DROPBOX_PROTOCOL);
		
		if(includeAttachments && isDropbox){	
			cacheEntries(selectedRefKeys,
					new CacheEntryResultListener() {
						public void success() {
							sendWithAttachments(EratosthenesParser.ERATOS_FILE_TYPE, selectedRefKeys, formattedCitationText);
							adapter.notifyDataSetChanged();
						}
						public void failed() {
							NotificationUtils.longToast(getActivity(), "Could not create email attachments. PDFs could not be correctly cached from dropbox.");
						}
					});
		}else if(includeAttachments){
			sendWithAttachments(EratosthenesParser.ERATOS_FILE_TYPE, selectedRefKeys, formattedCitationText);
		}else{
			sendWithoutAttachments(EratosthenesParser.ERATOS_FILE_TYPE, selectedRefKeys, formattedCitationText);
		}
	}

	private void sendWithAttachments(String bibType, List<String> selectedRefKeys, final String formattedCitationText) {
		FileManager fileManager = FileManager.getFileManager();
		
		final EratosUri tempBibtexUri = fileManager.getTemporaryExportUri();
		final EratosUri tempArchiveUri = fileManager.getTemporaryExportArchiveUri();
		final File tArchive = fileManager.getFile(tempArchiveUri);
		
		SaveDatabaseTask.exportEntries(getActivity(), 
				tempBibtexUri, 
				bibType,
				"UTF-8",
				tempArchiveUri,
				selectedRefKeys.toArray(new String[]{}), 
				new SaveDatabaseTask.ExportBibListener() {
					ProgressDialog progressDialog;
					
					@Override
					public void started() {
						progressDialog = ProgressDialog.show(getActivity(), "Exporting Entries...", "Exporting...", false, false);
					}
					@Override
					public void progress(int progress, int maxprogress) {
						progressDialog.setMax(maxprogress);
						progressDialog.setProgress(progress);
					}
					@Override
					public void finished(EratosUri exportUri, int entries, int objects, int strings) {
						progressDialog.dismiss();
						NotificationUtils.share(getActivity(), "", formattedCitationText, tArchive);
					}
					@Override
					public void failed(Throwable error) {
						progressDialog.dismiss();
						NotificationUtils.notifyException(getActivity(), "Share Entries Failed", error);
					}
				});
	}
	
	private void sendWithoutAttachments(String bibType, List<String> selectedRefKeys, final String formattedCitationText) {
		FileManager fileManager = FileManager.getFileManager();
		
		EratosUri exportUri = fileManager.getTemporaryExportUri();
		final File exportFile = fileManager.getFile(exportUri);
		
		SaveDatabaseTask.exportEntries(getActivity(), 
				exportUri,
				bibType,
				"UTF-8",
				selectedRefKeys.toArray(new String[]{}), 
				new SaveDatabaseTask.ExportBibListener() {
					ProgressDialog progressDialog;
					
					@Override
					public void started() {
						progressDialog = ProgressDialog.show(getActivity(), "Exporting Entries...", "Exporting...", false, false);
					}
					@Override
					public void progress(int progress, int maxprogress) {
						progressDialog.setMax(maxprogress);
						progressDialog.setProgress(progress);
					}
					@Override
					public void finished(EratosUri exportUri, int entries, int objects, int strings) {
						progressDialog.dismiss();
						NotificationUtils.share(getActivity(), "", formattedCitationText, exportFile);
					}
					@Override
					public void failed(Throwable error) {
						progressDialog.dismiss();
						NotificationUtils.notifyException(getActivity(), "Share Entries Failed", error);
					}
				});
	}

	protected void chooseCollection() {
		Intent i = ListChooserActivity.getCollection(getActivity());
		startActivityForResult(i, REQUEST_CHOOSE_COLLECTION);
	}
	
	protected void chooseGroup() {
		Intent i = ListChooserActivity.getGroup(getActivity());
		startActivityForResult(i, REQUEST_CHOOSE_GROUP);
	}

	class KeywordAddListener implements AutocompleteTextEntryDialog.OnFinishedListener {
		@Override
		public boolean onConfirm(String value) {
			try {
				assignKeyword(value);
				return true;
			} catch (Exception e) {
				NotificationUtils.notifyException(getActivity(), "Unexpected error", e);
			}
			return false;
		}

		@Override
		public void onCancel() {
			
		}
	}
	
	protected void chooseKeyword() {
		Cursor c = bibtexAdapter.getKeywords();
		Set<String> keywords = new HashSet<String>(c.getCount());
		while(c.moveToNext()){
			String kw = c.getString( c.getColumnIndex(DBHelper.COLUMN_KEYWORD) );
			keywords.add(kw);
		}
		
		final AutocompleteTextEntryDialog input = new AutocompleteTextEntryDialog(getActivity(), "Add Keyword", keywords, new KeywordAddListener());
		input.createDialog("Enter keyword...", "");
	}
	
	protected void addToGroup(String group) throws Exception {
		List<String> selectedRefKeys = getSelectedRefKeys();
		new AddGroupTransaction(getActivity(), bibtexAdapter, group).executeOnKeys(selectedRefKeys);
		NotificationUtils.shortToast(getActivity(), String.format("Assigned group to %d entries", selectedRefKeys.size()));
	}
	
	protected void assignKeyword(String keyword) throws Exception {
		List<String> selectedRefKeys = getSelectedRefKeys();
		new AddKeywordTransaction(getActivity(), bibtexAdapter, keyword).executeOnKeys(selectedRefKeys);
		NotificationUtils.shortToast(getActivity(), String.format("Assigned keyword to %d entries", selectedRefKeys.size()));
	}

    private void markRead(boolean state) {
    	try{
    		tryMarkRead(state);
    	} catch (Exception e) {
			NotificationUtils.notifyException(getActivity(), "Unexpected error", e);
		}
	}

	private void tryMarkRead(boolean state) throws Exception {
		List<String> selectedRefKeys = getSelectedRefKeys();
		new MarkReadTransaction(getActivity(), bibtexAdapter, state).executeOnKeys(selectedRefKeys);

		String opType = "unread";
		if(state)
			opType = "read";
		NotificationUtils.shortToast(getActivity(), String.format("Marked %d entries as %s", selectedRefKeys.size(), opType));
	}
	
	private void chooseExportLocation(){
		Intent i = FileBrowserActivity.getLocation(getActivity());
		startActivityForResult(i, REQUEST_CHOOSE_EXPORT_LOCATION);
	}
	
	private void deleteSelectedEntries(){
		List<String> selectedRefKeys = getSelectedRefKeys();
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		int deletedFiles = 0;
		for(String refKey : selectedRefKeys){
			BibTeXEntryModel entry = bibtexAdapter.getEntry(refKey, modelFactory);
			
			FileManager fileManager = FileManager.getFileManager();
			for(EratosUri uri : entry.getFiles()){
				File f = fileManager.getFile(uri);
				if (f.exists())
					f.delete();
				deletedFiles += 1;
				
				if(uri.isRemoteProtocol(EratosApplication.getApplication())){
					if(fileAdapter.exists(uri))
						fileAdapter.update(fileAdapter.getFile(uri).delete());
					else
						fileAdapter.update(fileAdapter.newSyncedFile(uri, "", f, entry.important()).delete());
				}
			}
			bibtexAdapter.delete(refKey);
		}
		
		NotificationUtils.shortToast(getActivity(), String.format("Deleted %d entries, %d files", selectedRefKeys.size(), deletedFiles));
		SaveDatabaseTask.autosaveDatabase(getActivity(), new FileSyncResultReceiver(getActivity()));
	}

	private void assignCollection(String collection) throws Exception {
		List<String> selectedRefKeys = getSelectedRefKeys();
		
		new AssignCollectionTransaction(getActivity(), bibtexAdapter, collection).executeOnKeys(selectedRefKeys);
		NotificationUtils.shortToast(getActivity(), String.format("Assigned collection to %d entries", selectedRefKeys.size()));
	}
	
	private void repairEntries() {
		List<String> selectedRefKeys = getSelectedRefKeys();
		
		BibTeXEntryTransaction operator = new RepairEntriesTransaction(getActivity(), bibtexAdapter, true);
		new AlterEntryTask(getActivity(), "Repairing Entries", operator).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, selectedRefKeys.toArray(new String[]{}));
	}

	private void finishExport(final EratosUri exportUri) {
		NotificationUtils.promptInput(getActivity(), "Choose a filename", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				final EratosUri exportFileUri = exportUri.append(result);
				String mimeType = WebUtils.getMimeType(exportFileUri);
				Map<String, String> types = IOFactory.getSupportedTypes(mimeType);
				
				if(types.size() == 0){
					NotificationUtils.notify(getActivity(), "Unsupported file extension", "Please enter a file with extension from one of: .bib .ris .enl .enz");
					return false;
				}else if(types.size() == 1){
					for(String k : types.keySet())
						finalizeExport(exportFileUri, types.get(k));
				}else{
					InputUtils.mapChoiceDialog(getActivity(), "Choose Export Type", types, new IStringResultListener() {
						public boolean onResult(String result, DialogInterface dialog) {
							finalizeExport(exportFileUri, result);
							return true;
						}
					});
				}
				return true;
			}
		});
	}
	
	private void finalizeExport(EratosUri exportFileUri, String fileType){
		List<String> exportKeys = getSelectedRefKeys();
		
		boolean isRemote = exportFileUri.isRemoteProtocol(EratosApplication.getApplication());
		ProtocolHandler handler = ProtocolHandler.getProtocolHandler(exportFileUri);
		
		SaveDatabaseTask.exportEntries(getActivity(), 
										exportFileUri, 
										fileType,
										"UTF-8",
										exportKeys.toArray(new String[]{}), 
										new ProtocolUploadResultReceiver(getActivity(), handler, isRemote));
	}

	private List<String> getSelectedRefKeys() {
		int itemCnt = selectableAdapter.countSelectedItems();
		final List<String> assignKeys = new ArrayList<String>(itemCnt);
		
		for(Integer pos : selectableAdapter.getSelectedPositions()) {
			int sectionpos = pinnedAdapter.getPositionForItem(pos);
			Cursor c = (Cursor) listView.getItemAtPosition(sectionpos);
			String refKey = c.getString(c.getColumnIndex(DBHelper.COLUMN_KEY));
			assignKeys.add(refKey);
		}
		
		cancelAction();
		return assignKeys;
	}
	
	public void cancelAction(){
		if(mActionMode != null)
			mActionMode.finish();
	}

	public String getItemId(int position) {
		Cursor c = (Cursor) adapter.getItem(position);
		return c.getString( c.getColumnIndex( DBHelper.COLUMN_KEY ) );
	}

	public void setDraggable(boolean enableDrag) {
		this.enableDrag = enableDrag;
	}

	public int getScrollPosition() {
		return listView.getScrollY();
	}

	public int getItemPos(String refKey) {
		for(int i = 0; i < adapter.getCount(); i++){
			Cursor c = (Cursor) adapter.getItem(i);
			if(refKey.equals(c.getString( c.getColumnIndex( DBHelper.COLUMN_KEY ) ))){
				return i;
			}
		}
		return ListView.INVALID_POSITION;
	}
	
	public void onCacheClear(){
		adapter.notifyDataSetChanged();
	}
}
