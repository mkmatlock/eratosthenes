package com.mm.eratos.fragment;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

import com.mm.eratos.R;
import com.mm.eratos.activity.FileBrowserActivity;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.dialog.CustomSearchHandlersDialog;
import com.mm.eratos.dialog.CustomSortFieldDialog;
import com.mm.eratos.ezproxy.EZProxyManageAccountsDialog;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.search.CustomSearchHandler;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.StringUtils;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener, OnPreferenceChangeListener, OnPreferenceClickListener {
	public static final String STORE_FILE_LOCATION_RELATIVE = "relative";
	public static final String STORE_FILE_LOCATION_OTHER = "other";

	private static final int REQUEST_STORE_LOCATION = 1;
	private Preference locationPref;
	private String lPDefault;
	private Preference historySizePref;
	private Preference citeKeyFormatPref;
	private Preference citeCommandPref;
	private Preference customSortFieldsPref;
	private Preference customSearchHandlersPref;
	private Preference errorFormatPref;
	private Preference ezProxyAccountNamePref;
	private EratosSettings settingsManager;
	

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EratosApplication application = EratosApplication.getApplication();
        settingsManager = application.getSettingsManager();

        Resources resources = getActivity().getResources();
        addPreferencesFromResource(R.xml.preferences);
       
        ezProxyAccountNamePref = getPreferenceManager().findPreference(EratosSettings.EZPROXY_ACCOUNT_NAME);
        ezProxyAccountNamePref.setOnPreferenceClickListener(this);
        
        setupEZProxySummary();
        
        locationPref = getPreferenceManager().findPreference(EratosSettings.STORE_FILE_LOCATION_FIELD);
        locationPref.setOnPreferenceChangeListener(this);
		lPDefault = resources.getString(R.string.store_summ);
		
		EratosUri uri = settingsManager.getStoreLocalLocation();
		if(EratosSettings.STORE_FILE_LOCATION_DEFAULT.equals(uri.toString())){
			String subdir = settingsManager.getStoreLocalSubdir();
			if(subdir.isEmpty())
				locationPref.setSummary(lPDefault);
			else
				locationPref.setSummary("Store in subdirectory: " + subdir);
		}else{
			locationPref.setSummary(uri.toString());
		}
		
		historySizePref = setNumericalPreferenceSummary(EratosSettings.STORE_HISTORY_FIELD, R.string.store_history_summ, R.string.store_history_default);
		historySizePref.setOnPreferenceChangeListener(this);
		
        citeKeyFormatPref = setStringArraySummary(EratosSettings.CITE_KEY_FORMATTER_FIELD, R.array.cite_key_format_entries, R.array.cite_key_format_values, R.string.cite_key_format_default);
        citeKeyFormatPref.setOnPreferenceChangeListener(this);
        citeCommandPref = setStringArraySummary(EratosSettings.CITE_COMMAND_FIELD, R.array.cite_command_entries, R.array.cite_command_values, R.string.cite_command_default);
        citeCommandPref.setOnPreferenceChangeListener(this);
        
        errorFormatPref = getPreferenceManager().findPreference(EratosSettings.TREAT_WARNINGS_AS_ERRORS);
        errorFormatPref.setOnPreferenceChangeListener(this);
        
        customSortFieldsPref = getPreferenceManager().findPreference(EratosSettings.CUSTOM_SORT_FIELDS);
        customSortFieldsPref.setOnPreferenceClickListener(this);

        customSearchHandlersPref = getPreferenceManager().findPreference(EratosSettings.CUSTOM_SEARCH_HANDLERS_PREF);
        customSearchHandlersPref.setOnPreferenceClickListener(this);
        
        updateSortFieldsText( EratosApplication.getApplication().getSettingsManager().getSortFields() );
        updateSearchHandlersText( EratosApplication.getApplication().getSettingsManager().getSearchHandlers() );
    }
	
	private void setupEZProxySummary(){
		Resources resources = getActivity().getResources();
		EratosApplication app = EratosApplication.getApplication();
		EratosSettings settingsManager = app.getSettingsManager();
		
		String acctName = settingsManager.getEZProxyAccount();
		if(acctName.isEmpty())
			ezProxyAccountNamePref.setSummary(resources.getString(R.string.pref_use_ezproxy_summ));
		else
			ezProxyAccountNamePref.setSummary("Using EZProxy: " + acctName);
	}
	
	private Preference setNumericalPreferenceSummary(String prefName, int summaryFormat, int defaultId){
		Resources resources = getActivity().getResources();
		EratosApplication application = EratosApplication.getApplication();
		String defaultVal = resources.getString(defaultId);
		String summFormat = resources.getString(summaryFormat);
		
		int currentVal = Integer.parseInt(application.getPreferences().getString(prefName, defaultVal)); 
        Preference prefField = getPreferenceManager().findPreference(prefName);
        prefField.setSummary(String.format(Locale.US, summFormat, currentVal));
        
        return prefField;
	}
	
	private Preference setStringArraySummary(String prefName, int entriesId, int valuesId, int defaultId){
		Resources resources = getActivity().getResources();
		String defaultVal = resources.getString(defaultId);
		String [] entries = resources.getStringArray(entriesId);
		String [] values = resources.getStringArray(valuesId);
		String currentVal = EratosApplication.getApplication().getPreferences().getString(prefName, defaultVal); 
			
		Preference prefField = getPreferenceManager().findPreference(prefName);
		
		for(int i = 0; i < entries.length; i++){
			if(values[i].equals(currentVal)){
				prefField.setSummary(entries[i]);
			}
		}
		
		return prefField;
	}
	
	private Preference setStringArraySummary(String prefName, int entriesId, int valuesId, String currentVal){
		Resources resources = getActivity().getResources();
		String [] entries = resources.getStringArray(entriesId);
		String [] values = resources.getStringArray(valuesId);
			
		Preference prefField = getPreferenceManager().findPreference(prefName);
		
		for(int i = 0; i < entries.length; i++){
			if(values[i].equals(currentVal)){
				prefField.setSummary(entries[i]);
			}
		}
		
		return prefField;
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    getPreferenceScreen().getSharedPreferences()
	            .registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    getPreferenceScreen().getSharedPreferences()
	           .unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if(EratosSettings.CITATION_FORMAT_FIELD.equals(key) ||
				EratosSettings.IMPORT_EXPORT_FORMAT_FIELD.equals(key) ||
				EratosSettings.TREAT_WARNINGS_AS_ERRORS.equals(key)) {
			EratosApplication.getApplication().getSettingsManager().changeBibtexHelper();
		} else if(ezProxyAccountNamePref.getKey().equals(key)) {
			setupEZProxySummary();
		} else if(EratosSettings.USE_EZPROXY.equals(key)){
			setupEZProxySummary();
		}
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		Resources resources = getActivity().getResources();
		
		if(locationPref.getKey().equals(preference.getKey())){
			String val = (String)newValue;
			if(STORE_FILE_LOCATION_OTHER.equals(val)){
				Intent i = FileBrowserActivity.getLocation(getActivity());
				startActivityForResult(i, REQUEST_STORE_LOCATION);
				return false;
			}else if(STORE_FILE_LOCATION_RELATIVE.equals(val)){
				NotificationUtils.promptInput(getActivity(), "Specify sub-path", "", new IStringResultListener() {
					public boolean onResult(String result, DialogInterface dialog) {
						return handlePathResult(result);
					}
				});
				return false;
			}else if(EratosSettings.STORE_FILE_LOCATION_DEFAULT.equals(val)){
				setDefaultPath();
				return true;
			}
		}else if(historySizePref.getKey().equals(preference.getKey())){
			String summFormat = resources.getString(R.string.store_history_summ);
			int currentVal = Integer.parseInt((String)newValue); 
			historySizePref.setSummary(String.format(Locale.US, summFormat, currentVal));
		}else if(citeKeyFormatPref.getKey().equals(preference.getKey())){
			setStringArraySummary(EratosSettings.CITE_KEY_FORMATTER_FIELD, R.array.cite_key_format_entries, R.array.cite_key_format_values, (String)newValue);
		}else if(citeCommandPref.getKey().equals(preference.getKey())){
			setStringArraySummary(EratosSettings.CITE_COMMAND_FIELD, R.array.cite_command_entries, R.array.cite_command_values, (String)newValue);
		}
		return true;
	}

	private void setDefaultPath() {
		locationPref.setSummary(lPDefault);
		
		Editor edit = EratosApplication.getApplication().getPreferences().edit();
		edit.putString(EratosSettings.STORE_FILE_LOCATION_FIELD, EratosSettings.STORE_FILE_LOCATION_DEFAULT);
		edit.putString(EratosSettings.STORE_FILE_SUBDIRECTORY_FIELD, "");
		edit.commit();
	}

	protected boolean handlePathResult(String path) {
		path = path.trim();
		EratosUri uri = EratosUri.parseUri(EratosSettings.STORE_FILE_LOCATION_DEFAULT + path);
		
		if(uri == null){
			NotificationUtils.notify(getActivity(), "Bad Path", "Path invalid:" + path);
			return false;
		}
		
		try{
			File f = FileManager.getFileManager().getFile(uri);
			if(!f.exists() && !f.mkdirs()){
				NotificationUtils.notify(getActivity(), "Error Creating Path", "Could not create subdirectory: " + path);
				return false;
			}
		}catch(NullPointerException npe){
			
		}
		
		Editor edit = EratosApplication.getApplication().getPreferences().edit();
		edit.putString(EratosSettings.STORE_FILE_LOCATION_FIELD, EratosSettings.STORE_FILE_LOCATION_DEFAULT);
		edit.putString(EratosSettings.STORE_FILE_SUBDIRECTORY_FIELD, path);
		edit.commit();
		
		locationPref.setSummary("Store in subdirectory: " + path);
		
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode == REQUEST_STORE_LOCATION){
			if(resultCode == Activity.RESULT_OK){
				String uri = data.getStringExtra(FileBrowserActivity.PATH_ARG);
				Editor edit = EratosApplication.getApplication().getPreferences().edit();
				edit.putString(EratosSettings.STORE_FILE_LOCATION_FIELD, uri);
				edit.putString(EratosSettings.STORE_FILE_SUBDIRECTORY_FIELD, "");
				edit.commit();
				locationPref.setSummary(uri);
			}else if(resultCode == Activity.RESULT_CANCELED){
				Editor edit = EratosApplication.getApplication().getPreferences().edit();
				edit.putString(EratosSettings.STORE_FILE_LOCATION_FIELD, EratosSettings.STORE_FILE_LOCATION_DEFAULT);
				edit.commit();
				String lPDefault = getActivity().getResources().getString(R.string.store_summ);
				locationPref.setSummary(lPDefault);
			}
		}
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if(preference.equals(customSortFieldsPref))
			openCustomSortDialog();
		else if(preference.equals(ezProxyAccountNamePref))
			openEZProxyAccountDialog();
		else if(preference.equals(customSearchHandlersPref)){
			openSearchHandlersDialog();
		}
		else
			return false;
		return true;
	}

	private void openSearchHandlersDialog() {
		new CustomSearchHandlersDialog(getActivity(), new CustomSearchHandlersDialog.FinishedListener() {
			@Override
			public void finished(List<CustomSearchHandler> results) {
				EratosApplication.getApplication().getSettingsManager().setSearchHandlers(results);
				
				Map<String, CustomSearchHandler> searchHandlers = new HashMap<String, CustomSearchHandler>();
				for(CustomSearchHandler handler : results)
					searchHandlers.put(handler.name(), handler);
				
				updateSearchHandlersText(searchHandlers);
			}
			@Override
			public void cancelled() {
				
			}
		}).create();
	}

	private void openEZProxyAccountDialog() {
		new EZProxyManageAccountsDialog(getActivity()).create();
	}

	private void openCustomSortDialog() {
		new CustomSortFieldDialog(getActivity(), new CustomSortFieldDialog.FinishedListener() {
			@Override
			public void finished(Map<String, String> results) {
				EratosApplication.getApplication().getSettingsManager().setSortFields(results);
				updateSortFieldsText(results);
			}
			@Override
			public void cancelled() {
				
			}
		}).create();
	}

	protected void updateSortFieldsText(Map<String, String> fields) {
		if(fields.size() == 0)
			customSortFieldsPref.setSummary(R.string.custom_sort_fields_summary);
		else
			customSortFieldsPref.setSummary("Custom sort fields: " + StringUtils.join(", ", fields.keySet()));
	}
	
	protected void updateSearchHandlersText(Map<String, CustomSearchHandler> searchHandlers) {
		if(searchHandlers.size() == 0)
			customSearchHandlersPref.setSummary(R.string.pref_search_handlers_summ);
		else
			customSearchHandlersPref.setSummary("Custom search handlers: " + StringUtils.join(", ", searchHandlers.keySet()));
	}
}