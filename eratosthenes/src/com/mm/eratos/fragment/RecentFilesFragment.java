package com.mm.eratos.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.mm.eratos.R;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.adapter.SelectableItemCompositeAdapter;
import com.mm.eratos.adapter.SelectableItemCompositeAdapter.ItemSelectedCallback;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibFile;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.task.LoadLibraryTask;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.NotificationUtils;

public class RecentFilesFragment extends Fragment implements OnItemClickListener, OnClickListener, LoaderCallbacks<Cursor> {
	private int mActivatedPosition = ListView.INVALID_POSITION;
	private int mCheckedPosition = ListView.INVALID_POSITION;
	private static final int LIST_LOADER = 0x01;
	
	public interface Callbacks {
		public void onOpenLibraryRequest();
		public void onLibraryLoaded();
	}
	
	private class SourceIconCallback implements ItemActionCallback<Cursor>{
		@Override
		public void perform(int position, Cursor c, View v) {
			
		}
		@Override
		public void initialize(int position, Cursor c, View v) {
			String protocol = c.getString(c.getColumnIndex(DBHelper.COLUMN_PROTOCOL));
			int drawable = getDrawableForSource(protocol);
			
			((ImageView)v).setImageResource(drawable);
			v.setEnabled(false);
			v.setClickable(false);
		}
		
		private int getDrawableForSource(String protocol) {
			if(protocol.equalsIgnoreCase("dropbox"))
				return R.drawable.content_dropbox;
			if(protocol.equalsIgnoreCase("drive"))
				return R.drawable.content_drive;
			
			return android.R.drawable.ic_menu_save;
		}
	}
	
	Callbacks mCallbacks = new Callbacks(){
		public void onOpenLibraryRequest(){
		}
		public void onLibraryLoaded() {
		}
	};
	
	private BibTeXContentProviderAdapter bibtexAdapter;
	
	private ListView fileListView;
	private ImageButton viewFileButton;
	private ImageButton closeFileButton;
	
	private SimpleCursorAdapter adapter;
	private ItemActionCompositeAdapter<Cursor> iconAdapter;
	private SelectableItemCompositeAdapter<Cursor> selectableAdapter;
	private ImageButton addFileButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String[] uiBindFrom = { DBHelper.COLUMN_PATH, DBHelper.COLUMN_COUNT };
	    int[] uiBindTo = { R.id.filePath, R.id.fileEntryCount };
	    int layout_item = R.layout.nav_drawer_item;
	    
	    bibtexAdapter = new BibTeXContentProviderAdapter(getActivity().getContentResolver());

		adapter = new SimpleCursorAdapter(
			getActivity().getBaseContext(), layout_item,
			null, uiBindFrom, uiBindTo,
			CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		
		createAdapter();
		getLoaderManager().initLoader( LIST_LOADER, null, this );
	}

	private void createAdapter() {
		ItemSelectedCallback<Cursor> callback = new ItemSelectedCallback<Cursor>() {
			public void selected(int position, Cursor item) {
				
			}
			public void deselected(int position, Cursor item) {
				
			}
			public boolean initialize(int position, Cursor item, View v) {
				EratosUri currentLibrary = EratosApplication.getApplication().getCurrentLibrary();
				
				String protocol = item.getString( item.getColumnIndex(DBHelper.COLUMN_PROTOCOL));
				String path = item.getString( item.getColumnIndex(DBHelper.COLUMN_PATH));
				EratosUri fileUri = EratosUri.makeUri(protocol, path);
				
				
				boolean selected = fileUri.equals(currentLibrary);
				if(selected)
					mCheckedPosition = position;
				
				return selected;
			}
		};
		
		iconAdapter = new ItemActionCompositeAdapter<Cursor>(R.id.sourceIcon, new SourceIconCallback(), adapter);
		selectableAdapter = new SelectableItemCompositeAdapter<Cursor>(R.id.fileListItem, R.id.sourceIcon, callback, iconAdapter);
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.nav_drawer, container);
		
		fileListView = (ListView) rootView.findViewById(R.id.navListView);
		addFileButton = (ImageButton) rootView.findViewById(R.id.navAddFile);
		viewFileButton = (ImageButton) rootView.findViewById(R.id.navOpenFile);
		closeFileButton = (ImageButton) rootView.findViewById(R.id.navCloseFile);
		
		fileListView.setOnItemClickListener(this);
		
		addFileButton.setOnClickListener(this);
		viewFileButton.setOnClickListener(this);
		closeFileButton.setOnClickListener(this);
		
		fileListView.setAdapter(selectableAdapter);
		
		disableOpenRemove();
		
	    return rootView;
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == addFileButton.getId())
			mCallbacks.onOpenLibraryRequest();
		if(v.getId() == viewFileButton.getId())
			confirmOverwrite();
		else if(v.getId() == closeFileButton.getId())
			closeFile();
	}

	private void confirmOverwrite() {
		if(EratosApplication.getApplication().isSaved()){
			viewFile();
		}else{
			NotificationUtils.confirmDialog(getActivity(), "Warning!", "Current library is not saved, continue anyway?", 
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							viewFile();
						}
					});
		}
	}
	
	private void closeFile() {
		if(mActivatedPosition != ListView.INVALID_POSITION) {
			Cursor cBibFile = (Cursor)iconAdapter.getItem(mActivatedPosition);
			BibFile bibFile = bibtexAdapter.cursorToRecentFile(cBibFile);
			bibtexAdapter.removeRecentFile(bibFile);
			mActivatedPosition = ListView.INVALID_POSITION;
		}
	}
	
	private void viewFile() {
		if(mActivatedPosition != ListView.INVALID_POSITION) {
			Cursor cBibFile = (Cursor)iconAdapter.getItem(mActivatedPosition);
			BibFile bibFile = bibtexAdapter.cursorToRecentFile(cBibFile);
			
			ProgressCallback callback = new ProgressCallback() {
				public void update(String stage, int progress, int maxProgress) {}
				public void finished() {
					mCallbacks.onLibraryLoaded();
				}
				public void error(Throwable error) {}
			};
			new LoadLibraryTask(getActivity(), bibFile.uri(), bibFile.type(), bibFile.encoding(), true, true, callback).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}
	}
	
	public void onLibraryOpened() {
		setItemChecked(ListView.INVALID_POSITION);
		setItemSelected(ListView.INVALID_POSITION);
		
		fileListView.refreshDrawableState();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int pos, long id) {
		setItemSelected(pos);
	}

	private void setItemChecked(int pos) {
		if(mCheckedPosition != ListView.INVALID_POSITION)
			selectableAdapter.deselect(mCheckedPosition);
		mCheckedPosition = pos;
		if(mCheckedPosition != ListView.INVALID_POSITION)
			selectableAdapter.select(mCheckedPosition);
	}
	
	private void setItemSelected(int pos) {
		if(mActivatedPosition != ListView.INVALID_POSITION)
			fileListView.setItemChecked(mActivatedPosition, false);
		mActivatedPosition = pos;
		if(pos != ListView.INVALID_POSITION){
			fileListView.setItemChecked(mActivatedPosition, true);
			enableOpenRemove();
		}else
			disableOpenRemove();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(mActivatedPosition != ListView.INVALID_POSITION)
			enableOpenRemove();
		else
			disableOpenRemove();
	}

	private void disableOpenRemove() {
		viewFileButton.setEnabled(false);
		viewFileButton.setAlpha(0.5f);
		
		closeFileButton.setEnabled(false);
		closeFileButton.setAlpha(0.5f);
	}

	private void enableOpenRemove() {
		viewFileButton.setEnabled(true);
		viewFileButton.setAlpha(1.0f);
		
		closeFileButton.setEnabled(true);
		closeFileButton.setAlpha(1.0f);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return bibtexAdapter.getRecentFiles(getActivity());
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
		adapter.swapCursor(c);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> c) {
		adapter.swapCursor(null);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = new Callbacks() {
			public void onOpenLibraryRequest(){
			}
			public void onLibraryLoaded() {
			}
		};
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException( "Activity must implement fragment's callbacks." );
		}
		mCallbacks = (Callbacks) activity;
	}
}
