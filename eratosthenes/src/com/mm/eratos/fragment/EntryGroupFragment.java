package com.mm.eratos.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import za.co.immedia.pinnedheaderlistview.PinnedHeaderListView;
import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Checkable;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.CategoryCountAdapter;
import com.mm.eratos.adapter.CategoryCursorAdapter;
import com.mm.eratos.adapter.DropTargetAdapter;
import com.mm.eratos.adapter.HeaderListCompositingAdapter;
import com.mm.eratos.adapter.SelectableItemCompositeAdapter;
import com.mm.eratos.adapter.SingleSelectableItemCompositeAdapter;
import com.mm.eratos.adapter.SingleSelectableItemCompositeAdapter.ItemSelectedCallback;
import com.mm.eratos.adapter.StatefulItemAdapter;
import com.mm.eratos.adapter.StatefulItemAdapter.SetupStateCallback;
import com.mm.eratos.adapter.StatefulItemAdapter.StatefulView;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.operations.AddGroupTransaction;
import com.mm.eratos.bibtex.operations.AddKeywordTransaction;
import com.mm.eratos.bibtex.operations.AssignCollectionTransaction;
import com.mm.eratos.bibtex.operations.MarkReadTransaction;
import com.mm.eratos.bibtex.operations.RemoveGroupTransaction;
import com.mm.eratos.bibtex.operations.RemoveKeywordTransaction;
import com.mm.eratos.bibtex.operations.RenameGroupTransaction;
import com.mm.eratos.bibtex.operations.RenameKeywordTransaction;
import com.mm.eratos.bibtex.operations.ToggleImportantTransaction;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementAnd;
import com.mm.eratos.search.db.QueryElementHasKeyword;
import com.mm.eratos.search.db.QueryElementInCollection;
import com.mm.eratos.search.db.QueryElementInGroup;
import com.mm.eratos.search.db.QueryElementIsImportant;
import com.mm.eratos.search.db.QueryElementIsUnread;
import com.mm.eratos.search.db.QueryElementOr;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.view.GroupListItem;
import com.mm.eratos.view.IMarkable;

public class EntryGroupFragment extends Fragment {
	private static final int ACTION_MODE_NONE = 0;
	private static final int ACTION_MODE_FILTERING = 1;
	private static final int ACTION_MODE_EDITING = 2;
	
	private static final String FILTERING_ACTION_TITLE = "Filtering";
	
	public static final String DATABASE_LIBRARY = "Library";
	public static final String DATABASE_IMPORTANT = "Starred";
	public static final String DATABASE_UNREAD = "Unread";
	
	public final static String CATEGORY_DATABASE = "DATABASE";
	public final static String CATEGORY_FILTERS = "FILTERS";
	public final static String CATEGORY_GROUPS = "GROUPS";
	public static final String CATEGORY_COLLECTIONS = "COLLECTIONS";
	public final static String CATEGORY_KEYWORDS = "KEYWORDS";
	
	public final static int SECTION_DATABASE = 0;
	public final static int SECTION_FILTERS = 1;
	public final static int SECTION_GROUPS = 2;
	public final static int SECTION_COLLECTIONS = 3;
	public final static int SECTION_KEYWORDS = 4;

	private final static int ALL_LOADER = 0x00;
	private final static int IMPORTANT_LOADER = 0x01;
	private final static int UNREAD_LOADER = 0x02;
	private final static int COLLECTIONS_LOADER = 0x03;
	private final static int GROUP_LOADER = 0x04;
	private final static int KEYWORD_LOADER = 0x05;
	private final static int FILTER_LOADER = 0x06;
	
	private int mActivePosition;
	
	public static interface Callbacks {
		public void groupSelected(String category);
		public void groupItemSelected(String category, String item);
		public void groupItemDeselected();
		public void applyFilter(QueryElement query);
		public boolean isLarge();
	}
	
	private class SingleItemSelectListener<T> implements ItemSelectedCallback<Cursor> {
		@Override
		public void selected(int position, Cursor item) {
		}
		@Override
		public void deselected(int position, Cursor item) {
		}
		@Override
		public void initialize(int position, Cursor item, View v, boolean selected) {
			((IMarkable)v).setMarked(selected);
		}
	}
	
	private class ChosenStateListener<T> implements SetupStateCallback<T, Integer> {
		@Override
		public Integer defaultState() {
			return GroupListItem.STATE_NONE;
		}

		@Override
		public Integer stateChanged(int position, T item, Integer oldState, Integer newState) {
			return newState;
		}

		@Override
		public Integer setupState(int position, T item, StatefulView<Integer> view, Integer state) {
			return state;
		}
	}
	
	private class MultiSelectListener<T> implements SelectableItemCompositeAdapter.ItemSelectedCallback<T> {
		@Override
		public void selected(int position, T item) {
			if(mActionMode != null && mActionModeType == ACTION_MODE_EDITING)
				finishActionMode(false, true);
			if(mActionMode == null)
				startActionMode(ACTION_MODE_FILTERING, FILTERING_ACTION_TITLE, new MultiCategoryActionMode());
		}

		@Override
		public void deselected(int position, T item) {
			
		}

		@Override
		public boolean initialize(int position, T item, View v) {
			return false;
		}
	}

	private class MultiCategoryActionMode implements ActionMode.Callback{

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			if(item.getItemId() == R.id.filter_any){
				List<QueryElement> subqueries = getSelectedSubQueries();
				QueryElement anyQuery = new QueryElementOr(subqueries);
				setChosenElements();
				
				mCallbacks.applyFilter(anyQuery);
				
				finishActionMode(true, true);
				return true;
			}else if(item.getItemId() == R.id.filter_all){
				List<QueryElement> subqueries = getSelectedSubQueries();
				QueryElement allQuery = new QueryElementAnd(subqueries);
				setChosenElements();
				
				mCallbacks.applyFilter(allQuery);
				
				finishActionMode(true, true);
				return true;
			}
			return false;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
	        inflater.inflate(R.menu.category_filter, menu);
	        return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mActionMode = null;
			clearSelections();
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}
		
	}
	
	private abstract class EditCategoryActionMode implements ActionMode.Callback {
		private final String categoryName;
		private final String category;
		private SingleSelectableItemCompositeAdapter<Cursor> adapter;
		private int position;

		protected abstract boolean renameCategory(String oldName, String newName) throws Exception;
		protected abstract void deleteCategory(String category) throws Exception;
		
		public EditCategoryActionMode(String categoryName, String category, int position, SingleSelectableItemCompositeAdapter<Cursor> adapter){
			this.categoryName = categoryName;
			this.category = category;
			this.adapter = adapter;
			this.position = position;
		}
		
	    @Override
	    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
	        MenuInflater inflater = mode.getMenuInflater();
	        inflater.inflate(R.menu.category_manage, menu);
	        
	        adapter.select(position);
	        return true;
	    }

	    @Override
	    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
	        return false; 
	    }
	    
	    public void promptRename(){
	    	NotificationUtils.promptInput(getActivity(), "Rename " + categoryName, category, new IStringResultListener() {
				public boolean onResult(String result, DialogInterface dialog) {
					try{
						return renameCategory(category, result);
					}catch(Exception e){
						NotificationUtils.notifyException(getActivity(), "Rename Failed", e);
					}
					return false;
				}
			});
	    }
	    
	    public void confirmDelete(){
	    	NotificationUtils.confirmDialog(getActivity(), "Delete " + categoryName, "Delete " + categoryName + " '" + category + "'?", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					try{
						deleteCategory(category);
					}catch(Exception e){
						NotificationUtils.notifyException(getActivity(), "Delete Failed", e);
					}
				}
			});
	    }

	    @Override
	    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
	    	switch (item.getItemId()) {
	    		case R.id.rename_category:
	    			promptRename();
	    			break;
	    		case R.id.delete_category:
	    			confirmDelete();
	    			break;
	    	}
	    	finishActionMode(true, false);
	    	return true;
	    }

	    @Override
	    public void onDestroyActionMode(ActionMode mode) {
	    	mActionMode = null;
	    	adapter.deselect();
	    }
	}
	
	private class ItemClickListener extends PinnedHeaderListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int section, int position, long id) {
			clearSelections();
			clearChosenElements();
			if(mActionMode != null)
				mActionMode.finish();
			
			mActivePosition = listView.getCheckedItemPosition();
			System.out.println("Active position: " + mActivePosition);
			
			String item = (String) headerAdapter.getItem(section, position);
			mCallbacks.groupItemSelected(headerAdapter.getSectionTitle(section), item);
		}

		@Override
		public void onSectionClick(AdapterView<?> adapterView, View view, int section, long id) {
			clearSelections();
			if(mActionMode != null)
				mActionMode.finish();
			
			mCallbacks.groupSelected(headerAdapter.getSectionTitle(section));
		}
	}
	
	private class ItemLongClickListener extends PinnedHeaderListView.OnItemLongClickListener {
		public boolean onItemLongClick(AdapterView<?> adapterView, View view, int section, int position, long id) {
			String item = (String) headerAdapter.getItem(section, position);
			
			if(mActionMode != null)
				mActionMode.finish();
			
			if(section == SECTION_GROUPS)
				handleGroupEdit(item, position, groupsEditAdapter);
			else if(section == SECTION_COLLECTIONS)
				handleCollectionEdit(item, position, collectionsEditAdapter);
			else if(section == SECTION_KEYWORDS)
				handleKeywordEdit(item, position, keywordsEditAdapter);
			
			return true;
		}
		
		private void handleKeywordEdit(String keyword, int pos, SingleSelectableItemCompositeAdapter<Cursor> adapter) {
			ActionMode.Callback callbacks = new EditCategoryActionMode("Keyword", keyword, pos, adapter){
				@Override
				protected boolean renameCategory(String keyword, String rename) throws Exception {
					Cursor c = bibtexAdapter.getEntriesForKeyword(keyword);
					int entryCnt = c.getCount();
					new RenameKeywordTransaction(getActivity(), bibtexAdapter, keyword, rename).execute(c);
					bibtexAdapter.deleteKeyword(keyword);
					
					NotificationUtils.longToast(getActivity(), String.format("Renamed keyword in %d entries", entryCnt));
					return true;
				}
				@Override
				protected void deleteCategory(String keyword) throws Exception {
					Cursor c = bibtexAdapter.getEntriesForKeyword(keyword);
					int entryCnt = c.getCount();
					new RemoveKeywordTransaction(getActivity(), bibtexAdapter, keyword).execute(c);
					bibtexAdapter.deleteKeyword(keyword);
					
					NotificationUtils.longToast(getActivity(), String.format("Removed keyword from %d entries", entryCnt));
				}
			};
			
			clearSelections();
			startActionMode(ACTION_MODE_EDITING, "Keyword: " + keyword, callbacks);
		}
		
		private void handleCollectionEdit(String collection, int pos, SingleSelectableItemCompositeAdapter<Cursor> adapter) {
			ActionMode.Callback callbacks = new EditCategoryActionMode("Collection", collection, pos, adapter){
				@Override
				protected boolean renameCategory(String collection, String rename) throws Exception {
					Cursor c = bibtexAdapter.getEntriesForCollection(collection);
					int entryCnt = c.getCount();
					new AssignCollectionTransaction(getActivity(), bibtexAdapter, rename).execute(c);
					bibtexAdapter.deleteCollection(collection);
					
					NotificationUtils.longToast(getActivity(), String.format("Renamed collection in %d entries", entryCnt));
					return true;
				}
				@Override
				protected void deleteCategory(String collection) throws Exception {
					Cursor c = bibtexAdapter.getEntriesForCollection(collection);
					int entryCnt = c.getCount();
					new AssignCollectionTransaction(getActivity(), bibtexAdapter, "").execute(c);
					bibtexAdapter.deleteCollection(collection);
					
					NotificationUtils.longToast(getActivity(), String.format("Removed collection from %d entries", entryCnt));
				}
			};
			
			clearSelections();
			startActionMode(ACTION_MODE_EDITING, "Collection: " + collection, callbacks);
		}
		
		private void handleGroupEdit(String group, int pos, SingleSelectableItemCompositeAdapter<Cursor> adapter) {
			if(group.equals(BibTeXContentProviderAdapter.NO_GROUP_FIELD_TITLE)){
				clearSelections();
				return;
			}
			
			ActionMode.Callback callbacks = new EditCategoryActionMode("Group", group, pos, adapter){
				@Override
				protected boolean renameCategory(String group, String rename) throws Exception {
					Cursor c = bibtexAdapter.getEntriesForGroup(group);
					int cnt = c.getCount();
					new RenameGroupTransaction(getActivity(), bibtexAdapter, group, rename).execute(c);
					bibtexAdapter.deleteGroup(group);
					
					NotificationUtils.longToast(getActivity(), String.format("Renamed group in %d entries", cnt));
					return true;
				}
				@Override
				protected void deleteCategory(String group) throws Exception {
					Cursor c = bibtexAdapter.getEntriesForGroup(group);
					int cnt = c.getCount();
					new RemoveGroupTransaction(getActivity(), bibtexAdapter, group).execute(c);
					bibtexAdapter.deleteGroup(group);
					
					NotificationUtils.longToast(getActivity(), String.format("Removed group from %d entries", cnt));
				}
			};
			
			clearSelections();
			startActionMode(ACTION_MODE_EDITING, "Group: " + group, callbacks);
		}
		
		public boolean onSectionLongClick(AdapterView<?> adapterView, View view, int section, long id) {
			return false;
		}
	}
	
	private abstract class EntryDropTargetListener implements DropTargetAdapter.DropTargetListener<Cursor> {
		public boolean start(int pos, Cursor item, View v, DragEvent event) {
			return EntryListFragment.ENTRY_CLIP_LABEL.equals(event.getClipDescription().getLabel());
		}
		public boolean enter(int pos, Cursor item, View v, DragEvent event) {
			((Checkable)v.findViewById(R.id.groupListItem)).setChecked(true);
			return true;
		}
		public boolean exit(int pos, Cursor item, View v, DragEvent event) {
			((Checkable)v.findViewById(R.id.groupListItem)).setChecked(false);
			return true;
		}
		public boolean ended(int pos, Cursor item, View v, DragEvent event) {
			((Checkable)v.findViewById(R.id.groupListItem)).setChecked(false);
			return true;
		}
	}
	
	private class DBEntryDropTargetListener implements DropTargetAdapter.DropTargetListener<String> {
		public boolean start(int pos, String item, View v, DragEvent event) {
			return EntryListFragment.ENTRY_CLIP_LABEL.equals(event.getClipDescription().getLabel());
		}
		public boolean enter(int pos, String item, View v, DragEvent event) {
			((Checkable)v.findViewById(R.id.groupListItem)).setChecked(true);
			return true;
		}
		public boolean exit(int pos, String item, View v, DragEvent event) {
			((Checkable)v.findViewById(R.id.groupListItem)).setChecked(false);
			return true;
		}
		public boolean ended(int pos, String item, View v, DragEvent event) {
			((Checkable)v.findViewById(R.id.groupListItem)).setChecked(false);
			return true;
		}
		public boolean drop(int pos, String item, View v, DragEvent event) {
			if(DATABASE_LIBRARY.equals(item)){
				return false;
			}else if(DATABASE_IMPORTANT.equals(item)){
				assignImportant(event);
			}else if(DATABASE_UNREAD.equals(item)){
				assignUnread(event);
			}
			return true;
		}
		private void assignUnread(DragEvent event) {
			try{
				List<String> refKeys = getRefKeysFromClip(event);
				new MarkReadTransaction(getActivity(), bibtexAdapter, false).executeOnKeys(refKeys);
				NotificationUtils.longToast(getActivity(), String.format(Locale.US, "Marked %d entries unread", refKeys.size()));
			}catch(Exception e){
				NotificationUtils.notifyException(getActivity(), "Assign Group Failed", e);
			}
		}
		private void assignImportant(DragEvent event) {
			try{
				List<String> refKeys = getRefKeysFromClip(event);
				new ToggleImportantTransaction(getActivity(), bibtexAdapter, fileAdapter);
				NotificationUtils.longToast(getActivity(), String.format(Locale.US, "Marked %d entries important", refKeys.size()));
			}catch(Exception e){
				NotificationUtils.notifyException(getActivity(), "Assign Group Failed", e);
			}
		}
	}
	
	private class GroupDropListener extends EntryDropTargetListener {
		public boolean drop(int pos, Cursor item, View v, DragEvent event) {
			String group = item.getString( item.getColumnIndex( DBHelper.COLUMN_GROUP ));
			if(!group.equals(BibTeXContentProviderAdapter.NO_GROUP_FIELD_TITLE)) {
				try{
					List<String> refKeys = getRefKeysFromClip(event);
					new AddGroupTransaction(getActivity(), bibtexAdapter, group).executeOnKeys(refKeys);
					NotificationUtils.longToast(getActivity(), String.format(Locale.US, "Assigned %d entries to group '%s'", refKeys.size(), group));
				}catch(Exception e){
					NotificationUtils.notifyException(getActivity(), "Assign Group Failed", e);
				}
			}
			return true;
		}
	}
	
	private class CollectionDropListener extends EntryDropTargetListener {
		public boolean drop(int pos, Cursor item, View v, DragEvent event) {
			String collection = item.getString( item.getColumnIndex( DBHelper.COLUMN_COLLECTION ));
			try{
				List<String> refKeys = getRefKeysFromClip(event);
				new AssignCollectionTransaction(getActivity(), bibtexAdapter, collection).executeOnKeys(refKeys);
				NotificationUtils.longToast(getActivity(), String.format(Locale.US, "Moved %d entries to collection: '%s'", refKeys.size(), collection));
			}catch(Exception e){
				NotificationUtils.notifyException(getActivity(), "Assign Collection Failed", e);
			}
			return true;
		}
	}
	
	private class KeywordDropListener extends EntryDropTargetListener {
		public boolean drop(int pos, Cursor item, View v, DragEvent event) {
			String keyword = item.getString( item.getColumnIndex( DBHelper.COLUMN_KEYWORD ));
			try{
				List<String> refKeys = getRefKeysFromClip(event);
				new AddKeywordTransaction(getActivity(), bibtexAdapter, keyword).executeOnKeys(refKeys);
				NotificationUtils.longToast(getActivity(), String.format(Locale.US, "Assigned keyword '%s' to %d entries", keyword, refKeys.size()));
			}catch(Exception e){
				NotificationUtils.notifyException(getActivity(), "Assign Keyword Failed", e);
			}
			return true;
		}
	}
	
	private class FilterDropListener extends EntryDropTargetListener {
		public boolean drop(int pos, Cursor item, View v, DragEvent event) {
			return false;
		}
	}
	
	private Callbacks mCallbacks;
	private PinnedHeaderListView listView;
	private Callbacks sDummyCallbacks;
	
	private HeaderListCompositingAdapter headerAdapter;
	private CategoryCountAdapter dbAdapter;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private FileRevisionContentProviderAdapter fileAdapter;

	public ActionMode mActionMode;
	private int mActionModeType;
	
	private SimpleCursorAdapter filterCursorAdapter;
	private SimpleCursorAdapter groupsCursorAdapter;
	private SimpleCursorAdapter collectionsCursorAdapter;
	private SimpleCursorAdapter keywordsCursorAdapter;
	
	private DropTargetAdapter<String> dbDropAdapter;
	private DropTargetAdapter<Cursor> filterDropAdapter;
	private DropTargetAdapter<Cursor> groupsDropAdapter;
	private DropTargetAdapter<Cursor> collectionsDropAdapter;
	private DropTargetAdapter<Cursor> keywordsDropAdapter;

	private SelectableItemCompositeAdapter<String> dbSelectAdapter;
	private SelectableItemCompositeAdapter<Cursor> filterSelectAdapter;
	private SelectableItemCompositeAdapter<Cursor> groupsSelectAdapter;
	private SelectableItemCompositeAdapter<Cursor> collectionsSelectAdapter;
	private SelectableItemCompositeAdapter<Cursor> keywordsSelectAdapter;

	private StatefulItemAdapter<String, Integer> dbChosenAdapter;
	private StatefulItemAdapter<Cursor, Integer> filterChosenAdapter;
	private StatefulItemAdapter<Cursor, Integer> groupsChosenAdapter;
	private StatefulItemAdapter<Cursor, Integer> collectionsChosenAdapter;
	private StatefulItemAdapter<Cursor, Integer> keywordsChosenAdapter;

	private SingleSelectableItemCompositeAdapter<Cursor> groupsEditAdapter;
	private SingleSelectableItemCompositeAdapter<Cursor> collectionsEditAdapter;
	private SingleSelectableItemCompositeAdapter<Cursor> keywordsEditAdapter;
	
	private CategoryCursorAdapter filterAdapter;
	private CategoryCursorAdapter groupsAdapter;
	private CategoryCursorAdapter collectionsAdapter;
	private CategoryCursorAdapter keywordsAdapter;
	
	public EntryGroupFragment(){
		sDummyCallbacks = new Callbacks(){
			public void groupItemSelected(String category, String item) { }
			public void groupItemDeselected() { }
			public void groupSelected(String category) { }
			public void applyFilter(QueryElement query) { }
			public boolean isLarge() {return false;}
		};
		mCallbacks = sDummyCallbacks;
	}
	
	public void clearChosenElements() {
		keywordsChosenAdapter.setAllStates(GroupListItem.STATE_NONE);
		collectionsChosenAdapter.setAllStates(GroupListItem.STATE_NONE);
		groupsChosenAdapter.setAllStates(GroupListItem.STATE_NONE);
		filterChosenAdapter.setAllStates(GroupListItem.STATE_NONE);
		dbChosenAdapter.setAllStates(GroupListItem.STATE_NONE);
	}
	
	public void setChosenElements() {
		clearChosenElements();
		for(int pos : keywordsSelectAdapter.getSelectedPositions()){
			keywordsChosenAdapter.setState(pos, GroupListItem.STATE_CHOSEN);
		}
		for(int pos : collectionsSelectAdapter.getSelectedPositions()){
			collectionsChosenAdapter.setState(pos, GroupListItem.STATE_CHOSEN);
		}
		for(int pos : groupsSelectAdapter.getSelectedPositions()){
			groupsChosenAdapter.setState(pos, GroupListItem.STATE_CHOSEN);
		}
		for(int pos : filterSelectAdapter.getSelectedPositions()){
			filterChosenAdapter.setState(pos, GroupListItem.STATE_CHOSEN);
		}
		Set<Integer> selectedItems = dbSelectAdapter.getSelectedPositions();
		
		for(int pos : selectedItems){
			String category = (String) dbAdapter.getItem(pos);
			if(category.equals(DATABASE_IMPORTANT))
				dbChosenAdapter.setState(pos, GroupListItem.STATE_CHOSEN);
			if(category.equals(DATABASE_UNREAD))
				dbChosenAdapter.setState(pos, GroupListItem.STATE_CHOSEN);
		}
	}

	public void clearSelections() {
		dbSelectAdapter.deselectAll();
		filterSelectAdapter.deselectAll();
		groupsSelectAdapter.deselectAll();
		collectionsSelectAdapter.deselectAll();
		keywordsSelectAdapter.deselectAll();
	}

	public List<QueryElement> getSelectedSubQueries() {
		List<QueryElement> elements = new ArrayList<QueryElement>();
		try{
			getSelectDBItems(elements);
			getSelectedFilters(elements);
			getSelectedGroups(elements);
			getSelectedCollections(elements);
			getSelectedKeywords(elements);
		}catch(IOException e){
			NotificationUtils.notifyException(getActivity(), "Unexpected Exception", e);
		}
		return elements;
	}

	private void getSelectedKeywords(List<QueryElement> elements) {
		for(int pos : keywordsSelectAdapter.getSelectedPositions()){
			Cursor c = (Cursor) keywordsCursorAdapter.getItem(pos);
			elements.add( new QueryElementHasKeyword( c.getString( c.getColumnIndex( DBHelper.COLUMN_KEYWORD ))) );
		}
	}

	private void getSelectedCollections(List<QueryElement> elements) {
		for(int pos : collectionsSelectAdapter.getSelectedPositions()){
			Cursor c = (Cursor) collectionsCursorAdapter.getItem(pos);
			elements.add( new QueryElementInCollection( c.getString( c.getColumnIndex( DBHelper.COLUMN_COLLECTION ))) );
		}
	}

	private void getSelectedGroups(List<QueryElement> elements) {
		for(int pos : groupsSelectAdapter.getSelectedPositions()){
			Cursor c = (Cursor) groupsSelectAdapter.getItem(pos);
			elements.add( new QueryElementInGroup( c.getString( c.getColumnIndex( DBHelper.COLUMN_GROUP ))) );
		}
	}

	private void getSelectedFilters(List<QueryElement> elements) throws IOException {
		for(int pos : filterSelectAdapter.getSelectedPositions()){
			Cursor c = (Cursor) filterSelectAdapter.getItem(pos);
			elements.add( bibtexAdapter.cursorToQuery(c) );
		}
	}

	private void getSelectDBItems(List<QueryElement> elements) {
		Set<Integer> selectedItems = dbSelectAdapter.getSelectedPositions();
		
		for(int pos : selectedItems){
			String category = (String) dbAdapter.getItem(pos);
			if(category.equals(DATABASE_IMPORTANT))
				elements.add(new QueryElementIsImportant() );
			if(category.equals(DATABASE_UNREAD))
				elements.add(new QueryElementIsUnread() );
		}
	}
	
	public void startActionMode(int type, String title, ActionMode.Callback callbacks){
		if(mActionMode != null)
			mActionMode.finish();
		
		mActionModeType = type; 
		mActionMode = getActivity().startActionMode(callbacks);
		mActionMode.setTitle(title);
	}
	
	public void finishActionMode(boolean clear, boolean reset){
		mActionModeType = ACTION_MODE_NONE;
		
		if(reset)
			listView.setItemChecked(mActivePosition, false);
		if(clear)
			clearSelections();
		
		if(mActionMode != null)
			mActionMode.finish();
	}
	
	public void setSelectionHandler(Callbacks callbacks) {
		this.mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
		mActivePosition = 1;
		
		bibtexAdapter = new BibTeXContentProviderAdapter(getActivity().getContentResolver());
		fileAdapter = new FileRevisionContentProviderAdapter(getActivity().getContentResolver());
		
		dbAdapter = new CategoryCountAdapter(getActivity(), R.layout.library_group, R.id.groupIcon, R.id.groupLabel, R.id.groupCount);
		dbAdapter.addCategory(DATABASE_LIBRARY, R.drawable.category_library, 0);
		dbAdapter.addCategory(DATABASE_UNREAD, R.drawable.category_unread, 0);
		dbAdapter.addCategory(DATABASE_IMPORTANT, R.drawable.category_important, 0);
		
	    int[] uiBindTo = { R.id.groupLabel };
	    
	    filterCursorAdapter = new SimpleCursorAdapter(
	            getActivity().getBaseContext(), R.layout.library_group,
	            null, new String[]{ DBHelper.COLUMN_TITLE }, uiBindTo,
	            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
	    
	    groupsCursorAdapter = new SimpleCursorAdapter(
	            getActivity().getBaseContext(), R.layout.library_group,
	            null, new String[]{ DBHelper.COLUMN_GROUP }, uiBindTo,
	            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
	    collectionsCursorAdapter = new SimpleCursorAdapter(
	            getActivity().getBaseContext(), R.layout.library_group,
	            null, new String[]{ DBHelper.COLUMN_COLLECTION }, uiBindTo,
	            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
	    keywordsCursorAdapter = new SimpleCursorAdapter(
	            getActivity().getBaseContext(), R.layout.library_group,
	            null, new String[]{ DBHelper.COLUMN_KEYWORD }, uiBindTo,
	            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
	    
	    dbSelectAdapter = new SelectableItemCompositeAdapter<String>(R.id.groupListItem, R.id.groupIcon, new MultiSelectListener<String>(), dbAdapter);
	    filterSelectAdapter = new SelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, R.id.groupIcon, new MultiSelectListener<Cursor>(), filterCursorAdapter);
	    groupsSelectAdapter = new SelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, R.id.groupIcon, new MultiSelectListener<Cursor>(), groupsCursorAdapter);
	    collectionsSelectAdapter = new SelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, R.id.groupIcon, new MultiSelectListener<Cursor>(), collectionsCursorAdapter);
	    keywordsSelectAdapter = new SelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, R.id.groupIcon, new MultiSelectListener<Cursor>(), keywordsCursorAdapter);
	    
	    dbDropAdapter = new DropTargetAdapter<String>(dbSelectAdapter, new DBEntryDropTargetListener());
	    filterDropAdapter = new DropTargetAdapter<Cursor>(filterSelectAdapter, new FilterDropListener());
	    groupsDropAdapter = new DropTargetAdapter<Cursor>(groupsSelectAdapter, new GroupDropListener());
	    collectionsDropAdapter = new DropTargetAdapter<Cursor>(collectionsSelectAdapter, new CollectionDropListener());
	    keywordsDropAdapter = new DropTargetAdapter<Cursor>(keywordsSelectAdapter, new KeywordDropListener());
	    
	    dbChosenAdapter = new StatefulItemAdapter<String, Integer>(R.id.groupListItem, new ChosenStateListener<String>(), dbDropAdapter);
	    filterChosenAdapter = new StatefulItemAdapter<Cursor, Integer>(R.id.groupListItem, new ChosenStateListener<Cursor>(), filterDropAdapter);
	    groupsChosenAdapter = new StatefulItemAdapter<Cursor, Integer>(R.id.groupListItem, new ChosenStateListener<Cursor>(), groupsDropAdapter);
	    collectionsChosenAdapter = new StatefulItemAdapter<Cursor, Integer>(R.id.groupListItem, new ChosenStateListener<Cursor>(), collectionsDropAdapter);
	    keywordsChosenAdapter = new StatefulItemAdapter<Cursor, Integer>(R.id.groupListItem, new ChosenStateListener<Cursor>(), keywordsDropAdapter);
	    
	    groupsEditAdapter = new SingleSelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, new SingleItemSelectListener<Cursor>(), groupsChosenAdapter);
	    collectionsEditAdapter = new SingleSelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, new SingleItemSelectListener<Cursor>(), collectionsChosenAdapter);
	    keywordsEditAdapter = new SingleSelectableItemCompositeAdapter<Cursor>(R.id.groupListItem, new SingleItemSelectListener<Cursor>(), keywordsChosenAdapter);
	    
	    filterAdapter = new CategoryCursorAdapter(getActivity(), R.id.groupIcon, R.id.groupCount, R.drawable.category_filter, DBHelper.COLUMN_TITLE, null, filterChosenAdapter);
	    groupsAdapter = new CategoryCursorAdapter(getActivity(), R.id.groupIcon, R.id.groupCount, R.drawable.category_group, DBHelper.COLUMN_GROUP, DBHelper.COLUMN_COUNT, groupsEditAdapter);
	    collectionsAdapter = new CategoryCursorAdapter(getActivity(), R.id.groupIcon, R.id.groupCount, R.drawable.category_collection, DBHelper.COLUMN_COLLECTION, DBHelper.COLUMN_COUNT, collectionsEditAdapter);
	    keywordsAdapter = new CategoryCursorAdapter(getActivity(), R.id.groupIcon, R.id.groupCount, R.drawable.category_keyword, DBHelper.COLUMN_KEYWORD, DBHelper.COLUMN_COUNT, keywordsEditAdapter);
	    
		headerAdapter = new HeaderListCompositingAdapter(getActivity(), R.layout.library_category, R.id.header);
		headerAdapter.addSection(CATEGORY_DATABASE, dbChosenAdapter);
		headerAdapter.addSection(CATEGORY_FILTERS, filterAdapter);
		headerAdapter.addSection(CATEGORY_GROUPS, groupsAdapter);
		headerAdapter.addSection(CATEGORY_COLLECTIONS, collectionsAdapter);
		headerAdapter.addSection(CATEGORY_KEYWORDS, keywordsAdapter);
		
		getLoaderManager().initLoader(ALL_LOADER, null, new LibraryLoaderListener());
		getLoaderManager().initLoader(UNREAD_LOADER, null, new UnreadLoaderListener());
		getLoaderManager().initLoader(IMPORTANT_LOADER, null, new ImportantLoaderListener());
		getLoaderManager().initLoader(GROUP_LOADER, null, new GroupLoaderListener());
		getLoaderManager().initLoader(COLLECTIONS_LOADER, null, new CollectionsLoaderListener());
		getLoaderManager().initLoader(KEYWORD_LOADER, null, new KeywordLoaderListener());
		getLoaderManager().initLoader(FILTER_LOADER, null, new FilterLoaderListener());
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.group_list_fragment, container, false);
		listView = (PinnedHeaderListView) rootView.findViewById(R.id.libraryGroups);
		listView.setOnItemClickListener(new ItemClickListener());
		listView.setOnItemLongClickListener(new ItemLongClickListener());
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setAdapter(headerAdapter);
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		if(mCallbacks.isLarge())
			listView.performItemClick(headerAdapter.getView(mActivePosition, null, null), mActivePosition, headerAdapter.getItemId(1));
	}
	
	@Override
	public void onResume(){
		super.onResume();
		getLoaderManager().restartLoader(ALL_LOADER, null, new LibraryLoaderListener());
		getLoaderManager().restartLoader(UNREAD_LOADER, null, new UnreadLoaderListener());
		getLoaderManager().restartLoader(IMPORTANT_LOADER, null, new ImportantLoaderListener());
		getLoaderManager().restartLoader(GROUP_LOADER, null, new GroupLoaderListener());
		getLoaderManager().restartLoader(COLLECTIONS_LOADER, null, new CollectionsLoaderListener());
		getLoaderManager().restartLoader(KEYWORD_LOADER, null, new KeywordLoaderListener());
		getLoaderManager().restartLoader(FILTER_LOADER, null, new FilterLoaderListener());
	}
	
	public void onResetSelection(){
		mActivePosition = 1;
		listView.setItemChecked(mActivePosition, true);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException( "Activity must implement fragment's callbacks." );
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = sDummyCallbacks;
	}
	
	private class LibraryLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadAllEntries(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			dbAdapter.setCategoryCount(DATABASE_LIBRARY, cursor.getCount());
			dbAdapter.notifyDataSetChanged();
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			
		}
	}
	
	private class ImportantLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadImportantEntries(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			dbAdapter.setCategoryCount(DATABASE_IMPORTANT, cursor.getCount());
			dbAdapter.notifyDataSetChanged();
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			
		}
	}
	
	private class UnreadLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadUnreadEntries(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			dbAdapter.setCategoryCount(DATABASE_UNREAD, cursor.getCount());
			dbAdapter.notifyDataSetChanged();
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			
		}
	}
	
	private class FilterLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadFilters(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			filterCursorAdapter.swapCursor(cursor);
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			filterCursorAdapter.swapCursor(null);
		}
	}
	
	private class GroupLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadGroups(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			groupsCursorAdapter.swapCursor(cursor);
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			groupsCursorAdapter.swapCursor(null);
		}
	}

	private class CollectionsLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadCollections(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			collectionsCursorAdapter.swapCursor(cursor);
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			collectionsCursorAdapter.swapCursor(null);
		}
	}

	private class KeywordLoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
			return bibtexAdapter.loadKeywords(getActivity());
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cLoader, Cursor cursor) {
			keywordsCursorAdapter.swapCursor(cursor);
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cLoader) {
			keywordsCursorAdapter.swapCursor(null);
		}
	}

	public void cancelAction() {
		if(mActionMode != null)
			mActionMode.finish();
	}

	private List<String> getRefKeysFromClip(DragEvent event) {
		List<String> refKeys = new ArrayList<String>(event.getClipData().getItemCount());
		for(int i = 0; i < event.getClipData().getItemCount(); i++){
			String refKey = event.getClipData().getItemAt(i).getText().toString();
			refKeys.add(refKey);
		}
		return refKeys;
	}
}
