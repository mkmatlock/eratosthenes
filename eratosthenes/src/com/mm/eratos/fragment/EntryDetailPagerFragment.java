package com.mm.eratos.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mm.eratos.R;
import com.mm.eratos.adapter.EntryDetailPagerAdapter;

public class EntryDetailPagerFragment extends Fragment implements OnClickListener, OnPageChangeListener {
	
	public interface EntryPagerCallbacks {
		public void activatePageView(int viewId);
		public void pagerViewCreated();
		public void onPageSelected(int page);
		public int getActivatedPageView();
	}
	
	private ViewPager mPager;

	private Button notesPageShowButton;
	private Button filesPageShowButton;
	private Button fieldsPageShowButton;
	
	private EntryPagerCallbacks mCallbacks;

	private LinearLayout pageTabSelector;

	private boolean creating = false;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		creating = true;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mCallbacks = (EntryPagerCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = new EntryPagerCallbacks() {
			public void pagerViewCreated() {
			}
			public void activatePageView(int viewId) {
			}
			public void onPageSelected(int page) {
			}
			public int getActivatedPageView() {
				return 0;
			}
		};
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_entry_detail, container, false);
		mPager = (ViewPager) rootView.findViewById(R.id.entry_detail_pager_view);
		mPager.setOnPageChangeListener(this);
		
		pageTabSelector = (LinearLayout) rootView.findViewById(R.id.entry_tabs);
		notesPageShowButton = (Button) rootView.findViewById(R.id.show_notes_button);
		filesPageShowButton = (Button) rootView.findViewById(R.id.show_files_button);
		fieldsPageShowButton = (Button) rootView.findViewById(R.id.show_fields_button);
		
		notesPageShowButton.setOnClickListener(this);
		filesPageShowButton.setOnClickListener(this);
		fieldsPageShowButton.setOnClickListener(this);
		
		setPageView(mCallbacks.getActivatedPageView());
		
	    return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(creating){
			mCallbacks.pagerViewCreated();
			creating = false;
		}
	}
	
	public void setPageView(int viewActivated) {
		if(viewActivated == R.id.notes_page_view){
			notesPageShowButton.setActivated(true);
		}else{
			notesPageShowButton.setActivated(false);
		}
		
		if(viewActivated == R.id.files_page_view){
			filesPageShowButton.setActivated(true);
		}else{
			filesPageShowButton.setActivated(false);
		}
		
		if(viewActivated == R.id.fields_page_view){
			fieldsPageShowButton.setActivated(true);
		}else{
			fieldsPageShowButton.setActivated(false);
		}
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == notesPageShowButton.getId()){
			mCallbacks.activatePageView(R.id.notes_page_view);
		} else if(v.getId() == filesPageShowButton.getId()){
			mCallbacks.activatePageView(R.id.files_page_view);
		} else if(v.getId() == fieldsPageShowButton.getId()){
			mCallbacks.activatePageView(R.id.fields_page_view);
		} 
	}

	public void setCurrentItem(int pos) {
		mPager.setCurrentItem(pos);
	}
	
	public void show() {
		mPager.setVisibility(View.VISIBLE);
		pageTabSelector.setVisibility(View.VISIBLE);
	}
	
	public void hide() {
		mPager.setVisibility(View.INVISIBLE);
		pageTabSelector.setVisibility(View.INVISIBLE);
	}

	public void onPageScrollStateChanged(int arg0) {
		
	}

	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}

	public void onPageSelected(int page) {
		mCallbacks.onPageSelected(page);
	}

	public void setAdapter(EntryDetailPagerAdapter pagerAdapter) {
		mPager.setAdapter(pagerAdapter);
	}
};