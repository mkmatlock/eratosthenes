package com.mm.eratos.fragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jbibtex.Key;
import org.jbibtex.ParseException;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.activity.EditEntryActivity;
import com.mm.eratos.activity.EntryListActivity;
import com.mm.eratos.activity.FileBrowserActivity;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.adapter.MapAdapter;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.operations.AddAttachmentTransaction;
import com.mm.eratos.bibtex.operations.AddKeywordTransaction;
import com.mm.eratos.bibtex.operations.AddUrlTransaction;
import com.mm.eratos.bibtex.operations.EntryWebUpdateTransaction;
import com.mm.eratos.bibtex.operations.MoveFieldTransaction;
import com.mm.eratos.bibtex.operations.RemoveFieldTransaction;
import com.mm.eratos.bibtex.operations.RemoveFileTransaction;
import com.mm.eratos.bibtex.operations.RemoveKeywordTransaction;
import com.mm.eratos.bibtex.operations.RemoveUrlTransaction;
import com.mm.eratos.bibtex.operations.RenameFileTransaction;
import com.mm.eratos.bibtex.operations.SetFieldTransaction;
import com.mm.eratos.bibtex.operations.ToggleImportantTransaction;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.dialog.BibtexEditorDialog;
import com.mm.eratos.dialog.BibtexEditorDialog.BibtexEditorListener;
import com.mm.eratos.dialog.AutocompleteTextEntryDialog;
import com.mm.eratos.dialog.FieldEditorDialog;
import com.mm.eratos.dialog.FieldRawEditorDialog;
import com.mm.eratos.exceptions.FileAlreadyExistsException;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.WebHandlerFactory;
import com.mm.eratos.files.FileCacheListener;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.files.IUriResultListener;
import com.mm.eratos.loader.BibTeXEntryLoader;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.search.ISearchHandler;
import com.mm.eratos.search.SearchFactory;
import com.mm.eratos.search.db.QueryElementHasKeyword;
import com.mm.eratos.task.AlterEntryTask;
import com.mm.eratos.utils.IStringCheckboxResultListener;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.InputUtils;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebUtils;
import com.mm.eratos.view.LinearListView;
import com.mm.eratos.view.WrappingListView;


/**
 * A fragment representing a single Entry detail screen. This fragment is either
 * contained in a {@link EntryListActivity} in two-pane mode (on tablets) or a
 * {@link EntryDetailActivity} on handsets.
 */
public class EntryDetailFragment extends Fragment implements OnClickListener, OnLongClickListener, LoaderCallbacks<BibTeXEntryModel>
{
	public interface Callbacks {
		public void selectEntry(String refKey);
		public int getActivatedPageView();
	}
	
	private class SourceIconCallback implements ItemActionCallback<EratosUri>{
		@Override
		public void perform(int position, EratosUri uri, View v) {
			
		}
		@Override
		public void initialize(int position, EratosUri uri, View v) {
			File f = FileManager.getFileManager().getFile(uri);
			int drawable = getDrawableForSource(f);
			
			((TextView)v).setCompoundDrawablesWithIntrinsicBounds( drawable, 0, 0, 0 );
			
			v.setClickable(false);
		}
		
		private int getDrawableForSource(File f) {
			if(f.exists())
				return R.drawable.ic_action_view_as_list;
			return R.drawable.ic_action_cloud;
		}
	}
	
	private class KeywordSearchListener implements ItemActionCallback<String>{
		public void perform(int position, String keyword, View v) {
			Intent i = new Intent(getActivity(), EntryListActivity.class);
			i.putExtra(EntryListActivity.QUERY, new QueryElementHasKeyword(keyword));
			startActivity(i);
		}
		public void initialize(int position, String item, View v) {
			
		}
	}
	
	private class RemoveKeywordListener implements ItemActionCallback<String>{
		public void perform(int position, final String keyword, View v) {
			NotificationUtils.confirmDialog(getActivity(), 
						"Remove Keyword?", 
						String.format("Remove keyword '%s'?", keyword), 
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								removeKeyword(keyword);
							}
						});
		}
		
		private void removeKeyword(String keyword) {
			try {
				new RemoveKeywordTransaction(getActivity(), contentAdapter, keyword).execute(entry);
			} catch(Exception e) {
				NotificationUtils.notify(getActivity(), "Error", "Field parser error: " + e.getMessage());
			}
		}
		
		public void initialize(int position, String item, View v) {
			
		}
	}
	
	private class KeywordAddListener implements AutocompleteTextEntryDialog.OnFinishedListener {
		@Override
		public boolean onConfirm(String result) {
			try {
				new AddKeywordTransaction(getActivity(), contentAdapter, result).execute(entry);
				return true;
			} catch(Exception e) {
				NotificationUtils.notify(getActivity(), "Error", "Field parser error: " + e.getMessage());
			}
			return false;
		}

		@Override
		public void onCancel() {
			
		}
	}
	
	private ImageButton addField;
 	private class FieldEditListener implements FieldEditorDialog.OnFinishedListener {
		public boolean onConfirm(String field, String newValue) {
			try {
				new SetFieldTransaction(getActivity(), contentAdapter, field, newValue).execute(entry);
				return true;
			} catch(Exception e) {
				NotificationUtils.notify(getActivity(), "Error", "Field parser error: " + e.getMessage());
			}
			return false;
		}
		
		public void onCancel() {
			
		}
	}
 	
 	private class FieldRawEditListener implements FieldRawEditorDialog.OnFinishedListener {
		@Override
		public boolean onConfirm(Key oKey, Key nKey, org.jbibtex.Value newValue) {
			try {
				if(!oKey.equals(nKey))
					new MoveFieldTransaction(getActivity(), contentAdapter, oKey, nKey, newValue).execute(entry);
				else
					new SetFieldTransaction(getActivity(), contentAdapter, oKey.getValue(), newValue).execute(entry);
				return true;
			} catch(Exception e) {
				NotificationUtils.notify(getActivity(), "Error", "Field parser error: " + e.getMessage());
			}
			return false;
		}

		@Override
		public void onCancel() {
			
		}
 	}
 	
 	private class NewFieldListener implements FieldEditorDialog.OnFinishedListener {
		public boolean onConfirm(String field, String newValue) {
			if(entry.hasField(field)){
				NotificationUtils.notify(getActivity(), "Field Exists", "Field '"+field+"' already exists.");
				return false;
			}
			
			return changeField(field, newValue);
		}

		private boolean changeField(String field, String newValue) {
			try {
				new SetFieldTransaction(getActivity(), contentAdapter, field, newValue).execute(entry);
				return true;
			} catch(Exception e) {
				NotificationUtils.notify(getActivity(), "Field Parser Error", e.getMessage());
			}
			return false;
		}
		
		public void onCancel() {
			
		}
	}

	private static final int REQUEST_CHOOSE_ATTACHMENT = 11;

	private class FieldClickListener implements OnItemClickListener
	{
		public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
			String key = (String) fieldList.getItemAtPosition(pos);
			new FieldEditorDialog(getActivity(), entry, key, false, new FieldEditListener());
		}
	}
	
	private class FieldLongClickListener implements OnItemLongClickListener
	{
		@Override
		public boolean onItemLongClick(AdapterView<?> adapter, View v, int pos, long id) {
			String key = (String) fieldList.getItemAtPosition(pos);
			new FieldRawEditorDialog(getActivity(), entry, key, new FieldRawEditListener());
			return true;
		}
	}

	public class RemoveFieldListener implements ItemActionCallback<String>
	{
		@Override
		public void perform(int position, final String key, View v) {
			NotificationUtils.confirmDialog(getActivity(), "Remove field?", "Remove '" + key + "' field?",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							removeField(key);
						}
					});
		}

		private void removeField(String key) {
			try {
				new RemoveFieldTransaction(getActivity(), contentAdapter, key).execute(entry);
			} catch (Exception e) {
				NotificationUtils.notify(getActivity(), "Remove Field Failed", e.getMessage());
			}
		}

		@Override
		public void initialize(int position, String item, View v) {
			
		}
	}
	
	private class URLClickListener implements OnItemClickListener {
		public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
			EratosUri eratosUri = (EratosUri) externalLinks.getItemAtPosition(pos);
			Uri uri = WebUtils.formatExternalUriRequest(eratosUri.toString());
			Intent i = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(i);
		}
	}
	
	private class URLAddListener implements IStringResultListener {
		public boolean onResult(String url, DialogInterface dialog) {
			try {
				new AddUrlTransaction(getActivity(), contentAdapter, EratosUri.parseUri(url)).execute(entry);
				return true;
			} catch (Exception e) {
				NotificationUtils.notifyException(getActivity(), "Add URL Failed", e);
			}
			return false;
		}
	}
	
	private class URLRemoveListener implements ItemActionCallback<EratosUri> {
		@Override
		public void perform(int position, final EratosUri uri, View v) {
			NotificationUtils.confirmDialog(getActivity(), "Remove External Link?", "URL: " + uri.toString(),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							removeUrl(uri);
						}
					});
		}

		private void removeUrl(EratosUri uri) {
			try {
				new RemoveUrlTransaction(getActivity(), contentAdapter, uri).execute(entry);
			} catch (Exception e) {
				NotificationUtils.notifyException(getActivity(), "Remove URL Failed", e);
			}
		}

		@Override
		public void initialize(int position, EratosUri uri, View v) {
			if(entry.isParsedLink(uri)){
				v.setEnabled(false);
				v.setAlpha(0.2f);
			}
		}
	}
	
	private class URLParseListener implements ItemActionCallback<EratosUri> {
		@Override
		public void perform(int position, final EratosUri uri, View v) {
			AbstractWebHandler handler = WebHandlerFactory.getHandler(uri);
			try{
				EntryWebUpdateTransaction operator = new EntryWebUpdateTransaction(getActivity(), contentAdapter, handler, uri);
				new AlterEntryTask(getActivity(), "Parsing Web Links", operator).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, entry.getRefKey());
			} catch (Exception e) {
				NotificationUtils.notifyException(getActivity(), "Parse URL failed", e);
			}
		}

		@Override
		public void initialize(int position, EratosUri uri, View v) {
			AbstractWebHandler handler = WebHandlerFactory.getHandler(uri);
			
			if(handler == null){
				v.setEnabled(false);
				v.setVisibility(View.INVISIBLE);
			}else{
				v.setEnabled(true);
				v.setVisibility(View.VISIBLE);
			}
		}
	}
	
	private class FileClickListener implements OnItemClickListener
	{
		public void onItemClick(AdapterView<?> adapter, View v, int pos, long id)
		{
			EratosUri uri = ((EratosUri) attachments.getItemAtPosition(pos));
			uri = uri.resolveUri(EratosApplication.getApplication());
			FileManager.getFileManager().cacheFile(getActivity(), uri, new FileCacheListener(getActivity(), new IUriResultListener() {
				public boolean onResult(EratosUri result) {
					attachmentArrayAdapter.notifyDataSetChanged();
					return true;
				}
			}));
		}
	}
	
	private class FileLongClickListener implements OnItemLongClickListener
	{
		public boolean onItemLongClick(AdapterView<?> adapter, View v, int pos, long id)
		{
			final int filePos = pos;
			final EratosUri uri = ((EratosUri)attachments.getItemAtPosition(pos)).resolveUri(EratosApplication.getApplication());
			
			NotificationUtils.promptInput(getActivity(), "Rename Attachment", uri.name(), new IStringResultListener() {
				public boolean onResult(String result, DialogInterface dialog) {
					if(result.equals(uri.name()))
						NotificationUtils.longToast(getActivity(), "Please choose a new file name.");
					else
						renameFile(filePos, uri, result, dialog);
					return false;
				}
			}, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
			return true;
		}

		private void renameFile(int filePos, EratosUri uri, String result, DialogInterface dialog) {
			try {
				new RenameFileTransaction(getActivity(), contentAdapter, fileAdapter, filePos, uri, result).execute(entry);
				NotificationUtils.longToast(getActivity(), "Renamed " + uri.name() + " to " + result);
				dialog.dismiss();
			} catch (Exception e) {
				if(e instanceof FileAlreadyExistsException)
					NotificationUtils.longToast(getActivity(), "That filename is already in use.");
				else{
					NotificationUtils.notifyException(getActivity(), "File Rename Failed", e);
					dialog.dismiss();
				}
			}
		}
	}
	
	private class ParseAttachmentListener implements ItemActionCallback<EratosUri> {
		@Override
		public void perform(int position, final EratosUri uri, View v) {
		}

		@Override
		public void initialize(int position, EratosUri uri, View v) {
			v.setEnabled(false);
			v.setVisibility(View.GONE);
		}
	}
	
	public class RemoveAttachmentListener implements ItemActionCallback<EratosUri>, DialogInterface.OnClickListener
	{
		int removePosition = -1;

		@Override
		public void perform(int position, EratosUri uri, View v)
		{
			if (entry != null)
			{
				removePosition = position;
				String removeMessage = String.format("The attachment \"%s\" will be deleted.", uri.name());
				NotificationUtils.confirmDialog(getActivity(), "Remove attachment?", removeMessage, this);
			}
		}

		private void handleConfirmed(int position)
		{
			try
			{
				new RemoveFileTransaction(getActivity(), contentAdapter, position, fileAdapter).execute(entry);
				NotificationUtils.shortToast(getActivity(), "Attachment removed");
			}
			catch (IOException e) {
				e.printStackTrace();
				NotificationUtils.longToast(getActivity(), "Could not remove file, could not access file");
			}
			catch (ParseException e) {
				e.printStackTrace();
				NotificationUtils.longToast(getActivity(), "Could not remove file, parse error in bibtex entry");
			}catch(Exception e) {
				NotificationUtils.notifyException(getActivity(), "Unexpected Error", e);
			}
		}

		@Override
		public void onClick(DialogInterface dialog, int which)
		{
			switch (which)
			{
				case DialogInterface.BUTTON_POSITIVE:
					handleConfirmed(removePosition);
				case DialogInterface.BUTTON_NEGATIVE:
					break;
			}
		}

		@Override
		public void initialize(int position, EratosUri item, View v) {
			
		}
	}
	
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";
	private BibTeXContentProviderAdapter contentAdapter;
	private FileRevisionContentProviderAdapter fileAdapter;

	private RemoveAttachmentListener removeAttachmentListener;
	private URLRemoveListener removeExternalLinkListener;
	private URLParseListener parseExternalLinkListener;
	private ParseAttachmentListener parseAttachmentListener;
	
	private String entryKey;
	private BibTeXEntryModel entry;
		
	private ImageButton starEntry;
	private ImageButton editEntry;
	private ImageButton searchEntry;
	private ImageButton viewAttachment;
	
	private TextView typeView;
	private TextView nameView;
	private TextView collectionName;
	private TextView citationView;
	private TextView abstractView;
	private LinearLayout keywordLabel;
	private WrappingListView keywordsView;
	private ImageButton addKeywordButton;
	
	private LinearLayout notesPageContainer;
	private TextView notesView;
	private EditText notesEditView;
	private View notesEditButtonPanel;
	private ImageButton notesEditCancelButton;
	private ImageButton notesEditCommitButton;
	
	
	private LinearLayout filesPageContainer;
	private ImageButton addAttachment;
	private LinearListView attachments;
	private TextView noFilesNotificationTextView;
	private ImageButton addLink;
	private LinearListView externalLinks;
	private TextView noLinksNotificationTextView;
	
	private LinearLayout fieldsPageContainer;
	private LinearListView fieldList;
	
	private Callbacks mCallbacks;
	
//	private EratosLogger logger;
	private ContentObserver mContentObserver;
	private ArrayAdapter<EratosUri> attachmentArrayAdapter;
	private TextView groupsView;
	
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public EntryDetailFragment()
	{
		removeAttachmentListener = new RemoveAttachmentListener();
		removeExternalLinkListener = new URLRemoveListener();
		parseExternalLinkListener = new URLParseListener();
		parseAttachmentListener = new ParseAttachmentListener();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		contentAdapter = new BibTeXContentProviderAdapter(getActivity().getContentResolver());
		fileAdapter = new FileRevisionContentProviderAdapter(getActivity().getContentResolver());
//		logger = EratosApplication.getApplication().getLogManager().getLogger(EntryDetailFragment.class);
		
		mContentObserver = new ContentObserver(new Handler()) {
			@Override
			public boolean deliverSelfNotifications() {
				return true;
			}
			@Override
			public void onChange(boolean selfChange) {
				getLoaderManager().restartLoader(1, null, EntryDetailFragment.this);
			}
		};
		
		entry = null;
		entryKey = null;
		if (getArguments().containsKey(ARG_ITEM_ID))
			entryKey = getArguments().getString(ARG_ITEM_ID);
	}

	@Override
	public Loader<BibTeXEntryModel> onCreateLoader(int arg0, Bundle arg1) {
		return new BibTeXEntryLoader(getActivity(), entryKey);
	}

	@Override
	public void onLoadFinished(Loader<BibTeXEntryModel> loader, BibTeXEntryModel entry) {
		setContent(entry);
		((BibTeXEntryLoader)loader).registerContentObserver(mContentObserver);
	}

	@Override
	public void onLoaderReset(Loader<BibTeXEntryModel> loader) {
		setContent(null);
		((BibTeXEntryLoader)loader).unregisterContentObserver(mContentObserver);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException( "Activity must implement fragment's callbacks." );
		}

		mCallbacks = (Callbacks) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.entry_fragment, container, false);

		notesPageContainer = (LinearLayout) rootView.findViewById(R.id.notes_page_view);
		filesPageContainer = (LinearLayout) rootView.findViewById(R.id.files_page_view);
		fieldsPageContainer = (LinearLayout) rootView.findViewById(R.id.fields_page_view);
		
		typeView = (TextView) rootView.findViewById(R.id.typeTextView);
		nameView = (TextView) rootView.findViewById(R.id.nameTextView);
		abstractView = (TextView) rootView.findViewById(R.id.abstractTextView);
		abstractView.setOnClickListener(this);
		groupsView = (TextView) rootView.findViewById(R.id.groupsTextView);
		groupsView.setOnClickListener(this);
		notesView = (TextView) rootView.findViewById(R.id.notesTextView);
		notesView.setOnClickListener(this);
		
		notesEditView = (EditText) rootView.findViewById(R.id.notesEditTextView);
		notesEditCommitButton = (ImageButton) rootView.findViewById(R.id.doneEditingNotesButton);
		notesEditCommitButton.setOnClickListener(this);
		notesEditCancelButton = (ImageButton) rootView.findViewById(R.id.cancelEditingNotesButton);
		notesEditCancelButton.setOnClickListener(this);
		notesEditButtonPanel = rootView.findViewById(R.id.editNotesButtonPanel);
		
		notesEditView.setVisibility(View.GONE);
		notesEditButtonPanel.setVisibility(View.GONE);
		
		collectionName = ((TextView) rootView.findViewById(R.id.collectionTextView));
		keywordsView = (WrappingListView) rootView.findViewById(R.id.tagsView);
		
		LinearLayout addTagButtonContainer = (LinearLayout) inflater.inflate(R.layout.new_tag_button, keywordsView, false);
		addKeywordButton = (ImageButton) addTagButtonContainer.findViewById(R.id.newKeyword);
		addKeywordButton.setOnClickListener(this);
		
		keywordLabel = (LinearLayout) inflater.inflate(R.layout.tag_field_label, keywordsView, false);
		keywordsView.setBookends(keywordLabel, addTagButtonContainer);
		
		starEntry = ((ImageButton) rootView.findViewById(R.id.starEntryButton));
		starEntry.setOnClickListener(this);
		
		editEntry = ((ImageButton) rootView.findViewById(R.id.editEntryButton));
		editEntry.setOnClickListener(this);
		editEntry.setOnLongClickListener(this);
		
		searchEntry = ((ImageButton) rootView.findViewById(R.id.searchEntryButton));
		searchEntry.setOnClickListener(this);
		searchEntry.setOnLongClickListener(this);
		
		viewAttachment = ((ImageButton) rootView.findViewById(R.id.attachmentViewButton));
		viewAttachment.setOnClickListener(this);
		viewAttachment.setOnLongClickListener(this);
		
		citationView = (TextView) rootView.findViewById(R.id.citationTextView);
		citationView.setMovementMethod(LinkMovementMethod.getInstance());

		attachments = (LinearListView) rootView.findViewById(R.id.attachmentList);
		attachments.setOnItemClickListener(new FileClickListener());
		attachments.setOnItemLongClickListener(new FileLongClickListener());
		addAttachment = (ImageButton) rootView.findViewById(R.id.addAttachmentButton);
		addAttachment.setOnClickListener(this);
		noFilesNotificationTextView = (TextView) rootView.findViewById(R.id.noFilesTextView);
		
		externalLinks = (LinearListView) rootView.findViewById(R.id.linkList);
		externalLinks.setOnItemClickListener(new URLClickListener());
		addLink = (ImageButton) rootView.findViewById(R.id.addLinkButton);
		addLink.setOnClickListener(this);
		noLinksNotificationTextView = (TextView) rootView.findViewById(R.id.noLinksTextView);

		addField = (ImageButton) rootView.findViewById(R.id.addFieldButton);
		addField.setOnClickListener(this);

		attachments = (LinearListView) rootView.findViewById(R.id.attachmentList);
		
		attachments.setOnItemClickListener(new FileClickListener());
		
		noFilesNotificationTextView = (TextView) rootView.findViewById(R.id.noFilesTextView);
		
		fieldList = (LinearListView) rootView.findViewById(R.id.fieldsList);
		fieldList.setOnItemClickListener(new FieldClickListener());
		fieldList.setOnItemLongClickListener(new FieldLongClickListener());
		
		if(entryKey != null)
			getLoaderManager().initLoader(1, null, this);
		return rootView;
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisible){
		super.setUserVisibleHint(isVisible);
		
	    if (isVisible && this.isResumed())
	    	setPageView(mCallbacks.getActivatedPageView());
	}

	public void setPageView(int viewId) {
		if(viewId == notesPageContainer.getId()){
			notesPageContainer.setVisibility(View.VISIBLE);
		}else{
			notesPageContainer.setVisibility(View.GONE);
		}
		
		if(viewId == filesPageContainer.getId()){
			filesPageContainer.setVisibility(View.VISIBLE);
		}else{
			filesPageContainer.setVisibility(View.GONE);
		}
		
		if(viewId == fieldsPageContainer.getId()){
			fieldsPageContainer.setVisibility(View.VISIBLE);
		}else{
			fieldsPageContainer.setVisibility(View.GONE);
		}
	}
	
	private void setContent(BibTeXEntryModel entry) {		
		this.entry = entry;

		if (entry != null) {
			entryKey = entry.getRefKey();
			
			typeView.setText(StringUtils.capitalize(entry.getType()));
			nameView.setText(entry.getTitle());
			
			if(entry.important()){
				starEntry.setImageResource(R.drawable.rating_important);
			}else{
				starEntry.setImageResource(R.drawable.rating_not_important);
			}
			
			if(entry.getSummary().isEmpty()){
				abstractView.setText(Html.fromHtml("<b>Summary:</b> <font color=\"#999999\">Tap to edit...</font>"));
			}else{
				String summaryHtml = latexToHTML(entry.getSummary());
				abstractView.setText(Html.fromHtml("<b>Summary:</b><br />" + summaryHtml));
			}
			
			if(entry.getGroups().size()==0){
				groupsView.setText(Html.fromHtml("<b>Groups:</b> <font color=\"#999999\">Not in any groups...</font>"));
			}else{
				String groupsText = StringUtils.join(", ", entry.getGroups());
				groupsView.setText(Html.fromHtml("<b>Groups:</b> " + groupsText));
			}
			
			if(entry.getNotes().isEmpty()){
				notesView.setText(Html.fromHtml("<font color=\"#999999\">Tap to edit...</font>"));
			}else{
				String notesHtml = latexToHTML(entry.getNotes());
				notesView.setText(Html.fromHtml(notesHtml));
			}
			
			String collection = entry.getCollection();
			if(collection.equals("")){
				collectionName.setVisibility(View.GONE);
			}else{
				collectionName.setVisibility(View.VISIBLE);
				collectionName.setText(collection);
			}
			
			attachments.setAdapter(createAttachmentListAdapter());
			
			externalLinks.setAdapter(createLinkListAdapter());

			Adapter keywordAdapter = createKeywordAdapter(entry);
			keywordsView.setAdapter(keywordAdapter);
			
			BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
			citationView.setText(Html.fromHtml(entry.getHtmlCitation(helper)));
			
			fieldList.setAdapter(createFieldListAdapter());
			setPageView(mCallbacks.getActivatedPageView());
		}
	}

	private String latexToHTML(String latex) {
		return latex.replaceAll("\n", "<br />");
	}

	private Adapter createLinkListAdapter() {

		List<EratosUri> extLinks = entry.getExternalLinks();
		if (extLinks.size() > 0)
			noLinksNotificationTextView.setVisibility(View.GONE);
		else
			noLinksNotificationTextView.setVisibility(View.VISIBLE);
		
		ArrayAdapter<EratosUri> arrayAdapter = new ArrayAdapter<EratosUri>(getActivity(), R.layout.attachment_list_element, R.id.filename, extLinks);
		ItemActionCompositeAdapter<EratosUri> parseAdapter = new ItemActionCompositeAdapter<EratosUri>(R.id.parseAttachment, parseExternalLinkListener, arrayAdapter);
		return new ItemActionCompositeAdapter<EratosUri>(R.id.removeAttachment, removeExternalLinkListener, parseAdapter);
	}

	private Adapter createKeywordAdapter(BibTeXEntryModel entry) {
		List<String> keywords = entry.getKeywords();
		ArrayAdapter<String> keywordArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.tag_list_entry, R.id.keywordText, keywords);
		ItemActionCompositeAdapter<String> keywordSearchAdapter = new ItemActionCompositeAdapter<String>(R.id.keywordText, new KeywordSearchListener(), keywordArrayAdapter);
		ItemActionCompositeAdapter<String> keywordRemoveAdapter = new ItemActionCompositeAdapter<String>(R.id.removeKeyword, new RemoveKeywordListener(), keywordSearchAdapter);
		return keywordRemoveAdapter;
	}

	private ItemActionCompositeAdapter<String> createFieldListAdapter() {
		MapAdapter mapAdapter = new MapAdapter(getActivity(), entry.getOtherFields(), R.layout.field_list_element, R.id.fieldNameView, R.id.fieldValueView);
		ItemActionCompositeAdapter<String> removableFieldAdapter = new ItemActionCompositeAdapter<String>(R.id.removeFieldButton, new RemoveFieldListener(), mapAdapter);
		return removableFieldAdapter;
	}

	private ItemActionCompositeAdapter<EratosUri> createAttachmentListAdapter() {
		List<EratosUri> files = entry.getFiles();
		
		if(files.size() == 0)
			viewAttachment.setVisibility(View.GONE);
		else
			viewAttachment.setVisibility(View.VISIBLE);

		if (files.size() > 0)
			noFilesNotificationTextView.setVisibility(View.GONE);
		else
			noFilesNotificationTextView.setVisibility(View.VISIBLE);
		
		attachmentArrayAdapter = new ArrayAdapter<EratosUri>(getActivity(), R.layout.attachment_list_element, R.id.filename, files);
		
		ItemActionCompositeAdapter<EratosUri> iconAdapter = new ItemActionCompositeAdapter<EratosUri>(R.id.filename, new SourceIconCallback(), attachmentArrayAdapter);
		ItemActionCompositeAdapter<EratosUri> parseAdapter = new ItemActionCompositeAdapter<EratosUri>(R.id.parseAttachment, parseAttachmentListener, iconAdapter);
		return new ItemActionCompositeAdapter<EratosUri>(R.id.removeAttachment, removeAttachmentListener, parseAdapter);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == addAttachment.getId()) {
			Intent i = FileBrowserActivity.getFile(getActivity());
			startActivityForResult(i, REQUEST_CHOOSE_ATTACHMENT);
		} else if(view.getId() == addField.getId()){
			new FieldEditorDialog(getActivity(), entry, false, new NewFieldListener());
		} else if(view.getId() == abstractView.getId()){
			new FieldEditorDialog(getActivity(), entry, BibTeXEntryModel.ABSTRACT_KEY.getValue(), true, new FieldEditListener());
		} else if(view.getId() == addKeywordButton.getId()){
			addKeywordDialog();
		} else if(view.getId() == starEntry.getId()){
			toggleImportant();
		} else if(view.getId() == editEntry.getId()){
			editEntry();
		} else if(view.getId() == viewAttachment.getId()){
			fetchAttachment();
		} else if(view.getId() == addLink.getId()){
			NotificationUtils.promptInput(getActivity(), "New URL", "", new URLAddListener(), InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
		} else if(view.getId() == searchEntry.getId()){
			ISearchHandler defaultSearchHandler = SearchFactory.getDefaultHandler(entry.getType());
			if(defaultSearchHandler != null){
				Intent i = defaultSearchHandler.search(getActivity(), entry);
				startActivity(i);
			}else{
				NotificationUtils.notify(getActivity(), "No Handler Found", "No search handler is available for this entry type");
			}
		} else if(view.getId() == notesView.getId()){
			notesEditView.setText(entry.getNotes());
			
			notesView.setVisibility(View.GONE);
			notesEditView.setVisibility(View.VISIBLE);
			notesEditButtonPanel.setVisibility(View.VISIBLE);
			
			notesEditView.requestFocus();
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(notesEditView, InputMethodManager.SHOW_IMPLICIT);
			
		} else if(view.getId() == notesEditCommitButton.getId()){
			trySetNotesField(notesEditView.getText().toString());
		} else if(view.getId() == notesEditCancelButton.getId()){
			notesView.setVisibility(View.VISIBLE);
			notesEditView.setVisibility(View.GONE);
			notesEditButtonPanel.setVisibility(View.GONE);
			
			closeKeyboard(notesEditView);
		}
	}

	private void addKeywordDialog() {
		Cursor c = contentAdapter.getKeywords();
		Set<String> keywords = new HashSet<String>(c.getCount());
		while(c.moveToNext()){
			String kw = c.getString( c.getColumnIndex(DBHelper.COLUMN_KEYWORD) );
			keywords.add(kw);
		}
		
		final AutocompleteTextEntryDialog input = new AutocompleteTextEntryDialog(getActivity(), "Add Keyword", keywords, new KeywordAddListener());
		input.createDialog("Enter keyword...", "");
	}

	private void closeKeyboard(View view) {
		view.clearFocus();
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//		getActivity().getWindow().setSoftInputMode(
//			      WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}

	private void trySetNotesField(String newValue) {
		try {
			String commentsField=BibTeXEntryModel.COMMENTS_KEY.toString();
			new SetFieldTransaction(getActivity(), contentAdapter, commentsField, newValue).execute(entry);
			
			if(newValue.isEmpty()){
				notesView.setText(Html.fromHtml("<font color=\"#999999\">Tap to edit...</font>"));
			}else{
				String notesHtml = latexToHTML(newValue);
				notesView.setText(Html.fromHtml(notesHtml));
			}
			
			notesView.setVisibility(View.VISIBLE);
			notesEditView.setVisibility(View.GONE);
			notesEditButtonPanel.setVisibility(View.GONE);
			
			closeKeyboard(notesEditView);
		} catch(Exception e) {
			NotificationUtils.notify(getActivity(), "Error", "Field parser error: " + e.getMessage());
		}
	}

	@Override
	public boolean onLongClick(View v) {
		if(v.getId() == editEntry.getId()){
			editEntryRaw();
			return true;
		}else if(v.getId() == searchEntry.getId()){
			showSearchOptions();
			return true;
		}else if(v.getId() == viewAttachment.getId()){
			forceDownloadAttachment();
			return true;
		}
		return false;
	}
	
	private void forceDownloadAttachment() {
		NotificationUtils.confirmDialog(getActivity(), "Force Download?", "Force download of remote file and overwrite local copy?", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				EratosUri uri = entry.getFiles().get(0);
				uri = uri.resolveUri(EratosApplication.getApplication());
				FileManager.getFileManager().fetchFile(getActivity(), uri, new FileCacheListener(getActivity()));
			}
		});
	}

	private void showSearchOptions() {
		final List<String> handlers = new ArrayList<String>(SearchFactory.getHandlers(entry.getType()));
		InputUtils.listChoiceCheckDialog(getActivity(), "Choose Search Service", "Use as default", handlers, new IStringCheckboxResultListener() {
			@Override
			public boolean onResult(String handlerName, boolean checked, DialogInterface dialog) {
				ISearchHandler handler = SearchFactory.getHandler(handlerName);
				if(handler == null){
					NotificationUtils.notify(getActivity(), "No Handler Selected", "You must select a handler.");
				}else{
					if(checked)
						EratosApplication.getApplication().getSettingsManager().makeHandlerDefault(handlerName);
					Intent i = handler.search(getActivity(), entry);
					startActivity(i);
				}
				return false;
			}
		});
	}

	private void fetchAttachment() {
		EratosUri uri = entry.getFiles().get(0);
		uri = uri.resolveUri(EratosApplication.getApplication());
		FileManager.getFileManager().cacheFile(getActivity(), uri, new FileCacheListener(getActivity()));
	}
	
	private void editEntryRaw() {
		new BibtexEditorDialog(getActivity(), entry, new BibtexEditorListener() {
			@Override
			public void success(List<BibTeXEntryModel> entries) {
				mCallbacks.selectEntry(entries.get(0).getRefKey());
			}
		});
	}

	private void editEntry() {
		Intent i = EditEntryActivity.editEntry(getActivity(), entry.getRefKey());
		startActivity(i);
	}

	private void toggleImportant() {
		try {
			boolean mode = !entry.important();
			new ToggleImportantTransaction(getActivity(), contentAdapter, fileAdapter).execute(entry);
			if(mode)
				NotificationUtils.shortToast(getActivity(), "Marked entry important");
			else
				NotificationUtils.shortToast(getActivity(), "Marked entry not important");
		}catch(Exception e) {
			NotificationUtils.notifyException(getActivity(), "Unexpected error", e);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CHOOSE_ATTACHMENT && resultCode == Activity.RESULT_OK) {
			EratosUri fileUri = EratosUri.parseUri( data.getStringExtra(FileBrowserActivity.PATH_ARG) );
			if(EratosApplication.getApplication().getSettingsManager().getManagedFiles())
				nameAttachment(fileUri);
			else
				addAttachment(fileUri);
		}
	}

	private void addAttachment(EratosUri fileUri) {
		FileManager.getFileManager().addFileToLibrary(getActivity(), fileUri, fileUri.name(), false, fileAdapter, new IFileOperationResultListener() {
			public void finished(FileResult result) {
				try{
					new AddAttachmentTransaction(getActivity(), contentAdapter, fileAdapter, result.uri()).execute(entry);
				}catch(Exception e){
					failed(e);
				}
			}
			public void failed(Throwable error) {
				NotificationUtils.notifyException(getActivity(), "Attach File Failed", error);
			}
			public void started(String taskName) {
				
			}
		});
	}
	
	private void addAttachment(final EratosUri fileUri, final String filename, final DialogInterface dialog, boolean forceOverwrite) {
		IFileOperationResultListener listener = new IFileOperationResultListener() {
			public void finished(FileResult result) {
				try {
					new AddAttachmentTransaction(getActivity(), contentAdapter, fileAdapter, result.uri()).execute(entry);
					dialog.dismiss();
				} catch(Exception e) {
					failed(e);
				}
			}
			public void failed(Throwable error) {
				if(error instanceof FileAlreadyExistsException) {
					NotificationUtils.confirmDialog(getActivity(), 
							"Filename In Use", "That filename is already in use! Would you like to overwrite?", 
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialogInterface, int which) {
									addAttachment(fileUri, filename, dialog, true);
								}
							});
				} else {
					dialog.dismiss();
					NotificationUtils.notifyException(getActivity(), "Attach File Failed", error);
				}
			}
			public void started(String taskName) {
				
			}
		};
		FileManager.getFileManager().addFileToLibrary(getActivity(), fileUri, filename, forceOverwrite, fileAdapter, listener);
	}

	private void nameAttachment(final EratosUri fileUri) {
		String suggestedName = entry.getSuggestedAttachmentName() + "." + fileUri.extension();

		NotificationUtils.promptInput(getActivity(), "Choose Filename", suggestedName, new IStringResultListener() {
				public boolean onResult(String filename, DialogInterface dialog)
				{
					addAttachment(fileUri, filename, dialog, false);
					return false;
				}
			}, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
	}
}
