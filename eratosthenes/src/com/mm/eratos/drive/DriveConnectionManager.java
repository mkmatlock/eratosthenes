package com.mm.eratos.drive;

import java.util.Collections;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolInternetConnectionException;

public class DriveConnectionManager extends ProtocolConnectionManager {
	
	protected static final String GOOGLE_ACCOUNT_NAME_PREF = "google_acct";
	
	protected static final int REQUEST_GOOGLE_PLAY_SERVICES = 524;
	protected static final int RESOLVE_ACCOUNT_REQUEST_CODE = 530;
	protected static final int RESOLVE_CONNECTION_REQUEST_CODE = 534;
	private static DriveConnectionManager singleton;

	public static DriveConnectionManager getDriveConnectionManager() {
		if(singleton == null)
			singleton = new DriveConnectionManager();
		return singleton;
	}
	
	private ConnectionListener listener;
	private Drive service;
	private GoogleAccountCredential credential;

	private EratosLogger logger;
	
	protected DriveConnectionManager(){
		logger = EratosApplication.getApplication().getLogManager().getLogger(DriveConnectionManager.class);
	}
	
	public Drive getApiClient(){
		return service;
	}
	
	private boolean initializeSession(Activity mContext) {
		credential = GoogleAccountCredential.usingOAuth2(mContext, Collections.singleton(DriveScopes.DRIVE));
		
		String accountName = getAccountName();
		if(accountName == null)
			mContext.startActivityForResult(credential.newChooseAccountIntent(), RESOLVE_ACCOUNT_REQUEST_CODE);
		else{
			credential.setSelectedAccountName(accountName);
			createService(mContext, accountName);
		}
	    return true;
	}

	private void createService(Activity mContext, String accountName) {
		GsonFactory gsonFactory = new GsonFactory();
		service = new Drive.Builder(AndroidHttp.newCompatibleTransport(), gsonFactory, credential)
							.setApplicationName(mContext.getApplication().getPackageName())
							.build();
		checkConnection(mContext);
	}

	@Override
	public String name() {
		return "Google Drive";
	}

	@Override
	public boolean sessionLinked() {
		return service != null;
	}
	
	@Override
	public void connect(Activity mContext, ConnectionListener listener){
		this.listener = listener;
		listener.connecting();
		EratosApplication app = EratosApplication.getApplication();
		
		if(!app.canSync()){
			Exception e = null;
			if(!app.isOnline())
				e = new ProtocolInternetConnectionException("Not connected to the network.");
			else
				e = new ProtocolInternetConnectionException("Cannot sync over mobile data network, please find a WiFi connection or change your settings.");
			listener.failed(e);
			return;
		}
		
		if(!sessionLinked())
			initializeSession(mContext);
		else
			checkConnection(mContext);
	}

	private boolean checkGooglePlayServicesAvailable(Activity mContext) {
		int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
		if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
//			showGooglePlayServicesAvailabilityErrorDialog(mContext, connectionStatusCode);
			return false;
		}
		return true;
	}

	void showGooglePlayServicesAvailabilityErrorDialog(final Activity mContext, final int connectionStatusCode) {
		Dialog dialog = GooglePlayServicesUtil.getErrorDialog(connectionStatusCode, mContext, REQUEST_GOOGLE_PLAY_SERVICES);
		dialog.show();
	}

	private void checkConnection(final Activity mContext) {
		new DriveCheckConnectionTask(new ConnectionListener() {
			public void failed(Exception e) {
				if( e instanceof GooglePlayServicesAvailabilityIOException ) {
					GooglePlayServicesAvailabilityIOException pse = (GooglePlayServicesAvailabilityIOException)e;
					showGooglePlayServicesAvailabilityErrorDialog(mContext, pse.getConnectionStatusCode());
				} else if ( e instanceof UserRecoverableAuthIOException ){
					UserRecoverableAuthIOException ure = (UserRecoverableAuthIOException)e;
					mContext.startActivityForResult(ure.getIntent(), RESOLVE_CONNECTION_REQUEST_CODE);
				} else {
					listener.failed(e);
				}
			}
			public void connecting() {
				
			}
			public void connected() {
				listener.connected();
			}
		}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}

	private void saveAccountName(String accountName) {
		Editor edit = EratosApplication.getApplication().getPreferences().edit();
		edit.putString(GOOGLE_ACCOUNT_NAME_PREF, accountName);
		edit.commit();
	}
	
	private String getAccountName() {
		String acctName = EratosApplication.getApplication().getPreferences().getString(GOOGLE_ACCOUNT_NAME_PREF, null);
		logger.info("Logging into google drive account: %s", acctName);
		return acctName;
	}
	
	@Override
	public void onActivityResult(Activity mContext, int requestCode, int resultCode, Intent data){
        // handle connection result
		if(requestCode == RESOLVE_ACCOUNT_REQUEST_CODE){
			if (resultCode == Activity.RESULT_OK && data != null && data.getExtras() != null) {
				String accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
				if (accountName != null) {
					credential.setSelectedAccountName(accountName);
					saveAccountName(accountName);
					createService(mContext, accountName);
					checkConnection(mContext);
				}
			}else
				listener.failed(new Exception("Connection attempt cancelled"));
			
		}else if(requestCode == RESOLVE_CONNECTION_REQUEST_CODE){
			if (resultCode == Activity.RESULT_OK)
				checkConnection(mContext);
			else 
				listener.failed(new Exception("Connection to Google Drive failed"));
		}else if(requestCode == REQUEST_GOOGLE_PLAY_SERVICES){
			if (resultCode == Activity.RESULT_OK && checkGooglePlayServicesAvailable(mContext))
				checkConnection(mContext);
			else
				listener.failed(new Exception("Google play services is not available on your device"));
		}
	}
	
	@Override
	public void onResume(Activity mContext) {
		// reconnect
	}
	
	@Override
	public void onPause(Activity mContext) {
        // disconnect
	}
}
