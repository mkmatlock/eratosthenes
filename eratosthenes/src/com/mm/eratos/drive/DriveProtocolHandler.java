package com.mm.eratos.drive;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.google.api.services.drive.Drive;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolHandler;

public class DriveProtocolHandler extends ProtocolHandler {
	public DriveProtocolHandler(){
		
	}
	
	private Drive mApiClient(){
		return DriveConnectionManager.getDriveConnectionManager().getApiClient();
	}
	
	private DriveMetadataCache metadataCache(){
		return DriveMetadataCache.getMetadataCache(mApiClient());
	}
	
	@Override
	public boolean isProtocol(EratosUri uri) {
		return EratosUri.DRIVE_PROTOCOL.equalsIgnoreCase(uri.protocol());
	}
	
	@Override
	public EratosUri root() {
		return EratosUri.makeUri(EratosUri.DRIVE_PROTOCOL, "/");
	}

	@Override
	public EratosUri parent(EratosUri uri) {
		if(isRoot(uri))
			return uri;
		return uri.parent();
	}

	@Override
	public boolean isRoot(EratosUri uri) {
		return root().equals(uri);
	}

	@Override
	public FileResult move(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);
		
		try {
			if(metadataCache().fileExists(dst))
				metadataCache().delete(dst);
			
			FileResult result = metadataCache().copy(src, dst);
			metadataCache().delete(src);
			return result;
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult put(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);

		try {
			File f = FileManager.getFileManager().getFile(src);
			return metadataCache().put(dst, f, new IFileOperationProgressListener() {
				public void progress(String message, int progress, int maxprogress) {
				}
			});
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult put(EratosUri src, EratosUri dst, IFileOperationProgressListener listener) throws ProtocolException {
		assertProtocol(src);
		
		try {
			File f = FileManager.getFileManager().getFile(src);
			return metadataCache().put(dst, f, listener);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}


	@Override
	public FileResult copy(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);
		
		try {
			if(metadataCache().fileExists(dst))
				metadataCache().delete(dst);
			
			return metadataCache().copy(src, dst);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult list(EratosUri dir) throws ProtocolException {
		assertProtocol(dir);
		
		try {
			List<FileResult> listing = metadataCache().list(dir, true);
			return new FileResult(dir, listing);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}
	
	@Override
	public FileResult refresh(EratosUri dir) throws ProtocolException {
		assertProtocol(dir);
		
		try {
			List<FileResult> listing = metadataCache().list(dir, false);
			return new FileResult(dir, listing);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc) throws ProtocolException {
		assertProtocol(src);
		return fetch(src, dst, deleteSrc, new IFileOperationProgressListener() {
			public void progress(String message, int progress, int maxprogress) {
			}
		});
	}
	
	@Override
	public FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc, IFileOperationProgressListener listener) throws ProtocolException {
		assertProtocol(src);

		try {
			File fileDestination = FileManager.getFileManager().getFile(dst);
			if(deleteSrc)
				metadataCache().delete(src);
			return metadataCache().fetch(src, fileDestination, listener);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult delete(EratosUri src) throws ProtocolException {
		assertProtocol(src);
		
		try {
			return metadataCache().delete(src);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult createFolder(EratosUri dir, String name) throws ProtocolException {
		assertProtocol(dir);
		
		try {
			EratosUri uri = metadataCache().createFolder(dir, name);
			return new FileResult(uri, true);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult info(EratosUri src) throws ProtocolException {
		assertProtocol(src);
		
		try {
			return metadataCache().info(src);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	private void assertProtocol(EratosUri src) throws ProtocolException {
		if(!src.protocol().equalsIgnoreCase(EratosUri.DRIVE_PROTOCOL))
			throw new ProtocolException("Invalid protocol: " + src.protocol() + ", expected: " + EratosUri.DRIVE_PROTOCOL);
	}

	@Override
	public String name() {
		return "Google Drive";
	}

	@Override
	public String protocol() {
		return EratosUri.DRIVE_PROTOCOL;
	}

	@Override
	public void clearCache() {
		metadataCache().clear();
	}

	@Override
	public FileResult createFile(EratosUri dst) throws ProtocolException {
		assertProtocol(dst);
		
		try {
			File tmpFile = File.createTempFile("eratos-empty-upload", "temp");
			FileResult result = metadataCache().put(dst, tmpFile, new IFileOperationProgressListener() {
				@Override
				public void progress(String message, int progress, int maxprogress) {
				}
			});
			return result;
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}
}
