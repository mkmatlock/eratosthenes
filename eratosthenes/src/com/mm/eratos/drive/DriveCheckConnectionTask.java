package com.mm.eratos.drive;

import android.os.AsyncTask;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Children;
import com.mm.eratos.protocol.ProtocolConnectionManager.ConnectionListener;

public class DriveCheckConnectionTask extends AsyncTask<Void, Void, Boolean> {

	private ConnectionListener listener;
	private Exception failure;

	public DriveCheckConnectionTask(ConnectionListener listener){
		this.listener = listener;
	}

	@Override
	protected void onPreExecute() {
		
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			Drive apiClient = DriveConnectionManager.getDriveConnectionManager().getApiClient();
			Children.List request = apiClient.children().list("root");
			request.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			failure = e;
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if(result)
			listener.connected();
		else if(failure != null)
			listener.failed(failure);
	}
}
