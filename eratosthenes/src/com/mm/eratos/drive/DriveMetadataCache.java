package com.mm.eratos.drive;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files.Copy;
import com.google.api.services.drive.Drive.Files.Delete;
import com.google.api.services.drive.Drive.Files.Insert;
import com.google.api.services.drive.Drive.Files.Update;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.File.Labels;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.ProtocolNoSuchFileException;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.WebUtils;

public class DriveMetadataCache {
	private static final String ROOT_FOLDER_ID = "root";
	private static final String DRIVE_FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";
	
	static private DriveMetadataCache singleton;
	private Drive apiClient;
	
	CaseInsensitiveMap<List<FileResult>> cachedListings;
	CaseInsensitiveMap<String> fileIds;
	Map<String, File> fileMetadata;
	
	public DriveMetadataCache() {
		cachedListings = new CaseInsensitiveMap<List<FileResult>>();
		fileIds = new CaseInsensitiveMap<String>();
		fileMetadata = new HashMap<String, File>();
	}

	private void setApiClient(Drive apiClient) {
		this.apiClient = apiClient;
	}
	
	public List<FileResult> list(EratosUri uri, boolean useCache) throws IOException, ProtocolException {
		String folderId = getFileId(uri);
		return list(uri, folderId, useCache);
	}
	
	public FileResult copy(EratosUri src, EratosUri dst) throws IOException, ProtocolException {
		String fileId = getFileId(src);
		String parentId = getFileId(dst.parent());
		File metadata = new File();
		
		ParentReference parentRef = new ParentReference();
		parentRef.setId(parentId);
		
		File copy = metadata.clone();
		copy.setParents(Collections.singletonList(parentRef));
		copy.setTitle(dst.name());
		
		Copy request = apiClient.files().copy(fileId, copy);
		File result = request.execute();
		
		return addToCache(dst, result);
	}
	
	public FileResult delete(EratosUri uri) throws IOException, ProtocolException {
		String fileId = getFileId(uri);
		File metadata = getMetadata(uri);
		
		Delete request = apiClient.files().delete(fileId);
		request.execute();
		
		deleteFromCache(uri);
		
		return metadataToFile(uri, metadata);
	}

	public FileResult fetch(EratosUri src, java.io.File fileDestination, final IFileOperationProgressListener listener) throws IOException, ProtocolException {
		String fileId = getFileId(src);
		File metadata = fileMetadata.get(fileId);
		
		OutputStream outputStream = new FileOutputStream(fileDestination);
		final String message = "Downloading: " + src;
		
		HttpRequestFactory requestFactory = apiClient.getRequestFactory();
		MediaHttpDownloader downloader =
		        new MediaHttpDownloader(requestFactory.getTransport(), requestFactory.getInitializer());
		downloader.setChunkSize(1024*128);
		downloader.setDirectDownloadEnabled(false);
		downloader.setProgressListener(
				new MediaHttpDownloaderProgressListener() {
					public void progressChanged(MediaHttpDownloader downloader) throws IOException {
						int bytes = (int)downloader.getNumBytesDownloaded();
						int total = (int)downloader.getLastBytePosition();
						
						switch (downloader.getDownloadState()) {
						case MEDIA_IN_PROGRESS:
							listener.progress(message, bytes, total);
							break;
						case MEDIA_COMPLETE:
							listener.progress(message, total, total);
						default:
							break;
						}
					}
				});
        downloader.download(new GenericUrl(metadata.getDownloadUrl()), outputStream);
        
		return addToCache(src, metadata);
	}
	
	public FileResult put(EratosUri dst, java.io.File fileContent, final IFileOperationProgressListener listener) throws IOException, ProtocolException {
		EratosUri parent = dst.parent();
		if(!folderExists(parent))
			createFolder(parent.parent(), parent.name());
		
		String folderId = getFileId(parent);
		list(parent, folderId, true);

		if(fileCached(dst))
			return updateFile(dst, fileContent, listener);
		else
			return insertFile(dst, folderId, fileContent, listener);
	}

	private boolean folderExists(EratosUri parent) {
		if( cachedListings.containsKey(parent.path()) )
			return true;
		
		try {
			String folderId = getFileId(parent);
			list(parent, folderId, true);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private FileResult updateFile(EratosUri dst, java.io.File fileContent, final IFileOperationProgressListener listener) throws IOException {
		String fileId = fileIds.get(dst.path());
		File metadata = fileMetadata.get(fileId);
		
		String mimeType = WebUtils.getMimeType(dst);
		if(mimeType != null)
			metadata.setMimeType(mimeType);
		FileContent mediaContent = new FileContent(mimeType, fileContent);

		final int fileLength = (int)fileContent.length();
		final String message = "Uploading: " + dst;
		
		Update request = apiClient.files().update(fileId, metadata, mediaContent);
		MediaHttpUploader uploader = request.getMediaHttpUploader();
		uploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
		uploader.setDirectUploadEnabled(false);
		uploader.setProgressListener(
				new MediaHttpUploaderProgressListener() {
					public void progressChanged(MediaHttpUploader uploader) throws IOException {
						switch (uploader.getUploadState()) {
						case INITIATION_STARTED:
							listener.progress(message, 0, fileLength);
							break;
						case MEDIA_IN_PROGRESS:
							int bytesUploaded = (int)(uploader.getProgress() * fileLength);
							listener.progress(message, bytesUploaded, fileLength);
							break;
						case MEDIA_COMPLETE:
							listener.progress(message, fileLength, fileLength);
						default:
							break;
						}
					}
				});
		
		File result = request.execute();
		return addToCache(dst, result);
	}

	private FileResult insertFile(EratosUri dst, String folderId, java.io.File fileContent, final IFileOperationProgressListener listener) throws IOException {
		ParentReference parentRef = new ParentReference();
		parentRef.setId(folderId);
		
		File upload = new File();
		upload.setTitle(dst.name());
		String mimeType = WebUtils.getMimeType(dst);
		if(mimeType != null)
			upload.setMimeType(mimeType);
		upload.setParents(Collections.singletonList(parentRef));
		
		final int fileLength = (int)fileContent.length();
		final String message = "Uploading: " + dst;
		
	    FileContent mediaContent = new FileContent(mimeType, fileContent);
		Insert request = apiClient.files().insert(upload, mediaContent);
		MediaHttpUploader uploader = request.getMediaHttpUploader();
		uploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
		uploader.setDirectUploadEnabled(false);
		uploader.setProgressListener(
				new MediaHttpUploaderProgressListener() {
					public void progressChanged(MediaHttpUploader uploader) throws IOException {
						switch (uploader.getUploadState()) {
						case INITIATION_STARTED:
							listener.progress(message, 0, fileLength);
							break;
						case MEDIA_IN_PROGRESS:
							int bytesUploaded = (int)(uploader.getProgress() * fileLength);
							listener.progress(message, bytesUploaded, fileLength);
							break;
						case MEDIA_COMPLETE:
							listener.progress(message, fileLength, fileLength);
						default:
							break;
						}
					}
				});
		
		File result = request.execute();
		return addToCache(dst, result);
	}

	public FileResult info(EratosUri uri) throws IOException, ProtocolException {
		String fileId = getFileId(uri);
		return metadataToFile(uri, fileMetadata.get(fileId));
	}
	
	public EratosUri createFolder(EratosUri dir, String name) throws IOException, ProtocolException {
		EratosUri subdirUri = dir.append(name);
		String subdirPath = subdirUri.path();
		
		String folderId = getFileId(dir);
		list(dir, folderId, true);
		
		if(fileIds.containsKey(subdirPath))
			throw new ProtocolException("File already exists: " + subdirUri);
		
		ParentReference parent = new ParentReference();
		parent.setId(folderId);
		
		File metadata = new File();
		metadata.setTitle(name);
		metadata.setMimeType(DRIVE_FOLDER_MIME_TYPE);
		metadata.setParents(Collections.singletonList(parent));
		
		File result = apiClient.files().insert(metadata).execute();
		addToCache(subdirUri, result);
		cachedListings.put(subdirPath, new ArrayList<FileResult>());
		
		return subdirUri;
	}
	
	private List<FileResult> list(EratosUri uri, String folderId, boolean useCache) throws IOException {
		String folderPath = uri.path();
		
		if(useCache && cachedListings.containsKey(folderPath))
			return cachedListings.get(folderPath);
		
		String q = String.format(Locale.US, "'%s' in parents", folderId);
		com.google.api.services.drive.Drive.Files.List request = apiClient.files().list().setQ(q);
		
		List<FileResult> listing = new ArrayList<FileResult>();
		
		do {
			FileList children = request.execute();
			for (File child : children.getItems()) {
				String childId = child.getId();
				
				Labels labels = child.getLabels();
				boolean hide = labels.getTrashed();
				
				if(!hide) {
					EratosUri childUri = uri.append(child.getTitle());
					FileResult fileResult = metadataToFile(childUri, child);
					
					String filePath = childUri.path();
					fileIds.put(filePath, childId);
					fileMetadata.put(childId, child);
					
					listing.add(fileResult);
				}
			}
			request.setPageToken(children.getNextPageToken());			
		} while (request.getPageToken() != null && request.getPageToken().length() > 0);
		
		cachedListings.put(folderPath, listing);
		return listing;
	}
	
	private FileResult metadataToFile(EratosUri uri, File metadata) {
		long modified = metadata.getModifiedDate().getValue();
		boolean isFolder = metadata.getMimeType().equals(DRIVE_FOLDER_MIME_TYPE);
		String rev = ""+modified;
		
		FileResult fileResult = new FileResult(uri, rev, modified, isFolder);
		return fileResult;
	}
	
	private FileResult addToCache(EratosUri uri, File metadata){
		FileResult result = metadataToFile(uri, metadata);
		String path = uri.path();
		
		fileIds.put(path, metadata.getId());
		fileMetadata.put(metadata.getId(), metadata);
		updateParentListing(result);
		
		return result;
	}

	private void updateParentListing(FileResult result) {
		String parentPath = result.uri().parent().path();
		List<FileResult> list = cachedListings.get(parentPath);
		removeFileListing(result.uri(), list);
		list.add(result);
		cachedListings.put(parentPath, list);
	}

	private void removeFileListing(EratosUri uri, List<FileResult> list) {
		FileResult remove = null;
		for(FileResult item : list){
			if(item.uri().equals(uri)){
				remove = item;
				break;
			}
		}
		if(remove != null)
			list.remove(remove);
	}

	private boolean fileCached(EratosUri uri) {
		return fileIds.containsKey(uri.path());
	}
	
	private File getMetadata(EratosUri uri) {
		String fileId = fileIds.get(uri.path());
		return fileMetadata.get(fileId);
	}

	private void deleteFromCache(EratosUri uri) {
		String path = uri.path();
		String parentPath = uri.parent().path();
		
		List<FileResult> list = cachedListings.get(parentPath);
		removeFileListing(uri, list);
		
		String fileId = fileIds.remove(path);
		fileMetadata.remove(fileId);
	}
	
	private String getFileId(EratosUri uri) throws IOException, ProtocolException {
		String path = uri.path();
		
		if(path.equals("/"))
			return ROOT_FOLDER_ID;
		if(fileIds.containsKey(path))
			return fileIds.get(path);
		
		EratosUri parent = uri.parent();
		String folderId = getFileId(parent);
		list(parent, folderId, true);
		
		if(fileIds.containsKey(path))
			return fileIds.get(path);
		
		throw new ProtocolNoSuchFileException("File does not exist: " + uri);
	}

	public static DriveMetadataCache getMetadataCache(Drive apiClient){
		if(singleton == null)
			singleton = new DriveMetadataCache();
		singleton.setApiClient(apiClient);
		return singleton;
	}

	public void clear() {
		cachedListings.clear();
		fileIds.clear();
		fileMetadata.clear();
	}

	public boolean fileExists(EratosUri uri) throws IOException, ProtocolException {
		try {
			getFileId(uri);
			return true;
		} catch (ProtocolNoSuchFileException e) {
			return false;
		}
	}
}
