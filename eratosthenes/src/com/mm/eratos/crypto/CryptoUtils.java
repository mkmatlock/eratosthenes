package com.mm.eratos.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;
import android.content.Context;

import com.dd.plist.Base64;
import com.mm.eratos.utils.WebUtils;

public class CryptoUtils {
	private static final String KEY_FILE = ".key";
	
	public static SecretKey getEncryptionKey(Context context) throws IOException {
		File keyFile = context.getFileStreamPath(KEY_FILE);
		if(keyFile.exists())
			return readKey(context.openFileInput(KEY_FILE));
		else
			return makeKey(context.openFileOutput(KEY_FILE, Context.MODE_PRIVATE));
	}
	
	@SuppressLint("TrulyRandom")
	public static String encrypt(String clear, SecretKey key) throws IOException {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
	        byte[] encrypted = cipher.doFinal(clear.getBytes("UTF8"));
	        return Base64.encodeBytes(encrypted);
        } catch (Exception e) {
        	throw new IOException(e);
        }
    }

	public static String decrypt(String encrypted, SecretKey key) throws IOException {
		try{
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(Cipher.DECRYPT_MODE, key);
	        byte[] decrypted = cipher.doFinal(Base64.decode(encrypted));
	        return new String(decrypted, "UTF8");
		} catch (Exception e) {
	    	throw new IOException(e);
	    }
    }
	
	private static SecretKey makeKey(FileOutputStream fos) throws IOException {
		SecretKey key = generateKey();
		String b64Key = Base64.encodeBytes(key.getEncoded());
		fos.write(b64Key.getBytes());
		fos.close();
		return key;
	}

	private static SecretKey readKey(FileInputStream fis) throws IOException {
		String b64key = WebUtils.readStream(fis);
		byte[] encodedKey = Base64.decode(b64key);
		SecretKey originalKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
		return originalKey;
	}

	@SuppressLint("TrulyRandom")
	public static SecretKey generateKey() throws IOException {
	    // Generate a 256-bit key
	    final int outputKeyLength = 256;

	    SecureRandom secureRandom = new SecureRandom();
	    // Do *not* seed secureRandom! Automatically seeded from system entropy.
	    KeyGenerator keyGenerator;
		try {
			keyGenerator = KeyGenerator.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			throw new IOException(e);
		}
	    keyGenerator.init(outputKeyLength, secureRandom);
	    SecretKey key = keyGenerator.generateKey();
	    return key;
	}
}