package com.mm.eratos.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.operations.AddAttachmentTransaction;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.dialog.BibtexEditorDialog;
import com.mm.eratos.exceptions.FileAlreadyExistsException;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.DOIWebHandler;
import com.mm.eratos.feeds.PubMedWebHandler;
import com.mm.eratos.feeds.WebHandlerFactory;
import com.mm.eratos.files.AbstractProgressFileOperationResultListener;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.fragment.EntryListFragment;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementSearchLike;
import com.mm.eratos.search.db.QueryElementTrue;
import com.mm.eratos.task.DownloadPdfFileTask;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.InputUtils;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.Validator;

public class AddPdfToEntryActivity extends FragmentActivity implements EntryListFragment.Callbacks, OnClickListener  {

	private EntryListFragment entryListFragment;
	private Button attachButton;
	private Button cancelButton;
	private ImageButton searchButton;
	private ImageButton clearButton;
	private EditText searchField;
	
	private String selectedRefKey;
	
	private ProgressDialog progressDialog;
	private EratosUri pdfSrcUri;
	private BibTeXContentProviderAdapter contentAdapter;
	private FileRevisionContentProviderAdapter fileAdapter;
	private boolean selected;
	private BibTeXEntryModelFactory modelFactory;
	private Button openButton;
	private Button addEntryButton;
	
	@Override
	public void onItemSelected(int pos, String id) {
		selectedRefKey = id;
		selected = true;
	}

	@Override
	public void onItemDeselected() {
		selected = false;
	}

	@Override
	public void onDataLoaded(Adapter adapter) {
		progressDialog.dismiss();
		
		for(int i = 0; i < adapter.getCount(); i++){ 
			Cursor cursor = (Cursor) adapter.getItem(i);
			String key = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_KEY) );
			
			if(key.equals(selectedRefKey)){
				entryListFragment.setActivatedPosition(i);
				entryListFragment.scrollIfNeeded(i);
				selected = true;
				break;
			}
		}
	}

	@Override
	public boolean isLargeView() {
		return true;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(EratosApplication.getApplication().getCurrentLibrary() == null){
			NotificationUtils.notify(this, "Error", "Please open or create a bibtex database before attempting to add PDFs to entries!", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
			return;
		}
		
		setContentView(R.layout.activity_add_pdf);
		setFinishOnTouchOutside(false);
		
		EratosApplication app = EratosApplication.getApplication();
		selectedRefKey = app.getLastViewedRefKey();

		contentAdapter = new BibTeXContentProviderAdapter(getContentResolver());
		fileAdapter = new FileRevisionContentProviderAdapter(getContentResolver());
		
		searchField = (EditText)findViewById(R.id.searchText);
		
		addEntryButton = (Button)findViewById(R.id.addNewEntryButton);
		addEntryButton.setOnClickListener(this);
		
		openButton = (Button)findViewById(R.id.open_pdf_button);
		openButton.setOnClickListener(this);
		
		cancelButton = (Button)findViewById(R.id.cancel_button);
		cancelButton.setOnClickListener(this);
		
		attachButton = (Button)findViewById(R.id.attach_button);
		attachButton.setOnClickListener(this);
		
		searchButton = (ImageButton) findViewById(R.id.search_button);
		searchButton.setOnClickListener(this);
		
		clearButton = (ImageButton) findViewById(R.id.clear_button);
		clearButton.setOnClickListener(this);
		
		modelFactory = new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(contentAdapter), app.getSettingsManager().getBibTeXHelper());
		
		try {
			pdfSrcUri = FileManager.getFileManager().getTemporaryExportPdfUri();
			fetchPdf(getIntent().getData(), pdfSrcUri);
			createEntryList("");
		} catch (IOException e) {
			NotificationUtils.notifyException(this, "Error Fetching PDF", e,
					new DialogInterface.OnClickListener(){
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
			});
			e.printStackTrace();
		}
	}
	
	private void fetchPdf(Uri data, EratosUri downloadLocation) throws IOException{
		File parent = FileManager.getFileManager().getFile(downloadLocation.parent());
		if(!parent.exists() && !parent.mkdirs())
			throw new IOException("Could not create download directory: " + parent.getAbsolutePath());
		
		if(data.getScheme().equals("content")){
    		copyContentToPath(data, downloadLocation);
    	}else if (data.getScheme().equals("http") || data.getScheme().equals("https")){
    		EratosUri srcUri = EratosUri.parseUri(data.toString());
    		downloadPdf(srcUri, downloadLocation);
    	}else if (data.getScheme().equals("file")){
    		EratosUri srcUri = EratosUri.parseUri(data.toString());
    		srcUri = EratosUri.makeUri("root", srcUri.path());
    		
    		File src = FileManager.getFileManager().getFile(srcUri);
    		File dst = FileManager.getFileManager().getFile(downloadLocation);
    		FileUtils.copy(src, dst);
    	}
	}

	private void copyContentToPath(Uri data, EratosUri destination) throws IOException {
		progressDialog = ProgressDialog.show(this, "Fetching Content", "Loading...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		
        InputStream attachment = getContentResolver().openInputStream(data);
        if (attachment == null)
            throw new IOException("cannot access mail attachment");
        else
        {
        	String fullPath = FileManager.getFileManager().getFile(destination).getAbsolutePath();
            FileOutputStream tmp = new FileOutputStream(fullPath);
            byte []buffer = new byte[1024];
            while (attachment.read(buffer) > 0)
                tmp.write(buffer);

            tmp.close();
            attachment.close();
        }
        
        progressDialog.dismiss();
	}
	
	private void downloadPdf(EratosUri srcUri, EratosUri downloadLocation) {
		new DownloadPdfFileTask(new AbstractProgressFileOperationResultListener(this) {
			protected void success(FileResult result) {
			}
			protected void failure(Throwable error) {
				NotificationUtils.notifyException(AddPdfToEntryActivity.this, 
						"PDF Download Failed", 
						error, 
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
			}
		}).execute(srcUri, downloadLocation);
	}

	private void createEntryList(String queryText)
	{
		progressDialog = ProgressDialog.show(this, "Loading Entries", "Loading...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		
		entryListFragment = new EntryListFragment();
		Bundle fragmentArgs = new Bundle();
		
		QueryElement query = new QueryElementTrue();
		if(queryText != null )
			query = new QueryElementSearchLike(queryText);
		
		fragmentArgs.putSerializable(EntryListActivity.QUERY, query);
		fragmentArgs.putBoolean(EntryListFragment.DISABLE_BUTTONS, true);
		fragmentArgs.putBoolean(EntryListFragment.ENABLE_DRAG, false);
		fragmentArgs.putInt(EntryListActivity.SORT_ORDER, BibTeXContentProviderAdapter.SORT_COLLECTION);
		fragmentArgs.putInt(EntryListActivity.SORT_DIRECTION, BibTeXContentProviderAdapter.SORT_ASCENDING);
		
		entryListFragment.setArguments(fragmentArgs);

		getSupportFragmentManager().beginTransaction()
			.replace(R.id.entry_list_container, entryListFragment)
			.commit();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.cancel_button){
			finish();
		}else if(v.getId() == R.id.attach_button){
			if(selected)
				finalizeAttachment(pdfSrcUri);
			else
				NotificationUtils.notify(this, "Error", "Please select an entry!");
		}else if(v.getId() == R.id.search_button){
			createEntryList(searchField.getText().toString());
		}else if(v.getId() == R.id.clear_button){
			createEntryList("");
		}else if(v.getId() == R.id.open_pdf_button){
			openPDFInExternalApplication(pdfSrcUri);
		}else if(v.getId() == R.id.addNewEntryButton){
			chooseEntrySource();
		}
	}
	
	private void chooseEntrySource() {
		List<CharSequence> items = new ArrayList<CharSequence>();
		items.add("Entry Creator");
		items.add("Bibtex");
		items.add("DOI");
		items.add("ISBN");
		items.add("PubMed");
		items.add("URL");
		items.add("Barcode Scanner");
		
		InputUtils.listChoiceDialog(AddPdfToEntryActivity.this, "Choose Type", items.toArray(new String[]{}), new IStringResultListener() {
			@Override
			public boolean onResult(String result, DialogInterface dialog) {
				if(result.equals("Entry Creator"))
					createNewEntryEditor();
				else if(result.equals("Bibtex"))
					new BibtexEditorDialog(AddPdfToEntryActivity.this);
				else if(result.equals("DOI"))
					getDOI();
				else if(result.equals("ISBN"))
					getISBN();
				else if(result.equals("PubMed"))
					getPubMed();
				else if(result.equals("URL"))
					getUrl();
				else if(result.equals("Barcode Scanner"))
					getBarcode();
				
				return false;
			}
		});
	}
	

	protected void getBarcode() {
		IntentIntegrator scanIntegrator = new IntentIntegrator(AddPdfToEntryActivity.this);
		scanIntegrator.initiateScan();
	}

	private void createNewEntryEditor() {
		Intent i = EditEntryActivity.newEntry(AddPdfToEntryActivity.this);
		AddPdfToEntryActivity.this.startActivity(i);
	}

	
	private void getPubMed() {
		NotificationUtils.promptInputNumber(AddPdfToEntryActivity.this, "Enter PubMed ID", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if(! Validator.isPMID(result) ){
					NotificationUtils.notify(AddPdfToEntryActivity.this, "Error", "Invalid PubMed ID format!");
					return false;
				}
				
				EratosUri uri = PubMedWebHandler.pubmedUrl.format(result);
				Intent i = new Intent(AddPdfToEntryActivity.this, AddEntryFromPubMed.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(uri.toString()));
				AddPdfToEntryActivity.this.startActivity(i);
				return true;
			}
		});
	}
	
	private void getUrl(){
		NotificationUtils.promptInput(AddPdfToEntryActivity.this, "Enter URL", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				EratosUri uri = EratosUri.parseUri(result);
				if(uri == null && !result.matches("^[a-zA-Z]+://.*"))
					uri = EratosUri.parseUri("http://" + result);
				
				if( uri == null ){
					NotificationUtils.notify(AddPdfToEntryActivity.this, "Error", "Invalid URL!");
					return false;
				}
				
				AbstractWebHandler handler = WebHandlerFactory.getHandler(uri);
				if(handler == null){
					NotificationUtils.notify(AddPdfToEntryActivity.this, "Unrecognized URL", "Eratosthenes does not currently have a handler for that URL");
					return false;
				}
				
				Intent i = new Intent(AddPdfToEntryActivity.this, AddEntryWebHandlerSearchActivity.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(result));
				AddPdfToEntryActivity.this.startActivity(i);
				return true;
			}
		});
	}
	
	private void getISBN() {
		NotificationUtils.promptInputNumber(AddPdfToEntryActivity.this, "Enter ISBN", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if(! Validator.isISBN(result) ){
					NotificationUtils.notify(AddPdfToEntryActivity.this, "Error", "Invalid ISBN format!");
					return false;
				}
				
				Intent i = new Intent(AddPdfToEntryActivity.this, AddEntryFromISBN.class);
				i.setAction(Intent.ACTION_VIEW);
				i.putExtra(AddEntryFromISBN.ISBN_VALUE, result);
				AddPdfToEntryActivity.this.startActivity(i);
				return true;
			}
		});
	}

	private void getDOI() {
		NotificationUtils.promptInput(AddPdfToEntryActivity.this, "Enter DOI", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if( ! Validator.isDOI(result) ){
					NotificationUtils.notify(AddPdfToEntryActivity.this, "Error", "Invalid DOI format!");
					return false;
				}
				
				String url = DOIWebHandler.doiUrl.append(result).toString();
				Intent i = new Intent(AddPdfToEntryActivity.this, AddEntryFromDOI.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				AddPdfToEntryActivity.this.startActivity(i);
				return true;
			}
		});
	}
	

	private void processBarcode(IntentResult result) {
		String content = result.getContents();
		String format = result.getFormatName();
		
		if(format.equals("EAN_13") && content.matches("^[0-9]+$")){
			Intent i = new Intent(AddPdfToEntryActivity.this, AddEntryFromISBN.class);
			i.setAction(Intent.ACTION_VIEW);
			i.putExtra(AddEntryFromISBN.ISBN_VALUE, content);
			AddPdfToEntryActivity.this.startActivity(i);
		}else if(format.equals("QR_CODE") && content.toLowerCase(Locale.US).startsWith("http://dx.doi.org/10.")){
			Intent i = new Intent(AddPdfToEntryActivity.this, AddEntryFromDOI.class);
			i.setAction(Intent.ACTION_VIEW);
			i.setData(Uri.parse(content));
			AddPdfToEntryActivity.this.startActivity(i);
		}else{
			Exception e = new Exception("Unrecognized bar code format or content: " + format + " --> " + content);
			NotificationUtils.notifyException(AddPdfToEntryActivity.this, "Unrecognized Code", e);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
		
		if (scanningResult != null && !scanningResult.failed()) {
			processBarcode(scanningResult);
		}
	}
		
	private void openPDFInExternalApplication(EratosUri pdfUri) {
		File f = FileManager.getFileManager().getFile(pdfUri);
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setDataAndType(Uri.fromFile(f), "application/pdf");
		startActivity(i);
		
//		Intent chooser = Intent.createChooser(i, "Choose PDF Application");
//		startActivity(chooser);
	}

	private void finalizeAttachment(EratosUri fileUri){
		final BibTeXEntryModel entry = contentAdapter.getEntry(selectedRefKey, modelFactory);
		nameAttachment(fileUri, entry);
	}
	

	private void notifyException(Throwable error) {
		NotificationUtils.notifyException(AddPdfToEntryActivity.this, "Attachment Failed", error,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
	}
	
	private void notifySuccess(EratosUri uri, String refKey){
		NotificationUtils.longToast(this, "Added file: " + uri.name() + " to entry: " + refKey);
		finish();
	}
	
	private void addAttachment(final EratosUri fileUri, final String filename, final DialogInterface dialog, final BibTeXEntryModel entry, boolean forceOverwrite) {
		IFileOperationResultListener iFileOperationResultListener = new IFileOperationResultListener() {
			public void finished(FileResult result) {
				try {
					new AddAttachmentTransaction(AddPdfToEntryActivity.this, contentAdapter, fileAdapter, result.uri()).execute(entry);
					dialog.dismiss();
					notifySuccess(result.uri(), entry.getRefKey());
				} catch(Exception e) {
					e.printStackTrace();
					failed(e);
				}
			}
			public void failed(Throwable error) {
				if(error instanceof FileAlreadyExistsException) {
					NotificationUtils.confirmDialog(AddPdfToEntryActivity.this, 
							"Filename In Use", "That filename is already in use! Would you like to overwrite?", 
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialogInterface, int which) {
									addAttachment(fileUri, filename, dialog, entry, true);
								}
							});
				} else {
					dialog.dismiss();
					notifyException(error);
				}
			}
			public void started(String taskName) {
				
			}
		};
		
		FileManager.getFileManager().addFileToLibrary(this, fileUri, filename, forceOverwrite, fileAdapter, iFileOperationResultListener);
	}
	
	private void nameAttachment(final EratosUri fileUri, final BibTeXEntryModel entry) {
		String suggestedName = entry.getSuggestedAttachmentName() + "." + fileUri.extension();
		
		NotificationUtils.promptInput(this, "Choose Filename", suggestedName, new IStringResultListener() {
				public boolean onResult(String filename, DialogInterface dialog)
				{
					addAttachment(fileUri, filename, dialog, entry, false);
					return false;
				}
			}, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
	}
}
