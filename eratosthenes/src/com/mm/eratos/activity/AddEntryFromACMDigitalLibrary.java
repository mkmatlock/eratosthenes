package com.mm.eratos.activity;

import java.io.IOException;

import com.mm.eratos.feeds.ACMDigitalLibraryHandler;
import com.mm.eratos.feeds.AbstractWebHandler;

public class AddEntryFromACMDigitalLibrary extends AddEntryWebHandlerActivity {

	@Override
	public AbstractWebHandler getHandler() throws IOException {
		return new ACMDigitalLibraryHandler();
	}

}
