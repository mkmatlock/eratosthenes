package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.OxfordJournalsWebHandler;

public class AddEntryFromOxfordJournals extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new OxfordJournalsWebHandler();
	}
}
