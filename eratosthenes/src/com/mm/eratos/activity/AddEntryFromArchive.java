package com.mm.eratos.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.jbibtex.ParseException;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.files.Decompress;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class AddEntryFromArchive extends AddEntryActivity {
	private List<String> unzip;
	
    public class RetrieveBibtexFileStream extends StreamLoader {
		public InputStream load(Intent intent) throws IOException {
        	FileManager fileManager = FileManager.getFileManager();
        	
        	Uri data = intent.getData();
        	
        	String fullPath = data.getPath();
			String scheme = data.getScheme();
			if(scheme.equals("content")){
        		EratosUri tmpUri = fileManager.getTemporaryExportArchiveUri();
        		fullPath = fileManager.getFile(tmpUri).getAbsolutePath();
        		copyContentToPath(data, fullPath);
        	}else if(scheme.equals("http") || scheme.equals("https")){
        		EratosUri tmpUri = fileManager.getTemporaryExportArchiveUri();
        		fullPath = fileManager.getFile(tmpUri).getAbsolutePath();
        		WebClient.newInstance().downloadFile(data.toString(), tmpUri);
        	}
			
        	String expPath = fileManager.getFile(fileManager.getDefaultExportUri()).getAbsolutePath();
        	unzip = new Decompress(new File(fullPath), expPath).unzip();

        	String bibPath = null;
        	for(String path : unzip){
        		if(path.endsWith(".bib"))
        			bibPath = path;
        	}
        	
			return new FileInputStream(new File(bibPath));
        }

		private void copyContentToPath(Uri data, String fullPath) throws IOException {
	        InputStream attachment = getContentResolver().openInputStream(data);
	        if (attachment == null)
	            Log.e("onCreate", "cannot access mail attachment");
	        else
	        {
	            FileOutputStream tmp = new FileOutputStream(fullPath);
	            byte []buffer = new byte[1024];
	            while (attachment.read(buffer) > 0)
	                tmp.write(buffer);

	            tmp.close();
	            attachment.close();
	        }
		}
    }
    
    public class FileAddProcessor extends PostProcessor {
		@Override
		protected BibTeXEntryModel process(BibTeXEntryModel entry, Intent intent, DownloadListener downloadListener) throws IOException, ParseException, FieldRequiredException {
			FileManager fileManager = FileManager.getFileManager();
        	
			for(String path : unzip){
        		if(!path.endsWith(".bib")) {
        			String fn = EratosUri.parseUri("device://" + path).name();
        			
        			File src = new File(path);
        			EratosUri dstUri = EratosUri.parseUri(EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + fn);
					File dst = fileManager.getFile(dstUri);
        			
					if(!src.equals(dst)){
						FileUtils.copy(src, dst);
						src.delete();
					}
        		}
        	}
			
			return entry;
		}
    }

	@Override
	protected PostProcessor getPostProcessor() {
		return new FileAddProcessor();
	}
	
	@Override
	protected StreamLoader getStreamLoader() {
		return new RetrieveBibtexFileStream();
	}

}
