package com.mm.eratos.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.EntryDetailPagerAdapter;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.fragment.EntryDetailFragment;
import com.mm.eratos.fragment.EntryDetailPagerFragment;
import com.mm.eratos.fragment.EntryGroupFragment;
import com.mm.eratos.fragment.EntryListFragment;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementHasKeyword;
import com.mm.eratos.search.db.QueryElementInCollection;
import com.mm.eratos.search.db.QueryElementInGroup;
import com.mm.eratos.search.db.QueryElementIsImportant;
import com.mm.eratos.search.db.QueryElementIsUnread;
import com.mm.eratos.search.db.QueryElementSearchLike;
import com.mm.eratos.search.db.QueryElementTrue;
import com.mm.eratos.view.ISearchViewCloseListener;
import com.mm.eratos.view.MainMenu.ActionCallback;
import com.mm.eratos.view.MainMenu.SortSelectedCallback;
import com.mm.eratos.view.RemoveTargetDragListener;

/**
 * An activity representing a list of Entries. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link EntryDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link EntryListFragment} and the item details (if present) is a
 * {@link EntryDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link EntryListFragment.Callbacks} interface to listen for item selections.
 */
public class EntryListActivity extends MainMenuActivity implements
		EntryListFragment.Callbacks, EntryDetailPagerFragment.EntryPagerCallbacks, 
		ActionCallback, EntryDetailFragment.Callbacks {

	public static final String SEARCH_URI = "content://com.mm.eratos.search";
	
	public static final String QUERY = "query";
	public static final String SELECTED_ITEM = "selected";
	public static final String SELECTED_PAGE_VIEW = "selected_page_view";

	public static final String SORT_ORDER = "sort";
	public static final String SORT_DIRECTION = "sort_order";

	public static final String SCROLL_POSITION = "scroll_position";

	
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */	
	
	private boolean mTwoPane;
	private EntryListFragment entryListFragment;
	
	private EntryDetailPagerAdapter mPagerAdapter;
	private boolean detailViewActive;
	private Adapter entryAdapter;
	
	private QueryElement query;
	private int mSelectedItem;

	private int sortOrder;
	private int sortDirection;

	private RemoveTargetDragListener removeTargetDragListener;

	private TextView mRemoveDropTarget;

	private boolean enableDrag;

	private EntryDetailPagerFragment entryDetailPagerFragment;

	private int mScrollPosition;

	private boolean isDestroyed;

	private int activatedPageViewId;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_entry_list);
		entryAdapter = null;
		detailViewActive = false;
		mTwoPane = false;
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
	    if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	    	getActionBar().setHomeButtonEnabled(true);
		
		Intent intent = getIntent();
		Bundle args = intent.getExtras();
		
		if(savedInstanceState != null)
			args = savedInstanceState;
		if(args == null)
			args = new Bundle();
		
		activatedPageViewId = args.getInt(SELECTED_PAGE_VIEW, R.id.notes_page_view);

		query = (QueryElement)args.getSerializable(QUERY);
		mSelectedItem = args.getInt(SELECTED_ITEM, -1);
		mScrollPosition = args.getInt(SCROLL_POSITION, 0);
		
		if(intent.getData() != null){
			String search = new String(Base64.decode(intent.getData().getLastPathSegment(), Base64.URL_SAFE));
			query = new QueryElementSearchLike(search);
		}

		sortOrder = args.getInt(SORT_ORDER, BibTeXContentProviderAdapter.SORT_COLLECTION);
		sortDirection = args.getInt(SORT_DIRECTION, BibTeXContentProviderAdapter.SORT_ASCENDING);

		enableDrag=false;
		if (findViewById(R.id.removeTarget) != null) {
			mRemoveDropTarget = (TextView) findViewById(R.id.removeTarget);
			removeTargetDragListener = new RemoveTargetDragListener(this, mRemoveDropTarget);
			mRemoveDropTarget.setOnDragListener(removeTargetDragListener);
			enableDrag=true;

			if( query instanceof QueryElementIsImportant )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_DATABASE, EntryGroupFragment.DATABASE_IMPORTANT);
			else if( query instanceof QueryElementIsUnread )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_DATABASE, EntryGroupFragment.DATABASE_UNREAD);
			else if( query instanceof QueryElementInGroup )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_GROUPS, ((QueryElementInGroup)query).group());
			else if( query instanceof QueryElementInCollection )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_COLLECTIONS, ((QueryElementInCollection)query).collection());
			else if( query instanceof QueryElementHasKeyword )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_KEYWORDS, ((QueryElementHasKeyword)query).keyword());
			else if( query instanceof QueryElementTrue )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_DATABASE, EntryGroupFragment.DATABASE_LIBRARY);
			else
				removeTargetDragListener.setDisabled();
		}
		
		if (findViewById(R.id.entry_pager_container) != null) {
			mTwoPane = true;
			initializeDetailView(null);
			entryDetailPagerFragment = new EntryDetailPagerFragment();
			getSupportFragmentManager().beginTransaction()
										.replace(R.id.entry_pager_container, entryDetailPagerFragment)
										.commit();
		}
			
		createEntryList();
		getSupportFragmentManager().beginTransaction()
			.replace(R.id.entry_list, entryListFragment)
			.commit();
	}

	private void createEntryList()
	{
		entryListFragment = new EntryListFragment();
		Bundle fragmentArgs = new Bundle();
		fragmentArgs.putInt(EntryListFragment.STATE_ACTIVATED_POSITION, mSelectedItem);
		fragmentArgs.putSerializable(EntryListActivity.QUERY, query);
		fragmentArgs.putBoolean(EntryListFragment.ENABLE_DRAG, enableDrag);
		fragmentArgs.putInt(EntryListActivity.SORT_ORDER, sortOrder);
		fragmentArgs.putInt(EntryListActivity.SORT_DIRECTION, sortDirection);
		entryListFragment.setArguments(fragmentArgs);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mSelectedItem != ListView.INVALID_POSITION)
			outState.putInt(SELECTED_ITEM, mSelectedItem);
		
		outState.putInt(SELECTED_PAGE_VIEW, activatedPageViewId);
		outState.putSerializable(EntryListActivity.QUERY, query);
		outState.putBoolean(EntryListFragment.ENABLE_DRAG, enableDrag);
		outState.putInt(EntryListActivity.SORT_ORDER, sortOrder);
		outState.putInt(EntryListActivity.SORT_DIRECTION, sortDirection);
	}

	private void initializeDetailView(Adapter adapter) {
		mPagerAdapter = new EntryDetailPagerAdapter(getSupportFragmentManager(), adapter);
	}

	private void deactivateDetailView() {
		getSupportFragmentManager().beginTransaction()
			.remove(entryDetailPagerFragment)
			.show(entryListFragment)
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
			.commit();
		entryDetailPagerFragment = null;
		detailViewActive = false;
	}

	public void pagerViewCreated() {
		if(mTwoPane || detailViewActive){
			initializeDetailView(entryAdapter);
			entryDetailPagerFragment.show();
			entryDetailPagerFragment.setAdapter(mPagerAdapter);
			entryDetailPagerFragment.setCurrentItem(mSelectedItem);
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(mTwoPane)
			entryDetailPagerFragment.setPageView(activatedPageViewId);
	}
	
    @Override
    public void onBackPressed() {
        if (detailViewActive){
    		deactivateDetailView();
        } else {
        	Intent i = new Intent();
        	i.putExtra(SELECTED_ITEM, mSelectedItem);
        	i.putExtra(SCROLL_POSITION, entryListFragment.getScrollPosition());
        	setResult(Activity.RESULT_OK, i);
			super.onBackPressed();
		} 
    }

	private void setQuery() {
		if( query instanceof QueryElementSearchLike ) {
			mainMenu.setQueryValue( ((QueryElementSearchLike)query).query(), new ISearchViewCloseListener(){
				public void searchViewClosed() {
					navigateUp();
				}
			});
		}else{
			mainMenu.clearQueryValue();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
		    case android.R.id.home:
		    	navigateUp();
		        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

	private void navigateUp() {
		NavUtils.navigateUpFromSameTask(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
	    setQuery();
	    return result;
	}

	/**
	 * Callback method from {@link EntryListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(int pos, String id) {
		EratosApplication.getApplication().setLastViewedRefKey(id);
		
		mSelectedItem = pos;
		if (mTwoPane) {
			entryDetailPagerFragment.show();
			entryDetailPagerFragment.setCurrentItem(pos);
		} else if(detailViewActive) {
			entryDetailPagerFragment.setCurrentItem(pos);
		} else {
			detailViewActive = true;
			entryListFragment.cancelAction();
			
			entryDetailPagerFragment = new EntryDetailPagerFragment();
			getSupportFragmentManager().beginTransaction()
										.add(R.id.entry_list, entryDetailPagerFragment)
										.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
										.commit();
		}
	}
	
	@Override
	public void selectEntry(String refKey) {
		int pos = entryListFragment.getItemPos(refKey);
		
		if(pos == ListView.INVALID_POSITION) {
			onItemDeselected();
			entryListFragment.setActivatedPosition(ListView.INVALID_POSITION);
		} else {
			onItemSelected(pos, refKey);
			entryListFragment.setActivatedPosition(pos);
		}
	}
	
	@Override
	public void onItemDeselected() {
		if (mTwoPane) {
			entryDetailPagerFragment.hide();
		}
	}
	
	@Override
	public boolean isLargeView(){
		return !mTwoPane;
	}

	@Override
	public void onPageSelected(int page) {
		String id = entryListFragment.getItemId(page);
		EratosApplication.getApplication().setLastViewedRefKey(id);
		
		entryListFragment.setActivatedPosition(page);
		mSelectedItem = page;
	}
	
	@Override
	public void onDataLoaded(Adapter adapter) {
		if(isDestroyed)
			return;
		
		entryAdapter = adapter;
		if(mTwoPane || detailViewActive){
			mPagerAdapter.setSource(adapter);
			mPagerAdapter.notifyDataSetChanged();
		}
		
		if(mSelectedItem != -1 && !detailViewActive){
			entryListFragment.setActivatedPosition(mSelectedItem);
			entryListFragment.setScroll(mScrollPosition);
		}
		
		if(mSelectedItem != -1 && mTwoPane){
			entryDetailPagerFragment.show();
			entryDetailPagerFragment.setCurrentItem(mSelectedItem);
		}
	}
	
	@Override
	protected void onDestroy() {
		isDestroyed = true;
		super.onDestroy();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(detailViewActive)
			deactivateDetailView();
	}

	@Override
	public void onLibraryOpened() {
		if(detailViewActive){
			deactivateDetailView();
		}else if(mTwoPane){
			initializeDetailView(null);
			entryListFragment.clearSelection();
		}
	}

	@Override
	protected SortSelectedCallback getSortSelectedCallback() {
		return new SortSelectedCallback() {
               public void sortSelected(int sortOrder, int sortDirection) {
            	   entryListFragment.setSortOrder(sortOrder, sortDirection);
               }
           };
	}

	@Override
	protected ActionCallback getActionCallback() {
		return this;
	}

	@Override
	public void onCacheCleared() {
		entryListFragment.onCacheClear();
	}

	@Override
	public int getActivatedPageView() {
		return activatedPageViewId;
	}

	@Override
	public void activatePageView(int viewId) {
		activatedPageViewId = viewId;
		EntryDetailFragment entryDetailFragment = (EntryDetailFragment) mPagerAdapter.getRegisteredFragment(mSelectedItem);
		entryDetailFragment.setPageView(viewId);
		entryDetailPagerFragment.setPageView(viewId);
	}
}
