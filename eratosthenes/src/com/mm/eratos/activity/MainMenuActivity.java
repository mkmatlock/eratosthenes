package com.mm.eratos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.view.MainMenu;
import com.mm.eratos.view.MainMenu.ActionCallback;
import com.mm.eratos.view.MainMenu.SortSelectedCallback;

public abstract class MainMenuActivity extends LicensedActivity {
	protected MainMenu mainMenu;
	private EratosLogger logger;

	protected abstract SortSelectedCallback getSortSelectedCallback();
	protected abstract ActionCallback getActionCallback();

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	    requestWindowFeature(Window.FEATURE_PROGRESS);
	    
	    logger = EratosApplication.getApplication().getLogManager().getLogger(getClass());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    
	    mainMenu = new MainMenu(this, inflater, menu, getSortSelectedCallback(), getActionCallback());
	    
	    if(EratosApplication.getApplication().isFirstRun())
			mainMenu.showAbout();
	    
	    return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(mainMenu != null)
			mainMenu.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    if(mainMenu != null)
	    	mainMenu.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(mainMenu != null)
			mainMenu.onPause();
	}
	
	protected EratosLogger getLogger() {
		return logger;
	}
}