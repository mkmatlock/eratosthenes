package com.mm.eratos.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.OfflineException;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebClient.DownloadListener;

public abstract class AddEntryActivity extends FragmentActivity implements android.view.View.OnClickListener {
	private static final int SUCCESS_COLOR = 0xFF00FF88;
	private static final int FAIL_COLOR = 0xFFFF6622;
	
	public static final int SUCCESS=0;
	public static final int FAILED_IO=1;
	public static final int FAILED_PARSE=2;
	public static final int FAILED_OFFLINE=3;
	
	public static abstract class StreamLoader {
		protected abstract InputStream load(Intent intent) throws IOException;
	}
	
	public static abstract class PostProcessor {
		protected abstract BibTeXEntryModel process(BibTeXEntryModel entry, Intent intent, DownloadListener downloadListener) throws IOException, FieldRequiredException, ParseException;
	}
	
	protected class AddEntryTask extends AsyncTask<Intent, Void, String> {
		protected String doInBackground(Intent ... args) {
			try {
				InputStream is = getStreamLoader().load(args[0]);
				BufferedReader in = new BufferedReader(new InputStreamReader(is));
				
				StringBuilder sb = new StringBuilder();
				String line = null;
				while((line = in.readLine()) != null){
					sb.append(line);
					sb.append("\n");
				}
				
				return sb.toString();
	        } catch(OfflineException oe){
	        	errorType = FAILED_OFFLINE;
	        	exception = oe;
	        } catch (IOException e) {
	        	errorType = FAILED_IO;
	        	exception = e;
			}
	        return null;
		}
		
		protected void onPostExecute(String result) {
			handleRawBibtex(result);
	    }
	}
	
	private class EntryPostProcessTask extends AsyncTask<Intent, Integer, List<BibTeXEntryModel>> {
		private List<BibTeXEntryModel> entries;
		private MultiProgressDialog progressDialog;
		private DownloadListener downloadListener;
		
		public EntryPostProcessTask(List<BibTeXEntryModel> entries) {
			this.entries = entries;
			this.downloadListener = new DownloadListener() {
				public void progress(long downloaded, long fileSize) {
					publishProgress((int)downloaded, (int)fileSize);
				}
			};
		}
		
		protected void onPreExecute() {
			progressDialog = MultiProgressDialog.newInstance("Parse Entry");
			progressDialog.show(getFragmentManager(), "pdialog");
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			progressDialog.setPrimarySubtitle("Fetching resource...");
			progressDialog.setPrimaryProgress(0, 0);
			if(values.length>0){
				progressDialog.setSecondarySubtitle("Downloading PDF...");
				progressDialog.setSecondaryProgress(values[0], values[1]);
			}
		}

		protected List<BibTeXEntryModel> doInBackground(Intent ... args) {
			publishProgress();
			
			List<BibTeXEntryModel> result = new ArrayList<BibTeXEntryModel>();
			for(BibTeXEntryModel entry : entries){
				try{
					BibTeXEntryModel processed = getPostProcessor().process(entry, args[0], downloadListener);
					result.add(processed);
				}catch(Exception e){
					result.add(entry);
				}
			}
	        return result;
		}
		
		protected void onPostExecute(List<BibTeXEntryModel> result) {
			progressDialog.dismissAllowingStateLoss();
			
			addEntries(result);
			if(result.size()>0){
				String refKey = result.get(0).getRefKey();
				EratosApplication.getApplication().setLastViewedRefKey(refKey);
			}
			finish();
	    }
	}
	
	private BibTeXContentProviderAdapter adapter;
	private Button importButton;
	private Button cancelButton;
	private EditText editField;
	
	public int errorType;
    public Exception exception;
    
	private View statusView;
	protected TextView statusText;
	private ImageView statusIcon;
	private View statusProgress;
	protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
	private boolean formattedKeys;
	private BibTeXHelper helper;
	private BibTeXEntryModelFactory modelFactory;
	private EratosLogger logger;
    
	protected abstract StreamLoader getStreamLoader();
	protected abstract PostProcessor getPostProcessor();
	
	protected List<BibTeXEntryModel> encodeEntries(List<BibTeXEntry> importedEntries) throws ParseException {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		try{
			List<BibTeXEntryModel> entries = formatEntries(importedEntries, helper);
			
			boolean formatCiteKey = EratosApplication.getApplication().getSettingsManager().getCiteKeyAutoformat();
			if(formatCiteKey && !formattedKeys){
				entries = formatCiteKeys(entries, helper);
				updateEditor(entries);
				formattedKeys=true;
			}
			insertEntries(entries);
			
			return entries;
		}catch(ParseException e){
			throw e;
		}
	}
	
	private void updateEditor(List<BibTeXEntryModel> entries) {
		StringBuilder bibtex = new StringBuilder();
		for(BibTeXEntryModel entry : entries){
			bibtex.append(entry.getBibtex());
			bibtex.append("\n\n");
		}
		editField.setText(bibtex);
	}
	
	private List<BibTeXEntryModel> formatEntries(List<BibTeXEntry> importedEntries, BibTeXHelper helper) throws ParseException {
		List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>(importedEntries.size());
		
		boolean convLaTeX = EratosApplication.getApplication().getSettingsManager().getConvertLaTeXtoUTF8();
		boolean convUTF8 = EratosApplication.getApplication().getSettingsManager().getConvertUTF8ToLaTeX();
		
		for(BibTeXEntry entry : importedEntries) {
			String entryKey = entry.getKey().getValue();
			try {
				if(convLaTeX)
					entry = helper.decodeLaTeX(entry, helper);
				if(convUTF8)
					entry = helper.decodeUnicode(entry);
				
				String now = dateFormat.format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
				entry.addField(BibTeXEntryModel.CREATED_KEY, new StringValue(now, Style.BRACED));
				entry.addField(BibTeXEntryModel.MODIFIED_KEY, new StringValue(now, Style.BRACED));
				
				BibTeXEntryModel model = modelFactory.construct(entry);
				BibTeXEntryVerifier.checkEntry(model.getType(), model.allFields(), model.hasXref(), helper.warningsAsErrors());
				
				entries.add(model);
			} catch (Exception e) {
				throw new ParseException("Error while parsing entry '"+entryKey+"': " + StringUtils.oneliner(e.getMessage()));
			}
		}
		return entries;
	}

	private void insertEntries(List<BibTeXEntryModel> entries) throws ParseException {
		Set<String> addedKeys = new HashSet<String>();
		for(BibTeXEntryModel model : entries){
			String entryKey = model.getRefKey();
			try{
				if(adapter.hasEntry(entryKey) || addedKeys.contains(entryKey))
					throw new KeyCollisionException("RefKey conflict '" + entryKey + "' already exists in the database");
				addedKeys.add(entryKey);
			} catch (Exception e) {
				throw new ParseException("Error while parsing entry '"+entryKey+"': " + StringUtils.oneliner(e.getMessage()));
			}
		}
	}

	private List<BibTeXEntryModel> formatCiteKeys(List<BibTeXEntryModel> entries, BibTeXHelper helper) throws ParseException {
		List<BibTeXEntryModel> newEntries = new ArrayList<BibTeXEntryModel>(entries.size());
		CiteKeyFormatter citeKeyFormatter = EratosApplication.getApplication().getSettingsManager().getCiteKeyFormatter();
		
		for(BibTeXEntryModel entry : entries) {
			String oKey = entry.getRefKey();
			String entryKey = citeKeyFormatter.format( entry );
			
			try{
				entry.setRefKey(entryKey);
				entry = modelFactory.commit(entry);
			} catch (Exception e) {
				entry.setRefKey(oKey);
			}
			
			newEntries.add(entry);
		}
		return newEntries;
	}
	protected void addEntries(List<BibTeXEntryModel> entries) {
		for(BibTeXEntryModel entry : entries){
			try {
				adapter.insert(entry);
			} catch (KeyCollisionException e) {
				throw new RuntimeException(e);
			}
		}
		String msg = String.format(Locale.getDefault(), "%d entries added successfully!", entries.size());
		NotificationUtils.longToast(this, msg);
		SaveDatabaseTask.autosaveDatabase(this, new FileSyncResultReceiver(this));
	}
	
	public BibTeXEntryModelFactory getModelFactory() {
		return modelFactory;
	}
	
	public EratosLogger getLogger(){
		return logger;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		createViewContent(savedInstanceState, R.layout.edit_raw_bibtex);
		processIntent();
	}
	
	protected void createViewContent(Bundle savedInstanceState, int layout_id) {
		super.onCreate(savedInstanceState);

		if(EratosApplication.getApplication().getCurrentLibrary() == null){
			NotificationUtils.notify(this, "Error", "Please open or create a bibtex database before attempting to add entries!", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
			return;
		}
		
		formattedKeys = false;
		adapter = new BibTeXContentProviderAdapter(getContentResolver());
		helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		logger = EratosApplication.getApplication().getLogManager().getLogger(AddEntryActivity.class); 
		modelFactory = new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(adapter), helper);
		
		setContentView(layout_id);
		setFinishOnTouchOutside(false);
		
		importButton = (Button) findViewById(R.id.import_button);
		importButton.setOnClickListener(this);
		
		importButton.setEnabled(false);
		
		cancelButton = (Button) findViewById(R.id.cancel_button);
		cancelButton.setOnClickListener(this);
		
		editField = (EditText) findViewById(R.id.bibtex_editor);
		
		statusView = findViewById(R.id.status_view);
		statusProgress = findViewById(R.id.status_wait);
		
		statusIcon = (ImageView) findViewById(R.id.status_icon);
		statusText = (TextView) findViewById(R.id.status_text);
	}
	
	protected void processIntent() {
		if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
			statusText.setText("Fetching entry...");
			new AddEntryTask().execute(getIntent());
		}else if (Intent.ACTION_SEND.equals(getIntent().getAction())) {
			statusText.setText("Fetching entry...");
			new AddEntryTask().execute(getIntent());
		}else{
			NotificationUtils.notifyException(this, "Error", 
					new Exception("No intent view data specified: " + getIntent()), 
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
		}
	}
	
	protected void handleRawBibtex(String result) {
		if(result != null){ 
			try{
				importButton.setEnabled(true);
				
				BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
				
				editField.setText(result);
				List<BibTeXEntry> importedEntries = helper.parseBibTeXEntries(new StringReader(result));
				List<BibTeXEntryModel> entries = encodeEntries(importedEntries);
				
				new EntryPostProcessTask(entries).execute(getIntent());
			} catch(NullPointerException npe){
				errorType = FAILED_IO;
	        	npe.printStackTrace();
	        	exception = npe;
			} catch (IOException e) {
	        	errorType = FAILED_IO;
	        	e.printStackTrace();
	        	exception = e;
			} catch (ParseException e) {
	        	errorType = FAILED_PARSE;
	        	e.printStackTrace();
	        	exception = e;
			}
		}
		notifyStatus(errorType);
	}

	protected void notifyStatus(int status) {
		statusProgress.setVisibility(View.GONE);
		
		if (status == SUCCESS){
			statusView.setBackgroundColor(SUCCESS_COLOR);
			statusIcon.setImageResource(R.drawable.navigation_accept);
			statusIcon.setVisibility(View.VISIBLE);
			statusText.setTextColor(0xFF000000);
			statusText.setText("Entry validated successfully!");
		} else if(status == FAILED_IO) {
			NotificationUtils.notifyException(this, "BibTeX Fetch Failed", exception, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
		} else if(status == FAILED_OFFLINE) {
			NotificationUtils.notify(this, "BibTeX Fetch Failed", "Not connected to the internet!", new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
		} else if(status == FAILED_PARSE) {
			statusView.setBackgroundColor(FAIL_COLOR);
			statusIcon.setImageResource(R.drawable.navigation_cancel);
			statusIcon.setVisibility(View.VISIBLE);
			statusText.setTextColor(0xFF000000);
			statusText.setText("BibTeX parser failed: " + exception.getMessage());
		}
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == importButton.getId()){
			handleRawBibtex(editField.getText().toString());
		}else if(v.getId() == cancelButton.getId()){
			finish();
		}
	}
	
}
