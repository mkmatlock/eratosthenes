package com.mm.eratos.activity;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;

import com.mm.eratos.protocol.ProtocolConnectionManager;

public class RemoteProtocolActivity extends FragmentActivity {
	public RemoteProtocolActivity() {
		super();
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    ProtocolConnectionManager.notifyOnResume(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ProtocolConnectionManager.notifyOnActivityResult(this, requestCode, resultCode, data);
	}

	@Override 
	protected void onPause(){
		super.onPause();
		ProtocolConnectionManager.notifyOnPause(this);
	}
}