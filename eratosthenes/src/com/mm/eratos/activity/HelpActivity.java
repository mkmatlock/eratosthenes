package com.mm.eratos.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.view.TutorialFactory;

public class HelpActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = new HelpSectionFragment();
			Bundle args = new Bundle();
			args.putInt(HelpSectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return 5;
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			return TutorialFactory.getHelpPageName(HelpActivity.this, position);
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class HelpSectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public HelpSectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.icon_cheat_sheet, container, false);
			TextView sectionLabel = (TextView) rootView.findViewById(R.id.section_label);
			TextView featureDesc = (TextView) rootView.findViewById(R.id.feature_description_text);
			final int sheetNumber = getArguments().getInt(ARG_SECTION_NUMBER)-1;
			
			sectionLabel.setText(TutorialFactory.getHelpPageName(getActivity(), sheetNumber));
			featureDesc.setText(TutorialFactory.getHelpPageText(getActivity(), sheetNumber));
			
			LinearLayout iconSet = (LinearLayout) rootView.findViewById(R.id.icon_cheat_sheet);			
			int numIcons = populateIconSheet(iconSet, sheetNumber, inflater);
			
			if(numIcons == 0){
				iconSet.setVisibility(View.GONE);
				rootView.findViewById(R.id.icon_cheat_label).setVisibility(View.GONE);
			}
			
			TextView moreInformationText = (TextView) rootView.findViewById(R.id.more_information_label);
			
			Button wikiLink = (Button) rootView.findViewById(R.id.goto_wiki_button);
			wikiLink.setEnabled(false);
			wikiLink.setVisibility(View.GONE);
			
			Button tutorialLink = (Button) rootView.findViewById(R.id.goto_tutorial_button);
			tutorialLink.setEnabled(false);
			tutorialLink.setVisibility(View.GONE);
			
			moreInformationText.setEnabled(false);
			moreInformationText.setVisibility(View.GONE);
			
			return rootView;
		}
	
		private int populateIconSheet(LinearLayout iconSet, int sheetNumber, LayoutInflater inflater) {
 			SparseArray<String> resources = TutorialFactory.getIconCheatSheet(sheetNumber);
			
			for(int i = 0; i < resources.size(); i++){
				int resource_id = resources.keyAt(i);
				String text = resources.get(resource_id);
				
				View item = inflater.inflate(R.layout.icon_cheat_element, iconSet, false);
				
				TextView tv = (TextView) item.findViewById(R.id.iconDescription);
				tv.setText(text);
				
				ImageView iv = (ImageView) item.findViewById(R.id.iconView);
				iv.setImageDrawable(getResources().getDrawable(resource_id));
				
				iconSet.addView(item);
			}
			
			return resources.size();
		}	
	}
}
