package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.ScienceDirectWebHandler;

public class AddEntryFromScienceDirect extends AddEntryWebHandlerActivity {

	@Override
	public AbstractWebHandler getHandler() {
		return new ScienceDirectWebHandler();
	}

}
