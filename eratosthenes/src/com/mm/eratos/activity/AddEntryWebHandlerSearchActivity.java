package com.mm.eratos.activity;

import java.io.IOException;
import java.io.InputStream;

import org.jbibtex.ParseException;

import android.content.Intent;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.WebHandlerFactory;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class AddEntryWebHandlerSearchActivity extends AddEntryActivity {

	public EratosUri getUri(Intent intent) throws IOException {
		String uriData = intent.getDataString();
		if(uriData == null)
			uriData = intent.getExtras().getString(Intent.EXTRA_TEXT);
		
		if(uriData != null){
			EratosUri uri = EratosUri.parseUri(uriData);
			if(uri == null)
				throw new IOException("Not a valid URI: " + uriData);
			return uri;
		}else{
			throw new IOException("No URI found in shared data");
		}
	}
	
	public AbstractWebHandler getHandler(EratosUri uri) throws IOException {
		AbstractWebHandler handler = WebHandlerFactory.getHandler(uri);
		if(handler == null)
			throw new IOException("No handler found for URI: " + uri);
		return handler;
	}
	
	protected class WebHandlerStreamLoader extends StreamLoader{
		@Override
		protected InputStream load(Intent intent) throws IOException {
			EratosUri uri = getUri(intent);
			AbstractWebHandler handler = getHandler(uri);
			return handler.bibtex(uri);
		}
	}
	
	protected class WebHandlerPostProcessor extends PostProcessor {
		
		@Override
		protected BibTeXEntryModel process(BibTeXEntryModel entry, Intent intent, DownloadListener downloadListener) throws IOException, FieldRequiredException, ParseException {
			EratosUri uri = getUri(intent);
			AbstractWebHandler handler = getHandler(uri);

			handler.extras(entry, uri);
			
			EratosUri pdfUri = handler.pdfUri(entry, uri);
			if(pdfUri != null)
				handler.downloadPdf(entry, pdfUri, downloadListener);
			
			return getModelFactory().commit(entry);
		}
	}
	
	@Override
	protected StreamLoader getStreamLoader() {
		return new WebHandlerStreamLoader();
	}

	@Override
	protected PostProcessor getPostProcessor() {
		return new WebHandlerPostProcessor();
	}
}
