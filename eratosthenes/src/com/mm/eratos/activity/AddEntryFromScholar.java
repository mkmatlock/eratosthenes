package com.mm.eratos.activity;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.jbibtex.ParseException;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.GoogleScholarHandler;
import com.mm.eratos.feeds.GoogleScholarHandler.ScholarEntry;
import com.mm.eratos.feeds.WebHandlerFactory;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class AddEntryFromScholar extends AddEntryActivity {
	GoogleScholarHandler handler = new GoogleScholarHandler();
	private WebView webView;
	private View editorView;
	private View webPanel;
	private Button extractButton;

	private ScholarEntry scholarEntry;
	private ScholarLoader bibtexLoader;
	private String scholarQuery;
	private String queryUrl;
	
//	protected class ScholarJSInterface
//	{
//	    @JavascriptInterface
//	    public void processHTML(final String html) {
//	    	getLogger().debug("Got HTML:\n %s", Jsoup.parse(html));
//	    }
//	}

	protected class ExtractButtonListener implements android.view.View.OnClickListener {
		public void onClick(View v) {
			String cookie = CookieManager.getInstance().getCookie("scholar.google.com");
			getLogger().debug("Cookie: %s", cookie);
			
			webPanel.setVisibility(View.GONE);
    		editorView.setVisibility(View.VISIBLE);
			processScholarResults( cookie );
		}
	}
	
	protected class ScholarWebViewClient extends WebViewClient {
		ProgressDialog pd;

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			if(pd != null)
				pd = ProgressDialog.show(AddEntryFromScholar.this, "Loading Scholar", "Loading...");
		}

		@Override
	    public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			if(pd!=null) {
				pd.hide();
				pd=null;
			}
			getLogger().debug("Loaded: %s", url);
			queryUrl = url;
			
//			webView.loadUrl("javascript:setTimeout(function(){window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>')}, 2000);");
		}
	}
	
	public class ScholarLoader extends StreamLoader {
		String cookie;
		public ScholarLoader(String queryUrl, String cookie){
			this.cookie = cookie;
		}

		@Override
		protected InputStream load(Intent intent) throws IOException {
//			String userAgent = WebSettings.getDefaultUserAgent(AddEntryFromScholar.this);
			
			List<ScholarEntry> entries = handler.getEntries(queryUrl, cookie);
			
			getLogger().debug("Got %d entries from page", entries.size());
			
			if(entries.size() == 0)
				throw new IOException("No entry for query: " + scholarQuery);
			
			scholarEntry = entries.get(0);
			getLogger().debug("Entry HTML:\n%s", scholarEntry.getHtml());
			
			final EratosUri bibUri = scholarEntry.retrieveBibtexUri();
			runOnUiThread(new Runnable() {
				public void run(){
					webView.loadUrl(bibUri.toString());
				}
			});
			
			String bibtex = handler.getBibtex(bibUri, cookie);
			getLogger().debug("Found bibtex content:\n%s", bibtex);
			
			return new ByteArrayInputStream(bibtex.getBytes());
		}
	}
	
    public class RetrievePdfProcessor extends PostProcessor {
		private BibTeXEntryModelFactory modelFactory;
		
		public RetrievePdfProcessor(BibTeXEntryModelFactory modelFactory){
			this.modelFactory = modelFactory;
		}
		
		@Override
		protected BibTeXEntryModel process(BibTeXEntryModel entry, Intent intent, DownloadListener downloadListener) throws IOException, FieldRequiredException, ParseException {
			Context mContext = AddEntryFromScholar.this;
			
			if(scholarEntry.hasWeb()){
				EratosUri webUri = scholarEntry.getWebUri();
				AbstractWebHandler handler = WebHandlerFactory.getHandler(webUri);
				if(handler != null)
					handler.extras(entry, webUri);
			}
			
			if(scholarEntry.hasPdf()){
				EratosApplication app = EratosApplication.getApplication();
				
				String fn = entry.getSuggestedAttachmentName() + ".pdf";
				FileManager fileManager = FileManager.getFileManager();
				FileRevisionContentProviderAdapter fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
				
				EratosUri relUri = fileManager.getDefaultAttachmentUri(fn);
				EratosUri fileUri = relUri.resolveUri(app);
				boolean isRemote = fileUri.isRemoteProtocol(app);
				
				File file = fileManager.getFile(fileUri);
				if(file.exists() || isRemote && fileAdapter.exists(fileUri))
					return entry;
				
				try{
					scholarEntry.retrievePdf(fileUri.resolveUri(app), downloadListener);
					
					if(fileUri.isRemoteProtocol(app) && !fileAdapter.exists(fileUri))
						fileAdapter.newSyncedFile(fileUri, "", file, entry.important());
					
					entry.attachFile(relUri);
					entry = modelFactory.commit(entry);
				}catch(IOException e){
					NotificationUtils.notifyException(mContext, "PDF Download Failed", e);
				}
			}
			return entry;
		}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	createViewContent(savedInstanceState, R.layout.edit_raw_bibtex_background_webview_layout);
    	setupWebView();
    	processIntent();
    }

	@SuppressLint("SetJavaScriptEnabled")
	private void setupWebView() {
    	webView = (WebView) findViewById(R.id.web_view);
    	editorView = findViewById(R.id.editor_view);
    	webPanel = findViewById(R.id.web_panel);
    	extractButton = (Button)findViewById(R.id.extract_button);
    	extractButton.setOnClickListener(new ExtractButtonListener());
    	
    	editorView.setVisibility(View.GONE);
    	webPanel.setVisibility(View.VISIBLE);
    	
    	webView.getSettings().setJavaScriptEnabled(true);
//    	webView.addJavascriptInterface(new ScholarJSInterface(), "HTMLOUT");

    	webView.setWebViewClient(new ScholarWebViewClient());
	}

	@Override
    protected void processIntent(){
		if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
			statusText.setText("Fetching entry...");
			fetchScholarEntry(getIntent());
		}else if (Intent.ACTION_SEND.equals(getIntent().getAction())) {
			statusText.setText("Fetching entry...");
			fetchScholarEntry(getIntent());
		}else{
			NotificationUtils.notifyException(this, "Error", 
					new Exception("No intent view data specified: " + getIntent()), 
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
		}
    }

	private void fetchScholarEntry(Intent intent) {
		scholarQuery = GoogleScholarHandler.parseSearchUri(intent.getDataString());
		String uri = GoogleScholarHandler.generateSearchUri(scholarQuery);
		webView.loadUrl(uri);
	}
	
    public void processScholarResults(String cookie) {
    	bibtexLoader = new ScholarLoader(queryUrl, cookie);
    	new AddEntryTask().execute(getIntent());
	}
    
	@Override
	protected PostProcessor getPostProcessor() {
		return new RetrievePdfProcessor(getModelFactory());
	}
	
	
	protected StreamLoader getStreamLoader() {
		return bibtexLoader;
	}
}
