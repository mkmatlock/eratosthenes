package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.PLOSWebHandler;

public class AddEntryFromPLOS extends AddEntryWebHandlerActivity {

	@Override
	public AbstractWebHandler getHandler() {
		return new PLOSWebHandler();
	}

}
