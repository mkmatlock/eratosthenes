package com.mm.eratos.activity;

import com.mm.eratos.feeds.ACSWebHandler;
import com.mm.eratos.feeds.AbstractWebHandler;

public class AddEntryFromACS extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new ACSWebHandler();
	}
}