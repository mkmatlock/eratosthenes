package com.mm.eratos.activity;

import java.io.IOException;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.fragment.EntryGroupFragment;
import com.mm.eratos.fragment.EntryListFragment;
import com.mm.eratos.fragment.RecentFilesFragment;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.task.ProtocolCheckUpdateTask;
import com.mm.eratos.protocol.task.ProtocolCheckUpdateTask.Callback;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementHasKeyword;
import com.mm.eratos.search.db.QueryElementInCollection;
import com.mm.eratos.search.db.QueryElementInGroup;
import com.mm.eratos.search.db.QueryElementIsImportant;
import com.mm.eratos.search.db.QueryElementIsUnread;
import com.mm.eratos.search.db.QueryElementTrue;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.view.MainMenu.ActionCallback;
import com.mm.eratos.view.MainMenu.SortSelectedCallback;
import com.mm.eratos.view.RemoveTargetDragListener;

public class EntryGroupActivity extends MainMenuActivity implements EntryGroupFragment.Callbacks, 
		EntryListFragment.Callbacks, ActionCallback, RecentFilesFragment.Callbacks  {
	private static final String SELECTED_CATEGORY = "selectedCategory";
	private static final String SELECTED_ITEM = "selectedItem";
	private static final int MAX_CHECK_AGE = 15 * 60 * 1000;

	public static int LIST_ACTIVITY_CODE = 44;
	
	boolean mTwoPane;
	private QueryElement query;
	EntryListFragment entryList;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private TextView mRemoveDropTarget;

	String selectedCategory;
	String selectedItem;
	
	
	private EntryGroupFragment entryGroupFragment;
	private RemoveTargetDragListener removeTargetDragListener;

	private int newSelectedItem;
	private int newScrollPosition;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private RecentFilesFragment recentFilesFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_group_list);
		
		bibtexAdapter = new BibTeXContentProviderAdapter(this.getContentResolver());
		mTwoPane = false;
		query = new QueryElementTrue();
		
		newSelectedItem = -1;
		newScrollPosition = -1;
		
		if(savedInstanceState != null)
			query = (QueryElement)savedInstanceState.getSerializable(EntryListActivity.QUERY);
		
		if (getResources().getBoolean(R.bool.large_screen_mode)) {
			mTwoPane = true;
			
			mRemoveDropTarget = (TextView) findViewById(R.id.removeTarget);
			removeTargetDragListener = new RemoveTargetDragListener(this, mRemoveDropTarget);
			mRemoveDropTarget.setOnDragListener(removeTargetDragListener);
			
			if( query instanceof QueryElementIsImportant )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_DATABASE, EntryGroupFragment.DATABASE_IMPORTANT);
			else if( query instanceof QueryElementIsUnread )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_DATABASE, EntryGroupFragment.DATABASE_UNREAD);
			else if( query instanceof QueryElementInGroup )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_GROUPS, ((QueryElementInGroup)query).group());
			else if( query instanceof QueryElementInCollection )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_COLLECTIONS, ((QueryElementInCollection)query).collection());
			else if( query instanceof QueryElementHasKeyword )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_KEYWORDS, ((QueryElementHasKeyword)query).keyword());
			else if( query instanceof QueryElementTrue )
				removeTargetDragListener.setSelectedCategory(EntryGroupFragment.CATEGORY_DATABASE, EntryGroupFragment.DATABASE_LIBRARY);
			else
				removeTargetDragListener.setDisabled();
		}
		
		entryGroupFragment = (EntryGroupFragment) getSupportFragmentManager().findFragmentById(R.id.group_list); 
		entryList = null;

		if(savedInstanceState != null && mTwoPane)
			openEntryList( query );
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.recent_drawer_open,
                R.string.recent_drawer_close );
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        recentFilesFragment = (RecentFilesFragment) getSupportFragmentManager().findFragmentById(R.id.recent_drawer);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		
		if(EratosApplication.getApplication().lastSyncCheckAge() > MAX_CHECK_AGE)
			checkForChanges();
	}
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) 
        	return true;
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void groupSelected(String category){
		if(EntryGroupFragment.CATEGORY_GROUPS.equals(category)){
			NotificationUtils.promptInput(this, "New Group", new IStringResultListener() {
				@Override
				public boolean onResult(String result, DialogInterface dialog) {
					return createNewGroup(result);
				}
			});
		}
		if(EntryGroupFragment.CATEGORY_COLLECTIONS.equals(category)){
			NotificationUtils.promptInput(this, "New Collection", new IStringResultListener() {
				@Override
				public boolean onResult(String result, DialogInterface dialog) {
					return createNewCollection(result);
				}
			});
		}
		if(EntryGroupFragment.CATEGORY_KEYWORDS.equals(category)){
			NotificationUtils.promptInput(this, "New Keyword", new IStringResultListener() {
				@Override
				public boolean onResult(String result, DialogInterface dialog) {
					return createNewKeyword(result);
				}
			});
		}
	}
	
	
	protected boolean createNewGroup(String result) {
		if(result.trim().isEmpty())
			return false;
		return bibtexAdapter.createGroup(result);
	}
	
	protected boolean createNewCollection(String result) {
		if(result.trim().isEmpty())
			return false;
		return bibtexAdapter.createCollection(result);
	}
	
	protected boolean createNewKeyword(String result) {
		if(result.trim().isEmpty())
			return false;
		return bibtexAdapter.createKeyword(result);
	}

	@Override
	public void applyFilter(QueryElement query){
		if(mTwoPane && entryList != null)
			entryList.cancelAction();

		newScrollPosition = 0;
		newSelectedItem = -1;
		
		openEntryList( query );
	}
	
	@Override
	public void groupItemSelected(String category, String item) {
		if(mTwoPane && entryList != null)
			entryList.cancelAction();
		
		newScrollPosition = 0;
		newSelectedItem = -1;
		
		if(EntryGroupFragment.CATEGORY_DATABASE.equals(category))
			selectDatabase(item);
		else if(EntryGroupFragment.CATEGORY_FILTERS.equals(category))
			selectFilter(item);
		else if(EntryGroupFragment.CATEGORY_GROUPS.equals(category))
			selectGroup(item);
		else if(EntryGroupFragment.CATEGORY_COLLECTIONS.equals(category))
			selectCollection(item);
		else if(EntryGroupFragment.CATEGORY_KEYWORDS.equals(category))
			selectKeyword(item);
	}

	@Override
	public void groupItemDeselected() {
		
	}

	private void selectFilter(String filter) {
		selectedCategory = EntryGroupFragment.CATEGORY_KEYWORDS;
		selectedItem = filter;
		
		if(mTwoPane)
			removeTargetDragListener.setDisabled();
		try{
			openEntryList( bibtexAdapter.getFilterByName(filter) );
		}catch(IOException e){
			NotificationUtils.notifyException(this, "Unexpected Exception", e);
		}
	}
	
	private void selectKeyword(String keyword) {
		selectedCategory = EntryGroupFragment.CATEGORY_KEYWORDS;
		selectedItem = keyword;
		
		if(mTwoPane){
			removeTargetDragListener.setSelectedCategory(selectedCategory, selectedItem);
			removeTargetDragListener.setEnabled();
		}
		openEntryList( new QueryElementHasKeyword(keyword) );
	}
	

	private void selectCollection(String collection) {
		selectedCategory = EntryGroupFragment.CATEGORY_COLLECTIONS;
		selectedItem = collection;
		
		if(mTwoPane){
			removeTargetDragListener.setSelectedCategory(selectedCategory, selectedItem);
			removeTargetDragListener.setEnabled();
		}
		openEntryList( new QueryElementInCollection(collection) );
	}


	private void selectGroup(String group) {
		if(group.equals(BibTeXContentProviderAdapter.NO_GROUP_FIELD_TITLE)){
			group = "";
		}
		
		selectedCategory = EntryGroupFragment.CATEGORY_GROUPS;
		selectedItem = group;
		
		if(mTwoPane){
			removeTargetDragListener.setSelectedCategory(selectedCategory, selectedItem);
			removeTargetDragListener.setEnabled();
		}
		openEntryList( new QueryElementInGroup(group) );
	}


	private void selectDatabase(String db) {
		selectedCategory = EntryGroupFragment.CATEGORY_DATABASE;
		selectedItem = db;
		
		if(mTwoPane){
			removeTargetDragListener.setSelectedCategory(selectedCategory, selectedItem);
			removeTargetDragListener.setEnabled();
		}
		if(EntryGroupFragment.DATABASE_LIBRARY.equals(db)){
			openEntryList( new QueryElementTrue() );
		}else if(EntryGroupFragment.DATABASE_UNREAD.equals(db)){
			openEntryList( new QueryElementIsUnread() );
		}else if(EntryGroupFragment.DATABASE_IMPORTANT.equals(db)){
			openEntryList( new QueryElementIsImportant()  );
		} 
	}


	private void openEntryList(QueryElement query) {
		this.query = query;
		
		if(mTwoPane){
			entryList = new EntryListFragment();
			
			Bundle args = new Bundle();
			args.putSerializable( EntryListActivity.QUERY, query );
			args.putBoolean( EntryListFragment.ENABLE_DRAG, true );
			
			entryList.setArguments(args);
			
			getSupportFragmentManager().beginTransaction()
										.replace(R.id.entry_list_container, entryList)
										.commit();
		} else {
			Intent i = new Intent(this, EntryListActivity.class);
			i.putExtra(EntryListActivity.QUERY, query);
			startActivity(i);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putSerializable(EntryListActivity.QUERY, query);
		outState.putString(EntryGroupActivity.SELECTED_CATEGORY, selectedCategory);
		outState.putString(EntryGroupActivity.SELECTED_ITEM, selectedItem);
	}
	
	@Override
	public boolean isLargeView(){
		return true;
	}

	@Override
	public void onItemSelected(int pos, String id) {
		entryGroupFragment.cancelAction();
		
		Intent i = new Intent(this, EntryListActivity.class);
		i.putExtra(EntryListActivity.QUERY, query);
		i.putExtra(EntryListActivity.SELECTED_ITEM, pos);
		i.putExtra(EntryListActivity.SCROLL_POSITION, entryList.getScrollPosition());
		
		startActivityForResult(i, LIST_ACTIVITY_CODE);
	}
	
	@Override
	public void onItemDeselected() {
		
	}
	
	@Override
	public void onDataLoaded(Adapter adapter) {
		if(newSelectedItem != -1){
			entryList.setActivatedPosition(newSelectedItem);
			entryList.setScroll(newScrollPosition);
			
			newSelectedItem = -1;
			newScrollPosition = 0;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == LIST_ACTIVITY_CODE && resultCode == Activity.RESULT_OK){
			Bundle args = data.getExtras();

			newSelectedItem = args.getInt(EntryListActivity.SELECTED_ITEM, -1);
			newScrollPosition = args.getInt(EntryListActivity.SCROLL_POSITION, 0);
			
			entryList.setActivatedPosition(newSelectedItem);
			entryList.setScroll(newScrollPosition);
		}
	}
	

	@Override
	public void onLibraryOpened() {
		if(mTwoPane){
			entryGroupFragment.onResetSelection();
			openEntryList( new QueryElementTrue() );
		}
		
		
	}

	@Override
	protected SortSelectedCallback getSortSelectedCallback() {
		return new SortSelectedCallback() {
			public void sortSelected(int  sortOrder, int sortDirection) {
				if(mTwoPane && entryList != null)
					entryList.setSortOrder(sortOrder, sortDirection);
			}
		};
	}

	@Override
	protected ActionCallback getActionCallback() {
		return this;
	}
	
	class SyncAvailableListener implements Callback {
		@Override
		public void updateAvailable() {
			setProgressBarIndeterminateVisibility(Boolean.FALSE);
			NotificationUtils.confirmDialog(EntryGroupActivity.this, 
					"Remote Version Changed", 
					"This file has been updated in your remote storage. You should sync to avoid losing changes.",
					"Sync", "Later",
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							mainMenu.sync();
						}
					});
			EratosApplication.getApplication().setSyncChecked();
		}
		
		@Override
		public void started() {
			setProgressBarIndeterminateVisibility(Boolean.TRUE);
			EratosApplication.getApplication().setSyncChecked();
		}
		
		@Override
		public void noChange() {
			setProgressBarIndeterminateVisibility(Boolean.FALSE);
			EratosApplication.getApplication().setSyncChecked();
		}
		
		@Override
		public void error(Throwable e) {
			setProgressBarIndeterminateVisibility(Boolean.FALSE);
			EratosApplication.getApplication().setSyncChecked();
		}
	};
	
	@Override
	public void onLibraryLoaded() {
		mDrawerLayout.closeDrawers();
		mainMenu.onLibraryOpened();
		recentFilesFragment.onLibraryOpened();
		checkForChanges();
	}

	private void checkForChanges() {
		EratosApplication app = EratosApplication.getApplication();
		final EratosUri libraryUri = app.getCurrentLibrary();
		
		if(app.canSync() && app.isOnline() && libraryUri != null && libraryUri.isRemoteProtocol(app)){
			final ProtocolHandler protocol = ProtocolHandler.getProtocolHandler(libraryUri);
			ProtocolConnectionManager connectionManager = ProtocolConnectionManager.getConnectionManager(libraryUri.protocol());
			connectionManager.connect(this,  new ProtocolConnectionManager.ConnectionListener() {
				public void failed(Exception e) {
					e.printStackTrace();
				}
				public void connecting() {
				}
				public void connected() {
					new ProtocolCheckUpdateTask(EntryGroupActivity.this, protocol, new SyncAvailableListener()).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, libraryUri);
				}
			});
		}
	}

	@Override
	public void onOpenLibraryRequest() {
		mainMenu.openDatabase();
	}

	@Override
	public void onCacheCleared() {
		if(mTwoPane && entryList != null)
			entryList.onCacheClear();
	}

	@Override
	public boolean isLarge() {
		return mTwoPane;
	}
}
