package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.WileyWebHandler;

public class AddEntryFromWileyOnline extends AddEntryWebHandlerActivity {

	@Override
	public AbstractWebHandler getHandler() {
		return new WileyWebHandler();
	}

}
