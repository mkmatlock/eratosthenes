package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.PubMedWebHandler;

public class AddEntryFromPubMed extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new PubMedWebHandler();
	}
}
