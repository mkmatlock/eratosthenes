package com.mm.eratos.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jbibtex.ParseException;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import com.mm.eratos.R;
import com.mm.eratos.adapter.EmptyChoiceAdapter;
import com.mm.eratos.adapter.FieldEditAdapter;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXAccentParser;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.view.LinearListView;

public class EditEntryActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>, OnItemSelectedListener, OnClickListener {

	private static final String REFKEY_ARG = "REFKEY";

	private SimpleCursorAdapter collectionAdapter;

	private static final int LIST_LOADER = 0x03;

	private List<String> refTypes;

	private BibTeXContentProviderAdapter adapter;
	private BibTeXEntryModel entry;
	private boolean editMode;
	
	private EditText refKeyEdit;
	private EditText titleEdit;
	private EditText summaryEdit;
	private Spinner typeEdit;
	private Spinner collectionEdit;
	private LinearListView fieldList;

	private Button cancelButton;
	private Button commitButton;

	private FieldEditAdapter fieldAdapter;

	private String oldKey;

	private boolean convertLaTeX;
	private boolean convertUTF8;
	private boolean editUTF8;

	private BibTeXHelper helper;

	private BibTeXEntryModelFactory modelFactory;

	private BibTeXContentProviderReferenceResolver resolver;

	private BibTeXContentProviderAdapter bibtexAdapter;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String refKey = getIntent().getStringExtra(REFKEY_ARG);
		editMode = refKey != null;
		
		bibtexAdapter = new BibTeXContentProviderAdapter(getContentResolver());
		helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		if(editMode){
			oldKey = refKey;
			entry = bibtexAdapter.getEntry(refKey, modelFactory);
		}else{
			oldKey = null;
			entry = new BibTeXEntryModel();
		}
		
		setContentView(initFields());
		setFinishOnTouchOutside(false);
		
		buildAdapter();
	}
	
	private void buildAdapter() {
		String[] typeArray = BibTeXEntryModel.TYPE_SET.toArray(new String[]{});
		refTypes = Arrays.asList(typeArray);
		Collections.sort(refTypes);
		
		int listItemView = android.R.layout.simple_spinner_item;
		typeEdit.setAdapter(new ArrayAdapter<String>(this, listItemView, android.R.id.text1, refTypes));
		
		collectionAdapter = new SimpleCursorAdapter(this, listItemView, null, 
	  			   new String[]{DBHelper.COLUMN_COLLECTION}, new int[]{android.R.id.text1},
					CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER );
		collectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		EmptyChoiceAdapter emptyChoiceAdapter = new EmptyChoiceAdapter(this, collectionAdapter);
		
		collectionEdit.setAdapter(emptyChoiceAdapter);
		
		getSupportLoaderManager().initLoader(LIST_LOADER, null, this);
	}

	private View initFields() {
		adapter = new BibTeXContentProviderAdapter(getContentResolver());

		View layout = getLayoutInflater().inflate(R.layout.edit_entry, null);
		typeEdit = (Spinner) layout.findViewById(R.id.documentType);
		refKeyEdit = (EditText) layout.findViewById(R.id.refKey);
		titleEdit = (EditText) layout.findViewById(R.id.documentTitle);
		summaryEdit= (EditText) layout.findViewById(R.id.documentSummary);
		collectionEdit = (Spinner) layout.findViewById(R.id.collectionName);
		fieldList = (LinearListView) layout.findViewById(R.id.fieldList);

		cancelButton = (Button) layout.findViewById(R.id.cancel_button);
		commitButton = (Button) layout.findViewById(R.id.commit_button);
		
		cancelButton.setOnClickListener(this);
		commitButton.setOnClickListener(this);
		
		convertLaTeX = EratosApplication.getApplication().getSettingsManager().getConvertLaTeXtoUTF8();
		convertUTF8 = EratosApplication.getApplication().getSettingsManager().getConvertUTF8ToLaTeX();
		editUTF8 = EratosApplication.getApplication().getSettingsManager().getEditInUTF8ConvertToLaTeX();
		
		typeEdit.setOnItemSelectedListener(this);
		return layout;
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return adapter.loadCollections(this);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		collectionAdapter.swapCursor(cursor);
		fillEditFields();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		collectionAdapter.swapCursor(null);
	}

	private void fillEditFields() {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		String tV = entry.getFieldValue(BibTeXEntryModel.TITLE_KEY.getValue(), editUTF8, helper);
		String sV = entry.getFieldValue(BibTeXEntryModel.ABSTRACT_KEY.getValue(), editUTF8, helper);
		
		refKeyEdit.setText(entry.getRefKey());
		titleEdit.setText(tV == null ? "" : tV);
		summaryEdit.setText(sV == null ? "" : sV);
		typeEdit.setSelection(0);
		collectionEdit.setSelection(0);
		
		String entryType = entry.getType();
		if(BibTeXEntryModel.TYPE_SET.contains(entryType)){
			typeEdit.setSelection(refTypes.indexOf(entryType.toLowerCase(Locale.US)));
			fieldList.setAdapter(createFieldEditAdapter(entryType));
		}
		
		String entryCollection = entry.getCollection();
		for(int i = 0; i < collectionAdapter.getCount(); i++){
			Cursor row = (Cursor) collectionAdapter.getItem(i);
			
			String collectionName = row.getString( row.getColumnIndex(DBHelper.COLUMN_COLLECTION) );
			if(collectionName.equals(entryCollection)){
				collectionEdit.setSelection(i + 1);
				break;
			}
		}
	}
	
	private Adapter createFieldEditAdapter(String type) {
		Map<String, String> values = new HashMap<String, String>();
		List<String> keyList = new ArrayList<String>();
		if(fieldAdapter == null){
			for(String key : BibTeXEntryVerifier.getRequiredFields(type)){
				String fieldValue = entry.getFieldValue(key, editUTF8, helper);
				values.put(key, fieldValue == null ? "" : fieldValue);
				keyList.add(key);
			}for(String key : BibTeXEntryVerifier.getOptionalFields(type)){
				String fieldValue = entry.getFieldValue(key, editUTF8, helper);
				values.put(key, fieldValue == null ? "" : fieldValue);
				keyList.add(key);
			}
		} else {
			Map<String, String> oldValues = fieldAdapter.updateValues();
			
			for(String key : BibTeXEntryVerifier.getRequiredFields(type)){
				String oldValue = "";
				if(oldValues.containsKey(key))
					oldValue = oldValues.get(key);
				else if(entry.hasField(key))
					oldValue = entry.getFieldValue(key, editUTF8, helper);
				values.put(key, oldValue);
				keyList.add(key);
			}for(String key : BibTeXEntryVerifier.getOptionalFields(type)){
				String oldValue = "";
				if(oldValues.containsKey(key))
					oldValue = oldValues.get(key);
				else if(entry.hasField(key))
					oldValue = entry.getFieldValue(key, editUTF8, helper);
				values.put(key, oldValue);
				keyList.add(key);
			}
		}
		
		if(values.containsKey(BibTeXEntryVerifier.TITLE_KEY)){
			values.remove(BibTeXEntryVerifier.TITLE_KEY);
			keyList.remove(BibTeXEntryVerifier.TITLE_KEY);
		}
		
		fieldAdapter = new FieldEditAdapter(this, keyList, values);
		return fieldAdapter;
	}

	public boolean handleResult() {
		try{
			String refKey = refKeyEdit.getText().toString().trim();
			String title = titleEdit.getText().toString().trim();
			
			BibTeXEntryModel copy = entry.copy();
			
			if(refKey.isEmpty())
				throw new FieldRequiredException("Ref Key is required");
			if(typeEdit.getSelectedItem() == null)
				throw new FieldRequiredException("Document type is required");
			
			copy.setType( (String)typeEdit.getSelectedItem() );
			copy.setRefKey( refKey );
			
			parseAndSetField(BibTeXEntryModel.TITLE_KEY.getValue(), title, copy);
			
			if(collectionEdit.getSelectedItem() != null){
				Cursor row = (Cursor) collectionEdit.getSelectedItem();
				String collectionName = row.getString( row.getColumnIndex(DBHelper.COLUMN_COLLECTION) );
				copy.setCollection( collectionName );
			} else {
				copy.setCollection("");
			}
			copy.setSummary( summaryEdit.getText().toString() );
			
			Map<String, String> fieldMap = fieldAdapter.updateValues();
			for(String key : fieldMap.keySet())
				parseAndSetField(key, fieldMap.get(key), copy);
			
			BibTeXEntryModel newEntry = modelFactory.commit(copy);
			if(editMode && newEntry.getRefKey().equals(oldKey)) {
				adapter.update(newEntry);
			} else if(editMode){
				adapter.delete(oldKey);
				adapter.insert(newEntry);
			} else {
				adapter.insert(newEntry);
			}
			
			SaveDatabaseTask.autosaveDatabase(this, new FileSyncResultReceiver(this));
			
			List<Exception> warnings = newEntry.getWarnings();
			if(warnings.size() > 0) {
				NotificationUtils.notifyWarnings(this, "Entry Parser Warning", warnings, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
				return false;
			}
			
			return true;
		}catch(FieldRequiredException rfe){
			NotificationUtils.notify(this, "Required Field Missing", rfe.getMessage());
		}catch(KeyCollisionException key){
			NotificationUtils.notify(this, "RefKey In Use", "That refkey is already in use for another document!");
		}catch(ParseException pe){
			NotificationUtils.notify(this, "Syntax Error", pe.getMessage());
		}catch(Exception e){
			NotificationUtils.notifyException(this, "Unexpected Error", e);
		}
		
		return false;
	}
	

	private void parseAndSetField(String key, String value, BibTeXEntryModel entry) throws IOException, ParseException {
		if( convertUTF8 ){
			value = BibTeXAccentParser.unicodeToLatex(value);
			value = BibTeXAccentParser.newlinesToLaTeX(value);
		}
		
		if( convertLaTeX ){
			value = helper.printLaTeX( helper.parseLaTeX(value) );
		}
		entry.setField(key, value);
	}

	@Override
	public void onItemSelected(AdapterView<?> adapter, View view, int pos, long id) {
		String refType = (String)typeEdit.getSelectedItem();
		fieldList.setAdapter(createFieldEditAdapter(refType));
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapter) {
		
	}



	@Override
	public void onClick(View v) {
		if(v.getId() == cancelButton.getId()){
			finish();
		}else if(v.getId() == commitButton.getId()){
			if(handleResult()) finish();
		}
	}
	
	
	public static Intent editEntry(Context context, String refKey){
		Intent i = new Intent(context, EditEntryActivity.class);
		i.putExtra(REFKEY_ARG, refKey);
		return i;
	}
	
	public static Intent newEntry(Context context){
		return new Intent(context, EditEntryActivity.class);
	}
}
