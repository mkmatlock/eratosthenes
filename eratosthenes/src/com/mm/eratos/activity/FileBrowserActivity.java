package com.mm.eratos.activity;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.DirectoryAdapter;
import com.mm.eratos.drive.DriveProtocolHandler;
import com.mm.eratos.dropbox.DropboxProtocolHandler;
import com.mm.eratos.files.FileBrowser;
import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.DeviceProtocolHandler;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.NotificationUtils;

public class FileBrowserActivity extends RemoteProtocolActivity implements OnItemClickListener, OnClickListener
{	
	public interface FileFilter {
		public boolean filter(EratosUri uri, boolean isDir);
	}
	
	public static final int CHOOSE_FOLDER = 0;
	public static final int CHOOSE_FILE = 1;
	public static final String TYPE_ARG = "type";
	public static final String PATH_ARG = "path";
	public static final String EXISTING_FILES_ARG = "files";
	private static final String PROTOCOL_ARG = "protocol";
	private static final String PROTOCOL_LOCK_ARG = "protocol_lock";
	private static final String EXT_FILTER = "ext_filter";
	
	private static final String DEFAULT_PROTOCOL = EratosUri.FILE_PROTOCOL;
	
	private int fileType = CHOOSE_FILE;
	
	private EratosUri selectedPath;
	private ListView directoryListingView;
	private Button okButton;
	private Button cancelButton;
	private ImageButton newButton;
	private Button deviceButton;
	private Button dropboxButton;
	private Button driveButton;
	
	private boolean protocolLock;
	private ProtocolHandler protocolHandler;
	private String protocol;
	
	CaseInsensitiveMap<FileBrowser> cFileBrowser;
	
	private TextView currentDirectoryTextView;
	private String[] extFilter;
	private FileFilter fileFilter;
	private FileResult currentDir;
	private DirectoryAdapter adapter;
	private IFileOperationResultListener changeDirectoryListener;
	private boolean initialChdir;
	private ImageButton refreshButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_chooser_list_view);
		setFinishOnTouchOutside(false);
		
		fileType = getIntent().getIntExtra(TYPE_ARG, CHOOSE_FILE);
		protocolLock = getIntent().getBooleanExtra(PROTOCOL_LOCK_ARG, false);
		protocol = getIntent().getStringExtra(PROTOCOL_ARG);
		extFilter = getIntent().getStringArrayExtra(EXT_FILTER);
		
		if(protocol == null || protocol.isEmpty())
			protocol = DEFAULT_PROTOCOL;
		
		initialChdir = false;
		
		currentDirectoryTextView = (TextView) findViewById(R.id.currentFolderName);
		
		deviceButton = (Button) findViewById(R.id.device_button);
		dropboxButton = (Button) findViewById(R.id.dropbox_button);
		driveButton = (Button) findViewById(R.id.drive_button);
		
		okButton = (Button) findViewById(R.id.ok_button);
		cancelButton = (Button) findViewById(R.id.cancel_button);
		newButton = (ImageButton) findViewById(R.id.button_new_folder);
		refreshButton = (ImageButton) findViewById(R.id.button_refresh_folder);
		
		
		okButton.setOnClickListener(this);
		cancelButton.setOnClickListener(this);
		newButton.setOnClickListener(this);
		refreshButton.setOnClickListener(this);
		
		deviceButton.setOnClickListener(this);
		dropboxButton.setOnClickListener(this);
		driveButton.setOnClickListener(this);
		
		cFileBrowser = new CaseInsensitiveMap<FileBrowser>();
		cFileBrowser.put(EratosUri.FILE_PROTOCOL, new FileBrowser(this, new DeviceProtocolHandler(), deviceButton));
		cFileBrowser.put(EratosUri.DROPBOX_PROTOCOL, new FileBrowser(this, new DropboxProtocolHandler(), dropboxButton));
		cFileBrowser.put(EratosUri.DRIVE_PROTOCOL, new FileBrowser(this, new DriveProtocolHandler(), driveButton));
		
		directoryListingView = (ListView) findViewById(R.id.directory_listing);
		directoryListingView.setOnItemClickListener(this);
		directoryListingView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		changeDirectoryListener = new IFileOperationResultListener() {
			private ProgressDialog progress;
			public void finished(FileResult result) {
				if(progress != null && progress.isShowing())
					progress.dismiss();
				
				currentDir = result;
				cFileBrowser.get(protocol).setCurrentDirectory(currentDir.uri());
				
				initialChdir = false;
				refresh();
			}
			public void failed(Throwable error) {
				progress.dismiss();
				
				if(initialChdir)
					navigate(cFileBrowser.get(protocol).root());
				else
					NotificationUtils.notifyException(FileBrowserActivity.this, "Change Directory Failed", error);
			}
			public void started(String taskName) {
				if(progress != null && progress.isShowing())
					progress.dismiss();
				progress = new ProgressDialog(FileBrowserActivity.this);
				progress.setIndeterminate(true);
				progress.setCancelable(false);
				progress.setTitle(taskName);
				progress.setMessage("Listing...");
				progress.show();
			}
		};
		
		fileFilter = new FileFilter(){
			public boolean filter(EratosUri uri, boolean isDir){
				boolean hidden = isHidden(uri);
				if(isDir && !hidden)
					return true;
				else if(fileType == CHOOSE_FOLDER)
					return false;
				return checkExtFilter(uri) && !hidden;
			}

			private boolean isHidden(EratosUri uri) {
				String fn = uri.name();
				return fn.startsWith(".");
			}

			private boolean checkExtFilter(EratosUri uri) {
				if(extFilter == null || extFilter.length == 0)
					return true;

				String ext = uri.extension();
				for(String fext : extFilter){
					if(fext.equals(ext))
						return true;
				}
				
				return false;
			}
		};
		
		activate(protocol);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		directoryListingView.setItemChecked(position, true);
		selectEntry(position);
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == okButton.getId()) {
			if(selectedPath != null) {
				finishDialog();
			} else {
				NotificationUtils.longToast(this, "No file selected!");
			}
		}else if(v.getId() == cancelButton.getId()) {
			cancelDialog();
		}else if(v.getId() == newButton.getId()) {
			createNewFolder();
		}else if(v.getId() == refreshButton.getId()){
			refreshCurrentFolder();
		}else if(v.getId() == dropboxButton.getId()) {
			if(!protocolLock && !protocol.equalsIgnoreCase(EratosUri.DROPBOX_PROTOCOL))
				activate(EratosUri.DROPBOX_PROTOCOL);
		}else if(v.getId() == driveButton.getId()) {
			if(!protocolLock && !protocol.equalsIgnoreCase(EratosUri.DRIVE_PROTOCOL))
				activate(EratosUri.DRIVE_PROTOCOL);
		}else if(v.getId() == deviceButton.getId()) {
			if(!protocolLock && !protocol.equalsIgnoreCase(EratosUri.FILE_PROTOCOL))
				activate(EratosUri.FILE_PROTOCOL);
		}
	}

	private void refreshCurrentFolder() {
		protocolHandler.refresh(currentDir.uri(), changeDirectoryListener);
	}

	private void activate(final String protocol) {
		ProtocolConnectionManager protocolManager = ProtocolConnectionManager.getConnectionManager(protocol);
		
		if(protocolManager != null){
			cFileBrowser.get(protocol).switchTo(new ProtocolConnectionManager.ConnectionListenerNotifier(protocolManager, this) {
				public void success() {
					switchProtocol(protocol);
				}
			});
		}else{
			switchProtocol(protocol);
		}
	}
	
	protected void switchProtocol(String protocol) {
		this.protocol = protocol; 
		
		for(String oProtocol : cFileBrowser.keySet())
			cFileBrowser.get(oProtocol).deactivate();
		
		initialChdir = true;
		
		FileBrowser fileBrowser = cFileBrowser.get(protocol);
		protocolHandler = fileBrowser.getProtocolHandler();
		fileBrowser.activate();
		
		EratosUri curDir = fileBrowser.getCurrentDirectory();
		navigate(curDir);
	}

	private void createNewFolder() {
		final EditText input = new EditText(this);
		input.setHint("New Folder...");
		input.setSingleLine();

		NotificationUtils.promptInput(this, "Create Folder", new IStringResultListener() {
			public boolean onResult(final String result, DialogInterface dialog) {
				if(currentDir.hasChild(result)){
					NotificationUtils.longToast(FileBrowserActivity.this, "A file with that name already exists!");
					return false;
				}else{
					protocolHandler.createFolder(currentDir.uri(), result, new IFileOperationResultListener() {
						private ProgressDialog progress;
						
						public void finished(FileResult result) {
							progress.dismiss();
							navigate(currentDir.uri());
						}
						
						public void failed(Throwable error) {
							progress.dismiss();
							NotificationUtils.notifyException(FileBrowserActivity.this, "Create Directory Failed", error);
						}
						
						public void started(String taskName) {
							progress = new ProgressDialog(FileBrowserActivity.this);
							progress.setIndeterminate(true);
							progress.setTitle(taskName);
							progress.setMessage("Creating directory: " + result);
							progress.show();
						}
					});
					return true;
				}
			}
		});
	}
	
	private void finishDialog() {
		Intent intent=new Intent();
		
		String pathString = selectedPath.toString();
		
		intent.putExtra(PATH_ARG, pathString);
		intent.putStringArrayListExtra(EXISTING_FILES_ARG, getFilenameList());
		
		EratosUri rootPath = selectedPath;
		if(fileType == CHOOSE_FILE)
			rootPath = selectedPath.parent();
		
		cFileBrowser.get(protocol).storeDefaultPath(rootPath);
		
		setResult(RESULT_OK, intent);
		finish();
	}

	private ArrayList<String> getFilenameList() {
		ArrayList<String> listing = new ArrayList<String>();
		for(FileResult file : currentDir.contents()){
			if(file != null){
				EratosUri uri = file.uri();
				String filename = uri.name();
				listing.add(filename);
			}
		}
		return listing;
	}

	private void cancelDialog() {
		setResult(RESULT_CANCELED);
		finish();
	}
	
	public void selectEntry(int position){
		Object selectedItem = adapter.getItem(position);
		
		if(selectedItem == null){
			navigateUp();
			return;
		}
		
		FileResult file = (FileResult) selectedItem;
		if(file.isDir()){
			navigate(file.uri());
		}else{
			directoryListingView.setItemChecked(position, true);
			
			if(fileType == CHOOSE_FILE)
				selectedPath = file.uri();
		}
	}
	
	public void refresh() {
		if(fileType == CHOOSE_FOLDER)
			selectedPath = currentDir.uri();
		else
			selectedPath = null;
		
		currentDirectoryTextView.setText(currentDir.uri().name());
		adapter = new DirectoryAdapter(this, currentDir.contents(), fileFilter, protocolHandler.isRoot(currentDir.uri()));
		directoryListingView.setAdapter(adapter);
	}
	
	public void navigateUp() {
		EratosUri parent = protocolHandler.parent(currentDir.uri());
		protocolHandler.list(parent, changeDirectoryListener);
	}
	
	public void navigate(EratosUri uri){
		protocolHandler.list(uri, changeDirectoryListener);
	}
	
	public static Intent getFile(Context caller){
		Intent i = new Intent(caller, FileBrowserActivity.class);
		i.putExtra(PROTOCOL_LOCK_ARG, false);
		i.putExtra(TYPE_ARG, CHOOSE_FILE);
		return i;
	}
	
	public static Intent getFile(Context caller, String ... extFilter){
		Intent i = new Intent(caller, FileBrowserActivity.class);
		i.putExtra(PROTOCOL_LOCK_ARG, false);
		i.putExtra(TYPE_ARG, CHOOSE_FILE);
		i.putExtra(EXT_FILTER, extFilter);
		return i;
	}
	
	public static Intent getLocation(Context caller){
		Intent i = new Intent(caller, FileBrowserActivity.class);
		i.putExtra(TYPE_ARG, CHOOSE_FOLDER);
		return i;
	}
	
	public static Intent getLocation(Context caller, String protocol, boolean protocolLock){
		Intent i = new Intent(caller, FileBrowserActivity.class);
		i.putExtra(PROTOCOL_LOCK_ARG, protocolLock);
		i.putExtra(PROTOCOL_ARG, protocol);
		i.putExtra(TYPE_ARG, CHOOSE_FOLDER);
		return i;
	}
}
