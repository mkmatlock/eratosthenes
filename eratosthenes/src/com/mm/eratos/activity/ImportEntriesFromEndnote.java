package com.mm.eratos.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.ParseException;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.FragmentActivity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.io.endnote.EndNoteParser;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.NotificationUtils;

public class ImportEntriesFromEndnote extends FragmentActivity {
	
	private MultiProgressDialog progressDialog;
	private BibTeXContentProviderAdapter contentAdapter;
	private int length;
	private Throwable failure;
	private BibTeXHelper helper;
	private BibTeXEntryModelFactory modelFactory;

	private class ParseEndNoteTask extends AsyncTask<Void, Integer, Void>{

		private int imported;
		Intent intent;
		
		public ParseEndNoteTask(Intent intent){
			this.intent = intent;
			imported = 0;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			progressDialog.setPrimaryProgress(values[0], values[1]);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				EndNoteParser parser = new EndNoteParser();
				InputStream endNoteStream = processIntent(intent);
				
				parser.init(endNoteStream, "UTF-8", length);
				publishProgress(0, (int)parser.total());
				while(parser.next()) {
					BibTeXEntry entry = (BibTeXEntry) parser.get();
					contentAdapter.insert(modelFactory.construct(entry));
					
					imported++;
					publishProgress((int)parser.processed(), (int)parser.total());
				}
				parser.extras(contentAdapter, modelFactory, new ProgressCallback() {
					public void update(String stage, int progress, int maxProgress) {
					}
					public void finished() {
					}
					public void error(Throwable error) {
					}
				});
			} catch (IOException e) {
				failure = e;
			} catch (ParseException e) {
				failure = e;
			} catch (KeyCollisionException e) {
				failure = e;
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(failure != null){
				NotificationUtils.notifyException(ImportEntriesFromEndnote.this, "Unexpected Exception", failure, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
			}else
				finish();
			
			NotificationUtils.longToast(ImportEntriesFromEndnote.this, String.format("Imported %d entries", imported));
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		contentAdapter = new BibTeXContentProviderAdapter(getContentResolver());
		modelFactory = new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(contentAdapter), helper);
		
		progressDialog = new MultiProgressDialog();
		progressDialog.show(getFragmentManager(), "pdialog");
		
		new ParseEndNoteTask(getIntent()).execute();
	}

	private InputStream processIntent(Intent intent) throws IOException {
		Uri data = intent.getData();
    	
    	String scheme = intent.getScheme();
    	boolean isWeb = scheme.equals("http") || scheme.equals("https");
    	
		if(scheme.equals("content")){
			ParcelFileDescriptor fd = getContentResolver().openFileDescriptor(data, "r");
			length = (int) fd.getStatSize();
			fd.close();
    		return getContentResolver().openInputStream(data);
    	}else if(isWeb){
			return fetchFromWeb(data);
		}else if(scheme.equals("file")){
			String path = data.getPath();
			File file = new File(path);
			length = (int) file.length();
			return new FileInputStream( file );
		}else{
			throw new IOException("Unsupported protocol: " + scheme);
		}
	}
	
	private InputStream fetchFromWeb(Uri data) throws IOException {
    	HttpClient mHttpClient = new DefaultHttpClient();
    	HttpGet httpGet = new HttpGet(data.toString());
    	
        HttpParams params = mHttpClient.getParams();
        HttpClientParams.setRedirecting(params, true);

        HttpResponse response = mHttpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode != 200)
        	throw new IOException(String.format("HTTP status %d: %s", statusCode, response.getStatusLine().getReasonPhrase()));
        
        HttpEntity entity= response.getEntity();
        
        length = (int) entity.getContentLength();
        return entity.getContent();
	}
}
