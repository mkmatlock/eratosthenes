package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.GenomeResearchWebHandler;

public class AddEntryFromGenomeResearch extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new GenomeResearchWebHandler();
	}
}
