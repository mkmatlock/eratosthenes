package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.JBCWebHandler;

public class AddEntryFromJBC extends AddEntryWebHandlerActivity {

	@Override
	public AbstractWebHandler getHandler() {
		return new JBCWebHandler();
	}

}
