package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.NatureWebHandler;

public class AddEntryFromNatureJournal extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new NatureWebHandler();
	}
}
