package com.mm.eratos.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.jbibtex.ParseException;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.mm.eratos.R;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.feeds.GoogleScholarHandler;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class AddEntryFromFile extends AddEntryActivity {
	GoogleScholarHandler handler = new GoogleScholarHandler();
	
	protected class RetrieveBibtexFileStream extends StreamLoader {
		
        protected InputStream load(Intent intent) throws IOException {
        	Uri data = intent.getData();
        	
        	String scheme = intent.getScheme();
        	boolean isScholar = isScholar(data);
        	boolean isWeb = scheme.equals("http") || scheme.equals("https");
        	
        	getLogger().info("Getting data: %s", data.toString());
        	getLogger().info("Scheme: %s isWeb? %b isScholar? %b", scheme, isWeb, isScholar);
        	getLogger().info("Host: %s path: %s", data.getHost(), data.getPath());
        	
        	if(scheme.equals("content")){
        		return getContentResolver().openInputStream(data);
        	}else if(isWeb){
					return fetchFromWeb(data);
			}else if(scheme.equals("file")){
				String path = data.getPath();
				return new FileInputStream( new File(path) );
			}else{
				throw new IOException("Unsupported protocol: " + scheme);
			}
        }

		private InputStream fetchFromWeb(Uri data) throws IOException {
        	HttpClient mHttpClient = new DefaultHttpClient();
        	HttpGet httpGet = new HttpGet(data.toString());
        	
            HttpParams params = mHttpClient.getParams();
            HttpClientParams.setRedirecting(params, true);

            HttpResponse response = mHttpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode != 200)
            	throw new IOException(String.format("HTTP status %d: %s", statusCode, response.getStatusLine().getReasonPhrase()));
            
            HttpEntity entity= response.getEntity();
            
            InputStream content = entity.getContent();
            return content;
		}
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.createViewContent(savedInstanceState, R.layout.edit_raw_bibtex);
		
		Intent intent = getIntent();
		
		String scheme = intent.getScheme();
		Uri data = intent.getData();
		
		boolean isWeb = scheme.equals("http") || scheme.equals("https");
		boolean isScholar = isScholar(data);
		
		if(isWeb && isScholar){
			Intent i = new Intent(this, AddEntryFromScholar.class);
			i.setData(data);
			startActivity(i);
			finish();
		}else{
			processIntent();
		}
	}
    
    public class NullProcessor extends PostProcessor {
		@Override
		protected BibTeXEntryModel process(BibTeXEntryModel entry, Intent intent, DownloadListener downloadListener) throws IOException, ParseException, FieldRequiredException {
			return entry;
		}
    }

	private boolean isScholar(Uri data) {
		EratosUri uri = EratosUri.parseUri(data.toString());
		return handler.handles(uri);
	}

	@Override
	protected PostProcessor getPostProcessor() {
		return new NullProcessor();
	}
	
	protected StreamLoader getStreamLoader() {
		return new RetrieveBibtexFileStream();
	}
}
