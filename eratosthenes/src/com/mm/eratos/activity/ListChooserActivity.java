package com.mm.eratos.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.mm.eratos.R;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.utils.NotificationUtils;

public class ListChooserActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>, OnFocusChangeListener, OnClickListener, OnItemClickListener
{
	public static final String LIST_ID = "list_id";
	public static final String LIST_COLLECTIONS = "collections";
	public static final String LIST_GROUPS = "groups";
	
	public static final String CATEGORY_FIELD = "category";
	private static final String NEW_ITEM = "NEW_CATEGORY_ELEMENT";
	
	private static final int LIST_LOADER = 0x02;
	private ListView listView;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private SimpleCursorAdapter adapter;
	private Button okButton;
	private Button cancelButton;
	private EditText newCategoryTextView;
	
	private String selectedItem = NEW_ITEM;
	private String listType;
	private String labelColumn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.list_choice_dialog);
		setFinishOnTouchOutside(false);
		
		listView = (ListView) findViewById(R.id.list_chooser);
		listType = getIntent().getStringExtra(LIST_ID);
		
		bibtexAdapter = new BibTeXContentProviderAdapter(getContentResolver());
		
		getSupportLoaderManager().initLoader(LIST_LOADER, null, this);
		
		if(LIST_COLLECTIONS.equals(listType)){
			setTitle("Choose Collection");
			labelColumn = DBHelper.COLUMN_COLLECTION;
		}
		else if(LIST_GROUPS.equals(listType)){
			setTitle("Choose Group");
			labelColumn = DBHelper.COLUMN_GROUP;
		}
		
		adapter = new SimpleCursorAdapter(this, R.layout.list_choice_element, null, 
				new String[]{labelColumn}, new int[]{R.id.listItemView},
	            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER );
		
	    listView.setAdapter(adapter);
	    listView.setOnItemClickListener(this);
	    
	    okButton = (Button) findViewById(R.id.ok_button);
	    okButton.setOnClickListener(this);
	    cancelButton = (Button) findViewById(R.id.cancel_button);
	    cancelButton.setOnClickListener(this);
	    
	    newCategoryTextView = (EditText) findViewById(R.id.new_element);
	    newCategoryTextView.setOnFocusChangeListener(this);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		if(LIST_COLLECTIONS.equals(listType))
			return bibtexAdapter.loadCollections(this);
		if(LIST_GROUPS.equals(listType))
			return bibtexAdapter.loadGroups(this);
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		Cursor cursor = (Cursor) listView.getItemAtPosition(position);	
		String category = cursor.getString( cursor.getColumnIndex( labelColumn ) );
		
		listView.setItemChecked(position, true);
		selectedItem = category;
		newCategoryTextView.setText("");
	}
	
	private String getNewCategory(){
		return newCategoryTextView.getText().toString().trim();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == okButton.getId()){
			String newCategory = getNewCategory();
			
			if(selectedItem == NEW_ITEM && !newCategory.isEmpty()){
				chooseCategory(newCategory);
			}else if(selectedItem != NEW_ITEM){
				chooseCategory(selectedItem);
			}else{
				NotificationUtils.longToast(this, "No category selected!");
			}
			
		}else if(v.getId() == cancelButton.getId()){
			cancelDialog();
		}
	}

	private void cancelDialog() {
		setResult(RESULT_CANCELED);
		finish();
	}

	private void chooseCategory(String value) {
		Intent intent=new Intent();
		intent.putExtra(CATEGORY_FIELD, value);
		setResult(RESULT_OK, intent);
		finish();
	}


	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if(hasFocus)
			listView.clearChoices();
	}
	
	public static Intent getCollection(Context caller){
		Intent i = new Intent(caller, ListChooserActivity.class);
		i.putExtra(LIST_ID, LIST_COLLECTIONS);
		return i;
	}
	
	public static Intent getGroup(Context caller){
		Intent i = new Intent(caller, ListChooserActivity.class);
		i.putExtra(LIST_ID, LIST_GROUPS);
		return i;
	}
}
