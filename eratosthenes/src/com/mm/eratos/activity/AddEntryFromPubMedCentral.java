package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.PMCWebHandler;

public class AddEntryFromPubMedCentral extends AddEntryWebHandlerActivity {

	public AbstractWebHandler getHandler() {
		return new PMCWebHandler();
	}

}
