package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.DOIWebHandler;

public class AddEntryFromDOI extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new DOIWebHandler();
	}
}
