package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.ScienceWebHandler;

public class AddEntryFromScienceMag extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new ScienceWebHandler();
	}
}
