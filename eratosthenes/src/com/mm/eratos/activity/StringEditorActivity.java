package com.mm.eratos.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jbibtex.ParseException;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.adapter.ObjectAdapter;
import com.mm.eratos.adapter.ObjectAdapter.ObjectMapper;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.BibTeXStringModel;
import com.mm.eratos.dialog.StringEditorDialog;
import com.mm.eratos.dialog.StringEditorDialog.OnFinishedListener;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.task.UpdateEntriesTask;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.NotificationUtils;

public class StringEditorActivity extends FragmentActivity implements LoaderCallbacks<Cursor>, OnItemClickListener, OnClickListener, OnItemLongClickListener, OnLongClickListener {
	private static final int STRING_LOADER = 0x03;
	private Button commitButton;
	private Button cancelButton;
	private ListView stringList;
	private ImageButton newButton;
	private ObjectAdapter<BibTeXStringModel> stringAdapter;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private ItemActionCompositeAdapter<BibTeXStringModel> stringRemoveAdapter;
	
	private ItemActionCallback<BibTeXStringModel> removeCallback = new ItemActionCallback<BibTeXStringModel>() {
		@Override
		public void perform(int position, BibTeXStringModel st, View v) {
			String value = st.getValue();
			final String key = st.getKey();
			
			NotificationUtils.confirmDialog(StringEditorActivity.this, "Remove String?", "Remove string: " + value, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					removeString(key);
				}
			});
		}
		@Override
		public void initialize(int position, BibTeXStringModel st, View v) {
		}
	};
	
	private OnFinishedListener newStringListener = new OnFinishedListener() {
		public boolean onConfirm(BibTeXStringModel st, String key, String value) {
			try {
				BibTeXStringModel newItem = st.modify(bibtexHelper, key, value);
				stringAdapter.addItem(newItem);
			} catch (IOException e) {
				NotificationUtils.notify(StringEditorActivity.this, "Format Error", e.getMessage());
				return false;
			}
			return true;
		}
		public void onCancel() {
			
		}
	};
	
	private OnFinishedListener editStringListener = new OnFinishedListener() {
		public boolean onConfirm(BibTeXStringModel st, String key, String value) {
			try {
				BibTeXStringModel newItem = st.modify(bibtexHelper, key, value);
				stringAdapter.replaceItem(st, newItem);
			} catch (IOException e) {
				NotificationUtils.notify(StringEditorActivity.this, "Format Error", e.getMessage());
				return false;
			}
			return true;
		}
		public void onCancel() {
			
		}
	};
	
	private ObjectMapper<BibTeXStringModel> mapperCallback = new ObjectMapper<BibTeXStringModel>() {
		public View map(LayoutInflater inflater, BibTeXStringModel st, View v, ViewGroup parent) {
			if(v == null)
				v = inflater.inflate(R.layout.string_editor_entry, null);
			
			TextView stringName = (TextView)v.findViewById(R.id.stringName);
			TextView stringContent = (TextView)v.findViewById(R.id.stringContent);
			
			stringName.setText(st.getKey());
			stringContent.setText(st.getValue());
			return v;
		}
	};
	
	private List<BibTeXStringModel> strings;
	private BibTeXHelper bibtexHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.string_editor_activity);
		setFinishOnTouchOutside(false);

		bibtexAdapter = new BibTeXContentProviderAdapter(getContentResolver());
		bibtexHelper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		commitButton = (Button)findViewById(R.id.commit_button);
		cancelButton = (Button)findViewById(R.id.cancel_button);
		newButton = (ImageButton)findViewById(R.id.button_new_string);
		
		cancelButton.setOnClickListener(this);
		commitButton.setOnClickListener(this);
		newButton.setOnClickListener(this);
		newButton.setOnLongClickListener(this);
		
		stringList = (ListView)findViewById(R.id.string_list_view);
		stringAdapter = new ObjectAdapter<BibTeXStringModel>(this, mapperCallback);
		stringRemoveAdapter = new ItemActionCompositeAdapter<BibTeXStringModel>(R.id.removeString, removeCallback, stringAdapter);
		
		stringList.setOnItemClickListener(this);
		stringList.setOnItemLongClickListener(this);
		stringList.setAdapter(stringRemoveAdapter);
		
		getSupportLoaderManager().initLoader(STRING_LOADER, null, this);
	}

	protected void removeString(String key) {
		BibTeXStringModel remove = null;
		for(int i=0; i < stringAdapter.getCount(); i++){
			BibTeXStringModel st = (BibTeXStringModel)stringAdapter.getItem(i);
			if(st.getKey().equalsIgnoreCase(key)){
				remove = st;
				break;
			}
		}
		if(remove != null)
			stringAdapter.removeItem(remove);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return bibtexAdapter.loadStrings(this);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
		int rows = c.getCount();
		strings = new ArrayList<BibTeXStringModel>(rows);
		while(c.moveToNext()){
			strings.add(bibtexAdapter.cursorToString(c));
		}
		stringAdapter.setItems(strings);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		stringAdapter.clearItems();	
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
		BibTeXStringModel stringModel = (BibTeXStringModel)stringRemoveAdapter.getItem(pos);
		new StringEditorDialog(this, stringModel, editStringListener);
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> adapter, View v, int pos, long id) {
		final BibTeXStringModel stringModel = (BibTeXStringModel) stringRemoveAdapter.getItem(pos);
		
		NotificationUtils.promptInputLarge(this, "Edit String", stringModel.getBibtex(), new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				try {
					BibTeXStringModel newItem = stringModel.modify(bibtexHelper, result);
					stringAdapter.replaceItem(stringModel, newItem);
				} catch (IOException e) {
					NotificationUtils.notify(StringEditorActivity.this, "Format Error", e.getMessage());
					return false;
				} catch (ParseException e) {
					NotificationUtils.notify(StringEditorActivity.this, "Format Error", e.getMessage());
					return false;
				}
				return true;
			}
		});
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.commit_button:
			commit();
			break;
		case R.id.cancel_button:
			finish();
			break;
		case R.id.button_new_string:
			new StringEditorDialog(this, newStringListener);
			break;
		default:
				
		}
	}
	
	@Override
	public boolean onLongClick(View v) {
		switch(v.getId()){
		case R.id.button_new_string:
			NotificationUtils.promptInputLarge(this, "New String", "", new IStringResultListener() {
				public boolean onResult(String result, DialogInterface dialog) {
					try {
						BibTeXStringModel newItem = new BibTeXStringModel().modify(bibtexHelper, result);
						stringAdapter.addItem(newItem);
					} catch (IOException e) {
						NotificationUtils.notify(StringEditorActivity.this, "Format Error", e.getMessage());
						return false;
					} catch (ParseException e) {
						NotificationUtils.notify(StringEditorActivity.this, "Format Error", e.getMessage());
						return false;
					}
					return true;
				}
			});
			return true;
		default:
			return false;
		}
	}
	
	private void commit() {
		try {
			List<BibTeXStringModel> items = stringAdapter.getItems();
			bibtexAdapter.deleteStrings();
			for(BibTeXStringModel st : items)
				bibtexAdapter.insert(st);
			
			new UpdateEntriesTask(this, new ProgressCallback() {
				public void update(String stage, int progress, int maxProgress) {
				}
				public void finished() {
					SaveDatabaseTask.autosaveDatabase(StringEditorActivity.this, new FileSyncResultReceiver(StringEditorActivity.this));
					finish();
				}
				public void error(Throwable e) {
					NotificationUtils.notifyException(StringEditorActivity.this, "Error", e);
					restore();
				}
			}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
			
		} catch (KeyCollisionException e) {
			restore();
			NotificationUtils.notify(this, "Error", e.getMessage());
		}
	}

	private void restore() {
		try {
			bibtexAdapter.deleteStrings();
			for(BibTeXStringModel st : strings)
				bibtexAdapter.insert(st);
		} catch (KeyCollisionException e) {
			Exception unexpected = new Exception("There was an error restoring the database state! Please close your database and reopen it to avoid data loss!");
			unexpected.initCause(e);
			NotificationUtils.notifyException(this, "Unexpected Error", unexpected);
		}
	}
}
