package com.mm.eratos.activity;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.content.Intent;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class AddEntryFromISBN extends AddEntryActivity {
	public static final String ISBN_VALUE = "isbn";
	public static String isbnServiceUrl = "http://www.ottobib.com/isbn/%s/bibtex";
	
	protected static class RetrieveBibtexISBNStream extends StreamLoader {
        protected InputStream load(Intent intent) throws IOException {
        	String isbn = intent.getStringExtra(ISBN_VALUE).replace("-", "");
        	String intentUri = String.format(isbnServiceUrl, isbn);
        	
        	Document htmlPage = WebClient.newInstance().queryHtmlPage(intentUri);
        	Elements textArea = htmlPage.select("div textarea");
        	if(textArea.size() == 0){
        		throw new IOException("Unable to access resource: " + intentUri);
        	}
        	
        	String bibtexContent = textArea.get(0).text();
            return new ByteArrayInputStream(bibtexContent.getBytes());
        }
    }
    
    public static class NullProcessor extends PostProcessor {
		@Override
		protected BibTeXEntryModel process(BibTeXEntryModel entry, Intent intent, DownloadListener downloadListener) throws IOException, ParseException, FieldRequiredException {
			return entry;
		}
    }

	@Override
	protected PostProcessor getPostProcessor() {
		return new NullProcessor();
	}
	
	
	protected StreamLoader getStreamLoader() {
		return new RetrieveBibtexISBNStream();
	}
}
