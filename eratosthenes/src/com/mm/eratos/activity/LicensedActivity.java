package com.mm.eratos.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;

public class LicensedActivity extends FragmentActivity
{
	private static boolean DISABLE=false;
	
    // Generate 20 random bytes, and put them here.
    private static final byte[] SALT = new byte[] {
		56, -48, 34, -18, -12, -5, 7, -62, 49, 89, -59,
		-54, 77, -117, -63, -113, -11, 23, -46, 98
	};
	private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn7MYpsrnyCLZVCS833QVEfFPek5ZAVctRw1qU2Mcqib8vIBQXjzJNOJccTGs0UJFgamoEDM0L8I5bSnlYj2pF/ySqXuj9RQUEZ2RkYU2xAgolMz/FfbsgIjGJaXmNwAlGABX2Tzlsl+cLXwvINLwsM9jWBsrbWLtLBMoW3lwI8/oD8hKkFtLVdKwUvccH/PRNIq+WdOiHNJKR89Tj0eP7zk6p4UINbEcO+hEiLGsD5PLE3qE/9eRgLyDkuVRMKqdQ+wPXIykdpfUPIFOjQzVjNP6dIa/5fR3fE8Ldhtw+ByvXlGptEDgWJHwcSZ+LRfdJPbHY5FgnrtvG1orNmqrRQIDAQAB";

	private class EratosLicenseCheckerCallback implements LicenseCheckerCallback {
		public void allow(int reason) {
			if (isFinishing())
				return;
			displayResult(getString(R.string.allow));
		}

		public void dontAllow(int reason) {
			if (isFinishing()) 
				return;
			displayResult(getString(R.string.dont_allow));
			displayDialog(reason == Policy.RETRY);
		}
		
        public void applicationError(int errorCode) {
            if (isFinishing())
                return;
            String result = String.format(getString(R.string.application_error), errorCode);
            displayResult(result);
        }
	}
	
	private LicenseCheckerCallback mLicenseCheckerCallback;
    private LicenseChecker mChecker;
	private Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        // Try to use more data here. ANDROID_ID is a single point of attack.
        String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        
        mHandler = new Handler();
        
		// Construct the LicenseCheckerCallback. The library calls this when done.
        mLicenseCheckerCallback = new EratosLicenseCheckerCallback();

        // Construct the LicenseChecker with a Policy.
        mChecker = new LicenseChecker(
            this, new ServerManagedPolicy(this,
										  new AESObfuscator(SALT, getPackageName(), deviceId)),
            BASE64_PUBLIC_KEY  // Your public licensing key.
		);
        
        if(!DISABLE && EratosApplication.getApplication().isOnline()){
        	doCheck();
        }
	}
	
    protected Dialog createDialog(int id) {
        final boolean bRetry = id == 1;
        return new AlertDialog.Builder(this)
            .setTitle(R.string.unlicensed_dialog_title)
            .setMessage(bRetry ? R.string.unlicensed_dialog_retry_body : R.string.unlicensed_dialog_body)
            .setPositiveButton(bRetry ? R.string.retry_button : R.string.buy_button, new DialogInterface.OnClickListener() {
                boolean mRetry = bRetry;
                public void onClick(DialogInterface dialog, int which) {
                    if ( mRetry ) {
                        doCheck();
                    } else {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                                "http://market.android.com/details?id=" + getPackageName()));
                            startActivity(marketIntent);
                    }
                }
            })
            .setNegativeButton(R.string.quit_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            })
            .setCancelable(false)
            .create();
    }
	
    private void doCheck() {
		setProgressBarIndeterminateVisibility(true);
    	mChecker.checkAccess(mLicenseCheckerCallback);
    }

    private void displayResult(final String result) {
        mHandler.post(new Runnable() {
            public void run() {
                setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    private void displayDialog(final boolean showRetry) {
        mHandler.post(new Runnable() {
            public void run() {
                setProgressBarIndeterminateVisibility(false);
                
                Dialog dialog = createDialog(showRetry ? 1 : 0);
                dialog.show();
            }
        });
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChecker.onDestroy();
    }
}
