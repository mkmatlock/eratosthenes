package com.mm.eratos.activity;

import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.ArXiVHandler;

public class AddEntryFromArXiV extends AddEntryWebHandlerActivity {
	@Override
	public AbstractWebHandler getHandler() {
		return new ArXiVHandler();
	}
}