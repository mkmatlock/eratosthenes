package com.mm.eratos.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.dropbox.DropboxConnectionManager;
import com.mm.eratos.dropbox.DropboxProtocolHandler;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.task.ProtocolImportTask;
import com.mm.eratos.protocol.task.ProtocolSyncTask;
import com.mm.eratos.task.LoadLibraryTask;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.IStringArrayResultListener;
import com.mm.eratos.utils.InputUtils;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.view.MainMenu.ConfirmCallback;

public class OpenBibDatabase extends RemoteProtocolActivity implements ConfirmCallback, ProgressCallback {
	
	private FileRevisionContentProviderAdapter fileAdapter;

	private void confirmOverwrite(final ConfirmCallback confirmCallback) {
		if(EratosApplication.getApplication().isSaved())
			confirmCallback.confirmed();
		else{
			NotificationUtils.confirmDialog(this, "Warning!", "Current library is not saved, continue anyway?", 
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							confirmCallback.confirmed();
						}
					},new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fileAdapter = new FileRevisionContentProviderAdapter(getContentResolver());
		confirmOverwrite(this);
		setFinishOnTouchOutside(false);
	}

	@Override
	public void update(String stage, int progress, int maxProgress) {
		
	}

	@Override
	public void finished() {
		Intent i = new Intent(this, EntryGroupActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}
	
	@Override
	public void error(Throwable error) {
		finish();
	}

	@Override
	public void confirmed() {
		String dataStr = getIntent().getDataString();
		EratosUri uri = EratosUri.parseUri(dataStr);
		
		InputUtils.chooseLibraryType(this, uri, 
				new IStringArrayResultListener() {
					@Override
					public boolean onResult(DialogInterface dialog, String ... results) {
						String bibType = results[0];
						String encoding = results[1];
						openBibDatabase(bibType, encoding);
						return false;
					}
				});
	}

	private void openBibDatabase(final String bibType, final String encoding) {
		Uri data = getIntent().getData();
		String dataStr = getIntent().getDataString();
		String dbPattern = "com.dropbox.android/files/scratch";
		
		if(dataStr.contains(dbPattern)){
			int start = dataStr.indexOf(dbPattern) + dbPattern.length();
			String dbPath = dataStr.substring(start);
			final EratosUri libraryUri = EratosUri.parseUri("dropbox://" + dbPath);
			
			DropboxConnectionManager dropboxConnectionManager = DropboxConnectionManager.getDropboxConnectionManager();
			dropboxConnectionManager.connect(this,
					new ProtocolConnectionManager.ConnectionListenerNotifier(dropboxConnectionManager, this){
						public void success() {
							loadDropboxLibrary(libraryUri, bibType, encoding);
						}
					});
		}else{
			EratosUri libraryUri = EratosUri.parseUri("root://" + data.getPath());
			new LoadLibraryTask(this, libraryUri, bibType, encoding, true, false, this).execute();
		}
	}

	private void loadDropboxLibrary(EratosUri libraryUri, String bibType, String encoding) {
		if(fileAdapter.exists(libraryUri) && fileAdapter.getFile(libraryUri).cached()){
			boolean readOnly = EratosApplication.getApplication().getSettingsManager().syncReadOnly();
			new ProtocolSyncTask(new DropboxProtocolHandler(), this, libraryUri, bibType, encoding, true, false, readOnly, this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}else{
			new ProtocolImportTask(new DropboxProtocolHandler(), this, libraryUri, bibType, encoding, this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}
	}
}
