package com.mm.eratos.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.utils.StringUtils;

public class SearchFactory {
	public static Collection<String> getHandlers(String type){
		EratosSettings settingsManager = EratosApplication.getApplication().getSettingsManager();
		
		List<String> handlerNames = new ArrayList<String>();
		List<String> handlerOrder = settingsManager.getHandlerOrder();
		Map<String, CustomSearchHandler> customHandlers = settingsManager.getSearchHandlers();
		
		for(String handlerName : handlerOrder){
			ISearchHandler searchHandler = customHandlers.get(handlerName);
			if(searchHandler != null && searchHandler.handles(type))
				handlerNames.add(handlerName);
		}
		
		return handlerNames;
	}

	public static ISearchHandler getHandler(String name) {
		EratosSettings settingsManager = EratosApplication.getApplication().getSettingsManager();
		
		Map<String, CustomSearchHandler> customHandlers = settingsManager.getSearchHandlers();
		if(customHandlers.containsKey(name))
			return customHandlers.get(name);
		return null;
	}
	
	public static ISearchHandler getDefaultHandler(String type) {
		EratosSettings settingsManager = EratosApplication.getApplication().getSettingsManager();
		
		List<String> handlerOrder = settingsManager.getHandlerOrder();
		Map<String, CustomSearchHandler> customHandlers = settingsManager.getSearchHandlers();
		
		for(String handlerName : handlerOrder) {
			ISearchHandler searchHandler = customHandlers.get(handlerName);
			if(searchHandler != null && searchHandler.handles(type))
				return searchHandler;
		}
		return null;
	}

	public static String getDefaultHandlers() {
		List<String> handlers = new ArrayList<String>();
		
		handlers.add(newGoogleScholar().toString());
		handlers.add(newPubMed().toString());
		handlers.add(newGoogleBooks().toString());
		
		return StringUtils.join("\n", handlers);
	}
	
	public static String getDefaultOrder() {
		return StringUtils.join(EratosSettings.SEARCH_HANDLER_ORDER_DELIMITER, "Google Scholar", "Google Books", "PubMed");
	}
	
	static final String gscholar_url = "http://scholar.google.com/scholar?hl=en&q=%(title)&btnG=Search&as_subj=eng&as_sdt=1,5&as_ylo=&as_vis=0";
	static final String gbooks_url = "https://www.google.com/search?q=%(title)&btnG=Search+Books&tbm=bks&tbo=1&gws_rd=ssl";
	static final String pubmed_url = "http://www.ncbi.nlm.nih.gov/pubmed/?term=%(title)";
	
	private static ISearchHandler newGoogleScholar(){
		return new CustomSearchHandler("Google Scholar", gscholar_url);
	}
	
	private static ISearchHandler newGoogleBooks(){
		return new CustomSearchHandler("Google Books", gbooks_url, "book", "booklet", "inbook", "chapter", "manual");
	}

	private static ISearchHandler newPubMed(){
		return new CustomSearchHandler("PubMed", pubmed_url, "article", "inproceedings", "incollection", "techreport", "phdthesis", "mastersthesis", "misc");
	}
}