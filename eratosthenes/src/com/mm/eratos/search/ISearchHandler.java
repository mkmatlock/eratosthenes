package com.mm.eratos.search;

import android.content.Context;
import android.content.Intent;

import com.mm.eratos.bibtex.BibTeXEntryModel;

public interface ISearchHandler {
	public Intent search(Context context, BibTeXEntryModel entry);
	public String name();
	public boolean handles(String type);
	public String toString();
}
