package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class QueryElementLike implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5839223649584088797L;
	private String column;
	private String text;

	public QueryElementLike(String column, String text){
		this.column = column;
		this.text = text;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s LIKE ?", column);
	}

	@Override
	public List<String> args() {
		return Arrays.asList("%" + text + "%");
	}

}