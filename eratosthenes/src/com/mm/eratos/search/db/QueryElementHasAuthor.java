package com.mm.eratos.search.db;

import java.util.List;
import java.util.Locale;

import com.mm.eratos.bibtex.AuthorList;
import com.mm.eratos.bibtex.AuthorList.Author;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.utils.Functional;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.TypedRunnable;

public class QueryElementHasAuthor implements QueryElement {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2582616463124917558L;

	private static final String columnQuery = String.format(Locale.US, "%s LIKE ?", DBHelper.COLUMN_AUTHOR);
	
	private List<String> formattedAuthors;

	public QueryElementHasAuthor(AuthorList authors){
		formattedAuthors = Functional.map(authors.list(), 
				new TypedRunnable<Author, String>() {
					@Override
					public String run(Author a) {
						return "%" + a.toString() + "%";
					}
				});
	}
	
	@Override
	public String sql() {
		return StringUtils.join(" AND ", Functional.map(formattedAuthors, new TypedRunnable<String, String>() {
			public String run(String arg) {
				return columnQuery;
			}
		}));
	}

	@Override
	public List<String> args() {
		return formattedAuthors;
	}

}
