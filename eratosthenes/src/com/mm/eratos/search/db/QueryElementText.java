package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class QueryElementText implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2081407532107910328L;
	private String column;
	private String text;

	public QueryElementText(String column, String text){
		this.column = column;
		this.text = text;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s = ?", column);
	}

	@Override
	public List<String> args() {
		return Arrays.asList(text);
	}

}
