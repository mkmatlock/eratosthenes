package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class QueryElementRegex implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5839223649584088797L;
	private String column;
	private String regex;

	public QueryElementRegex(String column, String regex){
		this.column = column;
		this.regex = regex;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s REGEXP ?", column);
	}

	@Override
	public List<String> args() {
		return Arrays.asList(regex);
	}

}