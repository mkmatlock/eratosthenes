package com.mm.eratos.search.db;

import java.util.List;
import java.util.Locale;

import com.mm.eratos.utils.Functional;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.TypedRunnable;

public class QueryElementAnd implements QueryElement {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5623360403086778976L;
	List<QueryElement> subqueries;
	
	public QueryElementAnd(List<QueryElement> subqueries){
		this.subqueries = subqueries;
	}

	@Override
	public String sql() {
		return StringUtils.join(" AND ", 
				Functional.map( subqueries, new TypedRunnable<QueryElement, String>(){
					public String run(QueryElement arg) {
						return String.format( Locale.US, "( %s )", arg.sql() );
					}
				}));
	}

	@Override
	public List<String> args() {
		return Functional.append( subqueries, new TypedRunnable<QueryElement, List<String>>() {
			public List<String> run(QueryElement arg) {
				return arg.args();
			}
		});
	}
}