package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;

public class QueryElementInCollection implements QueryElement {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6108398338463391324L;
	private String collection;

	public QueryElementInCollection(String collection){
		this.collection = collection;
	}

	@Override
	public String sql() {
		return String.format(Locale.US, "%s = ?", DBHelper.COLUMN_COLLECTION);
	}

	@Override
	public List<String> args() {
		return Arrays.asList(collection);
	}

	public String collection() {
		return collection;
	}
}
