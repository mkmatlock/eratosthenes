package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class QueryElementStartsWith implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8863994653026388851L;
	private String column;
	private String text;

	public QueryElementStartsWith(String column, String text){
		this.column = column;
		this.text = text;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s LIKE ?", column);
	}

	@Override
	public List<String> args() {
		return Arrays.asList(text + "%");
	}

}