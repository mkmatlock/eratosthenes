package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;

import com.mm.eratos.database.DBHelper;

public class QueryElementSearchLike implements QueryElement {
	/**
	 * 
	 */
	private static final long serialVersionUID = -296753950354079795L;
	private String query;

	public QueryElementSearchLike(String query){
		this.query = query;
	}
	
	@Override
	public String sql() {
		String whereQuery = "(";
		whereQuery += String.format( " %s LIKE ?", DBHelper.COLUMN_KEYWORDS ); 
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_TITLE );
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_AUTHOR );
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_YEAR );
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_SOURCE );
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_COLLECTION );
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_GROUPS );
		whereQuery += String.format( " OR %s LIKE ?", DBHelper.COLUMN_KEYWORDS );
		whereQuery += " )";
		return whereQuery;
	}

	@Override
	public List<String> args() {
		String lQuery = createLike(query, "");
		String filterGroup = createLike(query, "|");
		String filterKeyword = createLike(query, "|");
		return Arrays.asList(lQuery, lQuery, lQuery, lQuery, lQuery, lQuery, filterGroup, filterKeyword);
	}

	private String createLike(String val, String delimiter){
		return "%" + delimiter + val + delimiter + "%";
	}

	public String query() {
		return query;
	}
}
