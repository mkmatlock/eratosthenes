package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;

public class QueryElementInGroup implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 575215028202016348L;
	private String group;

	public QueryElementInGroup(String group){
		this.group = group;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s LIKE ?", DBHelper.COLUMN_GROUPS);
	}

	@Override
	public List<String> args() {
		return Arrays.asList( "%|"+group+"|%" );
	}

	public String group() {
		return group;
	}

}
