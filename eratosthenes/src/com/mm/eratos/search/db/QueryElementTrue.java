package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;

public class QueryElementTrue implements QueryElement {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5038773258067751500L;

	@Override
	public String sql() {
		return "1=1";
	}

	@Override
	public List<String> args() {
		return Arrays.asList();
	}

}
