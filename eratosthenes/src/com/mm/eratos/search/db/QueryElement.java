package com.mm.eratos.search.db;

import java.io.Serializable;
import java.util.List;

public interface QueryElement extends Serializable  {
	public String sql();
	public List<String> args();
}
