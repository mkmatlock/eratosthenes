package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;

public class QueryElementFieldLike implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5839223649584088797L;
	private String field;
	private String text;

	public QueryElementFieldLike(String field, String text){
		this.text = text;
		this.field = field;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s = ? AND %s LIKE ?", DBHelper.COLUMN_FIELD, DBHelper.COLUMN_VALUE);
	}

	@Override
	public List<String> args() {
		return Arrays.asList(field, "%" + text + "%");
	}
	
}
