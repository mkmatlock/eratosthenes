package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;

import com.mm.eratos.database.DBHelper;

public class QueryElementSearchRegex implements QueryElement {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9115725805547226714L;
	private String regex;

	public QueryElementSearchRegex(String regex){
		this.regex = regex;
	}

	@Override
	public String sql() {
		String whereQuery = "(";
		whereQuery += String.format( " %s REGEXP ?", DBHelper.COLUMN_KEYWORDS ); 
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_TITLE );
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_AUTHOR );
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_YEAR );
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_SOURCE );
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_COLLECTION );
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_GROUPS );
		whereQuery += String.format( " OR %s REGEXP ?", DBHelper.COLUMN_KEYWORDS );
		whereQuery += " )";
		return whereQuery;
	}

	@Override
	public List<String> args() {
		return Arrays.asList(regex, regex, regex, regex, regex, regex, regex, regex);
	}
}
