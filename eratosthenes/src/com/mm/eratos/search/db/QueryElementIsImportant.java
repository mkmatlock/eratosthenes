package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;

public class QueryElementIsImportant implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7566669927114569761L;

	@Override
	public String sql() {
		return String.format(Locale.US, "%s = 1", DBHelper.COLUMN_IMPORTANT);
	}

	@Override
	public List<String> args() {
		return Arrays.asList();
	}

}
