package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;

public class QueryElementIsUnread implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7793496948531314616L;

	@Override
	public String sql() {
		return String.format(Locale.US, "%s = 0", DBHelper.COLUMN_READ);
	}

	@Override
	public List<String> args() {
		return Arrays.asList();
	}

}
