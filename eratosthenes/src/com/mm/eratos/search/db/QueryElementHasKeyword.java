package com.mm.eratos.search.db;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;

public class QueryElementHasKeyword implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5326396108935304221L;
	private String keyword;

	public QueryElementHasKeyword(String keyword){
		this.keyword = keyword;
	}
	
	@Override
	public String sql() {
		return String.format(Locale.US, "%s LIKE ?", DBHelper.COLUMN_KEYWORDS);
	}

	@Override
	public List<String> args() {
		return Arrays.asList( "%|"+keyword+"|%" );
	}

	public String keyword() {
		return keyword;
	}

}
