package com.mm.eratos.search.db;

import java.util.List;

public class QueryElementNot implements QueryElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2541424561960901741L;
	private QueryElement subquery;

	public QueryElementNot(QueryElement subquery){
		this.subquery = subquery;
	}
	
	@Override
	public String sql() {
		return "NOT ( " + subquery.sql() + " )";
	}

	@Override
	public List<String> args() {
		return subquery.args();
	}

}
