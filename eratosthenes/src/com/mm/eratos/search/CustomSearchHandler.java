package com.mm.eratos.search;

import java.util.Map;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebUtils;

public class CustomSearchHandler implements ISearchHandler {
	private static final String delimiter = " ||| ";
	private static final String delimiter2 = ";";
	
	private String name;
	private String searchString;
	private CaseInsensitiveHashSet types;
	
	public CustomSearchHandler(String name, String searchString, String ... types){
		this.name = name;
		this.searchString = searchString;
		
		this.types = new CaseInsensitiveHashSet();
		for(String type : types){
			type = type.trim();
			if(!type.isEmpty())
				this.types.add(type);
		}
	}
	
	@Override
	public Intent search(Context context, BibTeXEntryModel entry) {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		Map<String, String> fields = entry.getFieldStrings(helper);
		Map<String, String> formattedFields = WebUtils.format(fields);
		String searchQuery = StringUtils.format(searchString, formattedFields);
		
		Uri uri = WebUtils.formatExternalUriRequest(searchQuery);
		return new Intent(Intent.ACTION_VIEW, uri);
	}
	
	@Override
	public String name() {
		return name;
	}
	
	public String url() {
		return searchString;
	}
	
	public String types() {
		return StringUtils.join(delimiter2, types);
	}
	
	@Override
	public boolean handles(String type) {
		return types.isEmpty() || types.contains(type);
	}
	
	@Override
	public String toString() {
		return name + delimiter + searchString + delimiter + StringUtils.join(delimiter2, types);
	}
	
	public static CustomSearchHandler parse(String serial) {
		String []items = serial.split( Pattern.quote(delimiter) );
		if(items.length>2){
			String []types = items[2].split( Pattern.quote(delimiter2) );
			return new CustomSearchHandler(items[0], items[1], types);
		}else{
			return new CustomSearchHandler(items[0], items[1]);
		}
	}
}
