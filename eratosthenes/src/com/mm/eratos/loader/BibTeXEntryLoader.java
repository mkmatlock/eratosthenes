package com.mm.eratos.loader;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProvider;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.utils.NotificationUtils;

public class BibTeXEntryLoader extends AsyncTaskLoader<BibTeXEntryModel> {

	private Context mContext;
	private ContentResolver contentResolver;
	
	private BibTeXHelper helper;
	private BibTeXContentProviderAdapter contentAdapter;
	private BibTeXContentProviderReferenceResolver resolver;
	private BibTeXEntryModelFactory modelFactory;
	
	private String refKey;
	private BibTeXEntryModel mData;
	private Cursor mCursor;
	private ContentObserver mRegisteredObserver;
	

	public BibTeXEntryLoader(Context mContext, String refKey){
		super(mContext);
		this.refKey = refKey;
		this.mContext = mContext;
		
		contentResolver = mContext.getContentResolver();
		contentAdapter = new BibTeXContentProviderAdapter(contentResolver);
		resolver = new BibTeXContentProviderReferenceResolver(contentAdapter);
		helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		modelFactory = new BibTeXEntryModelFactory(resolver, helper);
	}
	
	@Override
	public BibTeXEntryModel loadInBackground() {
		try {
			Cursor cursor = contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF}, 
					String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey}, null);
			
			if(cursor.getCount() > 0){
				cursor.moveToFirst();
				mCursor = cursor;
				mRegisteredObserver = null;
				BibTeXEntryModel entry = contentAdapter.cursorToEntry(cursor, modelFactory);
				return entry;
			}else{
				mCursor = null;
				mRegisteredObserver = null;
			}
		} catch (Exception e) {
			cancelLoad();
			NotificationUtils.notifyException(mContext, "Unexpected Error", e);
		} catch (Error e) {
			cancelLoad();
			NotificationUtils.notifyException(mContext, "Unexpected Error", e);
		}
		
		return null;
	}

	public void registerContentObserver(ContentObserver contentObserver) {
		if(mCursor != null){
			if(mRegisteredObserver != contentObserver){
				mCursor.registerContentObserver(contentObserver);
				mRegisteredObserver = contentObserver;
			}
		}
	}

	public void unregisterContentObserver(ContentObserver contentObserver) {
		if(mCursor != null){
			mCursor.unregisterContentObserver(contentObserver);
			mRegisteredObserver = null;
		}
	}

	@Override
	public void deliverResult(BibTeXEntryModel data) {
		if (isReset())
			return;

		// Hold a reference to the old data so it doesn't get garbage collected.
		// We must protect it until the new data has been delivered.
		BibTeXEntryModel oldData = mData;
		mData = data;

		if (isStarted())
			super.deliverResult(data);

		// Invalidate the old data as we don't need it any more.
		if (oldData != null && oldData != data) {
		}
	}

	@Override
	protected void onStartLoading() {
		if (mData != null) {
			deliverResult(mData);
		}

		if (takeContentChanged() || mData == null) {
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		// The Loader is in a stopped state, so we should attempt to cancel the
		// current load (if there is one).
		cancelLoad();

		// Note that we leave the observer as is. Loaders in a stopped state
		// should still monitor the data source for changes so that the Loader
		// will know to force a new load if it is ever started again.
	}

	@Override
	protected void onReset() {
		// Ensure the loader has been stopped.
		onStopLoading();

		// At this point we can release the resources associated with 'mData'.
		if (mData != null) {
			mData = null;
		}
	}

	@Override
	public void onCanceled(BibTeXEntryModel data) {
		// Attempt to cancel the current asynchronous load.
		super.onCanceled(data);
	}
}
