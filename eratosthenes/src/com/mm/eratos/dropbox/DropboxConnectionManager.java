package com.mm.eratos.dropbox;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AppKeyPair;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolInternetConnectionException;

public class DropboxConnectionManager extends ProtocolConnectionManager {
	private static final String USER_OAUTH2_KEY = "OAUTH2_KEY";
	
	final static private String APP_KEY = "7nhhwom6zx0qe74";
	final static private String APP_SECRET = "up5z8g8y3nvbh8q";
	
	boolean connecting = false;
	ConnectionListener listener;
	private DropboxAPIWrapper mDBApi;
	private static DropboxConnectionManager singleton;

	protected DropboxConnectionManager(){
		mDBApi = null;
		listener = null;
	}

	public static DropboxConnectionManager getDropboxConnectionManager() {
		if(singleton == null)
			singleton = new DropboxConnectionManager();
		return singleton;
	}

	public static DropboxConnectionManager replaceDropboxConnectionManager(DropboxConnectionManager dropboxConnectionManager) {
		DropboxConnectionManager oldDCM = singleton;
		singleton = dropboxConnectionManager;
		return oldDCM;
	}

	public DropboxAPIWrapper getDropboxAPI(){
		return mDBApi;
	}

	@Override
	public boolean sessionLinked() {
		return mDBApi != null && mDBApi.getSession().isLinked();
	}
	
	private boolean initializeSession(Context context){
		if(sessionLinked())
			return false;
		
		SharedPreferences preferences = EratosApplication.getApplication().getPreferences();
		
		AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
		AndroidAuthSession session = new AndroidAuthSession(appKeys);
		
		String oauth2key = preferences.getString(USER_OAUTH2_KEY, null);
		
		mDBApi = new DropboxAPIWrapper( new DropboxAPI<AndroidAuthSession>(session) );
		if(oauth2key != null) {
			mDBApi.getSession().setOAuth2AccessToken(oauth2key);
			return false;
		} else {
			mDBApi.getSession().startOAuth2Authentication(context);
			return true;
		}
	}
	
	@Override
	public void connect(Activity mContext, ConnectionListener listener){
		this.listener = listener;
		listener.connecting();
		EratosApplication app = EratosApplication.getApplication();
		
		if(!app.canSync()){
			Exception e = null;
			if(!app.isOnline())
				e = new ProtocolInternetConnectionException("Not connected to the network.");
			else
				e = new ProtocolInternetConnectionException("Cannot sync over mobile data network, please find a WiFi connection or change your settings.");
			listener.failed(e);
			return;
		}
		
		connecting = initializeSession(mContext);
		if(!connecting)
			checkAuthorization(mContext);
	}
	
	private void checkAuthorization(Context context) {
		new DropboxCheckConnectionTask(new ConnectionListener() {
			public void connected() {
				listener.connected();
			}
			public void failed(Exception e) {
				clearCredentials();
				listener.failed(e);
			}
			public void connecting() {
				
			}
		}).execute();
	}


	@Override
	public void onResume(Activity mContext) {
		if (connecting) 
			finalizeConnection(mContext);
	}

	@Override
	public void onActivityResult(Activity mContext, int requestCode, int resultCode, Intent data) {
		
	}
	
	public void finalizeConnection(Activity mContext){
		if(!mDBApi.getSession().authenticationSuccessful()) {
			listener.failed(new DropboxException("Could not connect to dropbox"));
		} else {
	        try {
	            mDBApi.getSession().finishAuthentication();
	            
	            String oauth2token = mDBApi.getSession().getOAuth2AccessToken();
	            
	            storeCredentials(oauth2token);
	            checkAuthorization(mContext);
	        } catch (IllegalStateException e) {
	            Log.i("DbAuthLog", "Error authenticating", e);
	            mDBApi = null;
	            e.printStackTrace();
	            listener.failed(new DropboxException(e));
	        }
		}
		connecting = false;
	}

	private void storeCredentials(String oauth2token) {
		SharedPreferences preferences = EratosApplication.getApplication().getPreferences();
		Editor edit = preferences.edit();
		edit.putString(USER_OAUTH2_KEY, oauth2token);
		edit.commit();
	}
	
	private void clearCredentials(){
		SharedPreferences preferences = EratosApplication.getApplication().getPreferences();
		Editor edit = preferences.edit();
		edit.putString(USER_OAUTH2_KEY, null);
		edit.commit();
	}

	@Override
	public void onPause(Activity mContext) {
		
	}

	@Override
	public String name() {
		return "Dropbox";
	}
}