package com.mm.eratos.dropbox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.RESTUtility;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxServerException;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolConnectionException;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolNoSuchFileException;

public class DropboxProtocolHandler extends ProtocolHandler
{
	private FileManager fileManager;

	public DropboxProtocolHandler(){
		fileManager = FileManager.getFileManager();
	}
	
	private DropboxAPIWrapper mDBApi(){
		return DropboxConnectionManager.getDropboxConnectionManager().getDropboxAPI();
	}
	
	private DropboxMetadataCache metadataCache(){
		return DropboxMetadataCache.getMetadataCache(mDBApi());
	}

	@Override
	public EratosUri parent(EratosUri uri) {
		return uri.parent();
	}
	
	@Override
	public EratosUri root() {
		return EratosUri.parseUri("dropbox:///");
	}

	@Override
	public boolean isRoot(EratosUri uri) {
		return uri.equals(root());
	}

	@Override
	public FileResult move(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);
		assertProtocol(dst);
		try {
			if(metadataCache().fileExists(dst))
				metadataCache().delete(dst);
			
			return  metadataCache().move(src, dst);
		} catch(DropboxServerException e){
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		}
	}


	@Override
	public FileResult put(EratosUri src, EratosUri dst) throws ProtocolException {
		return put(src, dst, new IFileOperationProgressListener() {
			public void progress(String message, int progress, int maxprogress) {}
		});
	}

	@Override
	public FileResult put(EratosUri src, final EratosUri dst, IFileOperationProgressListener listener) throws ProtocolException {
		assertProtocol(dst);
		try {
			File localFile = fileManager.getFile(src);
			File localCopy = fileManager.getFile(dst);
			
			if(!dst.equals(src))
				FileUtils.copy(localFile, localCopy);
			
			return metadataCache().pushFile(src, localCopy, listener);
		} catch(DropboxServerException e){
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		} catch (FileNotFoundException e) {
			throw new ProtocolNoSuchFileException(e);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult copy(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);
		assertProtocol(dst);
		try {
			if(metadataCache().fileExists(dst))
				metadataCache().delete(dst);
			
			return metadataCache().copy(src, dst);
		} catch (DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		}
	}


	@Override
	public FileResult list(EratosUri dir) throws ProtocolException {
		assertProtocol(dir);
		try {
			List<FileResult> listing = metadataCache().list(dir);
			return new FileResult(dir, listing);
		} catch(DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult refresh(EratosUri dir) throws ProtocolException {
		assertProtocol(dir);
		try {
			List<FileResult> listing = metadataCache().refresh(dir);
			return new FileResult(dir, listing);
		} catch(DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc) throws ProtocolException {
		return fetch(src, dst, deleteSrc, new IFileOperationProgressListener() {
			public void progress(String message, int progress, int maxprogress) {}
		});
	}

	@Override
	public FileResult fetch(final EratosUri src, EratosUri dst, boolean deleteSrc, final IFileOperationProgressListener listener) throws ProtocolException {
		assertProtocol(src);
		try {
			File dstFile = fileManager.getFile(dst);
			Entry e = metadataCache().getFile(src, dstFile, listener);
			
			long modified = RESTUtility.parseDate(e.modified).getTime();
			return new FileResult(dst, e.rev, modified, e.isDir);
		} catch(DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		} catch (FileNotFoundException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public FileResult delete(EratosUri src) throws ProtocolException {
		assertProtocol(src);
		try {
			metadataCache().delete(src);
			return new FileResult(src);
		} catch(DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		}
	}


	@Override
	public FileResult createFolder(EratosUri dir, String name) throws ProtocolException {
		assertProtocol(dir);
		try {
			EratosUri uri = dir.append(name);
			metadataCache().createFolder(uri);
			return new FileResult(uri);
		} catch(DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		}
	}

	@Override
	public boolean isProtocol(EratosUri uri) {
		return uri.protocol().equals(EratosUri.DROPBOX_PROTOCOL);
	}

	private void assertProtocol(EratosUri uri) throws ProtocolException {
		if(!isProtocol(uri))
			throw new ProtocolException("Protocol does not match: " + uri.protocol() + " != " + EratosUri.DROPBOX_PROTOCOL);
	}

	@Override
	public FileResult info(EratosUri src) throws ProtocolException {
		assertProtocol(src);
		try {
			Entry metadata = metadataCache().info(src);
			long modified = RESTUtility.parseDate(metadata.modified).getTime();
			
			return new FileResult(src, metadata.rev, modified, metadata.isDir);
		} catch(DropboxServerException e) {
			throw handleServerException(e);
		} catch (DropboxException e) {
			throw new ProtocolException(e);
		} catch (DropboxNoSuchFileException e) {
			throw new ProtocolNoSuchFileException(e);
		}
	}

	private ProtocolException handleServerException(DropboxServerException e) {
		if(e.error == DropboxServerException._404_NOT_FOUND)
			return new ProtocolNoSuchFileException(e);
		if(e.error == DropboxServerException._403_FORBIDDEN)
			return new ProtocolException(e);
		if(e.error == DropboxServerException._400_BAD_REQUEST)
			return new ProtocolException(e);
		
		return new ProtocolConnectionException(e);
	}

	@Override
	public String name() {
		return "Dropbox";
	}

	@Override
	public String protocol() {
		return EratosUri.DROPBOX_PROTOCOL;
	}

	@Override
	public void clearCache() {
		metadataCache().clear();
	}

	@Override
	public FileResult createFile(EratosUri dst) throws ProtocolException {
		try{
			File tmpFile = File.createTempFile("eratos-empty-upload", "temp");
			FileResult result = metadataCache().pushFile(dst, tmpFile, new IFileOperationProgressListener() {
				@Override
				public void progress(String message, int progress, int maxprogress) {
				}
			});
			return result;
		}catch(IOException e) {
			throw new ProtocolException(e);
		}catch(DropboxException e) {
			throw new ProtocolException(e);
		}
	}
}
