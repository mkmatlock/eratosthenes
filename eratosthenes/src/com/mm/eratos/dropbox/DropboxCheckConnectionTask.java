package com.mm.eratos.dropbox;

import android.os.AsyncTask;

import com.dropbox.client2.exception.DropboxException;
import com.mm.eratos.protocol.ProtocolConnectionManager.ConnectionListener;

public class DropboxCheckConnectionTask extends AsyncTask<Void, Void, Boolean> {
	private ConnectionListener listener;
	private DropboxException error;
	
	public DropboxCheckConnectionTask(ConnectionListener listener) {
		this.listener = listener;
	}

	@Override
	protected void onPreExecute() {
		listener.connecting();
	}
	
	@Override
	protected Boolean doInBackground(Void... arg0) {
		try{
			DropboxAPIWrapper dropboxAPI = DropboxConnectionManager.getDropboxConnectionManager().getDropboxAPI();
			dropboxAPI.metadata("/", 0, null, true, null);
			
			return true;
		}catch(DropboxException e){
			error = e;
			e.printStackTrace();
		}
		return false;
	}


	@Override
	protected void onPostExecute(Boolean result) {
		if(result)
			listener.connected();
		else
			listener.failed(error);
	}
}
