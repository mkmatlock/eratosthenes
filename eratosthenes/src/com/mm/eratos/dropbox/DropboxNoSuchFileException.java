package com.mm.eratos.dropbox;

public class DropboxNoSuchFileException extends Exception {
	private static final long serialVersionUID = 1L;

	public DropboxNoSuchFileException(String path){
		super("No such file found in dropbox: " + path);
	}
}
