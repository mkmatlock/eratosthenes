package com.mm.eratos.dropbox;

import java.io.InputStream;
import java.io.OutputStream;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.DropboxFileInfo;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;

public class DropboxAPIWrapper {
	private DropboxAPI<AndroidAuthSession> mDBApi;
	private EratosLogger logger;
	
	public DropboxAPIWrapper(DropboxAPI<AndroidAuthSession> mDBApi){
		this.mDBApi = mDBApi;
		this.logger = EratosApplication.getApplication().getLogManager().getLogger("com.mm.eratos.dropbox");
	}

	public EratosLogger getLogger() {
		return logger;
	}

	public Entry createFolder(String path) throws DropboxException {
		return mDBApi.createFolder(path);
	}

	public void delete(String path) throws DropboxException {
		mDBApi.delete(path);
	}

	public Entry getFile(String path, String rev, OutputStream os, ProgressListener listener) throws DropboxException {
		DropboxFileInfo fileInfo = mDBApi.getFile(path, rev, os, listener);
		return fileInfo.getMetadata();
	}

	public Entry metadata(String path, int fileLimit, String hash, boolean list, String rev) throws DropboxException {
		return mDBApi.metadata(path, fileLimit, hash, list, rev);
	}

	public Entry putFile(String path, InputStream is, long length, String parentRev, ProgressListener listener)	throws DropboxException {
		return mDBApi.putFile(path, is, length, parentRev, listener);
	}

	public Entry putFileOverwrite(String path, InputStream is, long length, ProgressListener listener) throws DropboxException {
		return mDBApi.putFileOverwrite(path, is, length, listener);
	}

	public AndroidAuthSession getSession() {
		return mDBApi.getSession();
	}

	
	public Entry copy(String fromPath, String toPath) throws DropboxException {
		return mDBApi.copy(fromPath, toPath);
	}

	public Entry move(String fromPath, String toPath) throws DropboxException {
		return mDBApi.move(fromPath, toPath);
	}
}
