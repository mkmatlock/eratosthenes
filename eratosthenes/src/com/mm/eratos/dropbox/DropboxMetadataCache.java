package com.mm.eratos.dropbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.RESTUtility;
import com.dropbox.client2.exception.DropboxException;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.CaseInsensitiveMap;

public class DropboxMetadataCache {
	private static DropboxMetadataCache mMetadataCache;
	private DropboxAPIWrapper mDBApi;
	private CaseInsensitiveHashSet queried;
	
	private CaseInsensitiveMap<Entry> metadataCache;
	private CaseInsensitiveMap<DropboxException> errorPaths;
	
	private CaseInsensitiveMap<List<FileResult>> folderListing;
	
	private DropboxMetadataCache(DropboxAPIWrapper dbApi){
		this.mDBApi = dbApi;
		
		errorPaths = new CaseInsensitiveMap<DropboxException>();
		metadataCache = new CaseInsensitiveMap<Entry>();
		queried = new CaseInsensitiveHashSet();
		folderListing = new CaseInsensitiveMap<List<FileResult>>();
	}

	public boolean folderExists(String dbPath) {
		if(queried.contains(dbPath))
			return !errorPaths.containsKey(dbPath);
		try{
			cache(dbPath);
			return true;
		}catch(DropboxException de){
			return false;
		}
	}
	
	public void createFolder(EratosUri uri) throws DropboxException {
		String dbParent = uri.parent().path();
		String dbPath = uri.path();
		
		mDBApi.createFolder(makeDBPath(dbPath));
		queried.add(dbPath);
		
		folderListing.put(dbPath, new ArrayList<FileResult>());
		if(folderListing.containsKey(dbParent))
			folderListing.get(dbParent).add(new FileResult(uri, new ArrayList<FileResult>()));
		
		if(errorPaths.containsKey(dbPath))
			errorPaths.remove(dbPath);
	}
	
	private void cache(String dbPath) throws DropboxException{
		try{
			dbPath = makeDBPath(dbPath);
			mDBApi.getLogger().debug("Cacheing: %s", dbPath);
			
			Entry listing = mDBApi.metadata(dbPath, 0, null, true, null);
			
			List<FileResult> fileList = new ArrayList<FileResult>();
			
			for(Entry f : listing.contents){
				if(!f.isDir)
					metadataCache.put(f.path, f);
			
				EratosUri uri = EratosUri.parseUri(EratosUri.DROPBOX_PROTOCOL + "://" + f.path);
				long modified = RESTUtility.parseDate(f.modified).getTime();
				fileList.add(new FileResult(uri, f.rev, modified, f.isDir));
			}
			
			folderListing.put(dbPath, fileList);
			queried.add(dbPath);
		}catch(DropboxException de){
			errorPaths.put(dbPath, de);
			throw de;
		}
	}
	
	public FileResult move(EratosUri src, EratosUri dst) throws DropboxException {
		String srcPath = src.path();
		String srcParent = src.parent().path();
		
		String dstPath = dst.path();
		String dstParent = dst.parent().path();
		
		Entry e = mDBApi.move(makeDBPath(srcPath), makeDBPath(dstPath));
		metadataCache.remove(srcPath);
		metadataCache.put(dstPath, e);
		
		if(folderListing.containsKey(srcParent))
			removeFromListing(srcParent, src);
		
		EratosUri uri = EratosUri.makeUri(EratosUri.DROPBOX_PROTOCOL, e.path);
		long modified = RESTUtility.parseDate(e.modified).getTime();
		
		FileResult result = new FileResult(uri, e.rev, modified, e.isDir);
		if(folderListing.containsKey(dstParent))
			folderListing.get(dstParent).add(result);
		return result;
	}
	
	public FileResult copy(EratosUri src, EratosUri dst) throws DropboxException {
		String srcPath = src.path();
		String dstPath = dst.path();
		String dstParent = dst.parent().path();
		
		Entry e = mDBApi.copy(makeDBPath(srcPath), makeDBPath(dstPath));
		metadataCache.put(dstPath, e);
		
		EratosUri uri = EratosUri.makeUri(EratosUri.DROPBOX_PROTOCOL, e.path);
		long modified = RESTUtility.parseDate(e.modified).getTime();
		
		FileResult result = new FileResult(uri, e.rev, modified, e.isDir);
		if(folderListing.containsKey(dstParent))
			folderListing.get(dstParent).add(result);
		return result;
	}

	private void removeFromListing(String srcParent, EratosUri src) {
		FileResult match = null;
		for(FileResult r : folderListing.get(srcParent)){
			if(r.uri().equals(src)){
				match = r;
				break;
			}
		}
		if(match != null)
			folderListing.get(srcParent).remove(match);
	}

	public List<FileResult> list(EratosUri dbUri) throws DropboxException {
		String dbPath = dbUri.path();
		
		if(errorPaths.containsKey(dbPath))
			throw errorPaths.get(dbPath);
		if(!queried.contains(dbPath))
			cache(dbPath);
		
		return new ArrayList<FileResult>(folderListing.get(dbPath));
	}
	

	public List<FileResult> refresh(EratosUri dbUri) throws DropboxException {
		String dbPath = dbUri.path();
		cache(dbPath);
		
		return new ArrayList<FileResult>(folderListing.get(dbPath));
	}
	
	public Entry info(EratosUri dbUri) throws DropboxException, DropboxNoSuchFileException {
		String dbPath = dbUri.path();
		String dbParent = dbUri.parent().path();
		
		if(errorPaths.containsKey(dbParent))
			throw errorPaths.get(dbParent);
		
		if(queried.contains(dbParent) && !metadataCache.containsKey(dbPath))
			throw new DropboxNoSuchFileException(dbPath);
		
		if(!queried.contains(dbParent))
			cache(dbParent);
		
		if(metadataCache.containsKey(dbPath))
			return metadataCache.get(dbPath);
		else
			throw new DropboxNoSuchFileException(dbPath);
	}

	public FileResult pushFile(final EratosUri dst, File localFile, final IFileOperationProgressListener listener) throws FileNotFoundException, DropboxException {
		String dbPath = dst.path();
		EratosUri parent = dst.parent();
		
		if(!folderExists(parent.path()))
			createFolder(parent);
		
		Entry e = mDBApi.putFileOverwrite(makeDBPath(dbPath), new FileInputStream(localFile), localFile.length(), new ProgressListener(){
			public long progressInterval() {
				return 100;
			}
			public void onProgress(long bytes, long total) {
				listener.progress("Uploading to: " + dst, (int)bytes, (int)total);
			}
		});
		metadataCache.put(e.path, e);
		

		EratosUri uri = EratosUri.makeUri(EratosUri.DROPBOX_PROTOCOL, e.path);
		long modified = RESTUtility.parseDate(e.modified).getTime();
		
		FileResult fileResult = new FileResult(uri, e.rev, modified, e.isDir);
		folderListing.get(parent.path()).add(fileResult);
		return fileResult;
	}

	public Entry getFile(final EratosUri src, File dstFile, final IFileOperationProgressListener listener) throws DropboxException, FileNotFoundException {
		FileOutputStream fos = new FileOutputStream(dstFile);
		Entry e = mDBApi.getFile(makeDBPath(src.path()), null, fos, new ProgressListener(){
			public long progressInterval() {
				return 100;
			}
			public void onProgress(long bytes, long total) {
				listener.progress("Downloading: " + src, (int)bytes, (int)total);
			}
		});
		return e;
	}

	public static DropboxMetadataCache getMetadataCache(DropboxAPIWrapper mDBApi) {
		if(mMetadataCache == null)
			mMetadataCache = new DropboxMetadataCache(mDBApi);
		return mMetadataCache;
	}

	public void clear() {
		metadataCache.clear();
		errorPaths.clear();
		folderListing.clear();
		queried.clear();
	}

	public void delete(EratosUri uri) throws DropboxException {
		String path = uri.path();
		String parent = uri.parent().path();
		mDBApi.delete(makeDBPath(path));
		
		if(metadataCache.containsKey(path))
			metadataCache.remove(path);
		if(folderListing.containsKey(parent)){
			List<FileResult> listing = folderListing.get(parent);
			FileResult match = null;
			for(FileResult file : listing){
				if(file.uri().equals(uri)){
					match = file;
					break;
				}
			}
			
			if(match != null){
				listing.remove(match);
				folderListing.put(parent, listing);
			}
		}
	}

	public boolean fileExists(EratosUri dbUri) throws DropboxException {
		try {
			info(dbUri);
			return true;
		} catch (DropboxNoSuchFileException e) {
			return false;
		}
	}
	

	private String makeDBPath(String dstPath) {
		if(dstPath.startsWith("/")) return dstPath;
		return "/" + dstPath;
	}
}