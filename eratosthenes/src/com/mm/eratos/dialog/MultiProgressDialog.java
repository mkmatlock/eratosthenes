package com.mm.eratos.dialog;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mm.eratos.R;
import com.mm.eratos.view.ProgressView;

public class MultiProgressDialog extends DialogFragment
{
	private ProgressView primaryProgress;
	private ProgressView secondaryProgress;
	
	private int pProgress;
	private int pMaxProgress;
	private int sProgress;
	private int sMaxProgress;
	
	private String title;
	private String pSubtitle;
	private String sSubtitle;
	
	private boolean secondaryVisible;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle args = getArguments();
        
        if(savedInstanceState != null){
        	args = savedInstanceState;
        }else if(args == null){
        	args = new Bundle();
        }
        
        title = args.getString("title", "");
        
        pSubtitle = args.getString("pSubtitle", null);
        pProgress = args.getInt("pProgress", 0);
        pMaxProgress = args.getInt("pMaxProgress", 1);
		
		sSubtitle = args.getString("sSubtitle", null);
		sProgress = args.getInt("sProgress", 0);
		sMaxProgress = args.getInt("sMaxProgress", 1);
		
		secondaryVisible = args.getBoolean("secondaryVisible", false);
        
        int style = DialogFragment.STYLE_NORMAL;
        int theme = android.R.style.Theme_Holo_Light_Dialog;
        
        setRetainInstance(true);
        setCancelable(false);
        setStyle(style, theme);
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putString("title", title);
        
        outState.putString("pSubtitle", pSubtitle);
        outState.putInt("pProgress", pProgress);
        outState.putInt("pMaxProgress", pMaxProgress);
		
		outState.putString("sSubtitle", sSubtitle);
		outState.putInt("sProgress", sProgress);
		outState.putInt("sMaxProgress", sMaxProgress);
	}
	
	@Override
	public void onDestroyView()
	{
	    Dialog dialog = getDialog();
	    if ((dialog != null) && getRetainInstance())
	        dialog.setDismissMessage(null);
	    super.onDestroyView();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		getDialog().setTitle(title);
		
		View layout = inflater.inflate(R.layout.subtask_progress_dialog, null);
		
		primaryProgress = (ProgressView)layout.findViewById(R.id.primaryProgress);
		secondaryProgress = (ProgressView)layout.findViewById(R.id.secondaryProgress);
		
		setPrimarySubtitle(pSubtitle);
		setPrimaryProgress(pProgress, pMaxProgress);
		
		if(!secondaryVisible)
			hideSecondary();
		else {
			setSecondaryProgress(sProgress, sMaxProgress);
			setSecondarySubtitle(sSubtitle);
		}
		
		return layout;
	}
	
	public void setPrimarySubtitle(String subtitle){
		pSubtitle = subtitle;
		primaryProgress.setMessage(subtitle);
	}
	
	public void setPrimaryProgress(int progress, int maxProgress){
		pProgress = progress;
		pMaxProgress = maxProgress;
		
		primaryProgress.setProgress(progress, maxProgress);
	}
	
	public void setSecondarySubtitle(String subtitle){
		sSubtitle = subtitle;
		secondaryProgress.setMessage(subtitle);
	}

	public void hideSecondary(){
		secondaryVisible = false;
		secondaryProgress.setVisibility(View.GONE);
	}
	
	public void setSecondaryProgress(int progress, int maxProgress){
		secondaryVisible = true;
		secondaryProgress.setVisibility(View.VISIBLE);
		
		sMaxProgress = maxProgress;
		sProgress = progress;
		
		secondaryProgress.setProgress(sProgress, sMaxProgress);
	}

	public static MultiProgressDialog newInstance(String title){
		MultiProgressDialog frag = new MultiProgressDialog();
		Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
	}

	public static MultiProgressDialog newInstance(String title, String message) {
		MultiProgressDialog frag = new MultiProgressDialog();
		Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("pSubtitle", message);
        frag.setArguments(args);
        return frag;
	}

	public static MultiProgressDialog newInstance(String title, int progress, int maxProgress) {
		MultiProgressDialog frag = new MultiProgressDialog();
		Bundle args = new Bundle();
        args.putString("title", title);
        args.putInt("pProgress", progress);
        args.putInt("pMaxProgress", maxProgress);
        frag.setArguments(args);
        return frag;
	}

	public void dismissCheckVisible() {
		if(isVisible())
			dismissAllowingStateLoss();
	}
}
