package com.mm.eratos.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.mm.eratos.R;
import com.mm.eratos.bibtex.BibTeXStringModel;
import com.mm.eratos.utils.NotificationUtils;

public class StringEditorDialog
{
	public static interface OnFinishedListener {
		public boolean onConfirm(BibTeXStringModel string, String key, String value);
		public void onCancel();
	}

	Context mContext;
	
	EditText stringKeyView;
	EditText stringValueView;
	
	AlertDialog.Builder dialogBuilder;
	OnFinishedListener listener;
	private AlertDialog dialog;

	private BibTeXStringModel stringModel;

	
	public StringEditorDialog(Context mContext, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.listener = listener;
		this.stringModel = new BibTeXStringModel();
		
		createLayout(false);
	}
	
	public StringEditorDialog(Context mContext, BibTeXStringModel stringModel, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.stringModel = stringModel;
		this.listener = listener;
		
		createLayout(true);
	}
	
	private void createLayout(boolean editMode) {
		
		LayoutInflater inflater = LayoutInflater.from(mContext);
		
		View layout = inflater.inflate(R.layout.string_editor_dialog, null);
		stringKeyView = (EditText) layout.findViewById(R.id.string_key_view);
		stringValueView = (EditText) layout.findViewById(R.id.string_value_text);
		stringValueView.setLines(5);
		
		String goText = "Ok";
		String title = "Edit String";
		if(editMode)
			stringKeyView.setEnabled(false);
		else
			title = "New String";
		
		String key = stringModel.getKey();
		String value = stringModel.getValue();
		
		stringKeyView.setText(key);
		stringValueView.setText(value);
		
		dialogBuilder = new AlertDialog.Builder(mContext);
		dialogBuilder.setTitle(title);
		dialogBuilder.setView(layout);
		dialogBuilder.setPositiveButton(goText, null);
		dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface p1, int p2)
				{
					notifyCancel();
				}
		});
		dialog = dialogBuilder.show();
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				notifyChange();
			}
		});
	}
	
	protected void notifyCancel() {
		listener.onCancel();
	}


	private void notifyChange() {
		String key = stringKeyView.getText().toString().trim();
		String value = stringValueView.getText().toString().trim();
		
		if(key.length() == 0)
			NotificationUtils.notify(mContext, "Error", "You must enter a refkey for the string!");
		
		if(listener.onConfirm(stringModel, key, value))
			dialog.dismiss();
	}
}
