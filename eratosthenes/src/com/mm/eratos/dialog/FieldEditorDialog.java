package com.mm.eratos.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.utils.NotificationUtils;

public class FieldEditorDialog
{
	public static interface OnFinishedListener {
		public boolean onConfirm(String field, String newValue);
		public void onCancel();
	}

	Context mContext;
	BibTeXEntryModel entry;
	String field;
	
	AutoCompleteTextView fieldTypeView;
	TextView fieldValueView;
	
	AlertDialog.Builder dialogBuilder;
	OnFinishedListener listener;
	private AlertDialog dialog;
	
	public FieldEditorDialog(Context mContext, BibTeXEntryModel entry, boolean largeField, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.entry = entry;
		this.field = "";
		this.listener = listener;
		
		createLayout(false, "", largeField);
	}
	
	public FieldEditorDialog(Context mContext, BibTeXEntryModel entry, String field, boolean largeField, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.entry = entry;
		this.field = field;
		this.listener = listener;

		boolean editUTF8 = EratosApplication.getApplication().getSettingsManager().getEditInUTF8ConvertToLaTeX();
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		String value = entry.getFieldValue(field, editUTF8, helper );
		
		createLayout(true, value, largeField);
	}
	
	public FieldEditorDialog(Context mContext, BibTeXEntryModel entry, String field, String value, boolean largeField, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.entry = entry;
		this.field = field;
		this.listener = listener;
		
		createLayout(true, value, largeField);
	}
	
	private void createLayout(boolean editMode, String value, boolean largeField) {
		
		LayoutInflater inflater = LayoutInflater.from(mContext);
		
		View layout = inflater.inflate(R.layout.field_edit_dialog, null);
		fieldTypeView = (AutoCompleteTextView) layout.findViewById(R.id.field_type_view);
		fieldValueView = (TextView) layout.findViewById(R.id.field_value_text);

		if(largeField){
			fieldValueView.setLines(5);
		}
		
		String title = "Edit Field";
		if(editMode){
			fieldTypeView.setEnabled(false);
		}else{
			title = "New Field";
			String[] fieldTypes = BibTeXEntryModel.STANDARD_FIELD_SET.toArray(new String[]{});
			fieldTypeView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_single_choice, android.R.id.text1, fieldTypes));
		}
		fieldTypeView.setText(field);

		fieldValueView.setText(value);
		
		dialogBuilder = new AlertDialog.Builder(mContext);
		dialogBuilder.setTitle(title);
		dialogBuilder.setView(layout);
		dialogBuilder.setPositiveButton("Apply", null);
		dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface p1, int p2)
				{
					notifyCancel();
				}
		});
		dialog = dialogBuilder.show();
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				notifyChange();
			}
		});
	}
	
	protected void notifyCancel() {
		listener.onCancel();
	}


	private void notifyChange() {
		String field = fieldTypeView.getText().toString().trim();
		String newValue = fieldValueView.getText().toString().trim();
		
		if(field.length() == 0)
			NotificationUtils.notify(mContext, "Error", "You must enter a field name!");
		
		if(listener.onConfirm(field, newValue))
			dialog.dismiss();
	}
}
