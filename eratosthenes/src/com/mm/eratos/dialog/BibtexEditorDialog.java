package com.mm.eratos.dialog;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.ParseException;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.StringUtils;

public class BibtexEditorDialog {
	public interface BibtexEditorListener{
		public void success(List<BibTeXEntryModel> entries);
	}
	private static final int SUCCESS_COLOR = 0xFF00FF88;
	private static final int FAIL_COLOR = 0xFFFF6622;
	
	Activity mContext;
	private OnClickListener dialogClickListener;
	private ImageView statusIcon;
	private EditText bibtexEditor;
	private TextView statusText;
	private View statusView;
	private BibTeXContentProviderAdapter adapter;
	private Button commitButton;
	private Button cancelButton;
	private boolean editMode;
	private BibtexEditorListener listener;
	private BibTeXEntryModel entry;
	private BibTeXHelper helper;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private BibTeXEntryModelFactory modelFactory;
	
	public BibtexEditorDialog(Activity context) {
		adapter = new BibTeXContentProviderAdapter(context.getContentResolver());
		mContext = context;
		
		helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		modelFactory = new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(bibtexAdapter), helper);
		
		String dialogTitle = "New Entry";
		View layout = initFields(context);
		
		createDialog(context, dialogTitle, layout);
		editMode = false;
		listener = null;
	}
	

	public BibtexEditorDialog(Activity context, BibTeXEntryModel entry, BibtexEditorListener listener) {
		adapter = new BibTeXContentProviderAdapter(context.getContentResolver());
		mContext = context;
		
		helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		modelFactory = new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(bibtexAdapter), helper);
		
		String dialogTitle = "Edit Entry: " + entry.getRefKey();
		View layout = initFields(context);
		
		createDialog(context, dialogTitle, layout);
		editMode = true;
		bibtexEditor.setText(entry.getBibtex());
		this.entry = entry;
		this.listener = listener;
	}

	private View initFields(Activity context) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(R.layout.edit_raw_bibtex, null);
		
		layout.findViewById(R.id.status_wait).setVisibility(View.GONE);
		layout.findViewById(R.id.button_bar).setVisibility(View.VISIBLE);
		
		statusView = layout.findViewById(R.id.status_view);
		statusIcon = (ImageView) layout.findViewById(R.id.status_icon);
		statusText = (TextView) layout.findViewById(R.id.status_text);
		
		bibtexEditor = (EditText) layout.findViewById(R.id.bibtex_editor);
		
		statusView.setVisibility(View.GONE);
		bibtexEditor.setVisibility(View.VISIBLE);
		
		commitButton = (Button) layout.findViewById(R.id.import_button);
		commitButton.setText("Commit");
		cancelButton = (Button) layout.findViewById(R.id.cancel_button);
		
		return layout;
	}
	
	private void createDialog(Activity context, String dialogTitle, View layout) {
		int themeFlags = android.R.style.Theme_Holo_DialogWhenLarge | android.R.style.Animation_Dialog;
		
		final Dialog dialog = new Dialog(context, themeFlags);
		
		dialog.setTitle(dialogTitle);
		dialog.setContentView(layout);
		
		dialogClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v.getId() == cancelButton.getId())
					dialog.dismiss();
				if(v.getId() == commitButton.getId())
					handleResult(dialog);
			}
		};
		
		cancelButton.setOnClickListener(dialogClickListener);
		commitButton.setOnClickListener(dialogClickListener);
		
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED | 
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		dialog.show();
	}
	
	private void statusSuccess(){
		statusView.setVisibility(View.VISIBLE);
		statusView.setBackgroundColor(SUCCESS_COLOR);
		statusIcon.setImageResource(R.drawable.navigation_accept);
		statusIcon.setVisibility(View.VISIBLE);
		statusText.setTextColor(0xFF000000);
		statusText.setText("Entry validated successfully!");
	}
	
	private void statusFailed(String failure){
		statusView.setVisibility(View.VISIBLE);
		statusView.setBackgroundColor(FAIL_COLOR);
		statusIcon.setImageResource(R.drawable.navigation_cancel);
		statusIcon.setVisibility(View.VISIBLE);
		statusText.setTextColor(0xFF000000);
		statusText.setText(failure);
	}

	private void handleResult(Dialog dialog) {
		try {
			String result = bibtexEditor.getText().toString();
			
			List<BibTeXEntry> importedEntries = helper.parseBibTeXEntries(new StringReader(result));
			
			if(importedEntries.size() == 0)
				statusFailed("No entries found in input");
			
			List<BibTeXEntryModel> entries = encodeEntries(importedEntries);
			if(editMode){
				if(entries.size() != 1)
					throw new ParseException(String.format(Locale.US, "Expected 1 entry, but got %d entries", entries.size()));
				updateEntry(entries.get(0));
			}else
				addEntries(entries);
			
			statusSuccess();
			
			if(listener != null)
				listener.success(entries);
			
			dialog.dismiss();
		} catch (ParseException e) {
			statusFailed("BibTeX parser failed: " + StringUtils.oneliner(e.getMessage()));
		} catch (IOException e) {
			e.printStackTrace();
			statusFailed("Unexpected Error: " + e.getMessage());
		}
	}

	protected List<BibTeXEntryModel> encodeEntries(List<BibTeXEntry> importedEntries) throws ParseException{
		boolean convLaTeX = EratosApplication.getApplication().getSettingsManager().getConvertLaTeXtoUTF8();
		boolean convUTF8 = EratosApplication.getApplication().getSettingsManager().getConvertUTF8ToLaTeX();
		
		List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>(importedEntries.size());
		
		Set<String> addedKeys = new HashSet<String>();
		
		List<Exception> warnings = new ArrayList<Exception>();
		
		for(BibTeXEntry entry : importedEntries) {
			String entryKey = entry.getKey().getValue();
			try {
				if(convLaTeX)
					entry = helper.decodeLaTeX(entry, helper);
				if(convUTF8)
					entry = helper.decodeUnicode(entry);
				
				BibTeXEntryModel model = modelFactory.construct(entry);
				if((!editMode && adapter.hasEntry(entryKey)) || addedKeys.contains(entryKey))
					throw new KeyCollisionException("RefKey conflict '" + entryKey + "' already exists in the database");
				
				for(Exception e : BibTeXEntryVerifier.checkEntry(model.getType(), model.allFields(), model.hasXref(), helper.warningsAsErrors()))
					warnings.add(e);
				
				addedKeys.add(entryKey);
				entries.add(model);
			} catch (Exception e) {
				throw new ParseException("Error while parsing entry '"+entryKey+"': " + StringUtils.oneliner(e.getMessage()));
			} 
		}
		
		if(warnings.size()>0)
			NotificationUtils.notifyWarnings(mContext, "Entry Parser Warning", warnings);
		
		return entries;
	}

	private void updateEntry(BibTeXEntryModel newEntry) {
		try {
			if(newEntry.getRefKey().equalsIgnoreCase(entry.getRefKey())){
				adapter.update(newEntry);
			}else{
				adapter.insert(newEntry);
				adapter.delete(entry.getRefKey());
			}
		} catch (KeyCollisionException e) {
			throw new RuntimeException(e);
		}
		
		NotificationUtils.longToast(mContext, "Entry updated successfully!");
		SaveDatabaseTask.autosaveDatabase(mContext, new FileSyncResultReceiver(mContext));
	}

	protected void addEntries(List<BibTeXEntryModel> entries) {
		for(BibTeXEntryModel entry : entries){
			try {
				adapter.insert(entry);
			} catch (KeyCollisionException e) {
				throw new RuntimeException(e);
			}
		}
		
		String msg = String.format(Locale.getDefault(), "%d entries added successfully!", entries.size());
		NotificationUtils.longToast(mContext, msg);
		SaveDatabaseTask.autosaveDatabase(mContext, new FileSyncResultReceiver(mContext));
	}
}
