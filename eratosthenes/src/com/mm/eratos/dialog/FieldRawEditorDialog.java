package com.mm.eratos.dialog;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.Value;

import android.app.Activity;
import android.content.DialogInterface;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.NotificationUtils;

public class FieldRawEditorDialog {
	private Activity mContext;
	private BibTeXEntryModel entry;
	private OnFinishedListener listener;
	private Key key;
	private Key nKey;
	private Value nValue;

	public interface OnFinishedListener {
		public boolean onConfirm(Key oKey, Key nKey, Value newValue);
		public void onCancel();
	}
	
	public FieldRawEditorDialog(Activity mContext, BibTeXEntryModel entry, String key, OnFinishedListener listener) {
		this.mContext = mContext;
		this.entry = entry;
		this.key = new Key(key);
		this.listener = listener;
		
		initFields();
	}

	private void initFields() {
		try{
			NotificationUtils.promptInputLarge(mContext, "Edit Field", formatField(), new IStringResultListener() {
				public boolean onResult(String result, DialogInterface dialog) {
					return handleResult(result);
				}
			});
		} catch (Exception e) {
			NotificationUtils.notify(mContext, "Error", "Field parser error: " + e.getMessage());
		} catch (Error e) {
			NotificationUtils.notify(mContext, "Error", "Field parser error: " + e.getMessage());
		}
	}

	private boolean handleResult(String result) {
		try {
			parseField(result);
			return listener.onConfirm(key, nKey, nValue);
		} catch (Exception e) {
			NotificationUtils.notify(mContext, "Error", "Field parser error: " + e.getMessage());
		} catch (Error e) {
			NotificationUtils.notify(mContext, "Error", "Field parser error: " + e.getMessage());
		}
		return false;
	}

	private String formatField() throws IOException {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		BibTeXEntry mockEntry = new BibTeXEntry(new Key("misc"), new Key("mock-entry"));
		mockEntry.addField(key, entry.getEntry().getField(key));
		String bibtex = helper.formatBibTeX(mockEntry).replace("\n", " ").trim();
		
		Pattern p = Pattern.compile("^\\@misc\\{\\s*mock-entry,(.*)\\}$");
		Matcher m = p.matcher(bibtex);
		
		if(!m.find())
			throw new IOException("Could not parse field.");
		
		return m.group(1).trim();
	}

	private Value parseField(String result) throws IOException, ParseException{
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		String bibtex = "@misc{mock-entry, " + result + " }\n";
		List<BibTeXEntry> entries = helper.parseBibTeXEntries(new StringReader(bibtex));
		if(entries.size() == 0)
			throw new ParseException("Could not parse field!");
		
		Map<Key, Value> fields = entries.get(0).getFields();
		for(Key k : fields.keySet()){
			nKey = k;
			nValue = fields.get(k);
			return fields.get(k);
		}
		throw new ParseException("No field found! You must specify a field");
	}
}
