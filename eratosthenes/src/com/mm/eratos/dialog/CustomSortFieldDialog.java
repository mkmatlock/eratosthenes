package com.mm.eratos.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.utils.NotificationUtils;

public class CustomSortFieldDialog implements OnClickListener {

	public interface FinishedListener{
		public void finished(Map<String, String> results);
		public void cancelled();
	}
	
	private Context mContext;
	private FinishedListener listener;
	private Dialog dialog;
	private Button cancelButton;
	private Button commitButton;
	private ImageButton addButton;
	private ListView fieldList;
	private ArrayAdapter<String> adapter;
	private ItemActionCompositeAdapter<String> removeAdapter;
	private ItemActionCompositeAdapter<String> headerAdapter;

	private Map<String, String> fields;
	private EratosApplication application;
	
	private OnClickListener dialogClickListener;

	private ItemActionCallback<String> removeCallback = new ItemActionCallback<String>() {
		@Override
		public void perform(int position, String item, View v) {
			final String field = item;
			NotificationUtils.confirmDialog(mContext, "Remove Sort Field?",  "Remove sort field: " + item, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					fields.remove(field);
					adapter.remove(field);
				}
			});
		}

		@Override
		public void initialize(int position, String item, View v) {
			
		}
	};
	
	private ItemActionCallback<String> headerCallback = new ItemActionCallback<String>() {
		@Override
		public void perform(int position, String item, View v) {
			
		}

		@Override
		public void initialize(int position, String item, View v) {
			String type = fields.get(item);
			
			if(type.equals(EratosSettings.CUSTOM_SORT_FIELD_TYPE_FULL_HEADER))
				v.setVisibility(View.VISIBLE);
			else
				v.setVisibility(View.GONE);
			
			v.setEnabled(false);
		}
	};
	
	public CustomSortFieldDialog(Context mContext, FinishedListener listener){
		this.mContext = mContext;
		this.listener = listener;
		this.application = EratosApplication.getApplication();
	}

	private View initFields(Context context) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(R.layout.custom_sort_field_dialog, null);
		
		layout.findViewById(R.id.button_bar).setVisibility(View.VISIBLE);
		
		fieldList = (ListView) layout.findViewById(R.id.field_list_view);
		
		addButton = (ImageButton) layout.findViewById(R.id.button_new_field);
		commitButton = (Button) layout.findViewById(R.id.commit_button);
		cancelButton = (Button) layout.findViewById(R.id.cancel_button);
		
		return layout;
	}

	private void createDialog(Context context, String dialogTitle, View layout) {
		int themeFlags = android.R.style.Theme_Holo_DialogWhenLarge | android.R.style.Animation_Dialog;
		
		dialog = new Dialog(context, themeFlags);
		dialog.setTitle(dialogTitle);
		dialog.setContentView(layout);
		
		dialogClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v.getId() == cancelButton.getId()){
					dismiss();
				}if(v.getId() == commitButton.getId()){
					handleResult();
				}
			}
		};
		
		addButton.setOnClickListener(this);
		cancelButton.setOnClickListener(dialogClickListener);
		commitButton.setOnClickListener(dialogClickListener);
		
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED | 
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		setListContents();
	}
	
	protected void handleResult() {
		listener.finished(fields);
		dialog.dismiss();
	}

	protected void dismiss() {
		listener.cancelled();
		dialog.dismiss();
	}

	private void setListContents() {
		fields = application.getSettingsManager().getSortFields();
		List<String> fieldNameList = new ArrayList<String>( fields.keySet() );
		
		adapter = new ArrayAdapter<String>(mContext, R.layout.custom_sort_field_entry, R.id.fieldName, fieldNameList);
		removeAdapter = new ItemActionCompositeAdapter<String>(R.id.removeField, removeCallback , adapter);
		headerAdapter = new ItemActionCompositeAdapter<String>(R.id.headerModeBackground, headerCallback, removeAdapter);
		fieldList.setAdapter(headerAdapter);
	}

	public void create() {
		View layout = initFields(mContext);
		createDialog(mContext, "Custom Sort Fields", layout);
		dialog.show();
	}

	public void onClick(View v) {
		if(v.getId() == addButton.getId()){
			createNewSortFieldDialog();
		}
	}


	private void createNewSortFieldDialog() {
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View v = inflater.inflate(R.layout.custom_sort_field_new, null);
		
		new AlertDialog.Builder(mContext)
						.setTitle("New Sort Field")
						.setView(v)
						.setCancelable(true)
						.setNegativeButton("Cancel", NotificationUtils.doNothingListener)
						.setPositiveButton("Create", new CreateSortFieldListener(v))
						.show();
	}
	
	private class CreateSortFieldListener implements DialogInterface.OnClickListener {
		private EditText fieldName;
		private CheckBox fullField;

		public CreateSortFieldListener(View v){
			fieldName = (EditText)v.findViewById(R.id.fieldName);
			fullField = (CheckBox)v.findViewById(R.id.fieldHeaderFull);
		}

		public void onClick(DialogInterface dialog, int which) {
			String fieldText = fieldName.getText().toString();
			boolean checked = fullField.isChecked();
			if(addField(fieldText, checked)) {
				dialog.dismiss();
			}
		}
		
		public boolean addField(String result, boolean checked) {
			if(!result.matches("[a-zA-Z][a-zA-Z0-9\\-_]+")){
				NotificationUtils.longToast(mContext, "Invalid Field Name Format");
				return false;
			}
			fields.put(result, checked ? EratosSettings.CUSTOM_SORT_FIELD_TYPE_FULL_HEADER : EratosSettings.CUSTOM_SORT_FIELD_TYPE_FIRST_CHAR);
			adapter.add(result);
			
			return true;
		}
	}
}
