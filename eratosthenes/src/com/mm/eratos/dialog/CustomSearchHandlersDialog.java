package com.mm.eratos.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.search.CustomSearchHandler;
import com.mm.eratos.utils.NotificationUtils;

public class CustomSearchHandlersDialog implements OnClickListener, OnItemClickListener {

	public interface FinishedListener{
		public void finished(List<CustomSearchHandler> results);
		public void cancelled();
	}
	
	private Context mContext;
	private FinishedListener listener;
	private Dialog dialog;
	private Button cancelButton;
	private Button commitButton;
	private ImageButton addButton;
	private ListView fieldList;
	private ArrayAdapter<String> adapter;
	private ItemActionCompositeAdapter<String> removeAdapter;
	private ItemActionCompositeAdapter<String> headerAdapter;

	private Map<String, CustomSearchHandler> searchHandlers;
	private EratosApplication application;
	
	private OnClickListener dialogClickListener;

	private ItemActionCallback<String> removeCallback = new ItemActionCallback<String>() {
		@Override
		public void perform(int position, String item, View v) {
			final String handlerName = item;
			NotificationUtils.confirmDialog(mContext, "Remove Search Handler?",  "Remove custom search handler: " + item, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					searchHandlers.remove(handlerName);
					adapter.remove(handlerName);
				}
			});
		}

		@Override
		public void initialize(int position, String item, View v) {
			
		}
	};
	
	private ItemActionCallback<String> headerCallback = new ItemActionCallback<String>() {
		@Override
		public void perform(int position, String item, View v) {
			
		}

		@Override
		public void initialize(int position, String item, View v) {
			v.setVisibility(View.GONE);
			v.setEnabled(false);
		}
	};
	
	public CustomSearchHandlersDialog(Context mContext, FinishedListener listener) {
		this.mContext = mContext;
		this.listener = listener;
		this.application = EratosApplication.getApplication();
	}

	private View initFields(Context context) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(R.layout.custom_sort_field_dialog, null);
		
		layout.findViewById(R.id.button_bar).setVisibility(View.VISIBLE);
		
		fieldList = (ListView) layout.findViewById(R.id.field_list_view);
		fieldList.setOnItemClickListener(this);
		
		addButton = (ImageButton) layout.findViewById(R.id.button_new_field);
		commitButton = (Button) layout.findViewById(R.id.commit_button);
		cancelButton = (Button) layout.findViewById(R.id.cancel_button);
		
		return layout;
	}

	private void createDialog(Context context, String dialogTitle, View layout) {
		int themeFlags = android.R.style.Theme_Holo_DialogWhenLarge | android.R.style.Animation_Dialog;
		
		dialog = new Dialog(context, themeFlags);
		dialog.setTitle(dialogTitle);
		dialog.setContentView(layout);
		
		dialogClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v.getId() == cancelButton.getId()){
					dismiss();
				}if(v.getId() == commitButton.getId()){
					handleResult();
				}
			}
		};
		
		addButton.setOnClickListener(this);
		cancelButton.setOnClickListener(dialogClickListener);
		commitButton.setOnClickListener(dialogClickListener);
		
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED | 
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		searchHandlers = application.getSettingsManager().getSearchHandlers();
		setListContents();
	}
	
	protected void handleResult() {
		listener.finished(new ArrayList<CustomSearchHandler>( searchHandlers.values() ));
		dialog.dismiss();
	}

	protected void dismiss() {
		listener.cancelled();
		dialog.dismiss();
	}

	private void setListContents() {
		List<String> fieldNameList = new ArrayList<String>( searchHandlers.keySet() );
		
		adapter = new ArrayAdapter<String>(mContext, R.layout.custom_sort_field_entry, R.id.fieldName, fieldNameList);
		removeAdapter = new ItemActionCompositeAdapter<String>(R.id.removeField, removeCallback , adapter);
		headerAdapter = new ItemActionCompositeAdapter<String>(R.id.headerModeBackground, headerCallback, removeAdapter);
		fieldList.setAdapter(headerAdapter);
	}

	public void create() {
		View layout = initFields(mContext);
		createDialog(mContext, "Custom Sort Fields", layout);
		dialog.show();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == addButton.getId()){
			createEditSearchHandlerDialog(new CustomSearchHandler("", ""), "Create");
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
		String handlerName = (String) adapter.getItemAtPosition(pos);
		createEditSearchHandlerDialog( searchHandlers.get(handlerName), "Edit" );
	}

	private void createEditSearchHandlerDialog(CustomSearchHandler handler, String commitButton) {
		final String originalHandlerName = handler.name();
		
		View layout = LayoutInflater.from(mContext).inflate(R.layout.three_item_text_entry_dialog, null);
		final EditText field1 = (EditText)layout.findViewById(R.id.Field1);
		field1.setHint("Handler name...");
		field1.setText(handler.name());
		
		final EditText field2 = (EditText)layout.findViewById(R.id.Field2);
		field2.setHint("Handler url...");
		field2.setText(handler.url());
		
		final EditText field3 = (EditText)layout.findViewById(R.id.Field3);
		field3.setHint("BibTeX entry types...");
		field3.setText(handler.types());
		
		final AlertDialog dialog = new AlertDialog.Builder(mContext)
				.setTitle("New Search Handler")
				.setView(layout)
				.setNegativeButton("Cancel", NotificationUtils.doNothingListener)
				.setPositiveButton(commitButton, null).show();
		
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = field1.getText().toString().trim();
				String searchString = field2.getText().toString().trim();
				String typeString = field3.getText().toString().trim();
				String []types = typeString.split("[ ;,]+");
				
				if(name.isEmpty())
					NotificationUtils.longToast(mContext, "Handler Name cannot be empty");
				else if(searchString.isEmpty())
					NotificationUtils.longToast(mContext, "Handler URL cannot be empty");
				else{
					if(searchHandlers.containsKey(originalHandlerName))
						searchHandlers.remove(originalHandlerName);
					
					searchHandlers.put(name, new CustomSearchHandler(name, searchString, types));
					setListContents();
					dialog.dismiss();
				}
			}
		});
	}

}
