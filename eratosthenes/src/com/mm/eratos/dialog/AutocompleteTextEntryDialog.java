package com.mm.eratos.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.mm.eratos.R;
import com.mm.eratos.utils.NotificationUtils;

public class AutocompleteTextEntryDialog {
	public static interface OnFinishedListener {
		public boolean onConfirm(String value);
		public void onCancel();
	}
	
	private Context mContext;
	private Builder dialogBuilder;
	private AutoCompleteTextView editTextView;
	private AlertDialog dialog;
	private String title;
	private Set<String> validValues;
	private OnFinishedListener listener;

	public AutocompleteTextEntryDialog(Context mContext, String title, Set<String> validValues, OnFinishedListener listener){
		this.mContext = mContext;
		this.title = title;
		this.validValues = validValues;
		this.listener = listener;
	}

	public void createDialog(String hint, String initialValue) {
		LayoutInflater inflater = LayoutInflater.from(mContext);
		
		View layout = inflater.inflate(R.layout.edit_text_autocomplete_dialog, null);
		
		List<String> valueList = new ArrayList<String>(validValues.size());
		valueList.addAll(validValues);
		
		editTextView = (AutoCompleteTextView) layout.findViewById(R.id.field_type_view);
		editTextView.setHint(hint);
		editTextView.setText(initialValue);
		editTextView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_single_choice, android.R.id.text1, valueList));
		
		dialogBuilder = new AlertDialog.Builder(mContext);
		dialogBuilder.setTitle(title);
		dialogBuilder.setView(layout);
		dialogBuilder.setPositiveButton("Apply", null);
		dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface p1, int p2)
				{
					notifyCancel();
				}
		});
		dialog = dialogBuilder.show();
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				notifyChange();
			}
		});
	}
	
	protected void notifyCancel() {
		listener.onCancel();
	}

	private void notifyChange() {
		String value = editTextView.getText().toString().trim();
		
		if(!validValues.contains(value))
			NotificationUtils.notify(mContext, "Not Valid", String.format(Locale.US, "The value '%s' is not valid", value));
		else if(listener.onConfirm(value))
			dialog.dismiss();
	}
}
