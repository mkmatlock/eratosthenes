package com.mm.eratos.model;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.utils.ArrayUtils;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.StringUtils;

public class EratosUri implements Comparable<EratosUri>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7824873497849194551L;
	
	private static final String UP_DIRECTORY = "..";
	private static final String THIS_DIRECTORY = ".";
	private static Set<String> remoteProtocols;
	
	public static final String ROOT_PROTOCOL = "root";
	public static final String FILE_PROTOCOL = "device";
	public static final String DROPBOX_PROTOCOL = "dropbox";
	public static final String DRIVE_PROTOCOL = "drive";
	public static final String BIBTEX_LOCAL_PROTOCOL = "bibtex";
	public static final String LIBRARY_LOCAL_PROTOCOL = "library";
	
	static{
		remoteProtocols = new CaseInsensitiveHashSet();
		remoteProtocols.add(DROPBOX_PROTOCOL);
		remoteProtocols.add(DRIVE_PROTOCOL);
	}
	
	private static final String URI_PATTERN = "([a-zA-Z0-9]+)://(.*?)(?:\\?(.*?))?(?:#(.*?))?";
	
	private static Pattern uriMatcher = Pattern.compile(URI_PATTERN);
	public static final String PATH_DELIMITER = "/";
	
	private final String protocol;
	private final String path;
	private final String name;
	private final String extension;
	private String[] elements;
	
	private String query;
	private String fragment;
	
	protected EratosUri(String protocol, String path, String query, String fragment){
		this.protocol = protocol;
		this.path = path;
		this.query = query;
		this.fragment = fragment;
		
		int index = path.lastIndexOf('/');
		if(index > -1)
			name = path.substring(index+1);
		else
			name = new String(path);
		
		
		index = name.lastIndexOf('.');
		if(index > -1)
			extension = name.substring(index+1);
		else
			extension = "";
		
		elements = getElements(path);
	}
	
	public int length() {
		return elements.length;
	}
	
	public List<String> elements(){
		return Arrays.asList(elements);
	}
	
	public String protocol(){
		return protocol;
	}
	
	public String path() {
		return path;
	}
	
	public String name(){
		return name;
	}

	public String extension() {
		return extension;
	}

	public String query() {
		return query == null ? "" : query;
	}
	
	public String fragment() {
		return fragment == null ? "" : fragment;
	}
	
	public EratosUri host() {
		return new EratosUri(protocol, elements[0], null, null);
	}
	
	public String toString() {
		return protocol + "://" + path + (query == null ? "" : "?" + query) + (fragment == null ? "" : "#" + fragment);
	}

	public EratosUri copy() {
		return new EratosUri(protocol, path, query, fragment);
	}

	public EratosUri format(String ... args) {
		return EratosUri.parseUri(String.format(Locale.US, toString(), (Object[]) args));
	}
	
	public Matcher extract(String pattern) {
		String decode = EratosUri.decode(path);
		return Pattern.compile(pattern).matcher(decode);
	}
	
	public boolean matches(String matchPattern) {
		String decode = EratosUri.decode(path);
		boolean matches = decode.matches(matchPattern);
		return matches;
	}
	
	public EratosUri query(String nQuery) {
		return new EratosUri(protocol, path, nQuery, fragment);
	}
	
	public EratosUri append(String subpath) {
		String newPath = "";
		
		if(path.isEmpty())
			newPath = subpath;
		else if(path.endsWith(PATH_DELIMITER) && subpath.startsWith(PATH_DELIMITER))
			newPath = path + subpath.substring(1);
		else if(path.endsWith(PATH_DELIMITER) || subpath.startsWith(PATH_DELIMITER))
			newPath = path + subpath;
		else
			newPath = path + PATH_DELIMITER + subpath;
			
		
		return new EratosUri(protocol, newPath, null, null);
	}

	public EratosUri parent() {
		String nPath = "";
		int index = path.lastIndexOf('/');
		if(index == 0){
			nPath = "/";
		}
		else if(index > -1){
			nPath = path.substring(0, index);
		}
		return new EratosUri(protocol, nPath, null, null);
	}
	
	public List<String> slice(int position){
		return ArrayUtils.slice(elements, position);
	}
	
	public String relativeTo(EratosUri uri) {
		if(!protocol.equals(uri.protocol()))
			throw new IllegalArgumentException(protocol + " != " + uri.protocol());
		
		String [] path1 = elements;
		String [] path2 = uri.elements;
		
		int i = 0;
		while(i < path1.length && i < path2.length && path1[i].equals( path2[i] ))
			i++;
		
		int up = path2.length - i;
		
		StringBuilder relpath = new StringBuilder();
		for(int j = 0; j < up; j++){
			relpath.append(UP_DIRECTORY);
			relpath.append(PATH_DELIMITER);
		}
		
		for(int j = i; j < path1.length; j++){
			relpath.append(path1[j]);
			relpath.append(PATH_DELIMITER);
		}
		
		return relpath.substring(0, relpath.length()-1);
	}

	public boolean childOf(EratosUri uri) {
		return uri.protocol().equals(protocol) && path.startsWith(uri.path);
	}
	
	public EratosUri resolveUri(EratosApplication app) {
		if(LIBRARY_LOCAL_PROTOCOL.equals(protocol)){
			EratosUri libStore = app.getSettingsManager().getStoreLocalLocation();
			return libStore.resolveUri(app).append( path );
		}else if(BIBTEX_LOCAL_PROTOCOL.equals(protocol)){
			EratosUri bibStore = app.getCurrentLibraryPath();
			return bibStore.append( path );
		}
		
		return copy();
	}
	
	public EratosUri resolveToRootFolder(String folderName, String newProtocol) {		
		int i = 0;
		for(String element : elements){
			if(element.equals(folderName))
				return EratosUri.parseUri(newProtocol + ":///" + StringUtils.join(PATH_DELIMITER, slice(i+1)));
			i++;
		}
		return copy();
	}
	
	public boolean isRemoteProtocol(EratosApplication app) {
		String srcProtocol = protocol;
		
		if(LIBRARY_LOCAL_PROTOCOL.equals(protocol))
			srcProtocol = app.getSettingsManager().getStoreLocalLocation().resolveUri(app).protocol();
		else if(BIBTEX_LOCAL_PROTOCOL.equals(protocol)) 
			srcProtocol = app.getCurrentLibraryPath().resolveUri(app).protocol();
		
		return remoteProtocols.contains(srcProtocol);
	}
	
	public boolean isDropbox(EratosApplication app){
		String srcProtocol = protocol;

		if(LIBRARY_LOCAL_PROTOCOL.equals(protocol))
			srcProtocol = app.getSettingsManager().getStoreLocalLocation().resolveUri(app).protocol();
		else if(BIBTEX_LOCAL_PROTOCOL.equals(protocol)) 
			srcProtocol = app.getCurrentLibraryPath().resolveUri(app).protocol();
		
		return srcProtocol.equals(DROPBOX_PROTOCOL);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result
				+ ((protocol == null) ? 0 : protocol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EratosUri other = (EratosUri) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (protocol == null) {
			if (other.protocol != null)
				return false;
		} else if (!protocol.equals(other.protocol))
			return false;
		return true;
	}
	
	private static String[] getElements(String path) {
		if(path.isEmpty())
			return new String[]{};
		return path.split(PATH_DELIMITER, -1);
	}
	
	private static String resolvePath(String path) {
		String[] elements = getElements(path);
		List<String> nElements = new ArrayList<String>(elements.length);
		for(String el : elements)
			if(!el.equals(THIS_DIRECTORY))
				nElements.add(el);
		return StringUtils.join(PATH_DELIMITER, nElements);
	}
	
	public static EratosUri parseUri(String uri){
		Matcher m = uriMatcher.matcher(uri);
		
		if(m.matches()){
			String protocol = m.group(1).toLowerCase(Locale.US);
			String path = resolvePath(m.group(2));
			String query = m.group(3);
			String fragment = m.group(4);
			return new EratosUri(protocol, path, query, fragment);
		}
		return null;
	}

	private static String decode(String path) {
		path = path.replace("%3A", ":");
		path = path.replace("%2F", "/");
		return path;
	}

	public static EratosUri makeUri(String protocol, String path) {
		return EratosUri.parseUri(protocol + "://" + path);
	}

	@Override
	public int compareTo(EratosUri o) {
		return toString().compareTo(o.toString());
	}

	public URI toURI() {
		return URI.create(toString());
	}
}
