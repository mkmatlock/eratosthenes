package com.mm.eratos.files;
 
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.util.Log;
 
 
public class Compress { 
  private static final int BUFFER = 2048; 
 
  private List<String> _files; 
  private File _zipFile; 
 
  public Compress(List<String> attachedFiles, File zipFile) { 
    _files = attachedFiles; 
    _zipFile = zipFile; 
  } 
 
  public void zip() throws IOException { 
    try  { 
      BufferedInputStream origin = null; 
      FileOutputStream dest = new FileOutputStream(_zipFile); 
      
      ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest)); 
 
      byte data[] = new byte[BUFFER]; 
 
      for(String path : _files) { 
        Log.v("Compress", "Adding: " + path); 
        FileInputStream fi = new FileInputStream(path); 
        origin = new BufferedInputStream(fi, BUFFER); 
        ZipEntry entry = new ZipEntry(path.substring(path.lastIndexOf("/") + 1)); 
        out.putNextEntry(entry); 
        int count; 
        while ((count = origin.read(data, 0, BUFFER)) != -1) { 
          out.write(data, 0, count); 
        } 
        origin.close(); 
      } 
 
      out.close(); 
    } catch(Exception e) { 
      throw new IOException(e);
    } 
 
  } 
 
} 
