package com.mm.eratos.files;

import java.io.File;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.NotificationUtils;

public class FileCacheListener implements IUriResultListener {
	private final Context mContext;
	private IUriResultListener listener;

	public FileCacheListener(Context context) {
		this.mContext = context;
		this.listener = null;
	}
	
	public FileCacheListener(Context context, IUriResultListener listener) {
		this.mContext = context;
		this.listener = listener;
	}

	@Override
	public boolean onResult(EratosUri uri) {
		try{
			File f = FileManager.getFileManager().getFile(uri);

			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setDataAndType(Uri.fromFile(f), "application/pdf");
			i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			
			if(listener != null)
				listener.onResult(uri);
			
			mContext.startActivity(i);
		}catch(ActivityNotFoundException e){
			NotificationUtils.notify(mContext, "Error", "Please install an application to handle this file type!");
		}catch(Exception e){
			NotificationUtils.notifyException(mContext, "Unexpected Error", e);
		}
		return true;
	}
}