package com.mm.eratos.files;

import com.mm.eratos.protocol.FileResult;

public interface IFileOperationResultListener {
	public void started(String taskName);
	public void finished(FileResult result);
	public void failed(Throwable error);
}