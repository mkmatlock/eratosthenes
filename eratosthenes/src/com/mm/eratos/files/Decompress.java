package com.mm.eratos.files;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class Decompress {
	private static final int BUFFER = 2048; 
	  
	private File zipFile;
	private String path;

	public Decompress(File zipFile, String path){
		this.zipFile = zipFile;
		this.path = path;
	}
	
	public List<String> unzip() throws ZipException, IOException {
		ZipFile zf = new ZipFile(zipFile);
		byte data[] = new byte[BUFFER]; 
		
		List<String> filenames = new ArrayList<String>(zf.size());
		Enumeration<? extends ZipEntry> entries = zf.entries();
		while(entries.hasMoreElements()){
			ZipEntry entry = entries.nextElement();
			String outputPath = path + "/" + entry.getName();
			
			if(entry.isDirectory()){
				new File(outputPath).mkdirs();
			}else{
				InputStream is = zf.getInputStream(entry);
				FileOutputStream out = new FileOutputStream(new File(outputPath));
				
				filenames.add(outputPath);
				
				int count; 
		        while ((count = is.read(data, 0, BUFFER)) != -1) { 
		          out.write(data, 0, count); 
		        } 				
		        out.close();
	        }
			
		}
		
		zf.close();
		return filenames;
	}
}
