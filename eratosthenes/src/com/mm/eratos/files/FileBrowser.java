package com.mm.eratos.files;

import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.widget.Button;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolConnectionManager.ConnectionListener;

public class FileBrowser {
	public String DEFAULT_PROTOCOL_DIR_POSTFIX = "_default_dir";
	
	EratosUri currentDir;
	EratosUri defaultPath;
	private ProtocolHandler protocol;

	private Button activateButton;

	private Activity mContext;
	
	public FileBrowser(Activity mContext, ProtocolHandler protocol, Button activateButton){
		this.mContext = mContext;
		this.protocol = protocol;
		this.activateButton = activateButton;
	}
	
	public EratosUri getDefaultPath(){
		String key = protocol.name() + DEFAULT_PROTOCOL_DIR_POSTFIX;
		String defaultPath = EratosApplication.getApplication().getPreferences().getString(key, protocol.root().toString());
		return EratosUri.parseUri(defaultPath);
	}

	public void storeDefaultPath(EratosUri rootPath) {
		String key = protocol.name() + DEFAULT_PROTOCOL_DIR_POSTFIX;
		Editor edit = EratosApplication.getApplication().getPreferences().edit();
		edit.putString(key, rootPath.toString());
		edit.commit();
	}
	
	public EratosUri root(){
		return protocol.root();
	}
	
	public ProtocolHandler getProtocolHandler(){
		return protocol;
	}

	public void setCurrentDirectory(EratosUri uri){
		currentDir = uri;
	}
	
	public EratosUri getCurrentDirectory(){
		if(currentDir != null)
			return currentDir;
		return root();
	}
	
	public void activate() {
		activateButton.setActivated(true);
	}
	
	public void deactivate() {
		activateButton.setActivated(false);
	}
	
	public void switchTo(ConnectionListener listener) {
		ProtocolConnectionManager connectionManager = ProtocolConnectionManager.getConnectionManager(protocol.root().protocol());
		connectionManager.connect(mContext, listener);
	}
}
