package com.mm.eratos.files;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.exceptions.FileAlreadyExistsException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.protocol.SyncedFileOp;
import com.mm.eratos.task.PruneFilesTask;
import com.mm.eratos.utils.NotificationUtils;

public class FileManager {
	public static final int SYNC_NOP = 0;
	public static final int SYNC_PUSH = 1;
	public static final int SYNC_PULL = 2;
	public static final int SYNC_DELETE = 3;
	public static final int SYNC_UPDATE = 4;
	
	public static final String DEFAULT_DROPBOX_PATH = "Library/.dropbox";
	public static final String DEFAULT_DRIVE_PATH = "Library/.drive";
	public static final String DEFAULT_EXPORT_PATH = "Library";
	public static final String DOCUMENT_MODEL_DIR = ".docmodel";
	public static final String DOCUMENT_MODEL_EXT = ".model";
	
	public static final String TEMPORARY_EXPORT_FILE = "export.bib";
	public static final String TEMPORARY_EXPORT_ARCHIVE = "export.bib.zip";
	public static final String TEMPORARY_EXPORT_PDF_FILE = "export.pdf";
	public static final String SCRATCH_FILE = "scratch";
	
	private EratosApplication app;
	private EratosSettings settingsManager;
	private static FileManager singleton;
	private String externalStoragePath;

	private FileManager(){
		app = EratosApplication.getApplication();
		settingsManager = app.getSettingsManager();
	}
	
	public static FileManager getFileManager() {
		if(singleton == null)
			singleton = new FileManager();
		return singleton;
	}
	
	public void setExternalStoragePath(String path){
		externalStoragePath = path;
	}

	public void setExportPath(String string) {
		
	}
	
	public void createDefaultDirectories() throws IOException {
		if(!Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED ))
			throw new IOException("External storage is not available: " + Environment.getExternalStorageState());
		
		externalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
		String dropboxSyncPath = externalStoragePath + EratosUri.PATH_DELIMITER + DEFAULT_DROPBOX_PATH;
		
		File dropboxDir = new File(dropboxSyncPath);
		if(!dropboxDir.exists() && !dropboxDir.mkdirs())
			throw new IOException("Could not create dropbox sync folder on external storage");
	}
	
	public File getRandomAccessFile() {
		return new File(app.getFilesDir(), SCRATCH_FILE);
	}
	
	public EratosUri getBackupFile(EratosUri exportUri) {
		return EratosUri.parseUri(exportUri.toString() + ".bak");
	}

	public EratosUri getTemporaryUri(String filename) {
		return settingsManager.getExportUri().append( filename );
	}
	
	public EratosUri getTemporaryExportArchiveUri() {
		return settingsManager.getExportUri().append( TEMPORARY_EXPORT_ARCHIVE );
	}
	
	public EratosUri getTemporaryExportUri() {
		return settingsManager.getExportUri().append( TEMPORARY_EXPORT_FILE );
	}
	

	public EratosUri getTemporaryExportPdfUri() {
		return settingsManager.getExportUri().append( TEMPORARY_EXPORT_PDF_FILE );
	}

	public EratosUri getDefaultExportUri() {
		return EratosUri.parseUri( "device:///" + DEFAULT_EXPORT_PATH );
	}
	
	public EratosUri getDefaultDropboxUri() {
		return EratosUri.parseUri( "device:///" + DEFAULT_DROPBOX_PATH );
	}
	
	public EratosUri getDefaultDriveUri() {
		return EratosUri.parseUri( "device:///" + DEFAULT_DRIVE_PATH );
	}
	
	public EratosUri getDefaultAttachmentUri(String filename) {
		return EratosUri.parseUri("library://" + settingsManager.getStoreLocalSubdir()).append(filename);
	}
	
	public File getFile(EratosUri uri) {
		String protocolStorage = getProtocolStorage(uri);
		String fPath = protocolStorage + EratosUri.PATH_DELIMITER + uri.path();
		fPath = fPath.replace("//", "/");
		return new File(fPath);
	}

	public File getFileCreateParent(EratosUri uri) throws IOException {
		File f = getFile(uri);
		File p = f.getParentFile();
		if(!p.exists() && !p.mkdirs())
			throw new IOException("Could not create parent directory: " + p.getAbsolutePath());
		return f;
	}

	public EratosUri getAnnotationFileUri(EratosUri documentUri) {
		String fn = documentUri.name() + DOCUMENT_MODEL_EXT;
		return documentUri.parent().append(DOCUMENT_MODEL_DIR).append(fn);
	}

	private boolean fileExists(EratosUri fileUri, FileRevisionContentProviderAdapter fileAdapter) {
		File dst = getFile(fileUri);
		if(dst.exists())
			return true;
		if(fileAdapter.exists(fileUri)){
			SyncedFile sf = fileAdapter.getFile(fileUri);
			return !sf.deleted();
		}
		return false;
	}

	private String getProtocolStorage(EratosUri uri) {
		if(EratosUri.ROOT_PROTOCOL.equals(uri.protocol())){
			return "/";
		}else if(EratosUri.FILE_PROTOCOL.equals(uri.protocol())){
			return externalStoragePath;
		}else if(EratosUri.DROPBOX_PROTOCOL.equals(uri.protocol())){
			EratosUri dropboxUri = settingsManager.getDropboxExportUri();
			return externalStoragePath + dropboxUri.path();
		}else if(EratosUri.DRIVE_PROTOCOL.equals(uri.protocol())){
			EratosUri driveUri = settingsManager.getDriveExportUri();
			return externalStoragePath + driveUri.path();
		}else if(EratosUri.BIBTEX_LOCAL_PROTOCOL.equals(uri.protocol())){
			EratosUri bibtexUri = app.getCurrentLibraryPath();
			return getProtocolStorage(bibtexUri) + bibtexUri.path();
		}else if(EratosUri.LIBRARY_LOCAL_PROTOCOL.equals(uri.protocol())){
			EratosUri libraryUri = settingsManager.getStoreLocalLocation();
			return getProtocolStorage(libraryUri) + libraryUri.path();
		}
		return null;
	}

	public EratosUri resolveIfLocal(EratosUri uri) {
		EratosUri storageUri = settingsManager.getStoreLocalLocation();
		String storagePath = getFile(storageUri).getAbsolutePath() + "/";
		String resolvePath = getFile(uri).getAbsolutePath();
		
		if(resolvePath.startsWith(storagePath))
			uri = EratosUri.parseUri( EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + resolvePath.substring(storagePath.length()) );
		
		return uri.copy();
	}
	
	public boolean isCached(EratosUri srcUri) {
		File srcFile = getFile(srcUri);
		return srcFile.exists();
	}

	private void attemptCopy(Activity mContext, EratosUri srcUri, EratosUri dstUri, FileRevisionContentProviderAdapter fileAdapter, final IFileOperationResultListener listener, boolean deleteSrc, boolean forceOverwrite) {
		ProtocolHandler srcProtocol = ProtocolHandler.getProtocolHandler(srcUri);
		
		if(!forceOverwrite && fileExists(dstUri, fileAdapter)) {
			listener.failed(new FileAlreadyExistsException(dstUri));
			return;
		}
		
		File dstDir = getFile(dstUri).getParentFile();
		if(!dstDir.exists() && !dstDir.mkdirs()) {
			listener.failed(new IOException("Could not create target directory: " + dstUri.parent()));
			return;
		}
		
		if(srcUri.protocol().equals(dstUri.protocol())) {
			if(deleteSrc)
				srcProtocol.move(srcUri, dstUri, listener);
			else
				srcProtocol.copy(srcUri, dstUri, listener);
		} else {
			AbstractProgressFileOperationResultListener fetchListener = new AbstractProgressFileOperationResultListener(mContext) {
				protected void success(FileResult result) {
					listener.finished(result);
				}
				protected void failure(Throwable error) {
					listener.failed(error);
				}
			};
			srcProtocol.fetch(srcUri, dstUri, deleteSrc, fetchListener, fetchListener);
		}
	}

	public EratosUri renameFile(final EratosUri srcUri, String newName, FileRevisionContentProviderAdapter fileAdapter, boolean important) throws IOException {
		EratosUri parentUri = srcUri.parent();
		EratosUri nUri = parentUri.append(newName);
		
		moveFile(fileAdapter, srcUri.resolveUri(app), nUri.resolveUri(app), important);
		return resolveIfLocal(nUri);
	}

	public void moveFile(FileRevisionContentProviderAdapter fileAdapter, EratosUri srcUri, EratosUri dstUri, boolean important) throws IOException {
		File src = getFile(srcUri);
		File dst = getFile(dstUri);
		
		boolean doCopy = src.exists();
		if(doCopy){
			FileUtils.copy(src, dst);
			src.delete();
		}
		
		
		boolean srcRemote = srcUri.isRemoteProtocol(app);
		boolean dstRemote = dstUri.isRemoteProtocol(app);
		
		if(!doCopy && srcRemote && dstRemote){
			SyncedFileOp op = SyncedFileOp.createFileMoveOp(srcUri, dstUri);
			fileAdapter.queueFileOp(op);
			
			SyncedFile srcFile = fileAdapter.getFile(srcUri);
			if(fileAdapter.exists(dstUri)){
				int destId = fileAdapter.getFile(dstUri).id();
				fileAdapter.update(
						srcFile.assignId(destId)
								.setUri(dstUri)
								.setLastModified(dst.lastModified())
								.revision("")
								.setCached(dst.exists())
								.setImportant(important));
			}else{
				fileAdapter.newSyncedFile(dstUri, "", dst, important);
			}
			
		}else{
			SyncedFile file = null;
			if(srcRemote) {
				file = fileAdapter.getFile(srcUri);
				fileAdapter.update(file.delete());
			}
			if(dstRemote) {
				if(file != null && fileAdapter.exists(dstUri)){
					int destId = fileAdapter.getFile(dstUri).id();
					fileAdapter.update(
							file.assignId(destId)
								.setUri(dstUri)
								.setLastModified(dst.lastModified())
								.revision("")
								.setCached(dst.exists())
								.setImportant(important));
				}else if(file != null){
					fileAdapter.newSyncedFile(dstUri, "", dst, important);
				}else if(fileAdapter.exists(dstUri)){
					SyncedFile dFile = fileAdapter.getFile(dstUri);
					fileAdapter.update(
							dFile.setUri(dstUri)
								 .setLastModified(dst.lastModified())
								 .revision("")
								 .setCached(dst.exists())
								 .setImportant(important));
				}else{
					fileAdapter.newSyncedFile(dstUri, "", dst, important);
				}
			}
		}
	}
	
	public void addFileToLibrary(Activity mContext, EratosUri srcUri, String filename, boolean forceOverwrite, FileRevisionContentProviderAdapter fileAdapter, IFileOperationResultListener listener) {
		boolean manage = settingsManager.getManagedFiles();
		boolean isRemote = srcUri.isRemoteProtocol(app);
		boolean localOnly = settingsManager.getCopyLocalOnly();
		
		EratosUri dstUri = getDefaultAttachmentUri(filename).resolveUri(app);
		
		if(srcUri.equals(dstUri)) {
			listener.finished(new FileResult(srcUri));
		} else if(manage && !(isRemote && localOnly)) {
			attemptCopy(mContext, srcUri, dstUri, fileAdapter, listener, settingsManager.getDeleteOriginals(), forceOverwrite);
		} else {
			listener.finished(new FileResult(srcUri));
		}
	}
	
	public void fetchFile(final Activity mContext, final EratosUri uri, final IUriResultListener listener) {
		final File file = getFile(uri);
		
		File parentDir = file.getParentFile();
		if(!parentDir.exists() && !parentDir.mkdirs()){
			NotificationUtils.notifyException(mContext, "Error Caching File", new IOException("Could not create local directory: " + uri.parent()));
			return;
		}
		
		if(uri.isRemoteProtocol(app)){
			ProtocolConnectionManager connectionManager = ProtocolConnectionManager.getConnectionManager(uri.protocol());
			connectionManager.connect(mContext,
					new ProtocolConnectionManager.ConnectionListenerNotifier(connectionManager, mContext){
						public void success() {
							ProtocolHandler protocol = ProtocolHandler.getProtocolHandler(uri);
							AbstractProgressFileOperationResultListener fetchListener = new AbstractProgressFileOperationResultListener(mContext) {
								protected void success(FileResult result) {
									postFileCached(mContext, result, listener);
								}
								protected void failure(Throwable error) {
									if(file.exists()) file.delete();
								}
							};
							protocol.fetch(uri, uri, false, fetchListener, fetchListener);
						}
					});				
		} else if( file.exists() ){
			postFileAccessed(mContext, uri, listener);
		} else {
			NotificationUtils.notify(mContext, "Error Caching File", "File does not exist!");
		}
	}
	
	public void cacheFile(final Activity mContext, final EratosUri uri, final IUriResultListener listener) {
		final File file = getFile(uri);
		
		File parentDir = file.getParentFile();
		if(!parentDir.exists() && !parentDir.mkdirs()){
			NotificationUtils.notifyException(mContext, "Error Caching File", new IOException("Could not create local directory: " + uri.parent()));
			return;
		}
		
		if( file.exists() ){
			postFileAccessed(mContext, uri, listener);
		} else if(uri.isRemoteProtocol(app)){
			ProtocolConnectionManager connectionManager = ProtocolConnectionManager.getConnectionManager(uri.protocol());
			connectionManager.connect(mContext,
					new ProtocolConnectionManager.ConnectionListenerNotifier(connectionManager, mContext){
						public void success() {
							ProtocolHandler protocol = ProtocolHandler.getProtocolHandler(uri);
							AbstractProgressFileOperationResultListener fetchListener = new AbstractProgressFileOperationResultListener(mContext) {
								protected void success(FileResult result) {
									postFileCached(mContext, result, listener);
								}
								protected void failure(Throwable error) {
									if(file.exists()) file.delete();
								}
							};
							protocol.fetch(uri, uri, false, fetchListener, fetchListener);
						}
					});				
		} else {
			NotificationUtils.notify(mContext, "Error Caching File", "File does not exist!");
		}
	}

	private void postFileAccessed(Activity mContext, EratosUri uri, IUriResultListener listener) {
		FileRevisionContentProviderAdapter fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		uri = uri.resolveUri(EratosApplication.getApplication());
		
		if(fileAdapter.exists(uri)) {
			SyncedFile sf = fileAdapter.getFile(uri);
			fileAdapter.update(sf.accessed().setCached(true));
		}
		
		listener.onResult(uri);
	}

	private void postFileCached(Context mContext, FileResult result, IUriResultListener listener) {
		FileRevisionContentProviderAdapter fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		EratosUri uri = result.uri();
		
		uri = uri.resolveUri(EratosApplication.getApplication());
		
		if(fileAdapter.exists(uri)){
			SyncedFile sf = fileAdapter.getFile(uri);
			File f = getFile(uri);
			
			sf = sf.revision(result.revision())
					.accessed()
					.setCached(true)
					.setLastModified(f.lastModified());
			fileAdapter.update(sf);
			new PruneFilesTask(mContext).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}
		
		listener.onResult(uri);
	}

	public boolean keepCached(SyncedFile sf) {
		return !settingsManager.storeOnlyRecentFiles() || sf.important();
	}
	
	public int decideSyncPriority(SyncedFile sf, String rev, long remoteModified) {
		boolean storeRecent = settingsManager.storeOnlyRecentFiles();
		File localFile = FileManager.getFileManager().getFile(sf.uri());
		long localModified = localFile.lastModified();
		
		int syncPriority = SYNC_NOP;
		if(sf.deleted())
			syncPriority = SYNC_DELETE;
		
		if(sf.cached() && !localFile.exists()){
			syncPriority = SYNC_PULL;
		}
		
		if(!sf.cached() && !sf.important() && storeRecent)
			syncPriority = SYNC_UPDATE;
		
		if(sf.cached() || !storeRecent || sf.important()){
			if(!sf.cached()){
				syncPriority = SYNC_PULL;
			} else if(rev.equals(sf.lastRevision())) {
				if(sf.lastModified() < localModified ){
					syncPriority = SYNC_PUSH;
				}
			} else {
				if( localModified < remoteModified ){
					syncPriority = SYNC_PULL;
				} else if (localModified >= remoteModified ) {
					syncPriority = SYNC_PUSH;
				}
			}
		}
		return syncPriority;
	}

	public String getSyncString(int syncPriority) {
		switch(syncPriority){
		case SYNC_DELETE:
			return "delete";
		case SYNC_PULL:
			return "pull";
		case SYNC_PUSH:
			return "push";
		case SYNC_UPDATE:
			return "update";
		case SYNC_NOP:
			return "nop";
		}
		return null;
	}
}
