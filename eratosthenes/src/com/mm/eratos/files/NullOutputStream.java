package com.mm.eratos.files;

import java.io.IOException;
import java.io.OutputStream;

public class NullOutputStream extends OutputStream {
    @Override
	public void close() throws IOException {
	}

	@Override
	public void flush() throws IOException {
	}

	@Override
	public void write(byte[] buffer, int offset, int count) throws IOException {
	}

	@Override
	public void write(byte[] buffer) throws IOException {
	}

	@Override
	public void write(int arg0) throws IOException {
	}
}