package com.mm.eratos.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import android.webkit.MimeTypeMap;

import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.Functional;

public class FileUtils {
	private static final Functional.PatternFind patternFind = new Functional.PatternFind();
	private static final Functional.Any any = new Functional.Any();
	
	public static List<String> grep(File f, String encoding, Pattern ... patterns) throws IOException {
		BufferedReader br = new BufferedReader( new InputStreamReader( new FileInputStream(f), encoding ) );
		
		try{
			List<String> matchedText = new ArrayList<String>();
			
			String line = null;
			while((line = br.readLine()) != null){
				patternFind.text = line;
				List<Boolean> patternMatches = Functional.map( patterns, patternFind );
				
				boolean matches = Functional.reduce(patternMatches, false, any);
				if(matches) matchedText.add(line);
			}
			
			return matchedText;
		}finally{
			br.close();
		}
	}
	
	public static void copy(File src, File dst) throws IOException {
		File parent = dst.getParentFile();
		if(!parent.exists() && parent.mkdirs())
			throw new IOException("Could not create directories: " + parent.getAbsolutePath());
		
	    InputStream in = new FileInputStream(src);
	    OutputStream out = new FileOutputStream(dst);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	
	public static String computeMD5(File pdfFile) throws IOException {
        // Create MD5 Hash
		try {
	        MessageDigest digest = MessageDigest.getInstance("MD5");
	        
	        long expBytes = pdfFile.length();
	        
	        if(expBytes == 0)
	        	throw new IOException("Cannot hash empty file!");
	        long readBytes = 0;
	        
	        FileInputStream fis = new FileInputStream(pdfFile);
	        byte[] buffer = new byte[4096];
	        do{
	        	int bytes = fis.read(buffer);
	        	digest.update(buffer, 0, bytes);
	        	readBytes+=bytes;
	        }while(readBytes < expBytes);
	        fis.close();
	        
	        byte messageDigest[] = digest.digest();
	        // Create Hex String
	        StringBuilder hexString = new StringBuilder();
	        for (byte aMessageDigest : messageDigest) {
	            String h = Integer.toHexString(0xFF & aMessageDigest);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new IOException(e);
		}
	}

	public static String getMimeType(EratosUri uri) {
		MimeTypeMap mime = MimeTypeMap.getSingleton();
		return mime.getMimeTypeFromExtension(uri.extension());
	}

	public static byte[] readBytes(File file) throws IOException {
		byte[] fileBytes = new byte[(int)file.length()];
		
		FileInputStream fis = new FileInputStream(file);
		fis.read(fileBytes);
		fis.close();
		return fileBytes;
	}

	public static long readStreamToFile(InputStream in, File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		long read = 0;
		
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) != -1) {
		    out.write(buffer, 0, len);
		    read += len;
		}
		
		out.close();
		in.close();
		return read;
	}
}