package com.mm.eratos.files;

public interface IFileOperationProgressListener {
	public void progress(String message, int progress, int maxprogress);
}
