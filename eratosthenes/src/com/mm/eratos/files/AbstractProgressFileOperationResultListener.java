package com.mm.eratos.files;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.utils.NotificationUtils;

public abstract class AbstractProgressFileOperationResultListener implements IFileOperationProgressListener, IFileOperationResultListener {

	private final Activity mContext;
	private MultiProgressDialog progressDialog;
	private String mTaskName;

	public AbstractProgressFileOperationResultListener(Activity mContext){
		this.mContext = mContext;
	}

	@Override
	public void started(String taskName) {
		this.mTaskName = taskName;
		progressDialog = MultiProgressDialog.newInstance(taskName + "...", 0, 100);
		progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}

	@Override
	public void progress(String message, int progress, int maxprogress) {
		if(message != null)
			progressDialog.setPrimarySubtitle(message);
		progressDialog.setPrimaryProgress(progress, maxprogress);
	}

	@Override
	public void finished(FileResult result) {
        progressDialog.dismissCheckVisible();
       	NotificationUtils.longToast(mContext, mTaskName + " succeeded");
       	success(result);
	}
	
	@Override
	public void failed(final Throwable error) {
		progressDialog.dismissCheckVisible();
		NotificationUtils.notifyException(mContext, mTaskName + " Failed", error, new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				failure(error);
			}
		});
	}

	protected abstract void success(FileResult result);
	protected abstract void failure(Throwable error);
}
