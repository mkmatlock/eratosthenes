package com.mm.eratos.files;

import com.mm.eratos.model.EratosUri;

public interface IUriResultListener {
	public boolean onResult(EratosUri result);
}
