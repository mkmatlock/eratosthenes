package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Checkable;

public class SelectableItemCompositeAdapter<T> extends CompositeAdapter {

	public static interface ItemSelectedCallback<T> {
		public abstract void selected(int position, T item);
		public abstract void deselected(int position, T item);
		public abstract boolean initialize(int position, T item, View v);
	}
	
	private class ClickableListener implements OnClickListener {
		private int itemPosition;

		public ClickableListener(int itemPosition){
			this.itemPosition = itemPosition;
		}
		
		@Override
		public void onClick(View buttonView) {
			boolean isChecked = selectedItems.contains(itemPosition);
			if(!isChecked){
				select(itemPosition);
			} else {
				deselect(itemPosition);
			}
		}
	}
	
	private final ItemSelectedCallback<T> selectedCallback;
	private HashSet<Integer> selectedItems;
	private int clickableElement;
	private int checkableElement;
	
	public SelectableItemCompositeAdapter(int checkableElement, int clickableElement, ItemSelectedCallback<T> callback, Adapter compositeAdapter) {
		super(compositeAdapter);
		this.clickableElement = clickableElement;
		this.checkableElement = checkableElement;
		this.selectedCallback = callback;
		this.selectedItems = new HashSet<Integer>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		view = getCompositingView(pos, view, parent);
		
		View clickable = view.findViewById(clickableElement);
    	Checkable checkable = (Checkable) view.findViewById(checkableElement);
    	
    	clickable.setFocusable(false);
    	clickable.setFocusableInTouchMode(false);
    	
    	clickable.setOnClickListener(new ClickableListener(pos));
    	
    	boolean isSelected = selectedCallback.initialize(pos, (T) getItem(pos), clickable);
    	isSelected = isSelected || selectedItems.contains(pos);
    	if(isSelected)
    		selectedItems.add(pos);
		checkable.setChecked(isSelected);
		
        return view;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getSelectedItems(){
		List<T> selected = new ArrayList<T>(selectedItems.size());
		for(Integer pos : selectedItems){
			selected.add( (T) getItem(pos) );
		}
		return selected;
	}
	
	public int countSelectedItems(){
		return selectedItems.size();
	}

	public void deselectAll() {
		selectedItems.clear();
		notifyDataSetChanged();
	}
	
	public void selectAll() {
		for(int i = 0; i < getCount(); i++)
			selectedItems.add(i);
		notifyDataSetChanged();
	}

	@SuppressWarnings("unchecked")
	public Set<Integer> getSelectedPositions() {
		return (Set<Integer>) selectedItems.clone();
	}

	@SuppressWarnings("unchecked")
	public void select(int itemPosition) {
		selectedItems.add(itemPosition);
		selectedCallback.selected(itemPosition, (T) getItem(itemPosition));
		notifyDataSetChanged();
	}

	@SuppressWarnings("unchecked")
	public void deselect(int itemPosition) {
		selectedItems.remove(itemPosition);
		selectedCallback.deselected(itemPosition, (T) getItem(itemPosition));
		notifyDataSetChanged();
	}
}
