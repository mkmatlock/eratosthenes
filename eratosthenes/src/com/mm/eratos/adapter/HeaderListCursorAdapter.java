package com.mm.eratos.adapter;
import java.util.ArrayList;
import java.util.Locale;

import za.co.immedia.pinnedheaderlistview.SectionedBaseAdapter;
import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;

public class HeaderListCursorAdapter extends SectionedBaseAdapter {
	
	private final Adapter compositeAdapter;
	ArrayList<String> sectionHeaders;
	SparseIntArray sectionCounts;
	SparseIntArray sectionIndex;
	ArrayList<Integer> sectionNumbers;
	
	private String sectionColumn;
	private int section_layout;
	private int header_id;
	private Context context;
	private boolean prefixOnly;
	
	public HeaderListCursorAdapter(Context context, int section_layout, int header_id, String sectionColumn, boolean prefixOnly, Adapter cAdapter){
		super();
		this.context = context;
		this.section_layout = section_layout;
		this.header_id = header_id;
		this.sectionColumn = sectionColumn;
		this.prefixOnly = prefixOnly;
		compositeAdapter = cAdapter;
		
		sectionHeaders = new ArrayList<String>();
		sectionCounts = new SparseIntArray();
		sectionIndex = new SparseIntArray();
		sectionNumbers = new ArrayList<Integer>();
		
		updateSectionCount();
		
		compositeAdapter.registerDataSetObserver(new DataSetObserver() {
			public void onChanged() {
				updateSectionCount();
				notifyDataSetChanged();
			}
			public void onInvalidated() {
				notifyDataSetInvalidated();
			}
		});
	}
	
	public void setSectionColumn(String sectionColumn, boolean prefixOnly){
		this.sectionColumn = sectionColumn;
		this.prefixOnly = prefixOnly;
	}
	
	protected void updateSectionCount() {
		sectionHeaders.clear();
		sectionCounts.clear();
		sectionIndex.clear();
		sectionNumbers.clear();
		
		int s = -1;
		for(int i = 0; i < compositeAdapter.getCount(); i++){
			Cursor c = (Cursor) compositeAdapter.getItem(i);
			String section = " ";
			if(sectionColumn != null)
				section = c.getString( c.getColumnIndex( sectionColumn ) );
			
			if(prefixOnly && section.length() > 0)
				section = section.substring(0, 1).toUpperCase(Locale.US);
			
			if(sectionHeaders.size() == 0 || !section.equalsIgnoreCase(sectionHeaders.get(sectionHeaders.size() - 1))){
				s++;
				sectionHeaders.add(section);
				
				sectionCounts.put(s, 1);
				sectionIndex.put(s, i);
			}else{
				sectionCounts.put(s, sectionCounts.get(s) + 1);
			}
			
			sectionNumbers.add(s);
		}
	}

	public Adapter getCompositeAdapter(){
		return compositeAdapter;
	}

	
	@Override
	public Object getItem(int section, int position) {
		int index = getAbsolutePosition(section, position);
		return compositeAdapter.getItem(index);
	}

	@Override
	public long getItemId(int section, int position) {
		int index = getAbsolutePosition(section, position);
		return compositeAdapter.getItemId(index);
	}

	@Override
	public int getSectionCount() {
		return sectionHeaders.size();
	}

	@Override
	public int getCountForSection(int section) {
		return sectionCounts.get(section);
	}
	
	@Override
	public View getItemView(int section, int position, View convertView, ViewGroup parent) {
		int index = getAbsolutePosition(section, position);
		return compositeAdapter.getView(index, convertView, parent);
	}

	public int getAbsolutePosition(int section, int position) {
		return sectionIndex.get(section) + position;
	}

	@Override
	public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		convertView = vi.inflate(section_layout, null);
		}
    	((TextView)convertView.findViewById(header_id)).setText( sectionHeaders.get(section) );
		return convertView;
	}

	public int getPositionForItem(int pos) {
		if(pos < sectionNumbers.size())
			return pos + sectionNumbers.get(pos) + 1;
		return -1;
	}

}
