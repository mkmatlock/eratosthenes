package com.mm.eratos.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

public class ImmutableAdapter extends CompositeAdapter {
	public ImmutableAdapter(Adapter cAdapter) {
		super(cAdapter);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCompositeAdapter().getView(position, convertView, parent);
	}
}
