package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mm.eratos.R;
import com.mm.eratos.activity.FileBrowserActivity.FileFilter;
import com.mm.eratos.protocol.FileResult;

public class DirectoryAdapter extends BaseAdapter {

	private List<FileResult> filtered;
	private final Context context;
	
    public DirectoryAdapter(Context context, List<FileResult> listing, FileFilter fileFilter, boolean isRoot){
        this.context = context;
        
        filtered = new ArrayList<FileResult>();
		for(FileResult file : listing){
        	if(fileFilter.filter(file.uri(), file.isDir())){
        		filtered.add(file);
        	}
        }
        
        Collections.sort(filtered, new Comparator<FileResult>() {
			@Override
			public int compare(FileResult lhs, FileResult rhs) {
				if(lhs.isDir() && !rhs.isDir()){
					return -1;
				}else if(!lhs.isDir() && rhs.isDir()){
					return 1;
				}else{
					return lhs.uri().name().compareToIgnoreCase(rhs.uri().name());
				}
			}
		});
        
        if(!isRoot)
        	filtered.add(0, null);
    }

	@Override
    public int getCount() {
        return filtered.size();
    }

    @Override
    public View getView(int pos, View view, ViewGroup parent) {
    	LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	
    	FileResult item = filtered.get(pos);
    	
    	if(item == null){
    		view = vi.inflate(R.layout.file_system_folder_element, null);
	        TextView fileName = (TextView) view.findViewById(R.id.folderNameView);
	        fileName.setText("..");
    	}else if(item.isDir()){
	    	view = vi.inflate(R.layout.file_system_folder_element, null);
	        TextView fileName = (TextView) view.findViewById(R.id.folderNameView);
	        fileName.setText(item.uri().name());
    	}else{
	    	view = vi.inflate(R.layout.file_system_file_element, null);
	        TextView fileName = (TextView) view.findViewById(R.id.fileNameView);
	        fileName.setText(item.uri().name());
    	}
        return view;
    }

	@Override
	public Object getItem(int pos) {
		return filtered.get(pos);
	}

	@Override
	public long getItemId(int position) {
		FileResult item = filtered.get(position);
		if(item != null){
			return item.hashCode();
		}
		return 0;
	}
}