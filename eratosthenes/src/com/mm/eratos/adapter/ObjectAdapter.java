package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ObjectAdapter<T> extends BaseAdapter {
	
	private Context mContext;
	private ObjectMapper<T> mapper;
	private List<T> objects;

	public interface ObjectMapper<T> {
		public View map(LayoutInflater inflater, T obj, View convertView, ViewGroup parent);
	}

	public ObjectAdapter(Context context, ObjectMapper<T> mapper){
		this.mContext = context;
		this.mapper = mapper;
		this.objects = new ArrayList<T>();
	}
	
	public void setItems(Collection<T> objects){
		this.objects = new ArrayList<T>(objects);
		notifyDataSetChanged();
	}
	
	public void addItem(T obj) {
		this.objects.add(obj);
		notifyDataSetChanged();
	}
	
	public void removeItem(T obj) {
		this.objects.remove(obj);
		notifyDataSetChanged();
	}

	public List<T> getItems() {
		return new ArrayList<T>(this.objects);
	}
	
	public void clearItems() {
		this.objects = new ArrayList<T>();
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return this.objects.size();
	}

	@Override
	public Object getItem(int position) {
		return this.objects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return this.objects.get(position).hashCode();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		T obj = objects.get(position);
		convertView = mapper.map(LayoutInflater.from(mContext), obj, convertView, parent);
		return convertView;
	}

	public void replaceItem(T oldItem, T newItem) {
		int index = objects.indexOf(oldItem);
		objects.remove(oldItem);
		objects.add(index, newItem);
	}
}
