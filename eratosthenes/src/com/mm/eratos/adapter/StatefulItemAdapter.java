package com.mm.eratos.adapter;

import java.util.HashSet;
import java.util.Set;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

public class StatefulItemAdapter<T, S> extends CompositeAdapter {
	public interface StatefulView<S> {
		public S getState();
		public void setState(S state);
	}
	
	public static interface SetupStateCallback<T, S> {
		public S defaultState();
		public S stateChanged(int position, T item, S oldState, S newState);
		public S setupState(int position, T item, StatefulView<S> view, S state);
	}
	
	private int statefulElementId;
	private SetupStateCallback<T, S> callbacks;
	private SparseArray<S> states;
	
	public StatefulItemAdapter(int statefulElementId, SetupStateCallback<T, S> callbacks, Adapter compositeAdapter){
		super(compositeAdapter);
		this.statefulElementId = statefulElementId;
		this.states= new SparseArray<S>(); 
		this.callbacks = callbacks;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = getCompositingView(position, convertView, parent);
		
		StatefulView<S> statefulView = (StatefulView<S>) convertView.findViewById(statefulElementId);
		S state = states.get(position, callbacks.defaultState());
		
		state = callbacks.setupState(position, (T) getItem(position), statefulView, state);
		statefulView.setState(state);
		states.put(position, state);
		
		return convertView;
	}
	
	public Set<Integer> filterByState(S filter) {
		Set<Integer> filtered = new HashSet<Integer>();
		for(int i = 0; i < getCount(); i++){
			if(filter.equals(states.get(i)))
				filtered.add(i);
		}
		return filtered;
	}
	
	@SuppressWarnings("unchecked")
	public void setState(int position, S state) {
		T item = (T) getItem(position);
		S oldState = states.get(position, callbacks.defaultState());
		state = callbacks.stateChanged(position, item, oldState, state);
		states.put(position, state);
		
		notifyDataSetChanged();
	}

	public void setAllStates(S state) {
		for(int position = 0; position < getCount(); position++)
			states.put(position, state);
		
		notifyDataSetChanged();
	}
}
