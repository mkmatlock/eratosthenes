package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryCountAdapter extends BaseAdapter {
	List<String> categories;
	Map<String, Integer> categoryIcons;
	Map<String, Integer> categoryCounts;
	
	Context mContext;
	private int categoryLayout;
	private int categoryLabelId;
	private int categoryCountId;
	private int categoryIconId;
	
	public CategoryCountAdapter(Context context, int categoryLayout, int categoryIconId, int categoryLabelId, int categoryCountId) {
		mContext = context;
		this.categoryLayout = categoryLayout;
		this.categoryIconId = categoryIconId;
		this.categoryLabelId = categoryLabelId;
		this.categoryCountId = categoryCountId;
		
		categories = new ArrayList<String>();
		categoryIcons = new HashMap<String, Integer>();
		categoryCounts = new HashMap<String, Integer>();
	}
	
	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public Object getItem(int position) {
		return categories.get(position);
	}

	@Override
	public long getItemId(int position) {
		return categories.get(position).hashCode();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = LayoutInflater.from(mContext).inflate(categoryLayout, null);
		
		String category = categories.get(position);
		String categoryCount = Integer.toString(categoryCounts.get(category));
		int categoryDrawable = categoryIcons.get(category);
		
		ImageView categoryIconView = (ImageView) convertView.findViewById(categoryIconId);
		categoryIconView.setImageResource(categoryDrawable);
		
		TextView categoryLabelTextView = (TextView) convertView.findViewById(categoryLabelId);
		categoryLabelTextView.setText(category);
		
		TextView categoryCountTextView = (TextView) convertView.findViewById(categoryCountId);
		categoryCountTextView.setText(categoryCount);
		
		return convertView;
	}
	
	public void addCategory(String category, int drawable, int count) {
		assert !categoryCounts.containsKey(category);
		
		categories.add(category);
		categoryIcons.put(category, drawable);
		categoryCounts.put(category, count);
	}
	
	public boolean hasCategory(String category){
		return categoryCounts.containsKey(category);
	}
	
	public void setCategoryCount(String category, int count) {
		assert categoryCounts.containsKey(category);
		
		categoryCounts.put(category, count);
	}
	
	public void sort(){
		Collections.sort(categories);
		notifyDataSetChanged();
	}
}
