package com.mm.eratos.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

public class CustomCursorAdapter extends CursorAdapter {
	private CursorMapper mapper;

	public interface CursorMapper {
		public void bind(View view, Context context, Cursor cursor);
		public View create(Context context, Cursor cursor, ViewGroup parent);
	}
	
	public CustomCursorAdapter(Context context, Cursor c, CursorMapper mapper) {
		super(context, c, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		this.mapper = mapper;
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		mapper.bind(view, context, cursor);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return mapper.create(context, cursor, parent);
	}
}