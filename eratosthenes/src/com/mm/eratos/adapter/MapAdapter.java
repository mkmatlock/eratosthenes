package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MapAdapter extends BaseAdapter {

    private Map<String, String> mData = new HashMap<String, String>();
    private ArrayList<String> mKeys;
	private final int field_name_id;
	private final int field_value_id;
	private final int layout_id;
	private final Context context;
	
    public MapAdapter(Context context, Map<String, String> data, int layout_id, int field_name_id, int field_value_id){
        this.context = context;
		this.mData = new HashMap<String, String>(data);
		this.layout_id = layout_id;
        
		this.field_name_id = field_name_id;
		this.field_value_id = field_value_id;
		
		
		mKeys = new ArrayList<String>();
		for(String key : mData.keySet()){
			mKeys.add(key);
		}
		Collections.sort(mKeys);
    }

    @Override
    public int getCount() {
        return mData.size();
    }
    
    public int position(String field){
    	return mKeys.indexOf(field);
    }
    
    public void put(String field, String content){
    	if(!mData.containsKey(field))
    		mKeys.add(field);
    	
    	mData.put(field, content);
    	notifyDataSetChanged();
    }
    
	public void remove(String field) {
		int idx = mKeys.indexOf(field);
		if(idx != -1){
			mKeys.remove(idx);
			mData.remove(field);
			notifyDataSetChanged();
		}
	}

    @Override
    public View getView(int pos, View view, ViewGroup parent) {
    	if(view == null){
	    	LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	view = vi.inflate(layout_id, null);
    	}
    	
        String key = mKeys.get(pos);
        String value = mData.get(key).toString();
        
        TextView fieldName = (TextView) view.findViewById(field_name_id);
        TextView fieldValue = (TextView) view.findViewById(field_value_id);
        
        fieldName.setText(key);
        fieldValue.setText(value);
        
        return view;
    }

	@Override
	public Object getItem(int pos) {
		return mKeys.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}