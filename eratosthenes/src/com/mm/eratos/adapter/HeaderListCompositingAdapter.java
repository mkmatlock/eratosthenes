package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.List;

import za.co.immedia.pinnedheaderlistview.SectionedBaseAdapter;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;

public class HeaderListCompositingAdapter extends SectionedBaseAdapter {
	private int headerLayout;
	private int headerLabel;
	
	private List<String> sections;
	private List<Adapter> adapters;
	private Context mContext;

	public HeaderListCompositingAdapter(Context context, int headerLayout, int headerLabel) {
		this.mContext = context;
		
		this.headerLayout = headerLayout;
		this.headerLabel = headerLabel;
		
		this.sections = new ArrayList<String>();
		this.adapters = new ArrayList<Adapter>();
	}
	
	public void addSection(String sectionTitle, Adapter adapter) {
		sections.add(sectionTitle);
		adapters.add(adapter);
		
		adapter.registerDataSetObserver(new DataSetObserver() {
			@Override
			public void onChanged() {
				notifyDataSetChanged();
			}
			@Override
			public void onInvalidated() {
				notifyDataSetInvalidated();
			}
		});
	}
	
	@Override
	public Object getItem(int section, int position) {
		if(position == -1)
			return sections.get(section);
		return adapters.get(section).getItem(position);
	}

	@Override
	public long getItemId(int section, int position) {
		if(position == -1)
			return sections.get(section).hashCode();
		return adapters.get(section).getItemId(position);
	}

	@Override
	public int getSectionCount() {
		return sections.size();
	}

	@Override
	public int getCountForSection(int section) {
		return adapters.get(section).getCount();
	}

	@Override
	public View getItemView(int section, int position, View convertView, ViewGroup parent) {
		return adapters.get(section).getView(position, convertView, parent);
	}

	@Override
	public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = LayoutInflater.from(mContext).inflate(headerLayout, null);
		
		String sectionLabel = sections.get(section);
		TextView sectionLabelTextView = (TextView) convertView.findViewById(headerLabel);
		sectionLabelTextView.setText(sectionLabel);
		
		return convertView;
	}

	public String getSectionTitle(int section) {
		return sections.get(section);
	}

	public int getAbsolutePosition(int section, int position) {
		int absolutePos = 0;
		for(int i = 0; i < section; i++){
			absolutePos += getCountForSection(i);
			absolutePos += 1;
		}
		return absolutePos + position;
	}
}
