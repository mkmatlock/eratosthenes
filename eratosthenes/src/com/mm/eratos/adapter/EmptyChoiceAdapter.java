package com.mm.eratos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;

public class EmptyChoiceAdapter extends CompositeAdapter {

	private Context context;
	public EmptyChoiceAdapter(Context context, Adapter cAdapter) {
		super(cAdapter);
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return super.getCount() + 1;
	}

	@Override
	public Object getItem(int position) {
		if(position == 0)
			return null;
		return super.getItem(position - 1);
	}

	@Override
	public long getItemId(int position) {
		if(position == 0)
			return 0;
		return super.getItemId(position - 1);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(position == 0){
			View layout = LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_dropdown_item, null);
			((TextView)layout.findViewById(android.R.id.text1)).setText("");
			
			return layout;
		}
		
		return super.getCompositingView(position - 1, convertView, parent);
	}
}
