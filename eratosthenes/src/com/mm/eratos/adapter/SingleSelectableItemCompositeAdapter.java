package com.mm.eratos.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

public class SingleSelectableItemCompositeAdapter<T> extends CompositeAdapter {

	public static interface ItemSelectedCallback<T> {
		public abstract void selected(int position, T item);
		public abstract void deselected(int position, T item);
		public abstract void initialize(int position, T item, View v, boolean selected);
	}
	
	private final ItemSelectedCallback<T> selectedCallback;
	private int selectedItem;
	private int selectableElementId;
	
	public SingleSelectableItemCompositeAdapter(int selectableElementId, ItemSelectedCallback<T> callback, Adapter compositeAdapter) {
		super(compositeAdapter);
		this.selectableElementId = selectableElementId;
		this.selectedCallback = callback;
		this.selectedItem = ListView.INVALID_POSITION;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		view = getCompositingView(pos, view, parent);
		
    	View selectable = view.findViewById(selectableElementId);
    	boolean isSelected = selectedItem == pos;
    	selectedCallback.initialize(pos, (T) getItem(pos), selectable, isSelected);
    	
        return view;
	}
	
	@SuppressWarnings("unchecked")
	public T getSelectedItem(){
		if(selectedItem != ListView.INVALID_POSITION)
			return (T) getItem(selectedItem);
		return null;
	}
	
	public int getSelectedPosition() {
		return selectedItem;
	}

	@SuppressWarnings("unchecked")
	public void select(int itemPosition) {
		if(selectedItem != ListView.INVALID_POSITION)
			selectedCallback.deselected(selectedItem, (T) getItem(selectedItem));
		
		selectedItem = itemPosition;
		
		if(selectedItem != ListView.INVALID_POSITION)
			selectedCallback.selected(selectedItem, (T) getItem(selectedItem));
		
		notifyDataSetChanged();
	}

	@SuppressWarnings("unchecked")
	public void deselect() {
		if(selectedItem != ListView.INVALID_POSITION)
			selectedCallback.deselected(selectedItem, (T) getItem(selectedItem));
		
		selectedItem = ListView.INVALID_POSITION;
		
		notifyDataSetChanged();
	}
}
