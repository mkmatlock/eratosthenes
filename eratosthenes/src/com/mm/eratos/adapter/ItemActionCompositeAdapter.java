package com.mm.eratos.adapter;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;

public class ItemActionCompositeAdapter<T> extends CompositeAdapter {
	public static interface ItemActionCallback<T> {
		public void perform(int position, T item, View v);
		public void initialize(int position, T item, View v);
	}
	
	public static interface ItemLongClickCallback<T> {
		public boolean perform(int position, T item, View v);
	}
	
	private class ClickableListener implements OnClickListener, OnLongClickListener {
		int itemPosition;
		
		public ClickableListener(int itemPosition){
			this.itemPosition = itemPosition;
		}
		
		@SuppressWarnings("unchecked")
		public void onClick(View v) {
			actionCallback.perform(itemPosition, (T) getItem(itemPosition), v);
		}

		@SuppressWarnings("unchecked")
		public boolean onLongClick(View v) {
			return longCallback.perform(itemPosition, (T) getItem(itemPosition), v);
		}
	}

	private final int actionElement;
	private final ItemActionCallback<T> actionCallback;
	private final ItemLongClickCallback<T> longCallback;

	public ItemActionCompositeAdapter(int actionElement, ItemActionCallback<T> callback, Adapter compositeAdapter) {
		super(compositeAdapter);
		this.actionElement = actionElement;
		this.actionCallback = callback;
		this.longCallback = null;
	}
	
	public ItemActionCompositeAdapter(int actionElement, ItemActionCallback<T> callback, ItemLongClickCallback<T> longCallback, Adapter compositeAdapter) {
		super(compositeAdapter);
		this.actionElement = actionElement;
		this.actionCallback = callback;
		this.longCallback = longCallback;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public View getView(int pos, View view, ViewGroup parent) {
		view = getCompositingView(pos, view, parent);
		
    	View actionButton = view.findViewById(actionElement);
    	actionButton.setFocusable(false);
    	actionButton.setFocusableInTouchMode(false);
    	
		ClickableListener clickableListener = new ClickableListener(pos);
		actionButton.setOnClickListener(clickableListener);
		if(this.longCallback != null)
			actionButton.setOnLongClickListener(clickableListener);
		
		actionCallback.initialize(pos, (T) getItem(pos), actionButton);
        return view;
	}
}
