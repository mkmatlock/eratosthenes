package com.mm.eratos.adapter;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;
import android.widget.Adapter;

import com.mm.eratos.database.DBHelper;
import com.mm.eratos.fragment.EntryDetailFragment;

public class EntryDetailPagerAdapter extends FragmentStatePagerAdapter {
	private Adapter source;
	SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
	private DataSetObserver observer;
	
    public EntryDetailPagerAdapter(FragmentManager fm) {
        super(fm);
        observer = new DataSetObserver() {
			@Override
			public void onChanged() {
				super.onChanged();
				EntryDetailPagerAdapter.this.notifyDataSetChanged();
			}

			@Override
			public void onInvalidated() {
				super.onInvalidated();
			}
        };
    }
    
    public EntryDetailPagerAdapter(FragmentManager fm, Adapter source) {
        super(fm);
        observer = new DataSetObserver() {
			@Override
			public void onChanged() {
				super.onChanged();
				EntryDetailPagerAdapter.this.notifyDataSetChanged();
			}

			@Override
			public void onInvalidated() {
				super.onInvalidated();
			}
        };
        setSource(source);
    }

    @Override
    public Fragment getItem(int position) {
    	Bundle arguments = new Bundle();
    	Cursor c = (Cursor) source.getItem(position);
    	String refKey = c.getString( c.getColumnIndex(DBHelper.COLUMN_KEY) );
		arguments.putString(EntryDetailFragment.ARG_ITEM_ID, refKey);
		
        EntryDetailFragment fragment = new EntryDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }
    
    public void setSource(Adapter newSource) {
    	if(source != null)
    		source.unregisterDataSetObserver(observer);
    	
		source = newSource;
		
		if(source != null)
			source.registerDataSetObserver(observer);
    }
    
    @Override
    public int getCount() {
    	if(source != null)
    		return source.getCount();
    	return 0;
    }
    
    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}