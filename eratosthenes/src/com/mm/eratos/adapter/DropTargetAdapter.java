package com.mm.eratos.adapter;

import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.widget.Adapter;

public class DropTargetAdapter<T> extends CompositeAdapter {

	private DropTargetListener<T> listener;

	public interface DropTargetListener<T> {
		public boolean start(int pos, T item, View v, DragEvent event);
		public boolean enter(int pos, T item, View v, DragEvent event);
		public boolean exit(int pos, T item, View v, DragEvent event);
		public boolean ended(int pos, T item, View v, DragEvent event);
		public boolean drop(int pos, T item, View v, DragEvent event);
	}
	
	private class DragTargetListener implements OnDragListener {
		int itemPos;
		
		public DragTargetListener(int itemPos){
			this.itemPos = itemPos;
		}
		
		@Override
		@SuppressWarnings("unchecked")
		public boolean onDrag(View v, DragEvent event) {
			T item =  (T) getItem(itemPos);
			if(event.getAction() == DragEvent.ACTION_DRAG_STARTED)
				return listener.start(itemPos, item, v, event);
			else if(event.getAction() == DragEvent.ACTION_DRAG_ENTERED)
				return listener.enter(itemPos, item, v, event);
			else if(event.getAction() == DragEvent.ACTION_DRAG_EXITED)
				return listener.exit(itemPos, item, v, event);
			else if(event.getAction() == DragEvent.ACTION_DRAG_ENDED)
				return listener.ended(itemPos, item, v, event);
			else if(event.getAction() == DragEvent.ACTION_DROP)
				return listener.drop(itemPos, item, v, event);
			
			return true;
		}
	}
	
	public DropTargetAdapter(Adapter cAdapter, DropTargetListener<T> listener) {
		super(cAdapter);
		this.listener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = getCompositingView(position, convertView, parent);
		
		convertView.setOnDragListener(new DragTargetListener(position));
		
		return convertView;
	}

}
