package com.mm.eratos.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

public class CustomAdapter<T> extends CompositeAdapter {
	private ObjectMapper<T> mapper;

	public interface ObjectMapper<T> {
		public View map(int pos, T obj, View convertView, ViewGroup parent);
	}

	public CustomAdapter(ObjectMapper<T> mapper, Adapter cAdapter){
		super(cAdapter);
		this.mapper = mapper;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = getCompositingView(position, convertView, parent);
		convertView = mapper.map(position, (T) getItem(position), convertView, parent);
		return convertView;
	}
}
