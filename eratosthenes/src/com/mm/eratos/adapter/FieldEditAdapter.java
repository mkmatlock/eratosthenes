package com.mm.eratos.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

import com.mm.eratos.R;

public class FieldEditAdapter extends BaseAdapter {

	private final Context context;
    private final List<String> keyList;
	private final Map<String, String> values;
	private final Map<String, EditText> views;
	
    public FieldEditAdapter(Context context, List<String> keyList, Map<String, String> values){
        this.context = context;
        this.keyList = keyList;
        this.views = new HashMap<String, EditText>();
        this.values = values;
    }

    @Override
    public int getCount() {
        return keyList.size();
    }

    @Override
    public View getView(int pos, View view, ViewGroup parent) {
    	if(view == null)
    		view = LayoutInflater.from(context).inflate(R.layout.edit_field_list_item, null);
    	
		EditText fieldEdit = (EditText)view.findViewById(R.id.edit_field);
		
        String key = keyList.get(pos);
        fieldEdit.setHint(key);
        fieldEdit.setText(values.get(key));
        
        views.put(key, fieldEdit);
        return view;
    }

	@Override
	public Object getItem(int pos) {
		return keyList.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return keyList.get(position).hashCode();
	}
	
	public Map<String, String> updateValues(){
		Map<String, String> values = new HashMap<String, String>();
		for(String key : keyList){
			values.put(key, views.get(key).getText().toString());
		}
		return values;
	}
}