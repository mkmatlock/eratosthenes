package com.mm.eratos.adapter;

import java.util.ArrayList;
import java.util.List;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;

public class ConcatenateAdapter extends BaseAdapter {
	List<Adapter> adapterList;
	
	class AdapterLocation {
		int position;
		Adapter adapter;
	}
	
	public ConcatenateAdapter(Adapter ... adapters) {
		adapterList = new ArrayList<Adapter>(adapters.length);
		for(Adapter a : adapters){
			adapterList.add(a);
			a.registerDataSetObserver(new DataSetObserver() {
				public void onChanged() {
					notifyDataSetChanged();
				}
				public void onInvalidated() {
					notifyDataSetInvalidated();
				}
			});
		}
	}
	

	@Override
	public int getCount() {
		int total = 0;
		for(Adapter a : adapterList)
			total += a.getCount();
		return total;
	}

	private AdapterLocation getAdapter(int position){
		int cpos = 0;
		int cnt = 0;
		AdapterLocation result = new AdapterLocation();
		for(Adapter a : adapterList){
			cnt=a.getCount();
			if(cpos + cnt > position){
				result.adapter = a;
				result.position = position - cpos;
				break;
			}else{
				cpos += cnt;
			}
		}
		return result;
	}
	
	@Override
	public Object getItem(int position) {
		AdapterLocation result = getAdapter(position);
		return result.adapter.getItem(result.position);
	}

	@Override
	public long getItemId(int position) {
		AdapterLocation result = getAdapter(position);
		return result.adapter.getItemId(result.position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AdapterLocation result = getAdapter(position);
		return result.adapter.getView(result.position, convertView, parent);
	}
}
