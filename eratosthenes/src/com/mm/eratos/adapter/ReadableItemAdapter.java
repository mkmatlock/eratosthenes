package com.mm.eratos.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

public class ReadableItemAdapter<T> extends CompositeAdapter {

	private int readableElement;
	private SetupStateCallback<T> callbacks;

	public static interface SetupStateCallback<T> {
		public void setupState(int position, T item, com.mm.eratos.view.IReadable view);
	}
	
	public ReadableItemAdapter(int readableElement, SetupStateCallback<T> callbacks, Adapter compositeAdapter){
		super(compositeAdapter);
		this.readableElement = readableElement;
		this.callbacks = callbacks;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = getCompositingView(position, convertView, parent);
		final com.mm.eratos.view.IReadable readable = (com.mm.eratos.view.IReadable) convertView.findViewById(readableElement);
		callbacks.setupState(position, (T) getItem(position), readable);
		return convertView;
	}
}
