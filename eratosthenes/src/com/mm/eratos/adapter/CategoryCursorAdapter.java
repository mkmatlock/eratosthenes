package com.mm.eratos.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class CategoryCursorAdapter extends CompositeAdapter {
	Context mContext;
	private int categoryIconId;
	private int categoryCountId;
	private int categoryDrawable;
	private String countColumn;
	private String headerColumn;
	
	public CategoryCursorAdapter(Context context, int categoryIconId, int categoryCountId, int categoryDrawable, String headerColumn, String countColumn, Adapter cAdapter) {
		super(cAdapter);
		mContext = context;
		this.categoryIconId = categoryIconId;
		this.categoryCountId = categoryCountId;
		this.categoryDrawable = categoryDrawable;
		this.headerColumn = headerColumn;
		this.countColumn = countColumn;
	}
	
	@Override
	public Object getItem(int position) {
		Cursor c = (Cursor) super.getItem(position);
		return c.getString( c.getColumnIndex(headerColumn) );
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = getCompositingView(position, convertView, parent);
		
		Cursor c = (Cursor) super.getItem(position);
		((ImageButton)convertView.findViewById(categoryIconId)).setImageResource(categoryDrawable);
		
		if(countColumn != null){
			String count = Integer.toString(c.getInt(c.getColumnIndex(countColumn)));
			((TextView)convertView.findViewById(categoryCountId)).setText(count);
		}else{
			((TextView)convertView.findViewById(categoryCountId)).setText("-");
		}
		
		return convertView;
	}
}