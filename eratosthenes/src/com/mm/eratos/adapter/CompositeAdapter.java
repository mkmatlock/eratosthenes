package com.mm.eratos.adapter;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;

public abstract class CompositeAdapter extends BaseAdapter {
	private final Adapter compositeAdapter;
	
	public CompositeAdapter(Adapter cAdapter){
		super();
		compositeAdapter = cAdapter;

		compositeAdapter.registerDataSetObserver(new DataSetObserver() {
			public void onChanged() {
				notifyDataSetChanged();
			}
			public void onInvalidated() {
				notifyDataSetInvalidated();
			}
		});
	}
	
	@Override
	public int getCount() {
		return compositeAdapter.getCount();
	}

	@Override
	public Object getItem(int position) {
		return compositeAdapter.getItem(position);
	}

	@Override
	public long getItemId(int position) {
		return compositeAdapter.getItemId(position);
	}

	public Adapter getCompositeAdapter(){
		return compositeAdapter;
	}

	public View getCompositingView(int position, View convertView, ViewGroup parent) {
		return compositeAdapter.getView(position, convertView, parent);
	}
}
