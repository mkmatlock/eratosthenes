package com.mm.eratos.adapter;

import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;

public class LongClickItemAdapter<T> extends CompositeAdapter {
	private LongClickCallback<T> callback;

	public interface LongClickCallback<T> {
		public boolean longClicked(int position, T item, View v);
	}
	
	public LongClickItemAdapter(Adapter cAdapter, LongClickCallback<T> callback ) {
		super(cAdapter);
		this.callback = callback;
	}

	@Override
	@SuppressWarnings("unchecked")
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = getCompositingView(position, convertView, parent);
		
		final int itemPos = position;
		convertView.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				return callback.longClicked(itemPos, (T) getItem(itemPos), v);
			}
		});
		
		return convertView;
	}

}
