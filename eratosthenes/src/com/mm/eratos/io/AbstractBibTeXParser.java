package com.mm.eratos.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.UUID;

import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXParser;
import org.jbibtex.ParseException;

import com.mm.eratos.files.ByteCountingInputStream;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.EOLConvertingInputStreamReader;

public abstract class AbstractBibTeXParser implements IParser {
	private OutputStream errorLog;
	private File errorFile;
	
	private EOLConvertingInputStreamReader stream;
	private ByteCountingInputStream byteStream;
	private long length;
	
	private BibTeXParser bibtexParser;
	BibTeXObject object = null;

	int index = 0;
	
	protected void writeError(Exception e) throws IOException {
		errorLog.write((e.getMessage() + "\n").getBytes());
	}

	private void createParser(InputStream is, String encoding) throws IOException, ParseException {
		bibtexParser = new BibTeXParser();
		stream = new EOLConvertingInputStreamReader(new InputStreamReader(is, encoding));
		errorLog = initErrorLog();
		bibtexParser.initPullParser(stream, errorLog);
	}

	private OutputStream initErrorLog() throws IOException {
		FileManager fileManager = FileManager.getFileManager();
		EratosUri temporaryUri = fileManager.getTemporaryUri( UUID.randomUUID().toString() );
		errorFile = fileManager.getFile(temporaryUri);
		return new FileOutputStream(errorFile);
	}

	@Override
	public void init(InputStream is, String encoding, int length) throws IOException, ParseException {
		byteStream = new ByteCountingInputStream(is);
		createParser(byteStream, encoding);
		this.length = length;
	}
	
	@Override
	public boolean next() throws IOException, ParseException {
		object = bibtexParser.next();
		if(object != null){
			index++;
			return true;
		}
		return false;
	}
	
	@Override
	public long processed() {
		return byteStream.getPosition();
	}

	@Override
	public long total() {
		return length;
	}

	@Override
	public BibTeXObject get() throws IOException, ParseException {
		return object;
	}

	@Override
	public BibTeXObject raw() {
		return object;
	}
	
	@Override
	public String errors() throws IOException {
		String errString = new String(FileUtils.readBytes(errorFile));
		errorFile.delete();
		return errString;
	}

	@Override
	public void finish() throws IOException {
		errorLog.flush();
		errorLog.close();
		stream.close();
	}
}
