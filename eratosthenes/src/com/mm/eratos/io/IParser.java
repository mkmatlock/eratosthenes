package com.mm.eratos.io;

import java.io.IOException;
import java.io.InputStream;

import org.jbibtex.BibTeXObject;
import org.jbibtex.ParseException;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.task.ProgressCallback;

public interface IParser {
	public String name();
	public String type();
	
	public void init(InputStream is, String encoding, int length) throws IOException, ParseException;
	
	public long processed();
	public long total();
	
	public boolean next() throws IOException, ParseException;
	public BibTeXObject get() throws IOException, ParseException;
	public BibTeXObject raw();
	public void extras(BibTeXContentProviderAdapter bibtexAdapter, BibTeXEntryModelFactory modelFactory, ProgressCallback progressHandler) throws IOException;
	
	public String errors() throws IOException;
	public void finish() throws IOException;
}