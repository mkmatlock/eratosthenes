package com.mm.eratos.io.bibtex.jabref;

import java.util.Locale;

public class JabRefUnsupportedGroup extends JabRefGroup {
	String type;
	String filter;
	
	public JabRefUnsupportedGroup(int level, String name, int idNum, String type, String filter){
		super(level, name, idNum);
		this.type = type;
		this.filter = filter;
	}
	
	@Override
	public String type(){
		return type;
	}
	
	public String filter(){
		return filter;
	}

	@Override
	public String print() {
		return String.format(Locale.US, "%d %s:%s\\;%s\\;;", level, type, name, filter);
	}
}
