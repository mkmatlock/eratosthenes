package com.mm.eratos.io.bibtex.jabref;

import java.util.ArrayList;
import java.util.List;

public abstract class JabRefGroup {
	protected static final String ALL_ENTRIES_GROUP = "AllEntriesGroup";
	protected static final String EXPLICIT_GROUP = "ExplicitGroup";
	protected static final String KEYWORD_GROUP = "KeywordGroup";
	protected static final String SEARCH_GROUP = "SearchGroup";
	
	protected static final String ROOT_GROUP = "0 AllEntriesGroup:;";
	
	int idNum;
	int level;
	String name;
	List<JabRefGroup> subGroups;
	
	protected JabRefGroup(int level, String name, int idNum) {
		this.level = level;
		this.name = name;
		this.idNum = idNum;
		subGroups = new ArrayList<JabRefGroup>();
	}
	
	public abstract String print();
	public abstract String type();
	
	public int level(){
		return level;
	}
	
	public String name(){
		return name;
	}
	
	public void addGroup(JabRefGroup g){
		subGroups.add(g);
	}
	
	public List<JabRefGroup> getSubGroups() {
		return subGroups;
	}
	
	public List<JabRefGroup> getAllSubGroups() {
		List<JabRefGroup> allSubGroups = new ArrayList<JabRefGroup>();
		allSubGroups.addAll(subGroups);
		for(JabRefGroup sGroup : subGroups){
			allSubGroups.addAll(sGroup.getAllSubGroups());
		}
		return allSubGroups;
	}
}
