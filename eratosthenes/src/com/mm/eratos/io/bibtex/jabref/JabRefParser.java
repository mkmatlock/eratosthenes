package com.mm.eratos.io.bibtex.jabref;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jbibtex.BibTeXComment;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter.BulkInsertUpdateListener;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.io.AbstractBibTeXParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.ArrayUtils;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.WebUtils;

public class JabRefParser extends AbstractBibTeXParser {
	private static final int MAX_PROCESS_GROUP_SIZE = 100;
	public static final String JABREF_FILE_TYPE = WebUtils.BIBTEX_MIMETYPE + ":jabref";
	
	private static final String FILE_DESCRIPTION_FIELD_PREFIX = "file-description-";
	private static DateFormat timestampFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.US);
	
	public static final String jabrefFile = "file";
	public static final String jabrefReview = "review";
	
	public static final String keyDelimiter = "\\;";
	public static final String groupDelimiter = ":";
	public static final String groupSetDelimiter = ";";
	
	static CaseInsensitiveHashSet reservedFields = new CaseInsensitiveHashSet();
	static {
		reservedFields.add("id");
		reservedFields.add("groups");
	}

	JabRefGroupsTree groupsTree;
	
	public JabRefParser(){
		
	}

	@Override
	public String name() {
		return "JabRef BibTeX";
	}

	@Override
	public String type() {
		return JABREF_FILE_TYPE;
	}

	@Override
	public BibTeXObject get() throws IOException, ParseException {
		BibTeXObject object = super.get();
		
		if(object instanceof BibTeXComment){
			String strValue = ((BibTeXComment) object).getValue().toUserString();
			if(strValue.startsWith("jabref-meta: groupstree:")){
				groupsTree = new JabRefGroupsTree();
				groupsTree.parse(strValue);
			}
		} else if(object instanceof BibTeXEntry) {
			object = toImport((BibTeXEntry)object);
		}
		
		return object;
	}

	@Override
	public BibTeXObject raw() {
		return super.raw();
	}

	public BibTeXEntry toImport(BibTeXEntry entry) throws IOException {
		BibTeXEntry newEntry = new BibTeXEntry(entry.getType(), entry.getKey());
		
		Map<Key, Value> fields = entry.getFields();
		for(Key k : fields.keySet()){
			String keyVal = k.getValue().toLowerCase(Locale.US);
			String val = fields.get(k).toUserString();
			
			if(keyVal.equalsIgnoreCase(jabrefFile)){
				importFileField(newEntry, val);
			} else if(keyVal.equalsIgnoreCase(jabrefReview)) {
				newEntry.addField(BibTeXEntryModel.COMMENTS_KEY, new StringValue(val, Style.BRACED));
			} else if(keyVal.equalsIgnoreCase(BibTeXEntryModel.TIMESTAMP_KEY.getValue())) {
				try{
					Date timestamp = timestampFormat.parse(val);
					newEntry.addField(BibTeXEntryModel.CREATED_KEY, new StringValue(BibTeXEntryModel.dateFormat.format(timestamp), Style.BRACED));
				}catch(java.text.ParseException pe){
					
				}
			} else if(!reservedFields.contains(keyVal)) {
				newEntry.addField(k, new StringValue(val, Style.BRACED));
			}
		}
		
		return newEntry;
	}

	private void importFileField(BibTeXEntry newEntry, String val) throws IOException {
		val = StringEscapeUtils.unescapeHtml4(val);
		String files[] = val.split("(?<![\\\\]);", -1);
		
		int i = 0;
		for(String file : files){
			
			String []attributes = file.split("(?<![\\\\]):", -1);
			
			if(attributes.length != 3){
//				logger.warn("Incorrectly formatted file field: {%s} Caused by: '%s' length=%d", val, file, attributes.length);
				continue;
			}
			
			String fPath = attributes[1];
			fPath = fPath.replace("\\\\", "/");
			EratosUri fUri = EratosUri.parseUri( EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + unescapeString(fPath) );
			if(fUri != null){
				String desc = unescapeString(attributes[0]);
				i++;
				
				Key fKey = new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + Integer.toString(i));
				Key fDescKey = new Key(FILE_DESCRIPTION_FIELD_PREFIX + Integer.toString(i));
				newEntry.addField(fKey, new StringValue(fUri.toString(), Style.BRACED));
				newEntry.addField(fDescKey, new StringValue(desc, Style.BRACED));
			}else{
				writeError(new IOException("Unable to parse file field: " + attributes[1]));
			}
		}
	}

	private String unescapeString(String value) {
		return value.replace("\\:", ":").replace("\\;",";");
	}

	@Override
	public void extras(BibTeXContentProviderAdapter bibtexAdapter, BibTeXEntryModelFactory modelFactory, ProgressCallback handler) throws IOException {
		if(groupsTree != null) {
			try{
				importGroups(bibtexAdapter, groupsTree.getRoot().getSubGroups(), modelFactory, handler);
			}catch(ParseException pe){
				throw new IOException(pe);
			}catch(FieldRequiredException fre){
				throw new IOException(fre);
			}
		}
	}

	private void importGroups(BibTeXContentProviderAdapter bibtexAdapter, List<JabRefGroup> subGroups, BibTeXEntryModelFactory modelFactory, ProgressCallback handler) throws IOException, ParseException, FieldRequiredException {
		int i = 0;
		for(JabRefGroup group : subGroups){
			handler.update("Importing groups...", i, subGroups.size());
			
			if(group instanceof JabRefKeywordGroup) {
				importKeywordGroup(bibtexAdapter, group);
			}else if(group instanceof JabRefSearchGroup) {
				importSearchGroup(bibtexAdapter, group);
			}else if(group instanceof JabRefExplicitGroup) {
				JabRefExplicitGroup eGroup = (JabRefExplicitGroup)group;
				
				List<String> allKeys = new ArrayList<String>(eGroup.getAllKeys());
				
				for(int j = 0; j < allKeys.size(); j += MAX_PROCESS_GROUP_SIZE) {
					int start = j;
					int end = Math.min(j+MAX_PROCESS_GROUP_SIZE, allKeys.size());
					List<String> entryKeys = ArrayUtils.slice(allKeys, start, end);
					
					System.out.println("Adding group: " + eGroup.name() + " to entries: " + entryKeys);
					
					System.out.println("Fetch entries...");
					List<BibTeXEntryModel> entries = bibtexAdapter.getEntries(entryKeys, modelFactory);
					List<BibTeXEntryModel> modified = new ArrayList<BibTeXEntryModel>(entryKeys.size());
					
					for(BibTeXEntryModel entry : entries){
						System.out.println("Adding group: " + eGroup.name()  + "to entry " + entry.getRefKey());
						entry.addGroup(eGroup.name());
						BibTeXEntryModel nEntry = modelFactory.commit(entry);
						modified.add(nEntry);
					}
					
					System.out.println("Delete entries...");
					bibtexAdapter.deleteAll(entryKeys);
					System.out.println("Bulk insert entries...");
					bibtexAdapter.bulkInsert(modified, new BulkInsertUpdateListener() {
						public void update(int progress, int max) {
						}
					});
				}
			}
			i++;
		}
	}

	private void importKeywordGroup(BibTeXContentProviderAdapter bibtexAdapter, JabRefGroup group) throws IOException {
		JabRefKeywordGroup kGroup = (JabRefKeywordGroup) group;
		QueryElement query = kGroup.query();
		if(query != null)
			bibtexAdapter.createFilter(kGroup.name(), query);
	}
	
	private void importSearchGroup(BibTeXContentProviderAdapter bibtexAdapter, JabRefGroup group) throws IOException {
		JabRefSearchGroup kGroup = (JabRefSearchGroup) group;
		QueryElement query = kGroup.query();
		if(query != null)
			bibtexAdapter.createFilter(kGroup.name(), query);
	}
}
