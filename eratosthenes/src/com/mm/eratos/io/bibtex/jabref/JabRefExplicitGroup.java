package com.mm.eratos.io.bibtex.jabref;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class JabRefExplicitGroup extends JabRefGroup {

	List<String> keyList;
	
	public JabRefExplicitGroup(int level, String name, int idNum) {
		super(level, name, idNum);
		keyList = new ArrayList<String>();
	}

	public void addKey(String key){
		keyList.add(key);
	}

	public void removeKey(String refKey) {
		if(keyList.contains(refKey)){
			keyList.remove(refKey);
		}
		
		for(JabRefGroup subGroup : getAllSubGroups()){
			if(subGroup instanceof JabRefExplicitGroup && ((JabRefExplicitGroup)subGroup).keyList().contains(refKey))
				((JabRefExplicitGroup)subGroup).removeKey(refKey);
		}
	}
	
	public List<String> keyList(){
		return new ArrayList<String>(keyList);
	}

	public Set<String> getAllKeys(){
		Set<String> allKeys = new HashSet<String>(keyList);
		
		for(JabRefGroup child : getAllSubGroups()){
			if(child instanceof JabRefExplicitGroup){
				JabRefExplicitGroup eGroup = (JabRefExplicitGroup)child;
				for(String k : eGroup.keyList())
					allKeys.add(k);
			}
		}
		
		return allKeys;
	}

	@Override
	public String type() {
		return JabRefGroup.EXPLICIT_GROUP;
	}
	
	@Override
	public String print() {
		String group = String.format(Locale.US, "%d %s:%s\\;%d\\;", level, JabRefGroup.EXPLICIT_GROUP, name, idNum);
		for(String k : keyList)
			group += k + "\\;";
		group += ";";
		return group;
	}
}
