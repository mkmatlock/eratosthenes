package com.mm.eratos.io.bibtex.jabref;

import java.io.IOException;
import java.util.Locale;

import com.mm.eratos.database.DBHelper;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementLike;

public class JabRefKeywordGroup extends JabRefGroup {

	private String filter;
	private QueryElement query;

	public JabRefKeywordGroup(int level, String name, int idNum, String filter) {
		super(level, name, idNum);
		this.filter = filter;
	}
	
	@Override
	public String print() {
		return String.format(Locale.US, "%d %s:%s\\;%s\\;;", level, JabRefGroup.KEYWORD_GROUP, name, filter);
	}

	@Override
	public String type() {
		return JabRefGroup.KEYWORD_GROUP;
	}
	
	public QueryElement query() {
		return query;
	}
	
	public String getFilter() {
		return filter;
	}
	
	public void parse() throws IOException {
		String []items = filter.split("\\\\;");
		
		String fieldName = items[1];
		String searchTerm = items[2];
		boolean regex = Integer.parseInt(items[4]) == 1;
		
		query = null;
		String columnName = getColumnName(fieldName);
		if(columnName != null){
			if(regex){
//				query = new QueryElementRegex(columnName, searchTerm);
			}else{
				query = new QueryElementLike(columnName, searchTerm);
			}
		}
	}

	private String getColumnName(String fieldName) {
		if(fieldName.equalsIgnoreCase("keywords"))
			return DBHelper.COLUMN_KEYWORDS;
		if(fieldName.equalsIgnoreCase("author"))
			return DBHelper.COLUMN_AUTHOR;
		if(fieldName.equalsIgnoreCase("title"))
			return DBHelper.COLUMN_TITLE;
		if(fieldName.equalsIgnoreCase("year"))
			return DBHelper.COLUMN_YEAR;
		return null;
	}
}
