package com.mm.eratos.io.bibtex.jabref;

import java.io.IOException;
import java.util.Locale;

import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementFieldLike;
import com.mm.eratos.search.db.QueryElementSearchLike;

public class JabRefSearchGroup extends JabRefGroup {

	private String filter;
	private QueryElement query;

	public JabRefSearchGroup(int level, String name, int idNum, String filter) {
		super(level, name, idNum);
		this.filter = filter;
	}
	
	@Override
	public String print() {
		return String.format(Locale.US, "%d %s:%s\\;%s\\;;", level, JabRefGroup.SEARCH_GROUP, name, filter);
	}

	@Override
	public String type() {
		return JabRefGroup.SEARCH_GROUP;
	}
	
	public QueryElement query() {
		return query;
	}

	public String getFilter() {
		return filter;
	}
	
	public void parse() throws IOException {
		String []items = filter.split("\\\\;");
		
		String searchTerm = items[1];
		boolean freeText = Integer.parseInt(items[0]) == 0;
//		boolean regex = Integer.parseInt(items[3]) == 1;
		query = null;
		
		if(freeText) {
			query = new QueryElementSearchLike(searchTerm);
		} else {
			if(searchTerm.contains("=")) {
				String[] kv = searchTerm.split("=");
				query = new QueryElementFieldLike(kv[0].trim(), kv[1].trim());
			}
		}
	}
}
