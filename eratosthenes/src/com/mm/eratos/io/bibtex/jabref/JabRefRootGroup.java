package com.mm.eratos.io.bibtex.jabref;

public class JabRefRootGroup extends JabRefGroup {

	public JabRefRootGroup(int level, String name, int idNum) {
		super(level, name, idNum);
	}
	
	@Override
	public String print() {
		return JabRefGroup.ROOT_GROUP;
	}

	@Override
	public String type() {
		return "AllEntriesGroup";
	}

}
