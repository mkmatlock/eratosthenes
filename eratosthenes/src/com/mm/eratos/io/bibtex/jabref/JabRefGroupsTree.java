package com.mm.eratos.io.bibtex.jabref;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;

public class JabRefGroupsTree {
	protected static final int GROUP_TREE_FORMAT_WIDTH = 70;
	static final String ident_regex="[a-zA-Z_][a-zA-Z0-9\\-_]*";
	static final String colon_regex=":";
	static final String semicolon_regex=";";
	static final String slashcolon_regex="\\\\;";
	static final String numeric_regex="[0-9]+";
	
	static final int EOF_TOKEN = 0;
	static final int IDENTIFIER_TOKEN = 1;
	static final int COLON_TOKEN = 2;
	static final int SEMICOLON_TOKEN = 3;
	static final int SLASHCOLON_TOKEN = 4;
	static final int NUMERIC_TOKEN = 5;
	
	static final Map<Integer, String> tokenMap = new HashMap<Integer, String>();
	static{
		tokenMap.put(EOF_TOKEN, "\\0");
		tokenMap.put(IDENTIFIER_TOKEN, ident_regex);
		tokenMap.put(COLON_TOKEN, ":");
		tokenMap.put(SEMICOLON_TOKEN, ";");
		tokenMap.put(SLASHCOLON_TOKEN, "\\;");
		tokenMap.put(NUMERIC_TOKEN, "[0-9]+");
	}
	
	static final Map<String, Integer> regexPatterns = new HashMap<String, Integer>();
	private static final Object PREAMBLE = "jabref-meta: groupstree:";
	
	static{
		regexPatterns.put(ident_regex, IDENTIFIER_TOKEN);
		regexPatterns.put(colon_regex, COLON_TOKEN);
		regexPatterns.put(semicolon_regex, SEMICOLON_TOKEN);
		regexPatterns.put(slashcolon_regex, SLASHCOLON_TOKEN);
		regexPatterns.put(numeric_regex, NUMERIC_TOKEN);
	}
	
	String token;
	int type;
	private String input;
	private int strindex;
	private int line;
	
	Stack<JabRefGroup> groupStack;
	JabRefGroup rootNode;
	private EratosLogger logger;
	
	public JabRefGroupsTree(){
		logger = EratosApplication.getApplication().getLogManager().getLogger("JabRef Groups");
	}
	
	private String getString(String termToken, boolean eofTerminate) throws IOException {
		String result = "";
		while(!result.matches(termToken)) {
			try{
				char next = nextChar();
				if(next=='\n')
					line++;
				else
					result+=next;
			} catch(EOFException e){
				if(!eofTerminate) throw e;
				else break;
			}
		}
		return result;
	}
	
	private String nextToken() throws IOException {
		try{
			consumeWhitespace();
		}catch(IOException e){
			token = "";
			type = EOF_TOKEN;
			return token;
		}
		
		Set<String> valid_tokens = new HashSet<String>();
		valid_tokens.addAll(regexPatterns.keySet());
		type = -1;
		token = "";
		while(valid_tokens.size() > 0){
			try {
				char next = nextChar();
				if(next == '\n'){
					line++;
					continue;
				}
				
				token += next;
				Set<String> defunct = new HashSet<String>();
				for(String regex : valid_tokens){
					if(!(token.matches(regex) || regex.startsWith(token))){
						defunct.add(regex);
					}
				}
				valid_tokens.removeAll(defunct);
			} catch (IOException e) {
				if(token.isEmpty()){
					type = EOF_TOKEN;
					token = "";
					return token;
				} else if(valid_tokens.size() == 1){
					String regex = valid_tokens.iterator().next();
					type = regexPatterns.get(regex);
					return token;
				} else
					throw new IOException("EOF encountered while parsing token: " + token);
			}
		}
		
		backup();
		token = token.substring(0, token.length()-1);
		
		int matches = 0;
		for(String regex : regexPatterns.keySet()){
			if(token.matches(regex) || regex.startsWith(token)){
				matches++;
				type = regexPatterns.get(regex);
			}
		}
		if(matches != 1)
			throw new IOException("Invalid token: '" + token + "'");
		
		return token;
	}
	
	private void consumeWhitespace() throws IOException {
		char c = 0;
		while((""+(c=nextChar())).matches("\\s")){
			if(c=='\n') line++;
		}
		backup();
	}
	
	private char nextChar() throws IOException {
		if(strindex == input.length())
			throw new EOFException();
		char rval = input.charAt(strindex);
		strindex++;
		return rval;
	}
	
	private void backup() {
		while(input.charAt(--strindex) == '\n');
	}
	
	private void backupToken() {
		int i = 0;
		while( i < token.length() ){
			strindex--;
			if(input.charAt(strindex) != '\n')
				i++;
		}
	}

	public void parse(String strValue) throws IOException{
		input = strValue.trim();
		line = 1;
		
		groupStack = new Stack<JabRefGroup>();
		
		strindex = 0;
		assertFound( "jabref-meta" );
		assertFound( ":" );
		assertFound( "groupstree" );
		assertFound( ":" );
		
		assertFound( "0" );
		assertFound( "AllEntriesGroup" );
		assertType( COLON_TOKEN );
		assertType( SEMICOLON_TOKEN );
		
		rootNode = new JabRefRootGroup(0, "", 0);
		groupStack.push(rootNode);
		
		try{
			parseGroupList();
		}catch(IOException e){
			throw new IOException(String.format(Locale.US, "Encountered error when parsing JabRef groups entry on input line: %d", line), e);
		}
	}

	private void parseGroupList() throws IOException {
		while(true) {
			nextToken();
			
			switch(type){
			case EOF_TOKEN:
				return;
			case NUMERIC_TOKEN:
				int level = getNumber();
				assertType( IDENTIFIER_TOKEN );
				String type = token;
				assertType( COLON_TOKEN );
				String name = getString( "^.*\\\\;$", false);
				name = name.substring(0, name.length()-2);
				
				while(level <= groupStack.peek().level())
					groupStack.pop();
				
				if( type.equals("ExplicitGroup") ){
					parseExplicitGroup(level, name);
				} else if( type.equals("KeywordGroup") ){
					parseKeywordGroup(level, name);
				} else if( type.equals("SearchGroup") ){
					parseSearchGroup(level, name);
				} else{
					parseOtherGroup(level, type, name);
				}
				break;
			default:
				throw new IOException("Unexpected token: " + token);
			}
		}
	}

	private void parseOtherGroup(int level, String type, String name) throws IOException {
		String filterStr = getString("^.*[^\\\\];$", true);
		if(filterStr.endsWith(";") && !filterStr.endsWith("\\;"))
			filterStr = filterStr.substring(0, filterStr.length()-1);
		
		logger.info("Ignoring %d %s:%s\\;%s", level, type, name, filterStr);
		
		JabRefUnsupportedGroup g = new JabRefUnsupportedGroup(level, name, 0, type, filterStr);
		groupStack.peek().addGroup(g);
		groupStack.push(g);
	}
	
	private void parseSearchGroup(int level, String name) throws IOException {
		String filterStr = getString("^.*[^\\\\];$", true);
		if(filterStr.endsWith(";") && !filterStr.endsWith("\\;"))
			filterStr = filterStr.substring(0, filterStr.length()-1);
		
		JabRefSearchGroup g = new JabRefSearchGroup(level, name, 0, filterStr);
		g.parse();
		groupStack.peek().addGroup(g);
		groupStack.push(g);
	}

	private void parseKeywordGroup(int level, String name) throws IOException {
		String filterStr = getString("^.*[^\\\\];$", true);
		if(filterStr.endsWith(";") && !filterStr.endsWith("\\;"))
			filterStr = filterStr.substring(0, filterStr.length()-1);
		
		JabRefKeywordGroup g = new JabRefKeywordGroup(level, name, 0, filterStr);
		g.parse();
		groupStack.peek().addGroup(g);
		groupStack.push(g);
	}
	
	private void parseExplicitGroup(int level, String name) throws IOException {
		assertType( NUMERIC_TOKEN );
		int idNum = getNumber();
		
		JabRefExplicitGroup g = new JabRefExplicitGroup(level, name, idNum);
		groupStack.peek().addGroup(g);
		groupStack.push(g);
		
		parseKeyList(name);
	}

	private void parseKeyList(String name) throws IOException {
		nextToken();
		if(type == SEMICOLON_TOKEN)
			return;
		backupToken();
		
		String filterStr = getString("^.*[^\\\\];$", true);
		if(filterStr.endsWith(";") && !filterStr.endsWith("\\;"))
			filterStr = filterStr.substring(0, filterStr.length()-1);
		
		for(String kw : filterStr.split("\\\\;")){
			kw = kw.trim();
			if(!kw.isEmpty())
				((JabRefExplicitGroup)groupStack.peek()).addKey(kw);
		}
	}

	private int getNumber() throws IOException {
		if(type != NUMERIC_TOKEN)
			throw new IOException("Unexpected token: " + token + " expected type: NUMERIC_TOKEN");
		return Integer.parseInt(token);
	}

	private void assertType(int expType) throws IOException {
		nextToken();
		
		if(type != expType)
			throw new IOException("Unexpected token: " + token + " expected: " + tokenMap.get(expType));
	}

	private void assertFound(String value) throws IOException {
		nextToken();
		
		if(!token.equals(value))
			throw new IOException("Unexpected token: " + token + " expected: " + value);
	}

	public String format() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(PREAMBLE);
		sb.append("\n");
		
		printNodes(sb, rootNode);
		return sb.toString();
	}
	
	private void printNodes(StringBuilder sb, JabRefGroup node) {
		for(String line : formatWidth(node.print(), GROUP_TREE_FORMAT_WIDTH)){
			sb.append(line);
			sb.append("\n");
		}
		
		for(JabRefGroup child : node.getSubGroups())
			printNodes(sb, child);
	}
	
	protected List<String> formatWidth(String group, int width) {
		List<String> lines = new ArrayList<String>();
		int i = 0;
		
		while(group.length() - i > width){
			lines.add(group.substring(i, i+width));
			i += width;
		}
		
		if(i != group.length())
			lines.add(group.substring(i, group.length()));
		
		return lines;
	}

	public void build() {
		rootNode = new JabRefRootGroup(0, "", 0);
	}
	
	public List<JabRefGroup> getGroups(int level, String type) {
		List<JabRefGroup> gList = new ArrayList<JabRefGroup>();
		for(JabRefGroup g : rootNode.getAllSubGroups()){
			if(g.level() == level && g.type().equalsIgnoreCase(type)){
				gList.add(g);
			}
		}
		return gList;
	}
	
	public JabRefGroup getGroup(int level, String type, String name) {
		for(JabRefGroup g : rootNode.getAllSubGroups()){
			if(g.level() == level && 
					g.type().equalsIgnoreCase(type) && 
					g.name().equalsIgnoreCase(name)){
				return g;
			}
		}
		return null;
	}

	public JabRefGroup getRoot() {
		return rootNode;
	}
}
