package com.mm.eratos.io.bibtex.jabref;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jbibtex.BibTeXComment;
import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXFormatter;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.io.IWriter;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.StringUtils;

public class JabRefWriter implements IWriter {
	private static final String FILE_DESCRIPTION_FIELD_PREFIX = "file-description-";
	private static DateFormat timestampFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.US);
	
	public static final String jabrefFile = "file";
	public static final String jabrefReview = "review";
	
	public static final String keyDelimiter = "\\;";
	public static final String groupDelimiter = ":";
	public static final String groupSetDelimiter = ";";
	public static final String TYPE_NAME = "jabref";
	
	static CaseInsensitiveHashSet reservedFields = new CaseInsensitiveHashSet();
	static {
		reservedFields.add("id");
		reservedFields.add("groups");
	}

	private OutputStreamWriter os;
	private String jabRefGroups;
	
	private BibTeXFormatter formatter;
	private Map<String, List<String>> groupMap;
	private Map<String, List<String>> entryMap;
	
	
	public JabRefWriter() {
		formatter = new BibTeXFormatter();
		groupMap = new HashMap<String, List<String>>();
		entryMap = new CaseInsensitiveMap<List<String>>();
	}

	@Override
	public String name() {
		return "JabRef BibTeX";
	}

	@Override
	public String type() {
		return JabRefParser.JABREF_FILE_TYPE;
	}

	@Override
	public void init(OutputStream os, String encoding) throws IOException {
		this.os = new OutputStreamWriter(os, encoding);
	}

	@Override
	public void write(BibTeXObject object) throws IOException {
		if(object instanceof BibTeXComment){
			String cV = ((BibTeXComment)object).getValue().toUserString();
			
			if(cV.startsWith("jabref-meta: groupstree:")) {
				jabRefGroups = cV;
				return;
			} else if(cV.startsWith("jabref-meta: groupsversion:")) {
				return;
			} else {
				os.write( format( object ) );
			}
		} else if(object instanceof BibTeXEntry) {
			BibTeXEntry entry = (BibTeXEntry) object;
			String refKey = entry.getKey().getValue();
			
			for(String g : getGroups(entry)) {
				if(!groupMap.containsKey(g))
					groupMap.put(g, new ArrayList<String>());
				if(!entryMap.containsKey(refKey))
					entryMap.put(refKey, new ArrayList<String>());
				groupMap.get(g).add(refKey);
				entryMap.get(refKey).add(g);
			}
			
			entry = toExport( entry );
			os.write( format( entry ) );
		} else {
			os.write( format( object ) );
		}
	}
	
	private BibTeXEntry toExport(BibTeXEntry entry) throws IOException {
		BibTeXEntry newEntry = new BibTeXEntry(entry.getType(), entry.getKey());
		
		List<String> relFiles = new ArrayList<String>();
		EratosUri libLocalUri = EratosUri.parseUri( EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" );
		
		Map<Key, Value> fields = entry.getFields();
		for(Key k : fields.keySet()){
			String keyVal = k.getValue().toLowerCase(Locale.US);
			
			
			if(keyVal.equalsIgnoreCase(BibTeXEntryModel.COMMENTS_KEY.getValue())){
				newEntry.addField(new Key(jabrefReview), new StringValue(fields.get(k).toUserString(), Style.BRACED));
				
			}else if(keyVal.equalsIgnoreCase(BibTeXEntryModel.CREATED_KEY.getValue())){
				try{
					Date timestamp = BibTeXEntryModel.dateFormat.parse(fields.get(k).toUserString());
					newEntry.addField(BibTeXEntryModel.TIMESTAMP_KEY, new StringValue(timestampFormat.format(timestamp), Style.BRACED));
				}catch(ParseException pe){
					
				}
			}else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, BibTeXEntryModel.eratosthenesLinkFieldPrefix)){
				
				if(fields.containsKey(BibTeXEntryModel.URL_KEY))
					newEntry.addField(k, fields.get(k));
				else if(keyVal.substring(BibTeXEntryModel.eratosthenesLinkFieldPrefix.length()).equals("1"))
					newEntry.addField(BibTeXEntryModel.URL_KEY, fields.get(k));
				else
					newEntry.addField(k, fields.get(k));
				
			} else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, BibTeXEntryModel.eratosthenesFileFieldPrefix)){
				exportFileField(relFiles, libLocalUri, fields, k, keyVal);
			} else if(!reservedFields.contains(keyVal) && !BibTeXEntryVerifier.fieldStartsWith(keyVal, FILE_DESCRIPTION_FIELD_PREFIX)) {
				newEntry.addField(k, fields.get(k));
			}
		}
		if(relFiles.size() > 0)
			newEntry.addField(new Key(jabrefFile), new StringValue(StringUtils.join(";", relFiles), Style.BRACED));
		
		return newEntry;
	}
	
	private List<String> getGroups(BibTeXEntry entry) {
		Value field = entry.getField(BibTeXEntryModel.GROUPS_KEY);
		if(field != null){
			String groupsValue = field.toUserString().trim();
			if(!groupsValue.isEmpty()){
				String [] groups = groupsValue.split(BibTeXEntryModel.DELIMITER);
				return Arrays.asList(groups);
			}
		}
		return Arrays.asList();
	}
	
	private void exportFileField(List<String> relFiles, EratosUri libLocalUri,
			Map<Key, Value> fields, Key k, String keyVal) {
		String fDesc = "";
		Key fDescKey = new Key(FILE_DESCRIPTION_FIELD_PREFIX + keyVal.substring(BibTeXEntryModel.eratosthenesFileFieldPrefix.length()));
		if(fields.containsKey(fDescKey)){
			fDesc = escapeString(fields.get(fDescKey).toUserString());
		}
		
		EratosUri fileUri = EratosUri.parseUri( fields.get(k).toUserString() );
		if(fileUri.childOf(libLocalUri)){
			String relPath = escapeString(fileUri.relativeTo(libLocalUri));
			String ext = fileUri.extension().toUpperCase(Locale.US);
			relFiles.add(String.format(Locale.US, "%s:%s:%s", fDesc, relPath, ext));
		}
	}

	private String escapeString(String value) {
		return value.replace(":", "\\:").replace(";", "\\;");
	}
	
	@Override
	public void writeExtras() throws IOException {
		os.write( format( createGroupsVersionComment() ) );
		os.write( format( createGroupsComment( jabRefGroups ) ) );
	}
	
	@Override
	public void finish() throws IOException {
		os.close();
	}
	
	public String format(BibTeXObject obj) throws IOException {
	    BibTeXDatabase btdb = new BibTeXDatabase();
	    btdb.addObject(obj);
	    
	    StringWriter sw = new StringWriter();
		formatter.format(btdb, sw);
	    
	    String formattedEntry = sw.getBuffer().toString();
		return formattedEntry.replaceAll("\n\\s+", "\n") + "\n\n";
	}
	
	private BibTeXObject createGroupsVersionComment() {
		return new BibTeXComment(new StringValue("jabref-meta: groupsversion:3;", Style.BRACED));
	}

	private BibTeXObject createGroupsComment(String importedGroups) throws IOException {
		JabRefGroupsTree groupTree = new JabRefGroupsTree();
		if(importedGroups == null)
			groupTree.build();
		else
			groupTree.parse(importedGroups);
		
		for(String refKey : entryMap.keySet()){
			List<String> groups = entryMap.get(refKey);
			
			for(JabRefGroup grp : groupTree.getGroups(1, "ExplicitGroup")){
				JabRefExplicitGroup eGroup = (JabRefExplicitGroup)grp;
				if(eGroup.getAllKeys().contains(refKey) && !groups.contains(eGroup.name()))
					eGroup.removeKey(refKey);
			}
		}
		
		for(String groupName : groupMap.keySet()){
			for(String refKey : groupMap.get(groupName))
				parseGroups(refKey, groupName, groupTree);
		}
		
		String groupComment = groupTree.format();
		return new BibTeXComment(new StringValue(groupComment, Style.BRACED));
	}

	private void parseGroups(String refKey, String groupName, JabRefGroupsTree groupTree) {
		JabRefGroup group = groupTree.getGroup(1, "ExplicitGroup", groupName);
		if(group == null) {
			group = new JabRefExplicitGroup(1, groupName, 0);
			groupTree.getRoot().addGroup(group);
		}
		
		JabRefExplicitGroup eGroup = (JabRefExplicitGroup)group;
		if(!eGroup.getAllKeys().contains(refKey))
			eGroup.addKey(refKey);
	}
}
