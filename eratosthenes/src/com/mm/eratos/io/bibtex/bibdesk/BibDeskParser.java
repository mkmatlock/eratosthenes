package com.mm.eratos.io.bibtex.bibdesk;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.BibTeXComment;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import com.dd.plist.Base64;
import com.dd.plist.BinaryPropertyListParser;
import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;
import com.dd.plist.XMLPropertyListParser;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter.BulkInsertUpdateListener;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.io.AbstractBibTeXParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.search.db.QueryElement;
import com.mm.eratos.search.db.QueryElementAnd;
import com.mm.eratos.search.db.QueryElementEndsWith;
import com.mm.eratos.search.db.QueryElementLike;
import com.mm.eratos.search.db.QueryElementNot;
import com.mm.eratos.search.db.QueryElementOr;
import com.mm.eratos.search.db.QueryElementStartsWith;
import com.mm.eratos.search.db.QueryElementText;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.ArrayUtils;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebUtils;

public class BibDeskParser extends AbstractBibTeXParser {
	private static final int MAX_PROCESS_GROUP_SIZE = 100;
	public static final String BIBDESK_FILE_TYPE = WebUtils.BIBTEX_MIMETYPE + ":bibdesk";
	
	static CaseInsensitiveHashSet reservedFields = new CaseInsensitiveHashSet();
	static {
		reservedFields.add("id");
		reservedFields.add("groups");
	}
	
	public static final String keyDelimiter = ",";
	public static final String bibdeskFileFieldPrefix = "bdsk-file-";
	public static final String bibdeskUrlFieldPrefix = "bdsk-url-";
	public static final String bibdeskExportFileFieldPrefix = "local-url";
	
	private Map<String, QueryElement> smartGroups;
	private Map<String, List<String>> staticGroups;
	
	public BibDeskParser(){
	}

	@Override
	public String name() {
		return "BibDesk BibTeX";
	}

	@Override
	public String type() {
		return BIBDESK_FILE_TYPE;
	}
	
	@Override
	public BibTeXObject get() throws IOException, ParseException {
		BibTeXObject object = super.get();
		
		if(object instanceof BibTeXComment){
			BibTeXComment comment = (BibTeXComment) object;
			String commentString = comment.getValue().toUserString().replace("\n","");

			Pattern pattern = Pattern.compile("BibDesk Static Groups\\{(.*)\\}");
			Matcher matcher = pattern.matcher(commentString);
			
			if(matcher.matches()){
				String plistXml = matcher.group(1);
				parseStaticGroups(plistXml);
			}
			
			pattern = Pattern.compile("BibDesk Smart Groups\\{(.*)\\}");
			matcher = pattern.matcher(commentString);
			
			if(matcher.matches()){
				String plistXml = matcher.group(1);
				parseSmartGroups(plistXml);
			}
		} else if(object instanceof BibTeXEntry) {
			BibTeXEntry entry = (BibTeXEntry)object;
			object = toImport(entry);
		}
		
		return object;
	}

	public BibTeXEntry toImport(BibTeXEntry entry) throws IOException {
		BibTeXEntry newEntry = new BibTeXEntry(entry.getType(), entry.getKey());

		CaseInsensitiveMap<String> localFieldFiles = new CaseInsensitiveMap<String>();
		CaseInsensitiveMap<String> bibdeskFieldFiles = new CaseInsensitiveMap<String>();
		
		Map<Key, Value> fields = entry.getFields();
		for(Key k : fields.keySet()){
			String keyVal = k.getValue().toLowerCase(Locale.US);
			
			if(BibTeXEntryVerifier.fieldStartsWith(keyVal, bibdeskFileFieldPrefix)){
				String filePath = getFileUri(fields, k);
				Value newVal = new StringValue(filePath, Style.BRACED);
				
				String num = keyVal.substring(bibdeskFileFieldPrefix.length());
				Key newKey = new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + num);
				newEntry.addField(newKey, newVal);
				newEntry.addField(k, fields.get(k));

				bibdeskFieldFiles.put(keyVal, filePath);
				
			} else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, bibdeskExportFileFieldPrefix)){
				String path = fields.get(k).toUserString();
				localFieldFiles.put(keyVal, path);
				
				newEntry.addField(k, fields.get(k));
			} else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, bibdeskUrlFieldPrefix)){
				String num = keyVal.substring(bibdeskUrlFieldPrefix.length());
				Key newKey = new Key(BibTeXEntryModel.eratosthenesLinkFieldPrefix + num);
				newEntry.addField(newKey, fields.get(k));
				
			} else if(BibTeXEntryVerifier.isBooleanField(keyVal)){
				String value = entry.getField(k).toUserString();
				if(value.equalsIgnoreCase("1")) value = "true";
				if(value.equalsIgnoreCase("0")) value = "false";
				
				newEntry.addField(k, new StringValue(value, Style.BRACED));
			} else if(! reservedFields.contains(keyVal)){
				newEntry.addField(k, fields.get(k));
			}
		}
		
//		String refKey = entry.getKey().getValue();
//		if(entryGroups.containsKey(refKey)){
//			List<String> groups = entryGroups.get(refKey);
//			String groupsValue = StringUtils.join(BibTeXEntryModel.DELIMITER, groups);
//			newEntry.addField(BibTeXEntryModel.GROUPS_KEY, new StringValue(groupsValue, Style.BRACED));
//		}
		
		importLocalFiles(newEntry, localFieldFiles, bibdeskFieldFiles);
		
		return newEntry;
	}
	
	private String parsePropertyList(String b64) throws IOException {
		try {
			InputStream is = new Base64.B64InputStream(new ByteArrayInputStream(b64.getBytes()));
			NSObject obj = BinaryPropertyListParser.parse(is);				
			NSArray arr = (NSArray) ((NSDictionary)obj).get("$objects");
			NSString nsRelPath = (NSString) arr.getArray()[4];
			return nsRelPath.getContent();
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	private String getFileUri(Map<Key, Value> fields, Key k) throws IOException {
		String b64 = fields.get(k).toUserString();
		String relPath = parsePropertyList(b64);
		return mapRelPath(relPath).toString();
	}

	private EratosUri mapRelPath(String relPath) {
		EratosUri fileUri = EratosUri.parseUri(EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + relPath);
		
		List<String> elements = fileUri.elements();
		if(elements.contains("Dropbox")){
			int pos = elements.indexOf("Dropbox");
			String path = StringUtils.join("/", fileUri.slice(pos+1));
			return EratosUri.parseUri(EratosUri.DROPBOX_PROTOCOL + ":///" + path);
		}
		return fileUri;
	}

	private void importLocalFiles(BibTeXEntry newEntry,
			CaseInsensitiveMap<String> localFieldFiles,
			CaseInsensitiveMap<String> bibdeskFieldFiles) throws IOException {
		int i = bibdeskFieldFiles.size() + 1;
		for(String k : localFieldFiles.keySet()) {
			String filePath = localFieldFiles.get(k);
			EratosUri fileUri = EratosUri.parseUri(EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + filePath);
			
			if( !hasFile(fileUri, bibdeskFieldFiles) ) {
				Value newVal = new StringValue(EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + filePath, Style.BRACED);
				Key newKey = new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + i);
				newEntry.addField(newKey, newVal);
				i+=1;
			}
		}
	}
	
	private boolean hasFile(EratosUri fileUri, CaseInsensitiveMap<String> fieldFiles) throws IOException {
		String canonicalPath = FileManager.getFileManager().getFile(fileUri).getCanonicalPath();
		
		for(String k : fieldFiles.keySet()){
			EratosUri bFileUri = EratosUri.parseUri(fieldFiles.get(k));
			File bFile = FileManager.getFileManager().getFile(bFileUri);
			
			if( canonicalPath.equals( bFile.getCanonicalPath() ) )
				return true;
		}
		return false;
	}

	private void parseStaticGroups(String plistXml) throws IOException {
		staticGroups = new HashMap<String, List<String>>();
		
		InputStream is = new ByteArrayInputStream(plistXml.getBytes());
		NSObject plist = null;
		try {
			plist = XMLPropertyListParser.parse(is);
		} catch (Exception e) {
			throw new IOException(e);
		}
		
		for(NSObject obj : ((NSArray)plist).getArray()){
			NSDictionary group = (NSDictionary) obj;
			
			String groupName = ((NSString) group.get("group name")).getContent();
			String keyList = ((NSString) group.get("keys")).getContent();
			
			staticGroups.put(groupName, Arrays.asList(keyList.split(keyDelimiter)));
		}
	}
	
	private void parseSmartGroups(String plistXml) throws IOException {
		smartGroups = new HashMap<String, QueryElement>();
		
		InputStream is = new ByteArrayInputStream(plistXml.getBytes());
		NSObject plist = null;
		try {
			plist = XMLPropertyListParser.parse(is);
		} catch (Exception e) {
			throw new IOException(e);
		}

		for(NSObject obj : ((NSArray)plist).getArray()){
			NSDictionary group = (NSDictionary) obj;
			
			try{
				String groupName = ((NSString) group.get("group name")).getContent();
				boolean conjunction = ((NSNumber) group.get("conjunction")).boolValue();
				
				NSArray conditions = (NSArray)group.get("conditions");
				List<QueryElement> subqueries = new ArrayList<QueryElement>();
				
				for(NSObject condition : conditions.getArray()) {
					NSDictionary dict = (NSDictionary) condition;
					int type = ((NSNumber) dict.get("comparison")).intValue();
					
					String field = ((NSString) dict.get("key")).getContent();
					String value = ((NSString) dict.get("value")).getContent();
					
					QueryElement q = buildCondition(type, field, value);
					subqueries.add(q);
				}
				
				if(conjunction)
					smartGroups.put(groupName, new QueryElementOr(subqueries));
				else
					smartGroups.put(groupName, new QueryElementAnd(subqueries));
			} catch(BibDeskUnsupportedFeatureException e) {
				EratosApplication.getApplication().getLogger().debug(e.getMessage());
			}
		}
	}

	// String field comparisons:
	// 0 groups contain
	// 1 groups do not contain
	// 2 contains
	// 3 does not contain
	// 4 is
	// 5 is not
	// 6 starts with
	// 7 ends with
	// 8 comes before
	// 9 comes after
	
	// Booleans: 0 and 1
	
	// Date field comparisons:
	// 0 is today
	// 1 is yesterday
	// 2 is this week
	// 3 is last week
	// 4 is exactly ... ago
	// 5 is in last ...
	// 6 is not in last ...
	// 7 is between ... and ... ago
	// 8 is date ...
	// 9 is after date ...
	// 10 is before date ...
	// 11 is in date range ... to ...
	
	
	private QueryElement buildCondition(int type, String field, String value) throws BibDeskUnsupportedFeatureException {
		String column = getColumnName(field);
		
		if(BibTeXEntryVerifier.isBooleanField(field)){
			if(value.equals("0"))
				value = "false";
			if(value.equals("1"))
				value = "true";
		}
		
		switch(type){
		case 2:
			return new QueryElementLike(column, value);
		case 3:
			return new QueryElementNot( new QueryElementLike(column, value));
		case 4:
			return new QueryElementText(column, value);
		case 5:
			return new QueryElementNot( new QueryElementText(column, value) );
		case 6:
			return new QueryElementStartsWith(column, value);
		case 7:
			return new QueryElementEndsWith(column, value);
		default:
			throw new BibDeskUnsupportedFeatureException("Unsupported comparison operation: " + type);
		}
	}

	private String getColumnName(String fieldName) throws BibDeskUnsupportedFeatureException {
		if(fieldName.equalsIgnoreCase("groups"))
			return DBHelper.COLUMN_GROUPS;
		if(fieldName.equalsIgnoreCase("keywords"))
			return DBHelper.COLUMN_KEYWORDS;
		
		if(fieldName.equalsIgnoreCase("bibtex type"))
			return DBHelper.COLUMN_TYPE;
		
		if(fieldName.equalsIgnoreCase("read"))
			return DBHelper.COLUMN_READ;
		
//		if(fieldName.equalsIgnoreCase("date-added"))
//			return DBHelper.COLUMN_DATE_ADDED;
//		if(fieldName.equalsIgnoreCase("date-modified"))
//			return DBHelper.COLUMN_DATE_MODIFIED;
		
		if(fieldName.equalsIgnoreCase("author"))
			return DBHelper.COLUMN_AUTHOR;
		if(fieldName.equalsIgnoreCase("title"))
			return DBHelper.COLUMN_TITLE;
		if(fieldName.equalsIgnoreCase("year"))
			return DBHelper.COLUMN_YEAR;
		
		throw new BibDeskUnsupportedFeatureException("Unsupported filter field: " + fieldName);
	}

	@Override
	public void extras(BibTeXContentProviderAdapter bibtexAdapter, BibTeXEntryModelFactory modelFactory, ProgressCallback handler) throws IOException {
		if(smartGroups != null){
			int i = 0;
			for(String gName : smartGroups.keySet()){
				handler.update("Inserting smart groups...", i, smartGroups.size());
				QueryElement query = smartGroups.get(gName);
				bibtexAdapter.createFilter(gName, query);
				i+=1;
			}
		}
		if(staticGroups != null){
			try{
				int i = 0;
				for(String gName : staticGroups.keySet()){
					handler.update("Inserting groups...", i, staticGroups.size());
					
					List<String> allKeys = new ArrayList<String>(staticGroups.get(gName));
					
					for(int j = 0; j < allKeys.size(); j += MAX_PROCESS_GROUP_SIZE) {
						int start = j;
						int end = Math.min(j+MAX_PROCESS_GROUP_SIZE, allKeys.size());
						List<String> entryKeys = ArrayUtils.slice(allKeys, start, end);
						
						System.out.println("Adding group: " + gName + " to entries: " + entryKeys);
						
						List<BibTeXEntryModel> entries = bibtexAdapter.getEntries(entryKeys, modelFactory);
						List<BibTeXEntryModel> modified = new ArrayList<BibTeXEntryModel>(entryKeys.size());
						
						for(BibTeXEntryModel entry : entries){
							entry.addGroup( gName );
							BibTeXEntryModel nEntry = modelFactory.commit(entry);
							modified.add( nEntry );
						}
						
						bibtexAdapter.deleteAll(entryKeys);
						bibtexAdapter.bulkInsert(modified, new BulkInsertUpdateListener() {
							public void update(int progress, int max) {
							}
						});
					}
					i+=1;
				}
			}catch(ParseException pe){
				throw new IOException(pe);
			}catch(FieldRequiredException fre){
				throw new IOException(fre);
			}
		}
	}

	protected Map<String, QueryElement> getSmartGroups() {
		return smartGroups;
	}
}
