package com.mm.eratos.io.bibtex.bibdesk;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jbibtex.BibTeXComment;
import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXFormatter;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import com.dd.plist.Base64;
import com.dd.plist.BinaryPropertyListParser;
import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.io.IWriter;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.StringUtils;

public class BibDeskWriter implements IWriter {
	static CaseInsensitiveHashSet reservedFields = new CaseInsensitiveHashSet();
	static {
		reservedFields.add("id");
		reservedFields.add("groups");
	}
	
	public static final String keyDelimiter = ",";
	public static final String bibdeskFileFieldPrefix = "bdsk-file-";
	public static final String bibdeskUrlFieldPrefix = "bdsk-url-";
	public static final String bibdeskExportFileFieldPrefix = "local-url";
	public static final String TYPE_NAME = "bibdesk";

	private OutputStreamWriter os;
	private BibTeXFormatter formatter;
	private Map<String, List<String>> groupMap;
	
	public BibDeskWriter(){
		formatter = new BibTeXFormatter();
		groupMap = new HashMap<String, List<String>>();
	}
	
	@Override
	public String name() {
		return "BibDesk BibTeX";
	}

	@Override
	public String type() {
		return BibDeskParser.BIBDESK_FILE_TYPE;
	}

	@Override
	public void init(OutputStream os, String encoding) throws IOException {
		this.os = new OutputStreamWriter(os, encoding);
	}

	@Override
	public void write(BibTeXObject object) throws IOException {
		if(object instanceof BibTeXComment) {
			BibTeXComment comment = (BibTeXComment) object;
			String commentString = comment.getValue().toUserString().replace("\n","");

			if(commentString.startsWith("BibDesk Static Groups{")){
				return;
			}else{
				os.write( format( comment ) );
			}
		} else if(object instanceof BibTeXEntry) {
			BibTeXEntry entry = (BibTeXEntry) object;
			String refKey = entry.getKey().getValue();
			
			for(String g : getGroups(entry)) {
				if(!groupMap.containsKey(g))
					groupMap.put(g, new ArrayList<String>());
				groupMap.get(g).add(refKey);
			}
			
			entry = toExport(entry);
			os.write( format( entry ) );
		} else {
			os.write( format( object ) );
		}
	}
	
	private List<String> getGroups(BibTeXEntry entry) {
		Value field = entry.getField(BibTeXEntryModel.GROUPS_KEY);
		if(field != null){
			String groupsValue = field.toUserString().trim();
			if(!groupsValue.isEmpty()){
				String [] groups = groupsValue.split(BibTeXEntryModel.DELIMITER);
				return Arrays.asList(groups);
			}
		}
		return Arrays.asList();
	}

	@Override
	public void writeExtras() throws IOException {
		BibTeXComment comment = createGroupsComment();
		os.write( format( comment ) );
	}

	@Override
	public void finish() throws IOException {
		os.close();
	}
	
	private BibTeXEntry toExport(BibTeXEntry entry) throws IOException {
		BibTeXEntry newEntry = new BibTeXEntry(entry.getType(), entry.getKey());
		
		CaseInsensitiveMap<String> localFieldFiles = new CaseInsensitiveMap<String>();
		CaseInsensitiveMap<String> eratosFieldFiles = new CaseInsensitiveMap<String>();
		CaseInsensitiveMap<String> bibdeskFieldFiles = new CaseInsensitiveMap<String>();
		
		Map<Key, Value> fields = entry.getFields();
		for(Key k : fields.keySet()){
			String keyVal = k.getValue().toLowerCase(Locale.US);
			
			if(BibTeXEntryVerifier.fieldStartsWith(keyVal, BibTeXEntryModel.eratosthenesLinkFieldPrefix)){
				String num = keyVal.substring(BibTeXEntryModel.eratosthenesLinkFieldPrefix.length());
				Key nKey = new Key(bibdeskUrlFieldPrefix + num);
				newEntry.addField(nKey, fields.get(k));
				
			} else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, bibdeskFileFieldPrefix)) {
				String fileUri = getFileUri(fields, k);
				bibdeskFieldFiles.put(keyVal, fileUri);
				newEntry.addField(k, fields.get(k));
				
			} else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, bibdeskExportFileFieldPrefix)){
				String fileUri = EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + fields.get(k).toUserString();
				localFieldFiles.put(keyVal, fileUri);

			} else if(BibTeXEntryVerifier.fieldStartsWith(keyVal, BibTeXEntryModel.eratosthenesFileFieldPrefix)){
				eratosFieldFiles.put(keyVal, fields.get(k).toUserString());
				
			} else if(BibTeXEntryVerifier.isBooleanField(keyVal)){
				String value = entry.getField(k).toUserString();
				if(value.equalsIgnoreCase("true")) value = "1";
				if(value.equalsIgnoreCase("false")) value = "0";
				
				newEntry.addField(k, new StringValue(value, Style.BRACED));
			} else if(! reservedFields.contains(keyVal)){
				newEntry.addField(k, fields.get(k));
			}
		}
		
		exportLocalFiles(newEntry, localFieldFiles, eratosFieldFiles, bibdeskFieldFiles);
		return newEntry;
	}
	
	private void exportLocalFiles(BibTeXEntry entry,
			CaseInsensitiveMap<String> localFieldFiles,
			CaseInsensitiveMap<String> eratosFieldFiles,
			CaseInsensitiveMap<String> bibdeskFieldFiles) throws IOException {
		
		int fileNum = 1;
		for(String k : localFieldFiles.keySet()) {
			EratosUri fileUri = EratosUri.parseUri(localFieldFiles.get(k));
			Key localKey = makeExportFieldKey(fileNum);
			
			Value filePath = new StringValue(fileUri.path(), Style.BRACED);
			entry.addField(localKey, filePath);
			
			fileNum++;
		}
		
		for(String k : eratosFieldFiles.keySet()) {
			EratosUri fileUri = EratosUri.parseUri(eratosFieldFiles.get(k));
			
			boolean isBdsk = hasFile(fileUri, bibdeskFieldFiles);
			boolean isLocal = hasFile(fileUri, localFieldFiles);
			
			if( !isBdsk && !isLocal ){
				Key localKey = makeExportFieldKey(fileNum);
				Value filePath = new StringValue(convertPathToRelative(fileUri), Style.BRACED);
				entry.addField(localKey, filePath);
				fileNum++;
			}
		}
	}

	private Key makeExportFieldKey(int fileNum) {
		String localKeyValue = bibdeskExportFileFieldPrefix + "-" +  fileNum;
		if(fileNum==1)
			localKeyValue = bibdeskExportFileFieldPrefix;
		return new Key(localKeyValue);
	}

	private boolean hasFile(EratosUri fileUri, CaseInsensitiveMap<String> fieldFiles) throws IOException {
		String canonicalPath = FileManager.getFileManager().getFile(fileUri).getCanonicalPath();
		
		for(String k : fieldFiles.keySet()){
			EratosUri bFileUri = EratosUri.parseUri(fieldFiles.get(k));
			File bFile = FileManager.getFileManager().getFile(bFileUri);
			
			if( canonicalPath.equals( bFile.getCanonicalPath() ) )
				return true;
		}
		return false;
	}

	private String convertPathToRelative(EratosUri fileUri) {
		EratosApplication app = EratosApplication.getApplication();
		EratosSettings settingsManager = app.getSettingsManager();
		EratosUri resUri = fileUri.resolveUri(app);
		return resUri.relativeTo(settingsManager.getStoreLocalLocation().resolveUri(app));
	}

	public String format(BibTeXObject obj) throws IOException {
	    BibTeXDatabase btdb = new BibTeXDatabase();
	    btdb.addObject(obj);
	    
	    StringWriter sw = new StringWriter();
		formatter.format(btdb, sw);
	    
	    String formattedEntry = sw.getBuffer().toString();
		return formattedEntry.replaceAll("\n\\s+", "\n") + "\n\n";
	}
	
	private BibTeXComment createGroupsComment() {
		return new BibTeXComment(new StringValue("BibDesk Static Groups{\n" + buildPlist(groupMap) + "\n}", Style.BRACED));
	}

	private String buildPlist(Map<String, List<String>> groupMap) {
		NSArray array = new NSArray(groupMap.size());
		
		int i = 0;
		for(String group : groupMap.keySet()){
			NSDictionary gDict = new NSDictionary();
			gDict.put("group name", group);
			gDict.put("keys", StringUtils.join(keyDelimiter, groupMap.get(group)));
			array.setValue(i, gDict);
			i++;
		}
		
		return array.toXMLPropertyList();
	}
	
	private String getFileUri(Map<Key, Value> fields, Key k) throws IOException {
		String b64 = fields.get(k).toUserString();
		String relPath = parsePropertyList(b64);
		return mapRelPath(relPath).toString();
	}

	private String parsePropertyList(String b64) throws IOException {
		try {
			InputStream is = new Base64.B64InputStream(new ByteArrayInputStream(b64.getBytes()));
			NSObject obj = BinaryPropertyListParser.parse(is);				
			NSArray arr = (NSArray) ((NSDictionary)obj).get("$objects");
			NSString nsRelPath = (NSString) arr.getArray()[4];
			return nsRelPath.getContent();
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	private EratosUri mapRelPath(String relPath) {
		EratosUri fileUri = EratosUri.parseUri(EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + relPath);
		
		List<String> elements = fileUri.elements();
		if(elements.contains("Dropbox")){
			int pos = elements.indexOf("Dropbox");
			String path = StringUtils.join("/", fileUri.slice(pos+1));
			return EratosUri.parseUri(EratosUri.DROPBOX_PROTOCOL + ":///" + path);
		}
		return fileUri;
	}

}
