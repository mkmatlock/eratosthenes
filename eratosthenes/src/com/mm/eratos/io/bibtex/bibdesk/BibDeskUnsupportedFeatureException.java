package com.mm.eratos.io.bibtex.bibdesk;

public class BibDeskUnsupportedFeatureException extends Exception {

	public BibDeskUnsupportedFeatureException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6454506215294362961L;

}
