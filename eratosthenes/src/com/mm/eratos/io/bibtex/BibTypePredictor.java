package com.mm.eratos.io.bibtex;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.io.bibtex.bibdesk.BibDeskParser;
import com.mm.eratos.io.bibtex.jabref.JabRefParser;
import com.mm.eratos.utils.Functional;
import com.mm.eratos.utils.TypedRunnable;

public class BibTypePredictor {
	private static final Functional.PatternFind patternFind = new Functional.PatternFind();
	private static final Functional.Any any = new Functional.Any();
	
	private static class PatternReduce implements TypedRunnable<String, Boolean> {
		Pattern [] patterns;
		
		public PatternReduce(Pattern ... patterns){
			this.patterns = patterns;
		}
		
		public Boolean run(String arg) {
			patternFind.text = arg;
			return Functional.reduce(Functional.map(patterns, patternFind), false, any);
		}
	}
	
	private static final Pattern ERATOSTHENES = Pattern.compile("Eratosthenes", Pattern.CASE_INSENSITIVE);
	private static final Pattern JABREF = Pattern.compile("JabRef", Pattern.CASE_INSENSITIVE);
	private static final Pattern BIBDESK = Pattern.compile("BibDesk", Pattern.CASE_INSENSITIVE);
	
	private static final Pattern ERATOS_FILE_FIELD = Pattern.compile(BibTeXEntryModel.eratosthenesFileFieldPrefix+"1", Pattern.CASE_INSENSITIVE);
	private static final Pattern BIBDESK_FILE_FIELD = Pattern.compile(BibDeskParser.bibdeskFileFieldPrefix+"1", Pattern.CASE_INSENSITIVE);
	
	private static final Pattern BIBDESK_STATIC_GROUPS = Pattern.compile("BibDesk Static Groups", Pattern.CASE_INSENSITIVE);
	private static final Pattern JABREF_META_GROUPSVERSION = Pattern.compile("jabref-meta: groupsversion:", Pattern.CASE_INSENSITIVE);
	private static final Pattern JABREF_META_GROUPSTREE = Pattern.compile("jabref-meta: groupstree:", Pattern.CASE_INSENSITIVE);

	public static String predictBibFileType(File bibtexFile, String encoding) throws IOException {
		boolean eratosEvidence = false;
		
		List<String> results = 
				FileUtils.grep(bibtexFile,
								encoding,
								JABREF,
								BIBDESK,
								ERATOSTHENES,
								JABREF_META_GROUPSTREE, 
								JABREF_META_GROUPSVERSION, 
								BIBDESK_STATIC_GROUPS,
								BIBDESK_FILE_FIELD,
								ERATOS_FILE_FIELD);

		if(containsMatches(results, ERATOSTHENES, ERATOS_FILE_FIELD))
			eratosEvidence = true;
		
		if(containsMatches(results, JABREF_META_GROUPSTREE, JABREF_META_GROUPSVERSION, JABREF))
			return JabRefParser.JABREF_FILE_TYPE;
		
		if(containsMatches(results, BIBDESK_STATIC_GROUPS, BIBDESK_FILE_FIELD, BIBDESK))
			return BibDeskParser.BIBDESK_FILE_TYPE;
		
		return eratosEvidence ? EratosthenesParser.ERATOS_FILE_TYPE : "";
	}

	private static boolean containsMatches(List<String> results, Pattern ... patterns) {
		return Functional.reduce(Functional.map(results, new PatternReduce(patterns)), false, any);
	}
	
	
}
