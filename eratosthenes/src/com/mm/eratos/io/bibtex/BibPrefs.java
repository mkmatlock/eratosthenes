package com.mm.eratos.io.bibtex;

import java.util.List;

import org.jbibtex.BibTeXComment;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

import com.mm.eratos.application.EratosApplication;

public class BibPrefs {
	public static final String eratosSettingsHeader = "EratosPrefs{";
	private EratosApplication app;

	public BibPrefs(EratosApplication app){
		this.app = app;
	}
	
	public BibTeXComment writePreferences(){
		String settings = "";
		return new BibTeXComment(new StringValue(settings, Style.BRACED));
	}
	
	public void readPreferences(List<BibTeXComment> comments){
		for(BibTeXComment comment : comments){
			String commentValue = comment.getValue().toUserString();
			if(commentValue.startsWith(eratosSettingsHeader)){
				readPreferences(commentValue);
				return;
			}
		}
	}

	private void readPreferences(String commentValue) {
		// TODO Auto-generated method stub
		
	}
}
