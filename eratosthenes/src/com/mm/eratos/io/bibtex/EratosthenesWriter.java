package com.mm.eratos.io.bibtex;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXFormatter;
import org.jbibtex.BibTeXObject;

import com.mm.eratos.io.IWriter;

public class EratosthenesWriter implements IWriter {
	private OutputStreamWriter os;
	
	@Override
	public String name() {
		return "Eratosthenes BibTeX";
	}
	
	@Override
	public String type() {
		return EratosthenesParser.ERATOS_FILE_TYPE;
	}
	
	@Override
	public void init(OutputStream os, String encoding) throws IOException {
		this.os = new OutputStreamWriter(os, encoding);
	}
	
	@Override
	public void write(BibTeXObject obj) throws IOException {
		String formattedEntry = format(obj);
		os.write( formattedEntry );
	}
	
	public String format(BibTeXObject obj) throws IOException {
	    BibTeXDatabase btdb = new BibTeXDatabase();
	    btdb.addObject(obj);
	    
	    StringWriter sw = new StringWriter();
	    new BibTeXFormatter().format(btdb, sw);
	    
	    String formattedEntry = sw.getBuffer().toString();
		return formattedEntry.replaceAll("\n\\s+", "\n") + "\n\n";
	}

	@Override
	public void writeExtras() throws IOException {
		
	}
	
	@Override
	public void finish() throws IOException {
		os.close();
	}
}
