package com.mm.eratos.io.bibtex;

import java.io.IOException;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.io.AbstractBibTeXParser;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.WebUtils;

public class EratosthenesParser extends AbstractBibTeXParser {
	public static final String ERATOS_FILE_TYPE = WebUtils.BIBTEX_MIMETYPE + ":eratosthenes";

	public EratosthenesParser(){
	}


	@Override
	public String name() {
		return "Eratosthenes BibTeX";
	}

	@Override
	public String type() {
		return ERATOS_FILE_TYPE;
	}
	
	@Override
	public void extras(BibTeXContentProviderAdapter bibtexAdapter, BibTeXEntryModelFactory modelFactory, ProgressCallback progressHandler) throws IOException {
		
	}
}
