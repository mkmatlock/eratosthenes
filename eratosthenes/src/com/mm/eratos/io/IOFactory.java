package com.mm.eratos.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mm.eratos.io.bibtex.EratosthenesParser;
import com.mm.eratos.io.bibtex.EratosthenesWriter;
import com.mm.eratos.io.bibtex.bibdesk.BibDeskParser;
import com.mm.eratos.io.bibtex.bibdesk.BibDeskWriter;
import com.mm.eratos.io.bibtex.jabref.JabRefParser;
import com.mm.eratos.io.bibtex.jabref.JabRefWriter;
import com.mm.eratos.io.endnote.EndNoteParser;
import com.mm.eratos.io.endnote.EndNoteWriter;
import com.mm.eratos.io.ris.RISParser;
import com.mm.eratos.io.ris.RISWriter;

public class IOFactory {
	private static Map<String, IParser> parsers = new HashMap<String, IParser>();
	private static Map<String, IWriter> writers = new HashMap<String, IWriter>();
	
	static{
		IOFactory.register(new BibDeskParser());
		IOFactory.register(new BibDeskWriter());
		
		IOFactory.register(new JabRefParser());
		IOFactory.register(new JabRefWriter());
		
		IOFactory.register(new EratosthenesParser());
		IOFactory.register(new EratosthenesWriter());
		
		IOFactory.register(new RISParser());
		IOFactory.register(new RISWriter());
		
		IOFactory.register(new EndNoteParser());
		IOFactory.register(new EndNoteWriter());
	}
	
	public static IParser getParser(String type) {
		return parsers.get(type);
	}
	
	public static IWriter getWriter(String type) {
		return writers.get(type);
	}
	
	public static Map<String, String> getSupportedTypes(){
		Map<String, String> names = new HashMap<String, String>(); 
		for(String type : parsers.keySet()){
			names.put(parsers.get(type).name(), type);
		}
		return names;
	}
	
	public static Map<String, String> getSupportedTypes(String mimeType) {
		Map<String, String> names = new HashMap<String, String>(); 
		for(String type : parsers.keySet()){
			if(type.startsWith(mimeType))
				names.put(parsers.get(type).name(), type);
		}
		return names;
	}
	
	public static void register(IParser parser){
		parsers.put(parser.type(), parser);
	}
	
	public static void register(IWriter writer){
		writers.put(writer.type(), writer);
	}

	public static List<String> getSupportedFileEncodings() {
		List<String> encodings =  new ArrayList<String>();
		encodings.add("UTF-8");
		encodings.add("ISO-8859-1");
		encodings.add("US-ASCII");
		encodings.add("UTF-16");
		encodings.add("UTF-16BE");
		encodings.add("UTF-16LE");
		return encodings;
	}
}
