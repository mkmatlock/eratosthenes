package com.mm.eratos.io.ris;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.DigitStringValue;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

import com.google.android.vending.licensing.util.Base64;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.io.IOFactory;
import com.mm.eratos.io.IParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebUtils;

public class RISParser implements IParser {
	static{
		IOFactory.register(new RISParser());
	}
	
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd", Locale.US);
	private static final Pattern fieldPattern = Pattern.compile("([A-Z][A-Z0-9])  \\-(.*)");
	public static final Key RIS_UNUSED_FIELD = new Key("ris-unused-values");
	
	
	private static final Map<String, String> typeMap;
	private static final Map<String, Key> fieldMap;
	private static final Map<Integer, String> months;
	
	static{
		typeMap = new HashMap<String, String>();
		fieldMap = new HashMap<String, Key>();
		months = new HashMap<Integer, String>();
		
		fieldMap.put(RISEntry.RESEARCH_NOTES_TAG, BibTeXEntryModel.COMMENTS_KEY);
		fieldMap.put(RISEntry.ABSTRACT_TAG, BibTeXEntryModel.ABSTRACT_KEY);
		
		fieldMap.put(RISEntry.ISBN_TAG, BibTeXEntryModel.ISBN_KEY);
		fieldMap.put(RISEntry.DOI_TAG, BibTeXEntryModel.DOI_KEY);
		
		fieldMap.put(RISEntry.PUBLISHER_TAG, BibTeXEntryModel.PUBLISHER_KEY);
		fieldMap.put(RISEntry.VOLUME_TAG, BibTeXEntryModel.VOLUME_KEY);
		
		fieldMap.put(RISEntry.JOURNAL_TAG, BibTeXEntryModel.JOURNAL_KEY);
		fieldMap.put(RISEntry.EDITION_TAG, BibTeXEntryModel.EDITION_KEY);
		fieldMap.put(RISEntry.NUMBER_TAG, BibTeXEntryModel.NUMBER_KEY);
		fieldMap.put(RISEntry.YEAR_TAG, BibTeXEntryModel.YEAR_KEY);
		fieldMap.put(RISEntry.CITY_TAG, BibTeXEntryModel.ADDRESS_KEY);
		
		months.put(1, "jan");
	    months.put(2, "feb");
	    months.put(3, "mar");
	    months.put(4, "apr");
	    months.put(5, "may");
	    months.put(6, "jun");
	    months.put(7, "jul");
	    months.put(8, "aug");
	    months.put(9, "sep");
	    months.put(10, "oct");
	    months.put(11, "nov");
	    months.put(12, "dec");
		
		typeMap.put("ABST", "misc"); // - Abstract
		typeMap.put("ADVS", "misc"); // - Audiovisual material
		typeMap.put("AGGR", "misc"); // - Aggregated Database
		typeMap.put("ANCIENT", "misc"); // - Ancient Text
		typeMap.put("ART", "misc"); // - Art Work
		typeMap.put("BILL", "misc"); // - Bill
		typeMap.put("BLOG", "misc"); // - Blog
		typeMap.put("BOOK", "book"); // - Whole book
		typeMap.put("CASE", "misc"); // - Case
		typeMap.put("CHAP", "inbook"); // - Book chapter
		typeMap.put("CHART", "misc"); // - Chart
		typeMap.put("CLSWK", "misc"); // - Classical Work
		typeMap.put("COMP", "misc"); // - Computer program
		typeMap.put("CONF", "proceedings"); // - Conference proceeding
		typeMap.put("CONF", "conference"); // - Conference proceeding
		typeMap.put("CPAPER", "inproceedings"); // - Conference paper
		typeMap.put("CTLG", "incollection"); // - Catalog
		typeMap.put("DATA", "misc"); // - Data file
		typeMap.put("DBASE", "misc"); // - Online Database
		typeMap.put("DICT", "misc"); // - Dictionary
		typeMap.put("EBOOK", "misc"); // - Electronic Book
		typeMap.put("ECHAP", "misc"); // - Electronic Book Section
		typeMap.put("EDBOOK", "misc"); // - Edited Book
		typeMap.put("EJOUR", "misc"); // - Electronic Article
		typeMap.put("ELEC", "misc"); // - Web Page
		typeMap.put("ENCYC", "misc"); // - Encyclopedia
		typeMap.put("EQUA", "misc"); // - Equation
		typeMap.put("FIGURE", "misc"); // - Figure
		typeMap.put("GEN", "misc"); // - Generic
		typeMap.put("GOVDOC", "misc"); // - Government Document
		typeMap.put("GRANT", "misc"); // - Grant
		typeMap.put("HEAR", "misc"); // - Hearing
		typeMap.put("ICOMM", "misc"); // - Internet Communication
		typeMap.put("INPR", "misc"); // - In Press
		typeMap.put("JFULL", "journal"); // - Journal (full)
		typeMap.put("JOUR", "article"); // - Journal
		typeMap.put("LEGAL", "misc"); // - Legal Rule or Regulation
		typeMap.put("MANSCPT", "misc"); // - Manuscript
		typeMap.put("MAP", "misc"); // - Map
		typeMap.put("MGZN", "misc"); // - Magazine article
		typeMap.put("MPCT", "misc"); // - Motion picture
		typeMap.put("MULTI", "misc"); // - Online Multimedia
		typeMap.put("MUSIC", "misc"); // - Music score
		typeMap.put("NEWS", "misc"); // - Newspaper
		typeMap.put("PAMP", "booklet"); // - Pamphlet
		typeMap.put("PAT", "misc"); // - Patent
		typeMap.put("PCOMM", "misc"); // - Personal communication
		typeMap.put("RPRT", "techreport"); // - Report
		typeMap.put("SER", "misc"); // - Serial publication
		typeMap.put("SLIDE", "misc"); // - Slide
		typeMap.put("SOUND", "misc"); // - Sound recording
		typeMap.put("STAND", "misc"); // - Standard
		typeMap.put("STAT", "misc"); // - Statute
		typeMap.put("THES", "phdthesis"); // - Thesis/Dissertation
		typeMap.put("THES", "mastersthesis"); // - Thesis/Dissertation
		typeMap.put("UNPB", "unpublished"); // - Unpublished work
		typeMap.put("VIDEO", "misc"); // - Video recording
	}
	
	
	private BufferedReader input;
	private List<Throwable> errors;
	private RISEntry object;
	private int lineNum;
	private boolean scanToNextType;
	private BibTeXHelper helper;
	private int length;
	private int processed;
	
	@Override
	public String name() {
		return "RIS";
	}

	@Override
	public String type() {
		return WebUtils.RIS_MIMETYPE;
	}

	@Override
	public void init(InputStream is, String encoding, int length) throws IOException, ParseException {
		this.lineNum = 0;
		this.processed = 0;
		this.scanToNextType = false;
		this.errors = new ArrayList<Throwable>();
		this.helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		this.input = new BufferedReader(new InputStreamReader(is, encoding));
		this.length = length;
	}
	
	@Override
	public long processed() {
		return processed;
	}

	@Override
	public long total() {
		return length;
	}
	

	@Override
	public boolean next() throws IOException, ParseException {
		String line = null;
		while((line = input.readLine()) != null){
			processed += line.getBytes().length;
			lineNum++;
			line = line.trim();
			
			Matcher m = fieldPattern.matcher(line);
			boolean matches = m.matches();
			
			if(matches){
				String tag = m.group(1);
				String val = m.group(2).trim();
				
				if(scanToNextType && tag.equals(RISEntry.TYPE_TAG)){
					process(tag, val);
				}else if(!scanToNextType){
					try{
						if( process(tag, val) ) return true;
					}catch(IOException e){
						errors.add(e);
						scanToNextType=true;
					}
				}
			}else if(line.isEmpty()){
				
			}else{
				errors.add(new ParseException(String.format("RIS Error at line #%d: Expected field or blank line, but got '%s'", lineNum, line)));
			}
		}
		
		return false;
	}

	private boolean process(String tag, String val) throws IOException {
		if(tag.equals(RISEntry.TYPE_TAG)) {
			object = new RISEntry();
			object.put(RISEntry.TYPE_TAG, val);
		} else if(tag.equals(RISEntry.END_TAG)) {
			object.close();
			return true;
		} else {
			object.put(tag, val);
		}
		return false;
	}

	@Override
	public BibTeXObject raw() {
		return null;
	}

	@Override
	public BibTeXObject get() throws IOException, ParseException {
		return translate(object);
	}

	private BibTeXObject translate(RISEntry ris) throws IOException, ParseException {
		String risType = ris.getType();
		String bibType = typeMap.get(risType);
		
		Key citeKey = new Key("tempKey");
		boolean gotCiteKey = false;
		if(ris.containsKey(RISEntry.CITEKEY_TAG)){
			gotCiteKey = true;
			citeKey = new Key(ris.get(RISEntry.CITEKEY_TAG).get(0));
		}
		BibTeXEntry entry = new BibTeXEntry(new Key(bibType), citeKey);
		
		for(String tag : new HashSet<String>(ris.keySet())){
			if(fieldMap.containsKey(tag)){
				Key bibField = fieldMap.get(tag);
				String value = ris.get(tag).get(0);
				addStringField(entry, bibField, value);
				ris.remove(tag);
			}
		}
		
		buildAuthorList(ris, entry);
		buildEditorList(ris, entry);
		buildTitle(ris, entry);
		buildDate(ris, entry);
		buildKeywords(ris, entry);
		buildURLs(ris, entry);
		buildFiles(ris, entry);
		buildPages(ris, entry);
		
		preserveExtras(ris, entry);
		if(!gotCiteKey)	entry = setRefKey(entry);
		
		return entry;
	}

	private void buildPages(RISEntry ris, BibTeXEntry entry) {
		String start = "";
		String end = "";
		if(ris.containsKey(RISEntry.START_PAGE_TAG))
			start = ris.get(RISEntry.START_PAGE_TAG).get(0).trim();
		if(ris.containsKey(RISEntry.END_PAGE_TAG))
			end = ris.get(RISEntry.END_PAGE_TAG).get(0).trim();
		
		if(!start.isEmpty() && end.isEmpty()){
			addStringField(entry, BibTeXEntryModel.PAGES_KEY, start);
			
			ris.remove(RISEntry.START_PAGE_TAG);
		}else if(!start.isEmpty() && !end.isEmpty() && start.equals(end)){
			addStringField(entry, BibTeXEntryModel.PAGES_KEY, start);
			
			ris.remove(RISEntry.START_PAGE_TAG);
			ris.remove(RISEntry.END_PAGE_TAG);
		}else if(!start.isEmpty() && !end.isEmpty() && !start.equals(end)){
			addStringField(entry, BibTeXEntryModel.PAGES_KEY, start + "-" + end);
			
			ris.remove(RISEntry.START_PAGE_TAG);
			ris.remove(RISEntry.END_PAGE_TAG);
		}
	}

	private void addStringField(BibTeXEntry entry, Key key, String value) {
		entry.addField(key, new StringValue(value, Style.BRACED));
	}

	private void buildFiles(RISEntry ris, BibTeXEntry entry) {
		if(ris.containsKey(RISEntry.FILE_TAG)){
			int i = 0;
			for(String path : ris.get(RISEntry.FILE_TAG)){
				Key k = new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + i);
				EratosUri uri = EratosUri.makeUri(EratosUri.BIBTEX_LOCAL_PROTOCOL, path);
				addStringField(entry, k, uri.toString());
				i++;
			}
			ris.remove(RISEntry.FILE_TAG);
		}
	}

	private void buildURLs(RISEntry ris, BibTeXEntry entry) {
		if(ris.containsKey(RISEntry.URL_TAG)){
			int i = 0;
			for(String url : ris.get(RISEntry.URL_TAG)){
				Key k = new Key(BibTeXEntryModel.eratosthenesLinkFieldPrefix + i);
				addStringField(entry, k, url);
				i++;
			}
			ris.remove(RISEntry.URL_TAG);
		}
	}

	private void buildKeywords(RISEntry ris, BibTeXEntry entry) {
		if(ris.containsKey(RISEntry.KEYWORD_TAG)){
			String kwStrValue = StringUtils.join(BibTeXEntryModel.DELIMITER, ris.get(RISEntry.KEYWORD_TAG));
			addStringField(entry, BibTeXEntryModel.KEYWORDS_KEY, kwStrValue);
			ris.remove(RISEntry.KEYWORD_TAG);
		}
	}

	private BibTeXEntry setRefKey(BibTeXEntry entry) throws IOException, ParseException {
		CiteKeyFormatter formatter = EratosApplication.getApplication().getSettingsManager().getCiteKeyFormatter();
		String newKey = formatter.format(entry, helper);
		
		BibTeXEntry copy = new BibTeXEntry(entry.getType(), new Key(newKey));
		for(Key k : entry.getFields().keySet())
			copy.addField(k, entry.getField(k));
		
		return copy;
	}

	private void buildDate(RISEntry ris, BibTeXEntry entry) {
		if(ris.containsKey(RISEntry.DATE_TAG)){
			String value = ris.get(RISEntry.DATE_TAG).get(0);
			String [] parts = value.split("/", -1);
			
			if(!parts[0].isEmpty())
				addStringField(entry, BibTeXEntryModel.YEAR_KEY, parts[0]);
			
			
			if(parts.length > 1 && !parts[1].isEmpty()){
				String monthMacro = months.get( Integer.parseInt(parts[1]) );
				entry.addField( BibTeXEntryModel.MONTH_KEY, new DigitStringValue(monthMacro) );
			}
			
			ris.remove(RISEntry.DATE_TAG);
		}
		
		if(ris.containsKey(RISEntry.YEAR_TAG)){
			String yearStr = ris.get(RISEntry.YEAR_TAG).get(0).split("/", -1)[0];
			if(!yearStr.isEmpty())
				addStringField(entry, BibTeXEntryModel.YEAR_KEY, yearStr);
			
			ris.remove(RISEntry.YEAR_TAG);
		}
		
		try {
			if(ris.containsKey(RISEntry.ACCESS_DATE_TAG)){
				Date d = dateFormat.parse(ris.get(RISEntry.ACCESS_DATE_TAG).get(0));
				String dateStr = BibTeXEntryModel.dateFormat.format(d);
				addStringField(entry, BibTeXEntryModel.CREATED_KEY, dateStr);
				ris.remove(RISEntry.ACCESS_DATE_TAG);
			}
		} catch (java.text.ParseException e) {
		}
	}

	private void preserveExtras(RISEntry ris, BibTeXEntry entry) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		new ObjectOutputStream( baos ).writeObject(ris);
		String base64ris = Base64.encode(baos.toByteArray());
		addStringField(entry, RIS_UNUSED_FIELD, base64ris);
	}

	private void buildAuthorList(RISEntry ris, BibTeXEntry entry) {
		List<String> authors = new ArrayList<String>();
		
		if(ris.containsKey(RISEntry.AUTHOR_TAG))
			authors.addAll( ris.get(RISEntry.AUTHOR_TAG) );
		if(ris.containsKey(RISEntry.AUTHOR_FIRST_TAG))
			authors.addAll( ris.get(RISEntry.AUTHOR_FIRST_TAG) );
		
		ris.remove(RISEntry.AUTHOR_TAG);
		ris.remove(RISEntry.AUTHOR_FIRST_TAG);
		
		if(authors.size() > 0){
			String authorString = StringUtils.join(" and ", authors);
			addStringField(entry, BibTeXEntryModel.AUTHOR_KEY, authorString);
		}
	}

	private void buildEditorList(RISEntry ris, BibTeXEntry entry) {
		List<String> editors = new ArrayList<String>();
		if(ris.containsKey(RISEntry.EDITOR_TAG))
			editors.addAll( ris.get(RISEntry.EDITOR_TAG) );
		
		ris.remove(RISEntry.EDITOR_TAG);
		if(editors.size() > 0){
			String editorString = StringUtils.join(" and ", editors);
			addStringField(entry, BibTeXEntryModel.EDITOR_KEY, editorString);
		}
	}

	private void buildTitle(RISEntry ris, BibTeXEntry entry){
		if(ris.containsKey(RISEntry.TITLE_TAG)){
			String title = ris.get(RISEntry.TITLE_TAG).get(0);
			addStringField(entry, BibTeXEntryModel.TITLE_KEY, title);
			ris.remove(RISEntry.TITLE_TAG);
		}else if(ris.containsKey(RISEntry.TITLE_FIRST_TAG)){
			String title = ris.get(RISEntry.TITLE_FIRST_TAG).get(0);
			addStringField(entry, BibTeXEntryModel.TITLE_KEY, title);
			ris.remove(RISEntry.TITLE_FIRST_TAG);
		}
		if(ris.containsKey(RISEntry.TITLE_SECOND_TAG)){
			String subtitle = ris.get(RISEntry.TITLE_SECOND_TAG).get(0);
			addStringField(entry, BibTeXEntryModel.SUBTITLE_KEY, subtitle);
			ris.remove(RISEntry.TITLE_SECOND_TAG);
		}
	}
	

	@Override
	public void extras(BibTeXContentProviderAdapter bibtexAdapter,
			BibTeXEntryModelFactory modelFactory,
			ProgressCallback progressHandler) throws IOException {
		
	}

	@Override
	public String errors() throws IOException {
		String errorString = "";
		for(Throwable t : errors)
			errorString += t.getMessage() +"\n";
		return errorString;
	}
	
	@Override
	public void finish() throws IOException {
		input.close();
	}
}
