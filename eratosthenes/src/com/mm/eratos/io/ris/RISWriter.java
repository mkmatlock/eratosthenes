package com.mm.eratos.io.ris;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.Value;

import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.io.IOFactory;
import com.mm.eratos.io.IWriter;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.WebUtils;

public class RISWriter implements IWriter {
	static{
		IOFactory.register(new RISWriter());
	}

	static final DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd", Locale.US);
	private static final Map<String, String> typeMap;
	private static final Map<Key, String> fieldMap;
	private static final CaseInsensitiveMap<String> macros;
	
	static{
		typeMap = new HashMap<String, String>();
		macros = new CaseInsensitiveMap<String>();
				
		macros.put("jan", "01");
	    macros.put("feb", "02");
	    macros.put("mar", "03");
	    macros.put("apr", "04");
	    macros.put("may", "05");
	    macros.put("jun", "06");
	    macros.put("jul", "07");
	    macros.put("aug", "08");
	    macros.put("sep", "09");
	    macros.put("oct", "10");
	    macros.put("nov", "11");
	    macros.put("dec", "12");
		
//		typeMap.put("", "ABST"); // - Abstract
//		typeMap.put("", "ADVS"); // - Audiovisual material
//		typeMap.put("", "AGGR"); // - Aggregated Database
//		typeMap.put("", "ANCIENT"); // - Ancient Text
//		typeMap.put("", "ART"); // - Art Work
//		typeMap.put("", "BILL"); // - Bill
//		typeMap.put("", "BLOG"); // - Blog
		typeMap.put("book", "BOOK"); // - Whole book
//		typeMap.put("", "CASE"); // - Case
		typeMap.put("inbook", "CHAP"); // - Book chapter
//		typeMap.put("", "CHART"); // - Chart
//		typeMap.put("", "CLSWK"); // - Classical Work
//		typeMap.put("", "COMP"); // - Computer program
		typeMap.put("proceedings", "CONF"); // - Conference proceeding
		typeMap.put("conference", "CONF"); // - Conference proceeding
		typeMap.put("inproceedings", "CPAPER"); // - Conference paper
		typeMap.put("incollection", "CTLG"); // - Catalog
//		typeMap.put("", "DATA"); // - Data file
//		typeMap.put("", "DBASE"); // - Online Database
//		typeMap.put("", "DICT"); // - Dictionary
//		typeMap.put("", "EBOOK"); // - Electronic Book
//		typeMap.put("", "ECHAP"); // - Electronic Book Section
//		typeMap.put("", "EDBOOK"); // - Edited Book
//		typeMap.put("", "EJOUR"); // - Electronic Article
//		typeMap.put("", "ELEC"); // - Web Page
//		typeMap.put("", "ENCYC"); // - Encyclopedia
//		typeMap.put("", "EQUA"); // - Equation
//		typeMap.put("", "FIGURE"); // - Figure
		typeMap.put("misc", "GEN"); // - Generic
//		typeMap.put("", "GOVDOC"); // - Government Document
//		typeMap.put("", "GRANT"); // - Grant
//		typeMap.put("", "HEAR"); // - Hearing
//		typeMap.put("", "ICOMM"); // - Internet Communication
//		typeMap.put("", "INPR"); // - In Press
//		typeMap.put("journal", "JFULL"); // - Journal (full)
		typeMap.put("article", "JOUR"); // - Journal
//		typeMap.put("", "LEGAL"); // - Legal Rule or Regulation
//		typeMap.put("", "MANSCPT"); // - Manuscript
//		typeMap.put("", "MAP"); // - Map
//		typeMap.put("", "MGZN"); // - Magazine article
//		typeMap.put("", "MPCT"); // - Motion picture
//		typeMap.put("", "MULTI"); // - Online Multimedia
//		typeMap.put("", "MUSIC"); // - Music score
//		typeMap.put("", "NEWS"); // - Newspaper
		typeMap.put("booklet", "PAMP"); // - Pamphlet
//		typeMap.put("", "PAT"); // - Patent
//		typeMap.put("", "PCOMM"); // - Personal communication
		typeMap.put("techreport", "RPRT"); // - Report
//		typeMap.put("", "SER"); // - Serial publication
//		typeMap.put("", "SLIDE"); // - Slide
//		typeMap.put("", "SOUND"); // - Sound recording
//		typeMap.put("", "STAND"); // - Standard
//		typeMap.put("", "STAT"); // - Statute
		typeMap.put("phdthesis", "THES"); // - Thesis/Dissertation
		typeMap.put("mastersthesis", "THES"); // - Thesis/Dissertation
		typeMap.put("unpublished", "UNPB"); // - Unpublished work
//		typeMap.put("", "VIDEO"); // - Video recording
		
		fieldMap = new HashMap<Key, String>();
		fieldMap.put(BibTeXEntryModel.COMMENTS_KEY, 	RISEntry.RESEARCH_NOTES_TAG);
		fieldMap.put(BibTeXEntryModel.ABSTRACT_KEY, 	RISEntry.ABSTRACT_TAG);
		
		fieldMap.put(BibTeXEntryModel.ISBN_KEY, 		RISEntry.ISBN_TAG);
		fieldMap.put(BibTeXEntryModel.DOI_KEY, 			RISEntry.DOI_TAG);
		
		fieldMap.put(BibTeXEntryModel.PUBLISHER_KEY, 	RISEntry.PUBLISHER_TAG);
		fieldMap.put(BibTeXEntryModel.VOLUME_KEY, 		RISEntry.VOLUME_TAG);
		
		fieldMap.put(BibTeXEntryModel.JOURNAL_KEY,		RISEntry.JOURNAL_TAG);
		fieldMap.put(BibTeXEntryModel.EDITION_KEY,		RISEntry.EDITION_TAG);
		fieldMap.put(BibTeXEntryModel.NUMBER_KEY,		RISEntry.NUMBER_TAG);
		fieldMap.put(BibTeXEntryModel.YEAR_KEY,			RISEntry.YEAR_TAG);
		fieldMap.put(BibTeXEntryModel.ADDRESS_KEY,		RISEntry.CITY_TAG);
		
		fieldMap.put(BibTeXEntryModel.TITLE_KEY,		RISEntry.TITLE_FIRST_TAG);
		fieldMap.put(BibTeXEntryModel.SUBTITLE_KEY,		RISEntry.TITLE_SECOND_TAG);

		// year         			-> PY
		// yyyy/mm/dd/other info 	-> Y1
		// files 					-> L1
		// links 					-> UR
		// pages 					-> SP and EP
		
	}
	
	private OutputStreamWriter os;

	@Override
	public String name() {
		return "RIS";
	}

	@Override
	public String type() {
		return WebUtils.RIS_MIMETYPE;
	}

	@Override
	public void init(OutputStream os, String encoding) throws UnsupportedEncodingException {
		this.os = new OutputStreamWriter(os, encoding);
	}

	@Override
	public void write(BibTeXObject object) throws IOException {
		if(object instanceof BibTeXEntry){
			RISEntry ris = fromBibTeX((BibTeXEntry)object);
			os.write( (ris.toString() + "\r\n\r\n") );
		}
	}

	@Override
	public void writeExtras() throws IOException {
		
	}
	
	@Override
	public void finish() throws IOException {
		os.close();
	}
	
	public static RISEntry fromBibTeX(BibTeXEntry entry) throws IOException {
		RISEntry ris = new RISEntry();
		
		String risType = typeMap.get(entry.getType().getValue());
		ris.put(RISEntry.TYPE_TAG, risType);
		ris.put(RISEntry.CITEKEY_TAG, entry.getKey().getValue());
		
		if(entry.getField(RISParser.RIS_UNUSED_FIELD) != null) {
			RISEntry extra = decodeMap(entry.getField(RISParser.RIS_UNUSED_FIELD).toUserString());
			ris.merge(extra);
		}
		
		for(Key key : fieldMap.keySet()){
			copyFieldToTag(entry, ris, key, fieldMap.get(key));
		}
		
		
		formatAuthors(entry, ris);
		formatEditors(entry, ris);
		
		copyDateToTag(entry, ris, BibTeXEntryModel.CREATED_KEY, RISEntry.ACCESS_DATE_TAG);
		String dateString = convertPublicationDate(entry);
		ris.put( RISEntry.DATE_TAG, dateString );
		
		formatKeywords(entry, ris);
		formatURLs(entry, ris);
		formatFiles(entry, ris);
		formatPages(entry, ris);
		
		return ris;
	}

	private static void formatAuthors(BibTeXEntry entry, RISEntry ris)
			throws IOException {
		Value authorValue = entry.getField(BibTeXEntryModel.AUTHOR_KEY);
		if(authorValue != null) {
			String authorString = authorValue.toUserString();
			String [] authors = authorString.split(" and ");
			for(String author : authors)
				ris.put(RISEntry.AUTHOR_FIRST_TAG, author);
		}
	}

	private static void formatEditors(BibTeXEntry entry, RISEntry ris)
			throws IOException {
		Value editorValue = entry.getField(BibTeXEntryModel.AUTHOR_KEY);
		if(editorValue != null) {
			String editorString = editorValue.toUserString();
			String [] editors = editorString.split(" and ");
			for(String editor : editors)
				ris.put(RISEntry.EDITOR_TAG, editor);
		}
	}

	private static void formatPages(BibTeXEntry entry, RISEntry ris) throws IOException {
		if(entry.getField(BibTeXEntryModel.PAGES_KEY) != null){
			String [] pageRange = entry.getField(BibTeXEntryModel.PAGES_KEY).toUserString().split("\\-");
			if(pageRange.length == 1){
				ris.put(RISEntry.START_PAGE_TAG, pageRange[0]);
			}else if(pageRange.length == 2){
				ris.put(RISEntry.START_PAGE_TAG, pageRange[0]);
				ris.put(RISEntry.END_PAGE_TAG, pageRange[1]);
			}
		}
	}

	private static void formatFiles(BibTeXEntry entry, RISEntry ris) throws IOException {
		int i = 1;
		Key field = new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + i);
		while(entry.getField(field) != null){
			String path = EratosUri.parseUri(entry.getField(field).toUserString()).path();
			ris.put(RISEntry.FILE_TAG, path);
			i+=1;
			field = new Key(BibTeXEntryModel.eratosthenesFileFieldPrefix + i);
		}
	}

	private static void formatURLs(BibTeXEntry entry, RISEntry ris) throws IOException {
		int i = 1;
		Key field = new Key(BibTeXEntryModel.eratosthenesLinkFieldPrefix + i);
		while(entry.getField(field) != null){
			String url = entry.getField(field).toUserString();
			ris.put(RISEntry.URL_TAG, url);
			i+=1;
			field = new Key(BibTeXEntryModel.eratosthenesLinkFieldPrefix + i);
		}
	}

	private static void formatKeywords(BibTeXEntry entry, RISEntry ris) throws IOException {
		if(entry.getField(BibTeXEntryModel.KEYWORDS_KEY) != null){
			String[] keywords = entry.getField(BibTeXEntryModel.KEYWORDS_KEY).toUserString().split(BibTeXEntryModel.DELIMITER);
			for(String kw : keywords){
				kw = kw.trim();
				if(!kw.isEmpty()) ris.put(RISEntry.KEYWORD_TAG, kw);
			}
		}
	}

	private static String convertPublicationDate(BibTeXEntry entry) {
		String yearStr = "";
		String monthStr = "";
		
		Value year = entry.getField(BibTeXEntryModel.YEAR_KEY);
		if(year != null) yearStr = year.toUserString();
		
		Value month = entry.getField(BibTeXEntryModel.MONTH_KEY);
		if(month != null) monthStr = month.toUserString();
		if(macros.containsKey(monthStr))
			monthStr = macros.get(monthStr);
		
		return String.format("%s/%s//", yearStr, monthStr);
	}

	private static void copyDateToTag(BibTeXEntry entry, RISEntry ris, Key key, String tag) throws IOException {
		try {
			Value val = entry.getField(key);
			if(val != null){
				Date d = BibTeXEntryModel.dateFormat.parse(val.toUserString());
				String dateString = dateFormat.format(d);
				ris.put(tag, dateString);
			}
		} catch (ParseException e) {
			throw new IOException(e);
		}
	}

	private static void copyFieldToTag(BibTeXEntry entry, RISEntry ris, Key key, String tag) throws IOException {
		Value val = entry.getField(key);
		if(val != null){
			ris.put(tag, val.toUserString().replace("\n", " ").replace("\r", ""));
		}
	}

	private static RISEntry decodeMap(String userString)  {
		try{
			byte[] decoded = Base64.decode(userString);
			return (RISEntry) new ObjectInputStream(new ByteArrayInputStream(decoded)).readObject();
		}catch(Base64DecoderException e){
		}catch(IOException e){
		}catch(ClassNotFoundException e){
		}
		return new RISEntry();
	}
}
