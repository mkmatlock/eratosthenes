package com.mm.eratos.io.ris;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class RISEntry implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1351736992292720282L;
	
	public static final String CITEKEY_TAG = "CK";
	
	public static final String TYPE_TAG = "TY";
	public static final String END_TAG = "ER";
	
	public static final String AUTHOR_TAG = "AU";
	public static final String AUTHOR_FIRST_TAG = "A1";
	public static final String AUTHOR_SECOND_TAG = "A2";
	public static final String AUTHOR_THIRD_TAG = "A3";
	public static final String AUTHOR_FOURTH_TAG = "A4";
	
	public static final String TITLE_TAG = "TI";
	public static final String TITLE_FIRST_TAG = "T1";
	public static final String TITLE_SECOND_TAG = "T2";
	public static final String TITLE_THIRD_TAG = "T3";
	public static final String TITLE_FOURTH_TAG = "T4";
	
	public static final String KEYWORD_TAG = "KW";
	public static final String JOURNAL_TAG = "JO";
	public static final String PUBLISHER_TAG = "PB";
	public static final String VOLUME_TAG = "VL";
	public static final String EDITION_TAG = "ET";
	public static final String NUMBER_TAG = "IS";
	
	public static final String START_PAGE_TAG = "SP";
	public static final String END_PAGE_TAG = "EP";
	
	public static final String DATE_TAG = "Y1";
	public static final String ACCESS_DATE_TAG = "Y2";
	
	public static final String ABSTRACT_TAG = "AB";
	public static final String RESEARCH_NOTES_TAG = "N2";
	
	public static final String ISBN_TAG = "SN";
	public static final String DOI_TAG = "DO";
	public static final String URL_TAG = "UR";
	public static final String FILE_TAG = "L1";
	public static final String YEAR_TAG = "PY";

	public static final String CITY_TAG = "CY";
	
	public static final String EDITOR_TAG = "ED";
	
	private String type;
	private Map<String, List<String>> values;
	private boolean closed;
	
	public RISEntry(){
		type = "";
		closed = false;
		values = new HashMap<String, List<String>>();
	}

	public void merge(RISEntry extra) throws IOException {
		for(String k : extra.keySet()){
			for(String v : extra.get(k)){
				put(k, v);
			}
		}
	}
	
	public boolean containsKey(String key){
		return values.containsKey(key);
	}
	
	public List<String> get(String key){
		if(values.containsKey(key))
			return values.get(key);
		return Arrays.asList();
	}

	public Set<String> keySet() {
		return values.keySet();
	}

	public String getType() {
		return type;
	}
	
	public void remove(String tag) {
		values.remove(tag);
	}
	
	public void put(String key, String value) throws IOException{
		if(closed)
			throw new IOException("Entry is closed");
		
		if(key.equals(TYPE_TAG)){
			type = value;
		}else{
			if(!values.containsKey(key))
				values.put(key, new ArrayList<String>());
			
			values.get(key).add(value);
		}
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(Locale.US, "%s  - %s\r\n", TYPE_TAG, type));
		
		for(String key : values.keySet())
			for(String val : values.get(key))
				sb.append(String.format(Locale.US, "%s  - %s\r\n", key, val));
	
		sb.append(String.format(Locale.US, "%s  - \r\n", END_TAG));
		
		return sb.toString();
	}
	
	public static RISEntry parse(InputStream content) throws IOException {
		RISEntry ris = new RISEntry();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(content));
		String line = null;
		while((line = br.readLine()) != null){
			int hypheni = line.indexOf('-');
			if(hypheni > -1){
				String key = line.substring(0, hypheni).trim();
				String value = line.substring(hypheni+1).trim();
				ris.put(key, value);
			}
		}
		return ris;
	}

	public void close() {
		closed=true;
	}
}