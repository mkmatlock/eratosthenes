package com.mm.eratos.io;

import java.io.IOException;
import java.io.OutputStream;

import org.jbibtex.BibTeXObject;

public interface IWriter {
	public String name();
	public String type();
	public void init(OutputStream os, String encoding) throws IOException;
	public void write(BibTeXObject object) throws IOException;
	public void writeExtras() throws IOException;
	public void finish() throws IOException;
}
