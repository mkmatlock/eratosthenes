package com.mm.eratos.io.endnote;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jbibtex.BibTeXObject;
import org.jbibtex.ParseException;
import org.xmlpull.v1.XmlPullParser;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.io.IParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.WebUtils;

public class EndNoteParser implements IParser {
	static Map<String, List<String>> fieldMap = new HashMap<String, List<String>>();
	
	static {
		fieldMap.put("ref-type", Arrays.asList("xml", "records", "record", "ref-type"));
		
		fieldMap.put(BibTeXEntryModel.TITLE_KEY.getValue(), Arrays.asList("xml", "records", "record", "titles", "title"));
		fieldMap.put(BibTeXEntryModel.AUTHOR_KEY.getValue(), Arrays.asList("xml", "records", "record", "contributors", "authors", "author"));
		fieldMap.put(BibTeXEntryModel.EDITOR_KEY.getValue(), Arrays.asList("xml", "records", "record", "contributors", "editors", "editor"));
		
		fieldMap.put(BibTeXEntryModel.JOURNAL_KEY.getValue(), Arrays.asList("xml", "records", "record", "periodical", "full-title"));
		
		fieldMap.put(BibTeXEntryModel.PAGES_KEY.getValue(), Arrays.asList("xml", "records", "record", "pages"));
		fieldMap.put(BibTeXEntryModel.VOLUME_KEY.getValue(), Arrays.asList("xml", "records", "record", "volume"));
		fieldMap.put(BibTeXEntryModel.NUMBER_KEY.getValue(), Arrays.asList("xml", "records", "record", "number"));
		fieldMap.put(BibTeXEntryModel.ISBN_KEY.getValue(), Arrays.asList("xml", "records", "record", "isbn"));
		fieldMap.put(BibTeXEntryModel.YEAR_KEY.getValue(), Arrays.asList("xml", "records", "record", "dates", "year"));
		fieldMap.put(BibTeXEntryModel.MONTH_KEY.getValue(), Arrays.asList("xml", "records", "record", "dates", "pub-dates", "date"));
		fieldMap.put(BibTeXEntryModel.COMMENTS_KEY.getValue(), Arrays.asList("xml", "records", "record", "notes"));
		
		fieldMap.put(BibTeXEntryModel.KEYWORDS_KEY.getValue(), Arrays.asList("xml", "records", "record", "keywords", "keyword"));
		fieldMap.put(BibTeXEntryModel.GROUPS_KEY.getValue(), Arrays.asList("xml", "records", "record", "groups", "group"));
		fieldMap.put("label", Arrays.asList("xml", "records", "record", "label"));
	}
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd", Locale.US);
	
	private int total;
	private int processed;
	private List<Throwable> errors;
	private IXMLParser parser;
	private LinkedList<String> tagStack;
	private EndNoteEntry record;
	private StringBuffer collectedText;
	private CiteKeyFormatter citeKeyFormatter;
	private BibTeXHelper helper;

	private InputStreamReader input;
	
	
	@Override
	public String name() {
		return "EndNote XML";
	}

	@Override
	public String type() {
		return WebUtils.ENDNOTE_MIMETPYE;
	}

	@Override
	public void init(InputStream is, String encoding, int total) throws IOException, ParseException {
		EratosSettings settingsManager = EratosApplication.getApplication().getSettingsManager();
		
		this.processed = 0;
		this.total = total;
		this.errors = new ArrayList<Throwable>();
		this.tagStack = new LinkedList<String>();
		
		this.citeKeyFormatter = settingsManager.getCiteKeyFormatter();
		this.helper = settingsManager.getBibTeXHelper();
		
		parser = IXMLParser.getXMLParser();
		input = new InputStreamReader(is, encoding);
		parser.setInput(input);
	}

	@Override
	public long processed() {
		return processed;
	}

	@Override
	public long total() {
		return total;
	}
	

	@Override
	public boolean next() throws IOException, ParseException {
		while(true){
			int event = parser.next();
			
			if(event == XmlPullParser.END_DOCUMENT)
				return false;
			
			if(event == XmlPullParser.TEXT)
				processText();
			
			if(event == XmlPullParser.START_TAG)
				processStartTag();
			
			if(event == XmlPullParser.END_TAG && processEndTag())
				return true;
		}
	}
	
	public boolean checkStack(List<String> tags){
		if(tagStack.size() != tags.size())
			return false;
		
		int i = 0;
		for(String tag : tagStack){
			if(!tags.get(i).equalsIgnoreCase(tag))
				return false;
			i++;
		}
		
		return true;
	}
	
	public boolean checkStack(String ... tags){
		if(tagStack.size() != tags.length)
			return false;
		
		int i = 0;
		for(String tag : tagStack){
			if(!tags[i].equalsIgnoreCase(tag))
				return false;
			i++;
		}
		
		return true;
	}

	private boolean processEndTag() throws IOException {
		String tagName = parser.getName();
		
		if( checkStack("xml", "records", "record") ){
			tagStack.removeLast();
			return true;
		}
		
		if( checkStack("xml", "records", "record", "date-added"))
			processDate("date-added", endText());
		if( checkStack("xml", "records", "record", "date-modified"))
			processDate("date-modified", endText());
		
		if( checkStack("xml", "records", "record", "urls", "pdf-urls", "url") )
			processFile(endText());
		if( checkStack("xml", "records", "record", "urls", "related-urls", "url") )
			processUrl(endText());
		
		for(String key : fieldMap.keySet()){
			List<String> stack = fieldMap.get(key);
			if(checkStack(stack))
				record.put(key, endText());
		}

		String expected = tagStack.removeLast();
		if(!tagName.equalsIgnoreCase(expected))
			throw new IOException("Unexpected end tag: " + tagName + " expected: " + expected);
		
		return false;
	}

	private void processUrl(String url) {
		EratosUri uri = EratosUri.parseUri(url);
		if(uri != null)
			record.put("links", uri.toString());
	}

	private void processFile(String filePath) {
		EratosUri uri = EratosUri.makeUri(EratosUri.LIBRARY_LOCAL_PROTOCOL, filePath);
		if(uri != null && !filePath.trim().isEmpty())
			record.put("files", uri.toString());
	}

	private void processDate(String key, String text) {
		try {
			Date date = dateFormat.parse(text);
			String dateStr = BibTeXEntryModel.dateFormat.format(date);
			record.put(key, dateStr);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
	}

	private void processStartTag() {
		String tagName = parser.getName();
		tagStack.add(tagName);
		
		if( checkStack("xml", "records", "record") ){
			record = new EndNoteEntry();
		}
		
		if( checkStack("xml", "records", "record", "date-added"))
			beginText();
		if( checkStack("xml", "records", "record", "date-modified"))
			beginText();

		if( checkStack("xml", "records", "record", "urls", "pdf-urls", "url") )
			beginText();
		if( checkStack("xml", "records", "record", "urls", "related-urls", "url") )
			beginText();
		
		for(String key : fieldMap.keySet()){
			List<String> stack = fieldMap.get(key);
			if(checkStack(stack)){
				beginText();
				break;
			}
		}
	}

	private void processText() {
		if(collectedText != null)
			collectedText.append(parser.getText());
	}
	
	private void beginText() {
		collectedText = new StringBuffer();
	}
	
	private String endText() {
		String result = collectedText.toString();
		collectedText = null;
		return result;
	}

	@Override
	public BibTeXObject raw() {
		return null;
	}

	@Override
	public BibTeXObject get() throws IOException, ParseException {
		return record.toBibTeX(citeKeyFormatter, helper);
	}

	@Override
	public void extras(BibTeXContentProviderAdapter bibtexAdapter, BibTeXEntryModelFactory modelFactory, ProgressCallback progressHandler) throws IOException {
		
	}

	@Override
	public String errors() throws IOException {
		String errorString = "";
		for(Throwable t : errors)
			errorString += t.getMessage() +"\n";
		return errorString;
	}
	
	@Override
	public void finish() throws IOException {
		input.close();
	}

}
