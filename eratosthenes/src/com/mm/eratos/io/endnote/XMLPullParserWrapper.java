package com.mm.eratos.io.endnote;

import java.io.IOException;
import java.io.InputStreamReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class XMLPullParserWrapper extends IXMLParser {

	private XmlPullParser pullParser;

	public XMLPullParserWrapper() throws IOException {
		try {
			pullParser = XmlPullParserFactory.newInstance().newPullParser();
		} catch (XmlPullParserException e) {
			throw new IOException(e);
		}
	}
	
	@Override
	public IXMLParser newInstance() throws IOException {
		return new XMLPullParserWrapper();
	}

	@Override
	public void setInput(InputStreamReader reader) throws IOException {
		try {
			pullParser.setInput(reader);
		} catch (XmlPullParserException e) {
			throw new IOException(e);
		}
	}

	@Override
	public int next() throws IOException {
		try {
			return pullParser.next();
		} catch (XmlPullParserException e) {
			throw new IOException(e);
		}
	}

	@Override
	public String getName() {
		return pullParser.getName();
	}
	
	@Override
	public String getText() {
		return pullParser.getText();
	}
}
