package com.mm.eratos.io.endnote;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.Value;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.io.IWriter;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebUtils;

public class EndNoteWriter implements IWriter {
	private static Map<Key, String> topLevelFields;
	static{
		topLevelFields = new HashMap<Key, String>();
		
		topLevelFields.put(BibTeXEntryModel.PAGES_KEY, "pages");
		topLevelFields.put(BibTeXEntryModel.NUMBER_KEY, "number");
		topLevelFields.put(BibTeXEntryModel.VOLUME_KEY, "volume");
		topLevelFields.put(BibTeXEntryModel.ISBN_KEY, "isbn");
		topLevelFields.put(BibTeXEntryModel.DOI_KEY, "doi");
		
		topLevelFields.put(BibTeXEntryModel.COMMENTS_KEY, "notes");
	}
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd", Locale.US);
	
	private int recordNum;
	private OutputStreamWriter writer;

	
	@Override
	public String name() {
		return "EndNote XML";
	}

	@Override
	public String type() {
		return WebUtils.ENDNOTE_MIMETPYE;
	}

	@Override
	public void init(OutputStream os, String encoding) throws IOException {
		recordNum = 0;
		writer = new OutputStreamWriter(os, encoding);
		
		writer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		writer.append("<xml>\n");
		writer.append("<records>\n");
	}

	@Override
	public void write(BibTeXObject object) throws IOException {
		if(object instanceof BibTeXEntry){
			recordNum++;
			BibTeXEntry entry = (BibTeXEntry)object;
			writer.append( generateXML(entry) );
		}
	}

	private String generateXML(BibTeXEntry entry) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("<record>");
		
		int refType = getRefType(entry);
		sb.append( String.format( Locale.US, "<rec-number>%d</rec-number>", recordNum ) );
		sb.append( String.format( Locale.US, "<ref-type>%d</ref-type>", refType ) );
		sb.append( String.format( Locale.US, "<label>%s</label>", entry.getKey().getValue() ) );
		
		formatTitles(entry, sb);
		
		sb.append("<contributors>");
		formatAuthors(entry, sb);
		formatEditors(entry, sb);
		sb.append("</contributors>");
		
		formatPeriodical(entry, sb);
		
		formatKeywords(entry, sb);
		formatGroups(entry, sb);
		formatDates(entry, sb);
		
		formatTimestamps(entry, sb);
		
		sb.append("<urls>");
		formatUrls(entry, sb);
		formatFiles(entry, sb);
		sb.append("</urls>");
		
		formatTopLevelFields(entry, sb);
		
		
		sb.append("</record>\n");
		
		return sb.toString();
	}

	private void formatTimestamps(BibTeXEntry entry, StringBuffer sb) {
		try {
			Value created = entry.getField(BibTeXEntryModel.CREATED_KEY);
			Date date = BibTeXEntryModel.dateFormat.parse(created.toUserString());
			String dateStr = prepareXMLText( dateFormat.format(date) );
			
			sb.append("<date-added>");
			sb.append( dateStr );
			sb.append("</date-added>");
		} catch (ParseException e) {
		} catch (NullPointerException e){
		}
		
		try {
			Value modified = entry.getField(BibTeXEntryModel.MODIFIED_KEY);
			Date date = BibTeXEntryModel.dateFormat.parse(modified.toUserString());
			String dateStr = prepareXMLText( dateFormat.format(date) );
			
			sb.append("<date-modified>");
			sb.append( dateStr );
			sb.append("</date-modified>");
		} catch (ParseException e) {
		} catch (NullPointerException e){
		}
		
	}

	private void formatPeriodical(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<periodical>");
		writeField(entry, "full-title", BibTeXEntryModel.JOURNAL_KEY, sb);
		sb.append("</periodical>");
	}

	private void formatTopLevelFields(BibTeXEntry entry, StringBuffer sb) {
		for(Key k : topLevelFields.keySet()){
			writeField(entry, topLevelFields.get(k), k, sb);
		}
	}

	private void formatTitles(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<titles>");

		writeField(entry, "title", BibTeXEntryModel.TITLE_KEY, sb);
		writeField(entry, "secondary-title", BibTeXEntryModel.SUBTITLE_KEY, sb);
		
		sb.append("</titles>");
	}

	private void formatDates(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<dates>");
		
		writeField(entry, "year", BibTeXEntryModel.YEAR_KEY, sb);
		
		sb.append("<pub-dates>");
		
		writeField(entry, "date", BibTeXEntryModel.MONTH_KEY, sb);
		
		sb.append("</pub-dates>");
		sb.append("</dates>");
	}

	private void formatFiles(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<pdf-urls>");
		
		for(Key k : entry.getFields().keySet()){
			String kStr = k.getValue();
			if(kStr.startsWith(BibTeXEntryModel.eratosthenesFileFieldPrefix)){
				Value fileField = entry.getField(k);
				EratosUri uri = EratosUri.parseUri(fileField.toUserString());
				
				if(uri != null) {
					sb.append("<url>");
					sb.append( prepareXMLText(uri.path()) );
					sb.append("</url>");
				}
			}
		}
		
		sb.append("</pdf-urls>");
	}

	public void writeField(BibTeXEntry entry, String tag, Key key, StringBuffer sb){
		Value field = entry.getField(key);
		if(field != null && !field.toUserString().isEmpty()){
			sb.append("<"+tag+">");
			sb.append( prepareXMLText(field.toUserString()) );
			sb.append("</"+tag+">");
		}
	}

	private void formatUrls(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<related-urls>");
		
		writeField(entry, "url", BibTeXEntryModel.URL_KEY, sb);
		
		for(Key k : entry.getFields().keySet()){
			String kStr = k.getValue();
			if(kStr.startsWith(BibTeXEntryModel.eratosthenesLinkFieldPrefix)){
				writeField(entry, "url", k, sb);
			}
		}
		
		sb.append("</related-urls>");
	}

	private void formatGroups(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<groups>");
		
		Value groupField = entry.getField(BibTeXEntryModel.GROUPS_KEY);
		if(groupField != null && !groupField.toUserString().isEmpty()){
			String []groups = groupField.toUserString().split(BibTeXEntryModel.DELIMITER);
			for(String group : groups){
				sb.append("<group>");
				sb.append( prepareXMLText(group) );
				sb.append("</group>");
			}
		}
		
		sb.append("</groups>");
	}

	private void formatKeywords(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<keywords>");
		
		Value keywordField = entry.getField(BibTeXEntryModel.KEYWORDS_KEY);
		if(keywordField != null && !keywordField.toUserString().isEmpty()){
			String []keywords = keywordField.toUserString().split(BibTeXEntryModel.DELIMITER);
			for(String keyword : keywords){
				sb.append("<keyword>");
				sb.append( prepareXMLText(keyword) );
				sb.append("</keyword>");
			}
		}
		
		sb.append("</keywords>");
	}

	private int getRefType(BibTeXEntry entry) {
		String bibType = entry.getType().getValue();
		
		if(bibType.equalsIgnoreCase("article"))
			return 17;
		if(bibType.equalsIgnoreCase("inproceedings"))
			return 47;
		if(bibType.equalsIgnoreCase("incollection"))
			return 5;
		
		return 0;
	}

	private void formatEditors(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<editors>");
		
		Value editorField = entry.getField(BibTeXEntryModel.EDITOR_KEY);
		if(editorField != null && !editorField.toUserString().isEmpty()){
			String []editors = editorField.toUserString().split(" and ");
			for(String editor : editors){
				sb.append("<editor>");
				sb.append( prepareXMLText(editor) );
				sb.append("</editor>");
			}
		}
		sb.append("</editors>");
	}

	private void formatAuthors(BibTeXEntry entry, StringBuffer sb) {
		sb.append("<authors>");
		
		Value authorField = entry.getField(BibTeXEntryModel.AUTHOR_KEY);
		if(authorField != null && !authorField.toUserString().isEmpty()){
			String []authors = authorField.toUserString().split(" and ");
			for(String author : authors){
				sb.append("<author>");
				sb.append( prepareXMLText(author) );
				sb.append("</author>");
			}
		}
		sb.append("</authors>");
	}
	
	private String prepareXMLText(String text) {
		return text.replace("\r", "\\n").replace("\n", "\\n");
	}

	@Override
	public void writeExtras() throws IOException {
	}

	@Override
	public void finish() throws IOException {
		writer.append("</records>\n");
		writer.append("</xml>\n");
		writer.close();
	}

}
