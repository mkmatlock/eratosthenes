package com.mm.eratos.io.endnote;

import java.io.IOException;
import java.io.InputStreamReader;

public abstract class IXMLParser {
	static IXMLParser xmlParser;
	
	public abstract IXMLParser newInstance() throws IOException;
	public abstract void setInput(InputStreamReader inputStreamReader) throws IOException;
	public abstract int next() throws IOException;
	public abstract String getName();
	public abstract String getText();
	
	public static IXMLParser getXMLParser() throws IOException {
		if(xmlParser == null)
			xmlParser = new XMLPullParserWrapper();
		return xmlParser.newInstance();
	}

}
