package com.mm.eratos.io.endnote;

import java.io.IOException;
import java.util.HashSet;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.utils.MultiHashMap;
import com.mm.eratos.utils.StringUtils;

public class EndNoteEntry {
	MultiHashMap<String, String> valueMap;
	
	public EndNoteEntry(){
		valueMap = new MultiHashMap<String, String>();
	}
	
	public void put(String key, String value){
		valueMap.put(key, value);
	}
	
	public String pop(String key){
		return valueMap.remove(key).get(0);
	}
	
	public String pop(String key, String delimiter){
		return StringUtils.join(delimiter, valueMap.remove(key));
	}
	
	public BibTeXObject toBibTeX(CiteKeyFormatter citeKeyFormatter, BibTeXHelper helper) throws IOException, ParseException {
		int refType = Integer.parseInt( pop("ref-type") );
		
		Key bibType = BibTeXEntry.TYPE_MISC;
		switch(refType){
		case 17:
			bibType = BibTeXEntry.TYPE_ARTICLE;
		case 47:
			bibType = BibTeXEntry.TYPE_INPROCEEDINGS;
		case 5:
			bibType = BibTeXEntry.TYPE_INCOLLECTION;
		}
		
		String citeKey = "tempKey";
		boolean needCiteKey=true;
		if(valueMap.containsKey("label")){
			citeKey = pop("label");
			needCiteKey=false;
		}
		
		BibTeXEntry entry = new BibTeXEntry(bibType, new Key(citeKey));
		
		if( valueMap.containsKey("files") ){
			int i = 1;
			for(String file : valueMap.remove("files")){
				stringValue(entry, BibTeXEntryModel.eratosthenesFileFieldPrefix + i, file);
				i+=1;
			}
		}
		
		if( valueMap.containsKey("links") ){
			int i = 1;
			for(String link : valueMap.remove("links")){
				stringValue(entry, BibTeXEntryModel.eratosthenesLinkFieldPrefix + i, link);
				i+=1;
			}
		}
		
		if( valueMap.containsKey("groups") )
			stringValue(entry, "groups", pop("keywords", BibTeXEntryModel.DELIMITER));
		if( valueMap.containsKey("keywords") )
			stringValue(entry, "keywords", pop("keywords", BibTeXEntryModel.DELIMITER));
		if( valueMap.containsKey("author") )
			stringValue(entry, "author", pop("author", " and "));
		if( valueMap.containsKey("editor") )
			stringValue(entry, "editor", pop("editor", " and "));
		
		HashSet<String> keys = new HashSet<String>(valueMap.keySet());
		for(String key : keys)
			stringValue(entry, key, pop(key));
		
		if(needCiteKey){
			String nCiteKey = citeKeyFormatter.format(entry, helper);
			BibTeXEntry newEntry = new BibTeXEntry(bibType, new Key(nCiteKey));
			copyEntry(entry, newEntry);
			return newEntry;
		}
		
		return entry;
	}

	private void copyEntry(BibTeXEntry entry, BibTeXEntry newEntry) {
		for(Key key : entry.getFields().keySet())
			newEntry.addField(key, entry.getField(key));
	}

	private void stringValue(BibTeXEntry entry, String key, String value) {
		entry.addField(new Key(key), new StringValue(value, Style.BRACED));
	}
	
	@Override
	public String toString() {
		return valueMap.toString();
	}
}
