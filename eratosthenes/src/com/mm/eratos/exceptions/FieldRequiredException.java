package com.mm.eratos.exceptions;

public class FieldRequiredException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public FieldRequiredException(String msg){
		super(msg);
	}
	public FieldRequiredException(String type, String field){
		super(String.format("Document type: %s requires field %s", type, field));
	}
}
