package com.mm.eratos.exceptions;

public class NoSuchEntryException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public NoSuchEntryException(String format) {
		super(format);
	}
}
