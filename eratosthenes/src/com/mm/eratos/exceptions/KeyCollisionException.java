package com.mm.eratos.exceptions;

public class KeyCollisionException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public KeyCollisionException(String format) {
		super(format);
	}
}
