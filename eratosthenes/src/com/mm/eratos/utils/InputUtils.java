package com.mm.eratos.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mm.eratos.io.IOFactory;
import com.mm.eratos.model.EratosUri;

public class InputUtils {
	public static OnClickListener doNothingListener = new OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		}
	};
	
	
	public static void listChoiceDialog(Context context, CharSequence title, Collection<? extends CharSequence> items, final IStringResultListener resultListener) {
		listChoiceDialog(context, title, items.toArray(new CharSequence[]{}), resultListener);
	}
	
	public static void listChoiceDialog(Context context, CharSequence title, final CharSequence[] items, final IStringResultListener resultListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setTitle(title)
	           .setItems(items, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					resultListener.onResult(items[which].toString(), dialog);
				}
			})
	           .show();
	}
	
	public static void mapChoiceDialog(Context context, CharSequence title, final Map<String, String> itemMap, final IStringResultListener resultListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		final String[] items = itemMap.keySet().toArray(new String[]{});
	    builder.setTitle(title)
	           .setItems(items, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String k = items[which];
						String v = itemMap.get(k);
						resultListener.onResult(v, dialog);
					}
				})
	           .show();
	}
	
	public static void listChoiceCheckDialog(final Context mContext, CharSequence title, CharSequence checkText, final List<String> handlers, final IStringCheckboxResultListener listener) {
		View view = LayoutInflater.from(mContext).inflate(com.mm.eratos.R.layout.list_choice_check_dialog, null);
		
		final ListView listView = (ListView) view.findViewById(com.mm.eratos.R.id.list_view);
		final CheckBox checkBox = (CheckBox) view.findViewById(com.mm.eratos.R.id.check_box);
		checkBox.setText(checkText);
		
		ArrayAdapter<String> items = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_activated_1, android.R.id.text1, handlers);
		listView.setAdapter(items);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_Dialog);
		final AlertDialog dialog = builder
			.setTitle(title)
			.setView(view)
			.setNegativeButton("Cancel", doNothingListener)
			.setPositiveButton("Choose", null).show();
		
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int pos = listView.getCheckedItemPosition();
				if( pos != ListView.INVALID_POSITION ) {
					String result = (String) handlers.get(pos);
					boolean checked = checkBox.isChecked();
					listener.onResult(result, checked, dialog);
					dialog.dismiss();
				}else{
					NotificationUtils.shortToast(mContext, "Choose a search handler!");
				}
			}
		});
	}
	
	public static void multimapChoiceDialog(final Context mContext, CharSequence title, CharSequence label1, List<String> list1, CharSequence label2, List<String> list2, final IStringArrayResultListener listener) {
		View view = LayoutInflater.from(mContext).inflate(com.mm.eratos.R.layout.multi_list_choice_dialog, null);
		
		TextView labelView1 = (TextView) view.findViewById(com.mm.eratos.R.id.label1);
		TextView labelView2 = (TextView) view.findViewById(com.mm.eratos.R.id.label2);
		final Spinner spinner1 = (Spinner) view.findViewById(com.mm.eratos.R.id.spinner1);
		final Spinner spinner2 = (Spinner) view.findViewById(com.mm.eratos.R.id.spinner2);
		
		labelView1.setText(label1);
		labelView2.setText(label2);
		
		ArrayAdapter<String> items1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_activated_1, android.R.id.text1, list1);
		spinner1.setAdapter(items1);
		
		ArrayAdapter<String> items2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_activated_1, android.R.id.text1, list2);
		spinner2.setAdapter(items2);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_Dialog);
		final AlertDialog dialog = builder
			.setTitle(title)
			.setView(view)
			.setPositiveButton("Ok", null)
			.show();

		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String r1 = spinner1.getSelectedItem().toString();
				String r2 = spinner2.getSelectedItem().toString();
				
				listener.onResult(dialog, r1, r2);
				dialog.dismiss();
			}
		});
	}
	

	public static void chooseLibraryType(Context mContext, EratosUri uri, final IStringArrayResultListener resultListener){
		String mimeType = WebUtils.getMimeType(uri);
		
		final Map<String, String> mimeTypes = IOFactory.getSupportedTypes(mimeType);
		ArrayList<String> types = new ArrayList<String>(mimeTypes.keySet());
		Collections.sort(types);
		
		List<String> encodings = IOFactory.getSupportedFileEncodings();
		
		multimapChoiceDialog(mContext, "File Settings", "File Type: ", types, "Encoding: ", encodings, new IStringArrayResultListener() {
			public boolean onResult(DialogInterface dialog, String... results) {
				String r1 = mimeTypes.get(results[0]);
				String r2 = results[1];
				return resultListener.onResult(dialog, r1, r2);
			}
		});
	}

}
