package com.mm.eratos.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;

public class CaseInsensitiveHashSet extends HashSet<String> {

	public CaseInsensitiveHashSet() {
		super();
	}
	
	public CaseInsensitiveHashSet(Collection<String> collection) {
		super();
		for(String item : collection)
			add(item);
	}


	@Override
	public boolean add(String item) {
		item = item.toLowerCase(Locale.US);
		return super.add(item);
	}

	@Override
	public boolean contains(Object item) {
		if(item instanceof String)
			item = ((String)item).toLowerCase(Locale.US);
		return super.contains(item);
	}

	@Override
	public boolean remove(Object item) {
		if(item instanceof String)
			item = ((String)item).toLowerCase(Locale.US);
		return super.remove(item);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
