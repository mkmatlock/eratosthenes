package com.mm.eratos.utils;

import android.content.DialogInterface;

public interface IStringResultListener {
	public boolean onResult(String result, DialogInterface dialog);
}
