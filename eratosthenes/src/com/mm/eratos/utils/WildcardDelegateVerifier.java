package com.mm.eratos.utils;

import javax.net.ssl.SSLException;

import org.apache.http.conn.ssl.AbstractVerifier;
import org.apache.http.conn.ssl.X509HostnameVerifier;

class WildcardDelegateVerifier extends AbstractVerifier {

    private final X509HostnameVerifier delegate;

    public WildcardDelegateVerifier(final X509HostnameVerifier delegate) {
        this.delegate = delegate;
    }

    @Override
    public void verify(String host, String[] cns, String[] subjectAlts)
                throws SSLException {
        boolean ok = false;
        try {
            delegate.verify(host, cns, subjectAlts);
        } catch (SSLException e) {
            for (String cn : cns) {
                if (cn.startsWith("*.")) {
                    try {
                          delegate.verify(host, new String[] { 
                                cn.substring(2) }, subjectAlts);
                          ok = true;
                    } catch (SSLException e1) {
                    	System.out.println("SSL Error: " + e1.getMessage());
                    }
                }
                if (cn.startsWith("www.google.com")) {
                	try {
                        delegate.verify(host, new String[] { 
                              "scholar.google.com" }, subjectAlts);
                        ok = true;
                  } catch (SSLException e1) { 
                	  System.out.println("SSL Error: " + e1.getMessage());
                  }
                }
            }
            if(!ok) throw e;
        }
    }
}