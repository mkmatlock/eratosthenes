package com.mm.eratos.utils;

import android.content.DialogInterface;

public interface IStringCheckboxResultListener {
	public boolean onResult(String result, boolean checked, DialogInterface dialog);
}