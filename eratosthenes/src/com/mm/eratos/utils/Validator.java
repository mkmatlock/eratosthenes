package com.mm.eratos.utils;

import java.util.Locale;

public class Validator {
	public static boolean isDOI(String doi){
		return doi.matches("10[.][0-9]{4,}/.*");
	}
	

	public static boolean isISBN(String isbn) {
		isbn = isbn.replace("-", "");
		try{
			int checksum = -1;
			if(isbn.length() == 10)
				checksum = isbn10checksum(isbn);
			else if(isbn.length() == 13)
				checksum = isbn13checksum(isbn);
			return checksum == Integer.parseInt(isbn.substring(isbn.length()-1,isbn.length()));
		}catch(NumberFormatException nfe){
		}
		return false;
	}

	private static int isbn10checksum(String isbn) {
		int checksum = 0;
		for(int i = 0; i < isbn.length()-1; i++){
			checksum += (10-i)*Integer.parseInt(isbn.substring(i, i+1));
		}
		checksum = checksum % 11;
		return 11 - checksum;
	}

	private static int isbn13checksum(String isbn) {
		int checksum = 0;
		for(int i = 0; i < isbn.length()-1; i++){
			int mul = i % 2 == 0 ? 1 : 3;
			checksum += mul * Integer.parseInt(isbn.substring(i,i+1));
		}
		return (10 - (checksum % 10)) % 10;
	}
	
	private static String isbn10ToIsbn13(String isbn10) {
		String isbn13 = "978" + isbn10;
		return isbn13.substring(0, isbn13.length()-1) + isbn13checksum(isbn13);
	}


	public static boolean isPMID(String pmid) {
		return pmid.matches("[0-9]{1,8}(\\.[0-9]+)?");
	}
	
	public static String ISBNtoDOI(String isbn){
		if(isbn.length()==10)
			isbn = isbn10ToIsbn13(isbn);
		isbn = isbn.replace("-", "");
		
		String country = isbn.substring(0,3);
		String group = isbn.substring(3,4);
		String cd = isbn.substring(12,13);
		
		int pc1 = Integer.parseInt(isbn.substring(4,6));
		int pc2 = Integer.parseInt(isbn.substring(4,7));
		int pc3 = Integer.parseInt(isbn.substring(4,8));
		
		String pubcode = "";
		if("0".equals(group)){
			if(0 <= pc1 && pc1 <= 19){
				pubcode = isbn.substring(4,6);
			}else if(20 <= pc1 && pc1 <= 69){
				pubcode = isbn.substring(4,7);
			}else if(70 <= pc1 && pc1 <= 84){
				pubcode = isbn.substring(4,9);
			}else if(85 <= pc1 && pc1 <= 89){
				pubcode = isbn.substring(4,10);
			}else if(90 <= pc1 && pc1 <= 94){
				pubcode = isbn.substring(4,11);
			}  
		}else if("1".equals(group)){
			if(0 <= pc1 && pc1 <= 9){
				pubcode = isbn.substring(4,6);
			}else if(10 <= pc1 && pc1 <= 39){
				pubcode = isbn.substring(4,7);
			}else if(40 <= pc1 && pc1 <= 54){
				pubcode = isbn.substring(4,9);
			}else if(55 <= pc1 && pc3 <= 8697){
				pubcode = isbn.substring(4,10);
			}else if(8698 <= pc3 && pc3 <= 9989){
				pubcode = isbn.substring(4,10);
			}else if(999 == pc2){
				pubcode = isbn.substring(4,11);
			}
		}
		
		String itemcode = isbn.substring(4 + pubcode.length(), 12);
		return String.format(Locale.US, "10.%s.%s%s/%s%s", country, group, pubcode, itemcode, cd);
	}

	public static boolean isArXiV(String result) {
		result = result.trim();
		return result.matches("[0-9]+\\.[0-9]+");
	}
}
