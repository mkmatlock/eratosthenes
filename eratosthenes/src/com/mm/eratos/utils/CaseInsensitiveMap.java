package com.mm.eratos.utils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class CaseInsensitiveMap<T> extends HashMap<String, T> {
	Map<String, String> originalCase = new HashMap<String, String>();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Set<String> originalKeys(){
		return new HashSet<String>(originalCase.values());
	}
	
	@Override
    public T put(String key, T value) {
       String lowerCase = key.toLowerCase(Locale.US);
       originalCase.put(lowerCase, key);
       return super.put(lowerCase, value);
    }

	@Override
    public T get(Object key) {
		if(key instanceof String)
			key = ((String)key).toLowerCase(Locale.US);
       return super.get(key);
    }
    
    @Override
    public boolean containsKey(Object key){
    	if(key instanceof String)
    		key = ((String)key).toLowerCase(Locale.US);
		return super.containsKey(key);
    }
    
    @Override
	public T remove(Object key) {
		if(key instanceof String)
			key = ((String)key).toLowerCase(Locale.US);
		originalCase.remove(key);
		return super.remove(key);
	}

}