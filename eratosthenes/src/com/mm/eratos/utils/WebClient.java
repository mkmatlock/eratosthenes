package com.mm.eratos.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.accounts.Account;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.ezproxy.EZProxyAccountManager;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;

public class WebClient {
	private static final String USER_AGENT_HEADER = "User-Agent";
	
	public static boolean checkConnectionState = true;
	public static final String default_user_agent = "Mozilla/5.0 (Linux; Android 4.4; Nexus 4 Build/KRT16H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36";
	
	public static interface DownloadListener{
		public void progress(long downloaded, long fileSize);
	}
	
	HttpClient mHttpClient;
	private HttpContext mLocalContext;
	private HttpParams mHttpParams;
	private String user_agent;

	private EratosUri proxyUri=null;
	private boolean initialized=false;
	private Account ezProxyAccount;

	private boolean ezProxyInitialized;
	protected boolean allowEZProxy;

	private EratosLogger logger;
	
	public WebClient(){
		this.ezProxyInitialized = false;
		this.ezProxyAccount = null;
		this.user_agent = default_user_agent;
	}
	
	public WebClient(Account ezProxyAccount){
		this.ezProxyInitialized = false;
		this.ezProxyAccount = ezProxyAccount;
		this.user_agent = default_user_agent;
	}
	
	public void setUserAgent(String user_agent){
		this.user_agent = user_agent;
	}
	
	public static WebClient newInstance() {
		return new WebClient();
	}

	public static WebClient newInstance(Account ezProxyAccount) {
		return new WebClient(ezProxyAccount);
	}
	
	public void initHttpClient() throws IOException {
		EratosApplication app = EratosApplication.getApplication();
		EratosSettings settingsManager = app.getSettingsManager();
		logger = app.getLogManager().getLogger(WebClient.class);
		
		if(checkConnectionState && !(app.isConnected() && WebUtils.checkNetworkState("http://www.google.com")))
			throw new OfflineException();
		
		mHttpClient = new DefaultHttpClient();
		
		SSLSocketFactory sslSocketFactory = (SSLSocketFactory) mHttpClient
	            .getConnectionManager().getSchemeRegistry().getScheme("https")
	            .getSocketFactory();
	    final X509HostnameVerifier delegate = sslSocketFactory.getHostnameVerifier();
	    if(!(delegate instanceof WildcardDelegateVerifier))
	        sslSocketFactory.setHostnameVerifier(new WildcardDelegateVerifier(delegate));
		
		mHttpParams = mHttpClient.getParams();
		HttpClientParams.setRedirecting(mHttpParams, true);
		HttpClientParams.setCookiePolicy(mHttpParams, CookiePolicy.BROWSER_COMPATIBILITY);
		
		mHttpParams.setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true); 
		
		CookieStore cookieStore = new BasicCookieStore();
		mLocalContext = new BasicHttpContext();
	    mLocalContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
	    
	    if(ezProxyAccount == null){
	    	EZProxyAccountManager acctManager = app.getEZProxyAccountManager();
		    String ezProxyAccountName = settingsManager.getEZProxyAccount();
		    
		    if(!ezProxyAccountName.isEmpty())
		    	ezProxyAccount = fetchEZProxyAccount(acctManager, ezProxyAccountName);
	    }
	    
	    if(ezProxyAccount != null)
	    	allowEZProxy = true;
	    
		initialized=true;
	}

	private void loginEZProxy() throws IOException {
		EratosApplication app = EratosApplication.getApplication();
		EZProxyAccountManager acctManager = app.getEZProxyAccountManager();
		
	    if(ezProxyAccount != null)
	    	submitEZProxyLoginForm(acctManager, ezProxyAccount);
	    ezProxyInitialized = true;
	}
	
	private Account fetchEZProxyAccount(EZProxyAccountManager acctManager, String acctName) throws IOException {
		try {
			return acctManager.getEZProxyAccount(acctName);
		} catch (AuthenticationException e) {
			throw new IOException("Account not configured for EZProxy: " + acctName);
		}
	}
	
	private void submitEZProxyLoginForm(EZProxyAccountManager acctManager, Account acct) throws UnsupportedEncodingException, IOException, ClientProtocolException {
		proxyUri = acctManager.getEZProxyFetchURL(acct);
		EratosUri loginUri = acctManager.getEZProxyLoginURL(acct);
		String userName = acctManager.getEZProxyUsername(acct);
		String password = acctManager.getEZProxyPassword(acct);		
		
		HttpPost httpPost = new HttpPost(loginUri.toURI());
		httpPost.setHeader(USER_AGENT_HEADER, user_agent);		
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("user", userName));
		urlParameters.add(new BasicNameValuePair("pass", password));
	 
		httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		HttpResponse response = mHttpClient.execute(httpPost, mLocalContext);
		String content = WebUtils.readStream(response.getEntity().getContent());
		
		if(content.contains("Please try again") || content.contains("username or password was incorrect"))
			throw new IOException(String.format(Locale.US, "Invalid EZProxy credentials for account: %s @ %s", userName, loginUri));
	}

	public File downloadPdfFile(String srcUrl, EratosUri fileUri) throws IOException {
		return downloadFile(srcUrl, fileUri, "application/pdf", "application/x-pdf");
	}
	
	public File downloadPdfFile(String srcUrl, EratosUri downloadUri, DownloadListener listener) throws IOException {
		File fileOut = FileManager.getFileManager().getFile(downloadUri);
		return downloadFile(srcUrl, fileOut, listener, "application/pdf", "application/x-pdf");
	}
	
	public File downloadFile(String srcUrl, EratosUri downloadUri, DownloadListener listener, String ... acceptedMimeTypes) throws IOException {
		File fileOut = FileManager.getFileManager().getFile(downloadUri);
		return downloadFile(srcUrl, fileOut, listener, acceptedMimeTypes);
	}
	
	public File downloadFile(String srcUrl, EratosUri downloadUri, String ... acceptedMimeTypes) throws IOException {
		File fileOut = FileManager.getFileManager().getFile(downloadUri);
		return downloadFile(srcUrl, fileOut, acceptedMimeTypes);
	}
	
	public File downloadFile(String srcUrl, File location, String ... acceptedMimeTypes) throws IOException {
		DownloadListener listener = new DownloadListener() {
			public void progress(long progress, long max) {
				
			}
		};
		return downloadFile(srcUrl, location, listener, acceptedMimeTypes);
	}
	
	private HttpResponse queryFile(String srcUrl, boolean useEZProxy) throws IOException {
		if(allowEZProxy && useEZProxy){
			if(!ezProxyInitialized)
				loginEZProxy();
			
			String uri = proxyUri.format(srcUrl).toString();
			HttpResponse response = getFileResponse(uri);
			String mimeType = WebUtils.getMimeType(response.getEntity());
			
			if(mimeType.equals("text/html")){
				Document doc = WebUtils.makeHtml(uri, response);
				String text = doc.text();
				
				if(text.contains("your EZproxy administrator must first authorize the hostname")){
					// ideally we should record the unsupported hosts to save query time later
					return getFileResponse(srcUrl);
				}else{
					Elements elements = doc.getElementsByTag("a");
					Element e = elements.get(0);
					String redirectUri = e.attr("href");
					return getFileResponse(redirectUri);
				}
			}else{
				return response;
			}
		}else{
			return getFileResponse(srcUrl);
		}
	}
	
	private HttpResponse getFileResponse(String url) throws IOException {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader(USER_AGENT_HEADER, user_agent);
		HttpResponse response = mHttpClient.execute(httpGet, mLocalContext);
		WebUtils.assertHttpStatus(response, 200);
		return response;
	}

	private File downloadFile(String srcUrl, File location, DownloadListener listener, String ... acceptedMimeTypes) throws IOException {
		if(!initialized)
			initHttpClient();
		
		HttpResponse response = queryFile(srcUrl, true);
		HttpEntity entity = response.getEntity();
		if(acceptedMimeTypes.length > 0)
			WebUtils.assertMimeType(entity, acceptedMimeTypes);
		
		InputStream in = entity.getContent();
		logger.info("Downloading: %s To: %s", srcUrl, location.getAbsolutePath());
		
		long fileSize = entity.getContentLength();
		FileOutputStream out = new FileOutputStream(location);
		
		byte [] buf = new byte[4096];
		
		int len;
		long total = 0;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
			total += len;
			listener.progress(total, fileSize);
		}
		in.close();
		out.close();
		
		return location;
	}

	private String createQueryUrl(String srcUrl, boolean useEZProxy) throws IOException {
		if(allowEZProxy && useEZProxy){
			
			if(!ezProxyInitialized)
				loginEZProxy();
			
			String uri = proxyUri.format(srcUrl).toString();
			return uri;
		}
		return srcUrl;
	}

	public HttpEntity queryContent(EratosUri requestUri) throws IOException {
		return queryContent(requestUri, false, new HashMap<String, String>());
	}
	
	public HttpEntity queryContent(EratosUri uri, Map<String, String> headers) throws IOException {
		return queryContent(uri.toString(), false, headers);
	}
	
	public HttpEntity queryContent(EratosUri requestUri, boolean useEZProxy) throws IOException {
		return queryContent(requestUri, useEZProxy, new HashMap<String, String>());
	}
	
	public HttpEntity queryContent(EratosUri uri, boolean useEZProxy, Map<String, String> headers) throws IOException {
		return queryContent(uri.toString(), useEZProxy, headers);
	}
	
	public HttpEntity queryContent(String uri, boolean useEZProxy, Map<String, String> headers) throws IOException {
		if(!initialized)
			initHttpClient();
		
		String queryUrl = createQueryUrl(uri, useEZProxy);
		HttpGet httpGet = new HttpGet(queryUrl);
		httpGet.setHeader(USER_AGENT_HEADER, user_agent);
		logger.info("Fetching content: %s", queryUrl);
		
        for(String name : headers.keySet()){
        	httpGet.addHeader(name, headers.get(name));
        }

        HttpResponse response = mHttpClient.execute(httpGet, mLocalContext);
        WebUtils.assertHttpStatus(response, 200);
        
        return response.getEntity();
	}

	public Document queryHtmlPage(EratosUri uri) throws IOException{
		return queryHtmlPage(uri.toString(), false, new HashMap<String, String>());
	}
	
	public Document queryHtmlPage(String uri) throws IOException{
		return queryHtmlPage(uri, false, new HashMap<String, String>());
	}
	
	public Document queryHtmlPage(EratosUri uri, Map<String, String> headers) throws IOException{
		return queryHtmlPage(uri.toString(), false, headers);
	}

	public Document queryHtmlPage(String uri, Map<String, String> headers) throws IOException {
		return queryHtmlPage(uri, false, headers);
	}
	
	public Document queryHtmlPage(EratosUri uri, boolean useEZProxy) throws IOException{
		return queryHtmlPage(uri.toString(), useEZProxy, new HashMap<String, String>());
	}
	
	public Document queryHtmlPage(String uri, boolean useEZProxy) throws IOException{
		return queryHtmlPage(uri, useEZProxy, new HashMap<String, String>());
	}
	
	public Document queryHtmlPage(EratosUri uri, boolean useEZProxy, Map<String, String> headers) throws IOException {
		return queryHtmlPage(uri.toString(), useEZProxy, headers);
	}
	
	public Document queryHtmlPage(String uri, boolean useEZProxy, Map<String, String> headers) throws IOException {
		if(!initialized)
			initHttpClient();
		
		String queryUrl = createQueryUrl(uri, useEZProxy);
		HttpGet httpGet = new HttpGet(queryUrl);
		httpGet.setHeader(USER_AGENT_HEADER, user_agent);
		logger.info("Fetching web page: %s", queryUrl);
		
        for(String name : headers.keySet()){
        	httpGet.addHeader(name, headers.get(name));
        }
        
        HttpResponse response = mHttpClient.execute(httpGet, mLocalContext);
        WebUtils.assertHttpStatus(response, 200);
        
        return WebUtils.makeHtml(uri, response);
	}

	public EratosUri followRedirects(EratosUri uri) throws IOException {
		if(!initialized)
			initHttpClient();
		
		String path = uri.toString();
		HttpClientParams.setRedirecting(mHttpClient.getParams(), false);
		HttpHead httpHead = new HttpHead(path);
		httpHead.setHeader(USER_AGENT_HEADER, user_agent);

		HttpResponse response = mHttpClient.execute(httpHead);
		int statusCode = response.getStatusLine().getStatusCode();
		
		if(statusCode == 200 || statusCode == 401 || statusCode == 403 || statusCode == 503){
			return uri;
		}else if(statusCode == 300 || statusCode == 301 || statusCode == 302 || statusCode == 303){
			String location = response.getFirstHeader("Location").getValue();
			
			EratosUri host = uri.host();
			EratosUri redirectUri = null;
			
			if(!(location.startsWith(host.toString()) || location.startsWith("http"))){
				redirectUri = host.append(location);
			}else
				redirectUri = EratosUri.parseUri(location);
			
			if(redirectUri != null)
				return followRedirects(redirectUri);
			else
				return uri;
		}else{
			throw new IOException("Unexpected status code: " + statusCode);
		}
	}
}
