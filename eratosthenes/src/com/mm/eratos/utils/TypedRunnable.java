package com.mm.eratos.utils;

public interface TypedRunnable<R, T> {
	public T run(R arg);
}
