package com.mm.eratos.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogManager;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.files.Compress;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;

public class NotificationUtils {
	public static OnClickListener doNothingListener = new OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		}
	};
	
	public static void notify(Context context, String title, String message) {
		notify(context, title, message, doNothingListener);
	}

	public static void notify(Context context, String title, String message, OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context, android.R.style.Theme_Holo_Light_Dialog);
		builder.setTitle(title)
				.setMessage(message)
				.setIcon(com.mm.eratos.R.drawable.action_about)
				.setCancelable(false)
				.setPositiveButton("Ok", listener)
				.show();
	}

	public static void notifyException(Context context, String title, Throwable e) {
		notifyException(context, title, e, doNothingListener);
	}
	
	public static void notifyException(final Context context, final String title, final Throwable e, final OnClickListener listener) {
		EratosApplication.getApplication().getLogger().error(e, "Error in %s: %s", context.getClass().getCanonicalName(), title);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context, android.R.style.Theme_Holo_Light_Dialog);
		builder.setTitle(title)
				.setMessage(e.getMessage())
				.setIcon(com.mm.eratos.R.drawable.ic_action_error)
				.setCancelable(false)
				.setPositiveButton("Ok", listener)
				.setNegativeButton("Report", new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						ProgressDialog pd = new ProgressDialog(context);
						pd.setIndeterminate(true);
						pd.setMessage("Preparing report");
						pd.setCancelable(false);
						pd.show();
						
						try{
							sendExceptionReport(context, title, e);
							pd.dismiss();
							listener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
						}catch(IOException e){
							pd.dismiss();
							NotificationUtils.notifyException(context, "Error preparing report", e, listener);
						}
					}
				})
				.show();
	}
	
	public static void sendExceptionReport(Context c, String t, Throwable e) throws IOException{
		String stackTrace = StringUtils.stackTraceToString(e);
		
		FileManager fileManager = FileManager.getFileManager();
		EratosApplication app = EratosApplication.getApplication();
		EratosSettings settingsManager = app.getSettingsManager();
		EratosLogManager logManager = app.getLogManager();
		
		EratosUri tmpFolder = settingsManager.getExportUri();
		EratosUri dbPath = tmpFolder.append("dbdump.sql");
		EratosUri prefPath = tmpFolder.append("prefdump.txt");
		EratosUri zipPath = tmpFolder.append("report.zip");
		
		EratosUri bibPath = app.getLastOpenedLibrary();
		
		File dbFile = fileManager.getFile(dbPath);
		File prefFile = fileManager.getFile(prefPath);
		File zipFile = fileManager.getFile(zipPath);
		
		new DBHelper(c).export(dbFile);
		settingsManager.writePreferences(prefFile);
		
		List<String> fileList = new ArrayList<String>();
		fileList.add(dbFile.getAbsolutePath());
		fileList.add(prefFile.getAbsolutePath());
		
		List<File> logFiles = logManager.copyLogFiles(fileManager.getFile(tmpFolder));
		for(File logFile : logFiles)
			fileList.add(logFile.getAbsolutePath());
		
		if(bibPath != null){
			File bibFile = fileManager.getFile(bibPath);
			if(bibFile.exists())
				fileList.add(bibFile.getAbsolutePath());
		}
		new Compress(fileList, zipFile).zip();
		
		String version = app.getVersion();
		String time = BibTeXEntryModel.dateFormat.format(Calendar.getInstance(Locale.US).getTime());
		String subject = "Eratosthenes: " + t;
		String message = String.format(Locale.US, "Eratosthenes Version: %s\n\nError on %s: %s\n\n%s", version, time, e.getMessage(), stackTrace);
		
		mailTo(c, EratosApplication.adminEmail, subject, message, zipFile);
		
		dbFile.delete();
		prefFile.delete();
	}
	
	public static void mailTo(Context c, String address, String subject, String message, File attachment){
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + address));
		
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, message);
		emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(attachment));
		
		c.startActivity(Intent.createChooser(emailIntent, "Creating message..."));
	}
	
	public static void mailTo(Context c, String address, String subject, String message){
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + address));
		
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, message);
		
		c.startActivity(Intent.createChooser(emailIntent, "Creating message..."));
	}
	
	public static void share(Context c, String subject, String message, File attachment){
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		
		intent.setType("text/plain");
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, message);
		intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(attachment));
		
		c.startActivity(Intent.createChooser(intent, "Creating message..."));
	}
	
	public static void promptInputNumber(final Context context, final CharSequence title, final IStringResultListener successListener) {
		promptInput(context, title, "", successListener, InputType.TYPE_CLASS_NUMBER);
	}
	
	public static void promptInputDecimal(final Context context, final CharSequence title, final IStringResultListener successListener) {
		promptInput(context, title, "", successListener, InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	}
	
	public static void promptInput(final Context context, final CharSequence title, final IStringResultListener successListener) {
		promptInput(context, title, "", successListener, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);
	}
	
	public static void promptInput(final Context context, final CharSequence title, final CharSequence initialValue, final IStringResultListener successListener) {
		promptInput(context, title, initialValue, successListener, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);
	}
	
	public static void promptInput(final Context context, final CharSequence title, final CharSequence initialValue, final IStringResultListener successListener, int type) {
		final EditText input = new EditText(context);
		input.setText(initialValue);
		input.setInputType(type);
		input.setLines(1);
		input.setSingleLine(true);
		input.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		final AlertDialog dialog = 
				new AlertDialog.Builder(context)
					.setTitle(title)
					.setView(input)
					.setPositiveButton("Ok", null)
					.setNegativeButton("Cancel", doNothingListener)
					.show();
		
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener( new View.OnClickListener() {
								public void onClick(View v) {
									if(successListener.onResult(input.getText().toString(), dialog))
										dialog.dismiss();
								}
							});
	}
	

	public static void promptInputLarge(final Context context, final CharSequence title, String initialValue, final IStringResultListener successListener) {
		final EditText input = new EditText(context);
		input.setText(initialValue);
		input.setLines(5);
		input.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		final AlertDialog dialog = 
				new AlertDialog.Builder(context)
					.setTitle(title)
					.setView(input)
					.setPositiveButton("Ok", null)
					.setNegativeButton("Cancel", doNothingListener)
					.show();
		
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener( new View.OnClickListener() {
								public void onClick(View v) {
									if(successListener.onResult(input.getText().toString(), dialog))
										dialog.dismiss();
								}
							});
		
	}
	
	public static void confirmDialog(Context context, CharSequence title, OnClickListener successListener) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setPositiveButton("Ok", successListener)
				.setNegativeButton("Cancel", doNothingListener)
				.show();
	}
	

	public static void confirmDialog(Context context, CharSequence title, CharSequence message, OnClickListener successListener) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton("Ok", successListener)
				.setNegativeButton("Cancel", doNothingListener)
				.show();
	}
	
	public static void confirmDialog(Context context, CharSequence title, CharSequence message, CharSequence okText, CharSequence cancelText, OnClickListener successListener) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(okText, successListener)
				.setNegativeButton(cancelText, doNothingListener)
				.show();
	}
	
	public static void confirmDialog(Context context, CharSequence title, CharSequence message, OnClickListener successListener, OnClickListener failListener) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton("Ok", successListener)
				.setNegativeButton("Cancel", failListener)
				.show();
	}
	

	public static void ternaryDialog(Context context, CharSequence title, CharSequence message, OnClickListener clickListener) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton("Yes", clickListener)
				.setNeutralButton("No", clickListener)
				.setNegativeButton("Cancel", clickListener)
				.show();
	}
	
	public static void longToast(Context context, String text, Object ... args) {
		text = String.format(Locale.US, text, args);
		Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
		toast.show();
	}

	public static void longToastTop(Context context, String text, Object ... args) {
		text = String.format(Locale.US, text, args);
		Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.TOP, 0, 0);
		toast.show();
	}

	public static void shortToast(Context context, String text, Object ... args) {
		text = String.format(Locale.US, text, args);
		Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	public static void notifyWarnings(Context mContext, String title, List<? extends Throwable> warnings) {
		notifyWarnings(mContext, title, warnings, doNothingListener);
	}
	
	public static void notifyWarnings(Context mContext, String title, List<? extends Throwable> warnings, OnClickListener listener) {
		String message = "";
		
		for(Throwable w : warnings){
			message += "** " + w.getMessage() + "\n";
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_Dialog);
		builder.setTitle(title)
				.setMessage(message)
				.setIcon(com.mm.eratos.R.drawable.ic_action_warning)
				.setCancelable(false)
				.setPositiveButton("Ok", listener)
				.show();
	}

	public static void notifyWarningsAllowEmail(final Activity mContext, final String title, List<? extends Throwable> errors) {
		String message = "";
		for(Throwable w : errors){
			message += "** " + w.getMessage() + "\n";
		}
		final String fMessage = message;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_Dialog);
		builder.setTitle(title)
				.setMessage(message)
				.setIcon(com.mm.eratos.R.drawable.ic_action_warning)
				.setCancelable(false)
				.setPositiveButton("Ok", doNothingListener)
				.setNegativeButton("Report", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mailTo(mContext, EratosApplication.adminEmail, title, fMessage);
					}
				})
				.show();
	}
}
