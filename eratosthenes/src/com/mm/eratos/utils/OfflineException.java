package com.mm.eratos.utils;

import java.io.IOException;

public class OfflineException extends IOException {
	private static final long serialVersionUID = 1L;
	
	public OfflineException(){
		super();
	}
	public OfflineException(String msg){
		super(msg);
	}
}
