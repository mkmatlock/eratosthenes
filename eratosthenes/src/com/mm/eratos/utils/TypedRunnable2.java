package com.mm.eratos.utils;

public interface TypedRunnable2<R, T, S> {
	public S run(T accum, R arg);
}
