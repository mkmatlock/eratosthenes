package com.mm.eratos.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Functional {
	
	public static class Any implements TypedRunnable2<Boolean, Boolean, Boolean> {
		public Boolean run(Boolean result, Boolean arg) {
			return result || arg;
		}
	};
	
	public static class And implements TypedRunnable2<Boolean, Boolean, Boolean> {
		public Boolean run(Boolean result, Boolean arg) {
			return result && arg;
		}
	};
	
	public static class PatternFind implements TypedRunnable<Pattern, Boolean> {
		public String text;
		
		public Boolean run(Pattern arg) {
			return arg.matcher(text).find();
		}
	};

	public static <R, T>  T reduce(List<R> inputs, T init, TypedRunnable2<R, T, T> func) {
		T output = init;
		for(R r : inputs)
			output = func.run(output, r);
		return output;
	}
	
	public static <R, T>  T reduce(R [] inputs, T init, TypedRunnable2<R, T, T> func) {
		T output = init;
		for(R r : inputs)
			output = func.run(output, r);
		return output;
	}
	
	public static <R, T>  List<T> map(List<R> inputs, TypedRunnable<R, T> func) {
		List<T> outputs = new ArrayList<T>(inputs.size());
		for(R r : inputs){
			outputs.add( func.run(r) );
		}
		return outputs;
	}
	
	public static <R, T>  List<T> map(R [] inputs, TypedRunnable<R, T> func) {
		List<T> outputs = new ArrayList<T>(inputs.length);
		for(R r : inputs){
			outputs.add( func.run(r) );
		}
		return outputs;
	}

	public static <R, T>  List<T> append(List<R> inputs, TypedRunnable<R, List<T>> func) {
		List<T> outputs = new ArrayList<T>(inputs.size());
		for(R r : inputs){
			outputs.addAll( func.run(r) );
		}
		return outputs;
	}
	
	public static <R, T>  List<T> append(R [] inputs, TypedRunnable<R, List<T>> func) {
		List<T> outputs = new ArrayList<T>(inputs.length);
		for(R r : inputs){
			outputs.addAll( func.run(r) );
		}
		return outputs;
	}
	
	public static <R> List<R> filter(List<R> inputs, TypedRunnable<R, Boolean> func) {
		List<R> outputs = new ArrayList<R>(inputs.size());
		for(R r : inputs){
			if(func.run(r))
				outputs.add( r );
		}
		return outputs;
	}
	
	public static <R> List<R> filter(R [] inputs, TypedRunnable<R, Boolean> func) {
		List<R> outputs = new ArrayList<R>(inputs.length);
		for(R r : inputs){
			if(func.run(r))
				outputs.add( r );
		}
		return outputs;
	}
	
}
