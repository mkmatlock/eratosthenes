package com.mm.eratos.utils;

import android.content.DialogInterface;

public interface IStringArrayResultListener {
	public boolean onResult(DialogInterface dialog, String ... results);
}
