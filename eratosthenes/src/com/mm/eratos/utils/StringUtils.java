package com.mm.eratos.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

public class StringUtils {
	public static final int ELLIPSIZE_START = 0;
	public static final int ELLIPSIZE_MIDDLE = 1;
	public static final int ELLIPSIZE_END = 2;
	
	static Random rand = new Random();
	
	public static String random(String charset, int length){
		String str = "";
		for(int i = 0; i < length; i++){
			str += charset.charAt(rand.nextInt(charset.length()));
		}
		return str;
	}
	
	public static String capitalize(String original){
		original = original.trim();
		return original.substring(0, 1).toUpperCase(Locale.US) + original.substring(1);
	}
	
	public static String join(String delimiter, Collection<?> collection){
		StringBuilder sb = new StringBuilder();
		
		for(Object o : collection){
			sb.append(o.toString());
			sb.append(delimiter);
		}
		
		if(sb.length()>0)
			sb.delete(sb.length()-delimiter.length(), sb.length());
		
		return sb.toString();
	}
	
	public static String join(String delimiter, String ... params) {
		return StringUtils.join(delimiter, (Object[]) params);
	}

	public static String join(String delimiter, Object ... params) {
		StringBuilder sb = new StringBuilder();
		
		for(Object o : params){
			sb.append(o.toString());
			sb.append(delimiter);
		}
		
		if(sb.length()>0)
			sb.delete(sb.length()-delimiter.length(), sb.length());
		
		return sb.toString();
	}
	
	public static String join(String delimiter, String[] params, int startIndex){
		StringBuilder sb = new StringBuilder();
		
		for(int i = startIndex; i < params.length; i++){
			String s = params[i];
			sb.append(s);
			sb.append(delimiter);
		}
		
		if(sb.length()>0)
			sb.delete(sb.length()-delimiter.length(), sb.length());
		
		return sb.toString();
	}

	public static String oneliner(String message) {
		int index = message.indexOf("\n");
		if(index == -1)
			return message;
		return message.substring(0, index);
	}

	public static String ellipsize(String text, int size, int direction) {
		if(text.length() <= size)
			return text;
		
		switch(direction){
		case ELLIPSIZE_START:
			text = "..." + text.substring(text.length() - size + 3);
			break;
		case ELLIPSIZE_MIDDLE:
			int R = size % 2;
			text = text.substring(0, size/2-1) + "..." + text.substring(text.length() - size/2 + 2 - R);
			break;
		case ELLIPSIZE_END:
			text = text.substring(0, size-3) + "...";
			break;
		}
		return text;
	}
	
	public static String format(String format, Map<String, ? extends Object> values) {
		Set<String> keys = values.keySet();
		String formatted = format;
		
		for(String key : keys){
			String formatKey = "%(" + key + ")";
			String value = values.get(key).toString();
			
			formatted = formatted.replaceAll(Pattern.quote(formatKey), value);
		}
		
		return formatted;
	}

	public static void printMap(Map<String, String> map) {
		for(String k : map.keySet()){
			System.out.println(String.format(Locale.US, "%s = %s", k, map.get(k)));
		}
	}
	
	public static String stackTraceToString(Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.flush();
		return sw.toString();
	}
	
	public static String whitespace(int len) {
		StringBuffer sb = new StringBuffer();
		for(int j = 0; j < len; j++)
			sb.append(' ');
		return sb.toString();
	}
}