package com.mm.eratos.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ArrayUtils {
	
	public static<T> List<T> slice(T[] elements, int start){
		List<T> slice = new ArrayList<T>(elements.length - start);
		
		for(int i = start; i < elements.length; i++)
			slice.add(elements[i]);
		
		return slice;
	}
	
	public static<T> List<T> slice(T[] elements, int start, int end){
		List<T> slice = new ArrayList<T>(end - start);
		
		for(int i = start; i < end; i++)
			slice.add(elements[i]);
		
		return slice;
	}
	
	public static<T> List<T> slice(List<T> elements, int start){
		List<T> slice = new ArrayList<T>(elements.size() - start);
		
		for(int i = start; i < elements.size(); i++)
			slice.add(elements.get(i));
		
		return slice;
	}
	
	public static<T> List<T> slice(List<T> elements, int start, int end){
		List<T> slice = new ArrayList<T>(end - start);
		
		for(int i = start; i < end; i++)
			slice.add(elements.get(i));
		
		return slice;
	}
	
	public static List<String> load(File f) throws IOException{
		List<String> items = new ArrayList<String>();
		
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = null;
		while((line = br.readLine()) != null){
			items.add(line.trim());
		}
		br.close();
		
		return items;
	}

	public static<T> int countOccurences(T item, List<T> objects) {
		int cnt = 0;
		for(T obj : objects){
			if(obj.equals(item)) cnt++;
		}
		return cnt;
	}
	
	public static boolean containsDuplicatesIgnoreCase(List<String> objects){
		for(int i = 0; i < objects.size(); i++){
			int cnt = 0;
			String item = objects.get(i);
			
			for(int j = i+1; j < objects.size(); j++){
				String obj = objects.get(j);
				if(obj.equalsIgnoreCase(item)) cnt++;
			}
			if(cnt > 0){
				System.out.println("Duplicate item: " + item);
				return true;
			}
		}
		return false;
	}
}