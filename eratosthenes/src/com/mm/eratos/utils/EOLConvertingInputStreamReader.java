package com.mm.eratos.utils;

import java.io.IOException;
import java.io.Reader;

public class EOLConvertingInputStreamReader extends Reader {
	private static final int bufSize = 4096;
	private Reader in;
	char[] circBuffer;
	int idx;
	int cnt;
	char lchar;
	private boolean done;
	private boolean skip;
	
	public EOLConvertingInputStreamReader(Reader in){
		this.in = in;
		circBuffer = new char[bufSize];
		idx = 0;
		cnt = -1;
		lchar = 0;
		done = false;
	}
	
	@Override
	public void close() throws IOException {
		in.close();
	}

	private char next() throws IOException {
		idx++;
		if(idx >= cnt){
			cnt = in.read(circBuffer, 0, bufSize);
			idx = 0;
		}
		
		if(cnt == -1){
			done = true;
			return 0;
		}

		char nchar = circBuffer[idx];
		char rchar = nchar;
		if(nchar == '\r') rchar = '\n';
		if(nchar == '\n' && lchar == '\r'){
			rchar = 0;
			skip = true;
		}
		
		lchar = nchar;
		return rchar;
	}
	
	@Override
	public int read(char[] buffer, int offset, int count) throws IOException {
		if(done) return -1;
		
		int read = 0;
		for(int i = 0; i < count; i++){
			char c = next();
			
			if(done)
				return read;
			if(skip){
				skip=false;
				i--;
			}else{
				buffer[i + offset] = c;
				read++;
			}
			
		}
		return read;
	}
}
