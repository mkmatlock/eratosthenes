package com.mm.eratos.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.accounts.Account;
import android.net.Uri;
import android.os.AsyncTask;
import android.webkit.MimeTypeMap;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.ezproxy.EZProxyAccountManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.task.CheckNetworkTask;

public class WebUtils {
	public static final String ENDNOTE_MIMETPYE = "application/x-endnote-library";
	public static final String RIS_MIMETYPE = "application/x-research-info-systems";
	public static final String BIBTEX_MIMETYPE = "application/x-bibtex";
	
	private static HashMap<String, String> extraMimeTypes;

	static{
		extraMimeTypes = new HashMap<String, String>();
		
		extraMimeTypes.put("bib", BIBTEX_MIMETYPE);
		extraMimeTypes.put("ris", RIS_MIMETYPE);
		extraMimeTypes.put("enl", ENDNOTE_MIMETPYE); // or application/x-endnote-refer
		extraMimeTypes.put("enz", ENDNOTE_MIMETPYE);
	}
	
	public static String readStream(InputStream is) throws IOException {
		return readStream(new InputStreamReader(is));
	}
	
	public static String readStream(Reader r) throws IOException {
		return readStream(new BufferedReader(r));
	}


	public static boolean arrayContains(int []array, int item){
		for(int i : array){
			if(i == item)
				return true;
		}
		return false;
	}
	
	public static void assertHttpStatus(HttpResponse response, int ... expectedStatuses) throws IOException {
		int statusCode = response.getStatusLine().getStatusCode();
		if(! arrayContains(expectedStatuses, statusCode) ){
			if(expectedStatuses.length == 1){
				System.out.println("HTTP Error!!!");
				System.out.println("HTTP Response:\n " + statusCode);
				System.out.println("HTTP Content:\n " + WebUtils.readStream(response.getEntity().getContent()));
				throw new IOException(String.format("Expected HTTP status %d, but got %d: %s", expectedStatuses[0], statusCode, response.getStatusLine().getReasonPhrase()));
			}else{
				throw new IOException(String.format("Expected HTTP status %d,%d,..., but got %d: %s", expectedStatuses[0], expectedStatuses[1], statusCode, response.getStatusLine().getReasonPhrase()));
			}
		}
	}

	public static String getMimeType(HttpEntity entity){
		Header contentType = entity.getContentType();
		String mimeType = contentType.getValue().split(";")[0].trim();
		return mimeType;
	}
	
	public static void assertMimeType(HttpEntity entity, String[] acceptedMimeTypes) throws IOException {
		Header contentType = entity.getContentType();
		String mimeType = contentType.getValue().split(";")[0].trim();
		
		boolean match=false;
		for(String aMimeType : acceptedMimeTypes){
			match |= aMimeType.equalsIgnoreCase(mimeType);
		}
		if(!match)
			throw new IOException(String.format(Locale.US, "Got mimeType %s, expected reponse mimeType: %s", mimeType, StringUtils.join(", ", acceptedMimeTypes)));
	}

	
	public static String readStream(BufferedReader bufferedReader)
			throws IOException {
		StringBuilder stream= new StringBuilder();
		String line = null;
		while((line = bufferedReader.readLine()) != null){
			stream.append(line);
			stream.append("\n");
		}
		return stream.toString();
	}

	public static Map<String, String> makeHeaders(String ... args) {
		Map<String, String> headers = new HashMap<String, String>();
		for(int i = 0; i < args.length; i += 2){
			headers.put(args[i], args[i+1]);
		}
		return headers;
	}
	
	public static String getMimeType(EratosUri uri){
		return getMimeType(uri.extension());
	}

	public static String getMimeType(String extension) {
		String type = null;
		
		if(extraMimeTypes.containsKey(extension))
			type = extraMimeTypes.get(extension);
		else{
			MimeTypeMap mime = MimeTypeMap.getSingleton();
			type = mime.getMimeTypeFromExtension(extension);
		}
		
        return type == null ? "application/octet-stream" : type;
	}

	public static boolean canAccess(String uri) {
		try {
			return new CheckNetworkTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, uri).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean checkNetworkState(String uri) {
		try {
			URL url = new URL(uri);
			HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
			
			urlc.setConnectTimeout(3000);
			urlc.connect();
			
			return true;
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
		return false;
	}
	
	public static Map<String, String> mapQuery(EratosUri uri) throws UnsupportedEncodingException {
	    Map<String, String> query_pairs = new HashMap<String, String>();
	    String query = uri.query();
	    String[] pairs = query.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	    }
	    return query_pairs;
	}
	
	public static Document makeHtml(String uri, HttpResponse response) throws IOException {
		HttpEntity entity= response.getEntity();
        InputStream content = entity.getContent();
        String charset = EntityUtils.getContentCharSet(entity);
        
        return Jsoup.parse(content, charset, uri);
	}

	public static InputStream makeStream(String text) {
		return new ByteArrayInputStream(text.getBytes());
	}
	
	public static Map<String, String> format(Map<String, String> unformatted) {
		try {
			Map<String, String> formatted = new HashMap<String, String>(unformatted.size());
			for(String key : unformatted.keySet()){
				formatted.put(key, URLEncoder.encode(unformatted.get(key), "UTF8"));
			}
			return formatted;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	static CaseInsensitiveHashSet ezProxyHosts = new CaseInsensitiveHashSet();
	static{
		ezProxyHosts.add("scholar.google.com");
		ezProxyHosts.add("books.google.com");
		ezProxyHosts.add("www.ncbi.nlm.nih.gov");
	}
	
	public static Uri formatExternalUriRequest(String url) {
		EratosApplication application = EratosApplication.getApplication();
		EratosSettings settingsManager = application.getSettingsManager();
		String ezProxyAccountName = settingsManager.getEZProxyAccount();
		
		Uri parsed = Uri.parse(url);
		if(parsed == null)
			return null;
		String host = parsed.getHost();
		
		if(ezProxyHosts.contains(host) && settingsManager.getEZProxyEnabled() && !ezProxyAccountName.isEmpty()){
			EZProxyAccountManager ezProxyAccountManager = application.getEZProxyAccountManager();
			
			try{
		    	Account acct = ezProxyAccountManager.getEZProxyAccount(ezProxyAccountName);
				EratosUri proxyUri = ezProxyAccountManager.getEZProxyFetchURL(acct);
				return Uri.parse(proxyUri.format(url).toString());
			} catch (AuthenticationException e) {
				System.err.println("Error retrieving EZProxy account: " + ezProxyAccountName);
			}
		}
		
		return parsed;
	}
}
