package com.mm.eratos.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MultiHashMap<K, V> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8023206928771404636L;
	
	public class Entry implements Map.Entry<K, V>{
		private K key;
		private V value;

		public Entry(K key, V value){
			this.key = key;
			this.value = value;
		}
		
		@Override
		public K getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V newValue) {
			V oldValue = value;
			List<V> values = map.get(key);
			
			int i = values.indexOf(oldValue);
			values.set(i, newValue);
			
			value = newValue;
			return oldValue;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		@SuppressWarnings("unchecked")
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			
			Entry other = (Entry) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
	}
	
	HashSet<V> values;
	HashSet<Entry> entries;
	HashMap<K, List<V>> map;
	
	public MultiHashMap(){
		map = new HashMap<K, List<V>>();
		values = new HashSet<V>();
		entries = new HashSet<Entry>();
	}
	
	public MultiHashMap(int capacity){
		map = new HashMap<K, List<V>>(capacity);
		values = new HashSet<V>(capacity);
		entries = new HashSet<Entry>(capacity);
	}
	
	public MultiHashMap(int capacity, float loadFactor){
		map = new HashMap<K, List<V>>(capacity, loadFactor);
		values = new HashSet<V>(capacity, loadFactor);
		entries = new HashSet<Entry>(capacity, loadFactor);
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}
	
	public Set<Entry> entrySet(){
		return Collections.unmodifiableSet(entries);
	}

	public boolean containsKey(K key) {
		return map.containsKey(key);
	}
	
	public boolean containsValue(V value) {
		return values.contains(value);
	}
	
	public List<V> get(K key) {
		return map.get(key);
	}

	public V put(K key, V value) {
		if(!map.containsKey(key))
			map.put(key, new ArrayList<V>());
		
		map.get(key).add(value);
		values.add(value);
		entries.add(new Entry(key, value));
		
		return value;
	}
	
	public List<V> remove(K key){
		List<V> keyvalues = map.remove(key);
		if(keyvalues != null){
			for(V value : keyvalues){
				values.remove(value);
				entries.remove(new Entry(key, value));
			}
		}
		
		return keyvalues;
	}

	public Collection<V> values() {
		return Collections.unmodifiableCollection(values);
	}
	
	public int size() {
		return map.size();
	}
	
	public boolean isEmpty() {
		return map.isEmpty();
	}
	
	@Override
	public String toString() {
		return map.toString();
	}
	
	@Override
	public int hashCode(){
		return map.hashCode();
	}
	
	@Override
	public boolean equals(Object object){
		return map.equals(object);
	}
}
