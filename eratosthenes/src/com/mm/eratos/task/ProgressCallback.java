package com.mm.eratos.task;

public interface ProgressCallback
{
	public void update(String stage, int progress, int maxProgress);
	public void finished();
	public void error(Throwable error);
}
