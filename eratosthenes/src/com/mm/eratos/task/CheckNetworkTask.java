package com.mm.eratos.task;

import android.os.AsyncTask;

import com.mm.eratos.utils.WebUtils;

public class CheckNetworkTask extends AsyncTask<String, Void, Boolean> {

	@Override
	protected Boolean doInBackground(String... uris) {
		return WebUtils.checkNetworkState(uris[0]);
	}

}
