package com.mm.eratos.task;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.utils.NotificationUtils;

public class CacheEntriesTask extends AsyncTask<String, Integer, Void> {
	public interface CacheEntryResultListener {
		public void success();
		public void failed();
	}
	
	Activity mContext;

	private MultiProgressDialog progressDialog;

	private BibTeXContentProviderAdapter bibtexAdapter;
	private FileRevisionContentProviderAdapter fileAdapter;

	private int downloaded;
	private int errors;
	private String curFilename;

	private CacheEntryResultListener listener;

	private FileManager fileManager;
	private EratosApplication app;
	
	public CacheEntriesTask(Activity mContext) {
		this(mContext, new CacheEntryResultListener() {
			public void success() { }
			public void failed() { }
		});
	}

	public CacheEntriesTask(Activity mContext, CacheEntryResultListener listener){
		this.mContext = mContext;
		this.listener = listener;
		
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());

		fileManager = FileManager.getFileManager();
		app = EratosApplication.getApplication();
	}
	
	@Override
	public void onPreExecute(){
		progressDialog = MultiProgressDialog.newInstance("Caching Entries");
		progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}

	@Override
	public void onProgressUpdate(Integer... values){
		progressDialog.setPrimaryProgress(values[0], values[1]);
		
		if(curFilename != null){
			progressDialog.setSecondarySubtitle("Downloading: " + curFilename);
			progressDialog.setSecondaryProgress(values[2], values[3]);
		}else{
			progressDialog.hideSecondary();
		}
	}
	
	@Override
	public Void doInBackground(String... params){
		publishProgress(0, params.length, 0, 0);
		
		downloaded = 0;
		errors = 0;
		curFilename = null;
		
		ProtocolHandler protocol = ProtocolHandler.getProtocolHandler(app.getCurrentLibrary());
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);

		final int maxProgress = params.length;
		int i = 0;
		for(String refKey : params){
			BibTeXEntryModel entry = bibtexAdapter.getEntry(refKey, modelFactory);
			
			for(EratosUri fileUri : entry.getFiles()) {
				try{
					EratosUri resUri = fileUri.resolveUri(app);
					File localFile = fileManager.getFile(resUri);
					boolean isRemote = resUri.isRemoteProtocol(app);
					
					File syncDir = localFile.getParentFile();
					if(!syncDir.exists() && !syncDir.mkdirs())
						throw new IOException("Could not create local sync directory: " + syncDir.getAbsolutePath());
					
					if (!localFile.exists() && isRemote)
					{
						SyncedFile sf = fileAdapter.getFile(resUri);
						curFilename = resUri.name();

						final int entryNumber = i;
						FileResult fr = protocol.fetch(resUri, resUri, false, new IFileOperationProgressListener() {
							@Override
							public void progress(String message, int bytes, int total) {
								publishProgress(entryNumber, maxProgress, bytes, total);
							}
						});
						
						fileAdapter.update(sf.revision(fr.revision()).setCached(true).accessed().setLastModified(localFile.lastModified()));
					}else if(isRemote){
						SyncedFile sf = fileAdapter.getFile(resUri);
						fileAdapter.update(sf.setCached(true).accessed().setLastModified(localFile.lastModified()));
					}
					downloaded++;
				}catch(Exception e){
					e.printStackTrace();
					errors++;
				}catch(Error e){
					e.printStackTrace();
					errors++;
				}
			}
			i++;
			publishProgress(i, params.length, 0, 0);
		}
		
		new PruneFilesTask(mContext).doInBackground();
		return null;
	}
	
	@Override
	public void onPostExecute(Void result){
		progressDialog.dismissCheckVisible();
		NotificationUtils.longToast(mContext, String.format(Locale.US, "Cached %d files (%d errors)", downloaded, errors));
		
		if(errors > 0)
			listener.failed();
		listener.success();
	}

}
