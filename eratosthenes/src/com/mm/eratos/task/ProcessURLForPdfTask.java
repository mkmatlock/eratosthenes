package com.mm.eratos.task;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.WebHandlerFactory;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class ProcessURLForPdfTask extends AsyncTask<String, Integer, Void> {
	Activity mContext;

	private MultiProgressDialog progressDialog;

	private BibTeXContentProviderAdapter bibtexAdapter;
	private FileRevisionContentProviderAdapter fileAdapter;

	private int downloaded;
	private int errors;
	private String curFilename;

	private int skipped;
	private int entryNum;

	private EratosApplication app;
	private FileManager fileManager;

	private int maxProgress;
	
	public ProcessURLForPdfTask(Activity mContext){
		this.mContext = mContext;
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
	}

	@Override
	public void onPreExecute(){
		progressDialog = MultiProgressDialog.newInstance("Downloading PDFs");
		progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}

	@Override
	public void onProgressUpdate(Integer... values){
		progressDialog.setPrimaryProgress(values[0], values[1]);
		
		if(values.length > 2){
			progressDialog.setSecondarySubtitle("Downloading: " + curFilename);
			progressDialog.setSecondaryProgress(values[2], values[3]);
		}else{
			progressDialog.hideSecondary();
		}
	}
	
	@Override
	public Void doInBackground(String... params){

		maxProgress = params.length;
		entryNum = 0;
		
		publishProgress(0, maxProgress);
		
		app = EratosApplication.getApplication();
		fileManager = FileManager.getFileManager();
		
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, app.getSettingsManager().getBibTeXHelper());
		
		downloaded = 0;
		errors = 0;
		skipped = 0;
		curFilename = null;
		
		
		for(String refKey : params){
			BibTeXEntryModel entry = bibtexAdapter.getEntry(refKey, modelFactory);
			
			EratosUri fileUri = fileManager.getDefaultAttachmentUri(entry.getSuggestedAttachmentName() + ".pdf");
			EratosUri resUri = fileUri.resolveUri(app);
			List<EratosUri> externalLinks = entry.getExternalLinks();
			if(entry.hasField("url"))
				externalLinks.add(EratosUri.parseUri(entry.getFieldValue("url")));
			
			
			boolean exists = fileAdapter.exists(resUri) || fileManager.getFile(resUri).exists();
			if(!entry.hasAttachments() && !exists && externalLinks.size() > 0){

				boolean success = false;
				for(EratosUri uri : externalLinks){
					AbstractWebHandler handler = WebHandlerFactory.getHandler(uri);
					
					try {
						EratosUri pdfUri = handler.pdfUri(entry, uri);
						
						download(entry, pdfUri, fileUri, resUri);
						
						entry = modelFactory.commit(entry);
						bibtexAdapter.update(entry);
						
						success=true;
						break;
					} catch (Exception e) {
						
					} catch(Error e) {
						
					}
				}
				if(!success)
					errors++;
					
			}else{
				skipped++;
			}
			entryNum++;
			publishProgress(entryNum, maxProgress);
		}
		return null;
	}
	
	
	public void download(BibTeXEntryModel entry, EratosUri pdfUri, EratosUri fileUri, EratosUri resUri) throws IOException{
		curFilename = pdfUri.toString();
		
		File localFile = WebClient.newInstance().downloadPdfFile(pdfUri.toString(), fileUri, new DownloadListener() {
			public void progress(long total, long fileSize) {
				publishProgress(entryNum, maxProgress, (int)total, (int)fileSize);
			}
		});
		if(resUri.isRemoteProtocol(app))
			fileAdapter.newSyncedFile(resUri, "", localFile, entry.important());
		entry.attachFile(fileUri);
		
		downloaded++;
	}
	
	@Override
	public void onPostExecute(Void result){
		progressDialog.dismissCheckVisible();
		NotificationUtils.longToast(mContext, String.format(Locale.US, "Downloaded %d PDF files (skipped %d, with %d errors)", downloaded, skipped, errors));
	}
}
