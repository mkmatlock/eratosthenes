package com.mm.eratos.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibFile;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter.BulkInsertUpdateListener;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.BibTeXNullReferenceResolver;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.io.IOFactory;
import com.mm.eratos.io.IParser;
import com.mm.eratos.io.bibtex.BibTypePredictor;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.ArrayUtils;
import com.mm.eratos.utils.NotificationUtils;

public class LoadLibraryTask extends AsyncTask<Void, Integer, Boolean>{
	private final static int BULK_INSERT_LIMIT = 500;
	
	static ProgressCallback nullCallback = new ProgressCallback() {
		public void update(String stage, int progress, int maxProgress) {
		}
		public void finished() {
		}
		public void error(Throwable error) {
		}
	};
	
	private BibTeXContentProviderAdapter contentProvider;
	private Activity mContext;
	private MultiProgressDialog progressDialog;
	private int entryCnt;
	private Throwable failure;
	private boolean showProgress;
	private ProgressCallback callback;
	private String message;
	private String typeWarningText;

	private String parserErrors;

	private EratosUri uri;
	private String bibType;

	private boolean skipCheck;

	private String encoding;

	public LoadLibraryTask(Activity mContext, EratosUri uri, String bibType, String encoding, boolean skipCheck, ProgressCallback callback) {
		this(mContext, uri, bibType, encoding, false, skipCheck, callback);
	}
	
	public LoadLibraryTask(Activity mContext, EratosUri uri, String bibType, String encoding, boolean showProgress, boolean skipCheck, ProgressCallback callback) {
		this.mContext = mContext;
		this.encoding = encoding;
		this.showProgress = showProgress;
		this.callback = callback;
		this.skipCheck = skipCheck;
		this.typeWarningText = "";
		this.uri = uri;
		this.bibType = bibType;
		contentProvider = new BibTeXContentProviderAdapter(mContext.getContentResolver());
	}
	
	public Throwable getError(){
		return failure;
	}
	
	@Override
    protected void onPreExecute()
    {
		if(showProgress){
			progressDialog = MultiProgressDialog.newInstance("Loading Library", message);
	        progressDialog.show(mContext.getFragmentManager(), "pdialog");
		}
		failure = null;
    };
	
    @Override
	protected void onProgressUpdate(Integer ... values) {
    	if(showProgress){
			progressDialog.setPrimarySubtitle(message);
	    	progressDialog.setPrimaryProgress(values[0], values[1]);
    	}
    	callback.update(message, values[0], values[1]);
	}
	
	@Override
	public Boolean doInBackground(Void... params) {
		try{
			File bibtexFile = FileManager.getFileManager().getFile(uri);
			entryCnt = 0;
			
			if(!bibtexFile.exists())
				throw new IOException("Bibtex entries file does not exist: " + uri);
			
			EratosApplication.getApplication().setOpenLibrary(uri, bibType, encoding);
			
			contentProvider.deleteAll();
			entryCnt = loadBibTeXObjects(bibtexFile, bibType, encoding);
			
			BibFile bibFile = new BibFile(uri, bibType, encoding, entryCnt);
			contentProvider.updateRecentFile(bibFile);
			
			return true;
		}catch(IllegalArgumentException e){
			e.printStackTrace();
			failure = new IOException("Failed to parse bibtex file: " + e.getMessage());
			EratosApplication.getApplication().closeLibrary();
			contentProvider.deleteAll();
		}catch(Exception e){
			e.printStackTrace();
			failure = e;
			EratosApplication.getApplication().closeLibrary();
			contentProvider.deleteAll();
		}catch(Error e){
			e.printStackTrace();
			failure = e;
			EratosApplication.getApplication().closeLibrary();
			contentProvider.deleteAll();
		}
		
		
		return false;
	}

	private int loadBibTeXObjects(File bibtexFile, String bibType, String encoding) throws IOException, ParseException, KeyCollisionException, ClassNotFoundException {
		final ProgressCallback progressHandler = new ProgressCallback() {
			public void update(String stage, int progress, int maxProgress) {
				message = stage;
				publishProgress(progress, maxProgress);
			}
			public void finished() {
			}
			public void error(Throwable e){
			}
		};
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		progressHandler.update("Checking file type...", 0, 0);
		
		if(!skipCheck) {
			String fileSoftwareType = BibTypePredictor.predictBibFileType(bibtexFile, encoding);
			if(!fileSoftwareType.isEmpty() && !bibType.equals(fileSoftwareType)){
				String message = "This file looks like it came from %s.\n\nPlease choose the appropriate compatibility mode from the settings and re-open this bibtex file to ensure that all features function correctly\n\nCurrent compatibility mode: %s";
				typeWarningText = String.format(Locale.US, message, fileSoftwareType, bibType);
			}
		}
		
		message = "Initializing parser...";
		publishProgress(0, 0);
		IParser parser = IOFactory.getParser(bibType);
		parser.init(new FileInputStream(bibtexFile), encoding, (int) bibtexFile.length());
		
		message = "Parsing database..."; 
		publishProgress(0, (int)parser.total());
		
		BibTeXEntryModelFactory modelFactory = 
				new BibTeXEntryModelFactory(new BibTeXNullReferenceResolver(), helper);
		
		int total = 0;
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		while(parser.next()){
			BibTeXObject object = parser.get();
			objects.add(object);
			
			if(objects.size() > BULK_INSERT_LIMIT){
				total += insertObjects(objects, helper, modelFactory);
				objects.clear();
			}
			
			publishProgress((int)parser.processed(), (int)parser.total());
		}
		if(objects.size() > 0){
			total += insertObjects(objects, helper, modelFactory);
			objects.clear();
		}
		
		progressHandler.update("Inserting extras...", 0, 0);
		parser.extras(contentProvider, modelFactory, progressHandler);
		
		parser.finish();
		parserErrors = parser.errors();
		
		postProcessEntries(helper, progressHandler);
		
		return total;
	}

	private void postProcessEntries(BibTeXHelper helper, final ProgressCallback progressHandler) {
		BibTeXEntryModelFactory modelFactory = 
				new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(contentProvider), helper);
		
		Cursor c = contentProvider.getAllKeys();
		
		List<String> allKeys = new ArrayList<String>(c.getCount());
		
		while(c.moveToNext()) {
			String refKey = c.getString( c.getColumnIndex(DBHelper.COLUMN_KEY) );
			allKeys.add(refKey);
		}
		c.close();
		
		for(int i = 0; i < allKeys.size(); i += BULK_INSERT_LIMIT){
			progressHandler.update("Post processing...", i, allKeys.size());
			
			int start = i;
			int end = Math.min(i + BULK_INSERT_LIMIT, allKeys.size());

			List<String> entryKeys = ArrayUtils.slice(allKeys, start, end);
			List<BibTeXEntryModel> entries = contentProvider.getEntries(entryKeys, modelFactory);
			List<BibTeXEntryModel> modified = new ArrayList<BibTeXEntryModel>(end-start);
			
			int j = 0;
			for(BibTeXEntryModel entry : entries){
				if(entry.getWarnings().size() > 0) {
					parserErrors += "Warnings when parsing entry: " + entry.getRefKey() + "\n";
					for(Throwable t : entry.getWarnings())
						parserErrors += t.getMessage() + "\n";
					parserErrors += "\n"; 
				}
				modified.add( entry );
				progressHandler.update("Post processing...", i+j, allKeys.size());
				j++;
			}
			
			contentProvider.deleteAll(entryKeys);
			contentProvider.bulkInsert(modified, new BulkInsertUpdateListener() {
				public void update(int progress, int max) {
				}
			});
		}
	}

	private int insertObjects(List<BibTeXObject> objects, BibTeXHelper helper, BibTeXEntryModelFactory modelFactory) throws IOException, ParseException {
		List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>();
		List<BibTeXString> strings = new ArrayList<BibTeXString>();
		
		for(BibTeXObject obj : objects){
			if(obj instanceof BibTeXEntry)
				entries.add((BibTeXEntry)obj);
			else if(obj instanceof BibTeXString)
				strings.add((BibTeXString)obj);
			else
				contentProvider.insert(helper, obj);
		}
		
		if(strings.size() > 0)
			contentProvider.bulkInsert(helper, strings);
		
		if(entries.size() > 0){
			List<BibTeXEntryModel> loadedEntries = loadBibtexEntries(entries, helper, modelFactory);
			
			contentProvider.bulkInsert(loadedEntries, new BulkInsertUpdateListener() {
				public void update(int progress, int max) {
				}
			});
			
			return loadedEntries.size();
		}
		
		return 0;
	}
	
	private List<BibTeXEntryModel> loadBibtexEntries(List<BibTeXEntry> loadedEntries, BibTeXHelper helper, BibTeXEntryModelFactory modelFactory) throws IOException, ParseException {
		List<BibTeXEntryModel> entryModels = new ArrayList<BibTeXEntryModel>();
		
        for(BibTeXEntry entry : loadedEntries) {
        	fixFileFields(entry);
        	BibTeXEntryModel model = modelFactory.construct(entry);
			entryModels.add(model);
        }
        return entryModels;
	}

	private void fixFileFields(BibTeXEntry entry)
	{
		Set<Key> keys = entry.getFields().keySet();
		for(Key k : keys){
			if(BibTeXEntryVerifier.fieldStartsWith(k.getValue(), "attached-file-")){
				EratosUri uri = EratosUri.parseUri(entry.getField(k).toUserString());
				EratosUri nuri = uri.resolveToRootFolder("Dropbox", EratosUri.DROPBOX_PROTOCOL);
				entry.addField(k, new StringValue(nuri.toString(), Style.BRACED));
			}
		}
	}
	
	@Override
    protected void onPostExecute(Boolean success){
		if(showProgress)
			progressDialog.dismissCheckVisible();
		
		if(success){
			NotificationUtils.shortToast(mContext, String.format("Loaded %d entries", entryCnt));
			EratosApplication.getApplication().markSaved();
			
			String warningText = getWarningText();
			if(!warningText.trim().isEmpty())
				NotificationUtils.notify(mContext, "Warning", warningText, new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						callback.finished();
					}
				});
			else
				callback.finished();
		}else{
			NotificationUtils.notifyException(mContext, "Load Failed", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					callback.error(failure);
				}
			});
		}
	}

	public String getWarningText() {
		StringBuilder message = new StringBuilder();
		if(!(parserErrors == null || parserErrors.trim().isEmpty())) {
			message.append("Encountered warnings while parsing: \n\n");
			message.append(parserErrors + "\n\n");
		}
		message.append(typeWarningText);
		return message.toString();
	}
}

