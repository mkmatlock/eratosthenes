package com.mm.eratos.task;
import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.mm.eratos.utils.NotificationUtils;

public class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap>
{
	private final WeakReference<ImageView> imageViewReference;
	private Context mContext;

//	private ProgressDialog progressDialog;
	
	public BitmapWorkerTask(Context mContext, ImageView imageView) {
		this.mContext = mContext;
		imageViewReference = new WeakReference<ImageView>(imageView);
	}

	@Override
	protected void onPreExecute()
	{
//		progressDialog = new ProgressDialog(mContext);
//		progressDialog.setIndeterminate(true); 
//		progressDialog.setCancelable(false);
//		progressDialog.setTitle("Loading");
//		progressDialog.setMessage("Loading tutorial page...");
//		progressDialog.show();
	}
	
	@Override
	protected Bitmap doInBackground(Integer... params) {
		return decodeSampledBitmapFromResource(mContext.getResources(), params[0], params[1], params[2]);
	}

	// Once complete, see if ImageView is still around and set bitmap.
	@Override
	protected void onPostExecute(Bitmap bitmap)
	{
//		progressDialog.dismiss();
		try{
			final ImageView imageView = imageViewReference.get();	
			imageView.setImageBitmap(bitmap);
			imageView.invalidate();
		}catch(NullPointerException npe){
			NotificationUtils.notifyException(mContext, "Unexpected Error", npe);
		}
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}
}
