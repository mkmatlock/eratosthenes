package com.mm.eratos.task;

import java.util.ArrayList;
import java.util.List;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

public class UpdateEntriesTask extends AsyncTask<Void, Integer, Void> {

	private ProgressCallback listener;
	private Context mContext;
	private ProgressDialog dialog;
	
	private BibTeXHelper helper;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private BibTeXContentProviderReferenceResolver resolver;
	private BibTeXEntryModelFactory modelFactory;
	
	private Throwable failure;
	private String stage;

	public UpdateEntriesTask(Context mContext, ProgressCallback listener){
		this.mContext = mContext;
		this.listener = listener;
		this.helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		this.modelFactory = new BibTeXEntryModelFactory(resolver, helper);
	}
	
	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(mContext);
		dialog.setTitle("Updating Entries");
		dialog.setIndeterminate(false);
		dialog.setCancelable(false);
		dialog.show();
		
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		listener.update(stage, values[0], values[1]);
		dialog.setMessage(stage);
		dialog.setProgress(values[0]);
		dialog.setMax(values[1]);
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try{
			List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>();
			
			Cursor c = bibtexAdapter.getAllEntries();
			stage = "Updating...";
			publishProgress(0, c.getCount());
			
			int i = 0;
			while(c.moveToNext()){
				BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(c, modelFactory);
				BibTeXEntryModel newEntry = modelFactory.commit(entry);
				entries.add(newEntry);
				
				i++;
				publishProgress(i, c.getCount());
			}
			c.close();
			
			stage = "Inserting...";
			publishProgress(0, entries.size());
			for(i = 0; i < entries.size(); i++){
				BibTeXEntryModel entry = entries.get(i);
				bibtexAdapter.update(entry);
				
				publishProgress(i, entries.size());
			}
		}catch(Exception e) {
			failure = e;
		}catch(Error e) {
			failure = e;
		}
		return null;
	}
	
	
	@Override
	protected void onPostExecute(Void result) {
		dialog.dismiss();
		
		if(failure != null)
			listener.error(failure);
		else
			listener.finished();
	}
	
	
}
