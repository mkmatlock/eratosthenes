package com.mm.eratos.task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibFile;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXEntryVerifier;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.files.Compress;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.io.IOFactory;
import com.mm.eratos.io.IWriter;
import com.mm.eratos.model.EratosUri;

public class SaveDatabaseTask extends AsyncTask<String, Integer, Void> {

	public interface ExportBibListener {
		public void started();
		public void progress(int progress, int maxprogress);
		public void finished(EratosUri exportUri, int entries, int objects, int strings);
		public void failed(Throwable error);
	}
	
	Throwable failure;
	private EratosUri exportUri;
	private boolean saveRecent;
	private boolean exportFiles;
	private ExportBibListener listener;
	private EratosUri archiveUri;
	private BibTeXContentProviderAdapter contentProvider;
	private int objects;
	private int entries;
	private int strings;
	private String bibType;
	private String encoding;
	private boolean makeBackup;
	
	public SaveDatabaseTask(Context mContext, EratosUri exportUri, EratosUri archiveUri, String bibType, String encoding, boolean saveRecent, boolean exportFiles, boolean makeBackup, ExportBibListener listener){
		this.exportUri = exportUri;
		this.archiveUri = archiveUri;
		this.bibType = bibType;
		this.encoding = encoding;
		this.saveRecent = saveRecent;
		this.exportFiles = exportFiles;
		this.makeBackup = makeBackup;
		this.listener = listener;
		
		contentProvider = new BibTeXContentProviderAdapter(mContext.getContentResolver());
	}

	@Override
	protected void onPreExecute() {
		listener.started();
	}


	@Override
	protected void onProgressUpdate(Integer... values) {
		listener.progress(values[0], values[1]);
	}

	@Override
	protected Void doInBackground(String... keys) {
		objects = -1;
		entries = -1;
		strings = -1;
		FileManager fileManager = FileManager.getFileManager();
		
		try{
			List<BibTeXEntry> entriesList = exportBibtexEntries(keys, exportFiles);
			
			if(makeBackup){
				File bibFile = fileManager.getFile(exportUri);
				if(bibFile.exists()){
					EratosUri backupFileUri = fileManager.getBackupFile(exportUri);
					File backupFile = fileManager.getFile(backupFileUri);
					
					if(backupFile.exists())
						backupFile.delete();
					
					FileUtils.copy(bibFile, backupFile);
				}
			}
			
			IWriter writer = IOFactory.getWriter(bibType);
			
			EratosUri tempUri = fileManager.getTemporaryUri("save.bib");
			File tempFile = createFile(tempUri);
			writer.init(new FileOutputStream(tempFile), encoding);
			
			strings = writeStrings(writer);
			entries = writeEntries(writer, entriesList);
			objects = writeObjects(writer);
			writer.writeExtras();
			writer.finish();
			
			File exportFile = createFile(exportUri);
			FileUtils.copy(tempFile, exportFile);

			if(saveRecent) {
				EratosApplication.getApplication().markSaved();
				contentProvider.updateRecentFile(new BibFile(exportUri, bibType, encoding, entries));
			}
			if(exportFiles) {
				exportFiles(archiveUri, exportUri, keys);
			}
			
		}catch(Exception e){
			failure = e;
		}catch(Error e){
			failure = e;
		}
		
		return null;
	}


	@Override
	protected void onPostExecute(Void result) {
		if(failure == null)
			listener.finished(exportUri, entries, objects, strings);
		else
			listener.failed(failure);
	}
	
	private File createFile(EratosUri exportUri) throws IOException {
		File f = FileManager.getFileManager().getFile(exportUri);
		if(f.exists())
			f.delete();
		f.createNewFile();
		f.setReadable(true, false);
		f.setWritable(true, false);
		
		return f;
	}

	private int writeObjects(IWriter writer) throws IOException {
		int cnt = 0;
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();

		Cursor c = contentProvider.getObjects();
		while(c.moveToNext()){
			BibTeXObject object = contentProvider.cursorToObject(c, helper);
			writer.write( object );
			cnt++;
		}
		return cnt;
	}

	private int writeStrings(IWriter writer) throws IOException {
		Cursor cursor = null;
		int cnt = 0;
		cursor = contentProvider.getStrings();
		while(cursor.moveToNext()){
			BibTeXString str = contentProvider.cursorToString(cursor).getBibTeXString();
			writer.write( str );
			cnt++;
		}
		cursor.close();
		return cnt;
	}
	

	private int writeEntries( IWriter writer, List<BibTeXEntry> entries ) throws IOException {
		int total = 0;
		
		for(BibTeXEntry entry : entries){
			writer.write( entry );
			total++;
		}
		
		return total;
	}

	private void exportFiles(EratosUri archiveUri, EratosUri bibtexUri, String[] keys) throws IOException, ParseException {
		Set<String> keySet = new HashSet<String>(Arrays.asList(keys));
		FileManager fileManager = FileManager.getFileManager();

		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(contentProvider);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		File archiveFile = fileManager.getFile(archiveUri);
		if(archiveFile.exists())
			archiveFile.delete();
			
		List<String> attachedFiles = new ArrayList<String>();
		Cursor cursor = contentProvider.getAllEntries();
		int i = 0;
		int max = cursor.getCount();
		while(cursor.moveToNext()){
			String key  = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_KEY) );
			if(keySet.contains(key)){
				BibTeXEntryModel entry = contentProvider.cursorToEntry(cursor, modelFactory);
				
				for(EratosUri uri : entry.getFiles()){
					File file = fileManager.getFile(uri);
					attachedFiles.add(file.getAbsolutePath());
				}
			}
			
			i++;
			publishProgress(i, max);
		}
		
		cursor.close();
		
		File bibtexFile = fileManager.getFile(bibtexUri);
		attachedFiles.add(bibtexFile.getAbsolutePath());
		
		new Compress(attachedFiles, archiveFile).zip();
	}

	protected List<BibTeXEntry> exportBibtexEntries(String [] keys, boolean exportFiles) throws IOException, ParseException {
		List<BibTeXEntry> entries = null;
		
		if(keys.length > 0)
			entries = parseKeys(keys, exportFiles);
		else 
			entries = parseAll();
		
		return entries;
	}

	private List<BibTeXEntry> parseAll() throws IOException, ParseException {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		StringBuffer sb = new StringBuffer();
		
		Cursor cursor = contentProvider.getAllEntries();
		int i = 0;
		int max = cursor.getCount();
		while(cursor.moveToNext()){
			String lines[] = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_ENTRY)).split("\n");
			for(String line : lines){			
				sb.append(line);
				sb.append("\n");
			}
			sb.append( "\n" );
			i++;
			publishProgress(i, max);
		}
		
		cursor.close();
		
		return helper.parseBibTeXEntries(new StringReader(sb.toString()));
	}

	private List<BibTeXEntry> parseKeys( String [] keys, boolean exportFiles ) throws IOException, ParseException {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		Set<String> keySet = new HashSet<String>(Arrays.asList(keys));
		StringBuffer sb = new StringBuffer();
		
		Cursor cursor = contentProvider.getAllEntries();
		int i = 0;
		int max = cursor.getCount();
		while(cursor.moveToNext()){
			String key  = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_KEY) );
			if(keySet.contains(key)){
				String entry = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_ENTRY) );
				
				if(exportFiles)
					entry = localizeFiles(entry);
				else
					entry = removeFiles(entry);
				
				sb.append(entry + "\n\n");
			}

			i++;
			publishProgress(i, max);
		}
		cursor.close();
		
		return helper.parseBibTeXEntries(new StringReader(sb.toString()));
	}
	

	private String localizeFiles(String entryStr) throws IOException, ParseException {
		BibTeXEntry entry = importEntry(entryStr);
		
		Map<Key, Value> fields = entry.getFields();
		
		for(Key k : fields.keySet()){
			String key = k.getValue().toLowerCase(Locale.US);
			if(BibTeXEntryVerifier.fieldStartsWith(key, BibTeXEntryModel.eratosthenesFileFieldPrefix)){
				EratosUri uri = EratosUri.parseUri( fields.get(k).toUserString() );
				String filename = uri.name();
				EratosUri fUri = EratosUri.parseUri( EratosUri.LIBRARY_LOCAL_PROTOCOL + "://" + filename );
				entry.addField(k, new StringValue(fUri.toString(), Style.BRACED));
			}
		}
		
		return exportEntry(entry);
	}
	
	private String removeFiles(String entryStr) throws IOException, ParseException {
		BibTeXEntry entry = importEntry(entryStr);
		
		Map<Key, Value> fields = entry.getFields();
		
		List<Key> removeFields = new ArrayList<Key>();
		for(Key k : fields.keySet()){
			String key = k.getValue().toLowerCase(Locale.US);
			if(BibTeXEntryVerifier.fieldStartsWith(key, BibTeXEntryModel.eratosthenesFileFieldPrefix)){
				removeFields.add(k);
			}
		}
		
		for(Key k : removeFields){
			entry.removeField(k);
		}
		
		return exportEntry(entry);
	}
	
	private String exportEntry(BibTeXEntry entry) throws IOException, ParseException {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		return helper.formatBibTeX(entry);
	}

	private BibTeXEntry importEntry(String entryStr) throws IOException, ParseException {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		
		List<BibTeXEntry> entries = helper.parseBibTeXEntries(new StringReader(entryStr));
		BibTeXEntry entry = entries.get(0);
		
		return entry;
	}

	public static void autosaveDatabase(Context context, ExportBibListener listener) {
		EratosApplication application = EratosApplication.getApplication();
		application.markUnsaved();
		
		if(application.getSettingsManager().getAutosaveEnabled())
			saveDatabase(context, listener);
	}
	
	public static void saveDatabase(Context context, ExportBibListener listener){
		EratosApplication application = EratosApplication.getApplication();
		EratosUri libraryUri = application.getCurrentLibrary();
		new SaveDatabaseTask(context, libraryUri, null, application.getCurrentLibraryType(), application.getCurrentLibraryEncoding(), true, false, true, listener).execute();
	}
	
	public static void exportEntries(Context context, EratosUri bibtexUri, String bibType, String encoding, String [] keys, ExportBibListener listener) {
		new SaveDatabaseTask(context, bibtexUri, null, bibType, encoding, false, false, false, listener).execute(keys);
	}
	
	public static void exportEntries(Context context, EratosUri bibtexUri, String bibType, String encoding, EratosUri archiveUri, String [] keys, ExportBibListener listener) {
		new SaveDatabaseTask(context, bibtexUri, archiveUri, bibType, encoding, false, true, false, listener).execute(keys);
	}
}
