package com.mm.eratos.task;

import android.os.AsyncTask;

import com.mm.eratos.files.AbstractProgressFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class DownloadPdfFileTask extends AsyncTask<EratosUri, Integer, EratosUri>{

	private AbstractProgressFileOperationResultListener listener;
	private Throwable error;
	
	public DownloadPdfFileTask(AbstractProgressFileOperationResultListener listener){
		this.listener = listener;
	}
	
	@Override
	protected void onPostExecute(EratosUri result) {
		if(result == null)
			listener.failed(error);
		else
			listener.finished(new FileResult(result));
		
	}

	@Override
	protected void onPreExecute() {
		listener.started("Downloading PDF");
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		listener.progress("Downloading...", values[0], values[1]);
	}

	@Override
	protected EratosUri doInBackground(EratosUri... params) {
        try {
            EratosUri srcUri = params[0];
            EratosUri destUri = params[1];
            
            WebClient.newInstance().downloadPdfFile(srcUri.toString(), destUri, new DownloadListener() {
				public void progress(long total, long fileSize) {
					publishProgress((int)total, (int)fileSize);
				}
			});
            
            return params[1];
        } catch (Exception e) {
        	error = e;
        } catch(Error e){
			e.printStackTrace();
			error = e;
		}
        return null;
	}
}
