package com.mm.eratos.task;

import android.content.Context;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.application.EratosApplication;
import java.util.List;

import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.SyncedFile;

import java.io.File;
import android.os.AsyncTask;

public class PruneFilesTask extends AsyncTask<Void, Void, Void>
{
	FileRevisionContentProviderAdapter fileAdapter;
	public PruneFilesTask(Context mContext){
		fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
	}
	
	@Override
	protected Void doInBackground(Void ... args){
		EratosApplication app = EratosApplication.getApplication();
		boolean storeRecent = app.getSettingsManager().storeOnlyRecentFiles();
		int limit = app.getSettingsManager().getStorageHistory();

		int fCnt = fileAdapter.countUnimportantFiles();
		if(storeRecent && limit < fCnt) {
			List<SyncedFile> sfiles = fileAdapter.getOldestFiles( fCnt - limit );
			for(SyncedFile sf : sfiles) {
				fileAdapter.update(sf.setCached(false));
				File localFile = FileManager.getFileManager().getFile(sf.uri());
				if(localFile.exists())
					localFile.delete();
			}
		}
		return null;
	}
	
}
