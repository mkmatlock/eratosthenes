package com.mm.eratos.task;

import android.app.Activity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.task.SaveDatabaseTask.ExportBibListener;
import com.mm.eratos.utils.NotificationUtils;

public class FileSyncResultReceiver implements ExportBibListener {

	private Activity mContext;

	public FileSyncResultReceiver(Activity mContext){
		this.mContext = mContext;
	}
	
	@Override
	public void started() {
		mContext.setProgressBarVisibility(Boolean.TRUE);
	}

	@Override
	public void finished(EratosUri exportUri, int entries, int objects, int strings) {
		mContext.setProgressBarVisibility(Boolean.FALSE);
		
		EratosApplication app = EratosApplication.getApplication();
		EratosUri currentLibrary = app.getCurrentLibrary();
		NotificationUtils.longToast(mContext, "Saved "+entries+" entries");
		
		if(exportUri.equals(currentLibrary) && currentLibrary.isRemoteProtocol(app))
			NotificationUtils.shortToast(mContext, "Don't forget to sync to dropbox!");
	}

	@Override
	public void failed(Throwable error) {
		NotificationUtils.notifyException(mContext, "Save Failed", error);
	}

	@Override
	public void progress(int progress, int maxprogress) {
		progress = progress*10000 / maxprogress;
		mContext.setProgress(progress);
	}
}
