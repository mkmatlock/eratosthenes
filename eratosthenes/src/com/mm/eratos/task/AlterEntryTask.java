package com.mm.eratos.task;

import java.util.Arrays;

import android.app.Activity;
import android.os.AsyncTask;

import com.mm.eratos.bibtex.operations.BibTeXEntryTransaction;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;

public class AlterEntryTask extends AsyncTask<String, Integer, Throwable> {
	private BibTeXEntryTransaction operator;
	private Activity mContext;

	private MultiProgressDialog progressDialog;
	private String message;
	private String title;
	private String downloadMessage;
	private Integer progress;
	private Integer maxprogress;
	private DownloadListener downloadListener;

	public AlterEntryTask(Activity mContext, String title, BibTeXEntryTransaction operator){
		this.title = title;
		this.mContext = mContext;
		this.operator = operator;
		this.downloadListener = new WebClient.DownloadListener() {
			public void progress(long downloaded, long fileSize) {
				publishProgress(progress, maxprogress, (int)downloaded, (int)fileSize);
			}
		};
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		message = "Processing Entries...";
		progressDialog = MultiProgressDialog.newInstance(title, message);
		progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		progress = values[0];
		maxprogress = values[1];
		progressDialog.setPrimaryProgress(progress, maxprogress);
		
		if(values.length>2){
			progressDialog.setSecondaryProgress(values[2], values[3]);
			progressDialog.setSecondarySubtitle(downloadMessage);
		}else{
			progressDialog.hideSecondary();
		}
	}


	@Override
	protected Throwable doInBackground(String... keys) {
		try {
			operator.registerDownloadListener(downloadListener);
			
			operator.executeOnKeys(Arrays.asList(keys), new ProgressCallback() {
				public void update(String stage, int progress, int maxProgress) {
					publishProgress(progress, maxProgress);
				}
				public void finished() {
				}
				public void error(Throwable error) {
				}
			});
			
			operator.unregisterDownloadListener();
		} catch (Exception e) {
			e.printStackTrace();
			return e;
		} catch (Error e) {
			e.printStackTrace();
			return e;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Throwable result) {
		super.onPostExecute(result);

		progressDialog.dismiss();		
		if(result != null){
			NotificationUtils.notifyException(mContext, "Operation Failed", result);
		}else{
			NotificationUtils.longToast(mContext, "Finished task: " + title);
		}
	}
}
