package com.mm.eratos.protocol;

import java.util.Locale;

import com.mm.eratos.model.EratosUri;

public class SyncedFileOp {
	public static final int FILE_OPERATION_MOVE = 1;

	final private int id;
	final private int type;
	final private EratosUri uri;
	final private String result;
	final private boolean committed;
	
	protected SyncedFileOp(int type, EratosUri uri, String result, boolean committed) {
		this(-1, type, uri, result, committed);
	}
	
	public SyncedFileOp(int id, int type, EratosUri uri, String result, boolean committed) {
		this.id = id;
		this.type = type;
		this.uri = uri;
		this.result = result;
		this.committed = committed;
	}
	
	public int id(){
		return id;
	}
	public int type(){
		return type;
	}
	public EratosUri uri(){
		return uri;
	}
	public EratosUri dest(){
		return EratosUri.parseUri(result);
	}
	public String result(){
		return result;
	}
	public boolean committed(){
		return committed;
	}
	
	public String toString(){
		return String.format(Locale.US, "op%d: %s( %s ) = %s, commited? %s", id, getTypeString(type), uri.toString(), result, Boolean.toString(committed));
	}

	public SyncedFileOp commit(){
		return new SyncedFileOp(id, type, uri, result, true);
	}
	
	public SyncedFileOp setId(int nId){
		return new SyncedFileOp(nId, type, uri, result, true);
	}
	
	private static Object getTypeString(int type) {
		switch(type){
		case FILE_OPERATION_MOVE:
			return "move";
		}
		return "";
	}

	public static SyncedFileOp createFileMoveOp(EratosUri srcUri, EratosUri dstUri) {
		return new SyncedFileOp(FILE_OPERATION_MOVE, srcUri, dstUri.toString(), false);
	}
}
