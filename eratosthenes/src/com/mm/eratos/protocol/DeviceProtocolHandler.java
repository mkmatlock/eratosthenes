package com.mm.eratos.protocol;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;

public class DeviceProtocolHandler extends ProtocolHandler
{
	private FileManager fileManager;

	public DeviceProtocolHandler() {
		fileManager = FileManager.getFileManager();
	}

	@Override
	public EratosUri root() {
		return EratosUri.parseUri("device:///");
	}

	@Override
	public boolean isRoot(EratosUri uri) {
		return uri.protocol().equals("root") && uri.path().equals("/");
	}

	@Override
	public EratosUri parent(EratosUri uri) {
		if(uri.equals(root())){
			EratosUri rootPath = EratosUri.parseUri("root://" + FileManager.getFileManager().getFile(uri).getAbsolutePath());
			return rootPath.parent();
		}
		return uri.parent();
	}


	@Override
	public FileResult move(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);
		assertProtocol(dst);
		return copy(src, dst, true);
	}


	@Override
	public FileResult put(EratosUri src, EratosUri dst)	throws ProtocolException {
		assertProtocol(dst);
		return copy(src, dst, false);
	}

	@Override
	public FileResult put(EratosUri src, EratosUri dst,	IFileOperationProgressListener listener) throws ProtocolException {
		assertProtocol(dst);
		return copy(src, dst, false);
	}

	
	@Override
	public FileResult copy(EratosUri src, EratosUri dst) throws ProtocolException {
		assertProtocol(src);
		assertProtocol(dst);
		return copy(src, dst, false);
	}
	
	public FileResult copy(EratosUri src, EratosUri dst, boolean deleteSrc) throws ProtocolException {
		File srcFile = fileManager.getFile(src);
		File dstFile = fileManager.getFile(dst);
		
		try {
			FileUtils.copy(srcFile, dstFile);
			if(deleteSrc) srcFile.delete();
			
			return new FileResult(dst);
		} catch(FileNotFoundException fe){
			throw new ProtocolNoSuchFileException(fe);
		} catch (IOException e) {
			throw new ProtocolException(e);
		}
	}


	@Override
	public FileResult list(EratosUri dirUri) throws ProtocolException {
		File dir = fileManager.getFile(dirUri);
		
		List<FileResult> listing = new ArrayList<FileResult>();
		for(File f : dir.listFiles()){
			FileResult child = new FileResult(dirUri.append(f.getName()), f.isDirectory());
			listing.add(child);
		}
		return new FileResult(dirUri, listing);
	}


	@Override
	public FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc) throws ProtocolException {
		return copy(src, dst, deleteSrc);
	}

	@Override
	public FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc, IFileOperationProgressListener listener) throws ProtocolException {
		return copy(src, dst, deleteSrc);
	}

	@Override
	public FileResult delete(EratosUri src) throws ProtocolException {
		File srcFile = fileManager.getFile(src);
		
		if(!srcFile.exists())
			throw new ProtocolNoSuchFileException("File does not exist: " + src);
		
		boolean success = srcFile.delete();
		if(!success)
			throw new ProtocolException("Error: could not delete file: " + src);
		
		return new FileResult(src);
	}


	@Override
	public FileResult createFolder(EratosUri dirUri, String name) throws ProtocolException {
		File dir = fileManager.getFile(dirUri);
		
		EratosUri childUri = dirUri.append(name);
		File child = fileManager.getFile(childUri);
		
		if(!dir.exists())
			throw new ProtocolNoSuchFileException("Directory does not exist: " + dirUri);
		if(!dir.isDirectory())
			throw new ProtocolNoSuchFileException("Not a directory: " + dirUri);
		if(!child.mkdir())
			throw new ProtocolException("Could not create directory: " + childUri);
		
		return new FileResult(childUri);
	}


	@Override
	public boolean isProtocol(EratosUri uri) {
		String protocol = uri.protocol();
		return protocol.equalsIgnoreCase(EratosUri.ROOT_PROTOCOL) || protocol.equalsIgnoreCase(EratosUri.FILE_PROTOCOL);
	}
	

	private void assertProtocol(EratosUri uri) throws ProtocolException {
		if(!isProtocol(uri))
			throw new ProtocolException("Protocol does not match: " + uri.protocol() + " != " + EratosUri.FILE_PROTOCOL + " or " + EratosUri.ROOT_PROTOCOL);
	}

	@Override
	public FileResult info(EratosUri src) throws ProtocolException {
		assertProtocol(src);
		File srcFile = fileManager.getFile(src);
		return new FileResult(src, "", srcFile.lastModified(), srcFile.isDirectory());
	}
	
	@Override
	public String name() {
		return "Local Device";
	}

	@Override
	public FileResult refresh(EratosUri dir) throws ProtocolException {
		return list(dir);
	}

	@Override
	public String protocol() {
		return EratosUri.FILE_PROTOCOL;
	}

	@Override
	public void clearCache() {
		
	}

	@Override
	public FileResult createFile(EratosUri dst) throws ProtocolException {
		try{
			if(fileManager.getFile(dst).createNewFile())
				return new FileResult(dst);
			throw new ProtocolException("Failed to create file: " + dst);
		}catch(Exception e){
			throw new ProtocolException(e);
		}
	}
}
