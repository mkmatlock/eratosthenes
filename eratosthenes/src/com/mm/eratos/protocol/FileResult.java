package com.mm.eratos.protocol;

import java.util.Collections;
import java.util.List;

import com.mm.eratos.model.EratosUri;

public class FileResult {
	List<FileResult> listing;
	boolean isDir;
	EratosUri uri;
	
	String rev;
	long modified;
	
	
	public FileResult(EratosUri uri){
		this.uri = uri;
		this.isDir=false;
		this.listing = null;
	}

	public FileResult(EratosUri uri, String rev, long modified, boolean isDir){
		this.uri = uri;
		this.rev = rev;
		this.modified = modified;
		this.isDir = isDir;
		this.listing = null;
	}
	
	public FileResult(EratosUri uri, List<FileResult> listing){
		this.uri = uri;
		this.isDir = true;
		this.listing = Collections.unmodifiableList(listing);
	}
	
	public FileResult(EratosUri uri, boolean isDir) {
		this.uri = uri;
		this.isDir = isDir;
		this.listing = null;
	}

	public boolean isDir(){
		return isDir;
	}
	
	public List<FileResult> contents(){
		return listing;
	}
	
	public EratosUri uri(){
		return uri;
	}

	public boolean hasChild(String fn) {
		if(listing != null){
			for(FileResult file : listing){
				if(file.uri().name().equals(fn))
					return true;
			}
		}
		return false;
	}
	
	public long modified(){
		return modified;
	}
	
	public String revision(){
		return rev;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileResult other = (FileResult) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}
}
