package com.mm.eratos.protocol;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.mm.eratos.database.DBHelper;

public class FileRevisionContentProvider extends ContentProvider {
	private DBHelper dbHelper;

	private static final int FILES = 10;
	private static final int FILE = 30;
	private static final int FILE_OPERATIONS = 50;
	
	private static final String AUTHORITY = "com.mm.eratos.files.contentprovider";

	private static final String FILE_PATH = "files";
	private static final String FILE_OPERATIONS_PATH = "file_operations";
	
	public static final Uri FILE_URI = Uri.parse("content://" + AUTHORITY + "/" + FILE_PATH);
	public static final Uri FILE_OPERATIONS_URI = Uri.parse("content://" + AUTHORITY + "/" + FILE_OPERATIONS_PATH);
	
	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, FILE_PATH, FILES);
		sURIMatcher.addURI(AUTHORITY, FILE_PATH + "/*", FILE);
		sURIMatcher.addURI(AUTHORITY, FILE_OPERATIONS_PATH, FILE_OPERATIONS);
	}

	@Override
	public boolean onCreate() {
		dbHelper = new DBHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		// Using SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		// Check if the caller has requested a column which does not exists
		checkColumns(projection);
		
		// Set the table
		queryBuilder.setTables(DBHelper.TABLE_FILES);
		
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case FILES:
			break;
		case FILE:
			queryBuilder.appendWhere(DBHelper.COLUMN_KEY + "=" + uri.getLastPathSegment());
			break;
		case FILE_OPERATIONS:
			queryBuilder.setTables(DBHelper.TABLE_FILE_OPERATIONS);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		//
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		// Make sure that potential listeners are getting notified
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		long id = 0;
		String path = "";
		switch (uriType) {
		case FILES:
			id = sqlDB.insert(DBHelper.TABLE_FILES, null, values);
			path = FILE_PATH;
			break;
		case FILE_OPERATIONS:
			id = sqlDB.insert(DBHelper.TABLE_FILE_OPERATIONS, null, values);
			path = FILE_OPERATIONS_PATH;
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(path + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case FILES:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_FILES, selection, selectionArgs);
			break;
		case FILE:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(DBHelper.TABLE_FILES,
						DBHelper.COLUMN_KEY + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(DBHelper.TABLE_FILES,
						DBHelper.COLUMN_KEY + "=" + id + " and " + selection,
						selectionArgs);
			}
			break;
		case FILE_OPERATIONS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_FILE_OPERATIONS, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {

		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case FILES:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_FILES, values, selection,
					selectionArgs);
			break;
		case FILE:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(DBHelper.TABLE_FILES, values,
						DBHelper.COLUMN_KEY + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(DBHelper.TABLE_FILES, values,
						DBHelper.COLUMN_KEY + "=" + id + " and " + selection,
						selectionArgs);
			}
			break;
		case FILE_OPERATIONS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_FILE_OPERATIONS, values, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

	private void checkColumns(String[] projection) {
		String[] available = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_URI,
				DBHelper.COLUMN_REVISION, DBHelper.COLUMN_LASTACCESS,
				DBHelper.COLUMN_MODIFIED, DBHelper.COLUMN_IMPORTANT, 
				DBHelper.COLUMN_DELETED, DBHelper.COLUMN_CACHED,
				DBHelper.COLUMN_PROTOCOL, DBHelper.COLUMN_OPERATION, 
				DBHelper.COLUMN_RESULT, DBHelper.COLUMN_COMMITTED };
		
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(available));
			// Check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException(
						"Unknown columns in projection");
			}
		}
	}
	
}
