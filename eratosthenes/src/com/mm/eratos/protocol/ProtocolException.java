package com.mm.eratos.protocol;


public class ProtocolException extends Exception {

	public ProtocolException(Exception e) {
		super(e);
	}

	public ProtocolException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
