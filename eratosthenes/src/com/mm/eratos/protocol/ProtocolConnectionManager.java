package com.mm.eratos.protocol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.mm.eratos.drive.DriveConnectionManager;
import com.mm.eratos.dropbox.DropboxConnectionManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.NotificationUtils;

public abstract class ProtocolConnectionManager {
	private static ProtocolConnectionManager lastProtocol;
	
	public interface ConnectionListener {
		public void connecting();
		public void connected();
		public void failed(Exception e);
	}

	public abstract static class ConnectionListenerNotifier implements ConnectionListener {
		private Context mContext;
		private ProtocolConnectionManager protocol;
		private ProgressDialog progressDialog;

		public ConnectionListenerNotifier(ProtocolConnectionManager protocol, Context mContext){
			this.protocol = protocol;
			this.mContext = mContext;
		}
		
		public abstract void success();
		
		public void connecting(){
			progressDialog = new ProgressDialog(mContext);
			progressDialog.setIndeterminate(true);
			progressDialog.setTitle("Connecting to " + protocol.name());
			progressDialog.setMessage("Connecting...");
			progressDialog.show();
		}
		
		public void connected(){
			progressDialog.dismiss();
			success();
		}
		
		public void failed(Exception e) {
			progressDialog.dismiss();
			
			if(e instanceof ProtocolInternetConnectionException){
				NotificationUtils.notify(mContext, "No Internet Connection", e.getMessage());
			}else{
				NotificationUtils.notifyException(mContext, protocol.name() + " Connection Failed", e);
			}
		}
	}
	
	public abstract String name();
	public abstract boolean sessionLinked();
	public abstract void connect(Activity mContext, ConnectionListener listener);
	public abstract void onResume(Activity mContext);
	public abstract void onPause(Activity mContext);
	public abstract void onActivityResult(Activity mContext, int requestCode, int resultCode, Intent data);
	
	public static ProtocolConnectionManager getConnectionManager(String protocol){
		if(protocol.equals(EratosUri.DROPBOX_PROTOCOL))
			lastProtocol = DropboxConnectionManager.getDropboxConnectionManager();
		else if(protocol.equals(EratosUri.DRIVE_PROTOCOL))
			lastProtocol = DriveConnectionManager.getDriveConnectionManager();
		else
			lastProtocol = null;
		return lastProtocol;
	}

	public static void notifyOnResume(Activity mContext){
		if(lastProtocol != null) lastProtocol.onResume(mContext);
	}
	
	public static void notifyOnActivityResult(Activity mContext, int requestCode, int resultCode, Intent data){
		if(lastProtocol != null) lastProtocol.onActivityResult(mContext, requestCode, resultCode, data);
	}
	public static void notifyOnPause(Activity mContext) {
		if(lastProtocol != null) lastProtocol.onPause(mContext);
	}
}
