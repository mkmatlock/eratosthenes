package com.mm.eratos.protocol;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;

public class SyncedFile {
	private final int id;
	private final EratosUri uri;
	private final String rev;
	private final long lastModified;
	
	private final boolean important;
	private final boolean deleted;
	private final File localFile;
	private final long lastAccessed;
	private final boolean cached;

	protected SyncedFile(int id, EratosUri uri, String rev, long lastAccessed, long lastModified, boolean cached, boolean important, boolean deleted) {
		this.id = id;
		this.uri = uri;
		this.rev = rev;
		this.lastAccessed = lastAccessed;
		this.lastModified = lastModified;
		this.cached = cached;
		this.important = important;
		
		this.localFile = FileManager.getFileManager().getFile(uri);
		this.deleted = deleted;
	}
	
	public int id(){
		return id;
	}
	
	public String lastRevision() {
		return rev;
	}

	public EratosUri uri(){
		return uri;
	}
	
	public long lastModified(){
		return lastModified;
	}

	public boolean important() {
		return important;
	}
	
	public boolean deleted() {
		return deleted;
	}
	
	public SyncedFile delete() {
		return new SyncedFile(id, uri, rev, lastAccessed, lastModified, cached, important, true);
	}

	public SyncedFile undelete() {
		return new SyncedFile(id, uri, rev, lastAccessed, lastModified, cached, important, false);
	}

	public SyncedFile assignId(int newId){
		return new SyncedFile(newId, uri, rev, lastAccessed, lastModified, cached, important, deleted);
	}

	public SyncedFile revision(String newRevision) {
		return new SyncedFile(id, uri, newRevision, lastAccessed, localFile.lastModified(), cached, important, deleted);
	}
	
	public SyncedFile accessed() {
		long now = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
		return new SyncedFile(id, uri, rev, now, lastModified, cached, important, deleted);
	}
	
	public SyncedFile setImportant(boolean isImportant) {
		long now = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
		return new SyncedFile(id, uri, rev, now, lastModified, cached, isImportant, deleted);
	}
	
	public SyncedFile setCached(boolean isCached) {
		long now = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
		return new SyncedFile(id, uri, rev, now, lastModified, isCached, important, deleted);
	}

	public long lastAccessed() {
		return lastAccessed;
	}

	public boolean cached() {
		return cached;
	}

	public SyncedFile setLastModified(long modified) {
		long now = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
		return new SyncedFile(id, uri, rev, now, modified, cached, important, deleted);
	}

	public SyncedFile setUri(EratosUri nUri) {
		long now = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
		return new SyncedFile(id, nUri, rev, now, lastModified, cached, important, deleted);
	}
	
	public String toString(){
		return String.format(Locale.US, "SyncedFile( %d, \"%s\", \"%s\", lastAccessed = %d, lastModified = %d, cached = %b, important = %b )",
								id, uri.toString(), rev, lastAccessed, lastModified, cached, important);
	}
}
