package com.mm.eratos.protocol;

import android.app.Activity;
import android.os.AsyncTask;

import com.mm.eratos.files.AbstractProgressFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.task.ProtocolUploadTask;
import com.mm.eratos.task.SaveDatabaseTask.ExportBibListener;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolUploadResultReceiver implements ExportBibListener {
	private final Activity mContext;
	private final boolean remote;
	private ProtocolHandler handler;

	public ProtocolUploadResultReceiver(Activity mContext, ProtocolHandler handler, boolean remote){
		this.mContext = mContext;
		this.handler = handler;
		this.remote = remote;
	}
	
	@Override
	public void started() {
		mContext.setProgressBarVisibility(Boolean.TRUE);
	}

	@Override
	public void finished(EratosUri exportUri, int entries, int objects, int strings) {
		final int entryCnt = entries;
		if(remote) {
			new ProtocolUploadTask(handler,
				new AbstractProgressFileOperationResultListener(mContext) {
					public void success(FileResult result) {
						NotificationUtils.shortToast(mContext, String.format("Uploaded %d entries", entryCnt));
					}
					public void failure(Throwable error) {
						NotificationUtils.notifyException(mContext, handler.name() + " Export Failed", error);
					}
			}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, exportUri, exportUri);
		}else {
			NotificationUtils.shortToast(mContext, String.format("Saved %d entries", entries));
		}
	}

	@Override
	public void failed(Throwable error) {
		NotificationUtils.notifyException(mContext, "Export Failed", error);
	}

	@Override
	public void progress(int progress, int maxprogress) {
		progress = progress*10000 / maxprogress;
		mContext.setProgress(progress);
	}
}

