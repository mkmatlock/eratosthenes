package com.mm.eratos.protocol;

public class ProtocolConnectionException extends ProtocolException {
	public ProtocolConnectionException(Exception e) {
		super(e);
	}
	
	public ProtocolConnectionException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

