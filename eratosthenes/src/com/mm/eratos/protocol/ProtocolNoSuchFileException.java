package com.mm.eratos.protocol;

public class ProtocolNoSuchFileException extends ProtocolException {

	public ProtocolNoSuchFileException(Exception e) {
		super(e);
	}
	
	public ProtocolNoSuchFileException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
