package com.mm.eratos.protocol;

import android.os.AsyncTask;

import com.mm.eratos.drive.DriveProtocolHandler;
import com.mm.eratos.dropbox.DropboxProtocolHandler;
import com.mm.eratos.files.AbstractProgressFileOperationResultListener;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.task.ProtocolCopyTask;
import com.mm.eratos.protocol.task.ProtocolCreateEmptyFileTask;
import com.mm.eratos.protocol.task.ProtocolCreateFolderTask;
import com.mm.eratos.protocol.task.ProtocolDeleteTask;
import com.mm.eratos.protocol.task.ProtocolDownloadTask;
import com.mm.eratos.protocol.task.ProtocolListTask;
import com.mm.eratos.protocol.task.ProtocolUploadTask;

public abstract class ProtocolHandler
{
	public abstract void clearCache();
	public abstract FileResult info(EratosUri src) throws ProtocolException;
	
	public abstract FileResult move(EratosUri src, EratosUri dst) throws ProtocolException;
	public abstract FileResult put(EratosUri src, EratosUri dst) throws ProtocolException;
	public abstract FileResult put(EratosUri src, EratosUri dst, IFileOperationProgressListener listener) throws ProtocolException;
	public abstract FileResult copy(EratosUri src, EratosUri dst) throws ProtocolException;
	public abstract FileResult list(EratosUri dir) throws ProtocolException;
	public abstract FileResult refresh(EratosUri dir) throws ProtocolException;
	public abstract FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc) throws ProtocolException;
	public abstract FileResult fetch(EratosUri src, EratosUri dst, boolean deleteSrc, IFileOperationProgressListener listener) throws ProtocolException;
	
	public abstract FileResult delete(EratosUri src) throws ProtocolException;
	public abstract FileResult createFolder(EratosUri dir, String name) throws ProtocolException;
	public abstract FileResult createFile(EratosUri dst) throws ProtocolException;
	
	public abstract String protocol();
	public abstract String name();
	public abstract EratosUri root();
	public abstract EratosUri parent(EratosUri uri);
	public abstract boolean isRoot(EratosUri uri);
	public abstract boolean isProtocol(EratosUri uri);
	
	
	public void move(EratosUri src, EratosUri dst, IFileOperationResultListener listener){
		new ProtocolCopyTask(this, listener, true).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, src, dst);
	}
	
	public void put(EratosUri src, EratosUri dst, AbstractProgressFileOperationResultListener listener){
		new ProtocolUploadTask(this, listener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, src, dst);
	}
	
	public void copy(EratosUri src, EratosUri dst, IFileOperationResultListener listener){
		new ProtocolCopyTask(this, listener, false).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, src, dst);
	}
	
	public void list(EratosUri dir, IFileOperationResultListener listener){
		new ProtocolListTask(this, listener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, dir);
	}
	
	public void refresh(EratosUri dir, IFileOperationResultListener listener){
		new ProtocolListTask(this, listener, true).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, dir);
	}
	
	public void fetch(EratosUri src, EratosUri dst, boolean deleteSrc, IFileOperationResultListener resultListener, IFileOperationProgressListener progressListener){
		new ProtocolDownloadTask(this, resultListener, progressListener, deleteSrc).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, src, dst);
	}
	
	public void delete(EratosUri src, IFileOperationResultListener listener){
		new ProtocolDeleteTask(this, listener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, src);
	}
	
	public void createFolder(EratosUri dir, String name, IFileOperationResultListener listener){
		new ProtocolCreateFolderTask(this, listener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, dir.append( name ));
	}
	
	public static ProtocolHandler getProtocolHandler(EratosUri uri) {
		if(uri.protocol().equals(EratosUri.FILE_PROTOCOL) || uri.protocol().equals(EratosUri.ROOT_PROTOCOL)){
			return new DeviceProtocolHandler();
		}else if(uri.protocol().equals(EratosUri.DROPBOX_PROTOCOL)){
			return new DropboxProtocolHandler();
		}else if(uri.protocol().equals(EratosUri.DRIVE_PROTOCOL)){
			return new DriveProtocolHandler();
		}
		return null;
	}
	
	public void createFile(EratosUri dst, IFileOperationResultListener listener) {
		new ProtocolCreateEmptyFileTask(this, listener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, dst);
	}
}
