package com.mm.eratos.protocol;

public class ProtocolInternetConnectionException extends ProtocolException {
	public ProtocolInternetConnectionException(Exception e) {
		super(e);
	}
	
	public ProtocolInternetConnectionException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

