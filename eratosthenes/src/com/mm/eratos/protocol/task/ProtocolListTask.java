package com.mm.eratos.protocol.task;

import android.os.AsyncTask;

import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolHandler;

public class ProtocolListTask extends AsyncTask<EratosUri, Void, FileResult> {
	private final IFileOperationResultListener listener;
	private Throwable failure;
	private ProtocolHandler protocol;
	private boolean refresh;
	
	public ProtocolListTask(ProtocolHandler protocol, IFileOperationResultListener listener){
		this(protocol, listener, false);
	}

	public ProtocolListTask(ProtocolHandler protocol, IFileOperationResultListener listener, boolean refresh){
		this.protocol = protocol;
		this.listener = listener;
		this.refresh = refresh;
	}
	
	@Override
    protected void onPreExecute()
    {
		listener.started("Listing directory...");	
    };

	@Override
	protected FileResult doInBackground(EratosUri... params) {
		try{
			EratosUri uri = params[0];
			
			if(refresh)
				return protocol.refresh(uri);
			else
				return protocol.list(uri);
			
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(FileResult result) {
		if(failure!=null)
			listener.failed(failure);
		else
			listener.finished(result);
	}
}
