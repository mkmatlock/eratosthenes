package com.mm.eratos.protocol.task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolConnectionException;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolNoSuchFileException;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.protocol.SyncedFileOp;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolSyncLibraryFilesTask extends AsyncTask<Void, Integer, Void> {
	private ProtocolHandler protocol;
	private Activity mContext;
	private FileRevisionContentProviderAdapter revisionAdapter;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private FileManager fileManager;

	private List<Throwable> errors;
	private Throwable failure;
	
	private int total;
	private int deleted;
	
	String stage;
	String substage;
	int progress;
	int max;
	
	public ProtocolSyncLibraryFilesTask(ProtocolHandler protocol, Activity mContext){
		this.protocol = protocol;
		this.mContext = mContext;
		this.revisionAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.fileManager = FileManager.getFileManager();
	}
	

	@Override
    protected void onPreExecute()
    {
		stage = "Initializing...";
		progress = 0;
		max = 1;
		
		mContext.setProgressBarVisibility(Boolean.TRUE);
        
		errors = new ArrayList<Throwable>();
		total = 0;
		deleted = 0;
		failure = null;
    }
	
    @Override
	protected void onProgressUpdate(Integer ... values) {
    	progress=values[0];
    	max=values[1];
    	if(max == 0)
    		progress = 0;
    	else
    		progress = progress*10000 / max;
    	
    	if(values.length == 4)
    		progress += values[2] * 10000 / (max * values[3]);
    	
		mContext.setProgress(progress);
    }
	
	
	@Override
	protected Void doInBackground(Void... params) {
		try{
	    	moveFiles();
	    	
	    	List<SyncedFile> syncedFiles = buildSyncedFileList(bibtexAdapter);
	    	List<SyncedFile> push = new ArrayList<SyncedFile>();
	    	List<SyncedFile> pull = new ArrayList<SyncedFile>();
	    	List<SyncedFile> delete = new ArrayList<SyncedFile>();
	    	
	    	calculateSync(syncedFiles, push, pull, delete);
	    	
	    	progress = 0;
	    	max = push.size() + pull.size() + delete.size();
	    	
	    	pushFiles(push);
	    	pullFiles(pull);
			deleteFiles(delete);
	
		} catch(Exception e) {
			failure = e;
			e.printStackTrace();
		} catch(Error e) {
			failure = e;
			e.printStackTrace();
		}
		return null;
	}

	public List<SyncedFile> buildSyncedFileList(BibTeXContentProviderAdapter bibtexAdapter) {
		stage = "Getting local file revisions...";
		
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		Cursor c = bibtexAdapter.getAllEntries();
    	List<SyncedFile> syncedFiles = new ArrayList<SyncedFile>();
		int maxProgress = c.getCount();
		
		publishProgress(0, maxProgress);
		EratosApplication app = EratosApplication.getApplication();
		
		int i = 0;
    	while(c.moveToNext()){
    		if(bibtexAdapter.hasAttachments(c)){
	    		BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(c, modelFactory);
	    		for(EratosUri uri : entry.getFiles()){
	    			uri = uri.resolveUri(app);
					boolean remote = uri.isRemoteProtocol(app);
					boolean exists = revisionAdapter.exists(uri);
	
					if(exists){
						SyncedFile sf = revisionAdapter.getFile(uri).setImportant(entry.important());
						revisionAdapter.update(sf);
						syncedFiles.add(sf);
					}else if(remote){
						File localFile = fileManager.getFile(uri);
						syncedFiles.add(revisionAdapter.newSyncedFile(uri, "", localFile, entry.important()));
					}
	    		}
    		}
			i++;
			publishProgress(i, maxProgress);
    	}
    	c.close();

    	syncedFiles.addAll( revisionAdapter.getDeleted( protocol.protocol() ) );
    	return syncedFiles;
	}
	
	private void calculateSync(List<SyncedFile> syncedFiles, List<SyncedFile> push, List<SyncedFile> pull, List<SyncedFile> delete) throws ProtocolException {
		int i = 0;
		stage = "Calculating Sync...";
    	publishProgress(0, syncedFiles.size());
    	
		for(SyncedFile sf : syncedFiles) {
			File f = fileManager.getFile(sf.uri());
			
			try {
				FileResult remoteInfo = protocol.info(sf.uri());
				String rev = remoteInfo.revision();
				
				int syncDir = fileManager.decideSyncPriority(sf, rev, remoteInfo.modified());
				
				addFileToQueues(sf, syncDir, push, pull, delete);
				if(syncDir == FileManager.SYNC_UPDATE)
					revisionAdapter.update(sf.revision(rev));
			} catch(ProtocolConnectionException e){
				throw e;
			} catch(ProtocolNoSuchFileException e){
				if(!sf.deleted() && f.exists())
					push.add(sf);
				else if(sf.deleted()){
					revisionAdapter.delete(sf.id());
				}
			} catch(ProtocolException e){
				errors.add(e);
				e.printStackTrace();
			}
			
			i++;
			publishProgress(i, syncedFiles.size());
		}
	}

	private void addFileToQueues(SyncedFile sf, int sync_dir, List<SyncedFile> push, List<SyncedFile> pull, List<SyncedFile> delete) {
		switch(sync_dir){
		case FileManager.SYNC_PULL:
			pull.add(sf);
			break;
		case FileManager.SYNC_PUSH:
			push.add(sf);
			break;
		case FileManager.SYNC_DELETE:
			delete.add(sf);
			break;
		default:
			break;
		}
	}
	
	private void pushFiles(List<SyncedFile> push) {
		stage = "Pushing local changes...";
		publishProgress(progress, max);
		
		for(SyncedFile file : push){
			try{
				substage = "Uploading: " + file.uri().toString();
				pushFile(file, new IFileOperationProgressListener(){
					public void progress(String stage, int bytes, int total) {
						publishProgress(progress, max, bytes, total);
					}
    			});
				total += 1;
			}catch(Exception e) {
				errors.add(e);
				e.printStackTrace();
			}
			progress+=1;
			publishProgress(progress, max);
		}
	}

	private void pushFile(SyncedFile file, IFileOperationProgressListener listener) throws ProtocolException {
		FileResult item = protocol.put(file.uri(), file.uri(), listener);
		revisionAdapter.update(file.revision(item.revision()).setCached(true));
	}

	private void deleteFiles(List<SyncedFile> delete) {
		stage = "Removing deleted files...";
		publishProgress(progress, max);
		
		for(SyncedFile sf : delete){
			try{
				File f = fileManager.getFile(sf.uri());
				if(f.exists())
					f.delete();
				
				protocol.delete(sf.uri());
				
				deleted += 1;
				revisionAdapter.delete(sf.id());
			}catch(ProtocolException e){
				errors.add(e);
				e.printStackTrace();
			}

			progress++;
			publishProgress(progress, max);
		}
	}

	private void pullFiles(List<SyncedFile> pull) {
		stage = "Pulling remote changes...";
		publishProgress(progress, max);
		
		for(SyncedFile file : pull){
			try{
				substage = "Downloading: " + file.uri().toString();
				pullFile(file, new IFileOperationProgressListener(){
					public void progress(String stage, int bytes, int total) {
						publishProgress(progress, max, bytes, total);
					}
    			});
				total += 1;
			}catch(Exception e) {
				errors.add(e);
				e.printStackTrace();
			}
			progress+=1;
			publishProgress(progress, max);
		}
	}

	private void pullFile(SyncedFile file, IFileOperationProgressListener listener) throws IOException, ProtocolException {
		File localFile = fileManager.getFile(file.uri());
		EratosUri tmpUri = fileManager.getTemporaryUri("temp.file");
		File tmpFile = fileManager.getFile(tmpUri);
		
		File syncDir = localFile.getParentFile();
		if(!syncDir.exists() && !syncDir.mkdirs())
			throw new IOException("Could not create local sync directory: " + syncDir.getAbsolutePath());
		
		FileResult entry = protocol.fetch(file.uri(), tmpUri, false, listener);
		
		FileUtils.copy(tmpFile, localFile);
		localFile.setReadable(true);
		localFile.setWritable(true);
		
		
		revisionAdapter.update(file.revision(entry.revision()).setCached(true).setLastModified(localFile.lastModified()));
	}

	private void moveFiles() {
		Collection<? extends SyncedFileOp> moveOps = revisionAdapter.getFileOperations(protocol.protocol());
		
		stage = "Updating File History...";
		int progress = 0;
		int max = moveOps.size();
		publishProgress(progress, max);
		
		for(SyncedFileOp op : moveOps) {
			try {
				execMoveOp(op);
			} catch(ProtocolNoSuchFileException e){
				errors.add(e);
				e.printStackTrace();
				revisionAdapter.updateFileOp(op.commit());
			} catch (ProtocolException e) {
				errors.add(e);
				e.printStackTrace();
			}
			progress+=1;
			publishProgress(progress, max);
		}
		
		revisionAdapter.clearCompletedFileOps(protocol.protocol());
	}

	private void execMoveOp(SyncedFileOp op) throws ProtocolException {
		EratosUri result = EratosUri.parseUri(op.result());
		FileResult file = protocol.move(op.uri(), result);
		revisionAdapter.updateFileOp(op.commit());
		
		revisionAdapter.delete(op.uri());
		if(revisionAdapter.exists(result)){
			SyncedFile sf = revisionAdapter.getFile(result);
			revisionAdapter.update(sf.revision(file.revision()));
		} else {
			revisionAdapter.newSyncedFile(result, file.revision(), fileManager.getFile(result), false);
		}
	}

	@Override
    protected void onPostExecute(Void result)
    {
		mContext.setProgressBarVisibility(Boolean.FALSE);
		
        if(failure != null && failure instanceof ProtocolNoSuchFileException) {
        	NotificationUtils.notifyException(mContext, "File Missing", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
        } else if(failure != null) {
        	NotificationUtils.notifyException(mContext, protocol.name() + " Sync Failed", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
        } else {
        	if(errors.size() > 0)
        		NotificationUtils.notifyWarningsAllowEmail(mContext, "Warnings During Sync", errors);
        	
        	NotificationUtils.longToast(mContext, String.format("Synced %d files (%d deleted, %d errors)", total, deleted, errors.size()));
        }
    }
}
