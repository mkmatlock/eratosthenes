package com.mm.eratos.protocol.task;

import android.os.AsyncTask;

import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolHandler;

public class ProtocolDeleteTask extends AsyncTask<EratosUri, Void, FileResult> {
	private final IFileOperationResultListener listener;
	private Throwable failure;
	private ProtocolHandler protocol;
	
	public ProtocolDeleteTask(ProtocolHandler protocol, IFileOperationResultListener listener){
		this.protocol = protocol;
		this.listener = listener;
	}
	
	@Override
    protected void onPreExecute(){
		listener.started("Deleting File");
    }

	@Override
	protected FileResult doInBackground(EratosUri... params) {
		EratosUri uri = params[0];
		
		try {
			protocol.refresh(uri.parent());
			return protocol.delete(uri);
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		return null;
	}

	@Override
    protected void onPostExecute(FileResult result)
    {
		if(failure != null)
        	listener.failed(failure);
		else
			listener.finished(result);
    }
}
