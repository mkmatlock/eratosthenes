package com.mm.eratos.protocol.task;

import java.io.File;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolClearCacheTask extends AsyncTask<Void, Integer, Void> {
	private Activity mContext;
	private FileRevisionContentProviderAdapter revisionAdapter;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private MultiProgressDialog progressDialog;
	private String stage;
	private int maxProgress;
	private int progress;
	private int deleted;
	private int important;
	private EratosApplication app;
	private FileManager fileManager;
	private ProgressCallback listener;

	public ProtocolClearCacheTask(Activity mContext, ProgressCallback listener){
		this.mContext = mContext;
		this.listener = listener;
		this.revisionAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.stage = "Initializing...";

		app = EratosApplication.getApplication();
		fileManager = FileManager.getFileManager();
	}
	public void createProgressDialog(){
		progressDialog = MultiProgressDialog.newInstance("Clearing Dropbox Cache", stage);
        progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}
	
	@Override
    protected void onPreExecute()
    {
		stage = "Initializing...";
        createProgressDialog();
    };

    @Override
	protected void onProgressUpdate(Integer ... values) {
    	progressDialog.setPrimarySubtitle(stage);
    	progressDialog.setPrimaryProgress(values[0], values[1]);
		listener.update(stage, values[0], values[1]);
	}
    
	@Override
	protected Void doInBackground(Void... params) {
		Cursor cursor = bibtexAdapter.getAllEntries();
		
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		stage = "Clearing Cached Files...";
		maxProgress = cursor.getCount();
		progress = 0;
		publishProgress(0, maxProgress);
		
		deleted = 0;
		important = 0;
		
		while(cursor.moveToNext()){
			if(bibtexAdapter.hasAttachments(cursor)){
				BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(cursor, modelFactory);
				
				for(EratosUri uri : entry.getFiles()) {
					uri = uri.resolveUri(app);
					if(uri.isRemoteProtocol(app) && revisionAdapter.exists(uri)) {
						SyncedFile sf = revisionAdapter.getFile(uri);
						File f = fileManager.getFile(sf.uri());
						
						if(!sf.important() && f.exists()){
							f.delete();
							deleted++;
						}
						
						if(sf.important())
							important++;
						else
							revisionAdapter.update(sf.setCached(false));
					}
				}
			}
			progress++;
			publishProgress(progress, maxProgress);
		}
		return null;
	}
	

	@Override
    protected void onPostExecute(Void total) {
		progressDialog.dismissCheckVisible();
		NotificationUtils.longToast(mContext, String.format("Removed %d cached files, kept %d important files", deleted, important));
		listener.finished();
    }

}
