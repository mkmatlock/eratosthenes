package com.mm.eratos.protocol.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolExportTask extends AsyncTask<EratosUri, Integer, EratosUri> {

	private Activity mContext;
	private FileRevisionContentProviderAdapter revisionAdapter;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private MultiProgressDialog progressDialog;
	private ArrayList<Exception> errors;
	private int total;
	private Throwable failure;
	private String stage;
	private String subtitle;
	private int progress;
	private int maxProgress;
	private ProtocolHandler protocol;
	private FileManager fileManager;
	private ProgressCallback listener;

	public ProtocolExportTask(ProtocolHandler protocol, Activity mContext, ProgressCallback listener){
		this.protocol = protocol;
		this.mContext = mContext;
		this.listener = listener;
		this.revisionAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.fileManager = FileManager.getFileManager();
	}
	
	public void createProgressDialog(){
		progressDialog = MultiProgressDialog.newInstance("Exporting to " + protocol.name(), stage);
        progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}
	
	@Override
    protected void onPreExecute()
    {
		stage = "Starting...";
        createProgressDialog();
        
		errors = new ArrayList<Exception>();
		total = 0;
		failure = null;
    };

    @Override
	protected void onProgressUpdate(Integer ... values) {
    	progressDialog.setPrimarySubtitle(stage);
    	progressDialog.setPrimaryProgress(values[0], values[1]);
    	
    	listener.update(stage, values[0], values[1]);
    	
    	if(values.length > 2){
    		progressDialog.setSecondarySubtitle(subtitle);
    		progressDialog.setSecondaryProgress(values[2], values[3]);
    	}else
    		progressDialog.hideSecondary();
	}
	
	@Override
	protected EratosUri doInBackground(EratosUri... params) {
		EratosApplication app = EratosApplication.getApplication();
		BibTeXHelper helper = app.getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		EratosUri bibtexUri = params[0];
		EratosUri exportUri = params[1];
		EratosUri libUri = app.getCurrentLibraryPath();
		
		try{
			protocol.clearCache();
			
			File syncDir = fileManager.getFile(exportUri);
			if(!syncDir.exists() && !syncDir.mkdirs())
				throw new Exception("Could not create local sync directory!");
			
			stage = "Listing Files...";
			Cursor cursor = bibtexAdapter.getAllEntries();
			progress = 0;
			maxProgress = cursor.getCount();
			publishProgress(0, maxProgress);
			
			Map<EratosUri, Boolean> fileList = new HashMap<EratosUri, Boolean>();
			while(cursor.moveToNext()){
				if(bibtexAdapter.hasAttachments(cursor)){
					BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(cursor, modelFactory);
					for(EratosUri fUri : entry.getFiles()){
						File localFile = fileManager.getFile(fUri);
						if(localFile.exists())
							fileList.put(fUri.resolveUri(app), entry.important());
						else
							errors.add(new FileNotFoundException("No such file: " + fUri.toString()));
					}
				}
				
				progress++;
				publishProgress(progress, maxProgress);
			}
			cursor.close();
			fileList.put( bibtexUri, true );
			
			Map<EratosUri, Boolean> uploadFiles = new HashMap<EratosUri, Boolean>();
			
			stage = "Copying Library...";
			progress = 0;
			maxProgress = fileList.size();
			publishProgress(0, maxProgress);
			
			for(EratosUri fUri : fileList.keySet()){
				EratosUri remoteUri = copyFile(fUri, exportUri, libUri);
				if(remoteUri != null)
					uploadFiles.put(remoteUri, fileList.get(fUri));
				progress++;
				publishProgress(progress, maxProgress);
			}
			
			stage = "Uploading to "+protocol.name()+"...";
			
			progress = 0;
			maxProgress = fileList.size();
			publishProgress(0, maxProgress);
			
			List<SyncedFile> remoteFiles = new ArrayList<SyncedFile>();
			for(EratosUri remoteUri : uploadFiles.keySet()){
				FileResult entry = protocol.put(remoteUri, remoteUri, new IFileOperationProgressListener() {
					public void progress(String stage, int bytes, int total) {
						ProtocolExportTask.this.stage = stage;
						publishProgress(progress, maxProgress, (int)bytes, (int)total);
					}
				});
				
				boolean important = uploadFiles.get(remoteUri);
				File localFile = fileManager.getFile(remoteUri);
				SyncedFile sf = revisionAdapter.newSyncedFile(remoteUri, entry.revision(), localFile, important);
				remoteFiles.add(sf);
				
				progress++;
				publishProgress(progress, maxProgress);
			}
			
			for(SyncedFile sf : remoteFiles){
				File iFile = fileManager.getFile(sf.uri());
				if(!fileManager.keepCached(sf)){
					iFile.delete();
					revisionAdapter.update(sf.setCached(false));
				}
			}
			return exportUri.append(bibtexUri.name());
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		
		return null;
	}

	private EratosUri copyFile(EratosUri srcUri, EratosUri exportUri, EratosUri libUri) throws IOException {
		FileManager fileManager = FileManager.getFileManager();
		
		if(srcUri.childOf(libUri)){
			EratosUri remoteUri = exportUri.append( srcUri.relativeTo(libUri) );
			
			File localFile = fileManager.getFile(srcUri);
			File destFile = fileManager.getFile(remoteUri);
			
			if(!localFile.equals(destFile))
				FileUtils.copy(localFile, destFile);
			
			return remoteUri;
		}
		return null;
	}

	@Override
    protected void onPostExecute(EratosUri uri) {
		progressDialog.dismissCheckVisible();
		if (failure != null){
			NotificationUtils.notifyException(mContext,	protocol.name() + " Export Failed", failure);
			listener.error(failure);
		}else{
			NotificationUtils.longToast(mContext, String.format("Exported %d files (%d errors)", total, errors.size()));
			listener.finished();
		}
    }
}
