package com.mm.eratos.protocol.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.SyncedFile;

public class ProtocolCheckUpdateTask extends AsyncTask<EratosUri, Void, Boolean> {
	public interface Callback {
		public void started();
		public void updateAvailable();
		public void noChange();
		public void error(Throwable e);
	}
	
	private Throwable failure;
	private Callback listener;
	private ProtocolHandler protocol;
	private FileRevisionContentProviderAdapter fileAdapter;
	private FileManager fileManager;
	
	public ProtocolCheckUpdateTask(Context mContext, ProtocolHandler protocol, Callback listener){
		this.protocol = protocol;
		this.listener = listener;
		fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		fileManager = FileManager.getFileManager();
	}
	
	@Override
	protected void onPreExecute() {
		listener.started();
	}
	
	@Override
	protected Boolean doInBackground(EratosUri... params) {
		EratosUri libUri = params[0];
		try {
			protocol.refresh(libUri.parent());
			
			FileResult remoteInfo = protocol.info(libUri);
			SyncedFile sf = fileAdapter.getFile(libUri);
			int sync_dir = fileManager.decideSyncPriority(sf, remoteInfo.revision(), remoteInfo.modified());
			
			return FileManager.SYNC_PULL == sync_dir;
		} catch (Exception e) {
			failure = e;
			e.printStackTrace();
		} catch (Error e){
			failure = e;
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		
		if(failure != null)
			listener.error(failure);
		else if(result)
			listener.updateAvailable();
		else
			listener.noChange();
	}

}
