package com.mm.eratos.protocol.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolNoSuchFileException;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolImportLibraryFilesTask extends AsyncTask<Void, Integer, EratosUri> {

	private FragmentActivity mContext;
	private ArrayList<Throwable> errors;
	private int total;
	private Throwable failure;
	private FileRevisionContentProviderAdapter revisionAdapter;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private int progress;
	private int maxProgress;
	private ProtocolHandler protocol;
	private FileManager fileManager;
	private EratosApplication app;
	private int max;
	
	public ProtocolImportLibraryFilesTask(ProtocolHandler protocol, FragmentActivity mContext){
		this.protocol = protocol;
		this.mContext = mContext;
		this.revisionAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		this.bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
		this.fileManager = FileManager.getFileManager();
		this.app = EratosApplication.getApplication();
	}
	
	@Override
    protected void onPreExecute()
    {
		progress = 0;
		max = 1;
		
		mContext.setProgressBarVisibility(Boolean.TRUE);
        
		errors = new ArrayList<Throwable>();
		total = 0;
		failure = null;
    }

    @Override
	protected void onProgressUpdate(Integer ... values) {
    	progress=values[0];
    	max=values[1];
    	if(max == 0)
    		progress = 0;
    	else
    		progress = progress*10000 / max;
    	
    	if(values.length == 4)
    		progress += values[2] * 10000 / (max * values[3]);
    	
		mContext.setProgress(progress);
	}
	
	@Override
	protected EratosUri doInBackground(Void... params) {
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		BibTeXContentProviderReferenceResolver resolver = new BibTeXContentProviderReferenceResolver(bibtexAdapter);
		BibTeXEntryModelFactory modelFactory = new BibTeXEntryModelFactory(resolver, helper);
		
		try{
			Map<EratosUri, Boolean> downloadFiles = new HashMap<EratosUri, Boolean>();
			
			Cursor cursor = bibtexAdapter.getAllEntries();
			maxProgress = cursor.getCount();
			publishProgress(0, maxProgress);
			progress = 0;
			while(cursor.moveToNext()){
				if(bibtexAdapter.hasAttachments(cursor)){
					BibTeXEntryModel entry = bibtexAdapter.cursorToEntry(cursor, modelFactory);
					
					for(EratosUri uri : entry.getFiles()){
						uri = uri.resolveUri(app);
						if(uri.isRemoteProtocol(app)){
							try{
								FileResult dbFile = protocol.info(uri);
								File localFile = fileManager.getFile(uri);
								
								if(!revisionAdapter.exists(uri)){
									SyncedFile sf = revisionAdapter.newSyncedFile(uri, dbFile.revision(), localFile, entry.important());
									if(fileManager.keepCached(sf))
										downloadFiles.put(uri, entry.important());
								}else if(!localFile.exists()){
									SyncedFile sf = revisionAdapter.getFile(uri);
									if(fileManager.keepCached(sf))
										downloadFiles.put(uri, entry.important());
								}
							}catch(Exception e){
								e.printStackTrace();
								errors.add(e);
							}
						}
					}
				}
				
				progress++;
				publishProgress(progress, maxProgress);
			}
			cursor.close();
			
			maxProgress = downloadFiles.size();
			publishProgress(0, maxProgress);
			progress = 0;
			for(EratosUri uri : downloadFiles.keySet()){
				try{
					downloadFile(uri, downloadFiles.get(uri), new IFileOperationProgressListener() {
						public void progress(String message, int bytes, int total) {
							publishProgress(progress, maxProgress, bytes, total);
						}
					});
				}catch(Exception e){
					e.printStackTrace();
					errors.add(e);
				}
				progress++;
				publishProgress(progress, maxProgress);
			}
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		
		return null;
	}

	private void downloadFile(EratosUri uri, boolean important, IFileOperationProgressListener progressListener) throws IOException, FileNotFoundException, ProtocolException {
		File localFile = fileManager.getFile(uri);
		EratosUri tmpUri = fileManager.getTemporaryUri("temp.file");
		File tmpFile = fileManager.getFile(tmpUri);
		
		File syncDir = localFile.getParentFile();
		if(!syncDir.exists() && !syncDir.mkdirs())
			throw new IOException("Could not create local sync directory: " + syncDir.getAbsolutePath());
		
		FileResult entry = protocol.fetch(uri, tmpUri, false, progressListener);
		
		FileUtils.copy(tmpFile, localFile);
		localFile.setReadable(true);
		localFile.setWritable(true);
		
		if(!revisionAdapter.exists(uri)){
			revisionAdapter.newSyncedFile(uri, entry.revision(), localFile, important);
		}else{
			SyncedFile syncFile = revisionAdapter.getFile(uri);
			revisionAdapter.update(syncFile.revision(entry.revision()).setLastModified(localFile.lastModified()).setCached(true));
		}
		total++;
	}
	
	@Override
    protected void onPostExecute(EratosUri remoteDB) {
		mContext.setProgressBarVisibility(Boolean.FALSE);
		
        if(failure != null && failure instanceof ProtocolNoSuchFileException) {
        	NotificationUtils.notifyException(mContext, "File Missing", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
        } else if(failure != null) {
        	NotificationUtils.notifyException(mContext, protocol.name() + " Import Failed", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
        } else {
        	if(errors.size() > 0)
        		NotificationUtils.notifyWarningsAllowEmail(mContext, "Warnings During Import", errors);
        	
        	NotificationUtils.longToast(mContext, String.format("Fetched %d files (%d errors)", total, errors.size()));
        }
    }
}
