package com.mm.eratos.protocol.task;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolNoSuchFileException;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.task.LoadLibraryTask;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolSyncTask extends AsyncTask<Void, Integer, Void> {
	private MultiProgressDialog progressDialog;
	private Activity mContext;
	private FileRevisionContentProviderAdapter revisionAdapter;
	
	private String stage;
	private String substage;
	
	private boolean forceReload;
	private boolean forceRevert;
	
	int progress, max;
	private String loadingWarningText;
	private ProgressCallback callback;
	private ProtocolHandler protocol;
	private FileManager fileManager;
	private EratosUri bibtexUri;
	private String bibType;
	
	private Throwable failure;
	private String encoding;
	private boolean readOnly;
	
	public ProtocolSyncTask(ProtocolHandler protocol, Activity mContext, EratosUri bibtexUri, String bibType, String encoding, boolean forceReload, boolean forceRevert, boolean readOnly, ProgressCallback callback){
		this.protocol = protocol;
		this.mContext = mContext;
		this.bibtexUri = bibtexUri;
		this.bibType = bibType;
		this.encoding = encoding;
		this.forceReload = forceReload;
		this.forceRevert = forceRevert;
		this.readOnly = readOnly;
		this.callback = callback;
		this.revisionAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		this.fileManager = FileManager.getFileManager();
	}
	
	public void createProgressDialog(){
		progressDialog = MultiProgressDialog.newInstance("Syncing with " + protocol.name(), stage);
        progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}
	
	@Override
    protected void onPreExecute()
    {
		stage = "Initializing...";
		progress = 0;
		max = 1;
		createProgressDialog();
		
		failure=null;
    };
    
    @Override
	protected void onProgressUpdate(Integer ... values) {
    	progressDialog.setPrimarySubtitle(stage);
    	progressDialog.setPrimaryProgress(values[0], values[1]);
    	
    	callback.update(stage, values[0], values[1]);
    	
    	if(values.length == 4){
    		progressDialog.setSecondarySubtitle(substage);
    		progressDialog.setSecondaryProgress(values[2], values[3]);
    	}else{
    		progressDialog.hideSecondary();
    	}
	}
    
    @Override
    protected Void doInBackground(Void ... params)
    {   
    	try{
			stage = "Syncing .bib database...";
			publishProgress(0,1);
			
			protocol.clearCache();
			
        	boolean reloadRequired = syncLibraryFile(bibtexUri, forceRevert || readOnly);
        	loadingWarningText = "";
        	if(reloadRequired || forceReload) {
				LoadLibraryTask loadLibraryTask = new LoadLibraryTask(mContext, 
						bibtexUri,
						bibType,
						encoding, 
						!forceReload,
						new ProgressCallback(){
						public void update(String stage, int progress, int maxProgress){
							ProtocolSyncTask.this.stage = stage;
							publishProgress(progress, maxProgress);
						}
						public void finished() {
						}
						public void error(Throwable e) {
						}
					});
				boolean success = loadLibraryTask.doInBackground();
				loadingWarningText = loadLibraryTask.getWarningText();
				if(!success){
					failure = loadLibraryTask.getError();
					return null;
				}
			}
		} catch(Exception e) {
			failure = e;
			e.printStackTrace();
		} catch(Error e) {
			failure = e;
			e.printStackTrace();
		}
    	return null;
    }

    private boolean syncLibraryFile(EratosUri bibtexUri, boolean forceRevert) throws ProtocolException, IOException {
		if(bibtexUri == null)
			return false;
		
		EratosApplication app = EratosApplication.getApplication();
		
    	if(bibtexUri.isRemoteProtocol(app)) {
    		SyncedFile sf = revisionAdapter.getFile(bibtexUri);
    		FileResult remoteInfo = protocol.info(bibtexUri);
    		
    		int sync_dir = fileManager.decideSyncPriority(sf, remoteInfo.revision(), remoteInfo.modified());
    		if(forceRevert)
				sync_dir = FileManager.SYNC_PULL;
			
    		switch(sync_dir){
    		case FileManager.SYNC_PUSH:
    			substage = "Uploading: " + bibtexUri.toString();
    			pushFile(sf, new IFileOperationProgressListener() {
					@Override
					public void progress(String message, int bytes, int total) {
						publishProgress(progress, max, bytes, total);
					}
				});
    			return false;
    		case FileManager.SYNC_PULL:
    			substage = "Downloading: " + bibtexUri.toString();
    			pullFile(sf, new IFileOperationProgressListener() {
					@Override
					public void progress(String message, int bytes, int total) {
						publishProgress(progress, max, bytes, total);
					}
				});
    			return true;
			default:
    		}
    	}
		return false;
	}
	

	private void pushFile(SyncedFile file, IFileOperationProgressListener listener) throws ProtocolException {
		FileResult item = protocol.put(file.uri(), file.uri(), listener);
		revisionAdapter.update(file.revision(item.revision()).setCached(true));
	}
	

	private void pullFile(SyncedFile file, IFileOperationProgressListener listener) throws IOException, ProtocolException {
		File localFile = fileManager.getFile(file.uri());
		File syncDir = localFile.getParentFile();
		if(!syncDir.exists() && !syncDir.mkdirs())
			throw new IOException("Could not create local sync directory: " + syncDir.getAbsolutePath());
		
		FileResult entry = protocol.fetch(file.uri(), file.uri(), false, listener);
		
		localFile.setReadable(true);
		localFile.setWritable(true);
		
		revisionAdapter.update(file.revision(entry.revision()).setCached(true).setLastModified(localFile.lastModified()));
	}
	

	@Override
    protected void onPostExecute(Void result)
    {
        progressDialog.dismissCheckVisible();
        if(failure != null && failure instanceof ProtocolNoSuchFileException) {
        	NotificationUtils.notifyException(mContext, "File Missing", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					callback.error(failure);
				}
			});
        } else if(failure != null) {
        	NotificationUtils.notifyException(mContext, protocol.name() + " Sync Failed", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					callback.error(failure);
				}
			});
        } else {
        	if(!loadingWarningText.isEmpty()) NotificationUtils.notify(mContext, "Warning", loadingWarningText, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					startLibraryFileSync();
				}
			});
        	else startLibraryFileSync();
        }
    }

	private void startLibraryFileSync() {
		callback.finished();
		new ProtocolSyncLibraryFilesTask(protocol, mContext).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}
}