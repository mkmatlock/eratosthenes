package com.mm.eratos.protocol.task;

import android.os.AsyncTask;

import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolHandler;

public class ProtocolCopyTask extends AsyncTask<EratosUri, Void, FileResult> {
	private Throwable failure;
	private final IFileOperationResultListener listener;
	private boolean deleteSrc;
	private ProtocolHandler protocol;
	
	public ProtocolCopyTask(ProtocolHandler protocol, IFileOperationResultListener listener, boolean deleteSrc){
		this.protocol = protocol;
		this.listener = listener;
		this.deleteSrc = deleteSrc;
	}
	
	@Override
    protected void onPreExecute()
    {
		listener.started("Copying Files");
		failure = null;
    }

	@Override
	protected FileResult doInBackground(EratosUri... params) {
		EratosUri src = params[0];
		EratosUri dest = params[1];
		
		try {
			protocol.refresh(src.parent());
			if(!src.parent().equals(dest.parent()))
				protocol.refresh(dest.parent());
			
			FileResult result = protocol.copy(src, dest);
			if(deleteSrc)
				protocol.delete(src);
			return result;
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		return null;
	}

	@Override
    protected void onPostExecute(FileResult result)
    {
        if(failure != null)
        	listener.failed(failure);
        else
        	listener.finished(result);
    }
}
