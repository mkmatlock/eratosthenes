package com.mm.eratos.protocol.task;

import android.os.AsyncTask;

import com.mm.eratos.files.AbstractProgressFileOperationResultListener;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolHandler;

public class ProtocolUploadTask extends AsyncTask<EratosUri, Integer, FileResult> {
	private Throwable failure;
	private final AbstractProgressFileOperationResultListener listener;
	private ProtocolHandler protocol;
	
	public ProtocolUploadTask(ProtocolHandler protocol, AbstractProgressFileOperationResultListener listener){
		this.protocol = protocol;
		this.listener = listener;
	}
	
	@Override
    protected void onPreExecute()
    {
		listener.started("Uploading File");
    };

    @Override
	protected void onProgressUpdate(Integer ... values) {
    	int percent = (int)(((float)(values[0])) / ((float)(values[1])));
    	listener.progress(null, percent, 100);
	}
	
	@Override
	protected FileResult doInBackground(EratosUri... params) {
		EratosUri src = params[0];
		EratosUri dst = params[1];
		
		try {
			protocol.refresh(dst.parent());
			FileResult result = protocol.put(src, dst, new IFileOperationProgressListener() {
				public void progress(String message, int progress, int maxprogress) {
					publishProgress(progress, maxprogress);
				}
			});
			return result;
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		return null;
	}

	@Override
    protected void onPostExecute(FileResult result)
    {
        if(failure != null)
        	listener.failed(failure);
        else
        	listener.finished(result);
    }
}
