package com.mm.eratos.protocol.task;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;

import android.os.AsyncTask;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.ProtocolNoSuchFileException;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.protocol.SyncedFileOp;

public class ProtocolDownloadTask extends AsyncTask<EratosUri, Integer, FileResult> {
	private Throwable failure;
	private boolean deleteSrc;
	private ProtocolHandler protocol;
	private FileManager fileManager;
	private FileRevisionContentProviderAdapter revisionAdapter;
	private String stage="";
	private IFileOperationResultListener resultListener;
	private IFileOperationProgressListener progressListener;
	
	public ProtocolDownloadTask(ProtocolHandler protocol, IFileOperationResultListener resultListener, IFileOperationProgressListener progressListener, boolean deleteSrc){
		this.protocol = protocol;
		this.resultListener = resultListener;
		this.progressListener = progressListener;
		this.deleteSrc = deleteSrc;
		this.fileManager = FileManager.getFileManager();
		this.revisionAdapter = new FileRevisionContentProviderAdapter(EratosApplication.getApplication().getContentResolver());
	}
	
	@Override
    protected void onPreExecute()
    {
        resultListener.started("Downloading File");
    };

    @Override
	protected void onProgressUpdate(Integer ... values) {
		progressListener.progress(stage, values[0], values[1]);
	}
    
	@Override
	protected FileResult doInBackground(EratosUri... params) {
		
		EratosUri srcUri = params[0];
		EratosUri dstUri = params[1];
		
		File localFile = fileManager.getFile(dstUri);
		File syncDir = localFile.getParentFile();
		
		try {
			moveFiles();
			stage = String.format(Locale.US, "Downloading %s...", srcUri.toString());
			if(!syncDir.exists() && !syncDir.mkdirs())
				throw new IOException("Could not create local sync directory: " + syncDir.getAbsolutePath());
			
			protocol.refresh(srcUri.parent());
			FileResult result = protocol.fetch(srcUri, dstUri, deleteSrc, new IFileOperationProgressListener() {
				@Override
				public void progress(String message, int progress, int maxprogress) {
					publishProgress(progress, maxprogress);
				}
			});
			
			return result;
		}catch(Exception e){
			if(localFile.exists()) localFile.delete(); 
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			if(localFile.exists()) localFile.delete();
			failure = e;
			e.printStackTrace();
		}
		return null;
	}

	private void moveFiles() {
		stage = "Updating File History...";
		Collection<? extends SyncedFileOp> moveOps = revisionAdapter.getFileOperations(protocol.protocol());
		int i = 0;
		publishProgress(i, moveOps.size());
		for(SyncedFileOp op : moveOps) {
			try {
				execMoveOp(op);
			} catch(ProtocolNoSuchFileException e){
				e.printStackTrace();
				revisionAdapter.updateFileOp(op.commit());
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			i++;
			publishProgress(i, moveOps.size());
		}
		revisionAdapter.clearCompletedFileOps(protocol.protocol());
	}

	private void execMoveOp(SyncedFileOp op) throws ProtocolException {
		EratosUri result = EratosUri.parseUri(op.result());
		FileResult file = protocol.move(op.uri(), result);
		revisionAdapter.updateFileOp(op.commit());
		
		revisionAdapter.delete(op.uri());
		if(revisionAdapter.exists(result)){
			SyncedFile sf = revisionAdapter.getFile(result);
			revisionAdapter.update(sf.revision(file.revision()));
		} else {
			revisionAdapter.newSyncedFile(result, file.revision(), fileManager.getFile(result), false);
		}
	}
	
	@Override
    protected void onPostExecute(FileResult result)
    {
        if(failure != null)
        	resultListener.failed(failure);
        else
        	resultListener.finished(result);
    }
}
