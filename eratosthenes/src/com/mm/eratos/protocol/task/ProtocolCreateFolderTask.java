package com.mm.eratos.protocol.task;

import android.os.AsyncTask;

import com.mm.eratos.files.IFileOperationResultListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.ProtocolHandler;

public class ProtocolCreateFolderTask extends AsyncTask<EratosUri, Void, FileResult>{
	private Throwable failure;
	private final IFileOperationResultListener listener;
	private ProtocolHandler protocol;

	public ProtocolCreateFolderTask(ProtocolHandler protocol, IFileOperationResultListener listener){
		this.protocol = protocol;
		this.listener = listener;
	}
	
	@Override
	protected void onPreExecute() {
		listener.started("Creating Folder");
	}
	
	@Override
	protected FileResult doInBackground(EratosUri... params) {
		try{
			EratosUri uri = params[0];
			protocol.refresh(uri.parent());
			protocol.createFolder(uri.parent(), uri.name());
			return new FileResult(uri, true);
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		return null;
	}
	

	@Override
	protected void onPostExecute(FileResult result) {
		if(failure == null)
			listener.finished(result);
		else
			listener.failed(failure);
	}
}