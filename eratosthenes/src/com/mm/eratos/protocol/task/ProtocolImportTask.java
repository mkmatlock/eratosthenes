package com.mm.eratos.protocol.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.mm.eratos.dialog.MultiProgressDialog;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.files.IFileOperationProgressListener;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.ProtocolException;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.SyncedFile;
import com.mm.eratos.task.LoadLibraryTask;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.utils.NotificationUtils;

public class ProtocolImportTask extends AsyncTask<Void, Integer, EratosUri> {

	private FragmentActivity mContext;
	private MultiProgressDialog progressDialog;
	private Throwable failure;
	private String stage;
	private String substage;
	private FileRevisionContentProviderAdapter revisionAdapter;
	private int progress;
	private int maxProgress;
	private String loadingWarningText;
	private ProgressCallback callback;
	private ProtocolHandler protocol;
	private FileManager fileManager;
	private String bibType;
	private EratosUri bibtexUri;
	private String encoding;
	
	public ProtocolImportTask(ProtocolHandler protocol, FragmentActivity mContext, EratosUri bibtexUri, String bibType, String encoding, ProgressCallback callback){
		this.protocol = protocol;
		this.bibtexUri = bibtexUri;
		this.bibType = bibType;
		this.mContext = mContext;
		this.encoding = encoding;
		this.callback = callback;
		this.revisionAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
		this.fileManager = FileManager.getFileManager();
	}
	
	public void createProgressDialog(){
		progressDialog = MultiProgressDialog.newInstance("Importing from " + protocol.name(), stage);
        progressDialog.show(mContext.getFragmentManager(), "pdialog");
	}
	
	@Override
    protected void onPreExecute()
    {
		stage = "Starting...";
        createProgressDialog();
		failure = null;
    };

    @Override
	protected void onProgressUpdate(Integer ... values) {
    	callback.update(stage, values[0], values[1]);
    	
    	progressDialog.setPrimarySubtitle(stage);
    	progressDialog.setPrimaryProgress(values[0], values[1]);
    	
    	if(values.length == 4){
    		progressDialog.setSecondarySubtitle(substage);
    		progressDialog.setSecondaryProgress(values[2], values[3]);
    	}else{
    		progressDialog.hideSecondary();
    	}
	}
	
	@Override
	protected EratosUri doInBackground(Void... params) {
		try{
			protocol.clearCache();
			
			stage = "Downloading bibtex...";
			
			progress = 0;
			maxProgress = 1;
			publishProgress(0, maxProgress);
			
			stage = "Loading library...";
			publishProgress(0, maxProgress);
			
			substage = "Downloading: " + bibtexUri.toString();
			downloadFile(bibtexUri, true, new IFileOperationProgressListener() {
				public void progress(String message, int bytes, int total) {
					publishProgress(progress, maxProgress, bytes, total);
				}
			});
			
			LoadLibraryTask loadLibraryTask = new LoadLibraryTask(mContext, 
					bibtexUri,
					bibType,
					encoding, 
					false,
					new ProgressCallback(){
						public void update(String stage, int progress, int maxProgress){
							ProtocolImportTask.this.stage = stage;
							publishProgress(progress, maxProgress);
						}
						public void finished() {
						}
						public void error(Throwable e){
						}
					});
			boolean success = loadLibraryTask.doInBackground();
			loadingWarningText = loadLibraryTask.getWarningText();
			
			if(!success){
				failure = loadLibraryTask.getError();
				return null;
			}
			
			return bibtexUri;
		}catch(Exception e){
			failure = e;
			e.printStackTrace();
		}catch(Error e){
			failure = e;
			e.printStackTrace();
		}
		
		return null;
	}

	private void downloadFile(EratosUri uri, boolean important, IFileOperationProgressListener progressListener) throws IOException, FileNotFoundException, ProtocolException {
		File localFile = fileManager.getFile(uri);
		File syncDir = localFile.getParentFile();
		if(!syncDir.exists() && !syncDir.mkdirs())
			throw new IOException("Could not create local sync directory: " + syncDir.getAbsolutePath());
		
		FileResult entry = protocol.fetch(uri, uri, false, progressListener);
		
		localFile.setReadable(true);
		localFile.setWritable(true);
		
		if(!revisionAdapter.exists(uri)){
			revisionAdapter.newSyncedFile(uri, entry.revision(), localFile, important);
		}else{
			SyncedFile syncFile = revisionAdapter.getFile(uri);
			revisionAdapter.update(syncFile.revision(entry.revision()).setLastModified(localFile.lastModified()).setCached(true));
		}
	}
	
	@Override
    protected void onPostExecute(EratosUri remoteDB) {
		progressDialog.dismissCheckVisible();
		if (failure != null)
			NotificationUtils.notifyException(mContext,	protocol.name() + " Import Failed", failure, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					callback.error(failure);
				}
			});
		else{
			if(!loadingWarningText.isEmpty()) NotificationUtils.notify(mContext, "Warning", loadingWarningText, new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					startLibraryFileDownload();
				}});
			else
				startLibraryFileDownload();
		}
		
    }

	private void startLibraryFileDownload() {
		callback.finished();
		new ProtocolImportLibraryFilesTask(protocol, mContext).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);		
	}
}
