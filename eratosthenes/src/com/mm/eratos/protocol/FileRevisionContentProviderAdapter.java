package com.mm.eratos.protocol;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.mm.eratos.database.DBHelper;
import com.mm.eratos.model.EratosUri;

public class FileRevisionContentProviderAdapter {
	private final ContentResolver contentResolver;

	public FileRevisionContentProviderAdapter(ContentResolver contentResolver){
		this.contentResolver = contentResolver;
	}
	

	public int insertOrUpdate(SyncedFile syncFile) {
		Cursor cursor = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), 
				String.format("%s = ?", DBHelper.COLUMN_URI), new String[]{syncFile.uri().toString()}, null);

		if(cursor.getCount() == 0){
			cursor.close();
			return insert(syncFile);
		}else{
			cursor.moveToNext();
			int id = cursor.getInt( cursor.getColumnIndex(DBHelper.COLUMN_KEY ));
			cursor.close();
			update(syncFile.assignId(id));
			return id;
		}
	}
	
	public int insert(SyncedFile syncFile) {
		ContentValues values = new ContentValues();

		values.put(DBHelper.COLUMN_URI, syncFile.uri().toString());
		values.put(DBHelper.COLUMN_REVISION, syncFile.lastRevision());
		values.put(DBHelper.COLUMN_LASTACCESS, syncFile.lastAccessed());
		values.put(DBHelper.COLUMN_MODIFIED, syncFile.lastModified());
		values.put(DBHelper.COLUMN_CACHED, syncFile.cached() ? 1 : 0);
		values.put(DBHelper.COLUMN_IMPORTANT, syncFile.important() ? 1 : 0);
		values.put(DBHelper.COLUMN_DELETED, syncFile.deleted() ? 1 : 0);
		Uri insert = contentResolver.insert(FileRevisionContentProvider.FILE_URI, values);
		
		contentResolver.notifyChange(FileRevisionContentProvider.FILE_URI, null);
		return Integer.parseInt(insert.getLastPathSegment());
	}
	
	public void update(SyncedFile syncFile) {
		int id = syncFile.id();
		
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_URI, syncFile.uri().toString());
		values.put(DBHelper.COLUMN_REVISION, syncFile.lastRevision());
		values.put(DBHelper.COLUMN_LASTACCESS, syncFile.lastAccessed());
		values.put(DBHelper.COLUMN_MODIFIED, syncFile.lastModified());
		values.put(DBHelper.COLUMN_CACHED, syncFile.cached() ? 1 : 0);
		values.put(DBHelper.COLUMN_IMPORTANT, syncFile.important() ? 1 : 0);
		values.put(DBHelper.COLUMN_DELETED, syncFile.deleted() ? 1 : 0);
		contentResolver.update(FileRevisionContentProvider.FILE_URI, values, String.format("%s = %d", DBHelper.COLUMN_KEY, id), null);
		
		contentResolver.notifyChange(FileRevisionContentProvider.FILE_URI, null);
	}
	
	public SyncedFile getFile(EratosUri fileUri) {
		Cursor cursor = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), 
								String.format("%s = ?", DBHelper.COLUMN_URI), new String[]{fileUri.toString()}, null);
		
		if(cursor.getCount() == 0)
			throw new RuntimeException(new FileNotFoundException(fileUri.toString()));
		cursor.moveToFirst();
		
		SyncedFile rval = cursorToFile(cursor);
		cursor.close();
		return rval;
	}

	public Cursor getAllSyncedFiles() {
		Cursor cursor = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), null, null, null);
		return cursor;
	}
	
	public List<SyncedFile> getOldestFiles(int limit) {
		Cursor c = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), DBHelper.COLUMN_IMPORTANT + "=0 AND " + DBHelper.COLUMN_CACHED + "=1", null, DBHelper.COLUMN_LASTACCESS);
		List<SyncedFile> result = new ArrayList<SyncedFile>();
		int i = 0;
		while(c.moveToNext() && i < limit){
			result.add(cursorToFile(c));
			i++;
		}
		c.close();
		return result;
	}
	
	public int countFiles() {
		Cursor syncedFiles = getAllSyncedFiles();
		int result = syncedFiles.getCount();
		syncedFiles.close();
		return result;
	}
	
	public int countUnimportantFiles() {
		Cursor syncedFiles = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), DBHelper.COLUMN_IMPORTANT + "=0 AND " + DBHelper.COLUMN_CACHED + "=1", null, null);
		int result = syncedFiles.getCount();
		syncedFiles.close();
		return result;
	}
	
	public SyncedFile cursorToFile(Cursor cursor){
		int id = cursor.getInt( cursor.getColumnIndex(DBHelper.COLUMN_KEY) );
		EratosUri uri = EratosUri.parseUri( cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_URI) ) );
		String rev = cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_REVISION) );
		long accessed = cursor.getLong( cursor.getColumnIndex(DBHelper.COLUMN_LASTACCESS) );
		long modified = cursor.getLong( cursor.getColumnIndex(DBHelper.COLUMN_MODIFIED) );
		boolean cached = cursor.getInt( cursor.getColumnIndex(DBHelper.COLUMN_CACHED) ) == 1;
		boolean important = cursor.getInt( cursor.getColumnIndex(DBHelper.COLUMN_IMPORTANT) ) == 1;
		boolean deleted = cursor.getInt( cursor.getColumnIndex(DBHelper.COLUMN_DELETED) ) == 1;
		
		return new SyncedFile(id, uri, rev, accessed, modified, cached, important, deleted);
	}
	
	private String[] allColumns() {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_URI,
											DBHelper.COLUMN_REVISION, DBHelper.COLUMN_LASTACCESS,
											DBHelper.COLUMN_MODIFIED, DBHelper.COLUMN_IMPORTANT, 
											DBHelper.COLUMN_DELETED, DBHelper.COLUMN_CACHED};
		return projection;
	}
	
	public int delete(int id) {
		int rowsDeleted = contentResolver.delete(FileRevisionContentProvider.FILE_URI, 
				String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{Integer.toString(id)});
		contentResolver.notifyChange(FileRevisionContentProvider.FILE_URI, null);
		return rowsDeleted;
	}
	
	public int delete(EratosUri uri) {
		int rowsDeleted = contentResolver.delete(FileRevisionContentProvider.FILE_URI, 
				String.format("%s = ?", DBHelper.COLUMN_URI), new String[]{uri.toString()});
		contentResolver.notifyChange(FileRevisionContentProvider.FILE_URI, null);
		return rowsDeleted;
	}

	public int deleteAll() {
		int rowsDeleted = contentResolver.delete(FileRevisionContentProvider.FILE_URI, null, null);
		contentResolver.notifyChange(FileRevisionContentProvider.FILE_URI, null);
		return rowsDeleted;
	}

	public void setImportant(EratosUri uri, boolean important) {
		SyncedFile sf = getFile(uri);
		SyncedFile nsf = sf.setImportant(important);
		update(nsf);
	}
	
	public void setCached(EratosUri uri, boolean cached) {
		SyncedFile sf = getFile(uri);
		SyncedFile nsf = sf.setCached(cached);
		update(nsf);
	}

//	public Collection<? extends SyncedFile> getCached(String protocol) {
//		Cursor c = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), DBHelper.COLUMN_CACHED + "=1", null, null);
//		List<SyncedFile> cachedFiles = new ArrayList<SyncedFile>(c.getCount());
//		while(c.moveToNext()){
//			SyncedFile cached = cursorToFile(c);
//			if(cached.uri().protocol().equals(protocol))
//				cachedFiles.add(cached);
//		}
//		c.close();
//		
//		return cachedFiles;
//	}

	public Collection<? extends SyncedFile> getDeleted(String protocol) {
		Cursor c = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), DBHelper.COLUMN_DELETED + "=1", null, null);
		List<SyncedFile> deletedFiles = new ArrayList<SyncedFile>(c.getCount());
		while(c.moveToNext()){
			SyncedFile deleted = cursorToFile(c);
			if(deleted.uri().protocol().equals(protocol))
				deletedFiles.add(deleted);
		}
		c.close();
		
		return deletedFiles;
	}
	
	public Collection<? extends SyncedFileOp> getFileOperations(String protocol) {
		Cursor c = contentResolver.query(FileRevisionContentProvider.FILE_OPERATIONS_URI, allFileOpColumns(), DBHelper.COLUMN_PROTOCOL + " = ? AND " + DBHelper.COLUMN_COMMITTED + " = 0", new String[]{protocol}, null);
		List<SyncedFileOp> ops = new ArrayList<SyncedFileOp>(c.getCount());
		
		while(c.moveToNext()){
			SyncedFileOp op = cursorToFileOp(c);
			ops.add(op);
		}
		c.close();
		
		return ops;
	}

	private SyncedFileOp cursorToFileOp(Cursor c) {
		int id = c.getInt( c.getColumnIndex( DBHelper.COLUMN_KEY) );
		int opType = c.getInt( c.getColumnIndex( DBHelper.COLUMN_OPERATION) );
		EratosUri uri = EratosUri.parseUri(c.getString( c.getColumnIndex( DBHelper.COLUMN_URI ) ));
		String result = c.getString( c.getColumnIndex( DBHelper.COLUMN_RESULT ) );
		boolean committed = c.getInt( c.getColumnIndex(DBHelper.COLUMN_COMMITTED) ) == 1;
		
		SyncedFileOp op = new SyncedFileOp(id, opType, uri, result, committed);
		return op;
	}
	
	public void queueFileOp(SyncedFileOp op) {
		ContentValues values = new ContentValues();
		getFileOpValues(op, values);
		
		contentResolver.insert(FileRevisionContentProvider.FILE_OPERATIONS_URI, values);
	}
	
	public void updateFileOp(SyncedFileOp op) {
		ContentValues values = new ContentValues();
		getFileOpValues(op, values);
		values.put(DBHelper.COLUMN_KEY, op.id());
		
		contentResolver.update(FileRevisionContentProvider.FILE_OPERATIONS_URI, values, DBHelper.COLUMN_KEY + " = " + op.id(), null);
	}

	public void clearCompletedFileOps(String protocol){
		contentResolver.delete(FileRevisionContentProvider.FILE_OPERATIONS_URI, DBHelper.COLUMN_PROTOCOL + " = ? AND " + DBHelper.COLUMN_COMMITTED + " = 1", new String[]{protocol});
	}
	
	private void getFileOpValues(SyncedFileOp op, ContentValues values) {
		values.put(DBHelper.COLUMN_PROTOCOL, op.uri().protocol());
		values.put(DBHelper.COLUMN_OPERATION, op.type());
		values.put(DBHelper.COLUMN_URI, op.uri().toString());
		values.put(DBHelper.COLUMN_RESULT, op.result());
		values.put(DBHelper.COLUMN_COMMITTED, op.committed() ? 1 : 0);
	}
	
	private String[] allFileOpColumns() {
		return new String[]{DBHelper.COLUMN_KEY,DBHelper.COLUMN_PROTOCOL,DBHelper.COLUMN_URI,DBHelper.COLUMN_OPERATION,DBHelper.COLUMN_RESULT,DBHelper.COLUMN_COMMITTED};
	}


	public boolean exists(EratosUri fileUri) {
		Cursor cursor = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), 
				String.format("%s = ?", DBHelper.COLUMN_URI), new String[]{fileUri.toString()}, null);

		boolean exists = cursor.getCount() != 0;
		cursor.close();
		return exists;
	}
	
	public boolean deleted(EratosUri uri) {
		Cursor cursor = contentResolver.query(FileRevisionContentProvider.FILE_URI, allColumns(), 
				String.format("%s = ? AND %s = 1", DBHelper.COLUMN_URI, DBHelper.COLUMN_DELETED), new String[]{uri.toString()}, null);

		boolean exists = cursor.getCount() != 0;
		if(exists){
			cursor.moveToFirst();
			SyncedFile sf = cursorToFile(cursor);
			exists = !sf.deleted();
		}
		cursor.close();
		return !exists;
	}

	public SyncedFile newSyncedFile(EratosUri uri, String rev, File localFile, boolean important) {
		if(exists(uri) && !getFile(uri).deleted())
			throw new RuntimeException("Tried to overwrite existing remote file entry: " + uri.toString());
		
		boolean cached = localFile.exists();
		long lastModified = 0;
		if(cached)
			lastModified = localFile.lastModified();
		
		SyncedFile sf = new SyncedFile(-1, uri, rev, lastModified, lastModified, cached, important, false);
		int newId = insertOrUpdate(sf);
		return sf.assignId(newId);
	}

	public boolean isEmpty() {
		Cursor c = getAllSyncedFiles();
		boolean rval = c.getCount() == 0;
		c.close();
		
		return rval;
	}
}
