package com.mm.eratos.ezproxy;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class EZProxyAuthenticationService extends Service {
	
	@Override
	public IBinder onBind(Intent intent) {
		return new EZProxyAccountAuthenticator(this).getIBinder();
	}
}