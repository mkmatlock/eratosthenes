package com.mm.eratos.ezproxy;

import java.io.IOException;

import android.accounts.Account;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.mm.eratos.R;
import com.mm.eratos.utils.NotificationUtils;

public class EZProxyEditAccountDialog {
	public static interface OnFinishedListener {
		public boolean onConfirm(String acctName, String acctLoginUrl, String acctFetchUrl, String acctUsername, String acctPassword);
		public void onCancel();
	}

	Context mContext;
	
	AlertDialog.Builder dialogBuilder;
	OnFinishedListener listener;
	private AlertDialog dialog;

	private Account ezProxyAccount;

	private EZProxyAccountManager accountManager;

	private EditText accountNameView;
	private EditText accountLoginUrlView;
	private EditText accountFetchUrlView;
	private EditText accountUsernameView;
	private EditText accountPasswordView;

	private CheckBox showPasswordCheckbox;

	
	public EZProxyEditAccountDialog(Context mContext, EZProxyAccountManager accountManager, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.accountManager = accountManager;
		this.listener = listener;
		
		createLayout(false);
	}
	
	public EZProxyEditAccountDialog(Context mContext, EZProxyAccountManager accountManager, Account ezProxyAccount, OnFinishedListener listener)
	{
		this.mContext = mContext;
		this.accountManager = accountManager;
		this.ezProxyAccount = ezProxyAccount;
		this.listener = listener;
		
		createLayout(true);
	}
	
	private void createLayout(boolean editMode) {
		LayoutInflater inflater = LayoutInflater.from(mContext);
		
		View layout = inflater.inflate(R.layout.ez_proxy_account_add, null);
		accountNameView = (EditText) layout.findViewById(R.id.acct_name_entry);
		accountLoginUrlView = (EditText) layout.findViewById(R.id.acct_login_url);
		accountFetchUrlView = (EditText) layout.findViewById(R.id.acct_fetch_url);
		accountUsernameView = (EditText) layout.findViewById(R.id.acct_username);
		accountPasswordView = (EditText) layout.findViewById(R.id.acct_password);
		showPasswordCheckbox = (CheckBox) layout.findViewById(R.id.show_password_button);
		
		showPasswordCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					accountPasswordView.setInputType(InputType.TYPE_CLASS_TEXT);
				else
					accountPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			}
		});
		
		String goText = "Edit";
		String title = "Edit Account";
		if(editMode){
			accountNameView.setEnabled(false);
			
			accountNameView.setText( ezProxyAccount.name );
			accountLoginUrlView.setText( accountManager.getEZProxyLoginURL(ezProxyAccount).toString() );
			accountFetchUrlView.setText( accountManager.getEZProxyFetchURL(ezProxyAccount).toString() );
			accountUsernameView.setText( accountManager.getEZProxyUsername(ezProxyAccount) );
			
			try{
				accountPasswordView.setText( accountManager.getEZProxyPassword(ezProxyAccount) );
			}catch (IOException e) {
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			}
		} else {
			goText = "Create";
			title = "New Account";
		}
		
		dialogBuilder = new AlertDialog.Builder(mContext);
		dialogBuilder.setTitle(title);
		dialogBuilder.setView(layout);
		dialogBuilder.setPositiveButton(goText, null);
		dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface p1, int p2)
				{
					notifyCancel();
				}
		});
		dialog = dialogBuilder.show();
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				notifyChange();
			}
		});
	}
	
	protected void notifyCancel() {
		listener.onCancel();
	}

	private void notifyChange() {
		String acctName = accountNameView.getText().toString().trim();
		String acctLoginUrl = accountLoginUrlView.getText().toString().trim();
		String acctFetchUrl = accountFetchUrlView.getText().toString().trim();
		String acctUsername = accountUsernameView.getText().toString().trim();
		String acctPassword = accountPasswordView.getText().toString().trim();
		
		if(acctName.length() == 0)
			NotificationUtils.notify(mContext, "Error", "You must enter an account name!");
		
		if(listener.onConfirm(acctName, acctLoginUrl, acctFetchUrl, acctUsername, acctPassword))
			dialog.dismiss();
	}
}