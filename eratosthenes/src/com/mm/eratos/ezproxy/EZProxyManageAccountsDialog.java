package com.mm.eratos.ezproxy;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.http.auth.AuthenticationException;

import android.accounts.Account;
import android.accounts.AccountsException;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mm.eratos.R;
import com.mm.eratos.adapter.ItemActionCompositeAdapter;
import com.mm.eratos.adapter.ItemActionCompositeAdapter.ItemActionCallback;
import com.mm.eratos.adapter.MapAdapter;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.NotificationUtils;

public class EZProxyManageAccountsDialog implements OnClickListener {
	private OnClickListener dialogClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(v.getId() == closeButton.getId()){
				dismiss();
			}
		}
	};
	
	private ItemActionCallback<String> removeCallback = new ItemActionCallback<String>() {
		@Override
		public void perform(int position, String item, View v) {
			try{
				final Account account = acctManager.getEZProxyAccount(item); 
				NotificationUtils.confirmDialog(mContext, "Remove Account?",  "Remove account: " + item, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mapAdapter.remove(account.name);
						acctManager.removeEZProxyAccount(account);
					}
				});
			}catch(AuthenticationException e){
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			}
		}

		@Override
		public void initialize(int position, String item, View v) {
			
		}
	};
	
	EZProxyCheckLoginTask.FinishedListener onLoginCheckFinishedListener = new EZProxyCheckLoginTask.FinishedListener() {
		public void success() {
			NotificationUtils.shortToast(mContext, "Credentials verified");
		}
		
		public void failure(Throwable failure) {
			NotificationUtils.notify(mContext, "Login Failure", "Could not login to Proxy, check your credentials!\n" + failure.getMessage());
			failure.printStackTrace();
		}
	};
	
	private OnItemClickListener selectAccountClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
			fieldList.setItemChecked(pos, true);
			String acctName = (String) mapAdapter.getItem(pos);
			EratosApplication app = EratosApplication.getApplication();
			EratosSettings settingsManager = app.getSettingsManager();
			settingsManager.useEZProxyAccount(acctName);
			
			if(app.isOnline())
				new EZProxyCheckLoginTask(mContext, onLoginCheckFinishedListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, acctName);
		}
	};
	
	private OnItemLongClickListener editAccountClickListener  = new OnItemLongClickListener() {
		public boolean onItemLongClick(AdapterView<?> adapter, View v, int pos, long id) {
			try {
				String acctName = (String) mapAdapter.getItem(pos);
				Account ezProxyAccount = acctManager.getEZProxyAccount(acctName);
				new EZProxyEditAccountDialog(mContext, acctManager, ezProxyAccount, editAccountListener);
			} catch (AuthenticationException e) {
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			}
			return true;
		}
	};
	
	private EZProxyEditAccountDialog.OnFinishedListener editAccountListener = new EZProxyEditAccountDialog.OnFinishedListener() {
		public boolean onConfirm(String acctName, String acctLoginUrl,
				String acctFetchUrl, String acctUsername, String acctPassword) {
			try {
				Account ezProxyAccount = acctManager.getEZProxyAccount(acctName);
				
				EratosUri acctLoginUri = EratosUri.parseUri(acctLoginUrl);
				EratosUri acctFetchUri = EratosUri.parseUri(acctFetchUrl);
				
				if(acctLoginUri == null){
					NotificationUtils.longToast(mContext, "Invalid URL: " + acctLoginUrl);
					return false;
				}
				if(acctFetchUri == null){
					NotificationUtils.longToast(mContext, "Invalid URL: " + acctFetchUrl);
					return false;
				}
				
				acctManager.modifyEZProxyAccount(ezProxyAccount, acctLoginUri, acctFetchUri, acctUsername, acctPassword);
				
				String desc = String.format(Locale.US, "%s@%s", acctUsername, acctLoginUrl);
				mapAdapter.put(ezProxyAccount.name, desc);
			} catch (AuthenticationException e) {
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			} catch (IOException e) {
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			}
			
			return true;
		}
		public void onCancel() {
			
		}
	};
	

	private EZProxyEditAccountDialog.OnFinishedListener newAccountListener = new EZProxyEditAccountDialog.OnFinishedListener() {
		public boolean onConfirm(String acctName, String acctLoginUrl,
				String acctFetchUrl, String acctUsername, String acctPassword) {
			
			if(acctName.isEmpty()){
				NotificationUtils.longToast(mContext, "Account Name cannot be empty");
				return false;
			}
			if(acctLoginUrl.isEmpty()){
				NotificationUtils.longToast(mContext, "Login URL cannot be empty");
				return false;
			}
			if(acctFetchUrl.isEmpty()){
				NotificationUtils.longToast(mContext, "Fetch URL cannot be empty");
				return false;
			}
			if(acctUsername.isEmpty()){
				NotificationUtils.longToast(mContext, "Username cannot be empty");
				return false;
			}
			if(acctPassword.isEmpty()){
				NotificationUtils.longToast(mContext, "Password cannot be empty");
				return false;
			}
			
			boolean exists = false;
			try {
				acctManager.getEZProxyAccount(acctName);
				exists = true;
			} catch (AuthenticationException e) {
				exists = false;
			}
			
			if(exists){
				NotificationUtils.longToast(mContext, "Account name already in use: " + acctName);
				return false;
			}
			
			EratosUri acctLoginUri = EratosUri.parseUri(acctLoginUrl);
			EratosUri acctFetchUri = EratosUri.parseUri(acctFetchUrl);
			if(acctLoginUri == null){
				NotificationUtils.longToast(mContext, "Invalid URL: " + acctLoginUrl);
				return false;
			}if(acctFetchUri == null){
				NotificationUtils.longToast(mContext, "Invalid URL: " + acctFetchUrl);
				return false;
			}
			
			try {
				acctManager.saveEZProxyAccount(acctName, acctLoginUri, acctFetchUri, acctUsername, acctPassword);
			} catch (AccountsException e) {
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			} catch (IOException e) {
				NotificationUtils.notifyException(mContext, "Unexpected Exception", e);
			}
			
			String desc = String.format(Locale.US, "%s@%s", acctUsername, acctLoginUrl);
			mapAdapter.put(acctName, desc);
			return true;
		}
		public void onCancel() {
			
		}
	};
	
	
	private Context mContext;
	private Dialog dialog;
	
	private ImageButton addButton;
	private Button closeButton;
	private ListView fieldList;
	
	private EZProxyAccountManager acctManager;
	private MapAdapter mapAdapter;
	private ItemActionCompositeAdapter<String> removeAdapter;

	private CheckBox enableCheckBox;

	private OnCheckedChangeListener onEnableToggle = new OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked)
				EratosApplication.getApplication().getSettingsManager().enableEZProxy();
			else
				EratosApplication.getApplication().getSettingsManager().disableEZProxy();
		}
	};

	public EZProxyManageAccountsDialog(Context mContext){
		acctManager = new EZProxyAccountManager(mContext);
		this.mContext = mContext;
	}
	
	private void createDialog(String dialogTitle, View layout) {
		int themeFlags = android.R.style.Theme_Holo_DialogWhenLarge | android.R.style.Animation_Dialog;
		
		dialog = new Dialog(mContext, themeFlags);
		dialog.setTitle(dialogTitle);
		dialog.setContentView(layout);
		
		addButton.setOnClickListener(this);
		enableCheckBox.setOnCheckedChangeListener(onEnableToggle );
		closeButton.setOnClickListener(dialogClickListener);
		fieldList.setOnItemClickListener(selectAccountClickListener);
		fieldList.setOnItemLongClickListener(editAccountClickListener);
		fieldList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED | 
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		setListContents();
	}

	private View initFields(){
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View layout = inflater.inflate(R.layout.ez_proxy_account_dialog, null);
		layout.findViewById(R.id.button_bar).setVisibility(View.VISIBLE);
		
		fieldList = (ListView) layout.findViewById(R.id.account_list_view);
		addButton = (ImageButton) layout.findViewById(R.id.button_new_account);
		enableCheckBox = (CheckBox) layout.findViewById(R.id.button_enable_ezproxy);
		closeButton = (Button) layout.findViewById(R.id.close_button);
		
		return layout;
	}

	private void setListContents() {
		Map<String, String> fields = new HashMap<String, String>();
		for(Account ezProxyAcct : acctManager.getEZProxyAccounts()){
			String user = acctManager.getEZProxyUsername(ezProxyAcct);
			String loginUrl = acctManager.getEZProxyLoginURL(ezProxyAcct).toString();
			String desc = String.format(Locale.US, "%s@%s", user, loginUrl);
			
			fields.put(ezProxyAcct.name, desc);
		}
		
		mapAdapter = new MapAdapter(mContext, fields, R.layout.ez_proxy_account_entry, R.id.accountName, R.id.accountLoginURL);
		removeAdapter = new ItemActionCompositeAdapter<String>(R.id.removeAccount, removeCallback , mapAdapter);
		fieldList.setAdapter(removeAdapter);
		
		EratosApplication app = EratosApplication.getApplication();
		EratosSettings settingsManager = app.getSettingsManager();
		
		boolean enabled = settingsManager.getEZProxyEnabled();
		enableCheckBox.setChecked(enabled);
		
		String acctName = settingsManager.getEZProxyAccount();
		if(!acctName.isEmpty()){
			int position = mapAdapter.position(acctName);
			fieldList.setItemChecked(position, true);
		}
	}
	
	public void create() {
		View layout = initFields();
		createDialog("EZProxy Accounts", layout);
		dialog.show();
	}
	
	protected void dismiss() {
		dialog.dismiss();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == addButton.getId()){
			new EZProxyEditAccountDialog(mContext, acctManager, newAccountListener);
		}
	}
}