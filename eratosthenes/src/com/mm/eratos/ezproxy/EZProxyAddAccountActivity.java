package com.mm.eratos.ezproxy;

import java.io.IOException;

import org.apache.http.auth.AuthenticationException;

import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AccountsException;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.mm.eratos.R;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.NotificationUtils;

public class EZProxyAddAccountActivity extends AccountAuthenticatorActivity {
	public static final String PARAM_AUTHTOKEN_TYPE = "auth_token_type";

	private EZProxyAccountManager ezProxyAccountManager;
	
	private EZProxyEditAccountDialog.OnFinishedListener onFinishedListener = new EZProxyEditAccountDialog.OnFinishedListener() {
		public boolean onConfirm(String acctName, String acctLoginUrl, String acctFetchUrl, String acctUsername, String acctPassword) {
			if(acctName.isEmpty()){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Account Name cannot be empty");
				return false;
			}
			if(acctLoginUrl.isEmpty()){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Login URL cannot be empty");
				return false;
			}
			if(acctFetchUrl.isEmpty()){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Fetch URL cannot be empty");
				return false;
			}
			if(acctUsername.isEmpty()){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Username cannot be empty");
				return false;
			}
			if(acctPassword.isEmpty()){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Password cannot be empty");
				return false;
			}
			
			boolean exists = false;
			try {
				ezProxyAccountManager.getEZProxyAccount(acctName);
				exists = true;
			} catch (AuthenticationException e) {
				exists = false;
			}
			
			if(exists){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Account name already in use: " + acctName);
				return false;
			}
			
			EratosUri acctLoginUri = EratosUri.parseUri(acctLoginUrl);
			EratosUri acctFetchUri = EratosUri.parseUri(acctFetchUrl);
			if(acctLoginUri == null){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Invalid URL: " + acctLoginUrl);
				return false;
			}if(acctFetchUri == null){
				NotificationUtils.longToast(EZProxyAddAccountActivity.this, "Invalid URL: " + acctFetchUrl);
				return false;
			}
			
			try {
				ezProxyAccountManager.saveEZProxyAccount(acctName, acctLoginUri, acctFetchUri, acctUsername, acctPassword);
			} catch (AccountsException e) {
				NotificationUtils.notifyException(EZProxyAddAccountActivity.this, "Unexpected Exception", e);
			} catch (IOException e) {
				NotificationUtils.notifyException(EZProxyAddAccountActivity.this, "Unexpected Exception", e);
			}
			
			setResult(acctUsername, acctPassword);
			new CountDownTimer(250, 250) {
			     public void onTick(long millisUntilFinished) {
			     }
			     public void onFinish() {
			         finish();
			     }
			  }.start();
			
			return true;
		}
		public void onCancel() {
			cancel();
		}
	};
	
	private void setResult(String userName, String password) {
	    Intent i = new Intent();
	    i.putExtra(AccountManager.KEY_ACCOUNT_NAME, userName);
	    i.putExtra(AccountManager.KEY_ACCOUNT_TYPE, EZProxyAccountManager.EZPROXY_ACCT_TYPE);
	    i.putExtra(AccountManager.KEY_AUTHTOKEN, EZProxyAccountManager.EZPROXY_ACCT_TYPE);
	    i.putExtra(AccountManager.KEY_PASSWORD, password);
	    
	    this.setAccountAuthenticatorResult(i.getExtras());
	    this.setResult(RESULT_OK, i);
	}
	
	protected void cancel() {
		this.setResult(RESULT_CANCELED);
		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ezproxy_add_account);
		
		ezProxyAccountManager = new EZProxyAccountManager(this);
		new EZProxyEditAccountDialog(this, ezProxyAccountManager, onFinishedListener);
	}
}
