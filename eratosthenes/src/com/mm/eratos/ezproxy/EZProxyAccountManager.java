package com.mm.eratos.ezproxy;

import java.io.IOException;

import javax.crypto.SecretKey;

import org.apache.http.auth.AuthenticationException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountsException;
import android.content.Context;
import android.os.Bundle;

import com.mm.eratos.crypto.CryptoUtils;
import com.mm.eratos.model.EratosUri;

public class EZProxyAccountManager {
	public static final String EZPROXY_ACCT_TYPE = "com.mm.eratos.ezproxy";
	
	public static final String EZPROXY_NAME = "EZPROXY_NAME";
	public static final String EZPROXY_LOGIN = "EZPROXY_LOGIN";
	public static final String EZPROXY_ADDRESS = "EZPROXY_ADDRESS";
	public static final String EZPROXY_USERNAME = "EZPROXY_USERNAME";
	
	private AccountManager accountManager;

	private Context mContext;

	public EZProxyAccountManager(Context context){
		mContext = context;
		accountManager = AccountManager.get(context);
	}
	

	public Account[] getEZProxyAccounts() {
		Account[] accountsByType = accountManager.getAccountsByType(EZPROXY_ACCT_TYPE);
		return accountsByType;
	}
	
	public Account getEZProxyAccount(String acctName) throws AuthenticationException{
		Account[] accountsByType = accountManager.getAccountsByType(EZPROXY_ACCT_TYPE);
		for(Account acct : accountsByType){
			if(acct.name.equals(acctName))
				return acct;
		}
		throw new AuthenticationException();
	}
	
	public String getEZProxyUsername(Account acct) {
		return accountManager.getUserData(acct, EZPROXY_USERNAME);
	}
	
	public String getEZProxyPassword(Account acct) throws IOException {
		String encryptedPassword = accountManager.getPassword(acct);
		SecretKey key = CryptoUtils.getEncryptionKey(mContext);
		String password = CryptoUtils.decrypt(encryptedPassword, key);
		return password;
	}
	
	public EratosUri getEZProxyFetchURL(Account acct){
		return EratosUri.parseUri( accountManager.getUserData(acct, EZPROXY_ADDRESS) );
	}
	
	public EratosUri getEZProxyLoginURL(Account acct){
		return EratosUri.parseUri( accountManager.getUserData(acct, EZPROXY_LOGIN) );
	}
	
	public void saveEZProxyAccount(String acctName, EratosUri loginUri, EratosUri queryUri, String userName, String password) throws AccountsException, IOException {
		if(accountExists(acctName))
			throw new AccountsException("Account already exists: " + acctName);
		
		Account account = new Account(acctName, EZPROXY_ACCT_TYPE);
		Bundle userData = new Bundle();
		
		userData.putString(EZPROXY_LOGIN, loginUri.toString());
		userData.putString(EZPROXY_ADDRESS, queryUri.toString());
		userData.putString(EZPROXY_USERNAME, userName);
		
		SecretKey key = CryptoUtils.getEncryptionKey(mContext);
		String encryptedPassword = CryptoUtils.encrypt(password, key);
		
		boolean success = accountManager.addAccountExplicitly(account, encryptedPassword, userData);
		if(!success)
			throw new AccountsException("Could not create account: " + acctName);
	}
	
	public void modifyEZProxyAccount(Account acct, EratosUri loginUri,
			EratosUri queryUri, String userName, String password) throws IOException {
		
		
		accountManager.setUserData(acct, EZPROXY_LOGIN, loginUri.toString());
		accountManager.setUserData(acct, EZPROXY_ADDRESS, queryUri.toString());
		accountManager.setUserData(acct, EZPROXY_USERNAME, userName);
		
		SecretKey key = CryptoUtils.getEncryptionKey(mContext);
		String encryptedPassword = CryptoUtils.encrypt(password, key);
		accountManager.setPassword(acct, encryptedPassword);
	}
	
	public void removeEZProxyAccount(Account account) {
		accountManager.removeAccount(account, null, null);
	}

	private boolean accountExists(String acctName) {
		try{
			getEZProxyAccount(acctName);
			return true;
		}catch(AuthenticationException e){
		}
		return false;
	}
}
