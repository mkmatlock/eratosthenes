package com.mm.eratos.ezproxy;

import android.accounts.Account;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mm.eratos.utils.WebClient;

public class EZProxyCheckLoginTask extends AsyncTask<String, Void, Void> {
	public interface FinishedListener {
		public void success();
		public void failure(Throwable failure);
	}
	
	private EZProxyAccountManager acctManager;
	private Throwable failure;
	private FinishedListener listener;
	private Context mContext;
	private ProgressDialog pDialog;

	public EZProxyCheckLoginTask(Context mContext, FinishedListener listener){
		this.mContext = mContext;
		this.listener = listener;
		this.acctManager = new EZProxyAccountManager(mContext);
	}
	
	@Override
	protected Void doInBackground(String... arg0) {
		String acctName = arg0[0];
		failure = null;
		try{
			Account ezProxyAccount = acctManager.getEZProxyAccount(acctName);
			WebClient wc = WebClient.newInstance(ezProxyAccount);
			wc.queryHtmlPage("http://www.nature.com", true);
		}catch(Exception e){
			failure = e;
		}
		return null;
	}

	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(pDialog.isShowing()) pDialog.dismiss();
		
		if(failure != null)
			listener.failure(failure);
		else
			listener.success();
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(mContext);
		pDialog.setIndeterminate(true);
		pDialog.setTitle("Checking Proxy Login");
		pDialog.setMessage("Logging in...");
		pDialog.setCancelable(false);
		pDialog.show();
	}
}
