package com.mm.eratos.database;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibFile;
import com.mm.eratos.files.FileUtils;
import com.mm.eratos.io.bibtex.EratosthenesParser;
import com.mm.eratos.io.bibtex.bibdesk.BibDeskParser;
import com.mm.eratos.io.bibtex.jabref.JabRefParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.CaseInsensitiveHashSet;

public class DBHelper extends SQLiteOpenHelper {
	private static final String CREATE_TABLE_PREAMBLE = "create table if not exists ";
	public static final CaseInsensitiveHashSet validIdTypes;

	static {
		validIdTypes = new CaseInsensitiveHashSet();
		validIdTypes.add("doi");
		validIdTypes.add("pmid");
		validIdTypes.add("pmcid");
		validIdTypes.add("isbn");
	}
	
	private static final String DATABASE_NAME = "entries.db";
	private static final int DATABASE_VERSION = 7;

	public static final String TABLE_ENTRIES = "entries";
	public static final String TABLE_FILES = "files";
	public static final String TABLE_GROUPS = "groups";
	public static final String TABLE_COLLECTIONS = "collections";
	public static final String TABLE_KEYWORDS = "keywords";
	public static final String TABLE_STRINGS = "strings";
	public static final String TABLE_OBJECTS = "objects";
	public static final String TABLE_DOCUMENT_IDS = "document_ids";
	public static final String TABLE_SORT_FIELDS = "sort_fields";
	public static final String TABLE_RECENT_FILES = "recent_files";
	public static final String TABLE_FILE_OPERATIONS = "file_operations";
	public static final String TABLE_FILTERS = "filters";

	public static final String COLUMN_DOCKEY = "doc_key";
	public static final String COLUMN_OBJECT = "object";
	public static final String COLUMN_FILTER = "filter";

	public static final String COLUMN_HASH = "hash";
	public static final String COLUMN_MIMETYPE = "mimetype";
	public static final String COLUMN_OPERATION = "op";
	public static final String COLUMN_RESULT = "result";
	public static final String COLUMN_COMMITTED = "committed";

	public static final String COLUMN_KEY = "_id";
	public static final String COLUMN_KEY2 = "_ids";
	public static final String COLUMN_KEYWORDS = "keywords";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_TIME = "time";
	public static final String COLUMN_TEXT = "text";
	public static final String COLUMN_VALUE = "value";
	public static final String COLUMN_AUTHOR = "author";
	public static final String COLUMN_YEAR = "year";
	public static final String COLUMN_COLLECTION = "collection";
	public static final String COLUMN_SOURCE = "source"; // not using for
															// citation anymore
	public static final String COLUMN_IMPORTANT = "important";
	public static final String COLUMN_READ = "read";
	public static final String COLUMN_HASATTACHMENTS = "hasattachments";
	public static final String COLUMN_ENTRY = "entry";
	public static final String COLUMN_GROUPS = "groups";
	public static final String COLUMN_FIELD = "field";
	public static final String COLUMN_ATTACHMENT = "attachment_uri";
	public static final String COLUMN_XREF = "xref";

	public static final String COLUMN_PATH = "path";
	public static final String COLUMN_PROTOCOL = "protocol";

	public static final String COLUMN_DATE_ADDED = "added";
	public static final String COLUMN_DATE_MODIFIED = "modified";

	public static final String COLUMN_REFKEY = "refkey";
	public static final String COLUMN_KEYWORD = "keyword";
	public static final String COLUMN_GROUP = "grp";

	public static final String COLUMN_URI = "uri";
	public static final String COLUMN_REVISION = "rev";
	public static final String COLUMN_LASTACCESS = "accessed";
	public static final String COLUMN_MODIFIED = "modified";
	public static final String COLUMN_CACHED = "stored";
	public static final String COLUMN_DELETED = "deleted";

	public static final String COLUMN_COUNT = "cnt";

	public static final String COLUMN_DOC_ID_TYPE = "type";
	public static final String COLUMN_DOC_ID = "refId";

	public static final String COLUMN_MESSAGE = "message";
	public static final String COLUMN_PROGRESS = "progress";
	public static final String COLUMN_MAX = "maxprogress";

	public static final String COLUMN_INDEX = "_index";
	public static final String COLUMN_ENCODING = "encoding";

	private static final String CREATE_FILTERS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_FILTERS 	+ "("
			+ COLUMN_KEY		+ " integer primary key autoincrement, "
			+ COLUMN_FILTER 	+ " text not null, "
			+ COLUMN_TITLE 		+ " char(100) not null"
			+ ");";
			
	
	private static final String CREATE_RECENT_FILES_TABLE = CREATE_TABLE_PREAMBLE 
			+ TABLE_RECENT_FILES	+ "("
			+ COLUMN_KEY		+ " integer primary key autoincrement, "
			+ COLUMN_TYPE		+ " char(20) not null, "
			+ COLUMN_ENCODING   + " char(20) not null, "
			+ COLUMN_PROTOCOL	+ " char(10) not null, "
			+ COLUMN_PATH		+ " text not null, "
			+ COLUMN_COUNT		+ " int not null " 
			+ ");";

	private static final String CREATE_DOC_ID_TABLE = CREATE_TABLE_PREAMBLE	
			+ TABLE_DOCUMENT_IDS + "(" 
			+ COLUMN_KEY			+ " integer primary key autoincrement, " 
			+ COLUMN_REFKEY			+ " char(100) not null COLLATE NOCASE, " 
			+ COLUMN_DOC_ID_TYPE	+ " char(5) not null, " 
			+ COLUMN_DOC_ID 		+ " char(30) not null "	
			+ ");";

	// Database creation sql statement
	private static final String CREATE_ENTRIES_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_ENTRIES + "(" 
			+ COLUMN_KEY			+ " char(100) primary key COLLATE NOCASE, " 
			+ COLUMN_KEYWORDS		+ " text not null, " 
			+ COLUMN_GROUPS 		+ " text not null, "
			+ COLUMN_TYPE 			+ " char(30) not null, " 
			+ COLUMN_TITLE			+ " text not null, " 
			+ COLUMN_AUTHOR 		+ " text not null, "
			+ COLUMN_YEAR 			+ " text not null, " 
			+ COLUMN_COLLECTION		+ " text not null, " 
			+ COLUMN_SOURCE 		+ " text not null, "
			+ COLUMN_DATE_ADDED 	+ " char(10) not null, " 
			+ COLUMN_DATE_MODIFIED	+ " char(10) not null, " 
			+ COLUMN_ENTRY 			+ " text not null, "
			+ COLUMN_HASATTACHMENTS + " int(1) not null, " 
			+ COLUMN_ATTACHMENT		+ " text not null, " 
			+ COLUMN_IMPORTANT 		+ " int(1) not null, "
			+ COLUMN_READ 			+ " int(1) not null, " 
			+ COLUMN_XREF			+ " char(100) not null " 
			+ ");";

	private static final String CREATE_FILES_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_FILES + "(" 
			+ COLUMN_KEY			+ " integer primary key autoincrement, " 
			+ COLUMN_URI			+ " text not null, " 
			+ COLUMN_REVISION 		+ " char(10) not null, "
			+ COLUMN_LASTACCESS 	+ " int not null, " 
			+ COLUMN_MODIFIED		+ " int not null, " 
			+ COLUMN_CACHED 		+ " int(1) not null, "
			+ COLUMN_IMPORTANT 		+ " int(1) not null, " 
			+ COLUMN_DELETED		+ " int(1) not null" 
			+ ");";

	private static final String CREATE_FILE_OPERATIONS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_FILE_OPERATIONS + "("
			+ COLUMN_KEY			+ " integer primary key autoincrement, "
			+ COLUMN_PROTOCOL		+ " char(10) not null,"
			+ COLUMN_OPERATION		+ " char(10) not null, "
			+ COLUMN_URI			+ " text not null, "
			+ COLUMN_RESULT			+ " text not null, "
			+ COLUMN_COMMITTED		+ " int(1) not null "
			+ ");";

	private static final String CREATE_GROUPS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_GROUPS + "(" 
			+ COLUMN_KEY			+ " integer primary key autoincrement, " 
			+ COLUMN_REFKEY			+ " char(100) null COLLATE NOCASE, " 
			+ COLUMN_GROUP			+ " char(100) not null " 
			+ ");";

	private static final String CREATE_COLLECTIONS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_COLLECTIONS + "("
			+ COLUMN_KEY			+ " integer primary key autoincrement, "
			+ COLUMN_REFKEY			+ " char(100) null COLLATE NOCASE, "
			+ COLUMN_COLLECTION		+ " char(100) not null " 
			+ ");";

	private static final String CREATE_KEYWORDS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_KEYWORDS + "(" 
			+ COLUMN_KEY			+ " integer primary key autoincrement, " 
			+ COLUMN_REFKEY			+ " char(100) null COLLATE NOCASE, " 
			+ COLUMN_KEYWORD		+ " char(100) not null" 
			+ ");";

	private static final String CREATE_OBJECTS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_OBJECTS + "(" 
			+ COLUMN_KEY		+ " integer primary key autoincrement, " 
			+ COLUMN_ENTRY		+ " text not null " 
			+ ");";

	private static final String CREATE_STRINGS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_STRINGS + "(" 
			+ COLUMN_KEY	+ " char(100) primary key COLLATE NOCASE, " 
			+ COLUMN_VALUE	+ " text not null, " 
			+ COLUMN_ENTRY 	+ " text not null " 
			+ ");";

	private static final String CREATE_SORT_FIELDS_TABLE = CREATE_TABLE_PREAMBLE
			+ TABLE_SORT_FIELDS + "("
			+ COLUMN_KEY2			+ " integer primary key autoincrement, "
			+ COLUMN_REFKEY			+ " char(100) null COLLATE NOCASE, "
			+ COLUMN_FIELD			+ " char(100) null COLLATE NOCASE, "
			+ COLUMN_VALUE			+ " text not null " 
			+ ");";
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_FILTERS_TABLE);
		
		database.execSQL(CREATE_RECENT_FILES_TABLE);
		database.execSQL(CREATE_DOC_ID_TABLE);
		database.execSQL(CREATE_ENTRIES_TABLE);

		database.execSQL(CREATE_FILES_TABLE);
		database.execSQL(CREATE_FILE_OPERATIONS_TABLE);

		database.execSQL(CREATE_GROUPS_TABLE);
		database.execSQL(CREATE_COLLECTIONS_TABLE);
		database.execSQL(CREATE_KEYWORDS_TABLE);
		database.execSQL(CREATE_OBJECTS_TABLE);
		database.execSQL(CREATE_STRINGS_TABLE);
		database.execSQL(CREATE_SORT_FIELDS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DBHelper.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		if (oldVersion <= 3) {
			upgrade3to4(db);
		}
		if (oldVersion <= 4) {
			upgrade4to5(db);
		}
		if (oldVersion <= 5) {
			upgrade5to6(db);
		}
		if (oldVersion <= 6) {
			upgrade6to7(db);
		}
	}
	
	private void upgrade3to4(SQLiteDatabase db) {
		Cursor c = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_ENTRIES, null);
		c.moveToFirst();
		int entryCnt = c.getInt(0);
		c.close();

		db.beginTransaction();

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_FILES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLECTIONS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_KEYWORDS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCUMENT_IDS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SORT_FIELDS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STRINGS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OBJECTS);
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILES);

		onCreate(db);

		EratosApplication app = EratosApplication.getApplication();

		EratosUri libraryUri = app.getCurrentLibrary();
		if (libraryUri != null) {
			db.execSQL(String.format(Locale.US,
					"INSERT INTO %s (%s, %s, %s) VALUES ('%s', '%s', '%d')",
					TABLE_RECENT_FILES, COLUMN_PROTOCOL, COLUMN_PATH,
					COLUMN_COUNT, libraryUri.protocol(), libraryUri.path(),
					entryCnt));
			app.closeLibrary();
		}

		db.setTransactionSuccessful();
		db.endTransaction();
	}

	private void upgrade4to5(SQLiteDatabase db) {
		db.beginTransaction();

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_FILES);
		db.execSQL(CREATE_RECENT_FILES_TABLE);
		
		Cursor c = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_ENTRIES, null);
		c.moveToFirst();
		int entryCnt = c.getInt(0);
		c.close();
		
		EratosApplication app = EratosApplication.getApplication();
		EratosUri libraryUri = app.getCurrentLibrary();
		
		String translatorClass = app.getSettingsManager().getBibTeXTranslatorClassName();
		
		String type = EratosthenesParser.ERATOS_FILE_TYPE;
		if(translatorClass.equals("com.mm.eratos.bibtex.format.JabRefTranslator")){
			type = JabRefParser.JABREF_FILE_TYPE;
		}else if(translatorClass.equals("com.mm.eratos.bibtex.format.BibDeskTranslator")){
			type = BibDeskParser.BIBDESK_FILE_TYPE;
		}
		
		if (libraryUri != null) {
			app.setOpenLibrary(libraryUri, type, "UTF-8");
			
			db.execSQL(String.format(Locale.US,
					"INSERT INTO %s (%s, %s, %s, %s) VALUES ('%s', '%s', '%s', '%d')",
					TABLE_RECENT_FILES, 
					COLUMN_TYPE, COLUMN_PROTOCOL, COLUMN_PATH, COLUMN_COUNT, 
					type, libraryUri.protocol(), libraryUri.path(), entryCnt));
		}
		
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	private void upgrade5to6(SQLiteDatabase db) {
		db.beginTransaction();

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILTERS);
		db.execSQL(CREATE_FILTERS_TABLE);

		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public BibFile cursorToRecentFile(Cursor c) {
		String type = c.getString(c.getColumnIndex(DBHelper.COLUMN_TYPE));
		String protocol = c.getString(c.getColumnIndex(DBHelper.COLUMN_PROTOCOL));
		String path = c.getString(c.getColumnIndex(DBHelper.COLUMN_PATH));
		EratosUri uri = EratosUri.parseUri(protocol + "://" + path);
		
		int entryCount = c.getInt(c.getColumnIndex(DBHelper.COLUMN_COUNT));
		return new BibFile(uri, type, "UTF-8", entryCount);
	}
	
	public void insertRecentFile(BibFile bibFile, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_TYPE, bibFile.type());
		values.put(DBHelper.COLUMN_PROTOCOL, bibFile.uri().protocol());
		values.put(DBHelper.COLUMN_ENCODING, bibFile.encoding());
		values.put(DBHelper.COLUMN_PATH, bibFile.uri().path());
		values.put(DBHelper.COLUMN_COUNT, bibFile.getEntryCount());
		
		db.insert(TABLE_RECENT_FILES, null, values);
	}

	private void upgrade6to7(SQLiteDatabase db) {
		db.beginTransaction();

		Cursor c = db.query(TABLE_RECENT_FILES, 
				new String[]{COLUMN_KEY, COLUMN_TYPE, COLUMN_PROTOCOL, COLUMN_PATH, COLUMN_COUNT}, 
				"1=1", new String[]{},
				"", "", "");
		
		List<BibFile> bibFiles = new ArrayList<BibFile>( c.getCount() );
		while(c.moveToNext()) {
			BibFile bf = cursorToRecentFile(c);
			bibFiles.add(bf);
		}
		c.close();
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_FILES);
		db.execSQL(CREATE_RECENT_FILES_TABLE);
		
		for(BibFile bibFile : bibFiles)
			insertRecentFile(bibFile, db);
		
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public void export(File exportFile) throws IOException {
		SQLiteDatabase db = getReadableDatabase();

		File dbFile = new File(db.getPath());
		FileUtils.copy(dbFile, exportFile);
	}
}
