package com.mm.eratos.bibtex;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

public class BibTeXAccentParser {
	private static Map<String, String> unicodeToLatex;
	private static Map<Integer, String> accents;
	private static HashMap<String, String> specialChars;
	
	static{
		unicodeToLatex = new HashMap<String, String>();
		
		specialChars = new HashMap<String, String>();
		specialChars.put("\\", "\\\\"); // Characters that should be quoted
		specialChars.put("~", "\\~");
		specialChars.put("&", "\\&");
		specialChars.put("$", "\\$");
		specialChars.put("{", "\\{");
		specialChars.put("}", "\\}");
		specialChars.put("%", "\\%");
		specialChars.put("#", "\\#");
		specialChars.put("_", "\\_");
		
		unicodeToLatex.put("à", "\\`a"); //  Grave accent
		unicodeToLatex.put("è", "\\`e");
		unicodeToLatex.put("ì", "\\`\\i");
		unicodeToLatex.put("ò", "\\`o");
		unicodeToLatex.put("ù", "\\`u");
		unicodeToLatex.put("ỳ", "\\`y");
		unicodeToLatex.put("À", "\\`A");
		unicodeToLatex.put("È", "\\`E");
		unicodeToLatex.put("Ì", "\\`\\I");
		unicodeToLatex.put("Ò", "\\`O");
		unicodeToLatex.put("Ù", "\\`U");
		unicodeToLatex.put("Ỳ", "\\`Y");
		unicodeToLatex.put("á", "\\'a"); // Acute accent
		unicodeToLatex.put("é", "\\'e");
		unicodeToLatex.put("í", "\\'\\i");
		unicodeToLatex.put("ó", "\\'o");
		unicodeToLatex.put("ú", "\\'u");
		unicodeToLatex.put("ý", "\\'y");
		unicodeToLatex.put("Á", "\\'A");
		unicodeToLatex.put("É", "\\'E");
		unicodeToLatex.put("Í", "\\'\\I");
		unicodeToLatex.put("Ó", "\\'O");
		unicodeToLatex.put("Ú", "\\'U");
		unicodeToLatex.put("Ý", "\\'Y");
		unicodeToLatex.put("â", "\\^a"); // Circumflex
		unicodeToLatex.put("ê", "\\^e");
		unicodeToLatex.put("î", "\\^\\i");
		unicodeToLatex.put("ô", "\\^o");
		unicodeToLatex.put("û", "\\^u");
		unicodeToLatex.put("ŷ", "\\^y");
		unicodeToLatex.put("Â", "\\^A");
		unicodeToLatex.put("Ê", "\\^E");
		unicodeToLatex.put("Î", "\\^\\I");
		unicodeToLatex.put("Ô", "\\^O");
		unicodeToLatex.put("Û", "\\^U");
		unicodeToLatex.put("Ŷ", "\\^Y");
		unicodeToLatex.put("ä", "\\\"a"); // Umlaut or dieresis
		unicodeToLatex.put("ë", "\\\"e");
		unicodeToLatex.put("ï", "\\\"\\i");
		unicodeToLatex.put("ö", "\\\"o");
		unicodeToLatex.put("ü", "\\\"u");
		unicodeToLatex.put("ÿ", "\\\"y");
		unicodeToLatex.put("Ä", "\\\"A");
		unicodeToLatex.put("Ë", "\\\"E");
		unicodeToLatex.put("Ï", "\\\"\\I");
		unicodeToLatex.put("Ö", "\\\"O");
		unicodeToLatex.put("Ü", "\\\"U");
		unicodeToLatex.put("Ÿ", "\\\"Y");
		unicodeToLatex.put("ç", "\\c{c}"); // Cedilla
		unicodeToLatex.put("Ç", "\\c{C}");
		unicodeToLatex.put("œ", "{\\oe}"); // Ligatures
		unicodeToLatex.put("Œ", "{\\OE}");
		unicodeToLatex.put("æ", "{\\ae}");
		unicodeToLatex.put("Æ", "{\\AE}");
		unicodeToLatex.put("å", "{\\aa}");
		unicodeToLatex.put("Å", "{\\AA}");
		unicodeToLatex.put("_", "\\_");
		unicodeToLatex.put("–", "--"); // Dashes
		unicodeToLatex.put("—", "---");
		unicodeToLatex.put("ø", "{\\o}"); // Misc latin-1 letters
		unicodeToLatex.put("Ø", "{\\O}");
		unicodeToLatex.put("ß", "{\\ss}");
		unicodeToLatex.put("¡", "{!`}");
		unicodeToLatex.put("¿", "{?`}");
		unicodeToLatex.put("≥", "$\\ge$"); // Math operators
		unicodeToLatex.put("≤", "$\\le$");
		unicodeToLatex.put("≠", "$\\neq$");
		unicodeToLatex.put("©", "\\copyright"); // Misc
		unicodeToLatex.put("ı", "{\\i}");
		unicodeToLatex.put("°", "$\\deg$");
		unicodeToLatex.put("‘", "`"); // Quotes
		unicodeToLatex.put("’", "'");
		unicodeToLatex.put("“", "``");
		unicodeToLatex.put("”", "''");
		unicodeToLatex.put("‚", ",");
		unicodeToLatex.put("„", ",,");
		unicodeToLatex.put("\u03b1", "\\alpha");
		unicodeToLatex.put("\u03b2", "\\beta");
		unicodeToLatex.put("\u03b3", "\\gamma");
		unicodeToLatex.put("\u03b4", "\\delta");
		unicodeToLatex.put("\u03b5", "\\epsilon");
		unicodeToLatex.put("\u03b6", "\\zeta");
		unicodeToLatex.put("\u03b7", "\\eta");
		unicodeToLatex.put("\u03b8", "\\theta");
		unicodeToLatex.put("\u03b9", "\\iota");
		unicodeToLatex.put("\u03ba", "\\kappa");
		unicodeToLatex.put("\u03bb", "\\lambda");
		unicodeToLatex.put("\u03bc", "\\mu");
		unicodeToLatex.put("\u03bd", "\\nu");
		unicodeToLatex.put("\u03be", "\\xi");
		unicodeToLatex.put("\u03bf", "\\omicron"); // XXX
		unicodeToLatex.put("\u03c0", "\\pi");
		unicodeToLatex.put("\u03c1", "\\rho");
		unicodeToLatex.put("\u03c2", "\\varsigma");
		unicodeToLatex.put("\u03c3", "\\sigma");
		unicodeToLatex.put("\u03c4", "\\tau");
		unicodeToLatex.put("\u03c5", "\\upsilon");
		unicodeToLatex.put("\u03c6", "\\phi");
		unicodeToLatex.put("\u03c7", "\\chi");
		unicodeToLatex.put("\u03c8", "\\psi");
		unicodeToLatex.put("\u03c9", "\\omega");
		
		unicodeToLatex.put("\u0391", "\\Alpha");
		unicodeToLatex.put("\u0392", "\\Beta");
		unicodeToLatex.put("\u0393", "\\Gamma");
		unicodeToLatex.put("\u0394", "\\Delta");
		unicodeToLatex.put("\u0395", "\\Epsilon");
		unicodeToLatex.put("\u0396", "\\Zeta");
		unicodeToLatex.put("\u0397", "\\Eta");
		unicodeToLatex.put("\u0398", "\\Theta");
		unicodeToLatex.put("\u0399", "\\Iota");
		unicodeToLatex.put("\u039a", "\\Kappa");
		unicodeToLatex.put("\u039b", "\\Lambda");
		unicodeToLatex.put("\u039c", "\\Mu");
		unicodeToLatex.put("\u039d", "\\Nu");
		unicodeToLatex.put("\u039e", "\\Xi");
		unicodeToLatex.put("\u039f", "\\Omicron"); // XXX
		unicodeToLatex.put("\u03a0", "\\Pi");
		unicodeToLatex.put("\u03a1", "\\Rho");
		unicodeToLatex.put("\u03a3", "\\Sigma");
		unicodeToLatex.put("\u03a4", "\\Tau");
		unicodeToLatex.put("\u03a5", "\\Upsilon");
		unicodeToLatex.put("\u03a6", "\\Phi");
		unicodeToLatex.put("\u03a7", "\\Chi");
		unicodeToLatex.put("\u03a8", "\\Psi");
		unicodeToLatex.put("\u03a9", "\\Omega");
		
		accents = new HashMap<Integer, String>();
	    
	    accents.put(0x0300, "`");
		accents.put(0x0301, "'");
		accents.put(0x0302, "^");
		accents.put(0x0308, "\"");

    	accents.put(0x030B, "H");
		accents.put(0x0303, "~");
		accents.put(0x0327, "c");
		accents.put(0x0328, "k");
		
	    accents.put(0x0304, "=");
		accents.put(0x0331, "b");
		accents.put(0x0307, ".");
		accents.put(0x0323, "d");

	    accents.put(0x030A, "r");
		accents.put(0x0306, "u");
		accents.put(0x030C, "v");

	}
	
	public static String nearestAscii(String stIn) {
		stIn = stIn.replaceAll("[\\\\\\{\\}'\"^`~=]", "");
		
		String stFormD =java.text.Normalizer.normalize(stIn, Normalizer.Form.NFD);
		StringBuilder sb = new StringBuilder();

		for(int ich = 0; ich < stFormD.length(); ich++) {
			int uc = Character.getType(stFormD.charAt(ich));
			
		    if(uc != Character.NON_SPACING_MARK) {
		        sb.append(stFormD.charAt(ich));
		    }
		}

		return java.text.Normalizer.normalize(sb.toString(), Normalizer.Form.NFC);
	}

	public static String replaceAll(String subject, Map<String, String> replacements){
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < subject.length(); i++){
			String key = subject.substring(i, i+1);
			if(replacements.containsKey(key))
				sb.append( replacements.get(key) );
			else
				sb.append( key );
		}
		
		return sb.toString();
	}
	
	public static String replaceSpecialChars(String unicode) {
		return replaceAll(unicode, specialChars);
	}
	
	public static String unicodeToLatex(String unicode) {
		String partial = replaceAll(unicode, unicodeToLatex);
		
		String stFormD = java.text.Normalizer.normalize(partial, Normalizer.Form.NFD);
		StringBuilder latex = new StringBuilder();
		
		int index = 0;
		while(index < stFormD.length()){
			int index2 = index+1;
			int code = 0;
			int uc = Character.NON_SPACING_MARK;
			while(index2 < stFormD.length() && (uc == Character.NON_SPACING_MARK)){
				code = Character.codePointAt(stFormD, index2);
				uc = Character.getType(code);
				index2++;
			}
			
			for(int i = index+1; i < index2; i++){
				code = Character.codePointAt(stFormD, i);
				String codeStr = accents.get(code);
				if(codeStr != null){
					latex.append("\\");
					latex.append(codeStr);
					latex.append("{");
				}
			}
			
			if(Character.getType(stFormD.codePointAt(index)) != Character.NON_SPACING_MARK && stFormD.codePointAt(index) <= 0xFF){
				latex.append( stFormD.substring(index, index+1) );
			}
			
			for(int i = index+1; i < index2; i++){
				code = Character.codePointAt(stFormD, i);
				if(accents.get(code) != null){
					latex.append("}");
				}
			}
			
			if(index2-1 > index)
				index = index2-1;
			else
				index++;
		}
		
		return latex.toString();
	}
	

	public static String newlinesToLaTeX(String value){
		return value.replace("\n", "\n\n");
	}
}
