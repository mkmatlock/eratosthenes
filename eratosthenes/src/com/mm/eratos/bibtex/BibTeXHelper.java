package com.mm.eratos.bibtex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXFormatter;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXParser;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.LaTeXObject;
import org.jbibtex.LaTeXParser;
import org.jbibtex.LaTeXPrinter;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.TokenMgrError;
import org.jbibtex.Value;
import org.jbibtex.citation.ReferenceFormatter;
import org.jbibtex.citation.ReferenceStyle;

import com.mm.eratos.files.NullOutputStream;

public class BibTeXHelper {
	List<String> errors;
	private ReferenceFormatter refFormatter;
	private boolean warningsAsErrors;

	public BibTeXHelper(ReferenceStyle refStyle, boolean warningsAsErrors){
		errors = new LinkedList<String>();
        refFormatter = new ReferenceFormatter(refStyle); 
        this.warningsAsErrors = warningsAsErrors;
	}
	
	public BibTeXParser getBibTeXParser() {
		return new BibTeXParser();
	}
	
	public String formatBibTeX(BibTeXDatabase btdb) throws IOException {
	    StringWriter sw = new StringWriter();
	    new BibTeXFormatter().format(btdb, sw);
	    String formattedEntry = sw.getBuffer().toString();
		return formattedEntry.replaceAll("\n\\s+", "\n");
	    
	}
	
	public String formatBibTeX(BibTeXEntry entry) throws IOException {
	    BibTeXDatabase btdb = new BibTeXDatabase();
	    btdb.addObject(entry);
        return formatBibTeX(btdb);
	}
	
	public String formatBibTeX(BibTeXObject obj) throws IOException {
	    BibTeXDatabase btdb = new BibTeXDatabase();
	    btdb.addObject(obj);
        return formatBibTeX(btdb);
	}
	

	public List<BibTeXEntry> parseBibTeXEntries(Reader reader) throws IOException, ParseException {
        try {
        	ByteArrayOutputStream errorLog = new ByteArrayOutputStream();
        	BibTeXDatabase db = parseBibTeX(reader, errorLog);
        	
        	String errorString = new String(errorLog.toByteArray());
			if(!errorString.trim().isEmpty())
				throw new ParseException(errorString);
			
        	Map<Key, BibTeXEntry> dbEntries = db.getEntries();
			List<BibTeXEntry> entries = new ArrayList<BibTeXEntry>(dbEntries.size());
        	for(Key refKey : dbEntries.keySet()){
        		entries.add(dbEntries.get(refKey));
        	}
        	return entries;
        } catch(TokenMgrError e){
        	throw new ParseException(e.getMessage());
        } finally {
            reader.close();
        }
    }

	public BibTeXObject parseBibTeXObject(Reader reader) throws IOException, ParseException {
		try {
			BibTeXDatabase db = parseBibTeX(reader, new NullOutputStream());
			
			
			
        	if(db.getObjects().size() == 0)
        		throw new ParseException("Object parse failed");
        	return db.getObjects().get(0);
        } catch(TokenMgrError e){
        	throw new ParseException(e.getMessage());
        } finally {
            reader.close();
        }
	}

	public BibTeXDatabase parseBibTeX(Reader reader, OutputStream errorLog) throws IOException, ParseException {
		BibTeXParser parser = getBibTeXParser();
		parser.initPullParser(reader, errorLog);
		BibTeXDatabase db = new BibTeXDatabase();
		BibTeXObject object = null;
		
		while((object = parser.next()) != null)
			db.addObject(object);
		
		return db;
	}

	public List<BibTeXObject> parseBibTeXObjects(Reader reader) throws IOException, ParseException {
		try {
        	BibTeXDatabase db = parseBibTeX(reader, new NullOutputStream());
        	if(db.getObjects().size() == 0)
        		throw new ParseException("Object parse failed");
        	return db.getObjects();
        } catch(TokenMgrError e){
        	throw new ParseException(e.getMessage());
        } finally {
            reader.close();
        }
	}
	

	public BibTeXString parseBibTeXString(Reader reader) throws IOException, ParseException {
		try {
			BibTeXDatabase db = parseBibTeX(reader, new NullOutputStream());
        	for( BibTeXString st : db.getStrings().values() ) return st;
        	
        	throw new ParseException("String parse failed");
        } catch(TokenMgrError e){
        	throw new ParseException(e.getMessage());
        } finally {
            reader.close();
        }
	}
    
    public List<LaTeXObject> parseLaTeX(String string) throws IOException, ParseException {
        Reader reader = new StringReader(string);
        try {
                return new LaTeXParser().parse(reader);
        } catch(TokenMgrError e){
        	throw new ParseException(e.getMessage());
        } finally {
                reader.close();
        }
    }

    public String printLaTeX(List<LaTeXObject> objects){
        String result = new LaTeXPrinter().print(objects);
        return result.replace("\n\n", "NEWLINE-IN-INPUT").replace("\n", " ").replace("NEWLINE-IN-INPUT", "\n");
    }
    
    public BibTeXEntry decodeLaTeX(BibTeXEntry entry, BibTeXHelper helper) throws IOException, ParseException {
		Key typeKey = entry.getType();
		String refKey = entry.getKey().getValue();
		Key newRefKey = new Key(BibTeXAccentParser.nearestAscii(refKey));
		
		BibTeXEntry newEntry = new BibTeXEntry(typeKey, newRefKey);
		
		
		Map<Key, Value> fields = entry.getFields();
		for(Key key : fields.keySet()){
			
			String value = fields.get(key).toUserString();
			try{
				if(! BibTeXEntryModel.isUnencodedField(key.getValue()))
					value = helper.printLaTeX( helper.parseLaTeX(value) );
			}catch(ParseException pe){
				pe.printStackTrace();
			}catch(IOException e) {
				e.printStackTrace();
			}
			newEntry.addField(key, new StringValue( value, Style.BRACED ));
		}
		
		return newEntry;
	}
	
	public BibTeXEntry decodeUnicode(BibTeXEntry entry) {
		Key typeKey = entry.getType();
		String refKey = entry.getKey().getValue();
		Key newRefKey = new Key(BibTeXAccentParser.nearestAscii(refKey));
		
		BibTeXEntry newEntry = new BibTeXEntry(typeKey, newRefKey);
		
		Map<Key, Value> fields = entry.getFields();
		for(Key key : fields.keySet()){
			String value = fields.get(key).toUserString();
			
			if(! BibTeXEntryModel.isUnencodedField(key.getValue())){
				value = BibTeXAccentParser.unicodeToLatex(value);
				value = BibTeXAccentParser.replaceSpecialChars(value);
			}
			
			newEntry.addField(key, new StringValue( value, Style.BRACED ));
		}
		
		return newEntry;
	}

	public boolean warningsAsErrors() {
		return warningsAsErrors;
	}

	public String formatCitation(BibTeXEntry entry, boolean html){
		return refFormatter.format(entry, true, html);
	}
	
	public String formatCitations(List<BibTeXEntryModel> entries, boolean html) {
		StringBuilder sb = new StringBuilder();
		for(BibTeXEntryModel entry : entries) {
			sb.append(formatCitation(entry.getEntry(), html));
			sb.append("\n\n");
		}
		return sb.toString();
	}
}
