package com.mm.eratos.bibtex;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.LaTeXObject;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.TokenMgrError;
import org.jbibtex.Value;

import com.mm.eratos.io.bibtex.bibdesk.BibDeskParser;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.CaseInsensitiveMap;
import com.mm.eratos.utils.StringUtils;

public class BibTeXEntryModel
{
	public static final String UNPUBLISHED = "unpublished";
	public static final String TECHREPORT = "techreport";
	public static final String PROCEEDINGS = "proceedings";
	public static final String PHDTHESIS = "phdthesis";
	public static final String MISC = "misc";
	public static final String MASTERSTHESIS = "mastersthesis";
	public static final String MANUAL = "manual";
	public static final String INPROCEEDINGS = "inproceedings";
	public static final String INCOLLECTION = "incollection";
	public static final String INBOOK = "inbook";
	public static final String CONFERENCE = "conference";
	public static final String BOOKLET = "booklet";
	public static final String BOOK = "book";
	public static final String ARTICLE = "article";
	
	public static final String DELIMITER = ";";
	public static final String TAG_DELIMITERS = "[;,\t\n]+";
	public static final String ENCODING_DELIMITER = "|";
	public static final String DEFAULT_COLLECTION = "";
	
	public static final String[] TYPES = new String[] { ARTICLE, BOOK,
			BOOKLET, CONFERENCE, INBOOK, INCOLLECTION, INPROCEEDINGS,
			MANUAL, MASTERSTHESIS, MISC, PHDTHESIS, PROCEEDINGS,
			TECHREPORT, UNPUBLISHED };
	
	public static final String[] STANDARD_FIELDS = new String[] { "author", "abstract", "annote",
			"title", "chapter", "comments", "collectionid", "attachments",
			"tags", "keywords", "organization", "note", "number", "journal", "publisher",
			"month", "year", "school", "type", "address", "howpublished",
			"volume", "series", "editor", "institution", "edition", "isbn", "booktitle",
			"crossref", "pages", "key", "url", "important", "read", "groups", "date-added", "date-modified" };
	
	public static final String[] SPECIAL_FIELDS = new String[] { "crossref",
			"abstract", "comments", "collectionid",
			"tags", "keywords", "important", "read", "groups",
			"date-added", "date-modified"};
	
	public static final String[] NON_LATEX_ENCODED_FIELDS = new String[] { "crossref", 
			"collectionid", "tags", "keywords", "important", "read", "groups",
			"date-added", "date-modified", "url", "doi", "pmid", "pmcid", "isbn", "attachments"};
	
	public static final Key MARKED_READ_KEY = new Key("read");
	public static final Key IMPORTANT_KEY = new Key("important");
	public static final Key COLLECTIONS_KEY = new Key("collectionId");
	public static final Key GROUPS_KEY = new Key("groups");
	public static final Key TAGS_KEY = new Key("tags");
	public static final Key KEYWORDS_KEY = new Key("keywords");
	public static final Key ABSTRACT_KEY = new Key("abstract");
	public static final Key COMMENTS_KEY = new Key("comments");
	public static final Key TITLE_KEY = new Key("title");
	public static final Key SUBTITLE_KEY = new Key("subtitle");
	public static final Key AUTHOR_KEY = new Key("author");
	public static final Key EDITOR_KEY = new Key("editor");
	public static final Key MONTH_KEY = new Key("month");
	public static final Key ORG_KEY = new Key("organization");
	public static final Key DOI_KEY = new Key("doi");
	public static final Key PMID_KEY = new Key("pmid");
	public static final Key PMCID_KEY = new Key("pmcid");
	public static final Key ISBN_KEY = new Key("isbn");
	public static final Key URL_KEY = new Key("url");
	public static final Key YEAR_KEY = new Key("year");
	public static final Key TIMESTAMP_KEY = new Key("timestamp");
	
	public static final Key JOURNAL_KEY = new Key("journal");
	public static final Key PUBLISHER_KEY = new Key("publisher");
	public static final Key INSTITUTION_KEY = new Key("institution");
	public static final Key ORGANIZATION_KEY = new Key("organization");
	public static final Key SCHOOL_KEY = new Key("school");
	
	public static final Key CREATED_KEY = new Key("date-added");
	public static final Key MODIFIED_KEY = new Key("date-modified");
	public static final Key FILE_KEY = new Key("file");
	public static final Key XREF_KEY = new Key("crossref");
	public static final Key PMCITES_KEY = new Key("pmcites");
	public static final Key VOLUME_KEY = new Key("volume");
	public static final Key EDITION_KEY = new Key("edition");
	public static final Key NUMBER_KEY = new Key("number");
	public static final Key PAGES_KEY = new Key("pages");
	public static final Key ADDRESS_KEY = new Key("address");
	public static final Key LOCATION_KEY = new Key("location");
	
	public static final String eratosthenesLinkFieldPrefix = "external-link-";
	public static final String eratosthenesFileFieldPrefix = "attached-file-";
	
	public static final CaseInsensitiveHashSet TYPE_SET = new CaseInsensitiveHashSet( Arrays.asList(TYPES) );
	public static final CaseInsensitiveHashSet STANDARD_FIELD_SET = new CaseInsensitiveHashSet( Arrays.asList(STANDARD_FIELDS) );
	public static final CaseInsensitiveHashSet SPECIAL_FIELD_SET = new CaseInsensitiveHashSet( Arrays.asList(SPECIAL_FIELDS) );
	public static final CaseInsensitiveHashSet NON_LATEX_ENCODED_FIELD_SET = new CaseInsensitiveHashSet( Arrays.asList(NON_LATEX_ENCODED_FIELDS) );
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
	
	protected String title;
	protected String bibtex;
	protected String type;
	protected String refKey;
	protected String summary;
	protected String year;
	
	protected String collection;
	protected String comments;
	
	protected String created;
	protected String modified;
	
	protected String xrefKey;
	protected BibTeXEntryModel xref;
	
	protected List<EratosUri> documents;
	protected List<EratosUri> parsedLinks;
	protected List<EratosUri> externalLinks;
	
	protected Map<String, String> ids;
	protected List<String> keywords;
	protected List<String> groups;
	protected CaseInsensitiveMap<Value> fieldMap;
	protected boolean important;
	protected boolean read;
	protected String creator;
	protected String month;
	private String source;
	private List<Exception> warnings;
	private BibTeXEntry entry;
	
	public BibTeXEntryModel() {
		title = "";
		bibtex = "";
		type = "";
		refKey = "";
		summary = "";
		collection = "";
		comments = "";
		creator = "";
		year = "";
		
		documents = new ArrayList<EratosUri>();
		externalLinks = new ArrayList<EratosUri>();
		parsedLinks = new ArrayList<EratosUri>();
		
		keywords = new ArrayList<String>();
		groups = new ArrayList<String>();
		fieldMap = new CaseInsensitiveMap<Value>();
		
		ids = new HashMap<String, String>();
		
		created = dateFormat.format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
		modified = created;
		
		warnings = new ArrayList<Exception>();
		xref = null;
	}
	
	public BibTeXEntryModel(BibTeXEntry entry, BibTeXHelper helper, BibTeXEntryModel xref, Map<Key, BibTeXString> st, List<Exception> warnings) throws IOException, ParseException {
		this.xref = xref;
		this.entry = entry;
		this.warnings = warnings;
		
		this.bibtex = helper.formatBibTeX( entry );
		
		this.type = entry.getType().getValue();
		this.refKey = entry.getKey().getValue();

		documents = new ArrayList<EratosUri>();
		externalLinks = new ArrayList<EratosUri>();
		parsedLinks = new ArrayList<EratosUri>();
		
		keywords = new ArrayList<String>();
		groups = new ArrayList<String>();
		fieldMap = new CaseInsensitiveMap<Value>();
		
		ids = new HashMap<String, String>();
		parseFields(entry, st);
		parseXref();
		
		parseTitle(helper);
		
		parseIdTypes();
		
		parseKeywords();
		
		parseSummary();
		parseNotes();
		
		parseStatus();
		
		parseCreator(helper);
		parseDate(helper);
		
		parseFileFields();
		parseExternalLinks();
		
		parseTimestamps();
		
		parseSource(helper);
	}

	protected void parseFields(BibTeXEntry entry, Map<Key, BibTeXString> strings) {
		Map<Key, Value> fields = entry.getFields();
		for(Key k : fields.keySet()){
			Value value = fields.get(k);
			value.assignValues(strings);
			fieldMap.put(k.getValue(), value);
		}
	}
	
	private void parseXref() {
		if(hasField(XREF_KEY))
			xrefKey = getFieldValue(XREF_KEY);
		else
			xrefKey = "";
	}

	private void parseSource(BibTeXHelper helper) {
		if(hasField(JOURNAL_KEY)){
			source = fieldToUserString(helper, JOURNAL_KEY);
		} else if(hasField(PUBLISHER_KEY)){
			source = fieldToUserString(helper, PUBLISHER_KEY);
		} else if(hasField(INSTITUTION_KEY)){
			source = fieldToUserString(helper, INSTITUTION_KEY);
		} else if(hasField(ORGANIZATION_KEY)){
			source = fieldToUserString(helper, ORGANIZATION_KEY);
		} else if(hasField(SCHOOL_KEY)){
			source = fieldToUserString(helper, SCHOOL_KEY);
		} else{
			source = "";
		}
	}

	public String getSource() {
		return source;
	}
	
	protected void parseIdTypes() {
		if(hasField(DOI_KEY))
			ids.put("doi", getFieldValue(DOI_KEY));
		if(hasField(PMID_KEY))
			ids.put("pmid", getFieldValue(PMID_KEY));
		if(hasField(PMCID_KEY))
			ids.put("pmcid", getFieldValue(PMCID_KEY));
		if(hasField(ISBN_KEY))
			ids.put("isbn", getFieldValue(ISBN_KEY));
	}
	
	protected void parseTimestamps() {
		created = dateFormat.format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
		modified = created;
		
		if(hasField(CREATED_KEY)){
			created = getFieldValue(CREATED_KEY);
		}else{
			created = dateFormat.format(new Date());
		}
		
		if(hasField(MODIFIED_KEY)){
			modified = getFieldValue(MODIFIED_KEY);
		}else{
			modified = created;
		}
	}

	protected void parseDate(BibTeXHelper helper) {
		if(hasField(YEAR_KEY))
			year = parseYear(getFieldValue(YEAR_KEY));
		else
			year = "";
		
		if(hasField(MONTH_KEY))
			month = getFieldValue(MONTH_KEY);
		else
			month = "";
	}

	private String parseYear(String yearString) {
		Pattern p = Pattern.compile("[0-9]{4}");
		Matcher m = p.matcher(yearString);
		
		int latestYear = 0;
		while(m.find()){
			int year = Integer.parseInt(m.group(0));
			if(latestYear < year)
				latestYear = year;
		}
		if(latestYear==0)
			return "";
		return ""+latestYear;
	}

	protected void parseCreator(BibTeXHelper helper) {
		if(hasField(AUTHOR_KEY)){
			creator = parseAuthorList(fieldToUserString(helper, AUTHOR_KEY));
		}else if(hasField(EDITOR_KEY)){
			creator = parseAuthorList(fieldToUserString(helper, EDITOR_KEY));
		}else if(hasField(ORG_KEY)){
			creator = fieldToUserString(helper, ORG_KEY);
		}else{
			creator = "";
		}
	}
	
	private String parseAuthorList(String authors) {
		try {
			return new AuthorList(authors, AuthorList.LAST_WITH_INITIALS_STYLE).format("; ");
		} catch (ParseException e) {
			return authors;
		}
	}

	protected void addParsedLink(EratosUri url){
		if(url != null && !parsedLinks.contains(url))
			parsedLinks.add(url);
	}

	protected void parseExternalLinks() {
		if(hasField(DOI_KEY)){
			EratosUri doi_url = EratosUri.parseUri("http://dx.doi.org/"+getFieldValue(DOI_KEY));
			addParsedLink(doi_url);
		}
		if(hasField(PMID_KEY)){
			EratosUri pmid_url = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pubmed/"+getFieldValue(PMID_KEY));
			addParsedLink(pmid_url);
		}
		if(hasField(PMCID_KEY)){
			EratosUri pmcid_url = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/"+getFieldValue(PMCID_KEY));
			addParsedLink(pmcid_url);
		}
		if(hasField(URL_KEY)){
			EratosUri url = EratosUri.parseUri(getFieldValue(URL_KEY));
			addParsedLink(url);
		}
		
		List<String> urlFields = new ArrayList<String>();
		for(String key : filledFields()){
			if(BibTeXEntryVerifier.fieldStartsWith(key, eratosthenesLinkFieldPrefix))
				urlFields.add(key);
		}
		
		Collections.sort(urlFields);
		for(String key : urlFields){
			EratosUri uri = EratosUri.parseUri(getFieldValue(key));
			if(uri != null)
				externalLinks.add(uri);
			fieldMap.remove(key);
		}
		
		String excludeEndingPunct = "[^\\,\\?\\/\\\\\\(\\)\\{\\}\\.\\s]";
		String uri_regex = "([a-z]+://[^\\s]+"+excludeEndingPunct+")";
		String webpage_regex = "\\s(www\\.[a-zA-Z0-9\\-_]+\\.[a-zA-Z0-9\\-_][^\\s]*"+excludeEndingPunct+")";
		
		for(String url : findExternalLinks(uri_regex, getSummary()))
			addParsedLink(EratosUri.parseUri(url));
		for(String url : findExternalLinks(uri_regex, getNotes()))
			addParsedLink(EratosUri.parseUri(url));
		
		for(String webpage : findExternalLinks(webpage_regex, getSummary()))
			addParsedLink(EratosUri.parseUri("http://" + webpage));
		for(String webpage : findExternalLinks(webpage_regex, getNotes()))
			addParsedLink(EratosUri.parseUri("http://" + webpage));
	}

	protected List<String> findExternalLinks(String regex, String testString) {
		List<String> matches = new LinkedList<String>();
		if(testString == null)
			return matches;
		
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(testString);
		while(m.find()){
			String url = m.group(1);
			matches.add(url);
		}
		
		return matches;
	}

	protected void parseStatus() {
		if(hasField(IMPORTANT_KEY)){
			String value = getFieldValue(IMPORTANT_KEY);
			try{
				important = Boolean.parseBoolean(value);
			}catch(Exception e){
				important = false;
			}
		}else{
			important=false;
		}
		if(hasField(MARKED_READ_KEY)){
			String value = getFieldValue(MARKED_READ_KEY);
			try{
				read = Boolean.parseBoolean(value);
			}catch(Exception e){
				read = false;
			}
		}else{
			read=false;
		}
	}

	protected String fieldToUserString(BibTeXHelper helper, Key key) {
		String latex = getFieldValue(key);
		try{
			if(! BibTeXEntryModel.isUnencodedField(key.getValue()))
				return parseLaTeX(helper, latex);
		} catch(IllegalArgumentException e){
		} catch (Exception e) {
		}
		return latex;
	}
	
	protected String parseLaTeX(BibTeXHelper helper, String latex) {
		try{
			List<LaTeXObject> objects = helper.parseLaTeX(latex);
	        return helper.printLaTeX(objects);
		} catch(IllegalArgumentException e){
		} catch (Exception e) {
		}
		return latex;
	}

	protected void parseSummary() {
		if(hasField(ABSTRACT_KEY)) {
			summary = getFieldValue(ABSTRACT_KEY);
		} else {
			summary = "";
		}
	}
	
	protected void parseTitle(BibTeXHelper helper) {
		if(hasField(TITLE_KEY)){
	        title = fieldToUserString(helper, TITLE_KEY);
		}else{
			title = "";
		}
		
		if(hasField(SUBTITLE_KEY)){
			title += ": " + fieldToUserString(helper, SUBTITLE_KEY);
		}
	}
	
	protected void parseNotes() {
		if(hasField(COMMENTS_KEY)){
	        comments = getFieldValue(COMMENTS_KEY);
		}else{
			comments = "";
		}
	}
	
	protected void parseKeywords() {
		if(hasField(COLLECTIONS_KEY)){
			collection = getFieldValue(COLLECTIONS_KEY);
		}else{
			collection = DEFAULT_COLLECTION;
		}
		
		if(hasField(TAGS_KEY)){
			String value = getFieldValue(TAGS_KEY);
			value = value.trim().replaceAll("\\s+", " ");
			
			String []tagValues = value.split(TAG_DELIMITERS);
			for(String tag : tagValues){
				tag = tag.trim();
				if(!tag.isEmpty() && !keywords.contains(tag))
					keywords.add(tag);
			}
		}
		
		if(hasField(KEYWORDS_KEY)){
			String value = getFieldValue(KEYWORDS_KEY);
			value = value.trim().replaceAll("\\s+", " ");
			
			String []keywordValues = value.split(TAG_DELIMITERS);
			for(String keyword : keywordValues){
				keyword = keyword.trim();
				if(!keyword.isEmpty() && !keywords.contains(keyword))
					keywords.add(keyword);
			}
		}
		
		if(hasField(GROUPS_KEY)){
			String value = getFieldValue(GROUPS_KEY);
			value = value.trim().replaceAll("\\s+", " ");
			
			String []groupValues = value.split(TAG_DELIMITERS);
			for(String group : groupValues){
				group = group.trim();
				if(!group.isEmpty() && !groups.contains(group))
					groups.add(group);
			}
		}
	}
	
	protected void parseFileFields() {
		List<String> fileKeys = new ArrayList<String>();
		for(String key : filledFields()){
			if(BibTeXEntryVerifier.fieldStartsWith(key, eratosthenesFileFieldPrefix))
				fileKeys.add(key);
		}
		
		Collections.sort(fileKeys);
		for(String key : fileKeys){
			EratosUri uri = EratosUri.parseUri(getFieldValue(key));
			documents.add(uri);
			removeField(key);
		}
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}
	
	public String getRefKey() {
		return refKey;
	}
	
	public String getSuggestedAttachmentName() {
		String suggested = refKey.replaceAll("[:;'\",]", "");
		return suggested;
	}
	
	public String getHtmlCitation(BibTeXHelper helper) {
		try{
			return helper.formatCitation(entry, true);
		}catch(TokenMgrError tme) {
			return makeErrorText(tme);
		}catch(IllegalArgumentException ie){
			return makeErrorText(ie);
		}
	}
	
	public String getTextCitation(BibTeXHelper helper) {
		try{
			return helper.formatCitation(entry, false);
		}catch(TokenMgrError tme) {
			return makeErrorText(tme);
		}catch(IllegalArgumentException ie){
			return makeErrorText(ie);
		}
	}

	private String makeErrorText(Throwable tme) {
		String errorTxt = String.format(Locale.US, "Error: Could not generate citation for %s: %s", refKey, tme.getMessage());
		return errorTxt;
	}
	
	public String getTitle() {
		return title;
	}
	
	public Map<String, String> getIds() {
		return ids;
	}

	public void setNotes(String notes) {
		this.comments = notes;
		setField(COMMENTS_KEY, notes);
	}
	
	public String getNotes() {
		return comments;
	}

	public void setCollection(String collection) {
		this.collection = collection;
		setField(COLLECTIONS_KEY.getValue(), collection);
	}
	
	public String getCollection() {
		return collection;
	}
	
	public String toString() {
		return title;
	}
	

	public String formatGroups() {
		return StringUtils.join(DELIMITER, groups);
	}
	
	public List<String> getGroups() {
		return groups;
	}

	
	public void addGroup(String group) {
		if(!groups.contains(group)){
			groups.add(group);
			String value = StringUtils.join(DELIMITER, groups);
			setField(GROUPS_KEY, value);
		}
	}
	
	public void removeGroup(String group) {
		int index = groups.indexOf(group);
		if(index > -1) {
			groups.remove( index );
			if(groups.size() == 0){
				removeField(GROUPS_KEY);
			}else{
				String value = StringUtils.join(DELIMITER, groups);
				setField(GROUPS_KEY, value);
			}
		}
	}

	public String encodeGroups() {
		return ENCODING_DELIMITER + StringUtils.join(ENCODING_DELIMITER, groups) + ENCODING_DELIMITER;
	}

	public String encodeKeywords() {
		return ENCODING_DELIMITER + StringUtils.join(ENCODING_DELIMITER, keywords) + ENCODING_DELIMITER;
	}
	
	public String formatKeywords() {
		return StringUtils.join(DELIMITER, keywords);
	}
	
	public void addKeyword(String keyword) {
		if(!keywords.contains(keyword)){
			keywords.add(keyword);
			String value = StringUtils.join(DELIMITER, keywords);
			fieldMap.put(KEYWORDS_KEY.getValue(), new StringValue(value, Style.BRACED));
			fieldMap.remove(TAGS_KEY.getValue());
		}
	}
	
	public void removeKeyword(String keyword) {
		int index = keywords.indexOf(keyword);
		if(index > -1) {
			keywords.remove( index );
			if(keywords.size() == 0){
				fieldMap.remove(KEYWORDS_KEY.getValue());
				fieldMap.remove(TAGS_KEY.getValue());
			}else{
				String value = StringUtils.join(DELIMITER, keywords);
				fieldMap.put(KEYWORDS_KEY.getValue(), new StringValue(value, Style.BRACED));
				fieldMap.remove(TAGS_KEY.getValue());
			}
		}
	}
	
	public List<String> getKeywords() {
		return keywords;
	}
	
	public void attachFile(EratosUri uri) {
		if(!documents.contains(uri))
			documents.add(uri);
	}
	
	public void removeFile(int position) {
		documents.remove(position);
	}

	public void replaceFile(int filePos, EratosUri uri) {
		documents.set(filePos, uri);
	}
	
	public List<EratosUri> getFiles() {
		return new ArrayList<EratosUri>(documents);
	}
	
	public int getFileCount()
	{
		return documents.size();
	}
	
	public EratosUri getFile(int position)
	{
		return documents.get(position);
	}
	
	public List<EratosUri> getExternalLinks(){
		ArrayList<EratosUri> links = new ArrayList<EratosUri>(parsedLinks.size() + externalLinks.size());
		links.addAll(parsedLinks);
		links.addAll(externalLinks);
		return links;
	}
	
	public void addExternalLink(EratosUri url) {
		if(!externalLinks.contains(url)){
			externalLinks.add(url);
		}
	}
	
	public void removeExternalLink(EratosUri url){
		if(externalLinks.contains(url)) {
			externalLinks.remove(url);
		}
	}
	
	public boolean isParsedLink(EratosUri url) {
		return parsedLinks.contains(url);
	}
	
	public void setSummary(String summary) {
		this.summary = summary;
		setField(ABSTRACT_KEY, summary);
	}

	public String getSummary() {
		return summary;
	}
	
	public Map<String, String> getOtherFields() {
		Map<String, String> nonSpecial = new HashMap<String, String>();
		for(String key : filledFields()){
			if(! isReservedField(key) ){
				nonSpecial.put(key, fieldMap.get(key).toUserString());				
			}
		}
		return nonSpecial;
	}

	public BibTeXEntry getEntry() {
		return entry;
	}
	
	public BibTeXEntry createEntry() throws ParseException {
		BibTeXEntry entry = new BibTeXEntry(new Key(type), new Key(refKey));
		for(String key : filledFields()) {
			entry.addField(new Key(key), fieldMap.get(key));
		}
		
		int i = 1;
		for(EratosUri uri : externalLinks){
			entry.addField(new Key(eratosthenesLinkFieldPrefix + i), new StringValue(uri.toString(), Style.BRACED));
			i++;
		}
		
		i = 1;
		for(EratosUri documentUri : documents){
			entry.addField(new Key(eratosthenesFileFieldPrefix + i), new StringValue(documentUri.toString(), Style.BRACED));
			i++;
		}
		
		String now = dateFormat.format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
		entry.addField(CREATED_KEY, new StringValue(created, Style.BRACED));
		entry.addField(MODIFIED_KEY, new StringValue(now, Style.BRACED));
		
		return entry;
	}
	
	public CaseInsensitiveHashSet filledFields() {
		return new CaseInsensitiveHashSet(fieldMap.keySet());
	}
	
	public CaseInsensitiveHashSet xrefFields() {
		if(xref != null)
			return xref.filledFields();
		return new CaseInsensitiveHashSet();
	}

	public CaseInsensitiveHashSet allFields() {
		CaseInsensitiveHashSet filledFields = filledFields();
		filledFields.addAll(xrefFields());
		return filledFields;
	}
	
	public String setField(String key, String value){
		return setField(new Key(key), value);
	}
	
	public String setField(Key key, String value){
		if(value == null)
			return removeField(key);
		
		value = value.trim();
		if(value.isEmpty())
			return removeField(key);
		return putFieldValue(key, new StringValue(value, Style.BRACED)).toUserString();
	}
	
	public String setField(String key, Value value){
		return setField(new Key(key), value);
	}
	
	public String setField(Key key, Value value){
		if(value == null)
			return removeField(key);
		
		if(value.toUserString().isEmpty())
			return removeField(key);
		else
			return putFieldValue(key, value).toUserString();
	}
	
	protected Value putFieldValue(Key key, Value value) {
		
		if(key.equals(KEYWORDS_KEY.getValue()) || key.equals(TAGS_KEY.getValue())) {
			String []tags = value.toUserString().split(TAG_DELIMITERS);
			for(int i = 0; i < tags.length; i++)
				tags[i] = tags[i].trim();
			value = new StringValue(StringUtils.join(DELIMITER, tags), Style.BRACED);
			key = KEYWORDS_KEY;
		}
		
		if(key.equals(GROUPS_KEY.getValue())) {
			String []groups = value.toUserString().split(TAG_DELIMITERS);
			for(int i = 0; i < groups.length; i++)
				groups[i] = groups[i].trim();
			value = new StringValue(StringUtils.join(DELIMITER, groups), Style.BRACED);
		}
		
		fieldMap.put(key.getValue(), value);
		return value;
	}
	
	public String getFieldValue(Key field) {
		return getFieldValue(field.getValue());
	}
	
	public String getFieldValue(String field) {
		if(fieldMap.containsKey(field)) {
			String value = fieldMap.get(field).toUserString();
			return value;
		} else if(xref != null) {
			return xref.getFieldValue(field);
		}
		return null;
	}
	
	public String getFieldValue(String field, boolean convertLaTeX, BibTeXHelper helper) {
		if(fieldMap.containsKey(field)) {
			String value = fieldMap.get(field).toUserString();
			try{
				if( BibTeXEntryModel.isUnencodedField(field) && convertLaTeX )
					value = parseLaTeX(helper, value);
			} catch(IllegalArgumentException e){
			} catch (Exception e) {
			}
			return value;
		} else if(xref != null) {
			return xref.getFieldValue(field, convertLaTeX, helper);
		}
		return null;
	}
	
	public String removeField(Key key){
		return removeField(key.getValue());
	}
	
	public String removeField(String key)
	{
		if(fieldMap.containsKey(key))
			return fieldMap.remove(key).toUserString();
		return null;
	}
	
	public String getBibtex() {
		return bibtex;
	}
	
	public void setImportant(boolean important) {
		this.important = important;
		setField(IMPORTANT_KEY, Boolean.toString(important));
	}
	
	public void toggleImportant() {
		important = !important;
		setField(IMPORTANT_KEY, Boolean.toString(important));
	}
	
	public boolean important() {
		return important;
	}
	
	public boolean read(){
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
		setField(MARKED_READ_KEY.getValue(), Boolean.toString(read));
	}

	public boolean hasField(Key field) {
		return hasField(field.getValue());
	}
	
	public boolean hasField(String field) {
		return fieldMap.containsKey(field) || (xref!= null && xref.hasField(field));
	}

	public String getCreator() {
		return creator;
	}
	
	public String getYear(){ 
		return year;
	}

	public String getMonth() {
		return month;
	}
	
	public Date getCreated(){
		try{
			return dateFormat.parse(created);
		}catch(java.text.ParseException pe){
			pe.printStackTrace();
			return new Date();
		}
	}
	
	public Date getModified(){
		try{
			return dateFormat.parse(modified);
		}catch(java.text.ParseException pe){
			pe.printStackTrace();
			return new Date();
		}
	}
	
	public String getDateAddedString() {
		return created.substring(0, 10);
	}

	public String getDateModifiedString() {
		return modified.substring(0, 10);
	}

	public boolean hasAttachments() {
		return documents.size() > 0;
	}
	
	public BibTeXEntryModel getXref() {
		return xref;
	}

	public String getXrefKey() {
		return xrefKey;
	}

	public boolean hasXrefKey() {
		return !xrefKey.isEmpty();
	}
	
	public boolean hasXref() {
		return xref != null;
	}

	public BibTeXEntryModel copy() {
		BibTeXEntryModel copy = new BibTeXEntryModel();
		copy.title = title;
		copy.bibtex = bibtex;
		copy.type = type;
		copy.refKey = refKey;
		copy.summary = summary;
		copy.year = year;
		
		copy.collection = collection;
		copy.comments = comments;
		
		copy.created = created;
		copy.modified = modified;
		
		copy.documents = copyUriList(documents);
		copy.externalLinks = copyUriList(externalLinks);
		copy.parsedLinks = copyUriList(parsedLinks);
		
		copy.keywords = copyList(keywords);
		copy.groups = copyList(groups);
		copy.fieldMap = copyMap(fieldMap);
		
		copy.important = important;
		copy.read = read;
		copy.creator = creator;
		copy.month = month;
		
		copy.xref=null;
		if(xref != null)
			copy.xref = xref.copy();
		copy.xrefKey = xrefKey;
		
		return copy;
	}

	protected CaseInsensitiveMap<Value> copyMap(CaseInsensitiveMap<Value> original) {
		CaseInsensitiveMap<Value> copy = new CaseInsensitiveMap<Value>();
		for(String k : original.keySet()){
			copy.put(k, original.get(k));
		}
		return copy;
	}
	
	protected List<EratosUri> copyUriList(List<EratosUri> original) {
		List<EratosUri> copy = new ArrayList<EratosUri>(original);
		return copy;
	}

	protected List<String> copyList(List<String> original) {
		List<String> copy = new ArrayList<String>(original);
		return copy;
	}
	

	public  static boolean isReservedField(String key) {
		if(SPECIAL_FIELD_SET.contains(key))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, eratosthenesLinkFieldPrefix))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, eratosthenesFileFieldPrefix))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, BibDeskParser.bibdeskFileFieldPrefix))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, BibDeskParser.bibdeskUrlFieldPrefix))
			return true;
		
		return false;
	}
	
	public static boolean isUnencodedField(String key){
		if(NON_LATEX_ENCODED_FIELD_SET.contains(key))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, eratosthenesLinkFieldPrefix))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, eratosthenesFileFieldPrefix))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, BibDeskParser.bibdeskFileFieldPrefix))
			return true;
		if(BibTeXEntryVerifier.fieldStartsWith(key, BibDeskParser.bibdeskUrlFieldPrefix))
			return true;
		
		return false;
	}
	
	public List<Exception> getWarnings(){
		return warnings;
	}

	public Map<String, String> getFieldStrings(BibTeXHelper helper) {
		Map<String, String> fieldStrings = new CaseInsensitiveMap<String>();
		for(String k : fieldMap.keySet())
			fieldStrings.put(k, fieldToUserString(helper, new Key(k)));
		return fieldStrings;
	}
}
