package com.mm.eratos.bibtex;

public class MissingXrefException extends Exception {

	public MissingXrefException(String xrefKey) {
		super("Missing xref entry: " + xrefKey);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8338857154188564320L;
}
