package com.mm.eratos.bibtex;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;

import com.mm.eratos.bibtex.BibTeXEntryModelFactory.ReferenceResolver;

public class BibTeXNullReferenceResolver extends ReferenceResolver {

	@Override
	public BibTeXEntry resolveEntry(Key key) {
		return null;
	}

	@Override
	public BibTeXString resolveString(Key key) {
		return null;
	}

}
