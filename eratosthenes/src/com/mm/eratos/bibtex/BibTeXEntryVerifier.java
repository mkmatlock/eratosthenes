package com.mm.eratos.bibtex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.CaseInsensitiveMap;

public class BibTeXEntryVerifier {
	public static final String EDITION_KEY = "edition";
	public static final String INSTITUTION_KEY = "institution";
	public static final String SCHOOL_KEY = "school";
	public static final String CHAPTER_KEY = "chapter";
	public static final String EDITOR_KEY = "editor";
	public static final String BOOKTITLE_KEY = "booktitle";
	public static final String JOURNAL_KEY = "journal";
	public static final String PUBLISHER_KEY = "publisher";
	public static final String YEAR_KEY = "year";
	public static final String TITLE_KEY = "title";
	public static final String ORGANIZATION_KEY = "organization";
	public static final String ADDRESS_KEY = "address";
	public static final String SERIES_KEY = "series";
	public static final String PAGES_KEY = "pages";
	public static final String HOWPUBLISHED_KEY = "howpublished";
	public static final String NUMBER_KEY = "number";
	public static final String VOLUME_KEY = "volume";
	public static final String AUTHOR_KEY = "author";
	public static final String TYPE_KEY = "type";
	public static final String MONTH_KEY = "month";
	public static final String DOI_KEY = "doi";
	public static final String PMID_KEY = "pmid";
	public static final String URL_KEY = "url";
	
	private static final CaseInsensitiveMap<List<String>> requiredFields;
	private static final CaseInsensitiveMap<List<String>> optionalFields;
	private static final CaseInsensitiveHashSet booleanFields;
	
	static {
		requiredFields = new CaseInsensitiveMap<List<String>>();
		optionalFields = new CaseInsensitiveMap<List<String>>();
		booleanFields = new CaseInsensitiveHashSet();
		
		requiredFields.put("article", 		Arrays.asList(new String[]{AUTHOR_KEY, TITLE_KEY, JOURNAL_KEY, YEAR_KEY}));
		requiredFields.put("book", 			Arrays.asList(new String[]{AUTHOR_KEY +"/"+ EDITOR_KEY, TITLE_KEY, PUBLISHER_KEY, YEAR_KEY}));
		requiredFields.put("booklet", 		Arrays.asList(new String[]{TITLE_KEY}));
		requiredFields.put("conference", 	Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,BOOKTITLE_KEY,YEAR_KEY}));
		requiredFields.put("inbook", 		Arrays.asList(new String[]{AUTHOR_KEY +"/"+ EDITOR_KEY,TITLE_KEY,CHAPTER_KEY +"/"+ PAGES_KEY,PUBLISHER_KEY,YEAR_KEY}));
		requiredFields.put("incollection", 	Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,BOOKTITLE_KEY,PUBLISHER_KEY, YEAR_KEY}));
		requiredFields.put("inproceedings", Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,BOOKTITLE_KEY,YEAR_KEY}));
		requiredFields.put("manual", 		Arrays.asList(new String[]{TITLE_KEY}));
		requiredFields.put("mastersthesis", Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,SCHOOL_KEY,YEAR_KEY}));
		requiredFields.put("misc", 			Arrays.asList(new String[]{}));
		requiredFields.put("phdthesis", 	Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,SCHOOL_KEY,YEAR_KEY}));
		requiredFields.put("proceedings", 	Arrays.asList(new String[]{TITLE_KEY,YEAR_KEY}));
		requiredFields.put("techreport", 	Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,INSTITUTION_KEY,YEAR_KEY}));
		requiredFields.put("unpublished", 	Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY}));
		
		
		optionalFields.put("article", 		Arrays.asList(new String[]{VOLUME_KEY,NUMBER_KEY,PAGES_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("book", 			Arrays.asList(new String[]{VOLUME_KEY,NUMBER_KEY,SERIES_KEY,ADDRESS_KEY,EDITION_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("booklet", 		Arrays.asList(new String[]{AUTHOR_KEY,HOWPUBLISHED_KEY,ADDRESS_KEY,MONTH_KEY,YEAR_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("conference", 	Arrays.asList(new String[]{EDITOR_KEY, VOLUME_KEY, NUMBER_KEY, SERIES_KEY, PAGES_KEY, ADDRESS_KEY, MONTH_KEY, ORGANIZATION_KEY, PUBLISHER_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("inbook", 		Arrays.asList(new String[]{VOLUME_KEY,NUMBER_KEY,SERIES_KEY,TYPE_KEY,ADDRESS_KEY,EDITION_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("incollection", 	Arrays.asList(new String[]{EDITOR_KEY,VOLUME_KEY,NUMBER_KEY,SERIES_KEY,TYPE_KEY,CHAPTER_KEY,PAGES_KEY,ADDRESS_KEY,EDITION_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("inproceedings", Arrays.asList(new String[]{EDITOR_KEY,VOLUME_KEY,NUMBER_KEY,SERIES_KEY,PAGES_KEY,ADDRESS_KEY,MONTH_KEY,ORGANIZATION_KEY,PUBLISHER_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("manual", 		Arrays.asList(new String[]{AUTHOR_KEY,ORGANIZATION_KEY,ADDRESS_KEY,EDITION_KEY,MONTH_KEY,YEAR_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("mastersthesis", Arrays.asList(new String[]{TYPE_KEY,ADDRESS_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("misc", 			Arrays.asList(new String[]{AUTHOR_KEY,TITLE_KEY,HOWPUBLISHED_KEY,MONTH_KEY,YEAR_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("phdthesis", 	Arrays.asList(new String[]{TYPE_KEY,ADDRESS_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("proceedings", 	Arrays.asList(new String[]{EDITOR_KEY,VOLUME_KEY,NUMBER_KEY,SERIES_KEY,ADDRESS_KEY,MONTH_KEY,PUBLISHER_KEY,ORGANIZATION_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("techreport", 	Arrays.asList(new String[]{TYPE_KEY,NUMBER_KEY,ADDRESS_KEY,MONTH_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
		optionalFields.put("unpublished", 	Arrays.asList(new String[]{MONTH_KEY,YEAR_KEY, DOI_KEY, PMID_KEY, URL_KEY}));
	
		booleanFields.add("important");
		booleanFields.add("read");
	}
	
	public static List<Exception> checkEntry(String type, CaseInsensitiveHashSet filledFields, boolean xrefExists, boolean throwErrors) throws FieldRequiredException {
		List<Exception> warnings = new ArrayList<Exception>();
				
		if(!requiredFields.containsKey(type)){
			type = "misc";
			warnings.add(new FieldRequiredException("Unrecognized entry type: '" + type + "'"));
		}
		
		if(filledFields.contains("crossref") && !xrefExists){
			warnings.add(new FieldRequiredException("Missing crossref entry!"));
		}
		
		List<String> required = requiredFields.get(type);
		for(String keyset : required){
			String []keys = keyset.split("/");
			
			boolean pass = false;
			for(String key : keys){
				pass = pass || filledFields.contains(key);
			}
			if(!pass){
				FieldRequiredException error = new FieldRequiredException(type, keyset);
				if(throwErrors) 
					throw error;
				warnings.add(error);
			}
		}
		
		return warnings;
	}
	
	private static List<String> parseFields(List<String> fieldSets) {
		List<String> fieldList = new ArrayList<String>(fieldSets.size());
		for(String keyset : fieldSets){
			String []keys = keyset.split("/");
			for(String key : keys){
				fieldList.add(key);
			}
		}
		return fieldList;
	}
	
	public static List<String> getRequiredFields(String type) {
		if(requiredFields.containsKey(type))
			return parseFields(requiredFields.get(type));
		return new ArrayList<String>();
	}
	
	public static List<String> getOptionalFields(String type) {
		if(optionalFields.containsKey(type))
			return parseFields(optionalFields.get(type));
		return new ArrayList<String>();
	}

	public static boolean isBooleanField(String keyVal) {
		return booleanFields.contains(keyVal);
	}
	
	public static boolean fieldStartsWith(String key, String prefix){
		if(prefix.length() > key.length())
			return false;
		return prefix.equalsIgnoreCase(key.substring(0, prefix.length()));
	}
}
