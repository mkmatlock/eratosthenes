package com.mm.eratos.bibtex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;

import com.mm.eratos.utils.CaseInsensitiveHashSet;
import com.mm.eratos.utils.StringUtils;

public class CiteKeyFormatter {
	private static final Set<String> throwAwayWords = new CaseInsensitiveHashSet();
	static{
		throwAwayWords.add("a");
		throwAwayWords.add("an");
		throwAwayWords.add("the");
		throwAwayWords.add("of");
		throwAwayWords.add("with");
		throwAwayWords.add("in");
		throwAwayWords.add("for");
	}
	
	private String format;

	public CiteKeyFormatter(String format) {
		this.format = format;
	}
	
	public String format(BibTeXEntryModel entry){
		String citeKey = new String(format);
		
		String titleString = BibTeXAccentParser.nearestAscii(entry.getTitle()).replaceAll("[\'\"\\/\\(\\)\\{\\}\\[\\]]", "").toLowerCase(Locale.US);
		String creatorString = BibTeXAccentParser.nearestAscii(entry.getCreator()).replaceAll("[\'\"\\/\\(\\)\\{\\}\\[\\]]", "");
		String month = entry.getMonth().toLowerCase(Locale.US);
		String year = entry.getYear();
		
		String []titleWords = titleString.split("[, :;.\t?&]+");
		String []creators = parseAuthors(creatorString);
		
		citeKey = citeKey.replace("%a", creators[0]);
		citeKey = citeKey.replace("%A", creators[0]+"_"+creators[1]);
		citeKey = citeKey.replace("%t", getTitleWords(titleWords, 1));
		citeKey = citeKey.replace("%T", getTitleWords(titleWords, 3));
		citeKey = citeKey.replace("%m", month);
		citeKey = citeKey.replace("%y", year);
		citeKey = citeKey.replace(" ", "");
		
		BibTeXAccentParser.nearestAscii(citeKey);
		
		return citeKey;
	}
	
	public String format(BibTeXEntry entry, BibTeXHelper helper) throws IOException, ParseException {
		BibTeXEntryModel entryModel = new BibTeXEntryModel(entry, helper, null, new HashMap<Key, BibTeXString>(), new ArrayList<Exception>());
		return format(entryModel);
	}
	
	private String[] parseAuthors(String creatorString) {
		// TODO Auto-generated method stub
		String creators[] = creatorString.split(" and ");
		List<String> creatorList = new ArrayList<String>();
		for(String creator : creators){
			if(creator.contains(",")){
				String last_first[] = creator.split(",");
				creatorList.add(last_first[0].trim().toLowerCase(Locale.US));
				creatorList.add(last_first[1].trim().toLowerCase(Locale.US));
			}else {
				String names[] = creator.split("\\s+");
				String first = names[0].trim().toLowerCase(Locale.US);
				String last = "";
				for(int i = 1; i < names.length; i++)
					last += names[i].trim().toLowerCase(Locale.US);
				
				creatorList.add(last);
				creatorList.add(first);
			}
		}
		
		return creatorList.toArray(new String[]{});
	}

	private CharSequence getTitleWords(String[] titleWords, int i) {
		List<String> formatted = new ArrayList<String>(i);
		int j = 0;
		int k = 0;
		while(j < i && k < titleWords.length){
			if(!throwAwayWords.contains(titleWords[k])){
				formatted.add(titleWords[k]);
				j++;
			}
			k++;
		}
		return StringUtils.join("_", formatted);
	}
}
