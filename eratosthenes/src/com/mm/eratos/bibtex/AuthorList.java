package com.mm.eratos.bibtex;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.jbibtex.ParseException;

import com.mm.eratos.utils.StringUtils;

public class AuthorList {
	public static final int LAST_FIRST_STYLE = 0;
	public static final int LAST_FIRST_MIDDLE_STYLE = 1;
	public static final int LAST_WITH_INITIALS_STYLE = 2;
	
	public class Author{
		String first;
		String middle;
		String last;
		String suffix;
		
		public String getFirstName(){
			return last;
		}
		
		public String getMiddleName(){
			return middle;
		}
		
		public String getLastName(){
			return last;
		}

		public String getSuffix() {
			return suffix;
		}
		
		public String toString(){
			if(first.isEmpty())
				return last;
			
			if(style == LAST_FIRST_STYLE)
				return last + ", " + first;
			
			if(style == LAST_FIRST_MIDDLE_STYLE && middle.isEmpty())
				return last + ", " + first;
			if(style == LAST_FIRST_MIDDLE_STYLE)
				return last + ", " + first + " " + middle;
			
			if(style == LAST_WITH_INITIALS_STYLE && middle.isEmpty())
				return last + ", " + first.substring(0,1);
			if(style == LAST_WITH_INITIALS_STYLE)
				return last + ", " + first.substring(0,1) + middleInitials();
			
			return "";
		}
		
		public String middleInitials(){
			String mi = "";
			for(String mname : middle.split("\\s+")){
				mi+=mname.substring(0,1).toUpperCase(Locale.US);
			}
			return mi;
		}
	}
	
	int style;
	List<Author> authors;
	
	public AuthorList(String authorString, int style) throws ParseException{
		this.style = style;
		authorString = authorString.trim();
		try{
			if(authorString.isEmpty()){
				authors = new ArrayList<Author>(0);
			}else{
				String []list = authorString.split(" [Aa][Nn][Dd] ");
				authors = new ArrayList<Author>(list.length);
				
				for(String aStr : list){
					Author a = parseAuthor(aStr);
					authors.add(a);
				}
			}
		}catch(Error e){
			throw new ParseException(String.format(Locale.US, "Failed to parse '%s': %s", authorString, e.getMessage()));
		}catch(Exception e){
			throw new ParseException(String.format(Locale.US, "Failed to parse '%s': %s", authorString, e.getMessage()));
		}
	}
	
	// Valid forms:
	// First Last
	// Last, First
	// Last, Suffix, First
	private Author parseAuthor(String aStr) {
		Author a = new Author();
		
		if(aStr.contains(","))
			parseLNF(aStr, a);
		else
			parseFNF(aStr, a);
		return a;
	}

	private void parseFNF(String aStr, Author a) {
		String[] items = aStr.trim().split("\\s+");
		
		a.first = items[0];
		a.suffix = "";
		
		int connectiveIndex = findConnective(items);
		if(items.length == 1){
			a.last = items[0];
			a.first = "";
			a.middle = "";
		}else if(items.length == 2){
			a.last = items[1];
			a.middle = "";
		}else if(connectiveIndex > -1){
			a.last = StringUtils.join(" ", items, connectiveIndex);
			parseMiddle(a, items, 1, connectiveIndex);
		}else{
			a.last = items[items.length-1];
			parseMiddle(a, items, 1, items.length-1);
		}
	}
	
	private int findConnective(String[] items){
		for(int i = 1; i<items.length; i++){
			String name = items[i];
			if(isConnective(name))
				return i;
		}
		return -1;
	}

	private boolean isConnective(String name) {
		if("de".equalsIgnoreCase(name)) return true;
		if("di".equalsIgnoreCase(name)) return true;
		if("van".equalsIgnoreCase(name)) return true;
		if("von".equalsIgnoreCase(name)) return true;
		return false;
	}

	private void parseLNF(String aStr, Author a) {
		String[] items = aStr.split(",");
		if(items.length == 2){
			parseLNFnoSuffix(a, items);
		}else{
			parseLNFsuffix(a, items);
		}
	}

	private void parseLNFnoSuffix(Author a, String[] items) {
		a.last = items[0].trim();
		String[] fn = items[1].trim().split("\\s+");
		
		a.first = fn[0].trim();
		parseMiddle(a, fn, 1, fn.length);
		a.suffix = "";
	}

	private void parseLNFsuffix(Author a, String[] items) {
		a.last = items[0].trim();
		a.suffix = items[1].trim();
		String[] fn = items[2].trim().split("\\s+");
		
		a.first = fn[0].trim();
		parseMiddle(a, fn, 1, fn.length);
	}

	private void parseMiddle(Author a, String[] fn, int start, int end) {
		a.middle = "";
		for(int i = start; i < end; i++)
			a.middle += fn[i].trim() + " ";
		a.middle = a.middle.trim();
	}

	public List<Author> list(){
		return new ArrayList<Author>(authors);
	}
	
	public Author get(int i){
		return authors.get(i);
	}
	
	public String format(String delimiter){
		return StringUtils.join(delimiter, authors);
	}

	public int size() {
		return authors.size();
	}
}
