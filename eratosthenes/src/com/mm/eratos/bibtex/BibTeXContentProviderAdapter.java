package com.mm.eratos.bibtex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.database.DBHelper;
import com.mm.eratos.exceptions.KeyCollisionException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.search.db.QueryElement;

public class BibTeXContentProviderAdapter {
	public static final int SORT_COLLECTION = 0;
	public static final int SORT_TITLE = 1;
	public static final int SORT_AUTHOR = 2;
	public static final int SORT_YEAR = 3;
	public static final int SORT_DATE_ADDED = 4;
	public static final int SORT_DATE_MODIFIED = 5;
	public static final int SORT_SOURCE = 6;
	public static final int SORT_RANDOM = 7;
	
	public static final int CUSTOM_SORT_BIT = 128;
	
	public static final int SORT_ASCENDING = 0;
	public static final int SORT_DESCENDING = 1;
	
	public static final String NO_GROUP_FIELD_TITLE = "Not Grouped";
	
	public interface BulkInsertUpdateListener{
		public void update(int progress, int max);
	}
	
	private final ContentResolver contentResolver;
	private EratosLogger logger;

	public BibTeXContentProviderAdapter(ContentResolver contentResolver) {
		this.contentResolver = contentResolver;
		this.logger = EratosApplication.getApplication().getLogManager().getLogger(BibTeXContentProviderAdapter.class);
	}
	
	public CursorLoader getRecentFiles(Context context) {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_TYPE, DBHelper.COLUMN_ENCODING, DBHelper.COLUMN_PROTOCOL, DBHelper.COLUMN_PATH, DBHelper.COLUMN_COUNT};
		return new CursorLoader(context, BibTeXContentProvider.RECENT_FILES_URI, projection, null, null, DBHelper.COLUMN_PROTOCOL + ", " + DBHelper.COLUMN_PATH);
	}
	
	public BibFile cursorToRecentFile(Cursor c) {
		String type = c.getString(c.getColumnIndex(DBHelper.COLUMN_TYPE));
		String encoding = c.getString(c.getColumnIndex(DBHelper.COLUMN_ENCODING));
		String protocol = c.getString(c.getColumnIndex(DBHelper.COLUMN_PROTOCOL));
		String path = c.getString(c.getColumnIndex(DBHelper.COLUMN_PATH));
		EratosUri uri = EratosUri.parseUri(protocol + "://" + path);
		
		int entryCount = c.getInt(c.getColumnIndex(DBHelper.COLUMN_COUNT));
		return new BibFile(uri, type, encoding, entryCount);
	}
	
	public void updateRecentFile(BibFile bibFile) {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_TYPE, DBHelper.COLUMN_PATH, DBHelper.COLUMN_PROTOCOL, DBHelper.COLUMN_COUNT};
		Cursor c = contentResolver.query(BibTeXContentProvider.RECENT_FILES_URI, projection, String.format("%s = ? and %s = ?", DBHelper.COLUMN_PROTOCOL, DBHelper.COLUMN_PATH), new String[]{bibFile.uri().protocol(), bibFile.uri().path()}, null);
		
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_TYPE, bibFile.type());
		values.put(DBHelper.COLUMN_ENCODING, bibFile.encoding());
		values.put(DBHelper.COLUMN_PROTOCOL, bibFile.uri().protocol());
		values.put(DBHelper.COLUMN_PATH, bibFile.uri().path());
		values.put(DBHelper.COLUMN_COUNT, bibFile.getEntryCount());
		
		if(c.getCount() == 0) {
			contentResolver.insert(BibTeXContentProvider.RECENT_FILES_URI, values);
		} else {
			String select = String.format("%s = ? and %s = ?", DBHelper.COLUMN_PROTOCOL, DBHelper.COLUMN_PATH);
			String []selectionArgs = new String[]{bibFile.uri().protocol(), bibFile.uri().path()};
			contentResolver.update(BibTeXContentProvider.RECENT_FILES_URI, values, select, selectionArgs);
		}
		c.close();
	}
	
	public void removeRecentFile(BibFile bibFile){
		contentResolver.delete(BibTeXContentProvider.RECENT_FILES_URI, String.format("%s = ? and %s = ?", DBHelper.COLUMN_PROTOCOL, DBHelper.COLUMN_PATH), new String[]{bibFile.uri().protocol(), bibFile.uri().path()});
	}
	
	public void update(BibTeXEntryModel entryModel) {
		String refKey = entryModel.getRefKey();
		
		ContentValues values = getEntryValues(entryModel);
		
		contentResolver.update(BibTeXContentProvider.ENTRY_URI, values, String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.GROUPS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.COLLECTIONS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.KEYWORDS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.DOCUMENT_IDS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.SORT_FIELDS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		
		if(!entryModel.getCollection().isEmpty()){
			ContentValues cvalues = getCollectionValues(entryModel);
			contentResolver.insert(BibTeXContentProvider.COLLECTIONS_URI, cvalues);
		}
		bulkInsertKeywords(entryModel);
		bulkInsertGroups(entryModel);
		bulkInsertDocIds(entryModel);
		bulkInsertSortFields(entryModel);
	}
	
	public void insert(BibTeXEntryModel entryModel) throws KeyCollisionException {
		String refKey = entryModel.getRefKey();
		Cursor cursor = contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{DBHelper.COLUMN_KEY}, 
								String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey}, null);
		int count = cursor.getCount();
		cursor.close();
		if(count > 0){
			throw new KeyCollisionException(String.format("The refKey '%s' is already in use.", refKey));
		}
		
		ContentValues values = getEntryValues(entryModel);
		contentResolver.insert(BibTeXContentProvider.ENTRY_URI, values);
		
		if(!entryModel.getCollection().isEmpty()){
			ContentValues cvalues = getCollectionValues(entryModel);
			contentResolver.insert(BibTeXContentProvider.COLLECTIONS_URI, cvalues);
		}
		bulkInsertKeywords(entryModel);
		bulkInsertGroups(entryModel);
		bulkInsertDocIds(entryModel);
	}

	private void bulkInsertDocIds(BibTeXEntryModel entryModel) {
		Map<String, String> ids = entryModel.getIds();
		ContentValues[] idValueSets = new ContentValues[ids.size()];
		int i = 0;
		for(String idType : ids.keySet()){
			ContentValues idValues = getDocumentIdValues(entryModel, ids, idType);
			idValueSets[i] = idValues;
			i++;
		}
		contentResolver.bulkInsert(BibTeXContentProvider.DOCUMENT_IDS_URI, idValueSets);
	}

	private void bulkInsertGroups(BibTeXEntryModel entryModel) {
		List<String> groups = entryModel.getGroups();
		ContentValues[] grpValueSets = new ContentValues[groups.size()];
		int i = 0;
		for(String grp : groups) {
			ContentValues grpValues = getGroupValues(entryModel, grp);
			grpValueSets[i] = grpValues;
			i++;
		}
		if(groups.size() == 0) {
			grpValueSets = new ContentValues[1];
			grpValueSets[0] = getGroupValues(entryModel, NO_GROUP_FIELD_TITLE);
		}
		
		contentResolver.bulkInsert(BibTeXContentProvider.GROUPS_URI, grpValueSets);
	}

	private void bulkInsertKeywords(BibTeXEntryModel entryModel) {
		List<String> keywords = entryModel.getKeywords();
		ContentValues[] kwValueSets = new ContentValues[keywords.size()];
		int i = 0;
		for(String kw : keywords){
			ContentValues kwValues = getKeywordValues(entryModel, kw);
			kwValueSets[i] = kwValues;
			i++;
		}
		contentResolver.bulkInsert(BibTeXContentProvider.KEYWORDS_URI, kwValueSets);
	}

	private void bulkInsertSortFields(BibTeXEntryModel entryModel) {
		Set<Key> keySet = entryModel.getEntry().getFields().keySet();
		ContentValues[] sfValueSets = new ContentValues[keySet.size()];
		int i = 0;
		for(Key field : keySet){
			ContentValues sfValues = getSortFieldValues(entryModel, field);
			sfValueSets[i] = sfValues;
			i++;
		}
		contentResolver.bulkInsert(BibTeXContentProvider.SORT_FIELDS_URI, sfValueSets);
	}
	
	public ContentValues getSortFieldValues(BibTeXEntryModel entryModel, Key key) {
		String value = entryModel.getFieldValue(key);
		if(value == null) value = "";
		ContentValues sfValues = new ContentValues();
		sfValues.put(DBHelper.COLUMN_REFKEY, entryModel.getRefKey());
		sfValues.put(DBHelper.COLUMN_FIELD, key.getValue());
		sfValues.put(DBHelper.COLUMN_VALUE, value);
		return sfValues;
	}

	private ContentValues getDocumentIdValues(BibTeXEntryModel entry, Map<String, String> ids, String idType) {
		ContentValues idvalues = new ContentValues();
		idvalues.put(DBHelper.COLUMN_DOC_ID_TYPE, idType);
		idvalues.put(DBHelper.COLUMN_DOC_ID, ids.get(idType));
		idvalues.put(DBHelper.COLUMN_REFKEY, entry.getRefKey());
		return idvalues;
	}

	private ContentValues getCollectionValues(BibTeXEntryModel entry) {
		ContentValues cvalues = new ContentValues();
		cvalues.put(DBHelper.COLUMN_COLLECTION, entry.getCollection());
		cvalues.put(DBHelper.COLUMN_REFKEY, entry.getRefKey());
		return cvalues;
	}

	private ContentValues getGroupValues(BibTeXEntryModel entry, String grp) {
		ContentValues grpvalues = new ContentValues();
		grpvalues.put(DBHelper.COLUMN_GROUP, grp);
		grpvalues.put(DBHelper.COLUMN_REFKEY, entry.getRefKey());
		return grpvalues;
	}

	private ContentValues getKeywordValues(BibTeXEntryModel entry, String kw) {
		ContentValues kwvalues = new ContentValues();
		kwvalues.put(DBHelper.COLUMN_KEYWORD, kw);
		kwvalues.put(DBHelper.COLUMN_REFKEY, entry.getRefKey());
		return kwvalues;
	}

	private ContentValues getEntryValues(BibTeXEntryModel entryModel) {
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_KEY, entryModel.getRefKey());
		values.put(DBHelper.COLUMN_KEYWORDS, entryModel.encodeKeywords());
		values.put(DBHelper.COLUMN_GROUPS, entryModel.encodeGroups());
		values.put(DBHelper.COLUMN_TYPE, entryModel.getType());
		values.put(DBHelper.COLUMN_TITLE, entryModel.getTitle());
		values.put(DBHelper.COLUMN_AUTHOR, entryModel.getCreator());
		values.put(DBHelper.COLUMN_YEAR, entryModel.getYear());
		values.put(DBHelper.COLUMN_COLLECTION, entryModel.getCollection());
		values.put(DBHelper.COLUMN_SOURCE, entryModel.getSource());
		values.put(DBHelper.COLUMN_DATE_ADDED, entryModel.getDateAddedString());
		values.put(DBHelper.COLUMN_DATE_MODIFIED, entryModel.getDateModifiedString());
		values.put(DBHelper.COLUMN_IMPORTANT, entryModel.important() ? 1 : 0);
		values.put(DBHelper.COLUMN_READ, entryModel.read() ? 1 : 0);
		values.put(DBHelper.COLUMN_HASATTACHMENTS, entryModel.hasAttachments() ? 1 : 0);
		values.put(DBHelper.COLUMN_ATTACHMENT, entryModel.hasAttachments() ? entryModel.getFile(0).toString() : "");
		values.put(DBHelper.COLUMN_ENTRY, entryModel.getBibtex());
		values.put(DBHelper.COLUMN_XREF, entryModel.getXrefKey());
		
		return values;
	}
	
	public void bulkInsert(List<BibTeXEntryModel> entryModels, BulkInsertUpdateListener bulkInsertUpdateListener) {
		bulkInsert(entryModels, 0, entryModels.size(), bulkInsertUpdateListener);
	}

	public void bulkInsert(List<BibTeXEntryModel> entryModels, int stepSize, BulkInsertUpdateListener bulkInsertUpdateListener) {
		for(int i = 0; i < entryModels.size(); i += stepSize){
			bulkInsert(entryModels, i, stepSize, bulkInsertUpdateListener);
		}
	}
	
	protected void bulkInsert(List<BibTeXEntryModel> entryModels, int start, int num, BulkInsertUpdateListener bulkInsertUpdateListener) {
		ContentValues[] entries = new ContentValues[entryModels.size()];

		List<ContentValues> collections = new ArrayList<ContentValues>();
		List<ContentValues> keywords = new ArrayList<ContentValues>();
		List<ContentValues> groups = new ArrayList<ContentValues>();
		List<ContentValues> documentIds = new ArrayList<ContentValues>();
		List<ContentValues> sortFieldValues = new ArrayList<ContentValues>();
		
		
		for(int i = start; i < start+num && i < entryModels.size(); i++) {
			BibTeXEntryModel entry = entryModels.get(i);
			Set<Key> keySet = entry.getEntry().getFields().keySet();
			entries[i] = getEntryValues(entry);
			
			List<String> entryGroups = entry.getGroups();
			for(String grp : entryGroups)
				groups.add(getGroupValues(entry, grp));
			if(entryGroups.size() == 0)
				groups.add(getGroupValues(entry, NO_GROUP_FIELD_TITLE));
			
			for(String kw : entry.getKeywords())
				keywords.add(getKeywordValues(entry, kw));
			
			for(Key k : keySet)
				sortFieldValues.add(getSortFieldValues(entry, k));
			
			Map<String, String> docIds = entry.getIds();
			for(String idType : docIds.keySet())
				documentIds.add(getDocumentIdValues(entry, docIds, idType));
			
			if(!entry.getCollection().isEmpty())
				collections.add(getCollectionValues(entry));
			
			bulkInsertUpdateListener.update(i+1, entryModels.size());
		}
		
		contentResolver.bulkInsert(BibTeXContentProvider.ENTRY_URI, entries);
		contentResolver.bulkInsert(BibTeXContentProvider.GROUPS_URI, groups.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.COLLECTIONS_URI, collections.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.KEYWORDS_URI, keywords.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.DOCUMENT_IDS_URI, documentIds.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.SORT_FIELDS_URI, sortFieldValues.toArray(new ContentValues[]{}));
	}
	
	public void bulkInsertData(List<BibTeXEntryModel> entryModels) {
		List<ContentValues> collections = new ArrayList<ContentValues>();
		List<ContentValues> keywords = new ArrayList<ContentValues>();
		List<ContentValues> groups = new ArrayList<ContentValues>();
		List<ContentValues> documentIds = new ArrayList<ContentValues>();
		List<ContentValues> sortFieldValues = new ArrayList<ContentValues>();

		for(BibTeXEntryModel entry : entryModels) {
			Set<Key> keySet = entry.getEntry().getFields().keySet();
			
			List<String> entryGroups = entry.getGroups();
			for(String grp : entryGroups)
				groups.add(getGroupValues(entry, grp));
			if(entryGroups.size() == 0)
				groups.add(getGroupValues(entry, NO_GROUP_FIELD_TITLE));
			
			for(String kw : entry.getKeywords())
				keywords.add(getKeywordValues(entry, kw));

			for(Key k : keySet)
				sortFieldValues.add(getSortFieldValues(entry, k));
			
			Map<String, String> docIds = entry.getIds();
			for(String idType : docIds.keySet())
				documentIds.add(getDocumentIdValues(entry, docIds, idType));
			
			if(!entry.getCollection().isEmpty())
				collections.add(getCollectionValues(entry));
		}
		
		contentResolver.bulkInsert(BibTeXContentProvider.GROUPS_URI, groups.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.COLLECTIONS_URI, collections.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.KEYWORDS_URI, keywords.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.DOCUMENT_IDS_URI, documentIds.toArray(new ContentValues[]{}));
		contentResolver.bulkInsert(BibTeXContentProvider.SORT_FIELDS_URI, sortFieldValues.toArray(new ContentValues[]{}));
	}
	
	public void bulkInsert(BibTeXHelper helper, List<BibTeXString> strings) throws IOException {
		ContentValues[] content = new ContentValues[strings.size()];
		
		int i = 0;
		for(BibTeXString st : strings){
			String key = st.getKey().getValue();
			String value = st.getValue().toUserString();
			String bibtex = helper.formatBibTeX(st);
			ContentValues values = new ContentValues();
			values.put(DBHelper.COLUMN_KEY, key);
			values.put(DBHelper.COLUMN_VALUE, value);
			values.put(DBHelper.COLUMN_ENTRY, bibtex);
			
			content[i] = values;
			i++;
		}
		contentResolver.bulkInsert(BibTeXContentProvider.STRINGS_URI, content);
	}
	
	public void insert(BibTeXStringModel st) throws KeyCollisionException {
		String key = st.getKey();
		String value = st.getValue();
		String bibtex = st.getBibtex();
		
		Cursor cursor = contentResolver.query(BibTeXContentProvider.STRINGS_URI, new String[]{DBHelper.COLUMN_KEY}, 
				String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{key}, null);
		int count = cursor.getCount();
		cursor.close();
		if(count > 0)
			throw new KeyCollisionException(String.format("The string key '%s' is already in use.", key));
		
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_KEY, key);
		values.put(DBHelper.COLUMN_VALUE, value);
		values.put(DBHelper.COLUMN_ENTRY, bibtex);
		contentResolver.insert(BibTeXContentProvider.STRINGS_URI, values);
	}
	
	public void insert(BibTeXHelper helper, BibTeXString st) throws IOException, KeyCollisionException {
		String key = st.getKey().getValue();
		String value = st.getValue().toUserString();
		String bibtex = helper.formatBibTeX(st);
		
		Cursor cursor = contentResolver.query(BibTeXContentProvider.STRINGS_URI, new String[]{DBHelper.COLUMN_KEY}, 
								String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{key}, null);
		
		int count = cursor.getCount();
		cursor.close();
		if(count > 0)
			throw new KeyCollisionException(String.format("The string key '%s' is already in use.", key));
		
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_KEY, key);
		values.put(DBHelper.COLUMN_VALUE, value);
		values.put(DBHelper.COLUMN_ENTRY, bibtex);
		contentResolver.insert(BibTeXContentProvider.STRINGS_URI, values);
	}
	
	public void insert(BibTeXHelper helper, BibTeXObject obj) throws IOException {
		String bibtex = helper.formatBibTeX(obj);
		
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_ENTRY, bibtex);
		contentResolver.insert(BibTeXContentProvider.OBJECTS_URI, values);
	}
	
	public boolean createFilter(String name, QueryElement query) throws IOException {
		Cursor c = contentResolver.query(BibTeXContentProvider.FILTERS_URI, null, DBHelper.COLUMN_TITLE + " = ?", new String[]{name}, null);
		int count = c.getCount();
		c.close();
		if(count > 0)
			return false;
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		new ObjectOutputStream(output).writeObject(query);
		String serialQuery = Base64.encode(output.toByteArray());
		
		ContentValues filterValues = new ContentValues();
		filterValues.put(DBHelper.COLUMN_TITLE, name);
		filterValues.put(DBHelper.COLUMN_FILTER, serialQuery);
		
		contentResolver.insert(BibTeXContentProvider.FILTERS_URI, filterValues);
		return true;
	}

	public boolean createGroup(String group){
		Cursor c = contentResolver.query(BibTeXContentProvider.GROUPS_URI, null, DBHelper.COLUMN_GROUP + " = ?", new String[]{group}, null);
		int count = c.getCount();
		c.close();
		if(count > 0)
			return false;
		
		ContentValues grpvalues = new ContentValues();
		grpvalues.put(DBHelper.COLUMN_GROUP, group);
		grpvalues.putNull(DBHelper.COLUMN_REFKEY);
		contentResolver.insert(BibTeXContentProvider.GROUPS_URI, grpvalues);
		return true;
	}
	
	public boolean createCollection(String collection){
		Cursor c = contentResolver.query(BibTeXContentProvider.COLLECTIONS_URI, null, DBHelper.COLUMN_COLLECTION + " = ?", new String[]{collection}, null);
		int count = c.getCount();
		c.close();
		if(count > 0)
			return false;
		
		ContentValues cvalues = new ContentValues();
		cvalues.put(DBHelper.COLUMN_COLLECTION, collection);
		cvalues.putNull(DBHelper.COLUMN_REFKEY);
		contentResolver.insert(BibTeXContentProvider.COLLECTIONS_URI, cvalues);
		return true;
	}
	
	public boolean createKeyword(String keyword){
		Cursor c = contentResolver.query(BibTeXContentProvider.KEYWORDS_URI, null, DBHelper.COLUMN_KEYWORD + " = ?", new String[]{keyword}, null);
		int count = c.getCount();
		c.close();
		if(count > 0)
			return false;
		
		ContentValues kwvalues = new ContentValues();
		kwvalues.put(DBHelper.COLUMN_KEYWORD, keyword);
		kwvalues.putNull(DBHelper.COLUMN_REFKEY);
		contentResolver.insert(BibTeXContentProvider.KEYWORDS_URI, kwvalues);
		return true;
	}

	public boolean hasEntry(String refKey) {
		Cursor cursor = contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{DBHelper.COLUMN_KEY}, 
				String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey}, null);
		
		boolean result = cursor.getCount() > 0;
		cursor.close();
		return result;
	}
	
	public BibTeXEntry getBibTeXEntry(String refKey) {
		Cursor cursor = contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF}, 
				String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey}, null);
		if(cursor.getCount() == 0){
			cursor.close();
			return null;
		}
		
		cursor.moveToFirst();
		
		BibTeXEntry entry = cursorToBibTeXEntry(cursor);
		cursor.close();
		
		return entry;
	}
	
	public BibTeXEntryModel getEntry(String refKey, BibTeXEntryModelFactory modelFactory) {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF};
		Cursor cursor = contentResolver.query(BibTeXContentProvider.ENTRY_URI, projection, 
				String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey}, null);
		
		if(cursor.getCount() == 0){
			cursor.close();
			return null;
		}
		
		cursor.moveToFirst();
		
		BibTeXEntryModel entry = cursorToEntry(cursor, modelFactory);
		cursor.close();
		
		return entry;
	}
	

	public List<BibTeXEntryModel> getEntries(Collection<String> allKeys, BibTeXEntryModelFactory modelFactory) {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF};
		
		String query = String.format("%s = ?", DBHelper.COLUMN_KEY);
		for(int i=1; i < allKeys.size(); i++)
			query += String.format(" OR %s = ?", DBHelper.COLUMN_KEY);
		Cursor cursor = contentResolver.query(BibTeXContentProvider.ENTRY_URI, projection, 
				query, allKeys.toArray(new String[]{}), null);
		
		List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>(cursor.getCount());
		while(cursor.moveToNext()){
			BibTeXEntryModel entry = cursorToEntry(cursor, modelFactory);
			entries.add(entry);
		}
		cursor.close();
		
		return entries;
	}
	
	public Loader<Cursor> loadEntry(Context mContext, String refKey) {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF};
		return new CursorLoader(mContext, BibTeXContentProvider.ENTRY_URI, projection, String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey}, null);
	}

	public BibTeXEntryModel cursorToEntry(Cursor cursor, BibTeXEntryModelFactory modelFactory) {
		String bte = "\n" + cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_ENTRY) );
		
		try {
			BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
			BibTeXEntry entry = helper.parseBibTeXEntries(new StringReader(bte)).get(0);
			return modelFactory.construct( entry );
		} catch (Exception e) {
			logger.zomg("Entry caused runtime exception: %s\n\nEntry:\n%s", e.getMessage(), bte);
			throw new RuntimeException(e);
		}
	}
	
	public BibTeXEntry cursorToBibTeXEntry(Cursor cursor) {
		String bte = "\n" + cursor.getString( cursor.getColumnIndex(DBHelper.COLUMN_ENTRY) );
		
		try {
			BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
			return helper.parseBibTeXEntries(new StringReader(bte)).get(0);
		} catch (Exception e) {
			logger.zomg("Entry caused runtime exception: %s\n\nEntry:\n%s", e.getMessage(), bte);
			throw new RuntimeException(e);
		}
	}

	public boolean isRead(Cursor c){
		return c.getInt( c.getColumnIndex( DBHelper.COLUMN_READ ) ) == 1;
	}
	
	public boolean isImportant(Cursor c){
		return c.getInt( c.getColumnIndex( DBHelper.COLUMN_IMPORTANT ) ) == 1;
	}
	
	public boolean hasAttachments(Cursor c){
		return c.getInt( c.getColumnIndex( DBHelper.COLUMN_HASATTACHMENTS ) ) == 1;
	}
	

	public EratosUri getAttachment(Cursor c) {
		return EratosUri.parseUri(c.getString( c.getColumnIndex( DBHelper.COLUMN_ATTACHMENT ) ));
	}
	
	public String getYear(Cursor c){
		return c.getString( c.getColumnIndex( DBHelper.COLUMN_YEAR ) );
	}
	
	public String getType(Cursor c) {
		return c.getString( c.getColumnIndex( DBHelper.COLUMN_TYPE ) );
	}

	private String createLike(String query, String delim){
		if(query == null)
			return "%";
		return "%" + delim + query + delim + "%";
	}
	
	public CursorLoader filter(Context context, QueryElement query, int sortMode, int sortDirection) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_TYPE,
				DBHelper.COLUMN_TITLE, DBHelper.COLUMN_AUTHOR,
				DBHelper.COLUMN_YEAR, DBHelper.COLUMN_COLLECTION, DBHelper.COLUMN_SOURCE,
				DBHelper.COLUMN_DATE_ADDED, DBHelper.COLUMN_DATE_MODIFIED,
				DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_READ,
				DBHelper.COLUMN_IMPORTANT, DBHelper.COLUMN_HASATTACHMENTS,
				DBHelper.COLUMN_ATTACHMENT, DBHelper.COLUMN_FIELD, 
				DBHelper.COLUMN_VALUE, DBHelper.COLUMN_XREF };
		String sortOrder = null;
		String sortDir = null;

		switch(sortDirection){
		case SORT_ASCENDING:
			sortDir = "ASC";
			break;
		case SORT_DESCENDING:
			sortDir = "DESC";
			break;
		default:
			break;
		}
		
		String whereQuery = query.sql();
		List<String> whereArgs = new ArrayList<String>();
		whereArgs.addAll(query.args());

		if((sortMode & CUSTOM_SORT_BIT) == CUSTOM_SORT_BIT){
			String sortField = getCustomSortField(sortMode - CUSTOM_SORT_BIT);
			sortOrder = String.format("%s COLLATE NOCASE %s", DBHelper.COLUMN_VALUE, sortDir);
			
			whereQuery += String.format(" AND %s = ?", DBHelper.COLUMN_FIELD);
			whereArgs.add(sortField);
		}else{
			sortOrder = getSortStmt(sortMode, sortDir);
		}

		return new CursorLoader(context, BibTeXContentProvider.ENTRY_URI, projection, 
						whereQuery,
			    		whereArgs.toArray(new String[]{}), sortOrder);

	}
	
	private String getCustomSortField(int sortMode) {
		return EratosApplication.getApplication().getSettingsManager().getSortField(sortMode);
	}

	private String getSortStmt(int sortMode, String sortDir) {
		String sortOrder = null;
		switch(sortMode) {
		case SORT_RANDOM:
			sortOrder = "random()";
			break;
		case SORT_COLLECTION:
			sortOrder = String.format("%s COLLATE NOCASE %s, %s COLLATE NOCASE %s", DBHelper.COLUMN_COLLECTION, sortDir, DBHelper.COLUMN_TITLE, sortDir);
			break;
		case SORT_AUTHOR:
			sortOrder = String.format("%s COLLATE NOCASE %s", DBHelper.COLUMN_AUTHOR, sortDir);
			break;
		case SORT_TITLE:
			sortOrder = String.format("%s COLLATE NOCASE %s", DBHelper.COLUMN_TITLE, sortDir);
			break;
		case SORT_YEAR:
			sortOrder = String.format("%s %s, %s COLLATE NOCASE %s", DBHelper.COLUMN_YEAR, sortDir, DBHelper.COLUMN_TITLE, sortDir);
			break;
		case SORT_DATE_ADDED:
			sortOrder = String.format("%s %s, %s COLLATE NOCASE %s", DBHelper.COLUMN_DATE_ADDED, sortDir, DBHelper.COLUMN_TITLE, sortDir);
			break;
		case SORT_DATE_MODIFIED:
			sortOrder = String.format("%s %s, %s COLLATE NOCASE %s", DBHelper.COLUMN_DATE_MODIFIED, sortDir, DBHelper.COLUMN_TITLE, sortDir);
			break;
		case SORT_SOURCE:
			sortOrder = String.format("%s COLLATE NOCASE %s, %s COLLATE NOCASE %s", DBHelper.COLUMN_SOURCE, sortDir, DBHelper.COLUMN_TITLE, sortDir);
			break;
		default:
			break;
		}
		return sortOrder;
	}
	
	public CursorLoader loadAllEntries(Context context) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_TYPE, 
							DBHelper.COLUMN_TITLE, DBHelper.COLUMN_AUTHOR, 
							DBHelper.COLUMN_YEAR, DBHelper.COLUMN_COLLECTION, DBHelper.COLUMN_SOURCE,
							DBHelper.COLUMN_DATE_ADDED, DBHelper.COLUMN_DATE_MODIFIED,
							DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_READ, 
							DBHelper.COLUMN_IMPORTANT, DBHelper.COLUMN_HASATTACHMENTS,
							DBHelper.COLUMN_ATTACHMENT, DBHelper.COLUMN_FIELD, 
							DBHelper.COLUMN_VALUE, DBHelper.COLUMN_XREF };
	    CursorLoader cursorLoader = 
			new CursorLoader(context, BibTeXContentProvider.ENTRY_URI, 
							projection, null, null, null);
	    return cursorLoader;
	}

	public Loader<Cursor> loadImportantEntries(Context context) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_TYPE, 
								DBHelper.COLUMN_TITLE, DBHelper.COLUMN_AUTHOR, 
								DBHelper.COLUMN_YEAR, DBHelper.COLUMN_COLLECTION, DBHelper.COLUMN_SOURCE,
								DBHelper.COLUMN_DATE_ADDED, DBHelper.COLUMN_DATE_MODIFIED,
								DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_READ, 
								DBHelper.COLUMN_IMPORTANT, DBHelper.COLUMN_HASATTACHMENTS,
								DBHelper.COLUMN_ATTACHMENT, DBHelper.COLUMN_FIELD, 
								DBHelper.COLUMN_VALUE, DBHelper.COLUMN_XREF };
	    CursorLoader cursorLoader = 
			new CursorLoader(context, BibTeXContentProvider.ENTRY_URI, 
							projection, DBHelper.COLUMN_IMPORTANT + " = 1", null, null);
	    return cursorLoader;
	}

	public Loader<Cursor> loadUnreadEntries(Context context) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_TYPE, 
								DBHelper.COLUMN_TITLE, DBHelper.COLUMN_AUTHOR, 
								DBHelper.COLUMN_YEAR, DBHelper.COLUMN_COLLECTION, DBHelper.COLUMN_SOURCE,
								DBHelper.COLUMN_DATE_ADDED, DBHelper.COLUMN_DATE_MODIFIED,
								DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_READ, 
								DBHelper.COLUMN_IMPORTANT, DBHelper.COLUMN_HASATTACHMENTS,
								DBHelper.COLUMN_ATTACHMENT, DBHelper.COLUMN_FIELD, 
								DBHelper.COLUMN_VALUE, DBHelper.COLUMN_XREF };
	    CursorLoader cursorLoader = 
			new CursorLoader(context, BibTeXContentProvider.ENTRY_URI, 
							projection, DBHelper.COLUMN_READ + " = 0", null, null);
	    return cursorLoader;
	}
	
	public QueryElement getFilterByName(String filterName) throws IOException {
		Cursor c = contentResolver.query(BibTeXContentProvider.FILTERS_URI, new String[]{DBHelper.COLUMN_TITLE, DBHelper.COLUMN_FILTER}, 
				String.format("%s = ?", DBHelper.COLUMN_TITLE), new String[]{filterName}, null);
		try{
			if(c.getCount() == 0)
				throw new IOException("No such filter: " + filterName);
			c.moveToFirst();
			
			return cursorToQuery(c);
		}finally{
			c.close();
		}
	
	}

	public QueryElement cursorToQuery(Cursor c) throws IOException {
		try{
			byte[] source = Base64.decode(c.getString( c.getColumnIndex(DBHelper.COLUMN_FILTER) ));
			return (QueryElement) new ObjectInputStream(new ByteArrayInputStream( source )).readObject();
		} catch(Base64DecoderException e){
			throw new IOException(e);
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		} 
	}
	
	public Loader<Cursor> loadFilters(Context context) {
		return new CursorLoader(context, BibTeXContentProvider.FILTERS_URI, new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_TITLE, DBHelper.COLUMN_FILTER}, null, null, DBHelper.COLUMN_TITLE + " COLLATE NOCASE ASC");
	}
	
	public Loader<Cursor> loadGroups(Context context) {
		return new CursorLoader(context, BibTeXContentProvider.GROUPS_URI, new String[]{}, null, null, DBHelper.COLUMN_GROUP + " COLLATE NOCASE ASC");
	}

	public Loader<Cursor> loadCollections(Context context) {
		return new CursorLoader(context, BibTeXContentProvider.COLLECTIONS_URI, new String[]{}, null, null, DBHelper.COLUMN_COLLECTION + " COLLATE NOCASE ASC");
	}
	
	public Loader<Cursor> loadKeywords(Context context) {
		return new CursorLoader(context, BibTeXContentProvider.KEYWORDS_URI, new String[]{}, null, null, DBHelper.COLUMN_KEYWORD + " COLLATE NOCASE ASC");
	}
	
	public Loader<Cursor> loadStrings(Context context) {
		String[] projection = new String[]{DBHelper.COLUMN_KEY, DBHelper.COLUMN_VALUE, DBHelper.COLUMN_ENTRY};
		return new CursorLoader(context, BibTeXContentProvider.STRINGS_URI, projection , null, null, null);
	}
	
	public Cursor getEntriesForGroup(String group) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF };
		String filterGroup = createLike(group, "|");
		String whereQuery = String.format("%s LIKE ?", DBHelper.COLUMN_GROUPS);
		String []whereArgs = {filterGroup};
		
		return contentResolver.query(BibTeXContentProvider.ENTRY_URI, projection, whereQuery, whereArgs, null);
	}
	
	public Cursor getEntriesForKeyword(String keyword) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF };
		String filterKeyword = createLike(keyword, "|");
		String whereQuery = String.format("%s LIKE ?", DBHelper.COLUMN_KEYWORDS);
		String []whereArgs = { filterKeyword };
		
		return contentResolver.query(BibTeXContentProvider.ENTRY_URI, projection, whereQuery, whereArgs, null);
	}
	
	public Cursor getEntriesForCollection(String collection) {
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_XREF };
		String whereQuery = String.format("%s = ?", DBHelper.COLUMN_COLLECTION);
		String []whereArgs = { collection };
		
		return contentResolver.query(BibTeXContentProvider.ENTRY_URI, projection, whereQuery, whereArgs, null);
	}
	
	public BibTeXEntryModel getEntryByDocumentId(String idType, String id, BibTeXEntryModelFactory modelFactory) {
		if(!DBHelper.validIdTypes.contains(idType))
			throw new AssertionError("Invalid document identifier: " + idType);
		
		String[] projection = { DBHelper.COLUMN_KEY, DBHelper.COLUMN_REFKEY };
		String whereQuery = String.format("%s = ? AND %s = ?", DBHelper.COLUMN_DOC_ID_TYPE, DBHelper.COLUMN_DOC_ID);
		String []whereArgs = { idType, id };
		
		Cursor c = contentResolver.query(BibTeXContentProvider.DOCUMENT_IDS_URI, projection, whereQuery, whereArgs, null);
		
		if(c.getCount() == 0){
			c.close();
			return null;
		}
		
		c.moveToFirst();
		String refKey = c.getString( c.getColumnIndex( DBHelper.COLUMN_REFKEY ) );
		c.close();
		
		return getEntry(refKey, modelFactory);
	}
	
	public Cursor getAllEntries() {
		return contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{ DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_HASATTACHMENTS, DBHelper.COLUMN_XREF }, null, null, null);
	}
	
	public Cursor getAllKeys() {
		return contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{ DBHelper.COLUMN_KEY }, null, null, null);
	}
	
	public Cursor getImportantEntries() {
		return contentResolver.query(BibTeXContentProvider.ENTRY_URI, new String[]{ DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY, DBHelper.COLUMN_HASATTACHMENTS, DBHelper.COLUMN_XREF },
				DBHelper.COLUMN_IMPORTANT + "=1", null, null);
	}
	
	public BibTeXStringModel cursorToString(Cursor c){
		String bibtex = c.getString( c.getColumnIndex( DBHelper.COLUMN_ENTRY ) );
		
		try {
			BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
			BibTeXString st = helper.parseBibTeXString( new StringReader( bibtex ) );
			return new BibTeXStringModel(st, bibtex);
		} catch (Exception e) {
			logger.zomg("Entry caused runtime exception: %s\n\nEntry:\n%s", e.getMessage(), bibtex);
			throw new RuntimeException(e);
		}
	}
	
	public BibTeXStringModel getString(String key) {
		String[] projection = new String[]{ DBHelper.COLUMN_ENTRY };
		Cursor c = contentResolver.query(BibTeXContentProvider.STRINGS_URI, projection, DBHelper.COLUMN_KEY + " = ?", new String[]{key}, null);
		
		if(c.getCount() == 0){
			c.close();
			return null;
		}
		
		c.moveToFirst();
		BibTeXStringModel string = cursorToString(c);
		c.close();
		
		return string;
	}
	
	public Cursor getStrings(){
		return contentResolver.query(BibTeXContentProvider.STRINGS_URI, new String[]{ DBHelper.COLUMN_KEY, DBHelper.COLUMN_VALUE, DBHelper.COLUMN_ENTRY }, null, null, null);
	}
	
	public Cursor getObjects(){
		return contentResolver.query(BibTeXContentProvider.OBJECTS_URI, new String[]{ DBHelper.COLUMN_KEY, DBHelper.COLUMN_ENTRY }, null, null, null);
	}
	
	public Cursor getKeywords() {
		return contentResolver.query(BibTeXContentProvider.KEYWORDS_URI, new String[]{ DBHelper.COLUMN_KEY, DBHelper.COLUMN_KEYWORD }, null, null, null);
	}

	public List<BibTeXObject> getAllObjects() {
		Cursor c = getObjects();
		BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		List<BibTeXObject> objects = new ArrayList<BibTeXObject>();
		
		while(c.moveToNext()){
			objects.add( cursorToObject(c, helper) ); 
		}
		
		c.close();
		return objects;
	}

	public BibTeXObject cursorToObject(Cursor c, BibTeXHelper helper) {
		String entry = c.getString(c.getColumnIndex(DBHelper.COLUMN_ENTRY));
		try {
			return helper.parseBibTeXObject(new StringReader(entry));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public int delete(String refKey) {
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.ENTRY_URI, String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.GROUPS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.COLLECTIONS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.KEYWORDS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.DOCUMENT_IDS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		contentResolver.delete(BibTeXContentProvider.SORT_FIELDS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		return rowsDeleted;
	}
	
	public int deleteAll(Collection<String> refKeys) {
		deleteData(refKeys);
		return deleteEntries(refKeys);
	}
	
	private int deleteEntries(Collection<String> refKeys) {
		String q1 = String.format("%s = ?", DBHelper.COLUMN_KEY);
		for(int i=0; i < refKeys.size(); i++)
			q1 += String.format(" OR %s = ?", DBHelper.COLUMN_KEY);
		String args[] = refKeys.toArray(new String[]{});
		
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.ENTRY_URI, q1, args);
		return rowsDeleted;
	}

	private void deleteData(Collection<String> refKeys) {
		String q2 = String.format("%s = ?", DBHelper.COLUMN_REFKEY);
		for(int i=0; i < refKeys.size(); i++)
			q2 += String.format(" OR %s = ?", DBHelper.COLUMN_REFKEY);
		String args[] = refKeys.toArray(new String[]{});
		
		contentResolver.delete(BibTeXContentProvider.GROUPS_URI, q2, args);
		contentResolver.delete(BibTeXContentProvider.COLLECTIONS_URI, q2, args);
		contentResolver.delete(BibTeXContentProvider.KEYWORDS_URI, q2, args);
		contentResolver.delete(BibTeXContentProvider.DOCUMENT_IDS_URI, q2, args);
		contentResolver.delete(BibTeXContentProvider.SORT_FIELDS_URI, q2, args);
	}

	public int deleteKeyword(String keyword){
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.KEYWORDS_URI, String.format("%s = ?", DBHelper.COLUMN_KEYWORD), new String[]{keyword});
		return rowsDeleted;
	}
	
	public int deleteGroup(String group){
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.GROUPS_URI, String.format("%s = ?", DBHelper.COLUMN_GROUP), new String[]{group});
		return rowsDeleted;
	}
	
	public int deleteCollection(String collection){
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.COLLECTIONS_URI, String.format("%s = ?", DBHelper.COLUMN_COLLECTION), new String[]{collection});
		return rowsDeleted;
	}
	

	public int deleteStrings() {
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.STRINGS_URI, null, null);
		return rowsDeleted;
	}

	public int deleteSortFields() {
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.SORT_FIELDS_URI, null, null);
		return rowsDeleted;
	}
	
	public int deleteAll() {
		int rowsDeleted = contentResolver.delete(BibTeXContentProvider.ENTRY_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.GROUPS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.COLLECTIONS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.KEYWORDS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.DOCUMENT_IDS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.STRINGS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.OBJECTS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.SORT_FIELDS_URI, null, null);
		contentResolver.delete(BibTeXContentProvider.FILTERS_URI, null, null);
		return rowsDeleted;
	}
	
	public void setCollection(String refKey, String collection) {
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_KEY, refKey);
		values.put(DBHelper.COLUMN_COLLECTION, collection);
		contentResolver.update(BibTeXContentProvider.ENTRY_URI, values, String.format("%s = ?", DBHelper.COLUMN_KEY), new String[]{refKey});
		
		contentResolver.delete(BibTeXContentProvider.COLLECTIONS_URI, String.format("%s = ?", DBHelper.COLUMN_REFKEY), new String[]{refKey});
		if(!collection.isEmpty()){
			ContentValues cvalues = new ContentValues();
			cvalues.put(DBHelper.COLUMN_COLLECTION, collection);
			cvalues.put(DBHelper.COLUMN_REFKEY, refKey);
			contentResolver.insert(BibTeXContentProvider.COLLECTIONS_URI, cvalues);
		}
	}

	public void bulkInsert(Uri uri, List<ContentValues> values) {
		contentResolver.bulkInsert(uri, values.toArray(new ContentValues[]{}));
	}
}
