package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;

public class RemoveUrlTransaction extends BibTeXEntryTransaction {
	private final EratosUri uri;

	public RemoveUrlTransaction(Activity mContext, BibTeXContentProviderAdapter adapter, EratosUri uri){
		super(mContext, adapter);
		this.uri = uri;
	}
	
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.removeExternalLink(uri);
		return false;
	}
}
