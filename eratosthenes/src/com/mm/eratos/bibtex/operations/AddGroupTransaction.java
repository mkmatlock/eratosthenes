package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class AddGroupTransaction extends BibTeXEntryTransaction {

	private final String group;

	public AddGroupTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String group) {
		super(mContext, contentAdapter);
		this.group = group;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.addGroup(group);
		return false;
	}

}
