package com.mm.eratos.bibtex.operations;

import java.io.File;

import android.app.Activity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;

public class RemoveFileTransaction extends BibTeXEntryTransaction {

	private final int filePos;
	private final FileRevisionContentProviderAdapter fileAdapter;

	public RemoveFileTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, int filePos, FileRevisionContentProviderAdapter fileAdapter) {
		super(mContext, contentAdapter);
		this.filePos = filePos;
		this.fileAdapter = fileAdapter;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		EratosApplication app = EratosApplication.getApplication();
		
		EratosUri uri = entry.getFiles().get(filePos).resolveUri(app);
		entry.removeFile(filePos);

		File f = FileManager.getFileManager().getFile(uri);
		if (f.exists())
			f.delete();
		
		if(uri.isRemoteProtocol(app)){
			if(fileAdapter.exists(uri))
				fileAdapter.update( fileAdapter.getFile(uri).delete() );
			else
				fileAdapter.update( fileAdapter.newSyncedFile(uri, "", f, entry.important()).delete() );
		}
		
		return false;
	}

}
