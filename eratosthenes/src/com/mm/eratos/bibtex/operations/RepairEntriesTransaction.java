package com.mm.eratos.bibtex.operations;

import java.io.IOException;

import android.app.Activity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;

public class RepairEntriesTransaction extends BibTeXEntryTransaction {
	
	private boolean moveFiles;
	private FileManager fileManager;
	private FileRevisionContentProviderAdapter fileAdapter;

	public RepairEntriesTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, boolean moveFiles) {
		super(mContext, contentAdapter);
		this.moveFiles = moveFiles;
		fileManager = FileManager.getFileManager();
		fileAdapter = new FileRevisionContentProviderAdapter(mContext.getContentResolver());
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		if(moveFiles)
			moveFiles(entry);
		return false;
	}

	private void moveFiles(BibTeXEntryModel entry) throws IOException {
		int fpos = 0;
		for(EratosUri uri : entry.getFiles()){
			String name = uri.name();
			EratosUri expUri = fileManager.getDefaultAttachmentUri(name);
			
			if(!uri.equals(expUri))
				moveFile(entry, fpos, uri, expUri);
			
			fpos++;
		}
	}

	private void moveFile(BibTeXEntryModel entry, int fpos, EratosUri srcUri, EratosUri dstUri) throws IOException {
		EratosApplication app = EratosApplication.getApplication();
		fileManager.moveFile(fileAdapter, srcUri.resolveUri(app), dstUri.resolveUri(app), entry.important());
		entry.replaceFile(fpos, dstUri);
	}
}
