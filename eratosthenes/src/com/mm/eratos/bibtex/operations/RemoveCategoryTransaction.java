package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.fragment.EntryGroupFragment;

public class RemoveCategoryTransaction extends BibTeXEntryTransaction {
	private String categoryName;
	private String category;

	public RemoveCategoryTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String categoryName, String category) {
		super(mContext, contentAdapter);
		this.categoryName = categoryName;
		this.category = category;
	}

	
	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		if(EntryGroupFragment.CATEGORY_DATABASE.equals(categoryName)){
			if(EntryGroupFragment.DATABASE_LIBRARY.equals(category)){
				return true;
			}else if(EntryGroupFragment.DATABASE_IMPORTANT.equals(category)){
				entry.setImportant(false);
			}else if(EntryGroupFragment.DATABASE_UNREAD.equals(category)){
				entry.setRead(true);
			}
		}else if(EntryGroupFragment.CATEGORY_GROUPS.equals(categoryName)){
			entry.removeGroup(category);
		}else if(EntryGroupFragment.CATEGORY_COLLECTIONS.equals(categoryName)){
			entry.setCollection("");
		}else if(EntryGroupFragment.CATEGORY_KEYWORDS.equals(categoryName)){
			entry.removeKeyword(category);
		}
		
		return false;
	}
}
