package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class RenameKeywordTransaction extends BibTeXEntryTransaction {

	private String original;
	private String rename;

	public RenameKeywordTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String original, String rename) {
		super(mContext, contentAdapter);
		this.original = original;
		this.rename = rename;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.removeKeyword(original);
		entry.addKeyword(rename);
		return false;
	}

}
