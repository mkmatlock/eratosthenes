package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class DeleteEntryTransaction extends BibTeXEntryTransaction {

	public DeleteEntryTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter) {
		super(mContext, contentAdapter);
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		return true;
	}

}
