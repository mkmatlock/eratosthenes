package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class RenameGroupTransaction extends BibTeXEntryTransaction {

	private String original;
	private String rename;

	public RenameGroupTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String original, String rename) {
		super(mContext, contentAdapter);
		this.original = original;
		this.rename = rename;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.removeGroup(original);
		entry.addGroup(rename);
		return false;
	}

}
