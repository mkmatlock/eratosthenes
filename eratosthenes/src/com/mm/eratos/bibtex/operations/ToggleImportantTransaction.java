package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.SyncedFile;

public class ToggleImportantTransaction extends BibTeXEntryTransaction {

	private final FileRevisionContentProviderAdapter fileAdapter;

	public ToggleImportantTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, FileRevisionContentProviderAdapter fileAdapter) {
		super(mContext, contentAdapter);
		this.fileAdapter = fileAdapter;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.toggleImportant();
		for(EratosUri uri : entry.getFiles()){
			if(fileAdapter.exists(uri)){
				SyncedFile sf = fileAdapter.getFile(uri);
				SyncedFile nsf = sf.setImportant(entry.important());
				fileAdapter.update(nsf);
			}
		}
		return false;
	}

}
