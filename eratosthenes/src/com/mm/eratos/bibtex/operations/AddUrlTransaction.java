package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.model.EratosUri;

public class AddUrlTransaction extends BibTeXEntryTransaction {
	private final EratosUri uri;

	public AddUrlTransaction(Activity mContext, BibTeXContentProviderAdapter adapter, EratosUri uri){
		super(mContext, adapter);
		this.uri = uri;
	}
	
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.addExternalLink(uri);
		return false;
	}
}
