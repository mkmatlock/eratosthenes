package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class AssignCollectionTransaction extends BibTeXEntryTransaction {

	private final String collection;

	public AssignCollectionTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String collection) {
		super(mContext, contentAdapter);
		this.collection = collection;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.setCollection(collection);
		return false;
	}

}
