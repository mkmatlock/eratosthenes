package com.mm.eratos.bibtex.operations;

import org.jbibtex.Key;
import org.jbibtex.Value;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class MoveFieldTransaction extends BibTeXEntryTransaction {

	private Key oKey;
	private Key nKey;
	private Value nValue;

	public MoveFieldTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, Key oKey, Key nKey, Value nValue) {
		super(mContext, contentAdapter);
		this.oKey = oKey;
		this.nKey = nKey;
		this.nValue = nValue;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.removeField(oKey);
		entry.setField(nKey, nValue);
		return false;
	}
}
