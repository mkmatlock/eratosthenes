package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class RemoveFieldTransaction extends BibTeXEntryTransaction {

	private final String field;

	public RemoveFieldTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String field) {
		super(mContext, contentAdapter);
		this.field = field;
		
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.removeField(field);
		return false;
	}

}
