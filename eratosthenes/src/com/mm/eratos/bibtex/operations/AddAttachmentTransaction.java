package com.mm.eratos.bibtex.operations;

import java.io.File;

import android.app.Activity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.SyncedFile;

public class AddAttachmentTransaction extends BibTeXEntryTransaction {

	private final EratosUri fileUri;
	private FileRevisionContentProviderAdapter fileAdapter;

	public AddAttachmentTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, FileRevisionContentProviderAdapter fileAdapter, EratosUri fileUri) {
		super(mContext, contentAdapter);
		this.fileAdapter = fileAdapter;
		this.fileUri = fileUri;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		File file = FileManager.getFileManager().getFile(fileUri);
		
		EratosUri relUri = FileManager.getFileManager().resolveIfLocal(fileUri);
		EratosApplication app = EratosApplication.getApplication();
		
		
		if(fileUri.isRemoteProtocol(app)){
			if(fileAdapter.exists(fileUri)){
				SyncedFile sf = fileAdapter.getFile(fileUri);
				sf = sf.setLastModified(file.lastModified()).setCached(true).setImportant(entry.important());
				fileAdapter.update(sf);
			}else
				fileAdapter.newSyncedFile(fileUri, "", file, entry.important());
		}
		entry.attachFile(relUri);
		
		return false;
	}

}
