package com.mm.eratos.bibtex.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.database.Cursor;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXContentProviderReferenceResolver;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.utils.WebClient.DownloadListener;

public abstract class BibTeXEntryTransaction {
	private BibTeXHelper helper;
	protected final Activity mContext;
	protected final BibTeXContentProviderAdapter contentAdapter;
	
	private final ProgressCallback doNothingListener = new ProgressCallback() {
		public void update(String stage, int progress, int maxProgress) {
		}
		public void finished() {
		}
		public void error(Throwable error) {
		}
	};
	private BibTeXEntryModelFactory modelFactory;
	protected DownloadListener downloadListener;

	protected abstract boolean performOperation(BibTeXEntryModel entry) throws Exception;
	
	public BibTeXEntryTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter){
		this.mContext = mContext;
		this.contentAdapter = contentAdapter;
		this.helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
		this.modelFactory = new BibTeXEntryModelFactory(new BibTeXContentProviderReferenceResolver(contentAdapter), helper);
		this.downloadListener = new DownloadListener() {
			public void progress(long downloaded, long fileSize) {
			}
		};
	}
	
	public void registerDownloadListener(DownloadListener listener){
		this.downloadListener = listener;
	}
	

	public void unregisterDownloadListener() {
		this.downloadListener = new DownloadListener() {
			public void progress(long downloaded, long fileSize) {
			}
		};
	}
	
	public void execute(BibTeXEntryModel entry) throws Exception {
		execute( Arrays.asList(new BibTeXEntryModel[]{entry}) );
	}
	
	public void execute(List<BibTeXEntryModel> entries) throws Exception {
		execute( entries, doNothingListener );
	}
	
	public void execute(List<BibTeXEntryModel> entries, ProgressCallback listener) throws Exception {
		List<BibTeXEntryModel> copies = copyAll(entries);
		List<BibTeXEntryModel> updated = new ArrayList<BibTeXEntryModel>(entries.size());
		List<BibTeXEntryModel> deleted = new ArrayList<BibTeXEntryModel>();
		
		int i = 0;
		for(BibTeXEntryModel copy : copies){
			boolean delete = performOperation(copy);
			if(delete){
				deleted.add(copy);
			}else{
				BibTeXEntryModel update = modelFactory.commit(copy);
				updated.add(update);
			}
			i++;
			listener.update("Executing task...", i, entries.size());
		}
		
		i=0;
		for(BibTeXEntryModel update : updated){
			contentAdapter.update(update);
			i++;
			listener.update("Updating entries...", i, updated.size());
		}
		
		i=0;
		for(BibTeXEntryModel delete : deleted){
			contentAdapter.delete(delete.getRefKey());
			i++;
			listener.update("Deleting entries...", i, deleted.size());
		}
		
		listener.finished();
		SaveDatabaseTask.autosaveDatabase(mContext, new FileSyncResultReceiver(mContext));
	}

	public void execute(Cursor c) throws Exception {
		List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>();
		
		while(c.moveToNext()){
			BibTeXEntryModel model = contentAdapter.cursorToEntry(c, modelFactory);
			entries.add(model);
		}
		c.close();
		
		execute(entries);
	}
	
	public void executeOnKeys(List<String> refKeys) throws Exception {
		executeOnKeys(refKeys, doNothingListener);
	}
	
	public void executeOnKeys(List<String> refKeys, ProgressCallback listener) throws Exception {
		List<BibTeXEntryModel> entries = new ArrayList<BibTeXEntryModel>();
		int i = 0;
		for(String refKey : refKeys){
			entries.add( contentAdapter.getEntry(refKey, modelFactory) );
			i++;
			listener.update("Enumerating entries...", i, refKeys.size());
		}
		execute(entries, listener);
	}

	private List<BibTeXEntryModel> copyAll(List<BibTeXEntryModel> originals) {
		List<BibTeXEntryModel> copies = new ArrayList<BibTeXEntryModel>(originals.size());
		
		for(BibTeXEntryModel entry : originals){
			copies.add(entry.copy());
		}
		
		return copies;
	}
}
