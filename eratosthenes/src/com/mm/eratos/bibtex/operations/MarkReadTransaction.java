package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class MarkReadTransaction extends BibTeXEntryTransaction {

	private final boolean readState;

	public MarkReadTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, boolean readState) {
		super(mContext, contentAdapter);
		this.readState = readState;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.setRead(readState);
		return false;
	}

}
