package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;

public class RenameFileTransaction extends BibTeXEntryTransaction {

	private final int filePos;
	private FileManager fileManager;
	private EratosUri srcUri;
	private String filename;
	private FileRevisionContentProviderAdapter fileAdapter;
	
	public RenameFileTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, FileRevisionContentProviderAdapter fileAdapter, int filePos, EratosUri srcUri, String filename) {
		super(mContext, contentAdapter);
		this.filePos = filePos;
		this.srcUri = srcUri;
		this.filename = filename;
		this.fileAdapter = fileAdapter;
		this.fileManager = FileManager.getFileManager();
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		EratosUri dstUri = fileManager.renameFile(srcUri, filename, fileAdapter, entry.important());
		entry.replaceFile(filePos, dstUri);
		return false;
	}
}
