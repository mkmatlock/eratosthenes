package com.mm.eratos.bibtex.operations;

import java.io.IOException;

import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import android.app.Activity;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.bibtex.BibTeXAccentParser;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.bibtex.BibTeXHelper;

public class SetFieldTransaction extends BibTeXEntryTransaction {

	private final String field;
	private final String strValue;
	private final Value newValue;
	private final boolean rawMode;
	
	public SetFieldTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String field, String newValue) {
		super(mContext, contentAdapter);
		this.field = field;
		this.strValue = newValue;
		this.newValue = null;
		this.rawMode = false;
	}
	
	public SetFieldTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String field, Value newValue) {
		super(mContext, contentAdapter);
		this.field = field;
		this.strValue = null;
		this.newValue = newValue;
		this.rawMode = true;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		Value fVal = null;
		if(rawMode)
			fVal = addRawValue();
		else
			fVal = addStrValue();
		
		entry.setField(field, fVal);
		return false;
	}

	private Value addRawValue() {
		return newValue;
	}

	private Value addStrValue() throws IOException, ParseException {
		String fVal = strValue;
		boolean convertLaTeX = EratosApplication.getApplication().getSettingsManager().getConvertLaTeXtoUTF8();
		boolean convertUTF8 = EratosApplication.getApplication().getSettingsManager().getConvertUTF8ToLaTeX();
		
		if( convertUTF8 ){
			fVal = BibTeXAccentParser.unicodeToLatex(fVal);
			fVal = BibTeXAccentParser.newlinesToLaTeX(fVal);
		}
		if( convertLaTeX ){
			BibTeXHelper helper = EratosApplication.getApplication().getSettingsManager().getBibTeXHelper();
			fVal = helper.printLaTeX( helper.parseLaTeX(fVal) );
		}
		return new StringValue(fVal, Style.BRACED);
	}

}
