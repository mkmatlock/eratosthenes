package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.model.EratosUri;

public class EntryWebUpdateTransaction extends BibTeXEntryTransaction {
	private AbstractWebHandler handler;
	private EratosUri uri;

	public EntryWebUpdateTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, AbstractWebHandler handler, EratosUri uri) {
		super(mContext, contentAdapter);
		this.handler = handler;
		this.uri = uri;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		handler.extras(entry, uri);
		
		EratosUri pdfUri = handler.pdfUri(entry, uri);
		if(!entry.hasAttachments() && pdfUri != null)
			handler.downloadPdf(entry, pdfUri, downloadListener);
		
		return false;
	}

}
