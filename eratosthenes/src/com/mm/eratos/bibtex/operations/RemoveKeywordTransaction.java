package com.mm.eratos.bibtex.operations;

import android.app.Activity;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXEntryModel;

public class RemoveKeywordTransaction extends BibTeXEntryTransaction {
	private final String keyword;

	public RemoveKeywordTransaction(Activity mContext, BibTeXContentProviderAdapter contentAdapter, String keyword) {
		super(mContext, contentAdapter);
		this.keyword = keyword;
	}

	@Override
	protected boolean performOperation(BibTeXEntryModel entry) throws Exception {
		entry.removeKeyword(keyword);
		return false;
	}

}
