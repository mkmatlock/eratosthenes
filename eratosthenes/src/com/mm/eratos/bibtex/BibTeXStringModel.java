package com.mm.eratos.bibtex;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;

public class BibTeXStringModel {
	private final String name;
	private final String content;
	private final String bibtex;
	private final BibTeXString bibString;
	
	public BibTeXStringModel(){
		this("","","");
	}
	
	protected BibTeXStringModel(String name, String content, String bibtex) {
		this.name = name;
		this.content = content;
		this.bibtex = bibtex;
		this.bibString = new BibTeXString(new Key(name), new StringValue(content, Style.BRACED));
	}
	
	protected BibTeXStringModel(BibTeXString st, String bibtex) {
		this.name = st.getKey().getValue();
		this.content = st.getValue().toUserString();
		this.bibtex = bibtex;
		this.bibString = st;
	}

	public String getKey() {
		return name;
	}

	public String getValue() {
		return content;
	}
	
	public String getBibtex() {
		return bibtex;
	}
	
	public BibTeXString getBibTeXString() {
		return bibString;
	}
	
	public BibTeXStringModel modify(BibTeXHelper helper, String name, String content) throws IOException {
		BibTeXString st = new BibTeXString(new Key(name), new StringValue(content, Style.BRACED));
		
		String bibtex = helper.formatBibTeX(st);
		return new BibTeXStringModel(st.getKey().getValue(), st.getValue().toUserString(), bibtex);
	}
	
	public BibTeXStringModel modify(BibTeXHelper helper, String bibtex) throws IOException, ParseException {
		Reader reader = new StringReader(bibtex);
		
		BibTeXString st = helper.parseBibTeXString(reader);
		return new BibTeXStringModel(st, bibtex);
	}
}
