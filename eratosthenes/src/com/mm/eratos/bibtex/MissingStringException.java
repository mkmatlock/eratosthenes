package com.mm.eratos.bibtex;

public class MissingStringException extends Exception {

	public MissingStringException(String value) {
		super("Missing referenced string entry: " + value);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8293080399383453741L;
}
