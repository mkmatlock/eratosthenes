package com.mm.eratos.bibtex;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.StringValue;
import org.jbibtex.StringValue.Style;
import org.jbibtex.Value;

import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.files.NullOutputStream;

public class BibTeXEntryModelFactory {
	static private Map<Key, BibTeXString> macros;
	private static void addMacro(String kVal, String value) {
		Key key = new Key(kVal);
		Value sValue = new StringValue(value, Style.QUOTED);
		macros.put(key, new BibTeXString(key, sValue));
	}
    static {
    	macros = new HashMap<Key, BibTeXString>();
        addMacro("jan", "January");
        addMacro("feb", "February");
        addMacro("mar", "March");
        addMacro("apr", "April");
        addMacro("may", "May");
        addMacro("jun", "June");
        addMacro("jul", "July");
        addMacro("aug", "August");
        addMacro("sep", "September");
        addMacro("oct", "October");
        addMacro("nov", "November");
        addMacro("dec", "December");
    }
	
	public static abstract class ReferenceResolver {
		public abstract BibTeXEntry resolveEntry(Key key);
		public abstract BibTeXString resolveString(Key key);
		public BibTeXString resolveStringOrMacro(Key key){
			if(macros.containsKey(key))
				return macros.get(key);
			return resolveString(key);
		}
	}

	private ReferenceResolver resolver;
	private BibTeXHelper helper;
//	private Map<Key, BibTeXEntryModel> models;
	
	public BibTeXEntryModelFactory(ReferenceResolver resolver, BibTeXHelper helper) {
		this.resolver = resolver;
		this.helper = helper;
//		this.models = new HashMap<Key, BibTeXEntryModel>();
	}

//	public boolean hasModel(Key key) {
//		return models.containsKey(key);
//	}
//	
//	public BibTeXEntryModel getModel(Key key) {
//		return models.get(key);
//	}
	
	public BibTeXEntryModel construct(BibTeXEntry entry) throws IOException, ParseException {
		return construct(entry, new ArrayList<Key>());
	}
	
	public BibTeXEntryModel construct(BibTeXEntry entry, List<Key> parents) throws IOException, ParseException {
		Key key = entry.getKey();
//		if(models.containsKey(key))
//			return models.get(key);
		
		List<Exception> warnings = new ArrayList<Exception>();
		Map<Key, BibTeXString> strings = new HashMap<Key, BibTeXString>();
		
		Key xrefKey = getXrefKey(entry);
		parents.add(key);
		
		BibTeXEntryModel xref = getXref(xrefKey, entry, parents, warnings);
		setReferences(entry, xref, strings, warnings);
		
		return new BibTeXEntryModel(entry, helper, xref, strings, warnings);
	}

	public BibTeXEntryModel construct(String bibtex) throws IOException, ParseException {
		BibTeXDatabase db = helper.parseBibTeX(new StringReader(bibtex), new NullOutputStream());
		
		Map<Key, BibTeXEntry> entries = db.getEntries();
		if(entries.size() != 1)
			throw new ParseException("Expected 1 entry, got " + entries.size());
		
		return construct(entries.get(0));
	}

	public BibTeXEntryModel commit(BibTeXEntryModel model) throws IOException, ParseException, FieldRequiredException {
		List<Exception> warnings = BibTeXEntryVerifier.checkEntry(model.getType(), model.allFields(), model.hasXref(), helper.warningsAsErrors());
		Map<Key, BibTeXString> strings = new HashMap<Key, BibTeXString>();
		
		BibTeXEntry newEntry = model.createEntry();
		tryParse(helper, newEntry);
		
		ArrayList<Key> parents = new ArrayList<Key>();
		parents.add(newEntry.getKey());
		
		BibTeXEntryModel xref = getXref(getXrefKey(newEntry), newEntry, parents, warnings);
		setReferences(newEntry, xref, strings, warnings);
		
		return new BibTeXEntryModel(newEntry, helper, xref, strings, warnings);
	}

	private void tryParse(BibTeXHelper helper, BibTeXEntry newEntry) throws IOException, ParseException {
		String bte = helper.formatBibTeX(newEntry);
		helper.parseBibTeXEntries(new StringReader(bte));
	}
	
	private void setReferences(BibTeXEntry entry, BibTeXEntryModel xref, Map<Key, BibTeXString> strings, List<Exception> warnings) {
		for(Key k : getReferences(entry, xref)){
			BibTeXString str = resolver.resolveStringOrMacro(k);
			if(str != null){ 
				strings.put(k, str);
				getStringReferences(str, strings, warnings);
			}else warnings.add(new MissingStringException(k.getValue()));
		}
	}

	private void getStringReferences(BibTeXString str, Map<Key, BibTeXString> strings, List<Exception> warnings) {
		for(Key k : str.getValue().getReferences()){
			BibTeXString substr = resolver.resolveStringOrMacro(k);
			if(str != null){ 
				strings.put(k, substr);
				getStringReferences(substr, strings, warnings);
			}else warnings.add(new MissingStringException(k.getValue()));
		}
	}

	private BibTeXEntryModel getXref(Key xrefKey, BibTeXEntry entry, List<Key> parents, List<Exception> warnings) throws IOException, ParseException {
		if(parents.contains(xrefKey)) {
			warnings.add(new ParseException("Circular cross reference detected: '" + entry.getKey().getValue() + "' cross-references '" + xrefKey.getValue() + "'"));
			return null;
		}
		
		BibTeXEntryModel xref = null;
		if(xrefKey != null) {
			BibTeXEntry xrefEntry = resolver.resolveEntry(xrefKey);
			
			if(xrefEntry != null) {
				parents = new ArrayList<Key>(parents);
				parents.add(entry.getKey());
				xref = construct(xrefEntry, parents);
				entry.setCrossReference(xrefKey, xrefEntry);
			} else warnings.add(new MissingXrefException(xrefKey.getValue()));
		}
		return xref;
	}
	
	private Set<Key> getReferences(BibTeXEntry entry, BibTeXEntryModel xref) {
		Set<Key> references = new HashSet<Key>();
		
		Map<Key, Value> fields = entry.getFields();
		for(Key k : fields.keySet())
			 references.addAll(fields.get(k).getReferences());
		
		if(xref != null)
			references.addAll(getReferences(xref.getEntry(), xref.getXref()));
		return references;
	}

	private Key getXrefKey(BibTeXEntry entry){
		Value value = entry.getField(BibTeXEntryModel.XREF_KEY);
		if(value == null)
			return null;
		return new Key(value.toUserString());
	}

}

