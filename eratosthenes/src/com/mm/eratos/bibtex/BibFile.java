package com.mm.eratos.bibtex;

import com.mm.eratos.model.EratosUri;

public class BibFile {
	EratosUri uri;
	String type;
	int entryCount;
	private String encoding;
	
	public BibFile(EratosUri uri, String type, String encoding, int entryCount) {
		this.uri = uri;
		this.type = type;
		this.encoding = encoding;
		this.entryCount = entryCount;
	}
	
	public EratosUri uri(){
		return uri;
	}
	
	public String type(){
		return type;
	}
	
	public int getEntryCount(){
		return entryCount;
	}
	
	@Override
	public String toString(){
		return "Bibfile Uri: " + uri.toString() + " #Entries: " + entryCount;
	}

	public String encoding() {
		return encoding;
	}
}
