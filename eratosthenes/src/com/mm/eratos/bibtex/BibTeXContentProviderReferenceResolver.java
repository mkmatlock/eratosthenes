package com.mm.eratos.bibtex;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.bibtex.BibTeXEntryModelFactory.ReferenceResolver;

public class BibTeXContentProviderReferenceResolver extends ReferenceResolver {

	private BibTeXContentProviderAdapter bibtexAdapter;
	private EratosLogger logger;

	public BibTeXContentProviderReferenceResolver(BibTeXContentProviderAdapter bibtexAdapter){
		super();
		this.bibtexAdapter = bibtexAdapter;
		this.logger = EratosApplication.getApplication().getLogManager().getLogger(BibTeXContentProviderReferenceResolver.class);
	}

	@Override
	public BibTeXEntry resolveEntry(Key key) {
		BibTeXEntry entry = bibtexAdapter.getBibTeXEntry(key.getValue());
		if(entry == null)
			logger.warn("Missing Xref Key: %s", key.getValue());
		return entry;
	}

	@Override
	public BibTeXString resolveString(Key key) {
		BibTeXStringModel stringModel = bibtexAdapter.getString(key.getValue());
		if(stringModel == null){
			logger.warn("Missing String Key: %s", key.getValue());
			return null;
		}
		
		BibTeXString str = stringModel.getBibTeXString();
		if(str == null)
			logger.warn("Missing String Key: %s", key.getValue());
		
		return str;
	}
}
