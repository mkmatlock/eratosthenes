package com.mm.eratos.bibtex;

import java.util.HashMap;
import java.util.List;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXString;
import org.jbibtex.Key;

import com.mm.eratos.bibtex.BibTeXEntryModelFactory.ReferenceResolver;

public class BibTeXMappingReferenceResolver extends ReferenceResolver{
	private HashMap<Key, BibTeXString> strings;
	private HashMap<Key, BibTeXEntry> entries;

	public BibTeXMappingReferenceResolver(List<BibTeXEntry> entries, List<BibTeXString> strings){
		super();
		this.strings = new HashMap<Key, BibTeXString>();
		this.entries = new HashMap<Key, BibTeXEntry>();
		
		for(BibTeXEntry entry : entries)
			this.entries.put(entry.getKey(), entry);
		for(BibTeXString string : strings)
			this.strings.put(string.getKey(), string);
	}

	@Override
	public BibTeXEntry resolveEntry(Key key) {
		return entries.get(key);
	}

	@Override
	public BibTeXString resolveString(Key key) {
		return strings.get(key);
	}
}
