package com.mm.eratos.bibtex;

import java.util.Locale;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.mm.eratos.database.DBHelper;

public class BibTeXContentProvider extends ContentProvider {
	private DBHelper dbHelper;

	// Used for the UriMacher
	private static final int ENTRIES = 1;
	private static final int ENTRY_KEY = 2;
	
	private static final int COLLECTIONS = 3;
	private static final int GROUPS = 4;
	private static final int KEYWORDS = 5;
	
	private static final int STRINGS = 6;
	private static final int OBJECTS = 7;
	private static final int DOCUMENTS = 8;
	
	private static final int SORT_FIELDS = 9;
	
	private static final int RECENT_FILES = 10;
	private static final int FILTERS = 11;
	
	private static final String AUTHORITY = "com.mm.eratos.bibtex.contentprovider";

	private static final String ENTRY_PATH = "entries";
	private static final String COLLECTIONS_PATH = "collections";
	private static final String GROUPS_PATH = "groups";
	private static final String KEYWORDS_PATH = "keywords";
	private static final String STRINGS_PATH = "strings";
	private static final String OBJECTS_PATH = "objects";
	private static final String DOCUMENT_IDS_PATH = "docids";
	private static final String SORT_FIELDS_PATH = "fields";
	private static final String RECENT_FILES_PATH = "files";
	private static final String FILTERS_PATH = "filters";
	
	public static final Uri ENTRY_URI = Uri.parse("content://" + AUTHORITY + "/" + ENTRY_PATH);
	public static final Uri COLLECTIONS_URI = Uri.parse("content://" + AUTHORITY + "/" + COLLECTIONS_PATH);
	public static final Uri GROUPS_URI = Uri.parse("content://" + AUTHORITY + "/" + GROUPS_PATH);
	public static final Uri KEYWORDS_URI = Uri.parse("content://" + AUTHORITY + "/" + KEYWORDS_PATH);
	public static final Uri STRINGS_URI = Uri.parse("content://" + AUTHORITY + "/" + STRINGS_PATH);
	public static final Uri OBJECTS_URI = Uri.parse("content://" + AUTHORITY + "/" + OBJECTS_PATH);
	public static final Uri DOCUMENT_IDS_URI = Uri.parse("content://" + AUTHORITY + "/" + DOCUMENT_IDS_PATH);
	public static final Uri SORT_FIELDS_URI = Uri.parse("content://" + AUTHORITY + "/" + SORT_FIELDS_PATH);
	public static final Uri RECENT_FILES_URI = Uri.parse("content://" + AUTHORITY + "/" + RECENT_FILES_PATH);
	public static final Uri FILTERS_URI = Uri.parse("content://" + AUTHORITY + "/" + FILTERS_PATH); 
	
	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);


	static {
		sURIMatcher.addURI(AUTHORITY, ENTRY_PATH, ENTRIES);
		sURIMatcher.addURI(AUTHORITY, COLLECTIONS_PATH, COLLECTIONS);
		sURIMatcher.addURI(AUTHORITY, GROUPS_PATH, GROUPS);
		sURIMatcher.addURI(AUTHORITY, KEYWORDS_PATH, KEYWORDS);
		sURIMatcher.addURI(AUTHORITY, STRINGS_PATH, STRINGS);
		sURIMatcher.addURI(AUTHORITY, OBJECTS_PATH, OBJECTS);
		sURIMatcher.addURI(AUTHORITY, DOCUMENT_IDS_PATH, DOCUMENTS);
		sURIMatcher.addURI(AUTHORITY, ENTRY_PATH + "/*", ENTRY_KEY);
		sURIMatcher.addURI(AUTHORITY, SORT_FIELDS_PATH, SORT_FIELDS);
		sURIMatcher.addURI(AUTHORITY, RECENT_FILES_PATH, RECENT_FILES);
		sURIMatcher.addURI(AUTHORITY, FILTERS_PATH, FILTERS);
	}
	
	public static Uri getEntryURI(String refKey){
		 return Uri.parse("content://" + AUTHORITY + "/" + ENTRY_PATH + "/" + refKey);
	}

	@Override
	public boolean onCreate() {
		dbHelper = new DBHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		// Using SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		// Check if the caller has requested a column which does not exists
		//
		int uriType = sURIMatcher.match(uri);
		String query = null;
		Cursor cursor = null;
		String groupBy = null;
		
		switch (uriType) {
		case ENTRIES:
			String joinStmt = String.format(Locale.US, "`%s` LEFT OUTER JOIN `%s` ON `%s`.`%s` = `%s`.`%s`",
												DBHelper.TABLE_ENTRIES, DBHelper.TABLE_SORT_FIELDS,
												DBHelper.TABLE_ENTRIES, DBHelper.COLUMN_KEY, 
												DBHelper.TABLE_SORT_FIELDS, DBHelper.COLUMN_REFKEY);
			queryBuilder.setTables(joinStmt);
			groupBy = String.format(Locale.US, "`%s`.`%s`", DBHelper.TABLE_ENTRIES, DBHelper.COLUMN_KEY);
			break;
		case SORT_FIELDS:
			queryBuilder.setTables(String.format(Locale.US, "%s JOIN %s ON %s.%s = %s.%s", 
					DBHelper.TABLE_ENTRIES, DBHelper.TABLE_SORT_FIELDS, 
					DBHelper.TABLE_ENTRIES, DBHelper.COLUMN_KEY,
					DBHelper.TABLE_SORT_FIELDS, DBHelper.COLUMN_REFKEY));
			break;
		case COLLECTIONS:
			query = String.format("SELECT MIN(%s) AS _id, COUNT(%s) AS %s, %s FROM %s %s GROUP BY %s ORDER BY %s COLLATE NOCASE ASC", 
											DBHelper.COLUMN_KEY,
											DBHelper.COLUMN_REFKEY,
											DBHelper.COLUMN_COUNT,
											DBHelper.COLUMN_COLLECTION,
											DBHelper.TABLE_COLLECTIONS,
											selection == null || selection.trim().isEmpty() ? "" : "WHERE " + selection,
											DBHelper.COLUMN_COLLECTION, 
											DBHelper.COLUMN_COLLECTION);
			cursor = dbHelper.getWritableDatabase().rawQuery(query, selectionArgs);
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
			return cursor;
		case FILTERS:
			queryBuilder.setTables(DBHelper.TABLE_FILTERS);
			break;
		case GROUPS:
			query = String.format("SELECT MIN(%s) AS _id, COUNT(%s) AS %s, %s FROM %s %s GROUP BY %s ORDER BY %s COLLATE NOCASE ASC", 
											DBHelper.COLUMN_KEY,
											DBHelper.COLUMN_REFKEY,
											DBHelper.COLUMN_COUNT,
											DBHelper.COLUMN_GROUP,
											DBHelper.TABLE_GROUPS,
											selection == null || selection.trim().isEmpty() ? "" : "WHERE " + selection,
											DBHelper.COLUMN_GROUP,
											DBHelper.COLUMN_GROUP);
			cursor = dbHelper.getWritableDatabase().rawQuery(query, selectionArgs);
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
			return cursor;
		case KEYWORDS:
			query = String.format("SELECT MIN(%s) AS _id, COUNT(%s) AS %s, %s FROM %s %s GROUP BY %s COLLATE NOCASE ORDER BY %s COLLATE NOCASE ASC", 
											DBHelper.COLUMN_KEY,
											DBHelper.COLUMN_REFKEY,
											DBHelper.COLUMN_COUNT,
											DBHelper.COLUMN_KEYWORD,
											DBHelper.TABLE_KEYWORDS,
											selection == null || selection.trim().isEmpty() ? "" : "WHERE " + selection,
											DBHelper.COLUMN_KEYWORD,
											DBHelper.COLUMN_KEYWORD);
			cursor = dbHelper.getWritableDatabase().rawQuery(query, selectionArgs);
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
			return cursor;
		case STRINGS:
			queryBuilder.setTables(DBHelper.TABLE_STRINGS);
			break;
		case OBJECTS:
			queryBuilder.setTables(DBHelper.TABLE_OBJECTS);
			break;
		case DOCUMENTS:
			queryBuilder.setTables(DBHelper.TABLE_DOCUMENT_IDS);
			break;
		case ENTRY_KEY:
			queryBuilder.setTables(DBHelper.TABLE_ENTRIES);
			queryBuilder.appendWhere(DBHelper.COLUMN_KEY + "=" + uri.getLastPathSegment());
			break;
		case RECENT_FILES:
			queryBuilder.setTables(DBHelper.TABLE_RECENT_FILES);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		cursor = queryBuilder.query(db, projection, selection, selectionArgs, groupBy, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		long id = 0;
		switch (uriType) {
		case ENTRIES:
			id = sqlDB.insert(DBHelper.TABLE_ENTRIES, null, values);
			break;
		case SORT_FIELDS:
			id = sqlDB.insert(DBHelper.TABLE_SORT_FIELDS, null, values);
			break;
		case COLLECTIONS:
			id = sqlDB.insert(DBHelper.TABLE_COLLECTIONS, null, values);
			break;
		case KEYWORDS:
			id = sqlDB.insert(DBHelper.TABLE_KEYWORDS, null, values);
			break;
		case GROUPS:
			id = sqlDB.insert(DBHelper.TABLE_GROUPS, null, values);
			break;
		case STRINGS:
			id = sqlDB.insert(DBHelper.TABLE_STRINGS, null, values);
			break;
		case OBJECTS:
			id = sqlDB.insert(DBHelper.TABLE_OBJECTS, null, values);
			break;
		case DOCUMENTS:
			id = sqlDB.insert(DBHelper.TABLE_DOCUMENT_IDS, null, values);
			break;
		case RECENT_FILES:
			id = sqlDB.insert(DBHelper.TABLE_RECENT_FILES, null, values);
			break;
		case FILTERS:
			id = sqlDB.insert(DBHelper.TABLE_FILTERS, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.withAppendedPath(uri, ""+id);
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] valueSet) {
		int uriType = sURIMatcher.match(uri);
		int inserted = 0;
		
		String tableName = null;
		switch (uriType) {
		case ENTRIES:
			tableName = DBHelper.TABLE_ENTRIES;
			break;
		case SORT_FIELDS:
			tableName = DBHelper.TABLE_SORT_FIELDS;
			break;
		case COLLECTIONS:
			tableName = DBHelper.TABLE_COLLECTIONS;
			break;
		case KEYWORDS:
			tableName = DBHelper.TABLE_KEYWORDS;
			break;
		case GROUPS:
			tableName = DBHelper.TABLE_GROUPS;
			break;
		case STRINGS:
			tableName = DBHelper.TABLE_STRINGS;
			break;
		case OBJECTS:
			tableName = DBHelper.TABLE_OBJECTS;
			break;
		case DOCUMENTS:
			tableName = DBHelper.TABLE_DOCUMENT_IDS;
			break;
		case RECENT_FILES:
			tableName = DBHelper.TABLE_RECENT_FILES;
			break;
		case FILTERS:
			tableName = DBHelper.TABLE_FILTERS;
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		sqlDB.beginTransaction();
		
		for(ContentValues values : valueSet){
			sqlDB.insert(tableName, null, values);
			inserted ++;
		}
		
		sqlDB.setTransactionSuccessful();
		sqlDB.endTransaction();
		
		getContext().getContentResolver().notifyChange(uri, null);
		return inserted;
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		int rowsDeleted = 0;
		String id = "";
		switch (uriType) {
		case ENTRIES:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_ENTRIES, selection, selectionArgs);
			break;
		case ENTRY_KEY:
			id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(DBHelper.TABLE_ENTRIES,
						DBHelper.COLUMN_KEY + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(DBHelper.TABLE_ENTRIES,
						DBHelper.COLUMN_KEY + "=" + id + " and " + selection,
						selectionArgs);
			}
			break;
		case SORT_FIELDS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_SORT_FIELDS, selection, selectionArgs);
			break;
		case COLLECTIONS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_COLLECTIONS, selection, selectionArgs);
			break;
		case KEYWORDS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_KEYWORDS, selection, selectionArgs);
			break;
		case GROUPS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_GROUPS, selection, selectionArgs);
			break;
		case STRINGS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_STRINGS, selection, selectionArgs);
			break;
		case OBJECTS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_OBJECTS, selection, selectionArgs);
			break;
		case DOCUMENTS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_DOCUMENT_IDS, selection, selectionArgs);
			break;
		case RECENT_FILES:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_RECENT_FILES, selection, selectionArgs);
			break;
		case FILTERS:
			rowsDeleted = sqlDB.delete(DBHelper.TABLE_FILTERS, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {

		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case ENTRIES:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_ENTRIES, values, selection,
					selectionArgs);
			break;
		case ENTRY_KEY:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(DBHelper.TABLE_ENTRIES, values,
						DBHelper.COLUMN_KEY + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(DBHelper.TABLE_ENTRIES, values,
						DBHelper.COLUMN_KEY + "=" + id + " and " + selection,
						selectionArgs);
			}
			break;
		case SORT_FIELDS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_SORT_FIELDS, values, selection, selectionArgs);
			break;
		case COLLECTIONS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_COLLECTIONS, values, selection, selectionArgs);
			break;
		case KEYWORDS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_KEYWORDS, values, selection, selectionArgs);
			break;
		case GROUPS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_GROUPS, values, selection, selectionArgs);
			break;
		case STRINGS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_STRINGS, values, selection, selectionArgs);
			break;
		case OBJECTS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_OBJECTS, values, selection, selectionArgs);
			break;
		case DOCUMENTS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_DOCUMENT_IDS, values, selection, selectionArgs);
			break;
		case RECENT_FILES:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_RECENT_FILES, values, selection, selectionArgs);
			break;
		case FILTERS:
			rowsUpdated = sqlDB.update(DBHelper.TABLE_FILTERS, values, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}
}
