package com.mm.eratos.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.mm.eratos.bibtex.ACSReferenceStyleExtended;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.BibTeXHelper;
import com.mm.eratos.bibtex.CiteKeyFormatter;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.search.CustomSearchHandler;
import com.mm.eratos.search.ISearchHandler;
import com.mm.eratos.search.SearchFactory;
import com.mm.eratos.utils.StringUtils;

public class EratosSettings {
	private static final String SORT_FIELD_DELIMITER = ";";
	public static final String SEARCH_HANDLER_DELIMITER = "\n";
	public static final String SEARCH_HANDLER_ORDER_DELIMITER = "|||";
	
	private static final String CURRENT_SORT_DIRECTION_FIELD = "sort_direction";
	private static final String CURRENT_SORT_ORDER_FIELD = "sort_order";
	
	private static final String DROPBOX_PATH_FIELD = "dropbox_path";
	private static final String DRIVE_PATH_FIELD = "drive_path";
	private static final String EXPORT_PATH_FIELD = "export_path";
	
	public static final String IMPORT_EXPORT_FORMAT_FIELD = "IMPORT_EXPORT_FORMAT";
	public static final String IMPORT_EXPORT_FORMAT_DEFAULT = "com.mm.eratos.bibtex.format.IdentityTranslator";

	public static final String STORE_FILE_SUBDIRECTORY_FIELD = "STORE_FILE_SUBDIRECTORY";
	public static final String STORE_FILE_LOCATION_FIELD = "STORE_FILE_LOCATION";
	public static final String MANAGE_FILES_FIELD = "MANAGE_FILES";
	public static final String COPY_LOCAL_FILES_FIELD = "COPY_LOCAL_FILES";
	public static final String DELETE_ORIGINALS_FIELD = "DELETE_ORIGINALS";
	
	private static final String IMPORT_LATEX_TO_UTF8_FIELD = "CONVERT_LATEX";
	public static final String IMPORT_UTF8_FIELD = "CONVERT_UTF8";
	public static final String AUTO_CONVERT_UTF8_FIELD = "AUTO_CONVERT_UTF8";
	
	public static final String CITE_KEY_AUTOFORMAT_FIELD = "CITE_KEY_AUTOFORMAT";
	public static final String CITE_KEY_FORMATTER_FIELD = "CITE_KEY_FORMATTER";
	public static final String CITE_KEY_FORMATTER_DEFAULT = "%a%y%t";
	
	public static final String CITE_COMMAND_FIELD = "CITE_COMMAND";
	public static final String CITATION_FORMAT_FIELD = "CITATION_FORMAT";
	public static final String CITATION_FORMAT_DEFAULT = "american-chemical-society";
	
	public static final String STORE_RECENT_FILES = "STORE_RECENT";
	public static final String STORE_HISTORY_FIELD = "STORE_HISTORY";
	public static final String SYNC_OVER_WIFI_ONLY_FIELD = "SYNC_OVER_WIFI_ONLY";
	public static final String SYNC_DOWNLOAD_ONLY = "SYNC_DOWNLOAD_ONLY";
	
	public static final String AUTOSAVE_ENABLED_FIELD = "AUTOSAVE_DATABASE";
	
	public static final String STORE_FILE_LOCATION_DEFAULT = "bibtex://";
	public static final String STORE_FILE_LOCATION_OTHER = "other";
	
	public static final String CUSTOM_SORT_FIELDS = "custom_sort_fields";
	public static final String CUSTOM_SORT_FIELD_TYPES = "custom_sort_field_types";
	public static final String CUSTOM_SORT_FIELD_TYPE_FULL_HEADER = "full";
	public static final String CUSTOM_SORT_FIELD_TYPE_FIRST_CHAR = "first";

	public static final String CUSTOM_SEARCH_HANDLERS_PREF = "custom_search_handlers";
	public static final String CUSTOM_SEARCH_HANDLERS_ORDER_PREF = "custom_search_handlers_order";
	public static final String DEFAULT_SEARCH_HANDLER_PREF = "default_search_handler";
	public static final String DEFAULT_SEARCH_HANDLER = "Google Scholar";
	
	public static final String TREAT_WARNINGS_AS_ERRORS = "warnings_as_errors";

	public static final String EZPROXY_ACCOUNT_NAME = "EZPROXY_ACCOUNT_NAME";
	public static final String USE_EZPROXY = "USE_EZPROXY";

	private static final String CUSTOM_SEARCH_HANDLERS_DEFAULT = SearchFactory.getDefaultHandlers();
	private static final String CUSTOM_SEARCH_HANDLERS_ORDER_DEFAULT = SearchFactory.getDefaultOrder();
	
	private SharedPreferences preferences;
	private BibTeXHelper bibtexHelper;

	public EratosSettings(SharedPreferences preferences){
		this.preferences = preferences;
		this.bibtexHelper = new BibTeXHelper(new ACSReferenceStyleExtended(), treatVerificationWarningsAsErrors());
	}

	public String getCitationFormat() {
		return getPreferences().getString(CITATION_FORMAT_FIELD, CITATION_FORMAT_DEFAULT);
	}
	
	public void setCitationFormat(String value) {
		Editor edit = getPreferences().edit();
		edit.putString(CITATION_FORMAT_FIELD, value);
		edit.commit();
	}
	
	public SharedPreferences getPreferences(){
		return preferences;
	}
	
	public List<String> getHandlerOrder() {
		String pref = getPreferences().getString(CUSTOM_SEARCH_HANDLERS_ORDER_PREF, CUSTOM_SEARCH_HANDLERS_ORDER_DEFAULT);
		
		List<String> order = new ArrayList<String>();
		for(String name : pref.split(Pattern.quote(SEARCH_HANDLER_ORDER_DELIMITER))) {
			String trim = name.trim();
			if(!trim.isEmpty())
				order.add(trim);
		}
		return order;
	}
	
	public void setHandlerOrder(List<String> order) {
		Editor edit = preferences.edit();
		edit.putString(CUSTOM_SEARCH_HANDLERS_ORDER_PREF, StringUtils.join(SEARCH_HANDLER_ORDER_DELIMITER, order));
		edit.commit();
	}
	
	public void makeHandlerDefault(String handler) {
		List<String> order = getHandlerOrder();
		
		if(order.contains(handler))
			order.remove(handler);
		order.add(0, handler);
		
		setHandlerOrder(order);
	}
	
	public void setSearchHandlers(List<CustomSearchHandler> searchHandlers) {
		SharedPreferences preferences = getPreferences();
		Editor edit = preferences.edit();
		
		Set<String> handlerNames = new HashSet<String>();
		
		List<String> serialized = new ArrayList<String>(searchHandlers.size());
		for(ISearchHandler handler : searchHandlers){
			handlerNames.add(handler.name());
			serialized.add( handler.toString() );
		}
		
		List<String> newOrder = new ArrayList<String>(searchHandlers.size());
		// filter deleted handlers
		for(String name : getHandlerOrder()){
			if(handlerNames.contains(name))
				newOrder.add(name);
		}
		// add new handlers
		for(ISearchHandler handler : searchHandlers){
			if( !newOrder.contains(handler.name()) )
				newOrder.add(handler.name());
		}
		setHandlerOrder(newOrder);
		
		String value = StringUtils.join(SEARCH_HANDLER_DELIMITER, serialized);
		edit.putString(CUSTOM_SEARCH_HANDLERS_PREF, value);
		edit.commit();
	}
	
	public Map<String, CustomSearchHandler> getSearchHandlers() {
		Map<String, CustomSearchHandler> customSearchHandlers = new HashMap<String, CustomSearchHandler>();
		
		String handlerList = getPreferences().getString(CUSTOM_SEARCH_HANDLERS_PREF, CUSTOM_SEARCH_HANDLERS_DEFAULT);
		for(String handler : handlerList.split(SEARCH_HANDLER_DELIMITER)) {
			handler = handler.trim();
			
			if(!handler.isEmpty()){
				CustomSearchHandler searchHandler = CustomSearchHandler.parse(handler);
				customSearchHandlers.put( searchHandler.name(), searchHandler );
			}
		}
		
		return customSearchHandlers;
	}
	
	public int getSortOrder() {
		return getPreferences().getInt(CURRENT_SORT_ORDER_FIELD, BibTeXContentProviderAdapter.SORT_AUTHOR);
	}
	
	public int getSortDirection() {
		return getPreferences().getInt(CURRENT_SORT_DIRECTION_FIELD, BibTeXContentProviderAdapter.SORT_ASCENDING);
	}
	
	public void setSortMode(int order, int direction) {
		Editor edit = getPreferences().edit();
		edit.putInt(CURRENT_SORT_ORDER_FIELD, order);
		edit.putInt(CURRENT_SORT_DIRECTION_FIELD, direction);
		edit.commit();
	}
	
	public boolean getAutosaveEnabled() {
		return getPreferences().getBoolean(AUTOSAVE_ENABLED_FIELD, false);
	}


	public EratosUri getExportUri(){
		return EratosUri.parseUri( getPreferences().getString(EXPORT_PATH_FIELD, FileManager.getFileManager().getDefaultExportUri().toString()) );
	}

	public EratosUri getDropboxExportUri() {
		return EratosUri.parseUri( getPreferences().getString(DROPBOX_PATH_FIELD, FileManager.getFileManager().getDefaultDropboxUri().toString()) );
	}
	
	public EratosUri getDriveExportUri() {
		return EratosUri.parseUri( getPreferences().getString(DRIVE_PATH_FIELD, FileManager.getFileManager().getDefaultDriveUri().toString()) );
	}
	public String getBibTeXTranslatorClassName() {
		return getPreferences().getString(IMPORT_EXPORT_FORMAT_FIELD, IMPORT_EXPORT_FORMAT_DEFAULT);
	}

	public BibTeXHelper getBibTeXHelper() {
		return bibtexHelper;
	}

	public void changeBibtexHelper() {
		this.bibtexHelper = new BibTeXHelper(new ACSReferenceStyleExtended(), treatVerificationWarningsAsErrors());
	}
	
	public String getStoreLocalSubdir() {
		return getPreferences().getString(STORE_FILE_SUBDIRECTORY_FIELD, "");
	}
	
	public EratosUri getStoreLocalLocation() {
		return EratosUri.parseUri( getPreferences().getString(STORE_FILE_LOCATION_FIELD, STORE_FILE_LOCATION_DEFAULT) );
	}
	
	public boolean getManagedFiles() {
		return getPreferences().getBoolean(MANAGE_FILES_FIELD, true);
	}

	public boolean getCopyLocalOnly() {
		return getPreferences().getBoolean(COPY_LOCAL_FILES_FIELD, false);
	}
	
	public boolean getDeleteOriginals() {
		return getPreferences().getBoolean(DELETE_ORIGINALS_FIELD, false);
	}
	
	public boolean getConvertLaTeXtoUTF8() {
		return getPreferences().getBoolean(IMPORT_LATEX_TO_UTF8_FIELD, true);
	}
	
	public boolean getConvertUTF8ToLaTeX() {
		return getPreferences().getBoolean(IMPORT_UTF8_FIELD, false);
	}
	
	public boolean getEditInUTF8ConvertToLaTeX() {
		return getPreferences().getBoolean(AUTO_CONVERT_UTF8_FIELD, false);
	}
	
	public boolean getCiteKeyAutoformat() {
		return getPreferences().getBoolean(CITE_KEY_AUTOFORMAT_FIELD, false);
	}
	
	public CiteKeyFormatter getCiteKeyFormatter() {
		return new CiteKeyFormatter(getPreferences().getString(CITE_KEY_FORMATTER_FIELD, CITE_KEY_FORMATTER_DEFAULT));
	}
	
	public String getCitationCommand() {
		return getPreferences().getString(CITE_COMMAND_FIELD, "cite");
	}
	
	public boolean storeOnlyRecentFiles() {
		return getPreferences().getBoolean(STORE_RECENT_FILES, true);
	}
	
	public int getStorageHistory(){
		return Integer.parseInt(getPreferences().getString(STORE_HISTORY_FIELD, "20"));
	}
	
	public boolean syncOverWiFiOnly() {
		return getPreferences().getBoolean(SYNC_OVER_WIFI_ONLY_FIELD, false);
	}
	
	public boolean syncReadOnly() {
		return getPreferences().getBoolean(SYNC_DOWNLOAD_ONLY, false);
	}

	public void writePreferences(File file) throws IOException {
		FileWriter fw = new FileWriter(file);
		
		Map<String, ?> allPrefs = getPreferences().getAll();
		
		for(String key : allPrefs.keySet()){
			Object value = allPrefs.get(key);
			if(value == null)
				fw.write(String.format(Locale.US, "%s=null\n", key));
			else
				fw.write(String.format(Locale.US, "%s=%s\n", key, value.toString()));
		}
		
		fw.close();
	}


	public boolean treatVerificationWarningsAsErrors(){
		return getPreferences().getBoolean(TREAT_WARNINGS_AS_ERRORS, false);
	}
	
	public String getEZProxyAccount(){
		if(getEZProxyEnabled())
			return getPreferences().getString(EZPROXY_ACCOUNT_NAME, "");
		return "";
	}
	
	public void useEZProxyAccount(String acctName){
		Editor edit = getPreferences().edit();
		edit.putString(EZPROXY_ACCOUNT_NAME, acctName);
		edit.commit();
	}
	
	public void enableEZProxy(){
		Editor edit = getPreferences().edit();
		edit.putBoolean(USE_EZPROXY, true);
		edit.commit();
	}
	
	public void disableEZProxy(){
		Editor edit = getPreferences().edit();
		edit.putBoolean(USE_EZPROXY, false);
		edit.commit();
	}
	
	public boolean getEZProxyEnabled(){
		return getPreferences().getBoolean(USE_EZPROXY, false);
	}
	

	public Map<String, String> getSortFields() {
		String[] fields = getPreferences().getString(CUSTOM_SORT_FIELDS, "").split(SORT_FIELD_DELIMITER);
		String[] types = getPreferences().getString(CUSTOM_SORT_FIELD_TYPES, "").split(SORT_FIELD_DELIMITER);
		
		Map<String, String> fieldsMap = new LinkedHashMap<String, String>();
		for(int i = 0; i < fields.length; i++){
			String field = fields[i].trim();
			String type = CUSTOM_SORT_FIELD_TYPE_FIRST_CHAR;
			if(i < types.length && !types[i].trim().isEmpty())
				type = types[i];
			
			if(!field.isEmpty()) fieldsMap.put(field, type);
		}
		return fieldsMap;
	}

	public String getSortField(int i) {
		String[] fields = getPreferences().getString(CUSTOM_SORT_FIELDS, "").split(SORT_FIELD_DELIMITER);
		if(i < fields.length && !fields[i].trim().isEmpty())
			return fields[i];
		
		return null;
	}
	
	public String getSortFieldHeaderMode(int i) {
		String[] types = getPreferences().getString(CUSTOM_SORT_FIELD_TYPES, "").split(SORT_FIELD_DELIMITER);
		if(i < types.length && !types[i].trim().isEmpty())
			return types[i];
		
		return CUSTOM_SORT_FIELD_TYPE_FIRST_CHAR;
	}
	
	public void setSortFields(Map<String, String> fieldsMap){
		List<String> fields = new ArrayList<String>(fieldsMap.size());
		List<String> types = new ArrayList<String>(fieldsMap.size());
		
		for(String field : fieldsMap.keySet()) {
			fields.add(field);
			types.add(fieldsMap.get(field));
		}
		
		Editor edit = getPreferences().edit();
		
		String value = StringUtils.join(SORT_FIELD_DELIMITER, fields);
		edit.putString(CUSTOM_SORT_FIELDS, value);
		
		value = StringUtils.join(SORT_FIELD_DELIMITER, types);
		edit.putString(CUSTOM_SORT_FIELD_TYPES, value);
		
		edit.commit();
	}
}
