package com.mm.eratos.application;

import java.io.IOException;
import java.util.Calendar;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import com.mm.eratos.ezproxy.EZProxyAccountManager;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.WebUtils;
import com.mm.eratos.view.MainMenu.ConfirmCallback;

public class EratosApplication extends Application {
	private static final String LAST_RUN_VERSION = "last_run_version";
	
	private static final String CURRENT_LIBRARY_TYPE = "library_type";
	private static final String CURRENT_LIBRARY_ENCODING = "library_encoding";
	private static final String CURRENT_LIBRARY_FIELD = "loaded";
	private static final String CURRENT_LIBRARY_PATH_FIELD = "loaded_path";
	
	private static final String CHANGES_SAVED = "CHANGES_SAVED";
	
	private static final String LAST_VIEWED_REFKEY = "LAST_VIEWED_REFKEY";
	private static final String LAST_OPENED_LIBRARY_FIELD = "last_library";
	private static final String LAST_CHECK_TIME = "last_sync_check_time";
	
	public static final String adminEmail = "eratosthenes.app@gmail.com";

	public static final String SENT_BY_ERATOSTHENES_MESSAGE = "Shared using Eratosthenes BibTeX Reference Manager (available on Google Play)";
	
	public static EratosApplication singleton;
	private EZProxyAccountManager ezProxyAccountManager;
	private EratosSettings settingsManager;
	private EratosLogManager logManager;
	private EratosLogger logger;
	
	public EratosApplication(){
		singleton = this;
	}
	
	public String getVersion(){
		try{
			return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}
	
	public boolean isFirstRun() {
		return ! getPreferences().getString(LAST_RUN_VERSION, "").equals(getVersion());
	}
	
	public void markRun() {
		Editor edit = getPreferences().edit();
		edit.putString(LAST_RUN_VERSION, getVersion());
		edit.commit();
	}

	public void markSaved() {
		Editor edit = getPreferences().edit();
		edit.putBoolean(CHANGES_SAVED, true);
		edit.commit();
	}
	
	public void markUnsaved() {
		Editor edit = getPreferences().edit();
		edit.putBoolean(CHANGES_SAVED, false);
		edit.commit();
	}
	
	public boolean isSaved() {
		return getPreferences().getBoolean(CHANGES_SAVED, true);
	}
	
	public String getCurrentLibraryType() {
		return getPreferences().getString(CURRENT_LIBRARY_TYPE, null);
	}
	
	public String getCurrentLibraryEncoding() {
		return getPreferences().getString(CURRENT_LIBRARY_ENCODING, "UTF-8");
	}
	
	public EratosUri getCurrentLibrary(){
		String libUri = getPreferences().getString(CURRENT_LIBRARY_FIELD, null);
		if(libUri == null)
			return null;
		return EratosUri.parseUri( libUri );
	}

	public EratosUri getCurrentLibraryPath() {
		String libUri = getPreferences().getString(CURRENT_LIBRARY_PATH_FIELD, null);
		if(libUri == null)
			return null;
		return EratosUri.parseUri( libUri );
	}
	
	public void closeLibrary() {
		Editor edit = getPreferences().edit();
		
		edit.putString(CURRENT_LIBRARY_TYPE, null);
		edit.putString(CURRENT_LIBRARY_FIELD, null);
		edit.putString(CURRENT_LIBRARY_PATH_FIELD, null);
		edit.putBoolean(CHANGES_SAVED, true);
		
		edit.commit();
	}
	

	public EratosUri getLastOpenedLibrary() {
		return EratosUri.parseUri(getPreferences().getString(LAST_OPENED_LIBRARY_FIELD, ""));
	}
	
	public void setOpenLibrary(EratosUri uri, String bibType, String encoding) {
		Editor edit = getPreferences().edit();
		
		edit.putString(LAST_OPENED_LIBRARY_FIELD, uri.toString());
		edit.putString(CURRENT_LIBRARY_FIELD, uri.toString());
		edit.putString(CURRENT_LIBRARY_PATH_FIELD, uri.parent().toString());
		edit.putString(CURRENT_LIBRARY_TYPE, bibType);
		edit.putString(CURRENT_LIBRARY_ENCODING, encoding);
		edit.putBoolean(CHANGES_SAVED, true);
		
		edit.commit();
	}
	
	public static EratosApplication getApplication(){
		return singleton;
	}
	
	public SharedPreferences getPreferences() {
		return PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
 
	@Override
	public void onCreate() {
		super.onCreate();
		try{
			logManager = new EratosLogManager(this, true);
			logger = logManager.getLogger(EratosApplication.class);
		}catch(IOException e){
			e.printStackTrace();
			NotificationUtils.notifyException(this, "Failed Creating Log File", e);
			throw new RuntimeException(e);
		}
		
		ezProxyAccountManager = new EZProxyAccountManager(this);
		settingsManager = new EratosSettings(getPreferences());
		
		try{
			FileManager.getFileManager().createDefaultDirectories();
		}catch(IOException e){
			e.printStackTrace();
			NotificationUtils.notifyException(this, "Failed Creating Library Root Path", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}
 
	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	public String getLastViewedRefKey() {
		return getPreferences().getString(LAST_VIEWED_REFKEY, null);
	}
	
	public void setLastViewedRefKey(String refKey) {
		Editor edit = getPreferences().edit();
		edit.putString(LAST_VIEWED_REFKEY, refKey);
		edit.commit();
	}

	public boolean isConnected() {
		ConnectivityManager conMgr =  (ConnectivityManager)EratosApplication.getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo wifiInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo ethInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
		NetworkInfo mobileInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		boolean mobile = mobileInfo != null && mobileInfo.getState() == NetworkInfo.State.CONNECTED;
		boolean wifi = wifiInfo != null && wifiInfo.getState() == NetworkInfo.State.CONNECTED;
		boolean eth = ethInfo != null && ethInfo.getState() == NetworkInfo.State.CONNECTED;
		
		return (wifi || mobile || eth);
	}
	
	public boolean isOnline() {
		return isConnected() && WebUtils.canAccess("http://www.google.com");
	}
	
	public boolean canSync() {
		ConnectivityManager conMgr =  (ConnectivityManager)EratosApplication.getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
		
		boolean wifiOnly = settingsManager.syncOverWiFiOnly();
		NetworkInfo wifiInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo ethInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
		NetworkInfo mobileInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		boolean mobile = mobileInfo != null && mobileInfo.getState() == NetworkInfo.State.CONNECTED;
		boolean wifi = wifiInfo != null && wifiInfo.getState() == NetworkInfo.State.CONNECTED;
		boolean eth = ethInfo != null && ethInfo.getState() == NetworkInfo.State.CONNECTED;
		
		return (wifi || (mobile && ! wifiOnly) || eth);
	}
	
	public long lastSyncCheckAge() {
		long now = Calendar.getInstance().getTimeInMillis();
		return now - getPreferences().getLong(LAST_CHECK_TIME, 0);
	}
	
	public void setSyncChecked() {
		long now = Calendar.getInstance().getTimeInMillis();
		Editor edit = getPreferences().edit();
		edit.putLong(LAST_CHECK_TIME, now);
		edit.commit();
	}
	
	public EZProxyAccountManager getEZProxyAccountManager() {
		return ezProxyAccountManager;
	}
	
	public EratosSettings getSettingsManager(){
		return settingsManager;
	}

	public EratosLogManager getLogManager() {
		return logManager;
	}
	

	public void confirmOverwrite(Context mContext, final ConfirmCallback confirmCallback) {
		if(EratosApplication.getApplication().isSaved())
			confirmCallback.confirmed();
		else{
			NotificationUtils.confirmDialog(mContext, "Warning!", "Current library is not saved, continue anyway?", 
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							confirmCallback.confirmed();
						}
					});
		}
	}

	public boolean isOpen() {
		return getCurrentLibrary() != null;
	}

	public EratosLogger getLogger() {
		return logger;
	}
}
