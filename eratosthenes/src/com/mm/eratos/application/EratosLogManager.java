package com.mm.eratos.application;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import android.util.Log;

import com.mm.eratos.files.FileUtils;
import com.mm.eratos.utils.StringUtils;

public class EratosLogManager {
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
	public static final String formatString = "[%s] <%s:%s> %s";
	public static final String logFileName = "eratos";
	
	private Logger eratosLogger;
	
	private String logFolder;
	private String logFilePath;
	private Handler handler;
	private SimpleFormatter textFormatter;
	private boolean outputLogcat;
	
	public EratosLogManager(EratosApplication application, boolean outputLogcat) throws IOException {
		this.outputLogcat = outputLogcat;
		
		logFolder = application.getFilesDir().getAbsolutePath();
		logFilePath = logFolder + "/" + logFileName + ".%g.log";
		
		textFormatter = new SimpleFormatter();
		
		handler = new FileHandler(logFilePath,  200 * 1024, 5, true);
		handler.setFormatter(textFormatter);
		
		eratosLogger = Logger.getLogger("com.mm.eratos");
		eratosLogger.addHandler(handler);
		eratosLogger.setLevel(Level.FINEST);
	}
	
	public EratosLogManager(EratosApplication application, boolean outputLogcat, Handler handler){
		this.outputLogcat = outputLogcat;
		
		this.handler = handler;
		textFormatter = new SimpleFormatter();
		
		eratosLogger = Logger.getLogger("com.mm.eratos");
		eratosLogger.addHandler(handler);
		eratosLogger.setLevel(Level.FINEST);
	}
	
	public EratosLogger getLogger(String tag){
		return new EratosLogger(this, tag);
	}
	
	public EratosLogger getLogger(Class<? extends Object> cls) {
		String tag = cls.getSimpleName();
		return new EratosLogger(this, tag);
	}
	
	public void verbose(String tag, String msg, Object ... args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		log(Log.VERBOSE, tag, msg);
	}
	
	public void debug(String tag, String msg, Object ... args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		log(Log.DEBUG, tag, msg);
	}
	
	public void info(String tag, String msg, Object ... args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		log(Log.INFO, tag, msg);
	}
	
	public void warn(String tag, String msg, Object ... args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		log(Log.WARN, tag, msg);
	}

	public void error(String tag, Throwable t, String msg, Object[] args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		msg += "\n\n" + StringUtils.stackTraceToString(t);
	}
	
	public void error(String tag, String msg, Object ... args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		log(Log.ERROR, tag, msg);
	}
	
	public void zomg(String tag, String msg, Object ... args) {
		if(args.length>0)
			msg = String.format(Locale.US, msg, args);
		
		Log.wtf(tag, msg);
		eratosLogger.log( Level.SEVERE, String.format(Locale.US, "<%s> %s", tag, msg) );
	}
	
	public void log(int priority, String tag, String msg) {
		Level level = getLabel(priority);
		eratosLogger.log( level, String.format(Locale.US, "<%s> %s", tag, msg) );
		
		if(outputLogcat)
			Log.println(priority, tag, msg);
	}
	
	private static Level getLabel(int priority){
		switch(priority){
		case Log.VERBOSE:
			return Level.FINER;
		case Log.INFO:
			return Level.FINE;
		case Log.DEBUG:
			return Level.INFO;
		case Log.WARN:
			return Level.WARNING;
		case Log.ERROR:
			return Level.SEVERE;
		default:
			return Level.FINEST;
		}
	}

	public List<File> copyLogFiles(File tmpFolder) throws IOException {
		List<File> logFiles = new ArrayList<File>();
		for(File f : new File(logFolder).listFiles()){
			if(f.getName().startsWith(logFileName) && f.getName().endsWith("log")){
				File logCopy = new File(tmpFolder.getAbsolutePath() + "/" + f.getName());
				FileUtils.copy(f, logCopy);
				logFiles.add(logCopy);
			}
		}
		return logFiles;
	}
}
