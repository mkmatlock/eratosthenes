package com.mm.eratos.application;

public class EratosLogger {
	private String tag;
	private EratosLogManager logManager;

	protected EratosLogger(EratosLogManager logManager, String tag){
		this.tag = tag;
		this.logManager = logManager;
	}
	
	public void verbose(String msg, Object ... args) {
		logManager.verbose(tag, msg, args);
	}
	
	public void debug(String msg, Object ... args) {
		logManager.debug(tag, msg, args);
	}
	
	public void info(String msg, Object ... args) {
		logManager.info(tag, msg, args);
	}
	
	public void warn(String msg, Object ... args) {
		logManager.warn(tag, msg, args);
	}
	
	public void error(Throwable t, String msg, Object ... args) {
		logManager.error(tag, t, msg, args);
	}
	
	public void error(String msg, Object ... args) {
		logManager.error(tag, msg, args);
	}
	
	public void zomg(String msg, Object ... args) {
		logManager.zomg(tag, msg, args);
	}
}
