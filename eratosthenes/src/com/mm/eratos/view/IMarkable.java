package com.mm.eratos.view;

public interface IMarkable {
	public boolean isMarked();
	public void setMarked(boolean marked);
	public void toggleMarked();
}
