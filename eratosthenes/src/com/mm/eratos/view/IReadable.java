package com.mm.eratos.view;

public interface IReadable {
	public boolean isRead();
	public void setRead(boolean read);
	public void toggleRead();
}
