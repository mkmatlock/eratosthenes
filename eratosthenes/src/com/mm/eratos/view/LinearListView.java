package com.mm.eratos.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;

public class LinearListView extends LinearLayout {
	private Adapter contentAdapter;
	private OnItemClickListener listener;
	private OnItemLongClickListener longClickListener;
	private DataSetObserver dataObserver = new DataSetObserver() {
		public void onChanged() {
			super.onChanged();
			fillListLayout();
		}
		public void onInvalidated() {
			super.onInvalidated();
			removeAllViews();
		}
		
	};
	
	public LinearListView(Context context) {
		super(context);
	}
	
	public LinearListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LinearListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setAdapter(Adapter adapter){
		if(contentAdapter != null)
			contentAdapter.unregisterDataSetObserver(dataObserver);
		contentAdapter = adapter;
		contentAdapter.registerDataSetObserver(dataObserver);
		fillListLayout();
	}

	private void fillListLayout() {
		removeAllViews();
		for(int i = 0; i < contentAdapter.getCount(); i++){
			View v = contentAdapter.getView(i, null, this);
			addView(v);
		}
		setOnItemClickListener(listener);
		setOnItemLongClickListener(longClickListener);
	}
	
	public Object getItemAtPosition(int pos) {
		if(contentAdapter == null)
			return null;
		return contentAdapter.getItem(pos);
	}

	public void setOnItemClickListener(final OnItemClickListener listener) {
		this.listener = listener;
		
		if(listener != null){
			for(int i = 0; i < getChildCount(); i++){
				final int vpos = i;
				getChildAt(i).setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						listener.onItemClick(null, v, vpos, contentAdapter.getItemId(vpos));
					}
				});
			}
		}
	}

	public void setOnItemLongClickListener(final OnItemLongClickListener listener) {
		this.longClickListener = listener;
		
		if(listener != null){
			for(int i = 0; i < getChildCount(); i++){
				final int vpos = i;
				getChildAt(i).setLongClickable(true);
				getChildAt(i).setOnLongClickListener(new OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						return listener.onItemLongClick(null, v, vpos, contentAdapter.getItemId(vpos));
					}
				});
			}
		}else{
			for(int i = 0; i < getChildCount(); i++){
				getChildAt(i).setLongClickable(false);
			}
		}
	}

	public Adapter getAdapter() {
		return contentAdapter;
	}
}

	
