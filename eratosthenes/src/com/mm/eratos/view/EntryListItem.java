package com.mm.eratos.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.RelativeLayout;

import com.mm.eratos.R;

public class EntryListItem extends RelativeLayout implements Checkable, IReadable {
	private static final int[] CheckedStateSet = {
	    android.R.attr.state_checked
	};
	
	private static final int[] ReadStateSet = {R.attr.state_read};
	
	boolean checked = false;
	boolean read = false;
	
	public EntryListItem(Context context) {
		super(context);
	}
	
	public EntryListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public EntryListItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	public boolean isChecked() {
		return checked;
	}
	
	@Override
	public void setChecked(boolean checked) {
		this.checked = checked;
		refreshDrawableState();
	}
	
	@Override
	public void toggle() {
		checked = !checked;
		refreshDrawableState();
	}
	
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
	    final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
	    if (isChecked()) {
	        mergeDrawableStates(drawableState, CheckedStateSet);
	    }
	    if (isRead()) {
	        mergeDrawableStates(drawableState, ReadStateSet);
	    }
	    return drawableState;
	}
	

	public void toggleRead() {
		read = !read;
		refreshDrawableState();
	}

	public void setRead(boolean read){
		this.read = read;
		refreshDrawableState();
	}
	
	public boolean isRead() {
		return read;
	}
}
