package com.mm.eratos.view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.mm.eratos.R;
import com.mm.eratos.activity.AddEntryFromArXiV;
import com.mm.eratos.activity.AddEntryFromDOI;
import com.mm.eratos.activity.AddEntryFromISBN;
import com.mm.eratos.activity.AddEntryFromPubMed;
import com.mm.eratos.activity.AddEntryWebHandlerSearchActivity;
import com.mm.eratos.activity.EditEntryActivity;
import com.mm.eratos.activity.EntryListActivity;
import com.mm.eratos.activity.FileBrowserActivity;
import com.mm.eratos.activity.HelpActivity;
import com.mm.eratos.activity.SettingsActivity;
import com.mm.eratos.activity.StringEditorActivity;
import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.dialog.BibtexEditorDialog;
import com.mm.eratos.feeds.AbstractWebHandler;
import com.mm.eratos.feeds.ArXiVHandler;
import com.mm.eratos.feeds.DOIWebHandler;
import com.mm.eratos.feeds.PubMedWebHandler;
import com.mm.eratos.feeds.WebHandlerFactory;
import com.mm.eratos.files.AbstractProgressFileOperationResultListener;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileResult;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.protocol.ProtocolConnectionManager;
import com.mm.eratos.protocol.ProtocolHandler;
import com.mm.eratos.protocol.task.ProtocolClearCacheTask;
import com.mm.eratos.protocol.task.ProtocolExportTask;
import com.mm.eratos.protocol.task.ProtocolImportTask;
import com.mm.eratos.protocol.task.ProtocolSyncTask;
import com.mm.eratos.task.FileSyncResultReceiver;
import com.mm.eratos.task.LoadLibraryTask;
import com.mm.eratos.task.ProgressCallback;
import com.mm.eratos.task.SaveDatabaseTask;
import com.mm.eratos.utils.IStringArrayResultListener;
import com.mm.eratos.utils.IStringResultListener;
import com.mm.eratos.utils.InputUtils;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.Validator;

public class MainMenu implements OnMenuItemClickListener, OnQueryTextListener
{
	public interface SortSelectedCallback {
		public void sortSelected(int sortOrder, int sortDirection);
	}

	public interface ConfirmCallback {
		public void confirmed();
	}
	
	public interface ActionCallback {
		public void onLibraryOpened();
		public void onCacheCleared();
	}
	
	private static final int REQUEST_NEW_LIBRARY_LOCATION = 11;
	private static final int REQUEST_OPEN_LIBRARY = 22;
	private static final int REQUEST_REMOTE_LOCATION = 33;
	
	private MenuItem newEntryMenuItem;
	private MenuItem searchMenuItem;
	
	private MenuItem newMenuItem;
	private MenuItem openMenuItem;
	private MenuItem saveMenuItem;
	
	private MenuItem dropboxCategory;
	private MenuItem dropboxSyncMenuItem;
	private MenuItem dropboxExportMenuItem;
	private MenuItem dropboxClearCacheMenuItem;
	private MenuItem dropboxRevertMenuItem;
	
	private MenuItem driveCategory;
	private MenuItem driveSyncMenuItem;
	private MenuItem driveExportMenuItem;
	private MenuItem driveClearCacheMenuItem;
	private MenuItem driveRevertMenuItem;
	
	private MenuItem settingsMenuItem;
	private MenuItem helpMenuItem;
	
	private MenuItem sortDateAddedMenuItem;
	private MenuItem sortCollectionMenuItem;
	private MenuItem sortAuthorMenuItem;
	private MenuItem sortTitleMenuItem;
	private MenuItem sortYearMenuItem;
	private MenuItem sortCategory;
	private MenuItem sortSourceMenuItem;
	private List<MenuItem> customSortMenuItems;
	
	private MenuItem stringEditorMenuItem;
	
	private SearchView searchView;
	
	private FragmentActivity mContext;
	private FileRevisionContentProviderAdapter fileAdapter;
	private final SortSelectedCallback sortCallback;
	private MenuItem sortDateModifiedMenuItem;
	private ActionCallback actionCallback;

	private MenuItem wikiMenuItem;
	private MenuItem aboutMenuItem;
	
	private ProgressCallback loaderListener = new ProgressCallback() {
		public void update(String stage, int progress, int maxProgress) {
			
		}
		public void finished() {
			EratosUri libraryUri = EratosApplication.getApplication().getCurrentLibrary();
			setMenuRemoteOptions(libraryUri);
			setAppSubtitle(libraryUri);
		}
		public void error(Throwable error) {
			setMenuRemoteOptions(null);
			setAppSubtitle(null);
		}
	};
	private MenuItem sortRandomMenuItem;
	private MenuItem reloadMenuItem;
	
	
	public MainMenu(FragmentActivity context, MenuInflater inflater, Menu menu, SortSelectedCallback sortCallback, ActionCallback actionCallback) {
		this.mContext = context;
		this.sortCallback = sortCallback;
		this.actionCallback = actionCallback;
		
		fileAdapter = new FileRevisionContentProviderAdapter(context.getContentResolver());
		
	    inflater.inflate(R.menu.manage_entries, menu);	    
	    searchMenuItem = menu.findItem(R.id.menu_search);
	    searchView = (SearchView)searchMenuItem.getActionView();
	    searchView.setOnQueryTextListener(this);
	    
	    newEntryMenuItem = menu.findItem(R.id.new_entry);
	    newEntryMenuItem.setOnMenuItemClickListener(this);
	    newMenuItem = menu.findItem(R.id.new_database);
	    newMenuItem.setOnMenuItemClickListener(this);
	    openMenuItem = menu.findItem(R.id.open_database);
	    openMenuItem.setOnMenuItemClickListener(this);
	    reloadMenuItem = menu.findItem(R.id.reload_database);
	    reloadMenuItem.setOnMenuItemClickListener(this);
		saveMenuItem = menu.findItem(R.id.save_database);
	    saveMenuItem.setOnMenuItemClickListener(this);
		
	    dropboxCategory = menu.findItem(R.id.dropbox);
	    
	    dropboxSyncMenuItem = menu.findItem(R.id.dropbox_sync);
	    dropboxSyncMenuItem.setOnMenuItemClickListener(this);
	    dropboxExportMenuItem = menu.findItem(R.id.dropbox_export);
	    dropboxExportMenuItem.setOnMenuItemClickListener(this);
	    dropboxClearCacheMenuItem = menu.findItem(R.id.dropbox_clear);
	    dropboxClearCacheMenuItem.setOnMenuItemClickListener(this);
	    dropboxRevertMenuItem = menu.findItem(R.id.dropbox_revert);
	    dropboxRevertMenuItem.setOnMenuItemClickListener(this);
	    
	    driveCategory = menu.findItem(R.id.drive);
	    
	    driveSyncMenuItem = menu.findItem(R.id.drive_sync);
	    driveSyncMenuItem.setOnMenuItemClickListener(this);
	    driveExportMenuItem = menu.findItem(R.id.drive_export);
	    driveExportMenuItem.setOnMenuItemClickListener(this);
	    driveClearCacheMenuItem = menu.findItem(R.id.drive_clear);
	    driveClearCacheMenuItem.setOnMenuItemClickListener(this);
	    driveRevertMenuItem = menu.findItem(R.id.drive_revert);
	    driveRevertMenuItem.setOnMenuItemClickListener(this);
		
	    sortCategory = menu.findItem(R.id.sort_type);
	    sortRandomMenuItem = menu.findItem(R.id.sort_random);
	    sortRandomMenuItem.setOnMenuItemClickListener(this);
		sortDateAddedMenuItem = menu.findItem(R.id.sort_date);
		sortDateAddedMenuItem.setOnMenuItemClickListener(this);
		sortDateModifiedMenuItem = menu.findItem(R.id.sort_modified);
		sortDateModifiedMenuItem.setOnMenuItemClickListener(this);
	    sortCollectionMenuItem = menu.findItem(R.id.sort_collection);
	    sortCollectionMenuItem.setOnMenuItemClickListener(this);
	    sortAuthorMenuItem = menu.findItem(R.id.sort_author);
	    sortAuthorMenuItem.setOnMenuItemClickListener(this);
	    sortTitleMenuItem = menu.findItem(R.id.sort_title);
	    sortTitleMenuItem.setOnMenuItemClickListener(this);
	    sortYearMenuItem = menu.findItem(R.id.sort_year);
	    sortYearMenuItem.setOnMenuItemClickListener(this);
	    sortSourceMenuItem = menu.findItem(R.id.sort_source);
	    sortSourceMenuItem.setOnMenuItemClickListener(this);
	    customSortMenuItems = new ArrayList<MenuItem>();
	    
	    settingsMenuItem = menu.findItem(R.id.settings);
	    settingsMenuItem.setOnMenuItemClickListener(this);
	    wikiMenuItem = menu.findItem(R.id.wiki);
	    wikiMenuItem.setOnMenuItemClickListener(this);
	    helpMenuItem = menu.findItem(R.id.help);
	    helpMenuItem.setOnMenuItemClickListener(this);
	    
	    stringEditorMenuItem = menu.findItem(R.id.string_editor);
	    stringEditorMenuItem.setOnMenuItemClickListener(this);
	    
	    aboutMenuItem = menu.findItem(R.id.about);
	    aboutMenuItem.setOnMenuItemClickListener(this);
		
	    init();
	}

	private void init() {
		EratosApplication application = EratosApplication.getApplication();
		EratosUri libUri = application.getCurrentLibrary();
		addCustomSortMenuItems();
		setMenuRemoteOptions(libUri);
		setAppSubtitle(libUri);
		setSelectedSort(application.getSettingsManager().getSortOrder());
	}
	
	public void setAppSubtitle(EratosUri libUri) {
		if(libUri != null){
			String subtitle = StringUtils.ellipsize(libUri.toString(), 20, StringUtils.ELLIPSIZE_START);
			mContext.getActionBar().setSubtitle(subtitle);
		}else{
			mContext.getActionBar().setSubtitle("");
		}
	}

	private void addCustomSortMenuItems() {
		int i = 0;
		Map<String, String> sortFields = EratosApplication.getApplication().getSettingsManager().getSortFields();
		
		for(MenuItem item : customSortMenuItems){
			sortCategory.getSubMenu().removeItem(item.getItemId());
		}
		customSortMenuItems.clear();
		
		for(String field : sortFields.keySet()) {
			final int sortColumn = BibTeXContentProviderAdapter.CUSTOM_SORT_BIT + i;
			
			MenuItem item = sortCategory.getSubMenu().add(field);
			item.setCheckable(true);
			item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				public boolean onMenuItemClick(MenuItem item) {
					notifySort(sortColumn);
					return true;
				}
			});
			customSortMenuItems.add(item);
			i++;
		}
	}

	public void setQueryValue(String queryValue, final ISearchViewCloseListener listener){
        searchMenuItem.setOnActionExpandListener(new OnActionExpandListener()
        {
            public boolean onMenuItemActionCollapse(MenuItem item)
            {
            	listener.searchViewClosed();
                return true;
            }
            public boolean onMenuItemActionExpand(MenuItem item)
            {
                return true;
            }
        });
        searchMenuItem.expandActionView();
    	searchView.setQuery(queryValue, false);
    	searchView.clearFocus();
	}
	
	public void clearQueryValue(){
		searchView.setQuery("", false);
    	searchMenuItem.collapseActionView();
	}
	
	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}


	@Override
	public boolean onQueryTextSubmit(String query) {
		String b64query = Base64.encodeToString(query.getBytes(), Base64.URL_SAFE);
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(EntryListActivity.SEARCH_URI + "/" + b64query));
		mContext.startActivity(i);
		return true;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		Intent i;
		
		switch(item.getItemId()){
		case R.id.new_entry:
			chooseEntrySource();
			break;
		case R.id.new_database:
			newDatabase();
			break;
		case R.id.open_database:
			openDatabase();
			break;
		case R.id.reload_database:
			reloadDatabase();
			break;
		case R.id.save_database:
			saveDatabase();
			break;
			
		case R.id.dropbox_sync:
			startSync(EratosUri.DROPBOX_PROTOCOL);
			break;
		case R.id.dropbox_export:
			startExport(EratosUri.DROPBOX_PROTOCOL);
			break;
		case R.id.dropbox_revert:
			startRevert(EratosUri.DROPBOX_PROTOCOL);
			break;
		case R.id.dropbox_clear:
			startClearCache();
			break;
			
		case R.id.drive_sync:
			startSync(EratosUri.DRIVE_PROTOCOL);
			break;
		case R.id.drive_export:
			startExport(EratosUri.DRIVE_PROTOCOL);
			break;
		case R.id.drive_revert:
			startRevert(EratosUri.DRIVE_PROTOCOL);
			break;
		case R.id.drive_clear:
			startClearCache();
			break;
		case R.id.sort_random:
			notifySort(BibTeXContentProviderAdapter.SORT_RANDOM);
			break;
		case R.id.sort_date:
			notifySort(BibTeXContentProviderAdapter.SORT_DATE_ADDED);
			break;
		case R.id.sort_modified:
			notifySort(BibTeXContentProviderAdapter.SORT_DATE_MODIFIED);
			break;
		case R.id.sort_collection:
			notifySort(BibTeXContentProviderAdapter.SORT_COLLECTION);
			break;
		case R.id.sort_title:
			notifySort(BibTeXContentProviderAdapter.SORT_TITLE);
			break;
		case R.id.sort_author:
			notifySort(BibTeXContentProviderAdapter.SORT_AUTHOR);
			break;
		case R.id.sort_year:
			notifySort(BibTeXContentProviderAdapter.SORT_YEAR);
			break;
		case R.id.sort_source:
			notifySort(BibTeXContentProviderAdapter.SORT_SOURCE);
			break;
		case R.id.settings:
			i = new Intent(mContext, SettingsActivity.class);
			mContext.startActivity(i);
			break;
		case R.id.string_editor:
			i = new Intent(mContext, StringEditorActivity.class);
			mContext.startActivity(i);
			break;
		case R.id.wiki:
			String wikiUrl = mContext.getResources().getString(R.string.wiki_url);
			i = new Intent(Intent.ACTION_VIEW, Uri.parse(wikiUrl));
			mContext.startActivity(i);
			break;
		case R.id.help:
			i = new Intent(mContext, HelpActivity.class);
			mContext.startActivity(i);
			break;
		case R.id.about:
			showAbout();
			break;
		default:
			return false;
		}
		return true;
	}
	
	public void showAbout(){
		String message = mContext.getResources().getString(R.string.application_version_message);
		
		final SpannableString msg = new SpannableString(message);
		Linkify.addLinks(msg, Linkify.WEB_URLS);
	    
	    final AlertDialog d = new AlertDialog.Builder(mContext)
	        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					EratosApplication.getApplication().markRun();
				}
			})
			.setCancelable( false )
			.setIcon( android.R.drawable.ic_menu_info_details )
			.setTitle( "Notification" )
	        .setMessage( msg )
	        .create();

	    d.show();

	    // Make the textview clickable. Must be called after show()
	    ((TextView)d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
	}

	public void newDatabase() {
		EratosApplication.getApplication().confirmOverwrite(mContext, new ConfirmCallback() {
			public void confirmed() {
				Intent i = FileBrowserActivity.getLocation(mContext);
				mContext.startActivityForResult(i, REQUEST_NEW_LIBRARY_LOCATION);
			}
		});
	}

	public void openDatabase() {
		EratosApplication.getApplication().confirmOverwrite(mContext, new ConfirmCallback() {
			public void confirmed() {
				Intent i = FileBrowserActivity.getFile(mContext, "bib");
				mContext.startActivityForResult(i, REQUEST_OPEN_LIBRARY);
			}
		});
	}
	
	public void reloadDatabase(){
		 if(!EratosApplication.getApplication().isOpen()){
			 NotificationUtils.notify(mContext, "No Open Database", "You have not opened a database yet.");
		 }else{
			 EratosApplication.getApplication().confirmOverwrite(mContext, new ConfirmCallback() {
				 public void confirmed() {
						doReloadDatabase();
					}
			 });
		 }
	}
	
	public void saveDatabase(){
		if(EratosApplication.getApplication().getCurrentLibrary()==null){
			NotificationUtils.notify(mContext, "No Open Database", "You have not opened or created a database yet.");
		}else{
			SaveDatabaseTask.saveDatabase(mContext, new FileSyncResultReceiver(mContext));
		}
	}
	
	private void notifySort(int sortColumn) {
		EratosSettings settingsManager = EratosApplication.getApplication().getSettingsManager();
		setSelectedSort(sortColumn);
		
		int defaultDir = getDefaultDir(sortColumn);
		int dir = defaultDir;
		
		int lastDir = settingsManager.getSortDirection();
		int lastSort = settingsManager.getSortOrder();
		
		if(lastSort == sortColumn && lastDir == defaultDir)
			dir = 1 - defaultDir;
		
		settingsManager.setSortMode(sortColumn, dir);
		sortCallback.sortSelected(sortColumn, dir);
	}

	private void setSelectedSort(int sortColumn) {
		sortRandomMenuItem.setChecked(false);
		sortDateAddedMenuItem.setChecked(false);
		sortDateModifiedMenuItem.setChecked(false);
		sortCollectionMenuItem.setChecked(false);
		sortTitleMenuItem.setChecked(false);
		sortAuthorMenuItem.setChecked(false);
		sortYearMenuItem.setChecked(false);
		sortSourceMenuItem.setChecked(false);
		
		for(MenuItem item : customSortMenuItems)
			item.setChecked(false);
		getSortMenuItem(sortColumn).setChecked(true);
	}
	
	private MenuItem getSortMenuItem(int sortColumn) {
		switch(sortColumn) {
		case BibTeXContentProviderAdapter.SORT_RANDOM:
			return sortRandomMenuItem;
		case BibTeXContentProviderAdapter.SORT_DATE_ADDED:
			return sortDateAddedMenuItem;
		case BibTeXContentProviderAdapter.SORT_DATE_MODIFIED:
			return sortDateModifiedMenuItem;
		case BibTeXContentProviderAdapter.SORT_COLLECTION:
			return sortCollectionMenuItem;
		case BibTeXContentProviderAdapter.SORT_TITLE:
			return sortTitleMenuItem;
		case BibTeXContentProviderAdapter.SORT_AUTHOR:
			return sortAuthorMenuItem;
		case BibTeXContentProviderAdapter.SORT_YEAR:
			return sortYearMenuItem;
		case BibTeXContentProviderAdapter.SORT_SOURCE:
			return sortSourceMenuItem;
		}
		
		if((sortColumn & BibTeXContentProviderAdapter.CUSTOM_SORT_BIT) == BibTeXContentProviderAdapter.CUSTOM_SORT_BIT){
			int pos = sortColumn - BibTeXContentProviderAdapter.CUSTOM_SORT_BIT;
			return customSortMenuItems.get(pos);
		}
		
		return null;
	}

	private int getDefaultDir(int sortColumn) {
		switch(sortColumn){
		case BibTeXContentProviderAdapter.SORT_DATE_ADDED:
		case BibTeXContentProviderAdapter.SORT_DATE_MODIFIED:
		case BibTeXContentProviderAdapter.SORT_YEAR:
			return BibTeXContentProviderAdapter.SORT_DESCENDING;
		default:
			return BibTeXContentProviderAdapter.SORT_ASCENDING;
		}
	}

	private void export(final Intent data) {
		EratosApplication.getApplication().confirmOverwrite(mContext, new ConfirmCallback() {
			public void confirmed() {
				EratosUri remoteUri = EratosUri.parseUri( data.getStringExtra(FileBrowserActivity.PATH_ARG) );
				EratosUri localDBUri = EratosApplication.getApplication().getCurrentLibrary();
				ProtocolHandler protocolHandler = ProtocolHandler.getProtocolHandler(remoteUri);
				new ProtocolExportTask(protocolHandler, mContext, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, localDBUri, remoteUri);
			}
		});
	}
	
	public void sync() {
		boolean readOnly = EratosApplication.getApplication().getSettingsManager().syncReadOnly();
		if(readOnly)
			doSync(readOnly);
		else{
			EratosApplication.getApplication().confirmOverwrite(mContext, new ConfirmCallback() {
				public void confirmed() {
					doSync(false);
				}
			});
		}
	}
	
	public void doSync(boolean readOnly) {
		EratosUri libraryUri = EratosApplication.getApplication().getCurrentLibrary();
		ProtocolHandler protocolHandler = ProtocolHandler.getProtocolHandler(libraryUri);
		String bibType = EratosApplication.getApplication().getCurrentLibraryType();
		String encoding = EratosApplication.getApplication().getCurrentLibraryEncoding();
		new ProtocolSyncTask(protocolHandler, mContext, libraryUri, bibType, encoding, false, false, readOnly, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}

	private void revert() {
		NotificationUtils.confirmDialog(mContext, "Warning",  "Changes since last sync will be lost. Continue?", 
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						EratosUri libraryUri = EratosApplication.getApplication().getCurrentLibrary();
						ProtocolHandler protocolHandler = ProtocolHandler.getProtocolHandler(libraryUri);
						String bibType = EratosApplication.getApplication().getCurrentLibraryType();
						String encoding = EratosApplication.getApplication().getCurrentLibraryEncoding();
						new ProtocolSyncTask(protocolHandler, mContext, libraryUri, bibType, encoding, false, true, false, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
					}
				});
	}
	
	private void startClearCache(){
		NotificationUtils.confirmDialog(mContext, "Clear Cache?", "Clearing the cache may delete local file changes that haven't been synced, are you sure you want to continue?", 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						clearCache();
					}
				});
	}

	private void clearCache() {
		new ProtocolClearCacheTask(mContext, new ProgressCallback() {
			public void update(String stage, int progress, int maxProgress) {
			}
			public void finished() {
				actionCallback.onCacheCleared();
			}
			public void error(Throwable error) {
			}
		}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}
	
	private void newLibrary(Intent data) {
		final EratosUri directoryUri = EratosUri.parseUri( data.getStringExtra(FileBrowserActivity.PATH_ARG) );
		final ArrayList<String> listing = data.getStringArrayListExtra(FileBrowserActivity.EXISTING_FILES_ARG);
		
		NotificationUtils.promptInput(mContext, "New Library Filename", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				if(!result.endsWith(".bib"))
					result += ".bib";
				if(listing.contains(result)){
					NotificationUtils.notify(mContext, "Error", "A file with that name already exists!");
					return false;
				}
				
				final String filename = result;
				final EratosUri libraryUri = directoryUri.append(filename);
				
				InputUtils.chooseLibraryType(mContext, libraryUri, new IStringArrayResultListener() {
					@Override
					public boolean onResult(DialogInterface dialog, String ... results) {
						boolean isRemote = libraryUri.isRemoteProtocol(EratosApplication.getApplication());
						final String fBibType = results[0];
						final String fEncoding = results[1];
						
						if(isRemote) {
							final ProtocolHandler pHandler = ProtocolHandler.getProtocolHandler(libraryUri);
							pHandler.createFile(libraryUri, new AbstractProgressFileOperationResultListener(mContext) {
								@Override
								protected void success(FileResult result) {
									new ProtocolImportTask(pHandler, mContext, libraryUri, fBibType, fEncoding, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
								}
								@Override
								protected void failure(Throwable error) {
									NotificationUtils.notifyException(mContext, "Failure", error);
								}
							});
						} else {
							boolean success = newLocalLibrary(directoryUri, filename);
							if(success)
								new LoadLibraryTask(mContext, libraryUri, fBibType, fEncoding, true, false, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
							else
								NotificationUtils.notify(mContext, "Failure", "Could not create file: " + filename);
						}
						
						return true;
					}
				});
				return true;
			}
		});
	}

	protected boolean newLocalLibrary(EratosUri libraryUri, String filename) {
		try{
			EratosUri libFileUri = libraryUri.append(filename); 
			File file = FileManager.getFileManager().getFile(libFileUri);
			File parent = file.getParentFile();
			if(!parent.exists() && !parent.mkdirs())
				throw new IOException("Could not create parent directory: " + parent.getAbsolutePath());
			
			boolean success = file.createNewFile();
			if(!success)
				throw new IOException("Error creating file: " + file.getAbsolutePath());

			return true;
		}catch(Exception e){
			NotificationUtils.notifyException(mContext, "Error", e);
		}
		return false;
	}

	private void openLibrary(Intent data) {
		final EratosUri libraryUri = EratosUri.parseUri( data.getStringExtra(FileBrowserActivity.PATH_ARG) );
		InputUtils.chooseLibraryType(mContext, libraryUri, new IStringArrayResultListener() {
			@Override
			public boolean onResult(DialogInterface dialog, String ... results) {
				String bibType = results[0];
				String encoding = results[1];
				if(libraryUri.isRemoteProtocol(EratosApplication.getApplication()))
					loadRemoteLibrary(libraryUri, bibType, encoding);
				else
					loadLocalLibrary(libraryUri, bibType, encoding);
				
				return false;
			}
		});
	}
	
	public void onLibraryOpened(){
		EratosUri libraryUri = EratosApplication.getApplication().getCurrentLibrary();
		setMenuRemoteOptions(libraryUri);
		setAppSubtitle(libraryUri);
	}

	private void setMenuRemoteOptions(EratosUri libraryUri) {
		if(libraryUri == null){
			dropboxCategory.setVisible(false);
			driveCategory.setVisible(false);
			return;
		}
		
		String protocol = libraryUri.protocol();

		if(EratosUri.DROPBOX_PROTOCOL.equals(protocol)){
			driveCategory.setVisible(false);
			
			dropboxCategory.setVisible(true);
			dropboxRevertMenuItem.setVisible(true);
			dropboxSyncMenuItem.setVisible(true);
			dropboxClearCacheMenuItem.setVisible(true);
			dropboxExportMenuItem.setVisible(false);
		}else if(EratosUri.DRIVE_PROTOCOL.equals(protocol)){
			dropboxCategory.setVisible(false);
			
			driveCategory.setVisible(true);
			driveRevertMenuItem.setVisible(true);
			driveSyncMenuItem.setVisible(true);
			driveClearCacheMenuItem.setVisible(true);
			driveExportMenuItem.setVisible(false);
		}else{
			dropboxRevertMenuItem.setVisible(false);
			dropboxSyncMenuItem.setVisible(false);
			dropboxClearCacheMenuItem.setVisible(false);
			dropboxExportMenuItem.setVisible(true);
			
			driveRevertMenuItem.setVisible(false);
			driveSyncMenuItem.setVisible(false);
			driveClearCacheMenuItem.setVisible(false);
			driveExportMenuItem.setVisible(true);
			
			dropboxCategory.setVisible(true);
			driveCategory.setVisible(true);
		}
	}

	private void loadRemoteLibrary(EratosUri libraryUri, String bibType, String encoding) {
		ProtocolHandler pHandler = ProtocolHandler.getProtocolHandler(libraryUri);
		if(fileAdapter.exists(libraryUri) && fileAdapter.getFile(libraryUri).cached()){
			new ProtocolSyncTask(pHandler, mContext, libraryUri, bibType, encoding, true, false, false, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}else{
			new ProtocolImportTask(pHandler, mContext, libraryUri, bibType, encoding, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}
	}

	private void loadLocalLibrary(EratosUri libraryUri, String bibType, String encoding) {
		new LoadLibraryTask(mContext, libraryUri, bibType, encoding, true, false, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}
	
	private void doReloadDatabase(){
		EratosUri libraryUri = EratosApplication.getApplication().getCurrentLibrary();
		String bibType = EratosApplication.getApplication().getCurrentLibraryType();
		String encoding = EratosApplication.getApplication().getCurrentLibraryEncoding();
		new LoadLibraryTask(mContext, libraryUri, bibType, encoding, true, false, loaderListener).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}
	
	private void chooseEntrySource() {
		if(!EratosApplication.getApplication().isOpen()){
			NotificationUtils.notify(mContext, "No Open Database", "You must open or create a .bib file before adding entries");
			return;
		}
		
		List<CharSequence> items = new ArrayList<CharSequence>();
		items.add("Entry Creator");
		items.add("Bibtex");
		items.add("DOI");
		items.add("ISBN");
		items.add("PubMed");
		items.add("ArXiV");
		items.add("URL");
		items.add("Barcode Scanner");
		
		
		InputUtils.listChoiceDialog(mContext, "Choose Type", items.toArray(new String[]{}), new IStringResultListener() {
			@Override
			public boolean onResult(String result, DialogInterface dialog) {
				if(result.equals("Entry Creator"))
					createNewEntryEditor();
				else if(result.equals("Bibtex"))
					new BibtexEditorDialog(mContext);
				else if(result.equals("DOI"))
					getDOI();
				else if(result.equals("ISBN"))
					getISBN();
				else if(result.equals("PubMed"))
					getPubMed();
				else if(result.equals("URL"))
					getUrl();
				else if(result.equals("Barcode Scanner"))
					getBarcode();
				else if(result.equals("ArXiV"))
					getArXiV();
				
				return false;
			}
		});
	}
	
	protected void getBarcode() {
		IntentIntegrator scanIntegrator = new IntentIntegrator(mContext);
		scanIntegrator.initiateScan();
	}

	private void createNewEntryEditor() {
		Intent i = EditEntryActivity.newEntry(mContext);
		mContext.startActivity(i);
	}

	
	private void getPubMed() {
		NotificationUtils.promptInputNumber(mContext, "Enter PubMed ID", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if(! Validator.isPMID(result) ){
					NotificationUtils.notify(mContext, "Error", "Invalid PubMed ID format!");
					return false;
				}
				
				EratosUri uri = PubMedWebHandler.pubmedUrl.format(result);
				Intent i = new Intent(mContext, AddEntryFromPubMed.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(uri.toString()));
				mContext.startActivity(i);
				return true;
			}
		});
	}
	
	private void getArXiV() {
		NotificationUtils.promptInputDecimal(mContext, "Enter ArXiV ID", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if(! Validator.isArXiV(result) ){
					NotificationUtils.notify(mContext, "Error", "Invalid ArXiV ID format!");
					return false;
				}
				
				EratosUri uri = ArXiVHandler.absUrl.format(result);
				Intent i = new Intent(mContext, AddEntryFromArXiV.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(uri.toString()));
				mContext.startActivity(i);
				return true;
			}
		});
	}
	
	private void getUrl(){
		NotificationUtils.promptInput(mContext, "Enter URL", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				EratosUri uri = EratosUri.parseUri(result);
				if(uri == null && !result.matches("^[a-zA-Z]+://.*"))
					uri = EratosUri.parseUri("http://" + result);
				
				if( uri == null ){
					NotificationUtils.notify(mContext, "Error", "Invalid URL!");
					return false;
				}
				
				AbstractWebHandler handler = WebHandlerFactory.getHandler(uri);
				if(handler == null){
					NotificationUtils.notify(mContext, "Unrecognized URL", "Eratosthenes does not currently have a handler for that URL");
					return false;
				}
				
				Intent i = new Intent(mContext, AddEntryWebHandlerSearchActivity.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(uri.toString()));
				mContext.startActivity(i);
				return true;
			}
		});
	}
	
	private void getISBN() {
		NotificationUtils.promptInputNumber(mContext, "Enter ISBN", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if(! Validator.isISBN(result) ){
					NotificationUtils.notify(mContext, "Error", "Invalid ISBN format!");
					return false;
				}
				
				Intent i = new Intent(mContext, AddEntryFromISBN.class);
				i.setAction(Intent.ACTION_VIEW);
				i.putExtra(AddEntryFromISBN.ISBN_VALUE, result);
				mContext.startActivity(i);
				return true;
			}
		});
	}

	private void getDOI() {
		NotificationUtils.promptInput(mContext, "Enter DOI", new IStringResultListener() {
			public boolean onResult(String result, DialogInterface dialog) {
				result = result.trim();
				if( ! Validator.isDOI(result) ){
					NotificationUtils.notify(mContext, "Error", "Invalid DOI format!");
					return false;
				}
				
				String url = DOIWebHandler.doiUrl.append(result).toString();
				Intent i = new Intent(mContext, AddEntryFromDOI.class);
				i.setAction(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				mContext.startActivity(i);
				return true;
			}
		});
	}
	

	private void processBarcode(IntentResult result) {
		String content = result.getContents();
		String format = result.getFormatName();
		
		if(format.equals("EAN_13") && content.matches("^[0-9]+$")){
			Intent i = new Intent(mContext, AddEntryFromISBN.class);
			i.setAction(Intent.ACTION_VIEW);
			i.putExtra(AddEntryFromISBN.ISBN_VALUE, content);
			mContext.startActivity(i);
		}else if(format.equals("QR_CODE") && content.toLowerCase(Locale.US).startsWith("http://dx.doi.org/10.")){
			Intent i = new Intent(mContext, AddEntryFromDOI.class);
			i.setAction(Intent.ACTION_VIEW);
			i.setData(Uri.parse(content));
			mContext.startActivity(i);
		}else{
			Exception e = new Exception("Unrecognized bar code format or content: " + format + " --> " + content);
			NotificationUtils.notifyException(mContext, "Unrecognized Code", e);
		}
	}

	private void startSync(String protocol) {
		if(fileAdapter.isEmpty()) {
			NotificationUtils.notify(mContext, "Error", "You have not opened any files from "+protocol+" yet!");
		} else {
			ProtocolConnectionManager connectionManager = ProtocolConnectionManager.getConnectionManager(protocol);
			connectionManager.connect(mContext,
					new ProtocolConnectionManager.ConnectionListenerNotifier(connectionManager, mContext){
						public void success() {
							sync();
						}
					});
		}
	}
	
	public void startExport(String protocol){
		EratosApplication app = EratosApplication.getApplication();
		if(app.getCurrentLibrary() == null){
			NotificationUtils.notify(mContext, "No Open Database", "You must open a .bib database before exporting it to " + protocol);
		}else if(app.getCurrentLibrary().isRemoteProtocol(app)){
			NotificationUtils.notify(mContext, "Already In " + StringUtils.capitalize(protocol), "This library has already been moved to " + protocol);
		}else{
			Intent i = FileBrowserActivity.getLocation(mContext, protocol, true);
			mContext.startActivityForResult(i, REQUEST_REMOTE_LOCATION);
		}
	}
	
	public void startRevert(String protocol){
		ProtocolConnectionManager connectionManager = ProtocolConnectionManager.getConnectionManager(protocol);
		connectionManager.connect(mContext,
			new ProtocolConnectionManager.ConnectionListenerNotifier(connectionManager, mContext){
				public void success() {
					revert();
				}
			});
	}
	
	public void onResume() {
	    ProtocolConnectionManager.notifyOnResume(mContext);
	    init();
	}
	
	public void onPause() {
		ProtocolConnectionManager.notifyOnPause(mContext);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		ProtocolConnectionManager.notifyOnActivityResult(mContext, requestCode, resultCode, data);
		
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
		
		if (scanningResult != null && !scanningResult.failed()) {
			processBarcode(scanningResult);
		}else if(requestCode == REQUEST_OPEN_LIBRARY && resultCode == Activity.RESULT_OK){
			actionCallback.onLibraryOpened();
			openLibrary(data);
		}else if(requestCode == REQUEST_REMOTE_LOCATION && resultCode == Activity.RESULT_OK){
			export(data);
		}else if(requestCode == REQUEST_NEW_LIBRARY_LOCATION && resultCode == Activity.RESULT_OK){
			newLibrary(data);
		}
	}
}
