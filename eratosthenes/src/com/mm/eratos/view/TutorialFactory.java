package com.mm.eratos.view;
import android.content.Context;
import android.util.SparseArray;

import com.mm.eratos.R;

public class TutorialFactory
{
	public static String getHelpPageName(Context mContext, int sheetNumber){
		switch(sheetNumber){
			case 0:
				return mContext.getResources().getString(R.string.db_help_title);
			case 1:
				return mContext.getResources().getString(R.string.entry_help_title);
			case 2:
				return mContext.getResources().getString(R.string.edit_help_title);
			case 3:
				return mContext.getResources().getString(R.string.io_help_title);
			case 4:
				return mContext.getResources().getString(R.string.compatibility_help_title);
		}
		return "";
	}
	
	public static String getHelpPageText(Context mContext, int sheetNumber){
		switch(sheetNumber){
			case 0:
				return mContext.getResources().getString(R.string.db_help);
			case 1:
				return mContext.getResources().getString(R.string.entry_help);
			case 2:
				return mContext.getResources().getString(R.string.edit_help);
			case 3:
				return mContext.getResources().getString(R.string.io_help);
			case 4:
				return mContext.getResources().getString(R.string.compatibility_help);
		}
		return "";
	}

	public static SparseArray<String> getIconCheatSheet(int sheetNumber)
	{
		SparseArray<String> resources = new SparseArray<String>();

		switch(sheetNumber){
			case 0:
				resources.put(R.drawable.content_new, "Create/Import new entry");
				resources.put(R.drawable.action_search, "Search database");
				resources.put(R.drawable.collections_sort, "Select entry sorting method");
				resources.put(R.drawable.collections_view_as_list, "Show the database menu (new/open/save)");
				resources.put(R.drawable.content_dropbox, "Show the dropbox menu (sync/export/revert)");
				resources.put(R.drawable.action_settings, "View/change application preferences");
				break;
			case 1:
				resources.put(R.drawable.content_discard, "Remove the selected entries");
				resources.put(R.drawable.rating_important, "Important entry");
				resources.put(R.drawable.collections_mark_read, "Mark entries as read");
				resources.put(R.drawable.collections_new_label, "Add a keyword to entries");
				resources.put(R.drawable.collections_group_new, "Add entries to a group");
				resources.put(R.drawable.content_select_all, "Assign entries to collection");
				break;
			case 2:
				resources.put(R.drawable.content_edit, "Open the guided entry editor, long press to open the raw bibtex editor");
				resources.put(R.drawable.action_search, "Search for this entry on google scholar (by title)");
				resources.put(R.drawable.rating_important, "Important entry");
				resources.put(R.drawable.ic_action_attachment, "Open the primary attachment");
				resources.put(R.drawable.content_new_attachment, "Add a new attachment");
				resources.put(android.R.drawable.ic_menu_close_clear_cancel, "Remove keyword, attachment, url or field");
				resources.put(android.R.drawable.ic_menu_set_as, "Parse link for additional entry metadata");
				resources.put(R.drawable.content_new, "Add a new element");
				resources.put(R.drawable.collections_new_label, "Add a new field to the BibTeX entry");
				break;
			case 3:
				resources.put(R.drawable.content_cite, "Generate a LaTeX cite command and copy it to the clipboard");
				resources.put(R.drawable.content_import_export, "Export entries to a file");
				resources.put(R.drawable.social_share, "Share entries with a colleague using another application, such as e-mail");
				break;
		}

		return resources;
	}
}
