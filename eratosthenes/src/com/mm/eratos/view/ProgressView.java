package com.mm.eratos.view;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mm.eratos.R;

public class ProgressView extends LinearLayout {
	private static final String SUPERSTATE="superState";
	
	private TextView progressMessage;
	private ProgressBar progressBar;
	private TextView progressPercent;
	private TextView progressFraction;
	  
	public ProgressView(Context context) {
		super(context);
		init(null);
	}

	public ProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public ProgressView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}
	
	private void init(AttributeSet attrs) {
		LayoutInflater inflater=null;

		if (getContext() instanceof Activity) {
	    	inflater=((Activity)getContext()).getLayoutInflater();
	    } else {
	    	inflater=LayoutInflater.from(getContext());
	    }

		inflater.inflate(R.layout.progress_bar_item, this, true);
		
		progressMessage = (TextView) findViewById(R.id.message);
		progressBar = (ProgressBar) findViewById(R.id.progress);
		progressPercent = (TextView) findViewById(R.id.percent);
		progressFraction = (TextView) findViewById(R.id.fraction);
	}

	public void setMessage(String message){
		if(message  == null)
			progressMessage.setVisibility(View.GONE);
		else{
			progressMessage.setText(message);
			progressMessage.setVisibility(View.VISIBLE);
		}
	}
	
	public void setMessage(Spanned message){
		if(message  == null)
			progressMessage.setVisibility(View.GONE);
		else{
			progressMessage.setText(message);
			progressMessage.setVisibility(View.VISIBLE);
		}
	}
	
	public void setProgress(int progress, int max){
		if(max == 0){
			progressBar.setIndeterminate(true);
			progressPercent.setText("");
			progressFraction.setText("");
		}else{
			progressBar.setIndeterminate(false);
			progressBar.setMax(max);
			progressBar.setProgress(progress);
			
			int percent = (int)(100.0f * ((float) progress) / ((float) max));
			progressPercent.setText(String.format(Locale.US, "%d %%", percent));
			progressFraction.setText(String.format(Locale.US, "%d / %d", progress, max));
		}
	}

	public void setFailed() {
		progressBar.setIndeterminate(false);
		progressBar.setMax(100);
		progressBar.setProgress(0);
		progressPercent.setText("");
		progressFraction.setText("");
	}
	
	public void setFinished(){
		progressBar.setIndeterminate(false);
		progressBar.setMax(100);
		progressBar.setProgress(100);
		progressPercent.setText("100 %");
		progressFraction.setText("");
	}
	
	@Override
	public Parcelable onSaveInstanceState() {
		Bundle state = new Bundle();
		state.putParcelable(SUPERSTATE, super.onSaveInstanceState());
		return (state);
	}
	
	@Override
	public void onRestoreInstanceState(Parcelable ss) {
		Bundle state = (Bundle) ss;
		super.onRestoreInstanceState(state.getParcelable(SUPERSTATE));
	}
}
