package com.mm.eratos.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.eratos.bibtex.BibTeXContentProviderAdapter;
import com.mm.eratos.bibtex.operations.RemoveCategoryTransaction;
import com.mm.eratos.fragment.EntryListFragment;
import com.mm.eratos.utils.NotificationUtils;

public class RemoveTargetDragListener implements OnDragListener {
	private final TextView dropTarget;
	private String selectedItem;
	private String selectedCategory;
	private Activity mContext;
	private BibTeXContentProviderAdapter bibtexAdapter;
	private boolean disabled;
	
	public RemoveTargetDragListener(Activity mContext, TextView dropTarget){
		this.mContext = mContext;
		this.dropTarget = dropTarget;
		this.disabled = false;
		bibtexAdapter = new BibTeXContentProviderAdapter(mContext.getContentResolver());
	}
	
	public void setSelectedCategory(String type, String item){
		selectedCategory = type;
		selectedItem = item;
	}
	
	@Override
	public boolean onDrag(View v, DragEvent event) {
		if(event.getAction() == DragEvent.ACTION_DRAG_STARTED)
			handleStart(event);
		else if(event.getAction() == DragEvent.ACTION_DRAG_ENTERED)
			handleEnter(event);
		else if(event.getAction() == DragEvent.ACTION_DRAG_EXITED)
			handleExit(event);
		else if(event.getAction() == DragEvent.ACTION_DRAG_ENDED)
			handleEnd(event);
		else if(event.getAction() == DragEvent.ACTION_DROP)
			handleDrop(event);
		
		return true;
	}

	private boolean handleDrop(DragEvent event) {
		if( EntryListFragment.ENTRY_CLIP_LABEL.equals(event.getClipDescription().getLabel()) ){
			confirmRemove(event);
		}
		return true;
	}

	private void confirmRemove(DragEvent event) {
		final List<String> refKeys = getRefKeysFromClip(event);
		
		NotificationUtils.confirmDialog(mContext, 
				String.format(Locale.US, "Remove Entries from %s?", selectedItem), 
				String.format(Locale.US, "%d entries will be removed", event.getClipData().getItemCount()),
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try{
					removeEntriesFromCategory(selectedCategory, selectedItem, refKeys);
				}catch(Exception e){
					NotificationUtils.notifyException(mContext, "Error Removing Entries", e);
				}
			}
		});
	}

	private List<String> getRefKeysFromClip(DragEvent event) {
		List<String> refKeys = new ArrayList<String>(event.getClipData().getItemCount());
		for(int i = 0; i < event.getClipData().getItemCount(); i++){
			String refKey = event.getClipData().getItemAt(i).getText().toString();
			refKeys.add(refKey);
		}
		return refKeys;
	}
	
	private void removeEntriesFromCategory(String categoryName, String category, List<String> refKeys) throws Exception {
		new RemoveCategoryTransaction(mContext, bibtexAdapter, categoryName, category).executeOnKeys(refKeys);
		NotificationUtils.longToast(mContext, String.format(Locale.US, "Removed %d entries from '%s'", refKeys.size(), category));
	}
	
	private void handleEnd(DragEvent event) {
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)dropTarget.getLayoutParams();
		params.setMargins(0, 200, 0, 0);
		dropTarget.setLayoutParams(params);
		dropTarget.setActivated(false);
	}

	private void handleExit(DragEvent event) {
		dropTarget.setActivated(false);
	}

	private void handleEnter(DragEvent event) {
		dropTarget.setActivated(true);
	}

	private void handleStart(DragEvent event) {
		if( !disabled && EntryListFragment.ENTRY_CLIP_LABEL.equals(event.getClipDescription().getLabel()) ){
			dropTarget.setText(String.format(Locale.US, "Remove from '%s'", selectedItem));
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)dropTarget.getLayoutParams();
			params.setMargins(0, 0, 0, 0);
			dropTarget.setLayoutParams(params);
		}
	}

	public void setDisabled() {
		disabled=true;
	}

	public void setEnabled() {
		disabled=false;
	}
}