package com.mm.eratos.view;

import com.mm.eratos.R;
import com.mm.eratos.adapter.StatefulItemAdapter.StatefulView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.RelativeLayout;

public class GroupListItem extends RelativeLayout implements Checkable, IMarkable, StatefulView<Integer> {
	public static final int STATE_NONE = 0;
	public static final int STATE_CHOSEN = 1;
	
	private static final int[] CheckedStateSet = {
	    android.R.attr.state_checked
	};
	private static final int[] MarkedStateSet = {R.attr.state_marked};
	private static final int[] ChosenStateSet = {R.attr.state_chosen};
	
	boolean checked = false;
	boolean marked = false;
	int state = 0;
	
	
	public GroupListItem(Context context) {
		super(context);
	}
	
	public GroupListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public GroupListItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	public boolean isChecked() {
		return checked;
	}
	
	@Override
	public void setChecked(boolean checked) {
		this.checked = checked;
		refreshDrawableState();
	}
	
	@Override
	public void toggle() {
		checked = !checked;
		refreshDrawableState();
	}
	
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
	    final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
	    if (isChecked()) {
	        mergeDrawableStates(drawableState, CheckedStateSet);
	    }
	    if (isMarked()) {
	        mergeDrawableStates(drawableState, MarkedStateSet);
	    }
	    if ((state & STATE_CHOSEN) == STATE_CHOSEN) {
	        mergeDrawableStates(drawableState, ChosenStateSet);
	    }
	    return drawableState;
	}

	@Override
	public Integer getState() {
		return state;
	}

	@Override
	public void setState(Integer state) {
		this.state = state;
		refreshDrawableState();
	}

	@Override
	public boolean isMarked() {
		return marked;
	}

	@Override
	public void setMarked(boolean marked) {
		this.marked = marked;
		refreshDrawableState();
	}

	@Override
	public void toggleMarked() {
		this.marked = !marked;
		refreshDrawableState();
	}
}
