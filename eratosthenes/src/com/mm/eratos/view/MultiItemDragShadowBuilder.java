package com.mm.eratos.view;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.DragShadowBuilder;

public class MultiItemDragShadowBuilder extends DragShadowBuilder {
	private static Drawable shadow;
	static{
		shadow = new ColorDrawable(Color.LTGRAY);
	}
	private static int fontSize = 40;
	
	private int width, height;
	private String text;
	
	public MultiItemDragShadowBuilder(View v, List<String> selectedKeys) {
		super(v);
		text = String.format("%d Entries", selectedKeys.size());
	}

    @Override
    public void onProvideShadowMetrics (Point size, Point touch) {
        width = getView().getWidth();
        height = getView().getHeight();

        shadow.setBounds(0, 0, width, height);
        size.set(width, height);

        touch.set(width / 2, height / 2);
    }

    @Override
    public void onDrawShadow(Canvas canvas) {
        shadow.draw(canvas);
        
        Paint textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(fontSize);
        textPaint.setTypeface(Typeface.DEFAULT_BOLD);
        
        canvas.drawText(text, fontSize, height / 2 + fontSize / 3, textPaint);
    }
}
