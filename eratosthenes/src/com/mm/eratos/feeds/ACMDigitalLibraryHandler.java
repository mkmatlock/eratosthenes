package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebUtils;

public class ACMDigitalLibraryHandler extends AbstractWebHandler {
	protected static String expHost = "dl.acm.org";
	protected static String expPrefix = "/citation.cfm";
	
	protected static EratosUri bibtexUri = EratosUri.parseUri("http://dl.acm.org/exportformats.cfm?id=%s&expformat=bibtex");
	protected static EratosUri abstractUri = EratosUri.parseUri("http://dl.acm.org/tab_abstract.cfm?id=%s");
	
	@Override
	public boolean handles(EratosUri uri) {
		return (uri.protocol().equals("http") || uri.protocol().equals("https"))
				&& uri.path().startsWith(expHost + expPrefix);
	}
	
	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String id = getDocumentId(uri);
		Document doc = WebClient.newInstance().queryHtmlPage(bibtexUri.format(id));
		
		return WebUtils.makeStream( doc.select("PRE").text() );
	}

	protected String getDocumentId(EratosUri uri) throws UnsupportedEncodingException {
		Map<String, String> query = WebUtils.mapQuery(uri);
		String id = query.get("id");
		if(id.contains(".")){
			String[] items = id.split("\\.");
			id = items[1];
		}
		return id;
	}
	
	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = abstractUri.format(getDocumentId(uri));
		Document doc = WebClient.newInstance().queryHtmlPage(uri);
		String summary = doc.select("div").text();
		entry.setSummary(summary);
	}
	
	
	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException {
		Document doc = WebClient.newInstance().queryHtmlPage(uri);
		Elements e = doc.select("a[name=FullTextPDF]");
		if(e.size()>0){
			String href = e.get(0).attr("href");
			return EratosUri.makeUri("http", expHost + "/" + href);
		}
		return null;
	}
}
