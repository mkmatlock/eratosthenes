package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class ACSWebHandler extends AbstractWebHandler {
	public static EratosUri ACSAbstractURL = EratosUri.parseUri("http://pubs.acs.org/doi/abs");
	public static EratosUri ACSPdfURL = EratosUri.parseUri("http://pubs.acs.org/doi/pdf");
		
	@Override
	public boolean handles(EratosUri uri) {
		return uri.protocol().equals("http") && uri.path().startsWith("pubs.acs.org/doi/abs/");
	}

	protected String getDOI(EratosUri uri) {
		List<String> path = uri.slice(uri.length() - 2);
		return String.format(Locale.US, "%s/%s", path.get(0), path.get(1));
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}
	
	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		EratosUri entryUri = ACSAbstractURL.append(doi);
		
		Document html = WebClient.newInstance().queryHtmlPage(entryUri);
		Element element = html.getElementById("abstractBox");
		if(element != null)
			entry.setSummary(element.text());
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		return ACSPdfURL.append(doi);
	}
}
