package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class OxfordJournalsWebHandler extends AbstractWebHandler {
	protected static final String matchPattern = "([a-zA-Z0-9\\.]+\\.)?oxfordjournals.org/content/([A-Za-z0-9]+/[A-Za-z0-9]+/[A-Za-z0-9]+)(.short|.full|.html)?";
	protected static final String matchPattern2 = "([a-zA-Z0-9\\.]+\\.)?oxfordjournals.org/content/early/([A-Za-z0-9]+/[A-Za-z0-9]+/[A-Za-z0-9]+/[A-Za-z0-9\\.]+?)(.short|.full|.html)?";
	
	protected static final EratosUri bioinfoUri = EratosUri.parseUri("http://%soxfordjournals.org/content/%s.short");
	protected static final EratosUri bioinfoPdf = EratosUri.parseUri("http://%soxfordjournals.org/content/%s.full.pdf");
	
	protected static final EratosUri bioinfoEarlyUri = EratosUri.parseUri("http://%soxfordjournals.org/content/early/%s.short");
	protected static final EratosUri bioinfoEarlyPdf = EratosUri.parseUri("http://%soxfordjournals.org/content/early/%s.full.pdf");

	public EratosUri trimUri(EratosUri uri) {
		Matcher m = uri.extract(matchPattern);
		if(m.matches()){
			String subdomain = m.group(1);
			if(subdomain.startsWith("m."))
				subdomain = subdomain.substring(2);
			String contentId = m.group(2);
			return bioinfoUri.format(subdomain, contentId);
		}
		
		m = uri.extract(matchPattern2);
		if(m.matches()){
			String subdomain = m.group(1);
			if(subdomain.startsWith("m."))
				subdomain = subdomain.substring(2);
			String contentId = m.group(2);
			return bioinfoEarlyUri.format(subdomain, contentId);
		}
		
		return null;
	}

	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(matchPattern) || uri.matches(matchPattern2);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		uri = trimUri(uri);
		System.out.println("Query: " + uri);
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		String doi = html.select("div#slugline span.slug-doi").text();
		
		System.out.println("Got DOI: " + doi);
		return getBibtexByDOI(doi);
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = trimUri(uri);
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		String summary = html.select("div#abstract-1 p").text();
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		Matcher m = uri.extract(matchPattern);
		if(m.matches()){
			String subdomain = m.group(1);
			if(subdomain.startsWith("m."))
				subdomain = subdomain.substring(2);
			String contentId = m.group(2);
			return bioinfoPdf.format(subdomain, contentId);
		}
		
		m = uri.extract(matchPattern2);
		if(m.matches()){
			String subdomain = m.group(1);
			if(subdomain.startsWith("m."))
				subdomain = subdomain.substring(2);
			String contentId = m.group(2);
			return bioinfoEarlyPdf.format(subdomain, contentId);
		}
		
		return null;
	}
}
