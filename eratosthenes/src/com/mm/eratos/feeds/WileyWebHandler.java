package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class WileyWebHandler extends AbstractWebHandler {
	// final arg: (abstract|full|references|citedby|pdf)
	final static EratosUri wileyOnlineUri = EratosUri.parseUri("http://onlinelibrary.wiley.com/doi");
	final static EratosUri wileyOnlineAbs = EratosUri.parseUri("http://onlinelibrary.wiley.com/doi/%s/abstract");
	final static EratosUri wileyOnlinePdf = EratosUri.parseUri("http://onlinelibrary.wiley.com/doi/%s/pdf");
	final static String matchPattern = "onlinelibrary.wiley.com/doi/(.*)/(abstract|full|references|citedby|pdf)";
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.childOf(wileyOnlineUri);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	private String getDOI(EratosUri uri) throws IOException {
		Matcher m = uri.extract(matchPattern);
		if(! m.matches()) throw new IOException("DOI number could not be found!");
		return m.group(1);
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		
		EratosUri articleUri = wileyOnlineAbs.format(doi);
		Document htmlDoc = WebClient.newInstance().queryHtmlPage(articleUri);
		Elements select = htmlDoc.select("div#abstract div div p");
		if(select.size()>0)
			entry.setSummary(select.get(0).text());
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		return wileyOnlinePdf.format(doi);
	}

}
