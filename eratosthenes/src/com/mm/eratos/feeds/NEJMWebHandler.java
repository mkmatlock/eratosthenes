package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class NEJMWebHandler extends AbstractWebHandler {
	// (abs|full|pdf)
	EratosUri nejmRoot = EratosUri.parseUri("http://www.nejm.org/doi/");
	EratosUri nejmUri = EratosUri.parseUri("http://www.nejm.org/doi/abs");
	EratosUri nejmPdf = EratosUri.parseUri("http://www.nejm.org/doi/pdf");
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.childOf(nejmRoot);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	private void getArticleInfo(BibTeXEntryModel entry, String doi) throws IOException {
		EratosUri articleUri = nejmUri.append(doi);
		Document htmlDoc = WebClient.newInstance().queryHtmlPage(articleUri);
		Elements abstractElements = htmlDoc.select("dd#abstract div.left div.section");
		
		StringBuilder summaryText = new StringBuilder();
		for(Element e : abstractElements){
			Elements h3s = e.select("h3");
			Elements ps = e.select("p");
			
			if(h3s.size() > 0 && ps.size() > 0){
				String header = h3s.get(0).text();
				String text = ps.get(0).text();
				
				summaryText.append(header);
				summaryText.append("\n");
				summaryText.append(text);
				summaryText.append("\n\n");
			}
		}
		
		entry.setSummary(summaryText.toString().trim());
	}

	protected String getDOI(EratosUri uri) {
		List<String> path = uri.slice(uri.length() - 2);
		return String.format(Locale.US, "%s/%s", path.get(0), path.get(1));
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		getArticleInfo(entry, doi);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		EratosUri pdfUri = nejmPdf.append(doi);
		return pdfUri;
	}
}
