package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class JBCWebHandler extends AbstractWebHandler {
	String jbcPatternA = "www.jbc.org/content/.*";
	String jbcPatternB = "m.jbc.org/content/.*";
	Pattern jbcExtractId = Pattern.compile("(m|www).jbc.org/content/([0-9a-zA-Z/\\.]+)(\\.(short|full|abs|abstract|html))?");
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(jbcPatternA) || uri.matches(jbcPatternB);
	}
	
	private EratosUri trimUri(EratosUri uri) {
		Matcher m = jbcExtractId.matcher(uri.path());
		m.find();
		String id = m.group(2);
		return uri.host().append("content").append(id);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		uri = trimUri(uri);
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	protected String getDOI(EratosUri uri) throws IOException {
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		String doi = html.select("div#slugline span.slug-doi").text();
		return doi;
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = trimUri(uri);
		
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		String summary = html.select("div#abstract-2 p").text();
		entry.setSummary(summary);
	}
	
	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) {
		uri = trimUri(uri);
		
		EratosUri pdfUri = EratosUri.parseUri(uri.protocol() + "://" + uri.path() + ".full.pdf");
		return pdfUri;
	}

}
