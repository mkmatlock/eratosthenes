package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class PLOSWebHandler extends AbstractWebHandler {
	// http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.0010049
	final static String matchPattern = "www.plos[a-z]+.org/article/info:doi/(.*)";
	final static EratosUri plosCompBioUri = EratosUri.parseUri("http://%s/article/info:doi/%s");
	final static EratosUri plosCompBioPdf = EratosUri.parseUri("http://%s/article/fetchObject.action?uri=info:doi/%s&representation=PDF");
	
	protected String getDOI(EratosUri uri) throws IOException{
		Matcher m = uri.extract(matchPattern);
		if(! m.matches()) throw new IOException("DOI number could not be found!");
		return m.group(1);
	}
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(matchPattern);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String source = getSource(uri);
		String doi = getDOI(uri);
		
		EratosUri articleUri = plosCompBioUri.format(source, doi);
		Document htmlDoc = WebClient.newInstance().queryHtmlPage(articleUri);
		
		Elements e = htmlDoc.select("div.article div.abstract p");
		if(e.size()>0)
			entry.setSummary(e.get(0).text());
	}

	protected String getSource(EratosUri uri) {
		String source = uri.host().path();
		return source;
	}

	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException {
		String source = getSource(uri);
		String doi = getDOI(uri);
		
		EratosUri pdfUri = plosCompBioPdf.format(source, doi);
		return pdfUri;
	}

}
