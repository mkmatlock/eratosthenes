package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class SpringerLinkWebHandler extends AbstractWebHandler {
	
	String matchPattern  = "link.springer.com/article/(10.[0-9]{4}/[a-zA-Z0-9\\-\\_]+)";
	EratosUri articleUrl = EratosUri.parseUri("http://link.springer.com/article/%s");
	EratosUri pdfUrl 	 = EratosUri.parseUri("http://link.springer.com/content/pdf/%s.pdf");
	
	protected String getDOI(EratosUri uri) throws IOException{
		Matcher m = uri.extract(matchPattern);
		if(! m.matches()) throw new IOException("DOI number could not be found!");
		return m.group(1);
	}
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(matchPattern);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		
		EratosUri articleUri = articleUrl.format(doi);
		Document htmlDoc = WebClient.newInstance().queryHtmlPage(articleUri, true);
		
		String summary = htmlDoc.select("div.abstract-content").text();
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String doi = getDOI(uri);
		return pdfUrl.format(doi);
	}
}
