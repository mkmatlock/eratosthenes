package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class PMCWebHandler extends AbstractWebHandler {
	static String matchPattern = "www.ncbi.nlm.nih.gov/pmc/articles/(PMC[0-9]+|pmid/[0-9]+).*";
		
	static EratosUri articleUri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/%s");
	static EratosUri pdfUri = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pmc/articles/%s/pdf");
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(matchPattern);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String pmcid = getPMCID(uri);
		uri = articleUri.format(pmcid);
		
		String pmid = getPMID(uri);
		return new PubMedWebHandler().bibtex(PubMedWebHandler.pubmedUrl.format(pmid));
	}

	private String getPMID(EratosUri uri) throws IOException {
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		
		for(Element e : html.select("div#rightcolumn a.brieflinkpopperctrl")){
			String href = e.attr("href");
			String text = e.text();
			Matcher hrefMatcher = Pattern.compile("/pubmed/([0-9]+).*").matcher(href);
			
			if(hrefMatcher.find() && text.equals("PubMed"))
				return hrefMatcher.group(1);
		}
		throw new IOException("Unable to retrieve pubmed ID");
	}
	
	protected String getPMCID(EratosUri uri) {
		Matcher m = uri.extract(matchPattern);
		m.find();
		return m.group(1);
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String pmcid = getPMCID(uri);
		uri = articleUri.format(pmcid);
		
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		
		String summary = "";
		for(Element e : html.select("div")){
			if(e.attr("class").contains("summary") || e.id().startsWith("__abstractid")){
				summary = e.select("p").text();
				break;
			}
		}
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String pmcid = getPMCID(uri);
		return pdfUri.format(pmcid);
	}

}
