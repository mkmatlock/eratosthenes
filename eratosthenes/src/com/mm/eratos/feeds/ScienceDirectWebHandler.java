package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebUtils;

public class ScienceDirectWebHandler extends AbstractWebHandler {

	String matchPattern = "www.sciencedirect.com/science/article/pii/.*";
	Pattern doiPattern = Pattern.compile("SDM.doi = '([0-9a-zA-Z\\./\\-]+)';");
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(matchPattern);
	}
	
	protected String getDOI(EratosUri uri) throws IOException{
		String html = WebUtils.readStream(WebClient.newInstance().queryContent(uri, false).getContent());
		Matcher m = doiPattern.matcher(html);
		if(!m.find())
			throw new IOException("Could not retrieve DOI for sciencedirect entry");
		return m.group(1);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	private EratosUri convertToNP(EratosUri uri) {
		return uri.query("np=y");
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = convertToNP(uri);
		Document html = WebClient.newInstance().queryHtmlPage(uri, false);
		String summary = html.select("div.abstract p").text();
		entry.setSummary(summary);	
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = convertToNP(uri);
		Document html = WebClient.newInstance().queryHtmlPage(uri, false);
		
		Element e = html.select("a#pdfLink").first();
		EratosUri pdfUri = EratosUri.parseUri(e.attr("href"));
		return pdfUri;
	}

}
