package com.mm.eratos.feeds;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class ArXiVHandler extends AbstractWebHandler {

	public static final EratosUri queryUrl = EratosUri.parseUri("http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:%s.%s");
	public static final EratosUri absUrl = EratosUri.parseUri("http://arxiv.org/abs/%s");
	public static final EratosUri pdfUrl = EratosUri.parseUri("http://arxiv.org/pdf/%s");
	private static final String arxivPattern = "arxiv.org/[a-zA-Z]+/([0-9]+\\.[0-9]+)";
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.path().matches(arxivPattern);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String arxivnum = getArXiVNumber(uri);
		String nums[] = arxivnum.split("\\.");
		
		EratosUri query = queryUrl.format(nums[0],nums[1]);
		System.out.println(query);
		Document htmlPage = WebClient.newInstance().queryHtmlPage(query, false);
		for(Element e : htmlPage.select("a")){
			if(e.text().startsWith("Bibtex")){
				EratosUri biburl = EratosUri.parseUri(e.attr("href"));
				System.out.println(biburl);
				Document bibPage =  WebClient.newInstance().queryHtmlPage(biburl, false);
				String bib = bibPage.text();
				bib = bib.substring(bib.indexOf("@"));
				return new ByteArrayInputStream(bib.getBytes());
			}
		}
		return new ByteArrayInputStream(new byte[]{});
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri)
			throws IOException, ParseException, FieldRequiredException {
		String arxivnum = getArXiVNumber(uri);
		
		Document htmlPage = WebClient.newInstance().queryHtmlPage(absUrl.format(arxivnum), false);
		String summary = htmlPage.select("blockquote.abstract").text();
		if(summary.startsWith("Abstract:"))
			summary = summary.substring("Abstract:".length()).trim();
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		String arxivnum = getArXiVNumber(uri);
		return pdfUrl.format(arxivnum);
	}

	private String getArXiVNumber(EratosUri uri) throws IOException {
		Matcher extract = uri.extract(arxivPattern);
		if(!extract.matches())
			throw new IOException("URI not supported: " + uri);
		String arxivnum = extract.group(1);
		return arxivnum;
	}

}
