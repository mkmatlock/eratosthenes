package com.mm.eratos.feeds;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.jbibtex.ParseException;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebUtils;

public class DOIWebHandler extends AbstractWebHandler
{
	public static final EratosUri doiUrl = EratosUri.parseUri("http://dx.doi.org");
	
	public boolean handles(EratosUri uri)
	{
		return uri.protocol().equals(doiUrl.protocol()) &&
				uri.path().startsWith(doiUrl.path());
	}
	
	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		WebClient browser = WebClient.newInstance();
		Map<String, String> headers = WebUtils.makeHeaders("Accept", "application/x-bibtex");
        HttpEntity entity = browser.queryContent(uri, headers);
        return entity.getContent();
	}

	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException
	{
		EratosUri redirectUri = WebClient.newInstance().followRedirects(uri);
		entry.addExternalLink(redirectUri);
		
		AbstractWebHandler secondary = WebHandlerFactory.getHandler(redirectUri);
		if(secondary != null)
			secondary.extras(entry, redirectUri);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		EratosUri redirectUri = WebClient.newInstance().followRedirects(uri);

		AbstractWebHandler secondary = WebHandlerFactory.getHandler(redirectUri);
		if(secondary != null)
			return secondary.pdfUri(entry, redirectUri);
		
		return null;
	}
}
