package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class ScienceWebHandler extends AbstractWebHandler {
	String sciPatternA = "(www|m)\\.sciencemag\\.org/content/.*";
	String sciPatternB = "(m\\.)?[a-zA-Z0-9]+\\.sciencemag\\.org/content/[a-zA-Z0-9_]+/.*";
	
	EratosUri sciJournalUri = EratosUri.parseUri("http://www.sciencemag.org/content/%s");
	EratosUri sciSubJournalUri = EratosUri.parseUri("http://%s.sciencemag.org/content/%s/%s");
	
	Pattern sciExtractId = Pattern.compile("(m|www)\\.sciencemag\\.org/content/([0-9A-Za-z/]*)(\\.(short|full|abs|abstract|html|full\\.html|short\\.html))?");
	Pattern sciSubJournalExtractId = Pattern.compile("(m\\.)?([a-zA-Z0-9]+)\\.sciencemag\\.org/content/([a-zA-Z0-9_]+)/([0-9A-Za-z/]*)(\\.(short|full|abs|abstract|html|full\\.html|short\\.html))?");
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(sciPatternA) || uri.matches(sciPatternB);
	}
	
	protected EratosUri trimUri(EratosUri uri) {
		Matcher m = sciExtractId.matcher(uri.path());
		if(m.matches()){
			String id = m.group(2);
			return sciJournalUri.format(id);
		}
		
		m = sciSubJournalExtractId.matcher(uri.path());
		if(m.matches()){
			String dom = m.group(2);
			String subj = m.group(3);
			String id = m.group(4);
			return sciSubJournalUri.format(dom, subj, id);
		}
		
		return null;
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		uri = trimUri(uri);
		System.out.println(uri);
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}

	private String getDOI(EratosUri uri) throws IOException {
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		String doi = html.select("div#slugline span.slug-doi").text();
		return doi;
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = trimUri(uri);
		
		Document html = WebClient.newInstance().queryHtmlPage(uri);
		String summary = html.select("div#abstract-3 p").text();
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = trimUri(uri);
		
		return EratosUri.parseUri(uri.protocol() + "://" + uri.path() + ".full.pdf");
	}

}
