package com.mm.eratos.feeds;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.jbibtex.ParseException;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosSettings;
import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.files.FileManager;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.protocol.FileRevisionContentProviderAdapter;
import com.mm.eratos.utils.NotificationUtils;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;

public abstract class AbstractWebHandler {
	public abstract boolean handles(EratosUri uri);
	
	public abstract InputStream bibtex(EratosUri uri) throws IOException;
	public abstract void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException;
	
	public abstract EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException;
	
	protected InputStream getBibtexByDOI(String doi) throws IOException {
		EratosUri doiUri = DOIWebHandler.doiUrl.append(doi);
		return new DOIWebHandler().bibtex(doiUri);
	}


	public void downloadPdf(BibTeXEntryModel entry, EratosUri pdfUri, DownloadListener listener) {
		EratosApplication app = EratosApplication.getApplication();
		EratosSettings settingsManager = app.getSettingsManager();
		
		FileRevisionContentProviderAdapter fileRevision = new FileRevisionContentProviderAdapter(app.getContentResolver());
		FileManager fileManager = FileManager.getFileManager();
		
		EratosUri fileUri = fileManager.getDefaultAttachmentUri(entry.getSuggestedAttachmentName() + ".pdf");
		if(fileUri == null) {
			NotificationUtils.longToast(app, "Bad file uri: " + settingsManager.getStoreLocalLocation() + settingsManager.getStoreLocalSubdir() + ".pdf");
			return;
		}
		
		EratosUri resUri = fileUri.resolveUri(app);
		File file = fileManager.getFile(resUri);
		boolean exists = fileRevision.exists(resUri) || file.exists();
		if(!exists){
			try{
				WebClient.newInstance().downloadPdfFile(pdfUri.toString(), fileUri, listener);
				
				if(resUri.isRemoteProtocol(app))
					fileRevision.newSyncedFile(resUri, "", file, entry.important());
				entry.attachFile(fileUri);
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}
