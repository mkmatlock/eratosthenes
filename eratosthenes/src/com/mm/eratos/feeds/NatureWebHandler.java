package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.io.ris.RISEntry;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.Validator;
import com.mm.eratos.utils.WebClient;

public class NatureWebHandler extends AbstractWebHandler {
	String naturePattern = "www.nature.com/([a-zA-Z0-9]+)/journal/(.*)/(.*)/(?:abs|full)/(.*).html";
	EratosUri natureRisUri = EratosUri.parseUri("http://www.nature.com/%s/journal/%s/%s/ris/%s.ris");
	EratosUri naturePdfUri = EratosUri.parseUri("http://www.nature.com/%s/journal/%s/%s/pdf/%s.pdf");
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.matches(naturePattern);
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		RISEntry ris = getRIS(uri);
		String doi = getDOI(ris);
		if(doi == null) throw new IOException("Unable to retrieve bibtex for entry: Could not find DOI");
		return getBibtexByDOI(doi);
	}

	private String getDOI(RISEntry ris) {
		for(String val : ris.get("UR"))
			if(val.startsWith("http://dx.doi.org/"))
				return val.substring("http://dx.doi.org/".length());
		
		for(String val : ris.get("L3"))
			if(Validator.isDOI(val.trim()))
				return val.trim();
		
		for(String val : ris.get("N1"))
			if(Validator.isDOI(val.trim()))
				return val.trim();
		
		for(String val : ris.get("M3"))
			if(Validator.isDOI(val.trim()))
				return val.trim();
		
		return null;
	}
	
	private EratosUri extractFormat(EratosUri uri, EratosUri destFormat) throws IOException{
		Matcher m = uri.extract(naturePattern);
		if(!m.matches()) throw new IOException("Incompatible URL format: " + uri);
		String journal = m.group(1);
		String id1 = m.group(2);
		String id2 = m.group(3);
		String id3 = m.group(4);
		
		EratosUri requestUri = destFormat.format(journal, id1, id2, id3);
		return requestUri;
	}

	private RISEntry getRIS(EratosUri uri) throws IOException {
		EratosUri requestUri = extractFormat(uri, natureRisUri);
		InputStream content = WebClient.newInstance().queryContent(requestUri, false).getContent();
		return RISEntry.parse(content);
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		Document htmlPage = WebClient.newInstance().queryHtmlPage(uri, true);
		String summary = htmlPage.select("div#abs p").text();
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri)
			throws IOException, ParseException, FieldRequiredException {
		EratosUri pdfUri = extractFormat(uri, naturePdfUri);
		return pdfUri;
	}
}
