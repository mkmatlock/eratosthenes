package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.ParseException;
import org.jsoup.nodes.Document;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.WebClient;

public class GenomeResearchWebHandler extends AbstractWebHandler {

	Pattern pathPattern = Pattern.compile("(m.)?genome.cshlp.org/content/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)(\\.html|\\.full|\\.short|\\.full.html|\\.short.html)?");
	EratosUri formatUri = EratosUri.parseUri("http://genome.cshlp.org/content/%s/%s/%s");
	
	@Override
	public boolean handles(EratosUri uri) {
		return pathPattern.matcher(uri.path()).matches();
	}

	private String getDOI(EratosUri uri) throws IOException {
		uri = parseUri(uri);
		Document page = WebClient.newInstance().queryHtmlPage(uri);
		return page.select("div#slugline span.slug-doi").text();
	}
	
	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		String doi = getDOI(uri);
		return getBibtexByDOI(doi);
	}
	
	private EratosUri parseUri(EratosUri uri) {
		Matcher m = pathPattern.matcher(uri.path());
		if(m.matches())
			return formatUri.format(m.group(2), m.group(3), m.group(4));
		return null;
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		uri = parseUri(uri);
		Document page = WebClient.newInstance().queryHtmlPage(uri);
		
		String summary = page.select("div#abstract-1 p").text();
		entry.setSummary(summary);
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		return EratosUri.parseUri(parseUri(uri).toString() + ".full.pdf");
	}
}
