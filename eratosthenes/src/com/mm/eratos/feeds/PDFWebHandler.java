package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.InputStream;

import org.jbibtex.ParseException;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;

public class PDFWebHandler extends AbstractWebHandler {

	@Override
	public boolean handles(EratosUri uri) {
		return uri.extension().equals("pdf");
	}

	@Override
	public InputStream bibtex(EratosUri uri) throws IOException {
		return null;
	}

	@Override
	public void extras(BibTeXEntryModel entry, EratosUri pdfUri) throws IOException, ParseException, FieldRequiredException {
		
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		return uri;
	}

}
