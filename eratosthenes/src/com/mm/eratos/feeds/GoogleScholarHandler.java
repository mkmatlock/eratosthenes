package com.mm.eratos.feeds;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mm.eratos.application.EratosApplication;
import com.mm.eratos.application.EratosLogger;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebClient.DownloadListener;
import com.mm.eratos.utils.WebUtils;

public class GoogleScholarHandler {
	static final String scholar_url = "https://scholar.google.com/scholar?hl=en&q=%s&btnG=Search&as_subj=eng&as_sdt=1,5&as_ylo=&as_vis=0";
	static final String cookie_field = "GSP";
	static final String cookie_str = "CF=%d";
	static final int cite_format_BIBTEX = 4;
	static final EratosUri scholarUri = EratosUri.parseUri("https://scholar.google.com");
	private WebClient mClient;

	public GoogleScholarHandler(){
		mClient = WebClient.newInstance();
	}
	
	public class ScholarEntry{
		String entryHtml;
		String bibUrl;
		String pdfUrl;
		String webUrl;
		
		public ScholarEntry(Element entryDiv){
			entryHtml = entryDiv.outerHtml();
			bibUrl = null;
			pdfUrl = null;
			
			Elements anchors = entryDiv.getElementsByTag("a");
			for(Element e : anchors){
				String link = e.text();
				String href = e.attr("href").trim();
				if(e.parent().attr("class").equals("gs_rt"))
					webUrl = href;
				if(href.startsWith("/scholar.bib?"))
					bibUrl = href;
				if(link.contains("[PDF]") || href.endsWith(".pdf"))
					pdfUrl = href;
			}
		}
		
		public String getHtml(){
			return entryHtml;
		}
		
		public boolean hasWeb(){
			return webUrl != null;
		}
		
		public boolean hasPdf(){
			return pdfUrl != null;
		}
		
		public EratosUri getWebUri(){
			return EratosUri.parseUri(webUrl);
		}
		
		public void retrievePdf(EratosUri dest, DownloadListener downloadListener) throws IOException{
			WebClient.newInstance().downloadFile(pdfUrl, dest, downloadListener);
		}
		
		public EratosUri retrieveBibtexUri() throws IOException{
			return scholarUri.append(bibUrl);
		}
	}
	
	public String setImportBibTeXFlag(String cookie) {
		String flag = String.format(Locale.US, cookie_str, cite_format_BIBTEX);
		
		String[] fields = cookie.split(";");
		List<String> nCookie = new ArrayList<String>();
		
		for(String f : fields) {
			f = f.trim();
			
			int eq_idx = f.indexOf("=");
			if(eq_idx > 0){
				String fname = f.substring(0, eq_idx);
				String fval = f.substring(eq_idx+1);
				if(fname.equals( "GSP" )){
					List<String> nArgs = new ArrayList<String>();
					boolean cfAdded = false;
					for(String farg : fval.split(":")){
						String[] sp = farg.split("=");
						if(sp[0].equals("CF")){
							nArgs.add(flag);
							cfAdded=true;
						}else{
							nArgs.add(farg);
						}
					}
					
					if(!cfAdded)
						nArgs.add(flag);
						
					String nArg = fname + "=" + StringUtils.join(":", nArgs);
					nCookie.add(nArg);
				}else{
					nCookie.add(f);
				}
			}else{
				nCookie.add(f);
			}
			
		}
		
		return StringUtils.join("; ", nCookie).trim()+";";
	}

	public static String generateSearchUri(String query){
		try{
			return String.format(scholar_url, URLEncoder.encode(query, "UTF-8"));
		}catch(UnsupportedEncodingException e){
			throw new RuntimeException(e);
		}
	}
	
	public static String parseSearchUri(String uri){
		List<NameValuePair> arguments = URLEncodedUtils.parse(URI.create(uri), "UTF-8");
		
		for(NameValuePair nvp : arguments){
			if(nvp.getName().equalsIgnoreCase("q"))
				return nvp.getValue();
		}
		
		return "";
	}
	
	public List<ScholarEntry> getEntries(String queryUrl, String cookie) throws IOException {
//		String requestUri = GoogleScholarHandler.generateSearchUri(query);
		cookie = setImportBibTeXFlag(cookie);
		
		EratosLogger logger = EratosApplication.getApplication().getLogger();
		logger.debug("Setting cookie: %s", cookie);
		
		Map<String, String> headers = WebUtils.makeHeaders("Cookie", cookie);
		Document html = mClient.queryHtmlPage(queryUrl, headers);
		
		logger.debug("Scholar Entry Query Result:\n%s", html.toString());
		
		Elements records = html.getElementsByClass("gs_r");
		
		List<ScholarEntry> entries = new ArrayList<ScholarEntry>(records.size());
		for(Element gs_r : records)
			entries.add(new ScholarEntry(gs_r));
		
		return entries;
	}
	
	public boolean handles(EratosUri uri) {
		return uri.matches("scholar.google.[a-z]+/scholar.bib.*");
	}

	public String getBibtex(EratosUri bibUri, String cookie) throws IOException {
		cookie = setImportBibTeXFlag(cookie);
		Map<String, String> headers = WebUtils.makeHeaders("Cookie", cookie);
		Document html = mClient.queryHtmlPage(bibUri, headers);
		return html.text();
	}
}
