package com.mm.eratos.feeds;

import java.util.ArrayList;
import java.util.List;

import com.mm.eratos.model.EratosUri;

public class WebHandlerFactory {
	private static List<AbstractWebHandler> handlerTypes;
	static{
		handlerTypes = new ArrayList<AbstractWebHandler>();
		handlerTypes.add(new ACMDigitalLibraryHandler());
		handlerTypes.add(new ACSWebHandler());
		handlerTypes.add(new OxfordJournalsWebHandler());
		handlerTypes.add(new DOIWebHandler());
		handlerTypes.add(new GenomeResearchWebHandler());
//		handlerTypes.add(new GoogleScholarHandler());
		handlerTypes.add(new JBCWebHandler());
		handlerTypes.add(new NatureWebHandler());
		handlerTypes.add(new NEJMWebHandler());
		handlerTypes.add(new PLOSWebHandler());
		handlerTypes.add(new PMCWebHandler());
		handlerTypes.add(new PubMedWebHandler());
		handlerTypes.add(new SpringerLinkWebHandler());
		handlerTypes.add(new ScienceDirectWebHandler());
		handlerTypes.add(new ScienceWebHandler());
		handlerTypes.add(new WileyWebHandler());
	}
	
	public static AbstractWebHandler getHandler(EratosUri uri){
		for(AbstractWebHandler handler : handlerTypes){
			if(handler.handles(uri)){
				return handler;
			}
		}
		return null;
	}
}
