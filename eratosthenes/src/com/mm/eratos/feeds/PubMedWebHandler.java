package com.mm.eratos.feeds;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.jbibtex.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mm.eratos.bibtex.BibTeXEntryModel;
import com.mm.eratos.exceptions.FieldRequiredException;
import com.mm.eratos.model.EratosUri;
import com.mm.eratos.utils.StringUtils;
import com.mm.eratos.utils.WebClient;
import com.mm.eratos.utils.WebUtils;

public class PubMedWebHandler extends AbstractWebHandler {
	public static final String pubmedServiceUrl = "http://www.bioinformatics.org/texmed/cgi-bin/list.cgi?PMID=%s";
	public static final EratosUri pubmedUrl = EratosUri.parseUri("http://www.ncbi.nlm.nih.gov/pubmed/%s");
	
	public static final String pubMedCentralPdfUri = "http://www.ncbi.nlm.nih.gov/pmc/articles/%s/pdf/";
	public static final String pubMedCentralUri = "http://www.ncbi.nlm.nih.gov/pmc/articles/%s";
	
	@Override
	public boolean handles(EratosUri uri) {
		return uri.protocol().equals("http") && 
				(uri.path().matches("www.ncbi.nlm.nih.gov/(m/)?pubmed/[0-9]+(/)?"));
	}


	@Override
	public void extras(BibTeXEntryModel entry, EratosUri uri)
			throws IOException, ParseException, FieldRequiredException {
		Document doc = WebClient.newInstance().queryHtmlPage(uri.toString() + "?report=xml&format=text");
		doc = Jsoup.parse(doc.select("pre").text().replace("&lt;", "<").replace("&gt;", ">"));
		
		extractAbstract(doc, entry);
		extractCitations(doc, entry);
		extractIds(doc, entry);
		entry.setField(BibTeXEntryModel.PMID_KEY, uri.name());
		
	}

	@Override
	public EratosUri pdfUri(BibTeXEntryModel entry, EratosUri uri) throws IOException, ParseException, FieldRequiredException {
		if(entry.hasField("pmcid"))
			return EratosUri.parseUri(String.format(Locale.US, pubMedCentralPdfUri, entry.getFieldValue("pmcid")));
		return null;
	}

	
	protected void extractCitations(Document doc, BibTeXEntryModel entry){
		List<String> citedPubMed = new ArrayList<String>();
		Elements citations = doc.select("PubmedArticle MedlineCitation CommentsCorrectionsList CommentsCorrections");
		
		for(Element citation : citations){
			String type = citations.attr("refType");
			if("cites".equalsIgnoreCase(type)){
				String pmid = citation.select("PMID").text();
				citedPubMed.add(pmid);
			}
		}
		
		if(citedPubMed.size() > 0) {
			String formattedCitations = StringUtils.join(";", citedPubMed);
			entry.setField(BibTeXEntryModel.PMCITES_KEY, formattedCitations);
		}
	}

	protected void extractAbstract(Document doc, BibTeXEntryModel entry)
	{
		Elements selection = doc.select("PubmedArticle MedlineCitation Article Abstract");
		if(selection.size() > 0) {
			Element abstractElem = selection.first();
	
			StringBuilder abstractText = new StringBuilder();
			for (Element child : abstractElem.children())
			{
				abstractText.append(child.attr("Label"));
				abstractText.append("\n\n");
	
				abstractText.append(child.text());
				abstractText.append("\n\n");
			}
			entry.setField(BibTeXEntryModel.ABSTRACT_KEY, abstractText.toString());
		}
	}

	protected void extractIds(Document doc, BibTeXEntryModel entry)
	{
		Map<String, String> idTypes = new HashMap<String, String>();

		Elements aids = doc.select("PubmedArticle PubmedData ArticleIdList ArticleId");
		for (Element e : aids)
		{
			String t = e.attr("IdType");
			String v = e.text();

			idTypes.put(t, v);
		}

		if (idTypes.containsKey("pmc"))
		{
			entry.setField(BibTeXEntryModel.PMCID_KEY, idTypes.get("pmc"));
		}
		if (idTypes.containsKey("doi"))
		{
			entry.setField(BibTeXEntryModel.DOI_KEY, idTypes.get("doi"));
		}
	}

	public InputStream bibtex(EratosUri uri) throws IOException {
		String pubmedId = uri.name();
		String intentURI = String.format(pubmedServiceUrl, pubmedId);
		
    	HttpClient mHttpClient = new DefaultHttpClient();
    	HttpGet httpGet = new HttpGet(intentURI);
    	
        httpGet.addHeader("Accept", "text/bibliography; style=bibtex");
        HttpParams params = mHttpClient.getParams();
        HttpClientParams.setRedirecting(params, true);

        HttpResponse response = mHttpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode != 200)
        	throw new IOException(String.format("HTTP status %d: %s", statusCode, response.getStatusLine().getReasonPhrase()));
        
        HttpEntity entity= response.getEntity();
        
        InputStream content = entity.getContent();
        String sb = WebUtils.readStream(content);
        
        Document doc = Jsoup.parse(sb);
        Elements selection = doc.select("html body pre");
        
		if(selection.size() == 0)
			throw new IOException(String.format("Error fetching %s. PMID not found.", intentURI));
		
    	String bibtexContent = selection.get(0).text();
        return new ByteArrayInputStream(bibtexContent.getBytes());
	}
}
