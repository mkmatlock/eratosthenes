#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

adb wait-for-device
adb push $DIR/files/clear_table_exec.sh /sdcard/Download/
adb shell sh /sdcard/Download/clear_table_exec.sh $1
