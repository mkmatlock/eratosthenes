#!/bin/bash

activity_name=${1:-EntryGroupActivity}

adb wait-for-device

if adb shell dumpsys input_method | grep -q "mScreenOn=false";
then
    adb shell input keyevent 26
fi
adb shell am start -n com.mm.eratos/.activity.$activity_name
