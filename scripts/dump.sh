#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
adb wait-for-device
adb pull /data/data/com.mm.eratos/databases/entries.db entries.db

if [ ! -e objects ]
then
    mkdir objects
fi

echo "select _id from annotations;" | sqlite3 entries.db > objects/id_file
cnt=`wc -l objects/id_file | awk {'print $1'}`
i=0

for id in `cat objects/id_file`
do
    echo "SELECT quote(object) FROM annotations WHERE _id = $id;" | sqlite3 entries.db | cut -d\' -f2 | xxd -r -p > objects/$id.object
    echo -en "\r$i / $cnt"
    i=$((i+1))
done
echo ""
